<?php
/**
 * StoreCore Installer
 *
 * This installation application bypasses the main front controller, which
 * MAY fail due tu a missing or misconfigured database connection or some
 * missing configuration setting.  To allow for a command-line interface (CLI)
 * installation, initial file system checks will result in PHP escape code
 * `exit(1)` on errors.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2018, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0-alpha.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Error reporting
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Only allow execution through a command-line interface (CLI).
if (php_sapi_name() !== 'cli') {
    exit('This utility must be executed from a command-line interface (CLI).');
}

// Load configuration files.
$dir = $_SERVER["SCRIPT_FILENAME"];
$dir = str_replace('bin' . DIRECTORY_SEPARATOR . 'install.php', 'config', $dir);
if (!is_dir($dir)) {
    exit('Configuration folder ' . $dir . ' not foud.');
}

// Require version.php.
$file = $dir . DIRECTORY_SEPARATOR . 'version.php';
if (is_file($file)) {
    require_once $file;
} else {
    exit('StoreCore versioning file ' . $file . ' not found.');
}

// Require config.php.
$file = $dir . DIRECTORY_SEPARATOR . 'config.php';
if (is_file($file)) {
    require_once $file;
} else {
    exit('StoreCore configuration file ' . $file . ' not found.');
}

// Boot the core: require bootloader.php from the /src/ library.
$dir = $_SERVER["SCRIPT_FILENAME"];
$dir = str_replace('bin' . DIRECTORY_SEPARATOR . 'install.php', 'src', $dir);
$file = realpath($dir . DIRECTORY_SEPARATOR . 'bootloader.php');
if ($file !== false && is_file($file)) {
    require_once $file;
} elseif (\defined('STORECORE_FILESYSTEM_SRC_DIR') && is_file(STORECORE_FILESYSTEM_SRC_DIR . 'bootloader.php')) {
    require_once STORECORE_FILESYSTEM_SRC_DIR . 'bootloader.php';
} else {
    exit('StoreCore bootstrap file bootloader.php not found.');
}

// The bootloader should provide a $registry and a $logger.
if (!isset($registry)) {
    exit('StoreCore registry could not be loaded.');
} elseif (!isset($registry)) {
    exit('Logger could not be loaded.');
} else {
    $logger->notice('Command-line installer loaded the bootstrap.');
}

// Run the installer through the administration front controller’s
// `install()` method as a single point of entry.
$route = new \StoreCore\Route('/install/', '\StoreCore\Admin\FrontController');
$route->dispatch();
