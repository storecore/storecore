<?php

declare(strict_types=1);

/**
 * StoreCore™ store front application.
 *
 * @api
 * @copyright Copyright © 2015–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0-alpha.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Global script/request timer start
if (empty($_SERVER['REQUEST_TIME_FLOAT'])) $_SERVER['REQUEST_TIME_FLOAT'] = microtime(true);

// Working directory
$dir = realpath(__DIR__);
if ($dir !== false) {
    define('STORECORE_FILESYSTEM_ROOT_DIR', $dir . DIRECTORY_SEPARATOR);
} else {
    define('STORECORE_FILESYSTEM_ROOT_DIR', __DIR__ . DIRECTORY_SEPARATOR);
}

// Load configuration
require STORECORE_FILESYSTEM_ROOT_DIR . 'config' . DIRECTORY_SEPARATOR . 'version.php';
require STORECORE_FILESYSTEM_ROOT_DIR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

// Boot
if (!\defined('STORECORE_FILESYSTEM_SRC_DIR')) {
    define('STORECORE_FILESYSTEM_SRC_DIR', STORECORE_FILESYSTEM_ROOT_DIR . 'src' . DIRECTORY_SEPARATOR);
}
require STORECORE_FILESYSTEM_SRC_DIR . 'bootloader.php';

// Handle redirects and cacheable requests in a Chain of Responsibility.
if (
    $registry->get('Request')->getMethod() === 'HEAD'
    || $registry->get('Request')->getMethod() === 'GET'
) {
    $request_handler_chain = new \StoreCore\Engine\RedirectRequestHandler(
        new \StoreCore\Engine\AssetCacheRequestHandler(
            new \StoreCore\Engine\FullPageCacheRequestHandler()
        )
    );
    $response = $request_handler_chain->handle($registry->get('Request'));
    if ($response->getStatusCode() >= 200) {
        $response->output();
        if ($response->getStatusCode() >= 500) {
            $logger->error('Server error ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());
        } elseif ($response->getStatusCode() >= 400) {
            $logger->info('Client error ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());
        }
        exit;
    }
    unset($request_handler_chain, $response);
}

// Create a response if there is no cache or API response.
$response_factory = new \StoreCore\Engine\ResponseFactory();

// Handle API requests for any `api.example.com` or `example.com/api/`.
try {
    if (
        str_starts_with($registry->get('Request')->getUri()->getHost(), 'api.')
        || str_starts_with($registry->get('Request')->getRequestTarget(), '/api/')
    ) {
        $api = new \StoreCore\API\RequestHandler($registry);
        $response = $api->handle($registry->get('Request'));
        $response->output();
        exit;
    }
} catch (\RuntimeException $e) {
    $logger->error(
        'API runtime exception ' . $e->getCode() . ' ' . $e->getMessage() . ': '
        . $registry->get('Request')->getMethod() . ' ' . $registry->get('Request')->getRequestTarget()
    );
    $response = $response_factory->createResponse(500);
    $response->output();
    exit;
}

// Execute an administration route.
if (str_starts_with($registry->get('Request')->getRequestTarget(), '/admin')) {
    $route = new \StoreCore\Route('/admin', 'StoreCore\Admin\FrontController');
    $registry->set('Route', $route);
    $route->dispatch();
    exit;
}

// Status check: GET /status
if ($registry->get('Request')->getRequestTarget() === '/status') {
    $endpoint = new \StoreCore\Status($registry);
    $endpoint->output();
}

// Start or restart session
$session_factory = new \StoreCore\SessionFactory($registry);
$session = $session_factory->createSession();
unset($session_factory);

// Load a language pack
$language = $session->has('Language') ? $session->get('Language') : null;
$language = \StoreCore\I18N\LanguagePacks::load($language);
$session->set('Language', $language);

// Register language and session
$registry->set('Language', $language);
$registry->set('Session', $session);
unset($language, $session);

// Routing
try {
    $route_repository = new \StoreCore\Database\RouteRepository($registry);
    $route = $route_repository->get($registry->get('Request')->getRequestTarget());
    $registry->set('Route', $route);
    $route->dispatch();
} catch (Psr\Container\NotFoundExceptionInterface $e) {
    $logger->notice(
        'HTTP/' . $response->getProtocolVersion() . ' 404 Not Found: ' 
        . $registry->get('Request')->getMethod() . ' ' . $registry->get('Request')->getUri()
    );
    $registry->set('Response', $response_factory->createResponse(404));
} catch (\Exception $e) {
    $logger->error(
        'Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line '
        . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace()
    );
    $registry->set('Response', $response_factory->createResponse(500));
}

if ($registry->has('Response')) {
    $registry->get('Response')->output();
}
