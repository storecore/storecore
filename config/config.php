<?php

/**
 * StoreCore™ Global Configuration File
 */

/* Core */
define('STORECORE_API_ROOT', '/api/v1/');
define('STORECORE_LANGUAGES', '{"en-GB":true,"en-US":true,"de-DE":true,"fr-FR":true,"nl-NL":true,"nl-BE":true}');
define('STORECORE_MAINTENANCE_MODE', false);
define('STORECORE_NULL_LOGGER', false);

/* Database */
define('STORECORE_DATABASE_DRIVER',           'mysql');
define('STORECORE_DATABASE_DEFAULT_HOST',     'localhost');
define('STORECORE_DATABASE_DEFAULT_DATABASE', 'test');
define('STORECORE_DATABASE_DEFAULT_USERNAME', 'root');
define('STORECORE_DATABASE_DEFAULT_PASSWORD', '');

/* File System */
define('STORECORE_FILESYSTEM_ASSETS_DIR', '');
// define('STORECORE_FILESYSTEM_CACHE_DIR', '');
// define('STORECORE_FILESYSTEM_CACHE_DATA_DIR', '');
// define('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR', '');
// define('STORECORE_FILESYSTEM_CACHE_PAGES_DIR', '');
// define('STORECORE_FILESYSTEM_LOGS_DIR', '');
define('STORECORE_FILESYSTEM_LOGS_FILENAME_FORMAT', 'Ymd');
// define('STORECORE_FILESYSTEM_SRC_DIR', '');

/* Business Intelligence (BI) */
define('STORECORE_BI', false);
define('STORECORE_BI_GOOGLE_ANALYTICS', false);
define('STORECORE_BI_GOOGLE_ANALYTICS_SITE_SPEED_SAMPLE_RATE', 1);
define('STORECORE_BI_GOOGLE_TAG_ID', 'G-XXXXXXXXX');

/* Content Management System (CMS) */
define('STORECORE_CMS_BLOCKLIST', false);
define('STORECORE_CMS_CRAWL_DELAY', 10);
define('STORECORE_CMS_REDIRECTS', false);

/*
 * Set global constants to defaults if they are not defined above.
 * This temporary section is replaced by actual contants during setup.
 */
if (!\defined('STORECORE_FILESYSTEM_CACHE_DIR')) {
    $dir = realpath(__DIR__ . '/../resources/cache');
    if ($dir !== false && is_dir($dir)) {
        define('STORECORE_FILESYSTEM_CACHE_DIR', $dir . DIRECTORY_SEPARATOR);
    } else {
        define('STORECORE_FILESYSTEM_CACHE_DIR', '/../resources/cache' . DIRECTORY_SEPARATOR);
    }
}
if (!\defined('STORECORE_FILESYSTEM_CACHE_DATA_DIR')) {
    define('STORECORE_FILESYSTEM_CACHE_DATA_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'data' . DIRECTORY_SEPARATOR);
}
if (!\defined('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR')) {
    define('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'objects' . DIRECTORY_SEPARATOR);
}
if (!\defined('STORECORE_FILESYSTEM_CACHE_PAGES_DIR')) {
    define('STORECORE_FILESYSTEM_CACHE_PAGES_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'pages' . DIRECTORY_SEPARATOR);
}

if (STORECORE_NULL_LOGGER !== true && !\defined('STORECORE_FILESYSTEM_LOGS_DIR')) {
    $dir = realpath(__DIR__ . '/../logs');
    if ($dir !== false && is_writable($dir)) {
        define('STORECORE_FILESYSTEM_LOGS_DIR', $dir . DIRECTORY_SEPARATOR);
    } else {
        define('STORECORE_FILESYSTEM_LOGS_DIR', __DIR__ . '/../logs' . DIRECTORY_SEPARATOR);
    }
    unset($dir);
}
