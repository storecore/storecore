# StoreCore configuration

The StoreCore™ ecommerce framework is configured with global PHP constants in
the config.php configuration file.  Configuration settings and feature flags
are listed below, grouped per domain, in alphabetical order.

## Content Management System (CMS)

### int STORECORE_CMS_CRAWL_DELAY = (int) 10

The `Crawl-delay` for the robots.txt file in seconds.  Minimum is `1` second;
maximum is `300` seconds (5 minutes).  Defaults to `10` seconds.

## File System

### string STORECORE_FILESYSTEM_CACHE_DIR

Path to the `cache` directory in the file system.  The `cache` directory
contains three subdirectories for `data`, `objects`, and `pages`.


Copyright © 2024 StoreCore™. All rights reserved.

Except as otherwise noted, the content of this document is licensed under the
[Creative Commons Attribution 4.0 License], and code samples are licensed under
the [GNU General Public License version 3 (GPLv3)].

[Creative Commons Attribution 4.0 License]: https://creativecommons.org/licenses/by/4.0/

[GNU General Public License version 3 (GPLv3)]: https://www.gnu.org/licenses/gpl.html
