<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\AbstractOrderVisitor::class)]
#[Group('hmvc')]
final class AbstractOrderVisitorTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractOrderVisitor class file exists')]
    public function testAbstractOrderVisitorClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractOrderVisitor.php');
    }

    #[Depends('testAbstractOrderVisitorClassFileExists')]
    #[Group('distro')]
    #[TestDox('AbstractOrderVisitor class file is readable')]
    public function testAbstractOrderVisitorClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractOrderVisitor.php');
    }

    #[Group('distro')]
    #[TestDox('AbstractOrderVisitor class exists')]
    public function testAbstractOrderVisitorClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\AbstractOrderVisitor'));
        $this->assertTrue(class_exists(AbstractOrderVisitor::class));
    }


    #[TestDox('AbstractOrderVisitor class is abstract')]
    public function testAbstractOrderVisitorClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractOrderVisitor::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }
}
