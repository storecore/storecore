<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\ItemList::class)]
final class ItemListTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ItemList class exists')]
    public function testItemListClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ItemList.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ItemList.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\ItemList'));
        $this->assertTrue(class_exists(ItemList::class));
    }

    #[TestDox('ItemList class is concrete')]
    public function testItemListClassIsConcrete(): void
    {
        $class = new ReflectionClass(ItemList::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ItemList is an Intangible Thing')]
    public function testItemListIsIntangibleThing(): void
    {
        $list = new ItemList();
        $this->assertInstanceOf(Intangible::class, $list);
        $this->assertInstanceOf(Thing::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('ItemList implements PHP ArrayAccess interface')]
    public function testItemListImplementsPhpArrayAccessInterface(): void
    {
        $list = new ItemList();
        $this->assertInstanceOf(\ArrayAccess::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('ItemList implements PHP Countable interface')]
    public function testItemListImplementsPhpCountableInterface(): void
    {
        $list = new ItemList();
        $this->assertInstanceOf(\Countable::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('ItemList is JSON serializable')]
    public function testItemListIsJsonSerializable(): void
    {
        $list = new ItemList();
        $this->assertInstanceOf(\JsonSerializable::class, $list);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ItemList::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ItemList::VERSION);
        $this->assertIsString(ItemList::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ItemList::VERSION, '0.1.0', '>=')
        );
    }

    /**
     * @see https://schema.org/ItemList#eg-0016
     *      Schema.org `ItemList` example 1
     * 
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "ItemList",
     *   "itemListElement": [
     *     "HP Pavilion dv6-6013cl",
     *     "Dell XPS 15 (Sandy Bridge)",
     *     "Lenovo ThinkPad X220"
     *   ],
     *   "itemListOrder": "https://schema.org/ItemListOrderDescending",
     *   "name": "Top 10 laptops"
     * }
     * ```
     */
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest(): void
    {
        $list = new ItemList('Top 10 laptops');

        $this->assertNotEmpty($list->name);
        $this->assertIsString($list->name);
        $this->assertEquals('Top 10 laptops', $list->name);

        $this->assertNull($list->itemListElement);

        $list->itemListElement = array(
            'HP Pavilion dv6-6013cl',
            'Dell XPS 15 (Sandy Bridge)',
            'Lenovo ThinkPad X220'
        );

        $this->assertNotNull($list->itemListElement);
        $this->assertNotEmpty($list->itemListElement);
        $this->assertIsArray($list->itemListElement);

        $this->assertNull($list->numberOfItems);
        $this->assertEquals(3, $list->count());
        $this->assertEquals(3, count($list));
        $this->assertCount(3, $list);

        $this->assertNotNull($list->numberOfItems);
        $this->assertEquals(3, $list->numberOfItems);
    }

    #[Depends('testSchemaOrgAcceptanceTest')]
    #[TestDox('ItemList elements MAY be set one by one')]
    public function testItemListElementsMayBeSetOneByOne(): void
    {
        $list = new ItemList();
        $list->name = 'Top 10 laptops';
        $this->assertEquals('Top 10 laptops', $list->name);

        $this->assertEmpty($list->itemListElement);
        $this->assertCount(0, $list);

        $list->itemListElement = 'HP Pavilion dv6-6013cl';
        $this->assertNotEmpty($list->itemListElement);
        $this->assertCount(1, $list);

        $list->itemListElement = 'Dell XPS 15 (Sandy Bridge)';
        $this->assertCount(2, $list);

        $list->itemListElement = 'Lenovo ThinkPad X220';
        $this->assertCount(3, $list);
    }
}
