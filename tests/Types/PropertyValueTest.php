<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Types\PropertyValue::class)]
final class PropertyValueTest extends TestCase
{
    #[TestDox('PropertyValue class is concrete')]
    public function testPropertyValueClassIsConcrete(): void
    {
        $class = new ReflectionClass(PropertyValue::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PropertyValue is an Intangible Thing')]
    public function testPropertyValueIsAnIntangibleThing(): void
    {
        $property_value = new PropertyValue();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $property_value);
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, $property_value);
    }

    #[Group('hmvc')]
    #[TestDox('PropertyValue is a StructuredValue')]
    public function testPropertyValueIsStructuredValue(): void
    {
        $this->assertInstanceOf(\StoreCore\Types\StructuredValue::class, new PropertyValue());
    }

    #[Group('hmvc')]
    #[TestDox('PropertyValue is JSON serializable')]
    public function testPropertyValueIsJsonSerializable(): void
    {
        $property_value = new PropertyValue();
        $this->assertInstanceOf(\JsonSerializable::class, $property_value);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PropertyValue::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PropertyValue::VERSION);
        $this->assertIsString(PropertyValue::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PropertyValue::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://schema.org/PropertyValue
     *   Use the `name` property for the name of the property.  If there is
     *   an additional human-readable version of the value, put that into the
     *   `description` property.
     */
    #[TestDox('PropertyValue.name and PropertyValue.description exist')]
    public function testPropertyValueNameAndPropertyValueDescriptionExist(): void
    {
        $propertyValue = new PropertyValue();
        $this->assertObjectHasProperty('name', $propertyValue);
        $this->assertObjectHasProperty('description', $propertyValue);
    }


    #[TestDox('PropertyValue.value exists')]
    public function testPropertyValueValueExists(): void
    {
        $propertyValue = new PropertyValue();
        $this->assertObjectHasProperty('value', $propertyValue);
        $this->assertNull($propertyValue->value);
    }

    #[TestDox('PropertyValue.minValue and PropertyValue.maxValue exist')]
    public function testPropertyValueMinValueAndPropertyValueMaxValueExist(): void
    {
        $propertyValue = new PropertyValue();

        $this->assertObjectHasProperty('minValue', $propertyValue);
        $this->assertNull($propertyValue->minValue);

        $this->assertObjectHasProperty('maxValue', $propertyValue);
        $this->assertNull($propertyValue->maxValue);
    }

    #[TestDox('PropertyValue.unitCode and PropertyValue.unitText exist')]
    public function testPropertyValueUnitCodeAndPropertyValueUnitTextExist(): void
    {
        $propertyValue = new PropertyValue();

        $this->assertObjectHasProperty('unitCode', $propertyValue);
        $this->assertNull($propertyValue->unitCode);

        $this->assertObjectHasProperty('unitText', $propertyValue);
        $this->assertNull($propertyValue->unitText);
    }


    /**
     * @see https://schema.org/PropertyValue#eg-0404
     *      Schema.org `PropertyValue` example 1
     *
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "ImageObject",
     *   "author": "Jane Doe",
     *   "contentLocation": "Puerto Vallarta, Mexico",
     *   "contentUrl": "mexico-beach.jpg",
     *   "datePublished": "2008-01-25",
     *   "description": "I took this picture while on vacation last year.",
     *   "name": "Beach in Mexico",
     *   "exifData": [
     *     {
     *       "@type": "PropertyValue",
     *       "name": "Exposure Time",
     *       "value": "1/659 sec."
     *     },
     *     {
     *      "@type": "PropertyValue",
     *       "name": "FNumber",
     *       "value": "f/4.0"
     *     },
     *     {
     *       "@type": "PropertyValue",
     *       "name": "MaxApertureValue",
     *       "value": "2.00"
     *     },
     *     {
     *       "@type": "PropertyValue",
     *       "name": "Metering Mode",
     *       "value": "Pattern"
     *     },
     *     {
     *       "@type": "PropertyValue",
     *       "name": "Flash",
     *       "value": "Flash did not fire."
     *     }
     *   ]
     * }
     * ```
     */
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest(): void
    {
        $exifData = array();

        $property = new PropertyValue();
        $property->name = 'Exposure Time';
        $property->value = '1/659 sec.';
        $exifData[] = $property;

        $property = new PropertyValue();
        $property->name = 'FNumber';
        $property->value = 'f/4.0';
        $exifData[] = $property;

        $expectedJson = '
            [
              {
                "@context": "https://schema.org",
                "@type": "PropertyValue",
                "name": "Exposure Time",
                "value": "1/659 sec."
              },
              {
                "@context": "https://schema.org",
                "@type": "PropertyValue",
                "name": "FNumber",
                "value": "f/4.0"
              }
            ]
        ';
        $actualJson = json_encode($exifData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        /*
         * Note that the first property has a value in seconds.
         * This MAY be clarified with a UN/CEFACT `unitCode` and `unitText`.
         */
        $property = new PropertyValue();
        $property->name = 'Exposure Time';
        $property->value = '1/659';
        $property->unitCode = 'SEC';
        $property->unitText = 's';

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "PropertyValue",
              "name": "Exposure Time",
              "value": "1/659",
              "unitCode": "SEC",
              "unitText": "s"
            }
        ';
        $actualJson = json_encode($property, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
