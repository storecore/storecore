<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\LanguageCode::class)]
#[UsesClass(\StoreCore\Types\Varchar::class)]
final class LanguageCodeTest extends TestCase
{
    #[TestDox('LanguageCode class is concrete')]
    public function testLanguageCodeClassIsConcrete(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('LanguageCode class is read-only')]
    public function testLanguageCodeClassIsReadOnly(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('LanguageCode is stringable')]
    public function testLanguageCodeIsStringable(): void
    {
        $code = new LanguageCode('en-US');
        $this->assertInstanceOf(\Stringable::class, $code);
    }

    #[Group('hmvc')]
    #[TestDox('LanguageCode class implements ValidateInterface')]
    public function testLanguageCodeClassImplementsValidateInterface()
    {
        $code = new LanguageCode('en-US');
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, $code);
        $this->assertInstanceOf(ValidateInterface::class, $code);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LanguageCode::VERSION);
        $this->assertIsString(LanguageCode::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LanguageCode::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('LanguageCode::__construct exists')]
    public function testLanguageCodeConstructorExists(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('LanguageCode::__construct is public constructor')]
    public function testLanguageCodeConstructorIsPublic(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('LanguageCode::__construct has one OPTIONAL parameter')]
    public function testLanguageCodeConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLanguageCodeConstructorHasOneOptionalParameter')]
    #[TestDox('LanguageCode::__construct has one OPTIONAL parameter')]
    public function testLanguageCodeConstructorSetsLanguageCode(): void
    {
        $language_code = new LanguageCode('de-DE');
        $this->assertEquals('de-DE', (string) $language_code);

        $language_code = new LanguageCode('en-US');
        $this->assertEquals('en-US', (string) $language_code);

        $language_code = new LanguageCode('fr-FR');
        $this->assertEquals('fr-FR', (string) $language_code);

        $language_code = new LanguageCode('nl-NL');
        $this->assertEquals('nl-NL', (string) $language_code);
    }

    #[TestDox('LanguageCode::__construct throws ValueError on invalid language code')]
    public function testLanguageCodeConstructThrowsValueErrorOnInvalidLanguageCode(): void
    {
        $this->expectException(\ValueError::class);
        $invalid = new LanguageCode('English');
    }

    /**
     * @see https://www.rfc-editor.org/rfc/rfc5646.txt
     *      Example from RFC 5646, section 2.1.1, “Formatting of Language Tags”
     */
    #[Depends('testLanguageCodeConstructorHasOneOptionalParameter')]
    #[Depends('testLanguageCodeConstructThrowsValueErrorOnInvalidLanguageCode')]
    #[TestDox('LanguageCode::__construct normalizes lowercase and uppercase')]
    public function testLanguageCodeConstructNormalizesLowercaseAndUppercase(): void
    {
        $language_code = new LanguageCode('en-us');
        $this->assertNotEquals('en-us', (string) $language_code);
        $this->assertEquals('en-US', (string) $language_code);
        
        $language_code = new LanguageCode('EN-US');
        $this->assertNotEquals('EN-US', (string) $language_code);
        $this->assertEquals('en-US', (string) $language_code);


        $language_code = new LanguageCode('MN-cYRL-mn');
        $this->assertEquals('mn-Cyrl-MN', (string) $language_code);

        $language_code = new LanguageCode('mN-cYrL-Mn');
        $this->assertEquals('mn-Cyrl-MN', (string) $language_code);
    }


    #[TestDox('LanguageCode::__toString exists')]
    public function testLanguageCodeToStringExists(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('LanguageCode::__toString is public')]
    public function testLanguageCodeToStringIsPublic(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('LanguageCode::__toString has no parameters')]
    public function testLanguageCodeToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('LanguageCode::__toString returns non-empty string')]
    public function testLanguageCodeToStringReturnsNonEmptyString(): void
    {
        $language_code = new LanguageCode();
        $this->assertNotEmpty((string) $language_code);
        $this->assertIsString((string) $language_code);
    }

    #[Depends('testLanguageCodeToStringReturnsNonEmptyString')]
    #[TestDox('LanguageCode::__toString defaults to "en-GB" for British English')]
    public function testLanguageCodeToStringDefaultsToEnGbForBritishEnglish(): void
    {
        $language_code = new LanguageCode();
        $this->assertEquals('en-GB', (string) $language_code);
    }


    #[TestDox('LanguageCode::filter exists')]
    public function testLanguageCodeFilterExists(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->hasMethod('filter'));
    }

    #[TestDox('LanguageCode::filter is public static')]
    public function testLanguageCodeFilterIsPublicStatic(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, 'filter');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('LanguageCode::filter has one REQUIRED parameter')]
    public function testLanguageCodeFilterHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, 'filter');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('LanguageCode::filter normalizes language tags')]
    public function testLanguageCodeFilterNormalizesLanguageTags(): void
    {
        $this->assertEquals(
            'en-GB',
            LanguageCode::filter('EN-gb'),
            'Language subtags MUST be lowercase and region subtags MUST be uppercase.'
        );

        $this->assertEquals(
            'es-005',
            LanguageCode::filter('ES-005'),
            'Language subtags MUST be lowercase and region subtags MAY be numbers.'
        );

        $this->assertEquals(
            'az',
            LanguageCode::filter('az-latn'),
            'Script subtag Latn for Latin script MUST be removed.'
        );

        $this->assertEquals(
            'zh-Hant-HK',
            LanguageCode::filter('zh-hant-hk'),
            'Region subtags MUST be uppercase.'
        );

        $this->assertEquals(
            'en-US',
            LanguageCode::filter('en-US-x-twain'),
            'Private use subtags MUST be filtered out.'
        );

        $this->assertEquals(
            'de-DE',
            LanguageCode::filter('de-DE-u-co-phonebk'),
            'Unicode extension subtags MUST be filtered out.'
        );
    }

    #[TestDox('LanguageCode::filter removes Latn for Latin script')]
    public function testLanguageCodeFilterRemovesLatnForLatinScript(): void
    {
        $this->assertEquals('az-AZ', LanguageCode::filter('az-Latn-AZ'));

        $this->assertEquals('bs-BA', LanguageCode::filter('bs-latn-ba'));

        $this->assertEquals('sr-RS', LanguageCode::filter('sr-Latn-RS'));
        $this->assertEquals('sr-Cyrl-RS', LanguageCode::filter('sr-Cyrl-RS'));
    }

    #[TestDox('LanguageCode::filter replaces three-letter ISO language codes')]
    public function testLanguageCodeFilterReplacesThreeLetterIsiLanguageCodes(): void
    {
        $this->assertEquals('nl-NL', LanguageCode::filter('dut'));
        $this->assertEquals('fr-FR', LanguageCode::filter('fre'));
        $this->assertEquals('de-DE', LanguageCode::filter('ger'));
    }


    #[TestDox('LanguageCode::filter maps World English to American English')]
    public function testLanguageCodeFilterMapsWorldEnglishToAmericanEnglish(): void
    {
        $this->assertEquals('en-US', LanguageCode::filter('en-001'));
        $this->assertEquals('en-US', LanguageCode::filter('en_001'));

        $language = new LanguageCode('en-001');
        $this->assertEquals('en-US', (string) $language);
    }

    #[TestDox('LanguageCode::filter maps European English to British English')]
    public function testLanguageCodeFilterMapsEuropeanEnglishToBritishEnglish(): void
    {
        $this->assertEquals('en-GB', LanguageCode::filter('en-150'));
        $this->assertEquals('en-GB', LanguageCode::filter('en_150'));

        $language = new LanguageCode('en-150');
        $this->assertEquals('en-GB', (string) $language);
    }


    #[TestDox('LanguageCode::validate exists')]
    public function testLanguageCodeValidateExists(): void
    {
        $class = new ReflectionClass(LanguageCode::class);
        $this->assertTrue($class->hasMethod('validate'));
    }

    #[TestDox('LanguageCode::validate is public static')]
    public function testLanguageCodeValidateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, 'validate');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('LanguageCode::validate has one REQUIRED parameter')]
    public function testLanguageCodeValidateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(LanguageCode::class, 'validate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('LanguageCode::validate accepts string')]
    public function testLanguageCodeValidateAcceptsString(): void
    {
        $code = 'en-US';
        $this->assertIsString($code);
        $this->assertTrue(LanguageCode::validate($code));

        $code = 'English';
        $this->assertIsString($code);
        $this->assertFalse(LanguageCode::validate($code));
    }

    #[TestDox('LanguageCode::validate accepts Stringable')]
    public function testLanguageCodeValidateAcceptsStringable(): void
    {
        $code = new Varchar('en-US');
        $this->assertInstanceOf(\Stringable::class, $code);
        $this->assertTrue(LanguageCode::validate($code));

        $code = new Varchar('English');
        $this->assertInstanceOf(\Stringable::class, $code);
        $this->assertFalse(LanguageCode::validate($code));
    }

    #[TestDox('LanguageCode::validate validates language codes')]
    public function testLanguageCodeValidateValidatesLanguageCodes(): void
    {
        $tests = [
            'en' => true,
            'EN' => false,

            'eng' => true,
            'ENG' => false,

            'EN-US' => false,
            'EN-us' => false,
            'en-US' => true,
            'en_US' => false,
            'en_US' => false,

            'es-005' => true,

            'zh-Hans' => true,
            'az-Latn' => true,

            'zh-Hant-HK' => true,
        ];

        foreach($tests as $code => $expected) {
            $this->assertEquals(
                $expected,
                LanguageCode::validate($code),
                'LanguageCode::validate("' . $code . '") MUST return ' . ($expected ? 'true' : 'false') . '.'
            );
        }
    }
}
