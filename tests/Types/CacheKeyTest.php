<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\Engine\UriFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Types\CacheKey::class)]
#[CoversClass(\StoreCore\Types\CacheKeyException::class)]
#[CoversClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class CacheKeyTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CacheKey class exists')]
    public function testCacheKeyClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR .  'CacheKey.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR .  'CacheKey.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\CacheKey'));
        $this->assertTrue(class_exists(CacheKey::class));
    }

    #[Group('hmvc')]
    #[TestDox('CacheKey class is concrete')]
    public function testCacheKeyClassIsConcrete(): void
    {
        $class = new ReflectionClass(CacheKey::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('CacheKey class is read-only')]
    public function testCacheKeyClassIsReadOnly(): void
    {
        $class = new ReflectionClass(CacheKey::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('hmvc')]
    #[TestDox('CacheKey is stringable')]
    public function testCacheKeyIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CacheKey());
    }

    #[Group('hmvc')]
    #[TestDox('CacheKey implements ValidateInterface')]
    public function testCacheKeyImplementsValidateInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, new CacheKey());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CacheKey::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CacheKey::VERSION);
        $this->assertIsString(CacheKey::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CacheKey::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CacheKey::__construct() exists')]
    public function testCacheKeyConstructorExists(): void
    {
        $class = new ReflectionClass(CacheKey::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('CacheKey::__construct() is public constructor')]
    public function testCacheKeyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(CacheKey::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('CacheKey::__construct() has one OPTIONAL parameter')]
    public function testCacheKeyConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(CacheKey::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CacheKey::__construct() throws TypeErrpr if parameter is not string or stringable')]
    public function testCacheKeyConstructorThrowsTypeErrorIfParameterIsNotStringOrStringable(): void
    {
        $this->expectException(\TypeError::class);
        $error = new CacheKey(42);
    }

    #[TestDox('CacheKey::__construct() accepts Psr\Http\Message\UriInterface')]
    public function testCacheKeyConstructorAcceptsPsrHttpMessageUriInterface(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUri('https://www.example.com/foo/bar');
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $uri);

        $key = new CacheKey($uri);
    }

    #[TestDox('CacheKey::__construct() throws CacheKeyException on empty string')]
    public function testCacheKeyConstructorThrowsCacheKeyExceptionOnEmptyString()
    {
        $this->expectException(CacheKeyException::class);
        $error = new CacheKey('');
    }

    #[TestDox('CacheKey::__construct() throws CacheKeyException on "http://"')]
    public function testCacheKeyConstructorThrowsCacheKeyExceptionOnHTTP(): void
    {
        $this->expectException(CacheKeyException::class);
        $error = new CacheKey('http://');
    }

    #[TestDox('CacheKey::__construct() throws CacheKeyException on "https://"')]
    public function testCacheKeyConstructorThrowsCacheKeyExceptionOnHTTPS(): void
    {
        $this->expectException(CacheKeyException::class);
        $error = new CacheKey('https://');
    }

    #[TestDox('CacheKey::__construct() accepts URL without host"')]
    public function testCacheKeyConstructAcceptsUrlWithoutHost(): void
    {
        $url = '/brands/adidas';
        $factory = new UriFactory();
        $uri = $factory->createUri($url);

        $key = new CacheKey($url);
        $this->assertEquals(sha1('/brands/adidas'), (string) $key);

        $key = new CacheKey($uri);
        $this->assertEquals(sha1('/brands/adidas'), (string) $key);
    }


    #[Depends('testCacheKeyIsStringable')]
    #[TestDox('Stringable CacheKey is a non-empty string')]
    public function testStringableCacheKeyIsNonEmptyString(): void
    {
        $cache_key = new CacheKey();
        $this->assertIsString((string) $cache_key);
        $this->assertNotEmpty((string) $cache_key);
    }

    #[Depends('testCacheKeyIsStringable')]
    #[Depends('testStringableCacheKeyIsNonEmptyString')]
    #[TestDox('Stringable CacheKey is 40 characters long string')]
    public function testStringableCacheKeyIs40CharactersLongString(): void
    {
        $cache_key = new CacheKey();
        $cache_key = (string) $cache_key;
        $this->assertEquals(40, strlen($cache_key));
        if (function_exists('mb_strlen')) {
            $this->assertEquals(40, mb_strlen($cache_key, 'UTF-8'));
        }
    }

    #[Depends('testCacheKeyIsStringable')]
    #[TestDox('CacheKey is case-insensitive')]
    public function testCacheKeyIsCaseInsensitive(): void
    {
        $foobar = new CacheKey('foo/bar');
        $FooBar = new CacheKey('Foo/Bar');
        $this->assertEquals((string) $FooBar, (string) $foobar);
    }

    #[Depends('testCacheKeyIsStringable')]
    #[TestDox('CacheKey ignores HTTP URI scheme')]
    public function testCacheKeyIgnoresHttpUriScheme(): void
    {
        $http  = new CacheKey('http://www.example.com/foo-bar/baz-qux');
        $https = new CacheKey('https://www.example.com/foo-bar/baz-qux');
        $this->assertEquals((string) $https, (string) $http);
    }

    #[Depends('testCacheKeyIsStringable')]
    #[TestDox('CacheKey ignores trailing slash in URLs')]
    public function testCacheKeyIgnoresTrailingSlashInUrls(): void
    {
        $with_slash    = new CacheKey('https://example.com/foo-bar/baz-qux/');
        $without_slash = new CacheKey('https://example.com/foo-bar/baz-qux');
        $this->assertEquals(
            (string) $without_slash, (string) $with_slash,
            'Cache key MUST ignore trailing slash in URLs.'
        );
    }


    #[TestDox('CacheKey::validate() exists')]
    public function testCacheKeyValidateExists(): void
    {
        $class = new ReflectionClass(CacheKey::class);
        $this->assertTrue($class->hasMethod('validate'));
    }

    #[Depends('testCacheKeyValidateExists')]
    #[TestDox('CacheKey::validate() is public static')]
    public function testCacheKeyValidateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(CacheKey::class, 'validate');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testCacheKeyValidateIsPublicStatic')]
    #[TestDox('CacheKey::validate() accepts string and returns bool')]
    public function testCacheKeyAcceptsStringAndReturnsBool(): void
    {
        $key = new CacheKey('https://example.com/foo-bar/baz-qux');
        $key = (string) $key;
        $this->assertIsBool(CacheKey::validate($key));
        $this->assertTrue(CacheKey::validate($key));

        $key = substr($key, 1);
        $this->assertFalse(CacheKey::validate($key));
    }

    #[Depends('testCacheKeyValidateIsPublicStatic')]
    #[TestDox('CacheKey::validate() accepts CacheKey and returns (bool) true')]
    public function testCacheKeyAcceptsCacheKeyAndReturnsBoolTrue(): void
    {
        $key = new CacheKey();
        $this->assertInstanceOf(CacheKey::class, $key);
        $this->assertIsBool(CacheKey::validate($key));
        $this->assertTrue(CacheKey::validate($key));
    }
}
