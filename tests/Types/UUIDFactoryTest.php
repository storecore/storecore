<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\Registry;
use StoreCore\Engine\UriFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(UUIDFactory::class)]
#[CoversClass(UUID::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
final class UUIDFactoryTest extends TestCase
{
    #[TestDox('UUIDFactory class is concrete')]
    public function testUUIDFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UUIDFactory::VERSION);
        $this->assertIsString(UUIDFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches master branch')]
    public function testVersionConstantMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UUIDFactory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('UUIDFactory::__construct exists')]
    public function testUUIDFactoryConstructorExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testUUIDFactoryConstructorExists')]
    #[TestDox('UUIDFactory::__construct is public constructor')]
    public function testUUIDFactoryConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testUUIDFactoryConstructorExists')]
    #[TestDox('UUIDFactory::__construct has one OPTIONAL parameter')]
    public function testUUIDFactoryConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUUIDFactoryConstructorHasOneOptionalParameter')]
    #[TestDox('UUIDFactory::__construct accepts StoreCore registry')]
    public function testUUIDFactoryConstructorAcceptsStoreCoreRegistry(): void
    {
        $factory = new UUIDFactory();
        $this->assertInstanceOf(UUIDFactory::class, $factory);
        $uuid = $factory->createUUID();
        $this->assertInstanceOf(UUID::class, $uuid);

        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $this->assertInstanceOf(UUIDFactory::class, $factory);
        $uuid = $factory->createUUID();
        $this->assertInstanceOf(UUID::class, $uuid);

        $this->expectException(\TypeError::class);
        $object = new \stdClass();
        $failure = new UUIDFactory($object);
    }


    #[TestDox('UUIDFactory::createFromString exists')]
    public function testUUIDFactoryCreateFromStringExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('createFromString'));
    }

    #[Depends('testUUIDFactoryCreateFromStringExists')]
    #[TestDox('UUIDFactory::createFromString is public static')]
    public function testUUIDFactoryCreateFromStringIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'createFromString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testUUIDFactoryCreateFromStringExists')]
    #[TestDox('UUIDFactory::createFromString has one REQUIRED parameter')]
    public function testUUIDFactoryCreateFromStringHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'createFromString');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUUIDFactoryCreateFromStringExists')]
    #[Depends('testUUIDFactoryCreateFromStringIsPublicStatic')]
    #[Depends('testUUIDFactoryCreateFromStringHasOneRequiredParameter')]
    #[TestDox('UUIDFactory::createFromString accepts string and returns UUID')]
    public function testUUIDFactoryCreateFromStringAcceptsStringAndReturnsUUID(): void
    {
        $string = '//www.example.com/foo-bar/baz-qux';
        $uuid = UUIDFactory::createFromString($string);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertEquals('6cbdcf4b-022b-45d2-b6d0-2125589852e4', (string) $uuid);
    }

    #[Depends('testUUIDFactoryCreateFromStringAcceptsStringAndReturnsUUID')]
    #[TestDox('UUIDFactory::createFromString is case-insensitive')]
    public function testUUIDFactoryCreateFromStringIsCaseInsensitive(): void
    {
        $string = '//www.example.com/Foo-Bar/Baz-Qux';
        $uuid = UUIDFactory::createFromString($string);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertEquals('6cbdcf4b-022b-45d2-b6d0-2125589852e4', (string) $uuid);
    }

    #[Depends('testUUIDFactoryCreateFromStringAcceptsStringAndReturnsUUID')]
    #[TestDox('UUIDFactory::createFromString accepts stringable and returns UUID')]
    public function testUUIDFactoryCreateFromStringAcceptsStringableAndReturnsUUID(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUri('//www.example.com/foo-bar/baz-qux');
        $this->assertinstanceOf(\Stringable::class, $uri);

        $uuid = UUIDFactory::createFromString($uri);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
    }


    #[TestDox('UUID::createUUID exists')]
    public function testUUIDCreateUUIDExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('createUUID'));
    }

    #[TestDox('UUID::createUUID is public')]
    public function testUUIDCreateUUIDIsPublic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'createUUID');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('UUID::createUUID has no parameters')]
    public function testUUIDCreateUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'createUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('UUID::createUUID returns stringable UUID value object')]
    public function testUUIDCreateUUIDReturnsStringableUUIDValueObject(): void
    {
        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();

        $this->assertIsObject($uuid);
        $this->assertInstanceOf(\Stringable::class, $uuid);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
    }

    #[TestDox('UUID::createUUID returns unique UUIDs')]
    public function testUUIDCreateUUIDReturnsUniqueUUIDs(): void
    {
        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $this->assertNotEquals((string) $factory->createUUID(), (string) $factory->createUUID());
    }


    #[TestDox('UUIDFactory::maxUUID exists')]
    public function testUUIDFactoryMaxUUIDExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('maxUUID'));
    }

    #[TestDox('UUIDFactory::maxUUID is public static')]
    public function testUUIDFactoryMaxUUIDIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'maxUUID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('UUIDFactory::maxUUID has no parameters')]
    public function testUUIDFactoryMaxUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'maxUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('UUIDFactory::maxUUID returns stringable “max” UUID value object')]
    public function testUUIDFactoryMaxUUIDReturnsStringableMaxUUIDValueObject(): void
    {
        $uuid = UUIDFactory::maxUUID();
        $this->assertIsObject($uuid);
        $this->assertInstanceOf(\Stringable::class, $uuid);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertEquals('ffffffff-ffff-ffff-ffff-ffffffffffff', (string) $uuid);
    }


    #[TestDox('UUIDFactory::nilUUID exists')]
    public function testUUIDFactoryNilUUIDExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('nilUUID'));
    }

    #[TestDox('UUIDFactory::nilUUID is public static')]
    public function testUUIDFactoryNilUUIDIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'nilUUID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('UUIDFactory::nilUUID has no parameters')]
    public function testUUIDFactoryNilUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'nilUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('UUIDFactory::nilUUID returns stringable “nil” UUID value object')]
    public function testUUIDFactoryNilUUIDReturnsStringableNilUUIDValueObject(): void
    {
        $uuid = UUIDFactory::nilUUID();
        $this->assertIsObject($uuid);
        $this->assertInstanceOf(\Stringable::class, $uuid);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertEquals('00000000-0000-0000-0000-000000000000', (string) $uuid);
    }

    #[TestDox('UUIDFactory::pseudoRandomUUID exists')]
    public function testUUIDFactoryPseudoRandomUUIDExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('pseudoRandomUUID'));
    }

    #[TestDox('UUIDFactory::pseudoRandomUUID is public static')]
    public function testUUIDFactoryPseudoRandomUUIDIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'pseudoRandomUUID');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('UUIDFactory::pseudoRandomUUID has no parameters')]
    public function testUUIDFactoryPseudoRandomUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'pseudoRandomUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('UUIDFactory::pseudoRandomUUID returns stringable UUID value object')]
    public function testUUIDFactoryPseudoRandomUUIDReturnsStringableUUIDValueObject(): void
    {
        $uuid = UUIDFactory::pseudoRandomUUID();
        $this->assertIsObject($uuid);
        $this->assertInstanceOf(\Stringable::class, $uuid);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
    }

    #[TestDox('UUIDFactory::pseudoRandomUUID returns unique UUIDs')]
    public function testUUIDFactoryPseudoRandomUUIDReturnsUniqueUUIDs(): void
    {
        $this->assertNotEquals((string) UUIDFactory::pseudoRandomUUID(), (string) UUIDFactory::pseudoRandomUUID());
    }


    #[TestDox('UUIDFactory::randomUUID exists')]
    public function testUUIDFactoryRandomUUIDExists(): void
    {
        $class = new ReflectionClass(UUIDFactory::class);
        $this->assertTrue($class->hasMethod('randomUUID'));
    }

    #[TestDox('UUIDFactory::randomUUID is public static')]
    public function testUUIDFactoryRandomUUIDIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'randomUUID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('UUIDFactory::randomUUID has no parameters')]
    public function testUUIDFactoryRandomUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(UUIDFactory::class, 'randomUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('UUIDFactory::randomUUID returns stringable UUID value object')]
    public function testUUIDFactoryRandomUUIDReturnsStringableUUIDValueObject(): void
    {
        $uuid = UUIDFactory::randomUUID();
        $this->assertIsObject($uuid);
        $this->assertInstanceOf(\Stringable::class, $uuid);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
    }

    #[TestDox('UUIDFactory::randomUUID returns unique UUIDs')]
    public function testUUIDFactoryRandomUUIDReturnsUniqueUUIDs(): void
    {
        $this->assertNotEquals((string) UUIDFactory::randomUUID(), (string) UUIDFactory::randomUUID());
    }
}
