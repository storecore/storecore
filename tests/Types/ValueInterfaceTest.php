<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ValueInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ValueInterface interface file exists')]
    public function testValueInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ValueInterface.php'
        );
    }

    #[Depends('testValueInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('ValueInterface interface file is readable')]
    public function testValueInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ValueInterface.php'
        );
    }

    #[Group('hmvc')]
    #[TestDox('ValueInterface is an interface')]
    public function testValueInterfaceIsInterface(): void
    {
        $interface = new ReflectionClass(ValueInterface::class);
        $this->assertTrue($interface->isInterface());
    }
}
