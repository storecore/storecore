<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\PhoneNumber::class)]
final class PhoneNumberTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PhoneNumber class exists')]
    public function testPhoneNumberClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PhoneNumber.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PhoneNumber.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\PhoneNumber'));
        $this->assertTrue(class_exists(PhoneNumber::class));
    }

    #[Group('hmvc')]
    #[TestDox('PhoneNumber class is concrete')]
    public function testPhoneNumberClassIsConcrete(): void
    {
        $class = new ReflectionClass(PhoneNumber::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('PhoneNumber implements ValidateInterface')]
    public function testPhoneNumberImplementsValidateInterface(): void
    {
        $phone_number = new PhoneNumber('(425) 123-4567');
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, $phone_number);
        $this->assertInstanceOf(ValidateInterface::class, $phone_number);
    }

    #[TestDox('PhoneNumber is stringable')]
    public function testPhoneNumberIsStringable(): void
    {
        $phone_number = new PhoneNumber('(425) 123-4567');
        $this->assertInstanceOf(\Stringable::class, $phone_number);
    }

    #[TestDox('PhoneNumber is JSON serializable')]
    public function testPhoneNumberIsIsJsonSerializable(): void
    {
        $phone_number = new PhoneNumber('(425) 123-4567');
        $this->assertInstanceOf(\JsonSerializable::class, $phone_number);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PhoneNumber::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PhoneNumber::VERSION);
        $this->assertIsString(PhoneNumber::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PhoneNumber::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('PhoneNumber::__construct() exists')]
    public function testPhoneNumberConstructorExists(): void
    {
        $class = new \ReflectionClass(PhoneNumber::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('PhoneNumber::__construct() is public constructor')]
    public function testPhoneNumberConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(PhoneNumber::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('PhoneNumber::__construct() has one REQUIRED parameter')]
    public function testPhoneNumberConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(PhoneNumber::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
