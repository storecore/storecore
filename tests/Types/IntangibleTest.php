<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\Intangible::class)]
final class IntangibleTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Intangible class exists')]
    public function testIntangibleClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Intangible.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Intangible.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\Intangible'));
        $this->assertTrue(class_exists(Intangible::class));
    }

    #[Group('hmvc')]
    #[TestDox('Intangible class is concrete')]
    public function testIntangibleClassIsConcrete(): void
    {
        $class = new ReflectionClass(Intangible::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Intangible is a Thing')]
    public function testIntangibleIsThing(): void
    {
        $intangible = new Intangible();
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, $intangible);
    }

    #[Group('hmvc')]
    #[TestDox('Intangible is JsonSerializable')]
    public function testIntangibleImplementsJsonSerializableInterface(): void
    {
        $object = new Intangible();
        $this->assertInstanceOf(\JsonSerializable::class, $object);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Intangible::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Intangible::VERSION);
        $this->assertIsString(Intangible::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Intangible::VERSION, '1.0.0', '>=')
        );
    }
}
