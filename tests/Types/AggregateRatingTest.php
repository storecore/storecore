<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Organization;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Types\AggregateRating::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class AggregateRatingTest extends TestCase
{
    #[TestDox('AggregateRating class is concrete')]
    public function testAggregateRatingClassIsConcrete(): void
    {
        $class = new ReflectionClass(AggregateRating::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('AggregateRating is Intangible')]
    public function testAggregateRatingIsIntangible(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Types\\Intangible'));
        $this->assertTrue(class_exists(Intangible::class));

        $rating = new AggregateRating();
        $this->assertInstanceOf(Intangible::class, $rating);
    }

    #[Group('hmvc')]
    #[TestDox('AggregateRating is JSON serializable')]
    public function testAggregateRatingIsJsonSerializable(): void
    {
        $rating = new AggregateRating();
        $this->assertInstanceOf(\JsonSerializable::class, $rating);
    }

    
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AggregateRating::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AggregateRating::VERSION);
        $this->assertIsString(AggregateRating::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AggregateRating::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AggregateRating.itemReviewed exists')]
    public function testAggregateRatingItemReviewedExists(): void
    {
        $rating = new AggregateRating();
        $this->assertObjectHasProperty('itemReviewed', $rating);
    }

    #[TestDox('AggregateRating.itemReviewed is null by default')]
    public function testAggregateRatingItemReviewedIsNullByDefault(): void
    {
        $rating = new AggregateRating();
        $this->assertNull($rating->itemReviewed);
    }

    #[Group('hmvc')]
    #[TestDox('AggregateRating.itemReviewed accepts JSON serializable object')]
    public function testAggregateRatingItemReviewedAcceptsJsonSerializableObject(): void
    {
        $rating = new AggregateRating();
        $rating->itemReviewed = new Organization('GreatFood');

        $this->assertNotNull($rating->itemReviewed);
        $this->assertIsObject($rating->itemReviewed);
        $this->assertInstanceOf(\JsonSerializable::class, $rating->itemReviewed);

        $this->expectException(\TypeError::class);
        $notJsonSerializable = new \stdClass();
        $rating->itemReviewed = $notJsonSerializable;
    }


    #[TestDox('AggregateRating::__construct() exists')]
    public function testAggregateRatingConstructorExists(): void
    {
        $class = new ReflectionClass(AggregateRating::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('AggregateRating::__construct() is public constructor')]
    public function testOrderItemConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(AggregateRating::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('AggregateRating::__construct() has three OPTIONAL parameters')]
    public function testAggregateRatingConstructorHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(AggregateRating::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product#product-review-page-example
     */
    #[TestDox('AggregateRating::__construct() sets ratingValue and reviewCount')]
    public function testAggregateRatingConstructorSetsRatingValueAndReviewCount(): void
    {
        /*
         * ```json
         * "aggregateRating": {
         *   "@type": "AggregateRating",
         *   "ratingValue": 4.4,
         *   "reviewCount": 89
         * }
         * ```
         */
        $aggregateRating = new AggregateRating(4.4, null, 89);
        $this->assertEquals(4.4, $aggregateRating->ratingValue);
        $this->assertNull($aggregateRating->ratingCount);
        $this->assertEquals(89, $aggregateRating->reviewCount);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "AggregateRating",
              "bestRating": 5,
              "ratingValue": 4.4,
              "reviewCount": 89,
              "worstRating": 1
            }
        ';
        $actualJson = json_encode($aggregateRating, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
