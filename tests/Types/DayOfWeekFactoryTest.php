<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\DayOfWeekFactory::class)]
final class DayOfWeekFactoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DayOfWeekFactory class is concrete')]
    public function testDayOfWeekFactoryIsConcrete(): void
    {
        $class = new ReflectionClass(DayOfWeekFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DayOfWeekFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DayOfWeekFactory::VERSION);
        $this->assertIsString(DayOfWeekFactory::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DayOfWeekFactory::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('DayOfWeekFactory::createFromString() is exists')]
    public function testDayOfWeekFactoryCreateFromStringExists(): void
    {
        $class = new ReflectionClass(DayOfWeekFactory::class);
        $this->assertTrue($class->hasMethod('createFromString'));
    }

    #[TestDox('DayOfWeekFactory::createFromString() is public static')]
    public function testDayOfWeekFactoryCreateFromStringIsPublicStatic(): void
    {
        $method = new ReflectionMethod(DayOfWeekFactory::class, 'createFromString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('DayOfWeekFactory::createFromString() has one REQUIRED parameter')]
    public function testDayOfWeekFactoryCreateFromStringHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(DayOfWeekFactory::class, 'createFromString');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Group('hmvc')]
    #[TestDox('DayOfWeekFactory::createFromString() accepts string "today"')]
    public function testDayOfWeekFactoryCreateFromStringAcceptsStringToday(): void
    {
        $this->assertInstanceOf(DayOfWeek::class, DayOfWeekFactory::createFromString('today'));
    }

    #[Group('hmvc')]
    #[TestDox('DayOfWeekFactory::createFromString() accepts string "tomorrow"')]
    public function testDayOfWeekFactoryCreateFromStringAcceptsStringTomorrow(): void
    {
        $this->assertInstanceOf(DayOfWeek::class, DayOfWeekFactory::createFromString('tomorrow'));
    }
}
