<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\PaymentMethod::class)]
class PaymentMethodTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PaymentMethod class exists')]
    public function testPaymentMethodClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PaymentMethod.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PaymentMethod.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\PaymentMethod'));
        $this->assertTrue(class_exists(PaymentMethod::class));
    }

    #[TestDox('PaymentMethod class is concrete')]
    public function testPaymentMethodClassIsConcrete(): void
    {
        $class = new ReflectionClass(PaymentMethod::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PaymentMethod is Intangible')]
    public function testPaymentMethodIsIntangible(): void
    {

        $payment_method = new PaymentMethod();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $payment_method);
        $this->assertInstanceOf(Intangible::class, $payment_method);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PaymentMethod::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PaymentMethod::VERSION);
        $this->assertIsString(PaymentMethod::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PaymentMethod::VERSION, '0.2.0', '>=')
        );
    }
}
