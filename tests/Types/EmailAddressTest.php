<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\RequiresPhp;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\EmailAddress::class)]
#[CoversClass(\StoreCore\Database\Metadata::class)]
#[RequiresPhp('>= 8.0.0')]
final class EmailAddressTest extends TestCase
{
    #[TestDox('EmailAddress class is concrete')]
    public function testEmailAddressClassIsConcrete(): void
    {
        $class = new ReflectionClass(EmailAddress::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('EmailAddress implements TypeInterface')]
    public function testEmailAddressImplementsTypeInterface(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertInstanceOf(\StoreCore\Types\TypeInterface::class, $email_address);
        $this->assertInstanceOf(TypeInterface::class, $email_address);
    }

    #[TestDox('EmailAddress implements ValidateInterface')]
    public function testEmailAddressImplementsValidateInterface(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, $email_address);
        $this->assertInstanceOf(ValidateInterface::class, $email_address);
    }


    #[Group('hmvc')]
    #[TestDox('EmailAddress is stringable')]
    public function testEmailAddressImplementsIsStringable(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertInstanceOf(\Stringable::class, $email_address);
    }

    #[Group('hmvc')]
    #[TestDox('EmailAddress is JSON serializable')]
    public function testEmailAddressImplementsIsJsonSerializable(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertInstanceOf(\JsonSerializable::class, $email_address);
    }

    #[Depends('testEmailAddressImplementsIsStringable')]
    #[Depends('testEmailAddressImplementsIsJsonSerializable')]
    #[Group('hmvc')]
    #[TestDox('JSON encoded email address is JSON string')]
    public function testJsonEncodedEmailAddressIsJsonString(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertEquals('john.doe@example.com', (string) $email_address);
        $this->assertEquals('"john.doe@example.com"', json_encode($email_address));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EmailAddress::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EmailAddress::VERSION);
        $this->assertIsString(EmailAddress::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EmailAddress::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('EmailAddress::__construct exists')]
    public function testEmailAddressConstructorExists(): void
    {
        $class = new ReflectionClass(EmailAddress::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('EmailAddress::__construct is public constructor')]
    public function testEmailAddressConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('EmailAddress::__construct has two parameters')]
    public function testEmailAddressConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testEmailAddressConstructorHasTwoParameters')]
    #[TestDox('EmailAddress::__construct has one REQUIRED parameter')]
    public function testEmailAddressConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    /**
     * @see https://schema.org/email 
     *   This Schema.org JSON-LD example contains a `mailto` prefix,
     *   so there are use cases when we MUST accept this prefix:
     * 
     *   ```html
     *   <script type="application/ld+json">
     *   {
     *   "@context": "https://schema.org",
     *     "@type": "Person",
     *     "address": {
     *       "@type": "PostalAddress",
     *       "addressLocality": "Seattle",
     *       "addressRegion": "WA",
     *       "postalCode": "98052",
     *       "streetAddress": "20341 Whitworth Institute 405 N. Whitworth"
     *     },
     *     "colleague": [
     *       "http://www.xyz.edu/students/alicejones.html",
     *       "http://www.xyz.edu/students/bobsmith.html"
     *     ],
     *     "email": "mailto:jane-doe@xyz.edu",
     *     "image": "janedoe.jpg",
     *     "jobTitle": "Professor",
     *     "name": "Jane Doe",
     *     "telephone": "(425) 123-4567",
     *     "url": "http://www.janedoe.com"
     *   }
     *   </script>
     *   ```
     */
    #[TestDox('EmailAddress::__construct accepts and removes mailto: prefix')]
    public function testEmailAddressConstructorAcceptsAndRemovesMailtoPrefix(): void
    {
        $email = new EmailAddress('mailto:jane-doe@xyz.edu', false);
        $this->assertStringNotContainsString('mailto:', (string) $email);

        $this->expectException(\ValueError::class);
        $email = new EmailAddress('mailto:jane-doe@xyz.edu');
    }


    #[TestDox('EmailAddress::__toString exists')]
    public function testEmailAddressToStringExists(): void
    {
        $class = new ReflectionClass(EmailAddress::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('EmailAddress::__toString is public')]
    public function testEmailAddressToStringIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('EmailAddress::__toString has no parameters')]
    public function testEmailAddressToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('EmailAddress::__toString returns non-empty string')]
    public function testEmailAddressToStringReturnsNonEmptyString(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $email_address = (string) $email_address;
        $this->assertNotEmpty($email_address);
        $this->assertIsString($email_address);
    }

    #[Depends('testEmailAddressToStringReturnsNonEmptyString')]
    #[TestDox('EmailAddress::__toString returns set email address')]
    public function testEmailAddressToStringReturnsSetEmailAddress(): void
    {
        $email_address = new EmailAddress('john.doe@example.com');
        $this->assertSame('john.doe@example.com', (string) $email_address);
    }

    #[Depends('testEmailAddressToStringReturnsSetEmailAddress')]
    #[TestDox('EmailAddress::__toString returns lowercase domain name')]
    public function testEmailAddressToStringReturnsLowercaseDomainName(): void
    {
        $email_address = new EmailAddress('Jane.Doe@Example.Net');
        $this->assertEquals('Jane.Doe@example.net', (string) $email_address);
    }


    #[TestDox('EmailAddress.displayName exists')]
    public function testEmailAddressDisplayNameExists(): void
    {
        $email_address = new EmailAddress('jane-doe@xyz.edu');
        $this->assertObjectHasProperty('displayName', $email_address);
    }

    #[TestDox('EmailAddress.displayName is null by default')]
    public function testEmailAddressDisplayNameIsNullByDefault(): void
    {
        $email_address = new EmailAddress('jane-doe@xyz.edu');
        $this->assertNull($email_address->displayName);
    }

    #[TestDox('EmailAddress.displayName can be set to string')]
    public function testEmailAddressDisplayNameCanBeSetToString(): void
    {
        $email_address = new EmailAddress('jane-doe@xyz.edu');
        $email_address->displayName = 'Jane Doe';

        $this->assertNotNull($email_address->displayName);
        $this->assertNotEmpty($email_address->displayName);
        $this->assertIsString($email_address->displayName);
        $this->assertEquals('Jane Doe', $email_address->displayName);
    }


    #[TestDox('EmailAddress.metadata exists')]
    public function testEmailAddressMetadataExists(): void
    {
        $email_address = new EmailAddress('info@example.com');
        $this->assertObjectHasProperty('metadata', $email_address);
    }

    #[TestDox('EmailAddress.metadata is public StoreCore\Database\Metadata')]
    public function testEmailAddressMetadataIsPublicStoreCoreDatabaseMetadata(): void
    {
        $property = new ReflectionProperty(EmailAddress::class, 'metadata');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());

        $email_address = new EmailAddress('info@example.com');
        $this->assertIsObject($email_address->metadata);
        $this->assertInstanceOf(\StoreCore\Database\Metadata::class, $email_address->metadata);
    }

    #[Depends('testEmailAddressMetadataIsPublicStoreCoreDatabaseMetadata')]
    #[TestDox('EmailAddress.metadata.hash is non-empty hexadecimal string')]
    public function testEmailAddressMetadataHashIsNonEmptyHexadecimalString(): void
    {
        $email_address = new EmailAddress('info@example.com');
        $this->assertNotEmpty($email_address->metadata->hash);
        $this->assertIsString($email_address->metadata->hash);
        $this->assertTrue(ctype_xdigit($email_address->metadata->hash));
    }


    #[TestDox('EmailAddress::validate exists')]
    public function testEmailAddressValidateExists(): void
    {
        $class = new ReflectionClass(EmailAddress::class);
        $this->assertTrue($class->hasMethod('validate'));
    }

    #[TestDox('EmailAddress::validate is public static')]
    public function testEmailAddressValidateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(EmailAddress::class, 'validate');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    /**
     * @see https://testsigma.com/blog/test-cases-for-email/
     *      “How to Write Test Cases For Email Field and Email Validation” by Ritika Kumari
     */
    #[TestDox('EmailAddress::validate returns (bool) true on valid email address')]
    public function testEmailAddressValidateReturnsBoolTrueOnValidEmailAddress(): void
    {
        $cases = [
            'user@example.com',
            'user123@email.co.uk',
            'john.doe@company.org',
            'user_name1234@email-provider.net',
            'info@sub.domain.com',
            'name@my-email-provider.xyz',
            'user123@[192.168.1.1]',
            'john.doe@email.travel',
            '_______@domain.com',
        ];

        foreach ($cases as $case) {
            $this->assertTrue(
                EmailAddress::validate($case),
                'Email address `' . $case .  '` is valid.'
            );
        }
    }

    #[Depends('testEmailAddressValidateReturnsBoolTrueOnValidEmailAddress')]
    #[TestDox('EmailAddress::validate returns (bool) false on invalid email address')]
    public function testEmailAddressValidateReturnsBoolFalseOnInvalidEmailAddress(): void
    {
        $cases = [
            'user@invalid-tld.123',
            'user#domain.com',
        //  'user@domain.con',
        //  'user&name@email-provider.net',
            'spaced user@domain.info',
            'double..dots@email.org',
            '@.com',
            'user@domain with space.com',
            'user@domain..com',
        ];

        foreach ($cases as $case) {
            $this->assertFalse(
                EmailAddress::validate($case),
                'Email address `' . $case .  '` is invalid.'
            );
        }
    }

    /**
     * @see https://bugzilla.mozilla.org/show_bug.cgi?id=465248
     */
    #[Depends('testEmailAddressValidateReturnsBoolFalseOnInvalidEmailAddress')]
    #[TestDox('Ampersand (&) is allowed in local address')]
    public function testAmpersandIsAllowedInLocalAddress(): void
    {
        $this->assertNotFalse(
            EmailAddress::validate('user&name@email-provider.net'),
            'Ampersand (&) SHOULD be allowed in local address.'
        );
    }


    #[TestDox('Gmail address acceptance test')]
    public function testGmailAddressAcceptanceTest(): void
    {
        $this->assertTrue(EmailAddress::validate('John.Smith@gmail.com'));

        // googlemail.com is an alias of gmail.com …
        $googlemail = new EmailAddress('John.Smith@googlemail.com');
        $gmail      = new EmailAddress('John.Smith@gmail.com');

        // … so the hashes MUST be identical for the same local address …
        $this->assertEquals($gmail->metadata->hash, $googlemail->metadata->hash);
        // … but the string representations SHOULD NOT be changed:
        $this->assertNotEquals($gmail->__toString(), $googlemail->__toString());

        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('john.smith@gmail.com'))->metadata->hash,
            'Gmail is case-insensitive.'
        );

        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('John.SMITH@gmail.com'))->metadata->hash,
            'Gmail is case-insensitive.'
        );

        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('John.Smith+Spam@gmail.com'))->metadata->hash,
            'Email tags MUST be ignored.'
        );

        // @see https://support.google.com/mail/answer/10313
        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('johnsmith@gmail.com'))->metadata->hash,
            'Dots MUST be ignored in local Gmail address.'
        );

        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('jo.hn.sm.ith@gmail.com'))->metadata->hash,
            'Dots MUST be ignored in local Gmail address.'
        );

        $this->assertEquals(
            $gmail->metadata->hash,
            (new EmailAddress('j.o.h.n.s.m.i.t.h@gmail.com'))->metadata->hash,
            'Dots MUST be ignored in local Gmail address.'
        );
    }
}
