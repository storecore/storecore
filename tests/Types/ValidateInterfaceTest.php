<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ValidateInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ValidateInterface interface file exists')]
    public function testValidateInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ValidateInterface.php'
        );
    }

    #[Depends('testValidateInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('ValidateInterface interface file is readable')]
    public function testValidateInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ValidateInterface.php'
        );
    }

    #[Group('hmvc')]
    #[TestDox('ValidateInterface is an interface')]
    public function testValidateInterfaceIsInterface(): void
    {
        $interface = new ReflectionClass(ValidateInterface::class);
        $this->assertTrue($interface->isInterface());
    }
}
