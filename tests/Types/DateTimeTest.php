<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\DateTime::class)]
final class DateTimeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DateTime class exists')]
    public function testDateTimeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'DateTime.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'DateTime.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\DateTime'));
        $this->assertTrue(class_exists(DateTime::class));
    }

    #[Group('hmvc')]
    #[TestDox('DateTime class is concrete')]
    public function testDateTimeClassIsConcrete(): void
    {
        $class = new ReflectionClass(DateTime::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('DateTime is a PHP DateTime')]
    public function testDateTimeIsPHPDateTime(): void
    {
        $stub = $this->createStub(DateTime::class);
        $this->assertInstanceOf(\DateTime::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('DateTime implements PHP DateTimeInterface')]
    public function testDateTimeImplementsPHPDateTimeInterface(): void
    {
        $stub = $this->createStub(DateTime::class);
        $this->assertInstanceOf(\DateTimeInterface::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('DateTime is stringable')]
    public function testDateTimeIsStringable(): void
    {
        $stub = $this->createStub(DateTime::class);
        $this->assertInstanceOf(\Stringable::class, $stub);
    }

    #[Depends('testDateTimeIsStringable')]
    #[Group('hmvc')]
    #[TestDox('DateTime is JSON serializable')]
    public function testDateTimeIsJsonSerializable(): void
    {
        $stub = $this->createStub(DateTime::class);
        $this->assertInstanceOf(\JsonSerializable::class, $stub);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DateTime::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DateTime::VERSION);
        $this->assertIsString(DateTime::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DateTime::VERSION, '1.0.0', '>=')
        );
    }
}
