<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CMS\{Audiobook, Book};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\Thing::class)]
#[CoversClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\CMS\AbstractThing::class)]
#[UsesClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[Group('hmvc')]
final class ThingTest extends TestCase
{
    #[TestDox('Thing class is concrete')]
    public function testThingClassIsConcrete(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Thing is JSON serializable')]
    public function testThingIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Thing());
    }

    #[TestDox('Thing is stringable')]
    public function testThingIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Thing());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Thing::VERSION);
        $this->assertIsString(Thing::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Thing::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('VERSION matches master branch')]
    public function testThingToStringExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('Thing::__toString() returns non-empty string')]
    public function testThingToStringReturnsNonEmptyString(): void
    {
        $thing = new Thing();
        $thing = (string) $thing;
        $this->assertNotEmpty($thing);
        $this->assertIsString($thing);
    }

    #[Depends('testThingToStringReturnsNonEmptyString')]
    #[TestDox('Thing::__toString() returns JSON')]
    public function testThingToStringReturnsJson(): void
    {
        $thing = new Thing();
        $thing->name = 'Foo Bar';
        $string = (string) $thing;

        // `null` is returned if the JSON cannot be decoded or
        // if the encoded data is deeper than the nesting limit.
        $json = json_decode($string, true);
        $this->assertNotNull($json);

        $this->assertNotEmpty($json);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('name', $json);
    }

    #[Depends('testThingToStringReturnsJson')]
    #[TestDox('Thing::__toString() returns Schema.org @context')]
    public function testThingToStringReturnsSchemaOrgContext(): void
    {
        $thing = new Thing();
        $thing->name = 'Foo Bar';
        $string = (string) $thing;
        $this->assertStringContainsString('"@context":"https://schema.org"', $string);
    }


    #[TestDox('Thing.additionalType exists')]
    public function testThingAdditionalTypeExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasProperty('additionalType'));
    }

    #[TestDox('Thing.additionalType is public')]
    public function testThingAdditionalTypeIsPublic(): void
    {
        $property = new ReflectionProperty(Thing::class, 'additionalType');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Thing.additionalType is null by default')]
    public function testThingAdditionalTypeIsNullByDefault(): void
    {
        $thing = new Thing();
        $this->assertNull($thing->additionalType);
    }

    #[TestDox('Thing.additionalType accepts Stringable URL')]
    public function testThingAdditionalTypeAcceptsStringableURL(): void
    {
        // Something that is also a `Book`:
        $thing = new Thing();
        $thing->additionalType = new URL('https://schema.org/Book');

        $this->assertNotNull($thing->additionalType);
        $this->assertInstanceOf(\Stringable::class, $thing->additionalType);
        $this->assertInstanceOf(URL::class, $thing->additionalType);
    }


    #[TestDox('Thing.name exists')]
    public function testThingNameExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasProperty('name'));
    }

    #[TestDox('Thing.name is public')]
    public function testThingNameIsPublic(): void
    {
        $property = new ReflectionProperty(Thing::class, 'name');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Thing.name is null by default')]
    public function testThingNameIsNullByDefault(): void
    {
        $thing = new Thing();
        $this->assertNull($thing->name);
    }


    #[TestDox('Thing.subjectOf exists')]
    public function testThingSubjectOfExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasProperty('subjectOf'));
    }

    #[TestDox('Thing.subjectOf is public')]
    public function testThingSubjectOfIsPublic(): void
    {
        $property = new ReflectionProperty(Thing::class, 'subjectOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Thing.subjectOf is null by default')]
    public function testThingSubjectOfIsNullByDefault(): void
    {
        $thing = new Thing();
        $this->assertNull($thing->subjectOf);
    }



    #[TestDox('Thing::__construct() exists')]
    public function testThingConstructorExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Thing::__construct() is public constructor')]
    public function testThingConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Thing::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Thing::__construct() has one OPTIONAL parameter')]
    public function testThingConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Thing::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    /**
     * In Schema.org JSON-LD, many objects are simply set by their name.
     * To facilitate this behavior, the `Thing` constructor accepts a string
     * for the object’s name.
     * 
     * ```json
     * {
     *   "@context": "https://schema.org/",
     *   "type": "Audiobook",
     *   "@id": "https://archive.org/details/alices_adventures_1005_librivox#work",
     *   "abridged": "false",
     *   "author": "Lewis Carroll",
     *   "description": "Alice's Adventures in Wonderland (commonly shortened to Alice in Wonderland) is an 1865 novel written by English author Charles Lutwidge Dodgson under the pseudonym Lewis Carroll. It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale is filled with allusions to Dodgson's friends. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the \"literary nonsense\" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre. (Introduction by Wikipedia)",
     *   "datePublished": "2010-05-03",
     *   "duration": "PT2H31M21S",
     *   "encodingFormat": "video/mp4",
     *   "name": "Alice's Adventures in Wonderland",
     *   "playerType": "M4B compatible",
     *   "provider": "The Internet Archive",
     *   "publisher": "LibriVox",
     *   "readBy": "Eric Leach",
     *   "contentUrl": "https://archive.org/download/alices_adventures_1005_librivox/AlicesAdventuresInWonderlandV5_librivox.m4b",
     *   "mainEntityOfPage": "https://archive.org/details/alices_adventures_1005_librivox"
     * }
     * ```
     * 
     * In this example of an `Audiobook`, the `author` is a `Person`, the
     * `provider` is an `Organization`, the `publisher` is an `NGO` organization,
     * and the `readBy` performer is another `Person`.  A more accurate representation
     * of the same object would be:
     *
     * ```json
     * {
     *   "@context": "https://schema.org/",
     *   "@type": "Audiobook",
     *   "@id": "https://archive.org/details/alices_adventures_1005_librivox#work",
     *   "abridged": "false",
     *   "author": {
     *     "@type": "Person",
     *     "name": "Lewis Carroll"
     *   },
     *   "description": "Alice's Adventures in Wonderland (commonly shortened to Alice in Wonderland) is an 1865 novel written by English author Charles Lutwidge Dodgson under the pseudonym Lewis Carroll. It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale is filled with allusions to Dodgson's friends. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the \"literary nonsense\" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre. (Introduction by Wikipedia)",
     *   "datePublished": "2010-05-03",
     *   "duration": "PT2H31M21S",
     *   "encodingFormat": "video/mp4",
     *   "name": "Alice's Adventures in Wonderland",
     *   "playerType": "M4B compatible",
     *   "provider": {
     *     "@type": "NGO",
     *     "name": "The Internet Archive"
     *   },
     *   "publisher": {
     *     "@type": "Organization",
     *     "name": "LibriVox"
     *   },
     *   "readBy": {
     *     "@type": "Person",
     *     "name": "Eric Leach"
     *   },
     *   "contentUrl": "https://archive.org/download/alices_adventures_1005_librivox/AlicesAdventuresInWonderlandV5_librivox.m4b",
     *   "mainEntityOfPage": "https://archive.org/details/alices_adventures_1005_librivox"
     * }
     * ```
     */
    #[TestDox('Thing::__construct() string parameter sets Thing.name')]
    public function testThingConstructorStringParameterSetsThingName(): void
    {
        $thing = new Thing();
        $this->assertNull($thing->name);

        $thing = new Thing('Numerius Negidius');
        $this->assertNotEmpty($thing->name);
        $this->assertEquals('Numerius Negidius', $thing->name);
    }


    #[TestDox('Thing::__set() exists')]
    public function testThingSetExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasMethod('__set'));
    }


    #[TestDox('Thing::toArray() exists')]
    public function testThingToArrayExists(): void
    {
        $class = new ReflectionClass(Thing::class);
        $this->assertTrue($class->hasMethod('toArray'));
    }

    #[TestDox('Thing::toArray() is public')]
    public function testThingToArrayIsPublic(): void
    {
        $method = new ReflectionMethod(Thing::class, 'toArray');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Thing::toArray() has no parameters')]
    public function testThingToArrayHasNoParameters(): void
    {
        $method = new ReflectionMethod(Thing::class, 'toArray');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testThingToArrayExists')]
    #[Depends('testThingToArrayHasNoParameters')]
    #[Depends('testThingToArrayHasNoParameters')]
    #[TestDox('Thing::toArray() returns array')]
    public function testThingToArrayReturnsArray(): void
    {
        $thing = new Thing();
        $this->assertEmpty($thing->toArray());
        $this->assertIsArray($thing->toArray());

        $thing->name = 'Schema.org Ontology';
        $this->assertNotEmpty($thing->toArray());
        $this->assertIsArray($thing->toArray());
        $this->assertArrayHasKey('name', $thing->toArray());
    }


    /**
     * @see https://schema.org/Thing#eg-0246
     *      Schema.org type `Thing` example 1
     */
    #[TestDox('Schema.org example')]
    public function testSchemaOrgExample(): void
    {
        /*
            ```html
            <script type="application/ld+json">
            {
              "@context": "https://schema.org/",
              "@type": "Thing",
              "name": "Schema.org Ontology",
              "subjectOf": {
                "@type": "Book",
                "name": "The Complete History of Schema.org"
              }
            }
            </script>
            ```
         */

        $thing = new Thing('Schema.org Ontology');
        $thing->subjectOf = new Book('The Complete History of Schema.org');

        $json = json_encode($thing, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"@type": "Thing"', $json);
        $this->assertStringContainsString('"name": "Schema.org Ontology"', $json);
        $this->assertStringContainsString('"subjectOf": {', $json);
        $this->assertStringContainsString('"@type": "Book"', $json);
        $this->assertStringContainsString('"name": "The Complete History of Schema.org"', $json);

        $data = json_decode($json, true);
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertSame('Schema.org Ontology', $data['name']);

        $this->assertArrayHasKey('subjectOf', $data);
        $this->assertIsArray($data['subjectOf']);
        $this->assertArrayHasKey('name', $data['subjectOf']);
        $this->assertSame('The Complete History of Schema.org', $data['subjectOf']['name']);
    }
}
