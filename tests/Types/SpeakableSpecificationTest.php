<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\SpeakableSpecification::class)]
final class SpeakableSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SpeakableSpecification class exists')]
    public function testSpeakableSpecificationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'SpeakableSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'SpeakableSpecification.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\SpeakableSpecification'));
        $this->assertTrue(class_exists(SpeakableSpecification::class));
    }

    #[TestDox('SpeakableSpecification class is concrete')]
    public function testSpeakableSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(SpeakableSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('SpeakableSpecification is Intangible')]
    public function testSpeakableSpecificationIsIntangible(): void
    {
        $this->assertInstanceOf(Intangible::class, new SpeakableSpecification());
    }

    #[Group('hmvc')]
    #[TestDox('SpeakableSpecification is JSON serializable')]
    public function testSpeakableSpecificationIsJsonSerializable(): void
    {
        $speakableSpecification = new SpeakableSpecification();
        $this->assertInstanceOf(\JsonSerializable::class, $speakableSpecification);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SpeakableSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SpeakableSpecification::VERSION);
        $this->assertIsString(SpeakableSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SpeakableSpecification::VERSION, '0.1.0', '>=')
        );
    }
}
