<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\RequiresPhp;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \explode;
use function \str_replace;
use function \strlen;
use function \strtoupper;
use function \ord;

#[CoversClass(\StoreCore\Types\ActivationKey::class)]
#[RequiresPhp('>= 8.2.0')]
final class ActivationKeyTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ActivationKey class exists')]
    public function testActivationKeyClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ActivationKey.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ActivationKey.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\ActivationKey'));
        $this->assertTrue(class_exists(ActivationKey::class));
    }


    #[Group('hmvc')]
    #[TestDox('ActivationKey class is concrete')]
    public function testActivationKeyClassIsConcrete(): void
    {
        $class = new ReflectionClass(ActivationKey::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Depends('testActivationKeyClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('ActivationKey is stringable')]
    public function testActivationKeyIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new ActivationKey());
    }


    #[TestDox('ActivationKey::__construct exists')]
    public function testActivationKeyConstructorExists(): void
    {
        $class = new ReflectionClass(ActivationKey::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testActivationKeyConstructorExists')]
    #[TestDox('ActivationKey::__construct is public constructor')]
    public function testActivationKeyConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(ActivationKey::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testActivationKeyConstructorExists')]
    #[TestDox('ActivationKey::__construct has three OPTIONAL parameters')]
    public function testActivationKeyConstructorHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(ActivationKey::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[Depends('testActivationKeyIsStringable')]
    #[TestDox('ActivationKey::__toString returns non-empty string')]
    public function testActivationKeyToStringReturnsNonEmptyString(): void
    {
        $key = new ActivationKey();
        $this->assertNotEmpty((string) $key);
        $this->assertNotEmpty($key->__toString());
        $this->assertIsString($key->__toString());
    }

    #[Depends('testActivationKeyIsStringable')]
    #[TestDox('ActivationKey::__toString returns uppercase string')]
    public function testActivationKeyToStringReturnsUppercaseString(): void
    {
        $key = new ActivationKey();
        $key = (string) $key;
        $this->assertEquals(strtoupper($key), $key);
        $this->assertNotEquals(strtolower($key), $key);
    }

    
    #[Depends('testActivationKeyToStringReturnsNonEmptyString')]
    #[TestDox('ActivationKey string contains hyphen - as default separator')]
    public function testActivationKeyStringContainsHyphenAsDefaultSeparator(): void
    {
        $key = new ActivationKey();
        $key = (string) $key;
        $this->assertStringContainsString('-', $key);
    }

    #[Depends('testActivationKeyStringContainsHyphenAsDefaultSeparator')]
    #[TestDox('ActivationKey::__construct changes default separator')]
    public function testActivationKeyConstructorChangesDefaultSeparator(): void
    {
        $key = new ActivationKey(separator: '.');
        $key = (string) $key;
        $this->assertStringNotContainsString('-', $key);
        $this->assertStringContainsString('.', $key);
    }


    #[Depends('testActivationKeyToStringReturnsNonEmptyString')]
    #[Depends('testActivationKeyStringContainsHyphenAsDefaultSeparator')]
    #[TestDox('ActivationKey::__toString returns 3 separated chunks of 4 characters by default')]
    public function testActivationKeyToStringReturnsThreeSeparatedChunksOfFourCharactersByDefault(): void
    {
        $key = new ActivationKey();
        $key = (string) $key;

        $chunks = explode('-', $key);
        $this->assertCount(3, $chunks);
        foreach ($chunks as $chunk) {
            $this->assertEquals(4, strlen($chunk));
        }
    }

    #[Depends('testActivationKeyToStringReturnsThreeSeparatedChunksOfFourCharactersByDefault')]
    #[TestDox('ActivationKey::__construct sets number of chunks and chunk length')]
    public function testActivationKeyConstructorSetsNumberOfChunksAndChunkLength(): void
    {
        // Create 4 chunks of 5 characters
        $key = new ActivationKey(4, 5);
        $this->assertEquals(23, strlen((string) $key));

        $chunks = explode('-', (string) $key);
        $this->assertCount(4, $chunks);
        foreach ($chunks as $chunk) {
            $this->assertEquals(5, strlen($chunk));
        }
    }


    #[Depends('testActivationKeyIsStringable')]
    #[Depends('testActivationKeyToStringReturnsNonEmptyString')]
    #[TestDox('ActivationKey string does not contain confusing characters')]
    public function testActivationKeyStringDoesNotContainConfusingCharacters(): void
    {
        $confusing_characters = [
            '0', 'O',
            '1', 'I',
            '2', 'Z',
            '5', 'S',
            '8', 'B', 
        ];

        for ($i = 0; $i < 10; $i++) {
            $key = new ActivationKey(4, 4);
            $key = (string) $key;
            foreach ($confusing_characters as $character) {
                $this->assertStringNotContainsString(
                    $character, $key,
                    'ActivationKey MUST NOT contains the character ' . $character . ' (ASCII ' . ord($character) . ')');
            }
        }
    }


    #[TestDox('ActivationKey::valueOf exists')]
    public function testActivationKeyValueOfExists(): void
    {
        $class = new ReflectionClass(ActivationKey::class);
        $this->assertTrue($class->hasMethod('valueOf'));
    }

    #[Depends('testActivationKeyValueOfExists')]
    #[TestDox('ActivationKey::valueOf is public')]
    public function testActivationKeyValueOfIsPublic(): void
    {
        $method = new ReflectionMethod(ActivationKey::class, 'valueOf');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testActivationKeyValueOfExists')]
    #[TestDox('ActivationKey::valueOf has no parameters')]
    public function testActivationKeyValueOfHasNoParameters(): void
    {
        $method = new ReflectionMethod(ActivationKey::class, 'valueOf');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('ActivationKey::valueOf returns string value without separator')]
    public function testActivationKeyValueOfReturnsStringValueWithoutSeparator(): void
    {
        // Product key formatted like XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
        $key = new ActivationKey(5, 5, '-');
        $this->assertNotEmpty($key->valueOf());
        $this->assertIsString($key->valueOf());

        $this->assertEquals(29, strlen($key->__toString()));
        $this->assertEquals(25, strlen($key->valueOf()));

        $this->assertEquals(
            $key->valueOf(), str_replace('-', '', $key->__toString()),
            'ActivationKey::valueOf MUST BE identical to the string value without separators'
        );
    }
}
