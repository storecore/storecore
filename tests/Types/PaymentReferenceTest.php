<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \ctype_digit;
use function \date;
use function \is_numeric;
use function \random_int;
use function \strlen;
use function \version_compare;

#[CoversClass(\StoreCore\Types\PaymentReference::class)]
final class PaymentReferenceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PaymentReference class exists')]
    public function testPaymentReferenceClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PaymentReference.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'PaymentReference.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\PaymentReference'));
        $this->assertTrue(class_exists(PaymentReference::class));
    }

    #[Group('hmvc')]
    #[TestDox('PaymentReference class is concrete')]
    public function testPaymentReferenceClassIsConcrete(): void
    {
        $class = new ReflectionClass(PaymentReference::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PaymentReference is stringable')]
    public function testPaymentReferenceIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new PaymentReference());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PaymentReference::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PaymentReference::VERSION);
        $this->assertIsString(PaymentReference::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PaymentReference::VERSION, '1.2.1', '>=')
        );
    }


    #[TestDox('PaymentReference::__construct() exists')]
    public function testPaymentReferenceConstructorExists(): void
    {
        $class = new ReflectionClass(PaymentReference::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('PaymentReference::__construct() is public constructor')]
    public function testPaymentReferenceConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(PaymentReference::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('PaymentReference::__construct() has two OPTIONAL parameters')]
    public function testPaymentReferenceConstructHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(PaymentReference::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('PaymentReference::__toString() exists')]
    public function testPaymentReferenceToStringExists(): void
    {
        $class = new ReflectionClass(PaymentReference::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testPaymentReferenceToStringExists')]
    #[TestDox('PaymentReference::__toString() returns non-empty string')]
    public function testPaymentReferenceToStringReturnsNonEmptyString(): void
    {
        $reference = new PaymentReference();
        $this->assertNotEmpty((string) $reference);
        $this->assertIsString((string) $reference);
    }

    #[Depends('testPaymentReferenceToStringReturnsNonEmptyString')]
    #[TestDox('PaymentReference::__toString() returns numeric string')]
    public function testPaymentReferenceToStringReturnsNumericString(): void
    {
        $reference = new PaymentReference();
        $this->assertTrue(is_numeric((string) $reference));
    }

    #[Depends('testPaymentReferenceToStringReturnsNumericString')]
    #[TestDox('PaymentReference::__toString() returns numeric string')]
    public function testPaymentReferenceToStringReturnsAtLeastSevenDigits(): void
    {
        $reference = new PaymentReference();
        $reference = (string) $reference;
        $this->assertTrue(ctype_digit($reference));
        $this->assertTrue(strlen($reference) >= 7);
    }

    #[Depends('testPaymentReferenceToStringReturnsNumericString')]
    #[TestDox('PaymentReference::__toString() returns 16 digits by default')]
    public function testPaymentReferenceToStringReturnsSixteenDigitsByDefault(): void
    {
        $reference = new PaymentReference();
        $reference = (string) $reference;
        $this->assertTrue(ctype_digit($reference));
        $this->assertTrue(strlen($reference) === 16);
    }

    #[Depends('testPaymentReferenceToStringReturnsSixteenDigitsByDefault')]
    #[TestDox('Uninitialized PaymentReference::__toString() contains current date')]
    public function testUninitializedPaymentReferenceToStringContainsCurrentDate(): void
    {
        $reference = new PaymentReference();
        $reference = (string) $reference;
        $this->assertEquals(16, strlen($reference));
        $this->assertStringContainsString(date('Ymd'), (string) $reference);
    }


    #[TestDox('PaymentReference::setTransactionID() exists')]
    public function testPaymentReferenceSetTransactionIdExists(): void
    {
        $class = new ReflectionClass(PaymentReference::class);
        $this->assertTrue($class->hasMethod('setTransactionID'));
    }

    #[TestDox('PaymentReference::setTransactionID() is public')]
    public function testPaymentReferenceSetTransactionIdIsPublic(): void
    {
        $method = new ReflectionMethod(PaymentReference::class, 'setTransactionID');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('PaymentReference::setTransactionID() has one REQUIRED parameter')]
    public function testPaymentReferenceSetTransactionIdHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PaymentReference::class, 'setTransactionID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PaymentReference::setTransactionID() has one REQUIRED parameter')]
    public function testPaymentReferenceToStringEndsWithTransactionId(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $payment_reference = new PaymentReference();
            $random_transaction_id = random_int(100000, 999999);
            $payment_reference->setTransactionID($random_transaction_id);
            $this->assertStringEndsWith((string) $random_transaction_id, (string) $payment_reference);
        }

        for ($i = 1; $i <= 10; $i++) {
            $random_transaction_id = random_int(100000, 999999);
            $payment_reference = new PaymentReference($random_transaction_id);
            $this->assertStringEndsWith((string) $random_transaction_id, (string) $payment_reference);
        }
    }
}
