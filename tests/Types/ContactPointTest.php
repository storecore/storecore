<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \json_encode;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Types\ContactPoint::class)]
class ContactPointTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ContactPoint class exists')]
    public function testContactPointClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ContactPoint.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ContactPoint.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\ContactPoint'));
        $this->assertTrue(class_exists(ContactPoint::class));
    }

    #[TestDox('ContactPoint class is concrete')]
    public function testContactPointClassIsConcrete(): void
    {
        $class = new ReflectionClass(ContactPoint::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
        $this->assertFalse($class->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('ContactPoint is an Intangible StructuredValue')]
    public function testContactPointIsIntangibleStructuredValue(): void
    {
        $contactPoint = new ContactPoint();
        $this->AssertInstanceOf(Intangible::class, $contactPoint);
        $this->AssertInstanceOf(StructuredValue::class, $contactPoint);
    }

    #[Group('hmvc')]
    #[TestDox('ContactPoint is JSON serializable')]
    public function testContactPointIsJsonSerializable(): void
    {
        $this->AssertInstanceOf(\JsonSerializable::class, new ContactPoint());
    }

    #[Depends('testContactPointIsJsonSerializable')]
    #[Group('hmvc')]
    #[TestDox('ContactPoint is stringable')]
    public function testContactPointIsStringable(): void
    {
        $this->AssertInstanceOf(\Stringable::class, new ContactPoint());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ContactPoint::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ContactPoint::VERSION);
        $this->assertIsString(ContactPoint::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ContactPoint::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('ContactPoint::__construct() exists')]
    public function testContactPointConstructorExists(): void
    {
        $class = new ReflectionClass(ContactPoint::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('ContactPoint::__construct() is public constructor')]
    public function testContactPointConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(ContactPoint::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('ContactPoint::__construct() has one OPTIONAL parameter')]
    public function testContactPointConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(ContactPoint::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    /**
     * @see https://schema.org/ContactPoint Schema.org `ContactPoint` example 2
     */
    #[Depends('testContactPointIsJsonSerializable')]
    #[Test]
    #[TestDox('Schema.org acceptance test')]
    public function SchemaOrgAcceptanceTest(): void
    {
        $contactPoint = new ContactPoint();
        $contactPoint->telephone = '+1-401-555-1212';
        $contactPoint->contactType = 'customer service';

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "ContactPoint",
              "telephone" : "+1-401-555-1212",
              "contactType" : "customer service"
            }
        ';
        $actualJson = json_encode($contactPoint, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
