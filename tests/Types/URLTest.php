<?php

/**
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\Engine\UriFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\URL::class)]
#[CoversClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class URLTest extends TestCase
{
    #[TestDox('URL class is concrete')]
    public function testURLClassIsConcrete(): void
    {
        $class = new ReflectionClass(URL::class);
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('URL is stringable')]
    public function testUrlIsStringable(): void
    {
        $url = new URL('https://www.example.com/');
        $this->assertInstanceOf(\Stringable::class, $url);

        $this->assertEquals('https://www.example.com/', $url->__toString());
        $this->assertEquals('https://www.example.com/', (string) $url);
    }

    #[Group('hmvc')]
    #[TestDox('URL is JSOM serializable')]
    public function testUrlIsJsonSerializable(): void
    {
        $url = new URL('https://www.example.com/');
        $this->assertInstanceOf(\JsonSerializable::class, $url);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(URL::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(URL::VERSION);
        $this->assertIsString(URL::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(URL::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('URL::__construct() exists')]
    public function testURLConstructorExists(): void
    {
        $class = new ReflectionClass(URL::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('URL::__construct() is public constructor')]
    public function testURLConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(URL::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('URL::__construct() has one REQUIRED parameter')]
    public function testURLConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(URL::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    /**
     * @see https://url.spec.whatwg.org/#example-url-parsing
     */
    #[TestDox('URL::__construct() accepts URL as string')]
    public function testURLConstructorAcceptsURLAsString(): void
    {
        $url = new URL('https://example.org/');
        $this->assertSame('https://example.org/', (string) $url);
    }

    #[TestDox('URL::__construct() throws value error exception on invalid string')]
    public function testURLConstructorThrowsValueErrorExceptionOnInvalidString(): void
    {
        $this->expectException(\ValueError::class);
        $url = new URL('https:example.org');
    }

    #[TestDox('URL::__construct() accepts URL as PSR-7 UriInterface')]
    public function testURLConstructorAcceptsURLAsPSR7UriInterface(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUri('https://example.org/');
        $this->assertIsObject($uri);
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $uri);

        $url = new URL($uri);
        $this->assertEquals('https://example.org/', (string) $url);
    }
}
