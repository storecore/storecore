<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(BusinessFunction::class)]
final class BusinessFunctionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('BusinessFunction enumeration file exists')]
    public function testBusinessFunctionEnumerationFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'BusinessFunction.php'
        );
    }

    #[Group('distro')]
    #[TestDox('BusinessFunction enumeration file is readable')]
    public function testBusinessFunctionEnumerationFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'BusinessFunction.php'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BusinessFunction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BusinessFunction::VERSION);
        $this->assertIsString(BusinessFunction::VERSION);
    }

    /**
     * @see https://schema.org/docs/releases.html
     */
    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org v23.0')]
    public function testVersionMatchesSchemaOrgVersion23(): void
    {
        $this->assertTrue(
            version_compare(BusinessFunction::VERSION, '23.0', '>=')
        );
    }


    /**
     * @see https://schema.org/BusinessFunction
     */
    #[TestDox('BusinessFunction enumeration has eight members')]
    public function testBusinessFunctionEnumerationHasEightMembers(): void
    {
        $this->assertCount(8, BusinessFunction::cases());
    }

    #[TestDox('BusinessFunction enumeration values are GoodRelations URLs')]
    public function testBusinessFunctionEnumerationValuesAreGoodRelationsUrls(): void
    {
        foreach (BusinessFunction::cases() as $case) {
            $this->assertNotEmpty($case->value);
            $this->assertIsString($case->value);
            $this->assertEquals(filter_var($case->value, \FILTER_VALIDATE_URL), $case->value);
            $this->assertStringStartsWith('http://purl.org/goodrelations/', $case->value);
        }
    }

    #[TestDox('Default BusinessFunction::Sell exists')]
    public function testDefaultBusinessFunctionSellExists(): void
    {
        $this->assertEquals('http://purl.org/goodrelations/v1#Sell', BusinessFunction::Sell->value);
    }
}
