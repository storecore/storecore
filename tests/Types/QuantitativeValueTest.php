<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Types\QuantitativeValue::class)]
#[CoversClass(\StoreCore\Types\CommonCode::class)]
final class QuantitativeValueTest extends TestCase
{
    #[TestDox('QuantitativeValue class is concrete')]
    public function testQuantitativeValueClassIsConcrete(): void
    {
        $class = new ReflectionClass(QuantitativeValue::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('QuantitativeValue is a StructuredValue')]
    public function testQuantitativeValueIsStructuredValue(): void
    {
        $value = new QuantitativeValue();
        $this->assertInstanceOf(\StoreCore\Types\StructuredValue::class, $value);
    }

    #[Group('hmvc')]
    #[TestDox('QuantitativeValue is JSON serializable')]
    public function testQuantitativeValueIsJsonSerializable(): void
    {
        $value = new QuantitativeValue();
        $this->assertInstanceOf(\JsonSerializable::class, $value);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(QuantitativeValue::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(QuantitativeValue::VERSION);
        $this->assertIsString(QuantitativeValue::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(QuantitativeValue::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('QuantitativeValue::__construct exists')]
    public function testQuantitativeValueConstructorExists(): void
    {
        $class = new ReflectionClass(QuantitativeValue::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('QuantitativeValue::__construct is public constructor')]
    public function testQuantitativeValueConstructorIsPublic(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('QuantitativeValue::__construct has four OPTIONAL parameters')]
    public function testQuantitativeValueConstructorHasFourOptionalParameters(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, '__construct');
        $this->assertEquals(4, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('QuantitativeValue.unitCode exists')]
    public function testQuantitativeValueUnitCodeExists(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertObjectHasProperty('unitCode', $quantitativeValue);
    }

    #[TestDox('QuantitativeValue.unitCode is public')]
    public function testQuantitativeValueUnitCodeIsPublic(): void
    {
        $property = new ReflectionProperty(QuantitativeValue::class, 'unitCode');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('QuantitativeValue.unitCode is null by default')]
    public function testQuantitativeValueUnitCodeIsNullByDefault(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertNull($quantitativeValue->unitCode);
    }

    #[TestDox('QuantitativeValue::setUnitCode exists')]
    public function testQuantitativeValueSetUnitCodeExists(): void
    {
        $class = new ReflectionClass(QuantitativeValue::class);
        $this->assertTrue($class->hasMethod('setUnitCode'));
    }

    #[TestDox('QuantitativeValue::setUnitCode is public')]
    public function testQuantitativeValueSetUnitCodeIsPublic(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, 'setUnitCode');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('QuantitativeValue::setUnitCode has one REQUIRED parameter')]
    public function testQuantitativeValueSetUnitCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, 'setUnitCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('QuantitativeValue.value exists')]
    public function testQuantitativeValueValueExists(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertObjectHasProperty('value', $quantitativeValue);
    }

    #[TestDox('QuantitativeValue.value is public')]
    public function testQuantitativeValueValueIsPublic(): void
    {
        $property = new ReflectionProperty(QuantitativeValue::class, 'value');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('QuantitativeValue.value is null by default')]
    public function testQuantitativeValueValueIsNullByDefault(): void
    {
        $value = new QuantitativeValue();
        $this->assertNull($value->value);
    }

    #[TestDox('QuantitativeValue::setValue exists')]
    public function testQuantitativeValueSetValueExists(): void
    {
        $class = new ReflectionClass(QuantitativeValue::class);
        $this->assertTrue($class->hasMethod('setValue'));
    }

    #[TestDox('QuantitativeValue::setValue is public')]
    public function testQuantitativeValueSetValueIsPublic(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, 'setValue');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('QuantitativeValue::setValue has one REQUIRED parameter')]
    public function testQuantitativeValueSetValueHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(QuantitativeValue::class, 'setValue');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('QuantitativeValue.minValue exists')]
    public function testQuantitativeValueMinValueExists(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertObjectHasProperty('minValue', $quantitativeValue);
    }

    #[TestDox('QuantitativeValue.minValue is public')]
    public function testQuantitativeValueMinValueIsPublic(): void
    {
        $property = new ReflectionProperty(QuantitativeValue::class, 'minValue');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('QuantitativeValue.minValue is null by default')]
    public function testQuantitativeValueMinValueIsNullByDefault(): void
    {
        $value = new QuantitativeValue();
        $this->assertNull($value->minValue);
    }


    #[TestDox('QuantitativeValue.maxValue exists')]
    public function testQuantitativeValueMaxValueExists(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertObjectHasProperty('maxValue', $quantitativeValue);
    }

    #[TestDox('QuantitativeValue.maxValue is public')]
    public function testQuantitativeValueMaxValueIsPublic(): void
    {
        $property = new ReflectionProperty(QuantitativeValue::class, 'maxValue');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('QuantitativeValue.maxValue is null by default')]
    public function testQuantitativeValueMaxValueIsNullByDefault(): void
    {
        $value = new QuantitativeValue();
        $this->assertNull($value->maxValue);
    }


    #[TestDox('QuantitativeValue.valueReference exists')]
    public function testQuantitativeValueValueReferenceExists(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertObjectHasProperty('valueReference', $quantitativeValue);
    }

    #[TestDox('QuantitativeValue.valueReference is public')]
    public function testQuantitativeValueValueReferenceIsPublic(): void
    {
        $property = new ReflectionProperty(QuantitativeValue::class, 'valueReference');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('QuantitativeValue.valueReference is null by default')]
    public function testQuantitativeValueValueReferenceIsNullByDefault(): void
    {
        $quantitativeValue = new QuantitativeValue();
        $this->assertNull($quantitativeValue->valueReference);
    }

    #[Test]
    #[TestDox('Quantitative value with a reference value')]
    public function QuantitativeValueWithReferenceValue(): void
    {
        // 0.5 kilogram is 500 gram
        $weight = new QuantitativeValue(0.5, 'KGM');
        $weight->valueReference = new QuantitativeValue(500, 'GRM');

        $this->assertEquals('KGM', $weight->unitCode);
        $this->assertEquals('GRM', $weight->valueReference->unitCode);

        $this->assertEquals(.5, $weight->value);
        $this->assertEquals(500, $weight->valueReference->value);


        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "QuantitativeValue",
              "unitCode": "KGM",
              "value": 0.5,
              "valueReference": {
                "@type": "QuantitativeValue",
                "unitCode": "GRM",
                "value": 500
              }
            }
        JSON;
        $this->assertJson($expectedJson);

        $actualJson = json_encode($weight, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
