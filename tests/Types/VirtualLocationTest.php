<?php

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\Types\URL;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class VirtualLocationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VirtualLocation class exists')]
    public function testVirtualLocationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'VirtualLocation.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'VirtualLocation.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\VirtualLocation'));
        $this->assertTrue(class_exists(VirtualLocation::class));
    }

    #[TestDox('VirtualLocation class is concrete')]
    public function testVirtualLocationClassIsConcrete(): void
    {
        $class = new ReflectionClass(VirtualLocation::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('VirtualLocation is an Intangible Thing')]
    public function testVirtualLocationIsIntangibleThing(): void
    {
        $object = new VirtualLocation();
        $this->assertInstanceOf(Intangible::class, $object);
        $this->assertInstanceOf(Thing::class, $object);
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('VirtualLocation is JSON serializable')]
    public function testVirtualLocationIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new VirtualLocation());
    }

    #[Group('hmvc')]
    #[TestDox('VirtualLocation is stringable')]
    public function testVirtualLocationIsStringable(): void
    {
        $virtual_location = new VirtualLocation();
        $this->assertInstanceOf(\Stringable::class, $virtual_location);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(VirtualLocation::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(VirtualLocation::VERSION);
        $this->assertIsString(VirtualLocation::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(VirtualLocation::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/VirtualLocation#eg-0014 
     *      Schema.org `VirtualLocation` example 1
     */
    #[TestDox('Schema.org example')]
    public function testSchemaOrgExample(): void
    {
        $location = new VirtualLocation();
        $location->url = new URL('https://stream.storytimereadings.com/');
        $json = json_encode($location, \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"@type":"VirtualLocation"', $json);
        $this->assertStringContainsString('"url":"https://stream.storytimereadings.com/"', $json);
    }
}
