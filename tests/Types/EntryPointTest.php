<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\EntryPoint::class)]
final class EntryPointTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('EntryPoint class exists')]
    public function testEntryPointClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'EntryPoint.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'EntryPoint.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\EntryPoint'));
        $this->assertTrue(class_exists(EntryPoint::class));
    }


    #[TestDox('EntryPoint class is concrete')]
    public function testEntryPointClassIsConcrete(): void
    {
        $class = new ReflectionClass(EntryPoint::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('EntryPoint is an Intangible Thing')]
    public function testEntryPointExtendsIntangibleThing(): void
    {
        $entry_point = new EntryPoint();
        $this->assertInstanceOf(Intangible::class, $entry_point);
        $this->assertInstanceOf(Thing::class, $entry_point);
    }

    #[Group('hmvc')]
    #[TestDox('EntryPoint is JSON serializable')]
    public function testEntryPointIsJsonSerializable(): void
    {
        $entry_point = new EntryPoint();
        $this->assertInstanceOf(\JsonSerializable::class, $entry_point);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EntryPoint::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EntryPoint::VERSION);
        $this->assertIsString(EntryPoint::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EntryPoint::VERSION, '0.2.0', '>=')
        );
    }


    /**
     * @see https://developers.google.com/search/docs/advanced/structured-data/sitelinks-searchbox
     */
    #[TestDox('EntryPoint.urlTemplate exists')]
    public function testEntryPointUrlTemplateExists(): void
    {
        $class = new ReflectionClass(EntryPoint::class);
        $this->assertTrue($class->hasProperty('urlTemplate'));
    }

    #[TestDox('EntryPoint.urlTemplate is public')]
    public function testEntryPointUrlTemplateIsPublic(): void
    {
        $property = new ReflectionProperty(EntryPoint::class, 'urlTemplate');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('EntryPoint.urlTemplate is nullable string')]
    public function testEntryPointUrlTemplateIsNullableString(): void
    {
        $entry_point = new EntryPoint();
        $entry_point->urlTemplate = 'https://query.example.com/search?q={search_term_string}';

        $this->assertNotEmpty($entry_point->urlTemplate);
        $this->assertIsString($entry_point->urlTemplate);
        $this->assertSame('https://query.example.com/search?q={search_term_string}', $entry_point->urlTemplate);

        $entry_point->urlTemplate = null;
        $this->assertNull($entry_point->urlTemplate);
    }

    #[TestDox('EntryPoint.urlTemplate is null by default')]
    public function testEntryPointUrlTemplateIsNullByDefault(): void
    {
        $entry_point = new EntryPoint();
        $this->assertNull($entry_point->urlTemplate);
    }
}
