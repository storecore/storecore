<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\Date::class)]
final class DateTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Date class exists')]
    public function testDateClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Date.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Date.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\Date'));
        $this->assertTrue(class_exists(Date::class));
    }

    #[Group('hmvc')]
    #[TestDox('Date class is concrete')]
    public function testDateClassIsConcrete(): void
    {
        $class = new ReflectionClass(Date::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Date is PHP DateTime')]
    public function testDateIsAPHPDateTime(): void
    {
        $stub = $this->createStub(Date::class);
        $this->assertInstanceOf(\DateTime::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Date implements PHP DateTimeInterface')]
    public function testDateImplementsPHPDateInterface(): void
    {
        $stub = $this->createStub(Date::class);
        $this->assertInstanceOf(\DateTimeInterface::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Date is stringable')]
    public function testDateIsStringable(): void
    {
        $stub = $this->createStub(Date::class);
        $this->assertInstanceOf(\Stringable::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Date is JSON serializable')]
    public function testDateIsJsonSerializable(): void
    {
        $stub = $this->createStub(Date::class);
        $this->assertInstanceOf(\JsonSerializable::class, $stub);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Date::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Date::VERSION);
        $this->assertIsString(Date::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Date::VERSION, '1.0.0', '>=')
        );
    }


    #[Depends('testDateIsJsonSerializable')]
    #[Depends('testDateIsStringable')]
    #[TestDox('Date can be created from ISO date as string')]
    public function testDateCanBeCreatedFromIsoDateAsString(): void
    {
        $date = new Date('2014-11-07');
        $this->assertInstanceOf(\DateTimeInterface::class, $date);
        $this->assertEquals('2014-11-07', $date->format('Y-m-d'));

        $this->assertEquals('2014-11-07', (string) $date);
        $this->assertEquals('"2014-11-07"', json_encode($date));
    }

    #[Depends('testDateCanBeCreatedFromIsoDateAsString')]
    #[TestDox('Date can be created from date as string')]
    public function testDateCanBeCreatedFromDateAsString(): void
    {
        $date = new Date('8 November 2023');
        $this->assertInstanceOf(\DateTimeInterface::class, $date);
        $this->assertEquals('2023-11-08', (string) $date);
        $this->assertEquals('"2023-11-08"', json_encode($date));

        $date = new Date('November 8, 2023');
        $this->assertInstanceOf(\DateTimeInterface::class, $date);
        $this->assertEquals('2023-11-08', (string) $date);
        $this->assertEquals('"2023-11-08"', json_encode($date));
    }
}
