<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Types\StructuredValue::class)]
final class StructuredValueTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('StructuredValue class exists')]
    public function testStructuredValueClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'StructuredValue.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'StructuredValue.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\StructuredValue'));
        $this->assertTrue(class_exists(StructuredValue::class));
    }

    #[TestDox('StructuredValue class is concrete')]
    public function testStructuredValueClassIsConcrete(): void
    {
        $class = new ReflectionClass(StructuredValue::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('StructuredValue is an Intangible Thing')]
    public function testStructuredValueIsIntangibleThing(): void
    {
        $structured_value = new StructuredValue();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $structured_value);
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, $structured_value);
    }

    #[Group('hmvc')]
    #[TestDox('StructuredValue is JSON serializable')]
    public function testStructuredValueIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new StructuredValue());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(StructuredValue::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(StructuredValue::VERSION);
        $this->assertIsString(StructuredValue::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(StructuredValue::VERSION, '1.0.0', '>=')
        );
    }
}
