<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Audience;
use StoreCore\CRM\Organization;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(Service::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class ServiceTest extends TestCase
{
    #[TestDox('Service class is concrete')]
    public function testServiceClassIsConcrete(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Service is an Intangible Thing')]
    public function testServiceIsIntangibleThing(): void
    {
        $service = new Service();
        $this->assertInstanceOf(Intangible::class, $service);
        $this->assertInstanceOf(Thing::class, $service);
    }

    #[TestDox('Service is JSON serializable')]
    public function testServiceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Service());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Service::VERSION);
        $this->assertIsString(Service::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Service::VERSION, '0.2.0', '>=')
        );
    }


    /**
     * @see https://schema.org/audience
     *      Schema.org `audience` property of a `Service`
     */

    #[TestDox('Service.audience exists')]
    public function testServiceAudienceExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasProperty('audience'));
    }

    #[Depends('testServiceAudienceExists')]
    #[TestDox('Service.audience is public')]
    public function testServiceAudienceIsPublic(): void
    {
        $property = new \ReflectionProperty(Service::class, 'audience');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testServiceAudienceExists')]
    #[Depends('testServiceAudienceIsPublic')]
    #[TestDox('Service.audience is null by default')]
    public function testServiceAudienceIsNullByDefault(): void
    {
        $service = new Service();
        $this->assertNull($service->audience);
    }

    #[TestDox('Service.audience accepts Audience')]
    public function testServiceAudienceAcceptsAudience(): void
    {
        $service = new Service('Carpet cleaning');
        $service->audience = new Audience('Home owners');
        $this->assertNotNull($service->audience);
        $this->assertIsObject($service->audience);
        $this->assertSame('Home owners', $service->audience->name);
    }


    /**
     * @see https://schema.org/provider
     *      Schema.org `audience` property of a `Service`
     */
    #[TestDox('Service.provider exists')]
    public function testServiceProviderExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasProperty('provider'));
    }

    #[Depends('testServiceProviderExists')]
    #[TestDox('Service.provider is public')]
    public function testServiceProviderIsPublic(): void
    {
        $property = new \ReflectionProperty(Service::class, 'provider');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testServiceProviderExists')]
    #[Depends('testServiceProviderIsPublic')]
    #[TestDox('Service.provider is null by default')]
    public function testServiceProviderIsNullByDefault(): void
    {
        $service = new Service();
        $this->assertNull($service->provider);
    }

    #[Depends('testServiceProviderExists')]
    #[Depends('testServiceProviderIsPublic')]
    #[TestDox('Service.provider accepts Organization')]
    public function testServiceProviderAcceptsOrganizationByDefault(): void
    {
        $provider = new Organization();
        $provider->name = 'StoreCore';

        $service = new Service();
        $service->provider = $provider;

        $this->assertNotNull($service->provider);
        $this->assertIsObject($service->provider);
        $this->assertInstanceOf(\JsonSerializable::class, $service->provider);
        $this->assertSame('StoreCore', $service->provider->name);
    }
}
