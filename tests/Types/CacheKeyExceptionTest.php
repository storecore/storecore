<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(CacheKeyException::class)]
class CacheKeyExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CacheKeyException class file exists')]
    public function testCacheKeyExceptionClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CacheKeyException.php'
        );
    }

    #[Depends('testCacheKeyExceptionClassFileExists')]
    #[Group('distro')]
    #[TestDox('CacheKeyException class file is readable')]
    public function testCacheKeyExceptionClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CacheKeyException.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('CacheKeyException class is concrete')]
    public function testCacheKeyExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CacheKeyException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CacheKeyException class is a throwable exception')]
    public function testCacheKeyExceptionClassIsThrowableException(): void
    {
        $exception = new CacheKeyException();
        $this->assertInstanceOf(\Throwable::class, $exception);
        $this->assertInstanceOf(\Exception::class, $exception);
    }

    /**
     * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-16-simple-cache.md
     */
    #[Group('hmvc')]
    #[TestDox('CacheKeyException is a PSR-16 SimpleCache CacheException')]
    public function testCacheKeyExceptionIsPSR16SimpleCacheCacheException(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheException::class));

        $this->expectException(\Psr\SimpleCache\CacheException::class);
        throw new CacheKeyException();

    }

    #[Depends('testCacheKeyExceptionIsPSR16SimpleCacheCacheException')]
    #[Group('hmvc')]
    #[TestDox('CacheKeyException is a PSR-16 SimpleCache InvalidArgumentException')]
    public function testCacheKeyExceptionIsPSR16SimpleCacheInvalidArgumentException(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\InvalidArgumentException'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\InvalidArgumentException::class));

        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        throw new CacheKeyException();
    }
}
