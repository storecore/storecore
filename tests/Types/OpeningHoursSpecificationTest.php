<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\OpeningHoursSpecification::class)]
final class OpeningHoursSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OpeningHoursSpecification class exists')]
    public function testOpeningHoursSpecificationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'OpeningHoursSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'OpeningHoursSpecification.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\OpeningHoursSpecification'));
        $this->assertTrue(class_exists(OpeningHoursSpecification::class));
    }

    #[TestDox('OpeningHoursSpecification class is concrete')]
    public function testOpeningHoursSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(OpeningHoursSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OpeningHoursSpecification is a StructuredValue')]
    public function testOpeningHoursSpecificationIsStructuredValue(): void
    {
        $opening_hours_specification = new OpeningHoursSpecification();
        $this->assertInstanceOf(Intangible::class, $opening_hours_specification);
        $this->assertInstanceOf(StructuredValue::class, $opening_hours_specification);
    }

    #[Group('hmvc')]
    #[TestDox('OpeningHoursSpecification is an Intangible Thing')]
    public function testOpeningHoursSpecificationIsIntangibleThing(): void
    {
        $opening_hours_specification = new OpeningHoursSpecification();
        $this->assertInstanceOf(Intangible::class, $opening_hours_specification);
        $this->assertInstanceOf(StructuredValue::class, $opening_hours_specification);
    }

    #[Group('seo')]
    #[TestDox('OpeningHoursSpecification is JSON serializable')]
    public function testOpeningHoursSpecificationIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new OpeningHoursSpecification());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OpeningHoursSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OpeningHoursSpecification::VERSION);
        $this->assertIsString(OpeningHoursSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OpeningHoursSpecification::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('OpeningHoursSpecification::closeAllDay exists')]
    public function testOpeningHoursSpecificationCloseAllDayExists(): void
    {
        $class = new ReflectionClass(OpeningHoursSpecification::class);
        $this->assertTrue($class->hasMethod('closeAllDay'));
    }

    #[TestDox('OpeningHoursSpecification::closeAllDay is public')]
    public function testOpeningHoursSpecificationCloseAllDayIsPublic(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'closeAllDay');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OpeningHoursSpecification::closeAllDay has one REQUIRED parameter')]
    public function testOpeningHoursSpecificationCloseAllDayHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'closeAllDay');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('OpeningHoursSpecification::openAllDay exists')]
    public function testOpeningHoursSpecificationOpenAllDayExists(): void
    {
        $class = new ReflectionClass(OpeningHoursSpecification::class);
        $this->assertTrue($class->hasMethod('openAllDay'));
    }

    #[TestDox('OpeningHoursSpecification::openAllDay is public')]
    public function testOpeningHoursSpecificationOpenAllDayIsPublic(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'openAllDay');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OpeningHoursSpecification::openAllDay has one REQUIRED parameter')]
    public function testOpeningHoursSpecificationOpenAllDayHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'openAllDay');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('OpeningHoursSpecification::set exists')]
    public function testOpeningHoursSpecificationSetExists(): void
    {
        $class = new ReflectionClass(OpeningHoursSpecification::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[TestDox('OpeningHoursSpecification::set is public')]
    public function testOpeningHoursSpecificationSetIsPublic(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'set');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('OpeningHoursSpecification::set has three parameters')]
    public function testOpeningHoursSpecificationSetHasThreeParameter(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'set');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testOpeningHoursSpecificationSetHasThreeParameter')]
    #[TestDox('OpeningHoursSpecification::set has two REQUIRED parameters')]
    public function testOpeningHoursSpecificationSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(OpeningHoursSpecification::class, 'set');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }
}
