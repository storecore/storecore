<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\DayOfWeek::class)]
final class DayOfWeekTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DayOfWeek class exists')]
    public function testDayOfWeekClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'DayOfWeek.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'DayOfWeek.php');
        $this->assertTrue(enum_exists('\\StoreCore\\Types\\DayOfWeek'));
        $this->assertTrue(enum_exists(DayOfWeek::class));
    }


    #[TestDox('DayOfWeek is an enumeration')]
    public function testDayOfWeekIsEnumeration(): void
    {
        $enum = new ReflectionClass(DayOfWeek::class);
        $this->assertTrue($enum->isEnum());
    }

    #[Depends('testDayOfWeekIsEnumeration')]
    #[TestDox('DayOfWeek is a backed enumeration')]
    public function testDayOfWeekIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(DayOfWeek::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DayOfWeek::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DayOfWeek::VERSION);
        $this->assertIsString(DayOfWeek::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DayOfWeek::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/PublicHolidays
     *      Schema.org enumeration member `PublicHolidays`
     */
    #[TestDox('DayOfWeek::PublicHolidays enumeration member exists')]
    public function testDayOfWeekPublicHolidaysEnumerationMemberExists(): void
    {
        $this->assertNotEmpty(DayOfWeek::PublicHolidays);
    }

    #[TestDox('DayOfWeek backed enum has eight members')]
    public function testDayOfWeekBackedEnumHasEightMembers(): void
    {
        $this->assertNotEmpty(DayOfWeek::cases());
        $this->assertIsArray(DayOfWeek::cases());
        $this->assertCount(8, DayOfWeek::cases());
    }
}
