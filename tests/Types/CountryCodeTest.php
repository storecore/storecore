<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\CountryCode::class)]
class CountryCodeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CountryCode class exists')]
    public function testCountryCodeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CountryCode.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CountryCode.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\CountryCode'));
        $this->assertTrue(class_exists(CountryCode::class));
    }

    #[TestDox('CountryCode class is concrete')]
    public function testCountryCodeClassIsConcrete(): void
    {
        $class = new ReflectionClass(CountryCode::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('CountryCode class is read-only')]
    public function testCountryCodeClassIsReadOnly(): void
    {
        $class = new ReflectionClass(CountryCode::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CountryCode::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CountryCode::VERSION);
        $this->assertIsString(CountryCode::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CountryCode::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('CountryCode::__construct() exists')]
    public function testCountryCodeConstructorExists()
    {
        $class = new ReflectionClass(CountryCode::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('CountryCode::__construct() has one REQUIRED parameter')]
    public function testCountryCodeConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(CountryCode::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CountryCode is stringable')]
    public function testCountryCodeIsStringable(): void
    {
        $countryCode = new CountryCode('NL');
        $this->assertInstanceOf(\Stringable::class, $countryCode);
    }

    #[Depends('testCountryCodeIsStringable')]
    #[TestDox('CountryCode::__toString() returns uppercase string')]
    public function testCountryCodeToStringReturnsUppercaseString(): void
    {
        $countryCode = new CountryCode('Be');
        $this->assertTrue(ctype_upper((string) $countryCode));
        $this->assertEquals('BE', (string) $countryCode);

        $countryCode = new CountryCode('nl');
        $this->assertTrue(ctype_upper((string) $countryCode));
        $this->assertEquals('NL', (string) $countryCode);
    }


    #[Depends('testCountryCodeIsStringable')]
    #[TestDox('CountryCode is JSON serializable')]
    public function testCountryCodeIsJsonSerializable(): void
    {
        $countryCode = new CountryCode('NL');
        $this->assertInstanceOf(\JsonSerializable::class, $countryCode);

        $this->assertNotEmpty(json_encode($countryCode));
        $this->assertIsString(json_encode($countryCode));
        $this->assertEquals(
            '"NL"',
            json_encode($countryCode),
            'In Schema.org JSON-LD a country code is a string and not a value object.'
        );
    }
}
