<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \random_int;
use function \version_compare;

#[CoversClass(\StoreCore\Types\AdultOrientedEnumeration::class)]
final class AdultOrientedEnumerationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AdultOrientedEnumeration enumeration file exists')]
    public function testAdultOrientedEnumerationEnumerationFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'AdultOrientedEnumeration.php'
        );
    }

    #[Depends('testAdultOrientedEnumerationEnumerationFileExists')]
    #[Group('distro')]
    #[TestDox('AdultOrientedEnumeration class file is readable')]
    public function testAdultOrientedEnumerationClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'AdultOrientedEnumeration.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('AdultOrientedEnumeration is an enumeration')]
    public function testAdultOrientedEnumerationIsEnumeration(): void
    {
        $enum = new ReflectionClass(AdultOrientedEnumeration::class);
        $this->assertTrue($enum->isEnum());
    }

    #[Depends('testAdultOrientedEnumerationIsEnumeration')]
    #[Group('hmvc')]
    #[TestDox('AdultOrientedEnumeration is a backed enumeration')]
    public function testAdultOrientedEnumerationIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(AdultOrientedEnumeration::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AdultOrientedEnumeration::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AdultOrientedEnumeration::VERSION);
        $this->assertIsString(AdultOrientedEnumeration::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org v23.0')]
    public function testVersionMatchesSchemaOrgVersion23(): void
    {
        $this->assertTrue(
            version_compare(AdultOrientedEnumeration::VERSION, '23.0', '>=')
        );
    }


    /**
     * @see https://schema.org/AdultOrientedEnumeration#subtypes
     */
    #[TestDox('AdultOrientedEnumeration enumeration members')]
    public function testAdultOrientedEnumerationEnumerationMembers(): void
    {
        $this->assertCount(10, AdultOrientedEnumeration::cases());

        $this->assertEquals('https://schema.org/AlcoholConsideration', AdultOrientedEnumeration::AlcoholConsideration->value);
        $this->assertEquals('https://schema.org/DangerousGoodConsideration', AdultOrientedEnumeration::DangerousGoodConsideration->value);
        $this->assertEquals('https://schema.org/HealthcareConsideration', AdultOrientedEnumeration::HealthcareConsideration->value);
        $this->assertEquals('https://schema.org/NarcoticConsideration', AdultOrientedEnumeration::NarcoticConsideration->value);
        $this->assertEquals('https://schema.org/ReducedRelevanceForChildrenConsideration', AdultOrientedEnumeration::ReducedRelevanceForChildrenConsideration->value);
        $this->assertEquals('https://schema.org/SexualContentConsideration', AdultOrientedEnumeration::SexualContentConsideration->value);
        $this->assertEquals('https://schema.org/TobaccoNicotineConsideration', AdultOrientedEnumeration::TobaccoNicotineConsideration->value);
        $this->assertEquals('https://schema.org/UnclassifiedAdultConsideration', AdultOrientedEnumeration::UnclassifiedAdultConsideration->value);
        $this->assertEquals('https://schema.org/ViolenceConsideration', AdultOrientedEnumeration::ViolenceConsideration->value);
        $this->assertEquals('https://schema.org/WeaponConsideration', AdultOrientedEnumeration::WeaponConsideration->value);
    }


    #[TestDox('AdultOrientedEnumeration::fromInteger() exists')]
    public function testAdultOrientedEnumerationFromIntegerExists(): void
    {
        $class = new ReflectionClass(AdultOrientedEnumeration::class);
        $this->assertTrue($class->hasMethod('fromInteger'));
    }

    #[TestDox('AdultOrientedEnumeration::fromInteger() is public static')]
    public function testAdultOrientedEnumerationFromIntegerIsPublicStatic(): void
    {
        $method = new ReflectionMethod(AdultOrientedEnumeration::class, 'fromInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
        $this->assertFalse($method->isConstructor());
    }

    #[TestDox('AdultOrientedEnumeration::fromInteger() has one REQUIRED parameter')]
    public function testAdultOrientedEnumerationFromIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AdultOrientedEnumeration::class, 'fromInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('AdultOrientedEnumeration::fromInteger() accepts tiny integers')]
    public function testAdultOrientedEnumerationFromIntegerAcceptsTinyIntegers(): void
    {
        $int = random_int(0, 9);
        $enum = AdultOrientedEnumeration::fromInteger($int);
        $this->assertInstanceOf(AdultOrientedEnumeration::class, $enum);
        $this->assertStringStartsWith('https://schema.org/', $enum->value);
    }

    #[Depends('testAdultOrientedEnumerationFromIntegerAcceptsTinyIntegers')]
    #[TestDox('AdultOrientedEnumeration::fromInteger() accepts (int) 0 as generic UnclassifiedAdultConsideration')]
    public function testAdultOrientedEnumerationFromIntegerAcceptsInt0AsGenericUnclassifiedAdultConsideration(): void
    {
        $enum = AdultOrientedEnumeration::fromInteger(0);
        $this->assertEquals('UnclassifiedAdultConsideration', $enum->name);
        $this->assertEquals('https://schema.org/UnclassifiedAdultConsideration', $enum->value);
    }

    #[TestDox('AdultOrientedEnumeration::fromInteger() throws value error on too large integer')]
    public function testAdultOrientedEnumerationFromIntegerThrowsValueErrorOnTooLargeInteger(): void
    {
        $this->expectException(\ValueError::class);
        $failure = AdultOrientedEnumeration::fromInteger(10);
    }
}
