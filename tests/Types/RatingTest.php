<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\{Organization, Person};

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \json_encode;
use function \version_compare;
 
#[CoversClass(\StoreCore\Types\Rating::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class RatingTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Rating class exists')]
    public function testRatingClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Rating.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Rating.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\Rating'));
        $this->assertTrue(class_exists(Rating::class));
    }

    #[TestDox('Rating class is concrete')]
    public function testRatingClassIsConcrete(): void
    {
        $class = new ReflectionClass(Rating::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Rating is Intangible')]
    public function testRatingIsIntangible(): void
    {
        $this->assertInstanceOf(Intangible::class, new Rating());
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Rating is JSON serializable')]
    public function testRatingIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Rating());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Rating::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Rating::VERSION);
        $this->assertIsString(Rating::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Rating::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Rating.alternateName exists')]
    public function testRatingAlternateNameExists(): void
    {
        $this->assertObjectHasProperty('alternateName', new Rating());
    }

    #[TestDox('Rating.alternateName is null by default')]
    public function testRatingAlternateNameIsNullByDefault(): void
    {
        $rating = new Rating();
        $this->assertNull($rating->alternateName);
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/factcheck#example
     */
    #[TestDox('Rating.alternateName accepts string')]
    public function testRatingAlternateNameAcceptsString(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Rating",
            "ratingValue": 1,
            "bestRating": 5,
            "worstRating": 1,
            "alternateName": "False"
          }
        ';

        $reviewRating = new Rating();
        $reviewRating->ratingValue = 1;
        $reviewRating->bestRating = 5;
        $reviewRating->worstRating = 1;

        $reviewRating->alternateName = 'False';
        $this->assertNotNull($reviewRating->alternateName);
        $this->assertIsNotBool($reviewRating->alternateName);
        $this->assertIsString($reviewRating->alternateName);

        $actualJson = json_encode($reviewRating, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[TestDox('Rating.alternateName accepts string')]
    public function testRatingRatingExplanationExists(): void
    {
        $this->assertObjectHasProperty('ratingExplanation', new Rating());
    }


    #[TestDox('Rating.author is null by default')]
    public function testRatingAuthorIsNullByDefault(): void
    {
        $rating = new Rating();
        $this->assertNull($rating->author);
    }

    #[Depends('testRatingAuthorIsNullByDefault')]
    #[TestDox('Rating.author accepts Organization')]
    public function testRatingAuthorAcceptsOrganization(): void
    {
        $rating = new Rating();
        $rating->author = new Organization('IMDB');
        $this->assertNotNull($rating->author);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $rating->author);
        $this->assertEquals('IMDB', $rating->author->name);
    }

    #[Depends('testRatingAuthorIsNullByDefault')]
    #[TestDox('Rating::setAuthor accepts author.name as string')]
    public function testRatingSetAuthorAcceptsAuthorNameAsString(): void
    {
        $rating = new Rating();
        $rating->setAuthor('John Doe');
        $this->assertIsNotString($rating->author);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $rating->author);
        $this->assertEquals('John Doe', $rating->author->name);
    }


    #[TestDox('Rating.ratingExplanation is null by default')]
    public function testRatingRatingExplanationIsNullByDefault(): void
    {
        $rating = new Rating();
        $this->assertNull($rating->ratingExplanation);
    }

    #[TestDox('Rating.ratingExplanation accepts string')]
    public function testRatingRatingExplanationAcceptsString(): void
    {
        $rating = new Rating();
        $rating->worstRating = 75;
        $rating->bestRating = 100;
        $rating->ratingExplanation = 'Bordeaux wines are rated on a scale from 75 to 100.';

        $this->assertNotNull($rating->ratingExplanation);
        $this->assertNotEmpty($rating->ratingExplanation);
        $this->assertIsString($rating->ratingExplanation);
    }


    #[TestDox('Rating.reviewAspect exists')]
    public function testRatingReviewAspectExists(): void
    {
        $rating = new Rating();
        $this->assertObjectHasProperty('reviewAspect', $rating);
    }

    #[TestDox('Rating.reviewAspect is null by default')]
    public function testRatingReviewAspectIsNullByDefault(): void
    {
        $rating = new Rating();
        $this->assertNull($rating->reviewAspect);
    }

    #[TestDox('Rating.reviewAspect accepts string')]
    public function testRatingReviewAspectAcceptsString(): void
    {
        $rating = new Rating();
        $rating->reviewAspect = 'Ambiance';

        $this->assertNotNull($rating->reviewAspect);
        $this->assertNotEmpty($rating->reviewAspect);
        $this->assertIsString($rating->reviewAspect);
    }


    /**
     * @see https://schema.org/Rating#eg-0010
     *      Schema.org `Rating` Example 1
     */
    #[TestDox('Schema.org Example 1')]
    public function testSchemaOrgExample1(): void
    {
        /*
            {
              "@type": "Rating",
              "bestRating": "5",
              "ratingValue": "1",
              "worstRating": "1"
            }
         */

        $reviewRating = new Rating();
        $reviewRating->ratingValue = 1;

        $this->assertEquals('5', $reviewRating->bestRating);
        $this->assertEquals('1', $reviewRating->ratingValue);
        $this->assertEquals('1', $reviewRating->worstRating);

        /*
            {
              "@type": "Rating",
              "bestRating": "5",
              "ratingValue": "4",
              "worstRating": "1"
            }
         */

         $reviewRating = new Rating(4);

         $this->assertEquals('5', $reviewRating->bestRating);
         $this->assertEquals('4', $reviewRating->ratingValue);
         $this->assertEquals('1', $reviewRating->worstRating);
    }

    /**
     * @see https://schema.org/Rating#eg-0248
     *      Schema.org `Rating` Example 2
     */
    #[TestDox('Schema.org Example 2')]
    public function testSchemaOrgExample2(): void
    {
        /*
            {
              "@type": "Rating",
              "ratingValue": 5,
              "worstRating": 1,
              "bestRating": 5,
              "reviewAspect": "Ambiance"
            }
         */

         $reviewRating = new Rating();
         $reviewRating->ratingValue = 5;
         $reviewRating->reviewAspect = 'Ambiance';

         $this->assertEquals(5, $reviewRating->ratingValue);
         $this->assertEquals(1, $reviewRating->worstRating);
         $this->assertEquals(5, $reviewRating->bestRating);
         $this->assertEquals('Ambiance', $reviewRating->reviewAspect);
    }
}
