<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\RequiresPhp;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\Duration::class)]
#[RequiresPhp('>= 8.1.0')]
final class DurationTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Duration class is concrete')]
    public function testDurationClassIsConcrete(): void
    {
        $class = new ReflectionClass(Duration::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Duration is a PHP DateInterval')]
    public function testDurationIsPhpDateInterval(): void
    {
        $stub = $this->createStub(Duration::class);
        $this->assertInstanceOf(\DateInterval::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Duration is JSON serializable')]
    public function testDurationIsJsonSerializable(): void
    {
        $stub = $this->createStub(Duration::class);
        $this->assertInstanceOf(\JsonSerializable::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Duration is stringable')]
    public function testDurationIsStringable(): void
    {
        $stub = $this->createStub(Duration::class);
        $this->assertInstanceOf(\Stringable::class, $stub);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Duration::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Duration::VERSION);
        $this->assertIsString(Duration::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Duration::VERSION, '1.0.0', '>=')
        );
    }

    /**
     * @see https://www.php.net/manual/en/dateinterval.construct.php
     */
    #[TestDox('DateInterval::__construct has limited support for strings')]
    #[RequiresPhp('>= 8.3.0')]
    public function testDurationConstructorAcceptsStrings(): void
    {
        $interval = \DateInterval::createFromDateString('1 hour');
        $this->assertInstanceOf(\DateInterval::class, $interval);
        $this->assertEquals(1, $interval->h);

        $this->expectException(\DateMalformedIntervalStringException::class);
        $failure = new \DateInterval('1 hour');
    }

    /**
     * @see https://www.php.net/manual/en/dateinterval.createfromdatestring.php
     *      DateInterval::createFromDateString
     */
    #[Depends('testDurationIsStringable')]
    #[TestDox('Duration::createFromDateString returns Duration')]
    public function testDurationCreateFromDateStringReturnsDuration(): void
    {
        $duration = Duration::createFromDateString('1 hour');
        $this->assertInstanceOf(\DateInterval::class, $duration);
        $this->assertInstanceOf(Duration::class, $duration);
        $this->assertEquals('PT1H', $duration->__toString());

        $duration = Duration::createFromDateString('15 minutes');
        $this->assertInstanceOf(\DateInterval::class, $duration);
        $this->assertInstanceOf(Duration::class, $duration);
        $this->assertEquals('PT15M', $duration->__toString());
    }
}
