<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Organization;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\EmployerAggregateRating::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class EmployerAggregateRatingTest extends TestCase
{
    #[TestDox('EmployerAggregateRating class is concrete')]
    public function testEmployerAggregateRatingClassIsConcrete(): void
    {
        $class = new ReflectionClass(EmployerAggregateRating::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('EmployerAggregateRating is an AggregateRating')]
    public function testEmployerAggregateRatingIsAggregateRating(): void
    {
        $this->assertInstanceOf(AggregateRating::class, new EmployerAggregateRating());
    }

    #[Group('hmvc')]
    #[TestDox('EmployerAggregateRating is an Intangible Rating')]
    public function testEmployerAggregateRatingIsIntangibleRating(): void
    {
        $rating = new EmployerAggregateRating();
        $this->assertInstanceOf(Intangible::class, $rating);
        $this->assertInstanceOf(Rating::class, $rating);
    }

    #[Group('hmvc')]
    #[TestDox('EmployerAggregateRating is JSON serializable')]
    public function testEmployerAggregateRatingIsJsonSerializable(): void
    {
        $rating = new EmployerAggregateRating();
        $this->assertInstanceOf(\JsonSerializable::class, $rating);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EmployerAggregateRating::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EmployerAggregateRating::VERSION);
        $this->assertIsString(EmployerAggregateRating::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EmployerAggregateRating::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('seo')]
    #[TestDox('Google rich results acceptance test')]
    public function testGoogleRichResultsAcceptanceTest(): void
    {
        /*
            {
              "@context" : "https://schema.org/",
              "@type": "EmployerAggregateRating",
              "itemReviewed": {
                "@type": "Organization",
                "name" : "World's Best Coffee Shop",
                "sameAs" : "https://example.com"
              },
              "ratingValue": "91",
              "bestRating": "100",
              "worstRating": "1",
              "ratingCount" : "10561"
            }
         */
        $expectedJson = '
          {
            "@context" : "https://schema.org",
            "@type": "EmployerAggregateRating",
            "itemReviewed": {
              "@type": "Organization",
              "name": "World\'s Best Coffee Shop",
              "sameAs": "https://example.com"
            },
            "ratingValue": 91,
            "bestRating": 100,
            "worstRating": 1,
            "ratingCount": 10561
          }
        ';

        $organization = new Organization("World's Best Coffee Shop");

        // This SHOULD be the `Organization.url` property, but
        // for now we’ll follow along with the example by Google.
        $organization->sameAs = new URL('https://example.com');

        $rating = new EmployerAggregateRating();
        $rating->itemReviewed = $organization;

        $rating->worstRating =   1;
        $rating->ratingValue =  91;
        $rating->bestRating  = 100;

        $rating->ratingCount = 10561;

        $actualJson = json_encode($rating, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
