<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Types\MonetaryAmount::class)]
#[CoversClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Currencies::class)]
final class MonetaryAmountTest extends TestCase
{
    #[TestDox('MonetaryAmount class is concrete')]
    public function testMonetaryAmountClassIsConcrete(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MonetaryAmount is a structured value')]
    public function testMonetaryAmountIsStructuredValue(): void
    {
        $amount = new MonetaryAmount(29.95);
        $this->assertInstanceOf(\StoreCore\Types\StructuredValue::class, $amount);
    }

    #[Group('hmvc')]
    #[TestDox('MonetaryAmount is JSON serializable')]
    public function testMonetaryAmountIsJsonSerializable(): void
    {
        $amount = new MonetaryAmount(29.95);
        $this->assertInstanceOf(\JsonSerializable::class, $amount);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MonetaryAmount::VERSION);
        $this->assertIsString(MonetaryAmount::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MonetaryAmount::VERSION, '0.2.2', '>=')
        );
    }


    #[TestDox('MonetaryAmount.currency exists')]
    public function testMonetaryAmountCurrencyExists(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasProperty('currency'));
    }

    #[TestDox('MonetaryAmount.currency is non-empty string')]
    public function testMonetaryAmountCurrencyIsNonEmptyString(): void
    {
        $amount = new MonetaryAmount();
        $this->assertNotEmpty($amount->currency);
        $this->assertIsString($amount->currency);
    }

    #[TestDox('MonetaryAmount.currency is "EUR" by default')]
    public function testMonetaryAmountCurrencyPropertyIsEurByDefault(): void
    {
        $amount = new MonetaryAmount();
        $this->assertEquals('EUR', $amount->currency);
    }


    #[TestDox('MonetaryAmount::__construct() exists')]
    public function testMonetaryAmountConstructorExists(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('MonetaryAmount::__construct() is public')]
    public function testMonetaryAmountConstructorIsPublic(): void
    {
        $method = new ReflectionMethod(MonetaryAmount::class, '__construct');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('MonetaryAmount::__construct() has two OPTIONAL parameters')]
    public function testMonetaryAmountConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(MonetaryAmount::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('MonetaryAmount::__construct() sets value property')]
    public function testMonetaryAmountConstructorSetsValueProperty(): void
    {
        $amount = new MonetaryAmount(239);
        $this->assertEquals(239, $amount->value);

        $amount = new MonetaryAmount(19.95);
        $this->assertEquals(19.95, $amount->value);
    }

    #[TestDox('MonetaryAmount::__construct() sets value and currency properties')]
    public function testMonetaryAmountConstructorSetsValueAndCurrencyProperties(): void
    {
        $amount = new MonetaryAmount(4674, 'USD');
        $this->assertEquals(4674, $amount->value);
        $this->assertEquals('USD', $amount->currency);
    }


    #[TestDox('MonetaryAmount::setCurrency() exists')]
    public function testMonetaryAmountSetCurrencyExists(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasMethod('setCurrency'));
    }

    #[TestDox('MonetaryAmount::setCurrency() is public')]
    public function testMonetaryAmountSetCurrencyThroughIsPublic(): void
    {
        $method = new ReflectionMethod(MonetaryAmount::class, 'setCurrency');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('MonetaryAmount::setCurrency() has one REQUIRED parameter')]
    public function testMonetaryAmountSetCurrencyHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(MonetaryAmount::class, 'setCurrency');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('MonetaryAmount::setCurrency() sets ISO currency codes')]
    public function testMonetaryAmountSetCurrencySetsIsoCurrencyCodes(): void
    {
        $currencies = ['CHF', 'DKK', 'EUR', 'GBP', 'SEK'];
        $amount = new MonetaryAmount();

        foreach ($currencies as $currency_code) {
            $amount->setCurrency($currency_code);
            $this->assertEquals($currency_code, $amount->currency);
        }
    }


    #[TestDox('MonetaryAmount.validThrough exists')]
    public function testMonetaryAmountValidThroughExists(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasProperty('validThrough'));
    }

    #[TestDox('MonetaryAmount.validThrough is public')]
    public function testMonetaryAmountValidThroughIsPublic(): void
    {
        $property = new ReflectionProperty(MonetaryAmount::class, 'validThrough');
        $this->assertTrue($property->isPublic());
    }


    #[TestDox('MonetaryAmount.value exists')]
    public function testMonetaryAmountValueExists(): void
    {
        $class = new ReflectionClass(MonetaryAmount::class);
        $this->assertTrue($class->hasProperty('value'));
    }

    #[TestDox('MonetaryAmount.value is public')]
    public function testMonetaryAmountValueIsPublic(): void
    {
        $method = new ReflectionProperty(MonetaryAmount::class, 'value');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('MonetaryAmount.value is null by default')]
    public function testMonetaryAmountValueIsNullByDefault(): void
    {
        $amount = new MonetaryAmount();
        $this->assertNull($amount->value);
    }

    #[TestDox('MonetaryAmount.value accepts float')]
    public function testMonetaryAmountValueAcceptsFloat(): void
    {
        $amount = new MonetaryAmount();
        $amount->value = 19.95;
        $this->assertNotNull($amount->value);
        $this->assertIsFloat($amount->value);
        $this->assertEquals(19.95, $amount->value);
    }

    #[TestDox('MonetaryAmount.value accepts integer')]
    public function testMonetaryAmountValueAcceptsInteger(): void
    {
        $amount = new MonetaryAmount();
        $amount->value = 239;
        $this->assertNotNull($amount->value);
        $this->assertIsInt($amount->value);
        $this->assertEquals(239, $amount->value);
    }
}
