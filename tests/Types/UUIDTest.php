<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \ctype_xdigit;

#[CoversClass(UUID::class)]
#[UsesClass(UUIDFactory::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class UUIDTest extends TestCase
{
    #[TestDox('UUID class is read-only')]
    public function testUUIDClassIsReadOnly(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[TestDox('UUID class is concrete')]
    public function testUUIDClassIsConcrete(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('UUID class is JSON serializable')]
    public function testUUIDClassIsJsonSerializable(): void
    {
        $uuid = new UUID('25ad562a-fcf6-4a41-82a9-6a91aa46a50a');
        $this->assertInstanceOf(\JsonSerializable::class, $uuid);
    }

    #[TestDox('UUID class is stringable')]
    public function testUUIDClassIsStringable(): void
    {
        $uuid = new UUID('499d5218-4038-4a91-b8bf-923baadb2de9');
        $this->assertInstanceOf(\Stringable::class, $uuid);
    }

    #[Group('hmvc')]
    #[TestDox('UUID class implements ValidateInterface')]
    public function testUUIDClassImplementsValidateInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Types\\ValidateInterface'));
        $this->assertTrue(interface_exists(\StoreCore\Types\ValidateInterface::class));

        $uuid = new UUID('45b90433-802c-4604-a29f-0328a5b8ff7c');
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, $uuid);
    }

    #[Group('hmvc')]
    #[TestDox('UUID class implements ValueInterface')]
    public function testUUIDClassImplementsValueInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Types\\ValueInterface'));
        $this->assertTrue(interface_exists(\StoreCore\Types\ValueInterface::class));

        $uuid = new UUID('45b90433-802c-4604-a29f-0328a5b8ff7c');
        $this->assertInstanceOf(\StoreCore\Types\ValueInterface::class, $uuid);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UUID::VERSION);
        $this->assertIsString(UUID::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UUID::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('UUID::__construct() exists')]
    public function testUUIDConstructorExists(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testUUIDConstructorExists')]
    #[TestDox('UUID::__construct() is public constructor')]
    public function testUUIDConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(UUID::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testUUIDConstructorExists')]
    #[TestDox('UUID::__construct() has one REQUIRED parameter')]
    public function testUUIDConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UUID::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUUIDConstructorHasOneRequiredParameter')]
    #[TestDox('UUID::__construct() throws value error exception on invalid UUID')]
    public function testUUIDConstructorThrowValueErrorExceptionOnInvalidUUID()
    {
        $this->expectException(\ValueError::class);
        $uuid = new UUID('612e4e29-ae3b-e0f4-fac1-eb8d4a7ef4');
    }

    #[Depends('testUUIDConstructorHasOneRequiredParameter')]
    #[TestDox('UUID::__construct() accepts Microsoft GUID with surrounding braces')]
    public function testUUIDConstructorAcceptsMicrosoftGuidWithSurroundingBraces(): void
    {
        $uuid = new UUID('{123e4567-e89b-12d3-a456-426652340000}');
        $this->assertNotEquals('{123e4567-e89b-12d3-a456-426652340000}', (string) $uuid);
        $this->assertEquals('123e4567-e89b-12d3-a456-426652340000', (string) $uuid);
    }

    #[Depends('testUUIDConstructorHasOneRequiredParameter')]
    #[TestDox('UUID::__construct() accepts special formats')]
    public function testUUIDConstructorAcceptsSpecialFormats(): void
    {
        $uuid = new UUID('{aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee}');
        $this->assertNotEquals('{aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee}', (string) $uuid);
        $this->assertEquals('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', (string) $uuid);

        $uuid = new UUID('aaaaaaaabbbbccccddddeeeeeeeeeeee');
        $this->assertNotEquals('aaaaaaaabbbbccccddddeeeeeeeeeeee', (string) $uuid);
        $this->assertEquals('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', (string) $uuid);
    }

    #[Depends('testUUIDConstructorHasOneRequiredParameter')]
    #[TestDox('UUID::__construct() accepts RFC 4122 Uniform Resource Name')]
    public function testUUIDConstructorAcceptsRFC4122UniformResourceName(): void
    {
        $uuid = new UUID('urn:uuid:123e4567-e89b-12d3-a456-426655440000');
        $this->assertNotEquals('urn:uuid:123e4567-e89b-12d3-a456-426655440000', (string) $uuid);
        $this->assertEquals('123e4567-e89b-12d3-a456-426655440000', (string) $uuid);

        $uuid = new UUID('urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6');
        $this->assertNotEquals('urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6', (string) $uuid);
        $this->assertEquals('f81d4fae-7dec-11d0-a765-00a0c91e6bf6', (string) $uuid);
    }

    #[TestDox('UUID::__construct() accepts nil UUID')]
    public function testUUIDConstructorAcceptsNilUUID(): void
    {
        $uuid = new UUID('00000000-0000-0000-0000-000000000000');
        $this->assertEquals('00000000-0000-0000-0000-000000000000', (string) $uuid);
    }

    #[TestDox('UUID::__construct() accepts max UUID')]
    public function testUUIDConstructorAcceptsMaxUUID(): void
    {
        $uuid = new UUID('ffffffff-ffff-ffff-ffff-ffffffffffff');
        $this->assertEquals('ffffffff-ffff-ffff-ffff-ffffffffffff', (string) $uuid);
    }


    #[TestDox('UUID::__toString() exists')]
    public function testUUIDToStringExists(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testUUIDToStringExists')]
    #[TestDox('UUID::__toString() is final public')]
    public function testUUIDToStringIsFinalPublic(): void
    {
        $method = new ReflectionMethod(UUID::class, '__toString');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testUUIDToStringExists')]
    #[TestDox('UUID::__toString() returns non-empty string')]
    public function testUUIDToStringReturnsNonEmptyString(): void
    {
        $uuid = new UUID('123e4567-e89b-12d3-a456-426655440000');
        $this->assertNotEmpty((string) $uuid);
        $this->assertIsString((string) $uuid);
    }

    #[Depends('testUUIDToStringReturnsNonEmptyString')]
    #[TestDox('UUID::__toString returns set UUID')]
    public function testUuidToStringReturnsSetUuid(): void
    {
        $uuid = new UUID('123e4567-e89b-12d3-a456-426655440000');
        $this->assertEquals('123e4567-e89b-12d3-a456-426655440000', $uuid->__toString());
        $this->assertEquals('123e4567-e89b-12d3-a456-426655440000', (string) $uuid);
    }


    #[TestDox('UUID::validate() exists')]
    public function testUUIDValidateExists(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->hasMethod('validate'));
    }

    #[Depends('testUUIDValidateExists')]
    #[TestDox('UUID::validate() is public static')]
    public function testUUIDValidateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UUID::class, 'validate');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testUUIDValidateExists')]
    #[TestDox('UUID::validate() has one REQUIRED parameter')]
    public function testUUIDValidateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UUID::class, 'validate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUUIDValidateExists')]
    #[Depends('testUUIDValidateIsPublicStatic')]
    #[Depends('testUUIDValidateHasOneRequiredParameter')]
    #[TestDox('UUID::validate() returns bool on valid or invalid UUID')]
    public function testUUIDValidateReturnsOnValidOrInvalidUUID(): void
    {
        $valid_uuid_v1 = 'bbd6c794-51bd-11ec-bf63-0242ac130002';
        $this->assertTrue(UUID::validate($valid_uuid_v1));

        $valid_uuid_v4 = 'd65f4dd3-5f37-43b2-9c21-eabdf5fe0f15';
        $this->assertTrue(UUID::validate($valid_uuid_v4));

        $invalid_uuid = '612e4e29-ae3b-e0f4-fac1-eb8d4a7ef4';
        $this->assertFalse(UUID::validate($invalid_uuid));


        $valid_microsoft_guid = '{123e4567-e89b-12d3-a456-426652340000}';
        $this->assertTrue(UUID::validate($valid_microsoft_guid));

        $valid_microsoft_guid = '123e4567-e89b-12d3-a456-426652340000';
        $this->assertTrue(UUID::validate($valid_microsoft_guid));

        $invalid_microsoft_guid = '123e4567-e89b-12d3-a456-42665234';
        $this->assertFalse(UUID::validate($invalid_microsoft_guid));


        $valid_uuid_urn = 'urn:uuid:123e4567-e89b-12d3-a456-426655440000';
        $this->assertTrue(UUID::validate($valid_uuid_urn));
    }


    #[TestDox('UUID::valueOf exists')]
    public function testUuidValueOfExists(): void
    {
        $class = new ReflectionClass(UUID::class);
        $this->assertTrue($class->hasMethod('valueOf'));
    }

    #[Depends('testUuidValueOfExists')]
    #[TestDox('UUID::valueOf is final public')]
    public function testUuidValueOfIsFinalPublic(): void
    {
        $method = new ReflectionMethod(UUID::class, 'valueOf');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUuidToStringReturnsSetUuid')]
    #[TestDox('UUID::valueOf returns set UUID as hexadecimal string')]
    public function testUuidValueOfReturnsSetUuidAsHexadecimalString(): void
    {
        $uuid = new UUID('123e4567-e89b-12d3-a456-426655440000');
        $this->assertTrue(ctype_xdigit($uuid->valueOf()));

        $this->assertEquals('123e4567-e89b-12d3-a456-426655440000', $uuid->__toString());
        $this->assertEquals('123e4567e89b12d3a456426655440000', $uuid->valueOf());
    }


    #[TestDox('UUID is JSON serializable')]
    public function testUUIDIsJsonSerializable(): void
    {
        $uuid = new UUID('c3e4ec17-b285-4115-a5bb-b9b9662c2bad');
        $this->assertEquals('"c3e4ec17-b285-4115-a5bb-b9b9662c2bad"', json_encode($uuid));

        $person = new Person('Jane Doe');
        $this->assertFalse($person->hasIdentifier());
        $person->identifier = UUIDFactory::randomUUID();
        $this->assertTrue($person->hasIdentifier());

        $uuid = UUIDFactory::randomUUID();
        $person->setIdentifier($uuid);
        $this->assertTrue($person->hasIdentifier());
        $this->assertSame($uuid, $person->getIdentifier());

        /*
         * Pretty printed JSON example:
         *
         *     {
         *       "@context": "https://schema.org",
         *       "@type": "Person",
         *       "identifier": "62a1fd55-69fd-5942-e672-0241beb52763",
         *       "name": "Jane Doe"
         *     }
         */
        $json = json_encode($person, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"identifier": "' . (string) $uuid . '"', $json);
    }
}
