<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ServiceTypeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ServiceType enumeration exists')]
    public function testServiceTypeEnumerationExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ServiceType.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ServiceType.php');
        $this->assertTrue(enum_exists('\\StoreCore\\Types\\ServiceType'));
        $this->assertTrue(enum_exists(ServiceType::class));
    }


    #[TestDox('ServiceType is an enumeration')]
    public function testServiceTypeIsEnumeration(): void
    {
        $enum = new ReflectionClass(ServiceType::class);
        $this->assertTrue($enum->isEnum());
    }

    #[Depends('testServiceTypeIsEnumeration')]
    #[TestDox('ServiceType is a backed enumeration')]
    public function testServiceTypeIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(ServiceType::class);
        $this->assertTrue($enum->isBacked());
    }

    #[Depends('testServiceTypeIsEnumeration')]
    #[TestDox('ServiceType enumeration has twenty-one (21) cases')]
    public function testServiceTypeEnumerationHasTwentyOneCases(): void
    {
        $this->assertCount(21, ServiceType::cases());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $enum = new ReflectionEnum(ServiceType::class);
        $this->assertTrue($enum->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ServiceType::VERSION);
        $this->assertIsString(ServiceType::VERSION);
    }

    /**
     * @see https://schema.org/docs/releases.html
     *      VERSION constant matches latest Schema.org release
     */
    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches latest Schema.org release')]
    public function testVersionMatchesLatestSchemaOrgRelease(): void
    {
        $this->assertGreaterThanOrEqual('27.0', ServiceType::VERSION);
    }
}
