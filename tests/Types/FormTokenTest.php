<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\FormToken::class)]
#[Group('security')]
class FormTokenTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('FormToken class exists')]
    public function testFormTokenClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'FormToken.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'FormToken.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\FormToken'));
        $this->assertTrue(class_exists(FormToken::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FormToken::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FormToken::VERSION);
        $this->assertIsString(FormToken::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FormToken::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('FormToken::getInstance() exists')]
    public function testFormTokenGetInstanceExists(): void
    {
        $class = new ReflectionClass(FormToken::class);
        $this->assertTrue($class->hasMethod('getInstance'));
    }

    #[TestDox('FormToken::getInstance() is public static')]
    public function testFormTokenGetInstanceIsPublicStatic(): void
    {
        $method = new ReflectionMethod(FormToken::class, 'getInstance');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('FormToken::getInstance() returns non-empty string')]
    public function testFormTokenGetInstanceReturnsNonEmptyString(): void
    {
        $this->assertNotEmpty(FormToken::getInstance());
        $this->assertIsString(FormToken::getInstance());
    }

    #[TestDox('FormToken::getInstance() returns 512 ASCII characters')]
    public function testFormTokenGetInstanceReturns512ASCIICharacters(): void
    {
        $this->assertEquals(512, strlen(FormToken::getInstance()));
        $this->assertEquals(512, mb_strlen(FormToken::getInstance()));

        $this->assertEquals(1, preg_match('/^[0-9a-zA-Z]+$/', FormToken::getInstance()));
        $this->assertEquals(1, preg_match('/^[[:alnum:]]+$/', FormToken::getInstance()));
    }
}
