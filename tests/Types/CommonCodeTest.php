<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(CommonCode::class)]
class CommonCodeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CommonCode class exists')]
    public function testCommonCodeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CommonCode.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'CommonCode.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\CommonCode'));
        $this->assertTrue(class_exists(CommonCode::class));
    }

    #[TestDox('CommonCode class is concrete')]
    public function testCommonCodeClassIsConcrete(): void
    {
        $class = new ReflectionClass(CommonCode::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('CommonCode class is read-only')]
    public function testCommonCodeClassIsReadOnly(): void
    {
        $class = new ReflectionClass(CommonCode::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CommonCode::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CommonCode::VERSION);
        $this->assertIsString(CommonCode::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CommonCode::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('CommonCode::__construct() exists')]
    public function testCommonCodeConstructorExists(): void
    {
        $class = new ReflectionClass(CommonCode::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('CommonCode::__construct() is public constructor')]
    public function testCommonCodeConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(CommonCode::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('CommonCode::__construct() has three parameters')]
    public function testCommonCodeConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(CommonCode::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testCommonCodeConstructorHasThreeParameters')]
    #[TestDox('CommonCode::__construct() has one REQUIRED parameter')]
    public function testCommonCodeConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CommonCode::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CommonCode is stringable')]
    public function testCommonCodeIsStringable(): void
    {
        $common_code = new CommonCode('KGM');
        $this->assertInstanceOf(\Stringable::class, $common_code);

        $this->assertNotEmpty((string) $common_code);
        $this->assertIsString((string) $common_code);
    }

    #[Depends('testCommonCodeIsStringable')]
    #[TestDox('CommonCode::__toString() returns uppercase string')]
    public function testCommonCodeToStringReturnsUppercaseString(): void
    {
        $common_code = new CommonCode('Ltr');
        $this->assertNotEquals('Ltr', (string) $common_code);
        $this->assertEquals('LTR', (string) $common_code);

        $common_code = new CommonCode('kgm');
        $this->assertNotEquals('kgm', (string) $common_code);
        $this->assertEquals('KGM', (string) $common_code);
    }


    #[TestDox('CommonCode examples')]
    public function testCommonCodeExamples(): void
    {
        // Common code only:
        $kilogram = new CommonCode('KGM');
        $this->assertSame('KGM', $kilogram->value);
        $this->assertEmpty($kilogram->unitOfMeasurement);
        $this->assertEmpty($kilogram->name);

        // Common code, Système International (SI) symbol, and name:
        $kilogram = new CommonCode('KGM', 'kg', 'kilogram');
        $this->assertEquals('KGM', $kilogram->value);
        $this->assertEquals('kg', $kilogram->unitOfMeasurement);
        $this->assertEquals('kilogram', $kilogram->name);

        // Most common codes consist of 3 characters, but there are important exceptions:
        $decibel = new CommonCode('2N', 'dB', 'decibel');
        $this->assertEquals('2N', $decibel->value);

        // Special symbols are supported:
        $litre = new CommonCode('LTR', 'ℓ', 'litre');
        $this->assertNotEquals('l', $litre->unitOfMeasurement);
        $this->assertEquals('ℓ', $litre->unitOfMeasurement);
    }
}
