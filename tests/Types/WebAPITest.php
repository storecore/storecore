<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Audience;
use StoreCore\CRM\Organization;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Types\WebAPI::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class WebAPITest extends TestCase
{
    #[TestDox('WebAPI class is concrete')]
    public function testWebAPIClassIsConcrete(): void
    {
        $class = new ReflectionClass(WebAPI::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('WebAPI is a Service')]
    public function testWebAPIIsService(): void
    {
        $this->assertInstanceOf(Service::class, new WebAPI());
    }

    #[Group('hmvc')]
    #[TestDox('WebAPI is an Intangible Thing')]
    public function testWebAPIIsIntangibleThing(): void
    {
        $api = new WebAPI();
        $this->assertInstanceOf(Intangible::class, $api);
        $this->assertInstanceOf(Thing::class, $api);
    }

    #[Group('hmvc')]
    #[TestDox('WebAPI is JSON serializable')]
    public function testWebAPIIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new WebAPI());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WebAPI::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WebAPI::VERSION);
        $this->assertIsString(WebAPI::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WebAPI::VERSION, '0.1.1', '>=')
        );
    }


    #[TestDox('WebAPI.audience exists')]
    public function testWebAPIWebAPIExists(): void
    {
        $class = new ReflectionClass(WebAPI::class);
        $this->assertTrue($class->hasProperty('audience'));
    }

    #[TestDox('WebAPI.audience is public')]
    public function testWebAPIWebAPIIsPublic(): void
    {
        $property = new ReflectionProperty(WebAPI::class, 'audience');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('WebAPI.audience is null by default')]
    public function testWebAPIWebAPIIsNullByDefault(): void
    {
        $api = new WebAPI();
        $this->assertNull($api->audience);
    }

    #[Group('hmvc')]
    #[TestDox('WebAPI.audience accepts Audience')]
    public function testWebAPIAudienceAcceptsAudience(): void
    {
        $api = new WebAPI('Carpet cleaning');
        $api->audience = new Audience('Home owners');
        $this->assertNotNull($api->audience);
        $this->assertIsObject($api->audience);
        $this->assertEquals('Home owners', $api->audience->name);
    }


    #[TestDox('WebAPI.provider exists')]
    public function testWebAPIProviderExists(): void
    {
        $class = new ReflectionClass(WebAPI::class);
        $this->assertTrue($class->hasProperty('provider'));
    }

    #[TestDox('WebAPI.provider is public')]
    public function testWebAPIProviderIsPublic(): void
    {
        $property = new ReflectionProperty(WebAPI::class, 'provider');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('WebAPI.provider is null by default')]
    public function testWebAPIProviderIsNullByDefault(): void
    {
        $api = new WebAPI();
        $this->assertNull($api->provider);
    }

    #[TestDox('WebAPI.provider accepts Organization')]
    public function testWebAPIProviderAcceptsOrganization(): void
    {
        $provider = new Organization();
        $provider->name = 'StoreCore';

        $api = new WebAPI();
        $api->name = 'StoreCore API';
        $api->provider = $provider;

        $this->assertNotNull($api->provider);
        $this->assertIsObject($api->provider);
        $this->assertInstanceOf(\JsonSerializable::class, $api->provider);
        $this->assertSame('StoreCore', $api->provider->name);
    }

    /**
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "WebAPI",
     *   "name": "Google Knowledge Graph Search API",
     *   "description": "The Knowledge Graph Search API lets you find entities in the Google Knowledge Graph. The API uses standard schema.org types and is compliant with the JSON-LD specification.",
     *   "documentation": "https://developers.google.com/knowledge-graph/",
     *   "termsOfService": "https://developers.google.com/knowledge-graph/terms",
     *   "provider": {
     *     "@type": "Organization",
     *     "name": "Google Inc."
     *   }
     * }
     * ```
     *
     * @see https://schema.org/WebAPI#eg-0239 Schema.org `WebAPI` Example 1
     */
    #[TestDox('WebAPI example')]
    public function testWebApiExample(): void
    {
        $api = new WebAPI('Google Knowledge Graph Search API');
        $this->assertEquals('Google Knowledge Graph Search API', $api->name);

        $api->description = 'The Knowledge Graph Search API lets you find entities in the Google Knowledge Graph. The API uses standard schema.org types and is compliant with the JSON-LD specification.';
        $api->documentation = new URL('https://developers.google.com/knowledge-graph/');
        $api->termsOfService = new URL('https://developers.google.com/knowledge-graph/terms');
        $api->provider = new Organization('Google Inc.');
        $this->assertEquals('Google Inc.', $api->provider->name);

        $expected = '
            {
              "@context": "https://schema.org",
              "@type": "WebAPI",
              "name": "Google Knowledge Graph Search API",
              "description": "The Knowledge Graph Search API lets you find entities in the Google Knowledge Graph. The API uses standard schema.org types and is compliant with the JSON-LD specification.",
              "documentation": "https://developers.google.com/knowledge-graph/",
              "termsOfService": "https://developers.google.com/knowledge-graph/terms",
              "provider": {
                "@type": "Organization",
                "name": "Google Inc."
              }
            }
        ';

        $json = json_encode($api, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertNotFalse($json);
        $this->assertJsonStringEqualsJsonString($expected, $json);
    }
}
