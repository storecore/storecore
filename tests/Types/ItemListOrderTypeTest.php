<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\ItemListOrderType::class)]
final class ItemListOrderTypeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ItemListOrderType enumeration exists')]
    public function testItemListOrderTypeEnumerationExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ItemListOrderType.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'ItemListOrderType.php');
        $this->assertTrue(enum_exists('\\StoreCore\\Types\\ItemListOrderType'));
        $this->assertTrue(enum_exists(ItemListOrderType::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionEnum(ItemListOrderType::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ItemListOrderType::VERSION);
        $this->assertIsString(ItemListOrderType::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org version')]
    public function testVersionMatchesSchemaOrgVersion(): void
    {
        $this->assertGreaterThanOrEqual('23.0', ItemListOrderType::VERSION);
    }
}
