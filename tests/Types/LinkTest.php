<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Types\Link::class)]
final class LinkTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Implemented PSR-13 LinkInterface exists')]
    public function testImplementedPSR13LinkInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Link\\LinkInterface'));
        $this->assertTrue(interface_exists(\Psr\Link\LinkInterface::class));
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('LinkClassImplementsPSR13LinkInterface')]
    public function testLinkImplementsPsr13PsrLinkLinkInterface(): void
    {
        $this->assertInstanceOf(\Psr\Link\LinkInterface::class, new Link());
    }

    #[Group('hmvc')]
    #[TestDox('Link is stringable')]
    public function testLinkIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Link());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Link::VERSION);
        $this->assertIsString(Link::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Link::VERSION, '0.3.0', '>=')
        );
    }



    #[TestDox('Link::__construct() exists')]
    public function testLinkConstructorExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Link::__construct() is public constructor')]
    public function testLinkConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Link::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Link::__construct() has three OPTIONAL parameters')]
    public function testLinkConstructHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(Link::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Link::__construct() sets `href` attribute')]
    public function testLinkConstructorSetsHrefAttribute(): void
    {
        $link = new Link('http://www.example.com/');
        $this->assertSame('http://www.example.com/', $link->getHref());

        $link = new Link('https://shopping.example.com/');
        $this->assertSame('https://shopping.example.com/', $link->getHref());
    }

    #[TestDox('Link::__construct() sets `rel` attribute')]
    public function testLinkConstructorSetsRelAttribute(): void
    {
        $link = new Link('https://fonts.googleapis.com/icon?family=Material+Icons', 'stylesheet');
        $this->assertSame(array('stylesheet'), $link->getRels());
    }

    #[TestDox('Link::__construct() sets multiple `rel` attributes as array')]
    public function testLinkConstructorSetsMultipleRelAttributesAsArray(): void
    {
        $link = new Link('https://www.janedoe.com/');
        $this->assertIsArray($link->getRels());
        $this->assertCount(0, $link->getRels());

        $rels = array('author', 'copyright');
        $link = new Link('https://www.janedoe.com/', $rels);
        $this->assertIsArray($link->getRels());
        $this->assertCount(2, $link->getRels());
        $this->assertSame($rels, $link->getRels());
    }

    #[TestDox('Link::__construct() sets multiple `rel` attributes as string')]
    public function testLinkConstructorSetsMultipleRelAttributesAsString(): void
    {
        $link = new Link('https://www.janedoe.com/', 'author copyright');
        $this->assertIsArray($link->getRels());
        $this->assertCount(2, $link->getRels());
    }

    #[TestDox('Link::__construct() sets other HTML attributes')]
    public function testLinkConstructorSetsOtherHtmlAttributes(): void
    {
        // <link crossorigin="anonymous" href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        $link = new Link(
            '//fonts.googleapis.com/icon?family=Material+Icons', 
            'stylesheet',
            array('crossorigin' => 'anonymous')
        );

        $this->assertIsArray($link->getAttributes());
        $this->assertArrayHasKey('crossorigin', $link->getAttributes());
        $this->assertEquals('anonymous', $link->getAttributes()['crossorigin']);
    }


    #[TestDox('Link::__set() exists')]
    public function testLinkSetExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('__set'));
    }

    #[TestDox('Link::__set() is public')]
    public function testLinkSetIsPublic(): void
    {
        $method = new ReflectionMethod(Link::class, '__set');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Link::__set() has two REQUIRED parameters')]
    public function testLinkSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Link::class, '__set');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Link::__toString() exists')]
    public function testLinkToStringExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('Link::__toString() is public')]
    public function testLinkToStringIsPublic(): void
    {
        $method = new ReflectionMethod(Link::class, '__toString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Link:__toString() returns <link> HTML tag')]
    public function testLinkToStringReturnsLinkHtmlTag(): void
    {
        $link = new Link('https://shopping.example.com/');
        $this->assertEquals('<link href="https://shopping.example.com/">', (string) $link);
    }

    #[TestDox('Link:__toString() returns <link> element with rel attribute')]
    public function testLinkToStringReturnsLinkElementWithRelAttribute(): void
    {
        $link = new Link('https://fonts.googleapis.com/icon?family=Material+Icons', 'stylesheet');
        $link = (string) $link;
        $this->assertStringContainsString(' rel="stylesheet"', $link);
        $this->assertEquals('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">', $link);
    }


    #[TestDox('Link::getAttributes() exists')]
    public function testLinkGetAttributesExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('getAttributes'));
    }

    #[TestDox('Link::getAttributes() is public')]
    public function testLinkGetAttributesIsPublic(): void
    {
        $method = new ReflectionMethod(Link::class, 'getAttributes');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Link::getAttributes() has no parameters')]
    public function testLinkGetAttributesHasNoParameters(): void
    {
        $method = new ReflectionMethod(Link::class, 'getAttributes');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Link::getAttributes() returns array')]
    public function testLinkGetAttributesReturnsArray(): void
    {
        $link = new Link();
        $this->assertIsArray($link->getAttributes());
    }

    #[Depends('testLinkGetAttributesReturnsArray')]
    #[TestDox('Link::getAttributes() returns non-empty array by default')]
    public function testLinkGetAttributesReturnsNonEmptyArrayByDefault(): void
    {
        $link = new Link();
        $this->assertNotEmpty($link->getAttributes());

        $link = new Link('https://www.apple.com/nl/store');
        $this->assertNotEmpty($link->getAttributes());
    }


    #[TestDox('Link::getHref() exists')]
    public function testLinkGetHrefExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('getHref'));
    }

    #[TestDox('Link::getHref() is public')]
    public function testLinkGetHrefIsPublic(): void
    {
        $method = new ReflectionMethod(Link::class, 'getHref');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Link::getHref() has no parameters')]
    public function testLinkGetHrefHasNoParameters(): void
    {
        $method = new ReflectionMethod(Link::class, 'getHref');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Link::getHref() returns string')]
    public function testLinkGetHrefReturnsString(): void
    {
        $link = new Link();
        $this->assertIsString($link->getHref());
    }

    #[TestDox('Link::getHref() returns set URL')]
    public function testLinkGetHrefReturnsSetUrl(): void
    {
        $link = new Link();
        $link->setAttribute('href', 'https://www.storecore.io/');
        $this->assertSame('https://www.storecore.io/', $link->getHref());

        $link = new Link('https://gitlab.com/storecore');
        $this->assertSame('https://gitlab.com/storecore', $link->getHref());
    }


    #[TestDox('Link::setAttribute() exists')]
    public function testLinkSetAttributeExists(): void
    {
        $class = new ReflectionClass(Link::class);
        $this->assertTrue($class->hasMethod('setAttribute'));
    }

    #[TestDox('Link::setAttribute() is public')]
    public function testLinkSetAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(Link::class, 'setAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Link::setAttribute() has two REQUIRED parameters')]
    public function testLinkSetAttributeHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Link::class, 'setAttribute');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Link::setAttribute() can set `href` attribute')]
    public function testLinkSetAttributeCanSetHrefAttribute(): void
    {
        $link = new Link('https://www.example.com/');
        $link->setAttribute('href', 'https://shop.example.be/fr/');

        $this->assertEquals('https://shop.example.be/fr/', $link->getHref());

        $link = (string) $link;
        $this->assertStringNotContainsString('href="https://www.example.com/"', $link);
        $this->assertStringContainsString('href="https://shop.example.be/fr/"', $link);
    }
}
