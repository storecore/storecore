<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\Time::class)]
final class TimeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Time class exists')]
    public function testTimeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Time.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Time.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\Time'));
        $this->assertTrue(class_exists(Time::class));
    }

    #[TestDox('Time class is concrete')]
    public function testTimeClassIsConcrete(): void
    {
        $class = new ReflectionClass(Time::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Time implements PHP DateTimeInterface')]
    public function testTimeImplementsPHPDateInterface(): void
    {
        $stub = $this->createStub(Time::class);
        $this->assertInstanceOf(\DateTimeInterface::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Time is JSON serializable')]
    public function testTimeIsJsonSerializable(): void
    {
        $stub = $this->createStub(Time::class);
        $this->assertInstanceOf(\JsonSerializable::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Time is stringable')]
    public function testTimeIsStringable(): void
    {
        $stub = $this->createStub(Time::class);
        $this->assertInstanceOf(\Stringable::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('Time is a PHP DateTime')]
    public function testTimeIsPHPDateTime(): void
    {
        $stub = $this->createStub(Time::class);
        $this->assertInstanceOf(\DateTime::class, $stub);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Time::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Time::VERSION);
        $this->assertIsString(Time::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Time::VERSION, '1.0.0', '>=')
        );
    }
}
