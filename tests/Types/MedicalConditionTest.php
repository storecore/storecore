<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\MedicalCondition::class)]
final class MedicalConditionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('MedicalCondition class exists')]
    public function testMedicalConditionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'MedicalCondition.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'MedicalCondition.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\MedicalCondition'));
        $this->assertTrue(class_exists(MedicalCondition::class));
    }

    #[TestDox('MedicalCondition class is concrete')]
    public function testMedicalConditionClassIsConcrete(): void
    {
        $class = new ReflectionClass(MedicalCondition::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MedicalCondition is a MedicalEntity Thing')]
    public function testMedicalConditionIsMedicalEntityThing(): void
    {
        $condition = new MedicalCondition();
        $this->assertInstanceOf(MedicalEntity::class, $condition);
        $this->assertInstanceOf(Thing::class, $condition);
    }

    #[Group('hmvc')]
    #[TestDox('MedicalCondition is JSON serializable')]
    public function testMedicalConditionIsJsonSerializable()
    {
        $this->assertInstanceOf(\JsonSerializable::class, new MedicalCondition());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MedicalCondition::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MedicalCondition::VERSION);
        $this->assertIsString(MedicalCondition::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MedicalCondition::VERSION, '0.1.0', '>=')
        );
    }
}
