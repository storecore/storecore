<?php

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\MedicalEntity::class)]
final class MedicalEntityTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('MedicalEntity class exists')]
    public function testMedicalEntityClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'MedicalEntity.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'MedicalEntity.php');
        $this->assertTrue(class_exists('\\StoreCore\\Types\\MedicalEntity'));
        $this->assertTrue(class_exists(MedicalEntity::class));
    }

    #[TestDox('MedicalEntity class is concrete')]
    public function testMedicalEntityClassIsConcrete(): void
    {
        $class = new ReflectionClass(MedicalEntity::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MedicalEntity is Schema.org Thing')]
    public function testMedicalEntityIsASchemaOrgThing(): void
    {
        $this->assertInstanceOf(Thing::class, new MedicalEntity());
    }

    #[Group('hmvc')]
    #[TestDox('MedicalEntity is JSON serializable')]
    public function testMedicalEntityIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new MedicalEntity());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MedicalEntity::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MedicalEntity::VERSION);
        $this->assertIsString(MedicalEntity::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MedicalEntity::VERSION, '0.1.0', '>=')
        );
    }
}
