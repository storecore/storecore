<?php

declare(strict_types=1);

namespace StoreCore\Types; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Types\Event::class)]
final class EventTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Event class exists')]
    public function testEventClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Event.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Types' . DIRECTORY_SEPARATOR . 'Event.php');

        $this->assertTrue(class_exists('\\StoreCore\\Types\\Event'));
        $this->assertTrue(class_exists(Event::class));
    }

    #[TestDox('Event class is concrete')]
    public function testEventClassIsConcrete(): void
    {
        $class = new ReflectionClass(Event::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Event is a Thing')]
    public function testEventIsThing(): void
    {
        $event = new Event();
        $this->assertInstanceOf(Thing::class, $event);
    }


    #[Group('hmvc')]
    #[TestDox('Event is JSON serializable')]
    public function testEventIsJsonSerializable(): void
    {
        $event = new Event();
        $this->assertInstanceOf(\JsonSerializable::class, $event);
    }

    #[Group('hmvc')]
    #[TestDox('Event is stringable')]
    public function testEventIsStringable(): void
    {
        $event = new Event();
        $this->assertInstanceOf(\Stringable::class, $event);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Event::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Event::VERSION);
        $this->assertIsString(Event::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Event::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('Event.endTime exists')]
    public function testEventEndTimeExists(): void
    {
        $event = new Event();
        $this->assertObjectHasProperty('endTime', $event);
    }

    #[TestDox('Event.endTime is null by default')]
    public function testEventEndTimeIsNullByDefault(): void
    {
        $event = new Event();
        $this->assertNull($event->endTime);
    }


    #[TestDox('Event.startTime exists')]
    public function testEventStartTimeExists(): void
    {
        $event = new Event();
        $this->assertObjectHasProperty('startTime', $event);
    }

    #[TestDox('Event.startTime is null by default')]
    public function testEventStartTimeIsNullByDefault(): void
    {
        $event = new Event();
        $this->assertNull($event->startTime);
    }
}
