<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\CRM\{Organization, Person};
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\BrokerOrder::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class BrokerOrderTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('BrokerOrder class is concrete')]
    public function testBrokerOrderClassIsConcrete(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('BrokerOrder is an Order')]
    public function testBrokerOrderIsOrder(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertTrue($class->isSubclassOf(Order::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BrokerOrder::VERSION);
        $this->assertIsString(BrokerOrder::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BrokerOrder::VERSION, '1.0.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('BrokerOrder::$brokerIdentifier exists')]
    public function testBrokerOrderBrokerIdentifierExists(): void
    {
        $registry = Registry::getInstance();
        $brokerOrder = new BrokerOrder($registry);
        $this->assertObjectHasProperty('brokerIdentifier', $brokerOrder);
    }

    #[Depends('testBrokerOrderBrokerIdentifierExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::$brokerIdentifier is not public')]
    public function testBrokerOrderBrokerIdentifierIsNotPublic(): void
    {
        $property = new ReflectionProperty(BrokerOrder::class, 'brokerIdentifier');
        $this->assertFalse($property->isPublic());
    }

    #[Group('hmvc')]
    #[TestDox('BrokerOrder::getBrokerIdentifier exists')]
    public function testBrokerOrderGetBrokerIdentifierExists(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertTrue($class->hasMethod('getBrokerIdentifier'));
    }

    #[Depends('testBrokerOrderGetBrokerIdentifierExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::getBrokerIdentifier is public')]
    public function testBrokerOrderGetBrokerIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'getBrokerIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrokerOrderGetBrokerIdentifierExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::getBrokerIdentifier has no parameters')]
    public function testBrokerOrderSetBrokerIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'getBrokerIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBroker exists')]
    public function testBrokerOrderSetBrokerExists(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertTrue($class->hasMethod('setBroker'));
    }

    #[Depends('testBrokerOrderSetBrokerExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBroker is public')]
    public function testBrokerOrderSetBrokerIsPublic(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'setBroker');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrokerOrderSetBrokerExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBroker has one REQUIRED parameter')]
    public function testBrokerOrderSetBrokerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'setBroker');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrokerOrderSetBrokerExists')]
    #[Depends('testBrokerOrderSetBrokerIsPublic')]
    #[Depends('testBrokerOrderSetBrokerHasOneRequiredParameter')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBroker accepts Organization')]
    public function testBrokerOrderSetBrokerAcceptsOrganization(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $broker = new Organization();
        $broker->identifier = '75c5f735-9c96-4dcf-a8dc-ba178a5a29e4';
        $broker->name = 'Amazon Deutschland';
        $broker->url = 'https://www.amazon.de/';

        $order = new BrokerOrder($registry);
        $order->setBroker($broker);

        $this->assertNotNull($order->getBrokerIdentifier());
        $this->assertNotEmpty($order->getBrokerIdentifier());
        $this->assertIsObject($order->getBrokerIdentifier());
        $this->assertInstanceOf(UUID::class, $order->getBrokerIdentifier());
        $this->assertSame($broker->getIdentifier(), $order->getBrokerIdentifier());
        $this->assertSame('75c5f735-9c96-4dcf-a8dc-ba178a5a29e4', (string) $order->getBrokerIdentifier());
    }

    #[Depends('testBrokerOrderSetBrokerAcceptsOrganization')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBroker throws \DomainException on unknown broker')]
    public function testBrokerOrderSetBrokerThrowsDomainExceptionOnUnknownBroker(): void
    {
        $broker = new Organization();
        $broker->name = 'Amazon Deutschland';
        $broker->url = 'https://www.amazon.de/';
        $this->assertNull($broker->identifier);

        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new BrokerOrder($registry);
        $this->expectException(\DomainException::class);
        $order->setBroker($broker);
    }


    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBrokerIdentifier exists')]
    public function testBrokerOrderSetBrokerIdentifierExists(): void
    {
        $class = new ReflectionClass(BrokerOrder::class);
        $this->assertTrue($class->hasMethod('setBrokerIdentifier'));
    }

    #[Depends('testBrokerOrderSetBrokerIdentifierExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBrokerIdentifier is public')]
    public function testBrokerOrderSetBrokerIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'setBrokerIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrokerOrderSetBrokerIdentifierExists')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBrokerIdentifier has one REQUIRED parameter')]
    public function testBrokerOrderSetBrokerIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BrokerOrder::class, 'setBrokerIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrokerOrderSetBrokerIdentifierExists')]
    #[Depends('testBrokerOrderSetBrokerIdentifierIsPublic')]
    #[Depends('testBrokerOrderSetBrokerIdentifierHasOneRequiredParameter')]
    #[Group('hmvc')]
    #[TestDox('BrokerOrder::setBrokerIdentifier accepts UUID as value object')]
    public function testBrokerOrderSetBrokerIdentifierAcceptsUUIDAsValueObject(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();
        $this->assertIsObject($uuid);

        $order = new BrokerOrder($registry);
        $order->setBrokerIdentifier($uuid);
        $this->assertSame($uuid, $order->getBrokerIdentifier());
    }

    #[Depends('testBrokerOrderSetBrokerIdentifierAcceptsUUIDAsValueObject')]
    #[TestDox('BrokerOrder::setBrokerIdentifier accepts UUID as string')]
    public function testBrokerOrderSetBrokerIdentifierAcceptsUUIDAsString(): void
    {
        $registry = Registry::getInstance();
        $order = new BrokerOrder($registry);
        $order->setBrokerIdentifier('75c5f735-9c96-4dcf-a8dc-ba178a5a29e4');
        $this->assertSame('75c5f735-9c96-4dcf-a8dc-ba178a5a29e4', (string) $order->getBrokerIdentifier());
    }
}
