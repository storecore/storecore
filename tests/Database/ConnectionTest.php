<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017, 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\Connection::class)]
class ConnectionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Connection class is concrete')]
    public function testConnectionClassIsConcrete(): void
    {
        $class = new ReflectionClass(Connection::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Database connection extends \PDO')]
    public function testDatabaseConnectionExtendsPdo(): void
    {
        $class = new ReflectionClass(Connection::class);
        $this->assertTrue($class->isSubclassOf(\PDO::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-3 LoggerAwareInterface exists')]
    public function testImplementedPSR3LoggerAwareInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Log\\LoggerAwareInterface'));
    }

    /**
     * @see https://github.com/php-fig/log/blob/master/src/LoggerAwareInterface.php
     */
    #[Depends('testImplementedPSR3LoggerAwareInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Database connection implements PSR-3 LoggerAwareInterface')]
    public function testDatabaseConnectionImplementsPsr3LoggerAwareInterface(): void
    {
        $class = new ReflectionClass(Connection::class);
        $this->assertTrue($class->hasMethod('setLogger'));
        $this->assertTrue($class->implementsInterface(\Psr\Log\LoggerAwareInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Connection::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Connection::VERSION);
        $this->assertIsString(Connection::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Connection::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('Connection::__construct exists')]
    public function testConnectionConstructorExists(): void
    {
        $class = new ReflectionClass(Connection::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testConnectionConstructorExists')]
    #[TestDox('Connection::__construct is public constructor')]
    public function testConnectionConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Connection::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testConnectionConstructorExists')]
    #[TestDox('Connection::__construct has three OPTIONAL parameters')]
    public function testConnectionConstructorHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(Connection::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }
}
