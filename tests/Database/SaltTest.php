<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2018, 2020, 2023–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[Group('security')]
#[CoversClass(\StoreCore\Database\Salt::class)]
final class SaltTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Salt class exists')]
    public function testSaltClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'Salt.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'Salt.php');

        $this->assertTrue(class_exists('\\StoreCore\\Database\\Salt'));
        $this->assertTrue(class_exists(Salt::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Salt::VERSION);
        $this->assertIsString(Salt::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Salt::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('CHARACTER_SET class constant is defined')]
    public function testCharacterSetConstantIsDefined(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertTrue($class->hasConstant('CHARACTER_SET'));
    }

    #[Depends('testCharacterSetConstantIsDefined')]
    #[TestDox('CHARACTER_SET is non-empty string')]
    public function testCharacterSetIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Salt::CHARACTER_SET);
        $this->assertIsString(Salt::CHARACTER_SET);
    }


    #[Group('hmvc')]
    #[TestDox('Salt class is concrete')]
    public function testSaltClassIsConcrete(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[TestDox('Salt::__construct exists')]
    public function testSaltConstructorExists(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testSaltConstructorExists')]
    #[TestDox('Salt::__construct is public constructor')]
    public function testSaltConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Salt::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testSaltConstructorExists')]
    #[TestDox('Salt::__construct has one OPTIONAL parameter')]
    public function testSaltConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Salt::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testSaltConstructorHasOneOptionalParameter')]
    #[TestDox('Salt::__construct parameter determines salt string length')]
    public function testSaltConstructorParameterDeterminesSaltLength(): void
    {
        $salt = new Salt(8);
        $this->assertEquals(8, strlen($salt->__toString()));
        $this->assertEquals(8, mb_strlen($salt->__toString()));

        $salt = new Salt(42);
        $this->assertEquals(42, strlen($salt->__toString()));
        $this->assertEquals(42, mb_strlen($salt->__toString()));
    }


    #[TestDox('Salt::__toString exists')]
    public function testSaltToStringExists(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testSaltToStringExists')]
    #[TestDox('Salt is read-only stringable')]
    public function testSaltIsStringable(): void
    {
        $class = new ReflectionClass(Salt::class);
        $this->assertTrue($class->isReadOnly());

        $this->assertInstanceOf(\Stringable::class, new Salt());
    }

    #[Depends('testSaltConstructorHasOneOptionalParameter')]
    #[Depends('testSaltIsStringable')]
    #[TestDox('Salt::__construct returns 255 characters by default')]
    public function testSaltGetInstanceReturns255CharactersByDefault(): void
    {
        $salt = new Salt();
        $salt = (string) $salt;
        $this->assertEquals(255, strlen($salt));
    }

    #[Depends('testSaltConstructorHasOneOptionalParameter')]
    #[Depends('testSaltIsStringable')]
    #[TestDox('Salt::__construct creates at least two (2) characters')]
    public function testSaltConstructorCreatesAtLeastTwoCharacters(): void
    {
        $salt = (string) new Salt();
        $this->assertGreaterThanOrEqual(2, strlen($salt));

        $salt = (string) new Salt(0);
        $this->assertGreaterThanOrEqual(2, strlen($salt));

        $salt = (string) new Salt(2);
        $this->assertGreaterThanOrEqual(2, strlen($salt));
    }
}
