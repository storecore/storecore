<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\CMS\AbstractCreativeWork;
use StoreCore\CMS\TechArticle;
use StoreCore\CMS\ThreeDModel;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\CreativeWorkTypes::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[UsesClass(\StoreCore\CMS\TechArticle::class)]
#[UsesClass(\StoreCore\CMS\ThreeDModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class CreativeWorkTypesTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    
        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_creative_work_types`');
        } catch (PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('distro')]
    #[TestDox('CreativeWorkTypes class file exists')]
    public function testCreativeWorkTypesClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkTypes.php'
        );
    }

    #[Depends('testCreativeWorkTypesClassFileExists')]
    #[Group('distro')]
    #[TestDox('CreativeWorkTypes class file is readable')]
    public function testCreativeWorkTypesClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkTypes.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('CreativeWorkTypes is a database model')]
    public function testCreativeWorkTypesIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CreativeWorkTypes::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkTypes::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkTypes::VERSION);
        $this->assertIsString(CreativeWorkTypes::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkTypes::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWorkTypes::fromCreativeWork() exists')]
    public function testCreativeWorkTypesFromCreativeWork(): void
    {
        $class = new ReflectionClass(CreativeWorkTypes::class);
        $this->assertTrue($class->hasMethod('fromCreativeWork'));
    }

    #[Depends('testCreativeWorkTypesFromCreativeWork')]
    #[TestDox('CreativeWorkTypes::fromCreativeWork() is public static')]
    public function testCreativeWorkTypesFromCreativeWorkIsPublicStatic(): void
    {
        $method = new ReflectionMethod(CreativeWorkTypes::class, 'fromCreativeWork');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testCreativeWorkTypesFromCreativeWork')]
    #[TestDox('CreativeWorkTypes::fromCreativeWork() has one REQUIRED parameter')]
    public function testCreativeWorkTypesFromCreativeWorkHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CreativeWorkTypes::class, 'fromCreativeWork');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCreativeWorkTypesFromCreativeWorkIsPublicStatic')]
    #[Depends('testCreativeWorkTypesFromCreativeWorkHasOneRequiredParameter')]
    #[TestDox('CreativeWorkTypes::fromCreativeWork() accepts `CreativeWork`s and returns `UNSIGNED TINYINT`s')]
    public function testCreativeWorkTypesFromCreativeWorkAcceptsCreativeWorksAndReturnsUnsignedTinyIntegers(): void
    {
        $manual = new TechArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $manual);
        $this->assertIsInt(CreativeWorkTypes::fromCreativeWork($manual));
        $this->assertGreaterThan(0, CreativeWorkTypes::fromCreativeWork($manual));
        $this->assertLessThan(256, CreativeWorkTypes::fromCreativeWork($manual));

        // Not a CreativeWork
        $object = new \stdClass();
        $this->expectException(\TypeError::class);
        $failure = CreativeWorkTypes::fromCreativeWork($object);
    }

    #[Depends('testCreativeWorkTypesFromCreativeWorkAcceptsCreativeWorksAndReturnsUnsignedTinyIntegers')]
    #[TestDox('CreativeWorkTypes::fromCreativeWork() accepts ThreeDModel')]
    public function testCreativeWorkTypesFromCreativeWorkAcceptsThreeDModel(): void
    {
        // Special case included because PHP does not allow the Schema.org class name `3DModel`.
        $model = new ThreeDModel();
        $this->assertInstanceOf(AbstractCreativeWork::class, $model);
        $this->assertIsInt(CreativeWorkTypes::fromCreativeWork($model));
        $this->assertGreaterThan(0, CreativeWorkTypes::fromCreativeWork($model));
        $this->assertLessThan(256, CreativeWorkTypes::fromCreativeWork($model));
    }
}
