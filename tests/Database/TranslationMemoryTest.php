<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Types\LanguageCode;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\TranslationMemory::class)]
#[CoversClass(\StoreCore\Types\LanguageCode::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class TranslationMemoryTest extends TestCase
{   
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    }

    #[Group('distro')]
    #[TestDox('TranslationMemory class is concrete')]
    public function testTranslationMemoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TranslationMemory is a database model')]
    public function testTranslationMemoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));

    }

    #[Group('hmvc')]
    #[TestDox('TranslationMemory is countable')]
    public function testTranslationMemoryIsCountable(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TranslationMemory::VERSION);
        $this->assertIsString(TranslationMemory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TranslationMemory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('TranslationMemory::count exists')]
    public function testTranslationMemoryCountExists(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('TranslationMemory::count is public')]
    public function testTranslationMemoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('TranslationMemory::count has no parameters')]
    public function testTranslationMemoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('TranslationMemory::count returns integer greater than 100')]
    public function testTranslationMemoryCountReturnsIntegerGreaterThanOneHundred(): void
    {
        $registry = Registry::getInstance();
        $tm = new TranslationMemory($registry);
        $result = $tm->count();
        $this->assertIsInt($result);
        $this->assertGreaterThan(100, $result);
    }


    #[TestDox('TranslationMemory::flush exists')]
    public function testTranslationMemoryFlushExists(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->hasMethod('flush'));
    }

    #[TestDox('TranslationMemory::flush is public')]
    public function testTranslationMemoryFlushIsPublic(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'flush');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('TranslationMemory::flush has no parameters')]
    public function testTranslationMemoryFlushHasNoParameters(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'flush');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testTranslationMemoryFlushExists')]
    #[Depends('testTranslationMemoryFlushIsPublic')]
    #[Depends('testTranslationMemoryFlushHasNoParameters')]
    #[TestDox('TranslationMemory::flush returns (bool) true')]
    public function testTranslationMemoryFlushReturnsBoolTrue(): void
    {
        $registry = Registry::getInstance();
        $tm = new TranslationMemory($registry);
        $this->assertIsBool($tm->flush());
        $this->assertTrue($tm->flush());
    }


    
    #[TestDox('TranslationMemory::getTranslations exists')]
    public function testTranslationMemoryGetTranslationsExists(): void
    {
        $class = new ReflectionClass(TranslationMemory::class);
        $this->assertTrue($class->hasMethod('getTranslations'));
    }

    #[TestDox('TranslationMemory::getTranslations is public')]
    public function testTranslationMemoryGetTranslationsIsPublic(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'getTranslations');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('TranslationMemory::getTranslations has two OPTIONAL parameters')]
    public function testTranslationMemoryGetTranslationsHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(TranslationMemory::class, 'getTranslations');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('TranslationMemory::getTranslations returns array with translations in set language')]
    public function testTranslationMemoryGetTranslationsReturnsArrayWithTranslationsInSetLanguage(): void
    {
        $registry = Registry::getInstance();
        $tm = new TranslationMemory($registry);
        $result = $tm->getTranslations('fr-CA');

        $this->assertNotEmpty($result);
        $this->assertIsArray($result);
    }
}
