<?php

declare(strict_types=1);

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\AbstractDataAccessObject::class)]
#[Group('hmvc')]
final class AbstractDataAccessObjectTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractDataAccessObject class file exists')]
    public function testAbstractDataAccessObjectClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'AbstractDataAccessObject.php'
        );
    }

    #[Depends('testAbstractDataAccessObjectClassFileExists')]
    #[Group('distro')]
    #[TestDox('AbstractDataAccessObject class file is readable')]
    public function testAbstractDataAccessObjectClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'AbstractDataAccessObject.php'
        );
    }

    #[Group('distro')]
    #[TestDox('AbstractDataAccessObject class exists')]
    public function testAbstractDataAccessObjectClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Database\\AbstractDataAccessObject'));
        $this->assertTrue(class_exists(AbstractDataAccessObject::class));
    }


    #[TestDox('AbstractDataAccessObject is abstract')]
    public function testAbstractDataAccessObjectIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractDataAccessObject is a database model')]
    public function testAbstractDataAccessObjectIsDatabaseModel(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[TestDox('AbstractDataAccessObject implements CRUD interface')]
    public function testAbstractDataAccessObjectImplementsCrudInterface(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\CRUDInterface::class));
    }


    #[TestDox('AbstractDataAccessObject::create exists')]
    public function testAbstractDataAccessObjectCreateExists(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->hasMethod('create'));
    }

    #[Depends('testAbstractDataAccessObjectCreateExists')]
    #[TestDox('AbstractDataAccessObject::create is public')]
    public function testAbstractDataAccessObjectCreateIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'create');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDataAccessObjectCreateExists')]
    #[TestDox('AbstractDataAccessObject::create has one REQUIRED parameter')]
    public function testAbstractDataAccessObjectCreateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'create');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractDataAccessObject::read exists')]
    public function testAbstractDataAccessObjectReadExists(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->hasMethod('read'));
    }

    #[Depends('testAbstractDataAccessObjectReadExists')]
    #[TestDox('AbstractDataAccessObject::read is public')]
    public function testAbstractDataAccessObjectReadIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'read');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractDataAccessObjectReadExists')]
    #[TestDox('AbstractDataAccessObject::read has two parameters')]
    public function testAbstractDataAccessObjectReadHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'read');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDataAccessObjectReadHasTwoParameters')]
    #[TestDox('AbstractDataAccessObject::read has one REQUIRED parameter')]
    public function testAbstractDataAccessObjectReadHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'read');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractDataAccessObject::update exists')]
    public function testAbstractDataAccessObjectUpdateExists(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->hasMethod('update'));
    }

    #[Depends('testAbstractDataAccessObjectUpdateExists')]
    #[TestDox('AbstractDataAccessObject::update is public')]
    public function testAbstractDataAccessObjectUpdateIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'update');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractDataAccessObjectUpdateExists')]
    #[TestDox('AbstractDataAccessObject::update has two parameters')]
    public function testAbstractDataAccessObjectUpdateHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'update');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDataAccessObjectUpdateHasTwoParameters')]
    #[TestDox('AbstractDataAccessObject::update has one REQUIRED parameter')]
    public function testAbstractDataAccessObjectUpdateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'update');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractDataAccessObject::delete exists')]
    public function testAbstractDataAccessObjectDeleteExists(): void
    {
        $class = new ReflectionClass(AbstractDataAccessObject::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testAbstractDataAccessObjectDeleteExists')]
    #[TestDox('AbstractDataAccessObject::delete is public')]
    public function testAbstractDataAccessObjectDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'delete');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractDataAccessObjectDeleteExists')]
    #[TestDox('AbstractDataAccessObject::delete is not final')]
    public function testAbstractDataAccessObjectDeleteIsNotFinal(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'delete');
        $this->assertFalse($method->isFinal());
    }

    #[Depends('testAbstractDataAccessObjectDeleteExists')]
    #[TestDox('AbstractDataAccessObject::delete has two parameters')]
    public function testAbstractDataAccessObjectDeleteHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'delete');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDataAccessObjectDeleteHasTwoParameters')]
    #[TestDox('AbstractDataAccessObject::delete has one REQUIRED parameter')]
    public function testAbstractDataAccessObjectDeletHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDataAccessObject::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
