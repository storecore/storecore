<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\BlocklistReader::class)]
#[CoversClass(\StoreCore\Database\Blocklist::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class BlocklistReaderTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('BlocklistReader class is concrete')]
    public function testBlocklistReaderClassIsConcrete(): void
    {
        $class = new ReflectionClass(BlocklistReader::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('BlocklistReader is database model')]
    public function testBlocklistReaderIsADatabaseModel(): void
    {
        $class = new ReflectionClass(BlocklistReader::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BlocklistReader::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BlocklistReader::VERSION);
        $this->assertIsString(BlocklistReader::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BlocklistReader::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('BlocklistReader::has exists')]
    public function testBlocklistReaderHasExists(): void
    {
        $class = new ReflectionClass(BlocklistReader::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testBlocklistReaderHasExists')]
    #[TestDox('BlocklistReader::has is final public')]
    public function testBlocklistReaderHasIsFinalPublic(): void
    {
        $method = new ReflectionMethod(BlocklistReader::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBlocklistReaderHasExists')]
    #[TestDox('BlocklistReader::has has one REQUIRED parameter')]
    public function testBlocklistReaderHasHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(BlocklistReader::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBlocklistReaderHasExists')]
    #[Depends('testBlocklistReaderHasIsFinalPublic')]
    #[Depends('testBlocklistReaderHasHasOneRequiredParameter')]
    #[TestDox('BlocklistReader::has returns (bool) true on blocked IP address')]
    public function testBlocklistReaderHasReturnsBoolTrueOnBlockedIpAddress(): void
    {
        try {
            $registry = Registry::getInstance();
            $database = $registry->get('Database');
            $database->query("INSERT INTO `sc_ip_blocklist` (`ip_address`, `from_date`, `thru_date`, `last_seen`) VALUES ('220.181.38.251', '2022-04-01 11:12:13', NULL, NULL)");
            $database->query("INSERT INTO `sc_ip_blocklist` (`ip_address`, `from_date`, `thru_date`, `last_seen`) VALUES ('2a02:6b8:a::a', '2022-04-01 11:12:13', NULL, NULL)");    
        } catch (\PDOException $e) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $blocklist = new BlocklistReader($registry);
        $this->assertIsBool($blocklist->has('220.181.38.251'));
        $this->assertIsBool($blocklist->has('2a02:6b8:a::a'));
        $this->assertTrue($blocklist->has('220.181.38.251'));
        $this->assertTrue($blocklist->has('2a02:6b8:a::a'));

        $database->query("DELETE FROM `sc_ip_blocklist` WHERE `ip_address` = '220.181.38.251'");
        $database->query("DELETE FROM `sc_ip_blocklist` WHERE `ip_address` = '2a02:6b8:a::a'");

        $this->assertFalse($blocklist->has('220.181.38.251'));
        $this->assertFalse($blocklist->has('2a02:6b8:a::a'));
    }

    #[Depends('testBlocklistReaderHasReturnsBoolTrueOnBlockedIpAddress')]
    #[TestDox('BlocklistReader::has() returns (bool) false on invalid IP address')]
    public function testBlocklistReaderHasReturnsBoolFalseOnInvalidIpAddress(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new \StoreCore\Database\BlocklistReader($registry);
        $this->assertIsBool($blocklist->has('42'));
        $this->assertFalse($blocklist->has('42'));
    }


    #[TestDox('BlocklistReader::log exists')]
    public function testBlocklistReaderLogExists(): void
    {
        $class = new ReflectionClass(BlocklistReader::class);
        $this->assertTrue($class->hasMethod('log'));
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[TestDox('BlocklistReader::log is final public')]
    public function testBlocklistReaderLogIsFinalPublic(): void
    {
        $method = new ReflectionMethod(BlocklistReader::class, 'log');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[TestDox('BlocklistReader::log has one REQUIRED parameter')]
    public function testBlocklistReaderLogHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BlocklistReader::class, 'log');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
