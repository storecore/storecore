<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Store;
use StoreCore\Types\{UUIDFactory, UUID};

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\StoreMapper::class)]
#[CoversClass(\StoreCore\Database\StoreRepository::class)]
#[CoversClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Store::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class StoreMapperTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('StoreMapper class is concrete')]
    public function testStoreMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('StoreMapper is a database model')]
    public function testStoreMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPsr11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPsr11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('StoreMapper implements PSR-11 ContainerInterface')]
    public function testStoreMapperImplementsPsr11ContainerInterface(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->hasMethod('get'));
        $this->assertTrue($class->hasMethod('has'));
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(StoreMapper::VERSION);
        $this->assertIsString(StoreMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(StoreMapper::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('StoreMapper::list exists')]
    public function testStoreMapperListExists(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[Depends('testStoreMapperListExists')]
    #[TestDox('StoreMapper::list is public')]
    public function testStoreMapperListIsPublic(): void
    {
        $method = new ReflectionMethod(StoreMapper::class, 'list');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreMapperListExists')]
    #[TestDox('StoreMapper::list has no parameters')]
    public function testStoreMapperListHasNoParameters(): void
    {
        $method = new ReflectionMethod(StoreMapper::class, 'list');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('StoreMapper::get exists')]
    public function testStoreMapperGetExists(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testStoreMapperGetExists')]
    #[TestDox('StoreMapper::get is public')]
    public function testStoreMapperGetIsPublic(): void
    {
        $method = new ReflectionMethod(StoreMapper::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreMapperGetExists')]
    #[TestDox('StoreMapper::get has one REQUIRED parameter')]
    public function testStoreMapperGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StoreMapper::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPsr11ContainerInterfaceExists')]
    #[Depends('testStoreMapperGetHasOneRequiredParameter')]
    #[TestDox('StoreMapper::get accepts SMALLINT UNSIGNED')]
    public function testStoreMapperGetAcceptsSmallintUnsigned(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $mapper = new StoreMapper($registry);
        $this->assertFalse($mapper->has(0));
        $this->assertFalse($mapper->has(65536));

        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        $failure = $mapper->get(65536);
    }


    #[TestDox('StoreMapper::toArray exists')]
    public function testStoreMapperToArrayExists(): void
    {
        $class = new ReflectionClass(StoreMapper::class);
        $this->assertTrue($class->hasMethod('toArray'));
    }

    #[Depends('testStoreMapperToArrayExists')]
    #[TestDox('StoreMapper::toArray is final public')]
    public function testStoreMapperToArrayIsFinalPublic(): void
    {
        $method = new ReflectionMethod(StoreMapper::class, 'toArray');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreMapperToArrayExists')]
    #[Depends('testStoreMapperToArrayIsFinalPublic')]
    #[TestDox('StoreMapper::toArray accepts Store object and returns non-empty array')]
    public function testStoreMapperToArrayAcceptsStoreObjectAndReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $mapper = new StoreMapper($registry);

        $store = new Store($registry);
        $this->assertIsObject($store);
        $this->assertNotEmpty($mapper->toArray($store));
        $this->assertIsArray($mapper->toArray($store));

        $this->assertFalse($store->hasIdentifier());
        $store->setIdentifier('b8012c5c-d01d-481c-b4e9-906bb2a1bd5a');
        $this->assertTrue($store->hasIdentifier());
        $this->assertArrayHasKey('store_uuid', $mapper->toArray($store));
        $this->assertEquals('b8012c5cd01d481cb4e9906bb2a1bd5a', $mapper->toArray($store)['store_uuid']);
    }
}
