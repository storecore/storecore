<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
final class NotFoundExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('NotFoundException class exists')]
    public function testNotFoundExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'NotFoundException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'NotFoundException.php');

        $this->assertTrue(class_exists('\\StoreCore\\Database\\NotFoundException'));
        $this->assertTrue(class_exists(NotFoundException::class));
    }

    #[Group('hmvc')]
    #[TestDox('NotFoundException class is concrete')]
    public function testNotFoundExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('NotFoundException is a throwable runtime exception')]
    public function testNotFoundExceptionIsThrowableRuntimeException(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));
    }

    #[Depends('testNotFoundExceptionIsThrowableRuntimeException')]
    #[Group('hmvc')]
    #[TestDox('NotFoundException is a database container exception')]
    public function testNotFoundExceptionIsDatabaseContainerException(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\ContainerException::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 NotFoundExceptionInterface exists')]
    public function testImplementedPSR11NotFoundExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\NotFoundExceptionInterface::class));
    }

    #[Depends('testImplementedPSR11NotFoundExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('NotFoundException implements PSR-11 NotFoundExceptionInterface')]
    public function testNotFoundExceptionImplementsPsr11NotFoundExceptionInterface(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\NotFoundExceptionInterface::class));
    }
}
