<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Currency;
use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\Currencies::class)]
#[CoversClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\StoreMapper::class)]
#[UsesClass(\StoreCore\Database\StoreRepository::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class CurrenciesTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Currencies class is concrete')]
    public function testCurrenciesClassIsConcrete(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Currencies is a database model')]
    public function testCurrenciesIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Group('hmvc')]
    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[TestDox('Currencies implements PSR-11 ContainerInterface')]
    public function testCurrenciesImplementsPSR11ContainerInterface(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new Currencies($registry);
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $repository);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Currencies::VERSION);
        $this->assertIsString(Currencies::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Currencies::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Currencies::createFromString exists')]
    public function testCurrenciesCreateFromStringExists(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertTrue($class->hasMethod('createFromString'));
    }

    #[Depends('testCurrenciesCreateFromStringExists')]
    #[TestDox('Currencies::createFromString is public static')]
    public function testCurrenciesCreateFromStringIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'createFromString');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testCurrenciesCreateFromStringExists')]
    #[TestDox('Currencies::createFromString has one REQUIRED parameter')]
    public function testCurrenciesCreateFromStringHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'createFromString');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCurrenciesCreateFromStringIsPublicStatic')]
    #[Depends('testCurrenciesCreateFromStringHasOneRequiredParameter')]
    #[TestDox('Currencies::createFromString returns stringable Currency')]
    public function testCurrenciesCreateFromStringReturnsStringableCurrency(): void
    {
        $this->assertNotEmpty(Currencies::createFromString('GBP'));
        $this->assertIsObject(Currencies::createFromString('GBP'));
        $this->assertInstanceOf(\Stringable::class, Currencies::createFromString('GBP'));
        $this->assertInstanceOf(\StoreCore\Currency::class, Currencies::createFromString('GBP'));
    }


    #[TestDox('Currencies::getStoreCurrencies exists')]
    public function testCurrenciesGetStoreCurrenciesExists(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertTrue($class->hasMethod('getStoreCurrencies'));
    }

    #[Depends('testCurrenciesGetStoreCurrenciesExists')]
    #[TestDox('Currencies::getStoreCurrencies is public')]
    public function testCurrenciesGetStoreCurrenciesIsPublic(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'getStoreCurrencies');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCurrenciesGetStoreCurrenciesExists')]
    #[TestDox('Currencies::getStoreCurrencies has one REQUIRED parameter')]
    public function testCurrenciesGetStoreCurrenciesHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'getStoreCurrencies');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCurrenciesGetStoreCurrenciesExists')]
    #[Depends('testCurrenciesGetStoreCurrenciesIsPublic')]
    #[Depends('testCurrenciesGetStoreCurrenciesHasOneRequiredParameter')]
    #[TestDox('Currencies::getStoreCurrencies always returns non-empty array')]
    public function testCurrenciesGetStoreCurrenciesAlwaysReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_currencies`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }

        $store_id = random_int(127, 255);
        $store_repository = new StoreRepository($registry);
        $this->assertFalse($store_repository->has((string) $store_id));

        $currencies = new Currencies($registry);
        $this->assertNotNull($currencies->getStoreCurrencies($store_id));
        $this->assertNotEmpty($currencies->getStoreCurrencies($store_id));
        $this->assertIsArray($currencies->getStoreCurrencies($store_id));
    }

    #[Depends('testCurrenciesGetStoreCurrenciesAlwaysReturnsNonEmptyArray')]
    #[TestDox('Currencies::getStoreCurrencies returns EUR (978) as fallback default currency')]
    public function testCurrenciesGetStoreCurrenciesReturnsEuroAsFallbackDefaultCurrency(): void
    {
        $registry = Registry::getInstance();
        $store_id = random_int(127, 255);
        $currencies = new Currencies($registry);
        $this->assertCount(1, $currencies->getStoreCurrencies($store_id));
        $this->assertArrayHasKey(978, $currencies->getStoreCurrencies($store_id));
        $this->assertInstanceOf(Currency::class, $currencies->getStoreCurrencies($store_id)[978]);
    }


    #[TestDox('Currencies::list exists')]
    public function testCurrenciesListExists(): void
    {
        $class = new ReflectionClass(Currencies::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[Depends('testCurrenciesListExists')]
    #[TestDox('Currencies::list is public')]
    public function testCurrenciesListIsPublic(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'list');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCurrenciesListExists')]
    #[TestDox('Currencies::list has no parameters')]
    public function testCurrenciesListHasNoParameters(): void
    {
        $method = new ReflectionMethod(Currencies::class, 'list');
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCurrenciesListExists')]
    #[TestDox('Currencies::list returns array with Currency objects')]
    public function testCurrenciesListReturnsArrayWithCurrencyObjects(): void
    {
        try {
            $registry = Registry::getInstance();
            $repository = new Currencies($registry);
            $currencies = $repository->list();
        } catch (\PDOException $e) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $this->assertNotEmpty($currencies);
        $this->assertIsArray($currencies);

        $this->assertArrayHasKey(978, $currencies);
        $this->assertInstanceOf(\StoreCore\Currency::class, $currencies[978]);
    }
}
