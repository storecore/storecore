<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\AccessControlList::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class AccessControlListTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_ip_access_control_list`');
        } catch (\PDOException $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    #[Group('distro')]
    #[TestDox('lList class file exists')]
    public function testAccessControlListClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR .  'AccessControlList.php'
        );
    }

    #[Depends('testAccessControlListClassFileExists')]
    #[Group('distro')]
    #[TestDox('AccessControlList class file is readable')]
    public function testAccessControlListClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR .  'AccessControlList.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('AccessControlList is a database model')]
    public function testAccessControlListIsDatabaseModel(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('AccessControlList is countable')]
    public function testAccessControlListIsCountable(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->isSubclassOf(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AccessControlList::VERSION);
        $this->assertIsString(AccessControlList::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AccessControlList::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AccessControlList::clear exists')]
    public function testAccessControlListClearExists(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[Depends('testAccessControlListClearExists')]
    #[TestDox('AccessControlList::clear is public')]
    public function testAccessControlListClearIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'clear');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('AccessControlList::clear has no parameters')]
    public function testAccessControlListClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('AccessControlList::clear returns true on success')]
    public function testAccessControlListClearReturnsTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);
        $this->assertIsBool($acl->clear());
        $this->assertTrue($acl->clear());
        $this->assertSame(
            0,
            $acl->count(),
            'After calling `clear()` the ACL count must be 0.'
        );
    }


    #[TestDox('AccessControlList::count exists')]
    public function testAccessControlListCountExists(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testAccessControlListCountExists')]
    #[TestDox('AccessControlList::count is public')]
    public function testAccessControlListCountIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAccessControlListCountExists')]
    #[TestDox('AccessControlList::count has no parameters')]
    public function testAccessControlListCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAccessControlListCountExists')]
    #[Depends('testAccessControlListCountHasNoParameters')]
    #[TestDox('AccessControlList::count returns int')]
    public function testAccessControlListCountReturnsInt(): void
    {
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);
        $this->assertIsInt($acl->count());
        $this->assertTrue($acl->count() >= 0);

        if ($acl->isEmpty()) {
            $this->assertSame(
                0,
                $acl->count(),
                'If the ACL is empty, the count MUST be 0.'
            );
        }
    }


    #[TestDox('AccessControlList::delete exists')]
    public function testAccessControlListDeleteExists(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testAccessControlListDeleteExists')]
    #[TestDox('AccessControlList::delete is public')]
    public function testAccessControlListDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'delete');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAccessControlListDeleteExists')]
    #[TestDox('AccessControlList::delete has one REQUIRED parameter')]
    public function testAccessControlListDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAccessControlListDeleteExists')]
    #[TestDox('AccessControlList::delete throws value error on invalid IP address')]
    public function testAccessControlListDeleteThrowsValueErrorOnInvalidIpAddress(): void
    {
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);
        $this->expectException(\ValueError::class);
        $acl->delete('42');
    }

    #[Depends('testAccessControlListDeleteHasOneRequiredParameter')]
    #[TestDox('AccessControlList::delete always returns (bool) true on success')]
    public function testAccessControlListDeleteAlwaysReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);
        $this->assertIsBool($acl->delete('192.0.2.0'));
        $this->assertTrue($acl->delete('192.0.2.0'));
    }


    #[TestDox('AccessControlList::has exists')]
    public function testAccessControlListHasExists(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testAccessControlListHasExists')]
    #[TestDox('AccessControlList::has is public')]
    public function testAccessControlListHasIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAccessControlListHasExists')]
    #[TestDox('AccessControlList::has has two parameters')]
    public function testAccessControlListHasHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'has');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAccessControlListHasHasTwoParameters')]
    #[TestDox('AccessControlList::has has one REQUIRED parameter')]
    public function testAccessControlListHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'has');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAccessControlListHasHasOneRequiredParameter')]
    #[TestDox('AccessControlList::has returns (bool) false if IP address has no access')]
    public function testAccessControlListHasReturnsBoolFalseIfIpAddressHasNoAccess(): void
    {
        
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);

        $this->assertIsBool($acl->has('198.51.100.0'));
        $this->assertFalse($acl->has('198.51.100.0'));

        $this->assertIsBool($acl->has('198.51.100.0', true));
        $this->assertFalse($acl->has('198.51.100.0', true));
    }


    #[TestDox('AccessControlList::isEmpty() exists')]
    public function testAccessControlListIsEmptyExists(): void
    {
        $class = new ReflectionClass(AccessControlList::class);
        $this->assertTrue($class->hasMethod('isEmpty'));
    }

    #[Depends('testAccessControlListIsEmptyExists')]
    #[TestDox('AccessControlList::isEmpty() is public')]
    public function testAccessControlListIsEmptyIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'isEmpty');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAccessControlListIsEmptyExists')]
    #[TestDox('AccessControlList::isEmpty() has no parameters')]
    public function testAccessControlListIsEmptyHasNoParameters(): void
    {
        $method = new ReflectionMethod(AccessControlList::class, 'isEmpty');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAccessControlListIsEmptyExists')]
    #[Depends('testAccessControlListIsEmptyIsPublic')]
    #[Depends('testAccessControlListIsEmptyHasNoParameters')]
    #[TestDox('AccessControlList::isEmpty() returns bool')]
    public function testAccessControlListIsEmptyReturnsBools(): void
    {
        $registry = Registry::getInstance();
        $acl = new AccessControlList($registry);
        $this->assertIsBool($acl->isEmpty());
        $this->assertTrue($acl->isEmpty());
    }
}
