<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \DateInterval;
use StoreCore\Engine\Redirect;
use StoreCore\Registry;

use \ReflectionClass, ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\RedirectCache::class)]
#[CoversClass(\StoreCore\Database\RedirectMapper::class)]
#[CoversClass(\StoreCore\Engine\Redirect::class)]
#[CoversClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class RedirectCacheTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('RedirectCache class is concrete')]
    public function testRedirectCacheClassIsConcrete(): void
    {
        $class = new ReflectionClass(RedirectCache::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('RedirectCache is a database model')]
    public function testRedirectCacheIsDatabaseModel(): void
    {
        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));

    }

    #[TestDox('RedirectCache implements PSR-11 ContainerInterface')]
    public function testRedirectCacheImplementsPSR11ContainerInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));

        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[TestDox('RedirectCache implements PSR-16 CacheInterface')]
    public function testRedirectCacheImplementsPSR16CacheInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));

        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->implementsInterface(\Psr\SimpleCache\CacheInterface::class));
    }

    #[TestDox('RedirectCache is countable')]
    public function testRedirectCacheIsCountable(): void
    {
        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RedirectCache::VERSION);
        $this->assertIsString(RedirectCache::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RedirectCache::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('RedirectCache::count() exists')]
    public function testRedirectCacheCountExists(): void
    {
        $class = new ReflectionClass(RedirectCache::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testRedirectCacheCountExists')]
    #[TestDox('RedirectCache::count() is public')]
    public function testRedirectCacheCountIsPublic(): void
    {
        $method = new ReflectionMethod(RedirectCache::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testRedirectCacheCountIsPublic')]
    #[TestDox('RedirectCache::count() has no parameters')]
    public function testRedirectCacheCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(RedirectCache::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRedirectCacheCountExists')]
    #[Depends('testRedirectCacheCountIsPublic')]
    #[Depends('testRedirectCacheCountHasNoParameters')]
    #[TestDox('RedirectCache::count() returns integer')]
    public function testRedirectCacheCountReturnsInteger(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $cache = new RedirectCache($registry);
        $this->assertIsInt($cache->count());
        $this->assertIsInt(count($cache));
        $this->assertEquals(count($cache), $cache->count());
    }


    #[TestDox('RedirectCache::set() persists redirect')]
    public function testRedirectCacheSetPersistsRedirect(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $redirect = new Redirect('//localhost/kb', 'https://www.storecore.io/knowledge-base');
        $redirect->expiresAfter(DateInterval::createFromDateString('5 minutes'));

        $cache = new RedirectCache($registry);
        $this->assertTrue($cache->clear());
        $this->assertEquals(0, $cache->count());

        $mapper = new RedirectMapper($registry);
        $this->assertTrue($mapper->save($redirect));
        $this->assertEquals(1, $cache->count());

        unset($mapper);
        $this->assertTrue($cache->clear());
        $this->assertEquals(0, $cache->count());

        $key = RedirectCache::createKey($redirect);
        $this->assertTrue($cache->set($key, $redirect));
        $this->assertEquals(1, $cache->count());
    }
}
