<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\CRM\Person;
use StoreCore\Geo\Country;
use StoreCore\Geo\Place;
use StoreCore\Types\Date;
use StoreCore\Types\URL;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\PersonMapper::class)]
#[CoversClass(\StoreCore\CRM\Person::class)]
#[CoversClass(\StoreCore\Google\People\Name::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class PersonMapperTest extends TestCase
{
    protected function setUp(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->beginTransaction();
        }
    }

    protected function tearDown(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->rollback();
        }
    }

    public static function setUpAfterClass(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->exec('FLUSH QUERY CACHE');
        }
    }


    #[TestDox('PersonMapper class is concrete')]
    public function testPersonMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(PersonMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PersonMapper is an abstract data access object (DAO)')]
    public function testPersonMapperIsAbstractDataAccessObjectDAO(): void
    {
        $class = new ReflectionClass(PersonMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractDataAccessObject::class));
    }

    #[Group('hmvc')]
    #[TestDox('PersonMapper implements CRUD interface')]
    public function testPersonMapperImplementsCrudInterface(): void
    {
        $class = new ReflectionClass(PersonMapper::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\Database\CRUDInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PersonMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PersonMapper::VERSION);
        $this->assertIsString(PersonMapper::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PersonMapper::VERSION, '0.3.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('PersonMapper::save() creates and updates Person')]
    public function testPersonMapperSaveCreatesAndUpdatesPerson(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $person = new Person();
        $person->givenName = 'René';
        $person->familyName = 'Magritte';
        $person->gender = 1;
        $person->birthDate = new Date('1898-11-21');
        $person->birthPlace =  new Place('Lessines');
        $person->nationality = new Country('BE', 'Belgium');
        $person->sameAs = new URL('https://www.wikidata.org/wiki/Q7836');

        $this->assertFalse(
            $person->hasIdentifier(),
            'Newly created Person MUST NOT have an identifier.'
        );
        $this->assertNull(
            $person->dateModified,
            'Newly created Person SHOULD NOT have a dateModified.'
        );
        $this->assertNull(
            $person->eTag,
            'Newly created Person has no entity tag (ETag).'
        );

        $mapper = new PersonMapper($registry);
        $this->assertTrue(
            $mapper->save($person),
            'PersonMapper::save SHOULD return true on CREATE.'
        );

        $this->assertTrue(
            $person->hasIdentifier(),
            'Person persisted to storage MUST have an identifier.'
        );
        $this->assertNotNull(
            $person->dateCreated,
            'Person persisted to storage MUST have a dateCreated.'
        );
        $this->assertNotNull(
            $person->eTag,
            'Person persisted to storage MUST have an ETag.'
        );
        $this->assertNull(
            $person->dateModified,
            'Newly created Person SHOULD NOT have a dateModified.'
        );

        $person->deathDate = new Date('1967-08-15');
        $person->deathPlace = new Place('Brussels');

        $this->assertTrue(
            $mapper->save($person),
            'PersonMapper::save SHOULD return true on UPDATE.'
        );
        $this->assertNotNull(
            $person->dateModified,
            'Updated Person SHOULD have a dateModified.'
        );
    }
}
