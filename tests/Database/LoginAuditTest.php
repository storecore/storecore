<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;
use function \random_int;

#[CoversClass(\StoreCore\Database\LoginAudit::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
class LoginAuditTest extends TestCase
{
    /**
     * Logs 100 login attempts, of which about 1 in 7 is successful.
     */
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $randomizer = new \Random\Randomizer();
            $logger = new LoginAttempts($registry);
            for ($i = 1; $i <= 100; $i++) {
                $email_address = $randomizer->getBytesFromString('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', random_int(3, 64)) . '@example.com';
                $ip_address = '203.0.113.' . random_int(0, 255);
                $successful = random_int(1, 7) === 1 ? true : false;
                $logger->storeAttempt($email_address, $ip_address, $successful);
            }
        }
    }

    /**
     * @see https://www.rfc-editor.org/rfc/rfc5737#section-3
     *      “Documentation Address Blocks” in RFC 5737
     */
    public static function setUpAfterClass(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->exec(
                "DELETE
                   FROM `sc_login_attempts`
                  WHERE `user_identity` = '%@example.com'
                     OR `remote_address` = '192.0.2.%'
                     OR `remote_address` = '198.51.100.%'
                     OR `remote_address` = '203.0.113.%'"
            );

            $registry->get('Database')->exec('FLUSH QUERY CACHE');
            $registry->get('Database')->exec('FLUSH TABLES `sc_login_attempts`');
        }
    }


    #[Group('hmvc')]
    #[TestDox('LoginAudit class is concrete')]
    public function testLoginAuditClassIsConcrete(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LoginAudit class is a database model')]
    public function testLoginAuditClassIsAModel(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LoginAudit::VERSION);
        $this->assertIsString(LoginAudit::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LoginAudit::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('LoginAudit::count exists')]
    public function testLoginAuditCountExists(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('LoginAudit::count is public')]
    public function testLoginAuditCountIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('LoginAudit::count has no parameter')]
    public function testLoginAuditCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLoginAuditCountExists')]
    #[Depends('testLoginAuditCountIsPublic')]
    #[Depends('testLoginAuditCountHasNoParameters')]
    #[TestDox('LoginAudit::count returns integer greater than or equal to 0 (zero)')]
    public function testLoginAuditCountReturnsIntegerGreaterThanOrEqualToZero(): void
    {
        $registry = Registry::getInstance();
        $audit = new LoginAudit($registry);
        $this->assertIsInt($audit->count());
        $this->assertGreaterThanOrEqual(0, $audit->count());

        $this->assertGreaterThanOrEqual(
            80, $audit->count(),
            'Because the test setup writes 100 rows, the count SHOULD be at least 80.'
        );
    }

    #[Depends('testLoginAuditCountReturnsIntegerGreaterThanOrEqualToZero')]
    #[TestDox('LoginAudit is countable')]
    public function testLoginAuditIsCountable(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));

        $registry = Registry::getInstance();
        $audit = new LoginAudit($registry);
        $this->assertIsInt(count($audit));
        $this->assertEquals($audit->count(), count($audit));
        $this->assertGreaterThanOrEqual(0, count($audit));
    }


    #[TestDox('LoginAudit::countLastFailedAttempts exists')]
    public function testLoginAuditCountLastFailedAttemptsExists(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->hasMethod('countLastFailedAttempts'));
    }

    #[TestDox('LoginAudit::countLastFailedAttempts is public')]
    public function testLoginAuditCountLastFailedAttemptsIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'countLastFailedAttempts');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('LoginAudit::countLastFailedAttempts has one OPTIONAL parameter')]
    public function testLoginAuditCountLastFailedAttemptsHasNoParameters(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'countLastFailedAttempts');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLoginAuditCountLastFailedAttemptsExists')]
    #[Depends('testLoginAuditCountLastFailedAttemptsIsPublic')]
    #[Depends('testLoginAuditCountLastFailedAttemptsHasNoParameters')]
    #[TestDox('LoginAudit::countLastFailedAttempts returns integer greater than or equal to 0 (zero)')]
    public function testLoginAuditCountLastFailedAttemptsReturnsIntegerGreaterThanOrEqualToZero(): void
    {
        $registry = Registry::getInstance();
        $audit = new LoginAudit($registry);
        $this->assertIsInt($audit->countLastFailedAttempts());
        $this->assertGreaterThanOrEqual(0, $audit->countLastFailedAttempts());

        $this->assertGreaterThanOrEqual(
            50, $audit->countLastFailedAttempts(),
            'Because the test setup writes 100 rows, countLastFailedAttempts() SHOULD be at least 50.'
        );

        $this->assertLessThan(
            $audit->count(), $audit->countLastFailedAttempts(),
            'Because the test setup also logs successful attempts, the total count MUST be greater than the count of failed attemps.'
        );
    }


    #[TestDox('LoginAudit::list exists')]
    public function testLoginAuditListExists(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[TestDox('LoginAudit::list is public')]
    public function testLoginAuditListIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'list');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('LoginAudit::list has two OPTIONAL parameters')]
    public function testLoginAuditListHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'list');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('LoginAudit::list returns non-empty array')]
    public function testLoginAuditListReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        $audit = new LoginAudit($registry);
        $result = $audit->list();
        $this->assertNotEmpty($result);
        $this->assertIsArray($result);

        $this->assertGreaterThan(
            68, count($result),
            'Because the test setup writes 100 rows, the result set SHOULD contain at least about 68.27 rows.'
        );
    }


    #[TestDox('LoginAudit::optimize exists')]
    public function testLoginAuditOptimizeExists(): void
    {
        $class = new ReflectionClass(LoginAudit::class);
        $this->assertTrue($class->hasMethod('optimize'));
    }

    #[Depends('testLoginAuditOptimizeExists')]
    #[TestDox('LoginAudit::optimize is public')]
    public function testLoginAuditOptimizeIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'optimize');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testLoginAuditOptimizeExists')]
    #[TestDox('LoginAudit::optimize has no parameters')]
    public function testLoginAuditOptimizeHasNoParameters(): void
    {
        $method = new ReflectionMethod(LoginAudit::class, 'optimize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('LoginAudit::optimize returns integer on success')]
    public function testLoginAuditOptimizeReturnsIntegerOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $audit = new LoginAudit($registry);
        $result = $audit->optimize();
        $this->assertIsInt($result);
        $this->assertGreaterThanOrEqual(0, $result);
    }
}
