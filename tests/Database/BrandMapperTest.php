<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Route;
use StoreCore\RoutingQueue;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[Group('hmvc')]
#[CoversClass(\StoreCore\Database\BrandMapper::class)]
#[CoversClass(\StoreCore\PIM\Brand::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\RoutingQueue::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class BrandMapperTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_brands`');

            $registry->get('Database')->exec(
                "INSERT IGNORE INTO `sc_brands` (`brand_uuid`, `global_brand_name`) 
                   VALUES (UNHEX('c57e4ef3986e43b9aa5a1e64d04dac35'), 'Nike')"
            );

        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[TestDox('BrandMapper class is concrete')]
    public function testBrandMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('BrandMapper is a database model')]
    public function testBrandMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[TestDox('BrandMapper implements PSR-11 ContainerInterface')]
    public function testBrandMapperImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BrandMapper::VERSION);
        $this->assertIsString(BrandMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BrandMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('BrandMapper::get exists')]
    public function testBrandMapperGetExists(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testBrandMapperGetExists')]
    #[TestDox('BrandMapper::get is public')]
    public function testBrandMapperGetIsPublic(): void
    {
        $method = new ReflectionMethod(BrandMapper::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandMapperGetExists')]
    #[TestDox('BrandMapper::get has one REQUIRED parameter')]
    public function testBrandMapperGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BrandMapper::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrandMapperGetExists')]
    #[Depends('testBrandMapperGetIsPublic')]
    #[Depends('testBrandMapperGetHasOneRequiredParameter')]
    #[TestDox('BrandMapper::get throws PSR-11 NotFoundExceptionInterface if brand does not exist')]
    public function testBrandMapperGetThrowsPSR11NotFoundExceptionInterfaceIfBrandDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $container = new BrandMapper($registry);

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('c7f4912f-00b3-4313-97de-67969a266c0c');
    }

    #[Depends('testBrandMapperGetThrowsPSR11NotFoundExceptionInterfaceIfBrandDoesNotExist')]
    #[TestDox('BrandMapper::get returns Brand if brand exists')]
    public function testBrandMapperGetReturnsBoolFalseIfBrandDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $container = new BrandMapper($registry);

        $brand = $container->get('c57e4ef3-986e-43b9-aa5a-1e64d04dac35');
        $this->assertInstanceOf(\StoreCore\PIM\Brand::class, $brand);
    }

    #[Depends('testBrandMapperGetReturnsBoolFalseIfBrandDoesNotExist')]
    #[TestDox('BrandMapper::get returns IdentityInterface and API endpoint')]
    public function testBrandMapperGetReturnsIdentityInterfaceAndAPIEndpoint(): void
    {
        $registry = Registry::getInstance();
        $container = new BrandMapper($registry);

        $brand = $container->get('c57e4ef3-986e-43b9-aa5a-1e64d04dac35');
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $brand);
        $this->assertTrue($brand->hasIdentifier());
        $this->assertEquals('c57e4ef3-986e-43b9-aa5a-1e64d04dac35', $brand->getIdentifier()->__toString());

        $expectedJson = <<<'JSON'
            {
              "@id": "/api/v1/brands/c57e4ef3-986e-43b9-aa5a-1e64d04dac35",
              "@context": "https://schema.org",
              "@type": "Brand",
              "identifier": "c57e4ef3-986e-43b9-aa5a-1e64d04dac35",
              "name": "Nike"
            }
        JSON;

        $actualJson = json_encode($brand, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('BrandMapper::has exists')]
    public function testBrandMapperHasExists(): void
    {
        $class = new ReflectionClass(BrandMapper::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testBrandMapperHasExists')]
    #[TestDox('BrandMapper::has is public')]
    public function testBrandMapperHasIsPublic(): void
    {
        $method = new ReflectionMethod(BrandMapper::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandMapperHasExists')]
    #[TestDox('BrandMapper::has has one REQUIRED parameter')]
    public function testBrandMapperHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BrandMapper::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrandMapperHasExists')]
    #[Depends('testBrandMapperHasIsPublic')]
    #[Depends('testBrandMapperHasHasOneRequiredParameter')]
    #[TestDox('BrandMapper::has returns (bool) false if brand does not exist')]
    public function testBrandMapperHasReturnsBoolFalseIfBrandDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $container = new BrandMapper($registry);
        $this->assertIsBool($container->has('9e3bbb93-1c1f-4cee-9dc6-aca80df0a576'));
        $this->assertFalse($container->has('c7f4912f-00b3-4313-97de-67969a266c0c'));

        $this->assertFalse(
            $container->has('65536'),
            'The `SMALLINT UNSIGNED` maximum is 65535, so id `65536` SHOULD NOT exist.'
        );

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('65536');
    }

    #[Depends('testBrandMapperHasReturnsBoolFalseIfBrandDoesNotExist')]
    #[TestDox('BrandMapper::has returns (bool) true if brand does exist')]
    public function testBrandMapperHasReturnsBoolTrueIfBrandDoesExist(): void
    {
        $registry = Registry::getInstance();
        $container = new BrandMapper($registry);
        $this->assertTrue($container->has('c57e4ef3-986e-43b9-aa5a-1e64d04dac35'));
    }
}
