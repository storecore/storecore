<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\AggregateRatings::class)]
#[CoversClass(\StoreCore\Types\AggregateRating::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\Rating::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class AggregateRatingsTest extends TestCase
{
    /**
     * Adds a `Restaurant` with an `AggregateRating` to the database.
     */
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $db = $registry->get('Database');
            $db->exec("DELETE FROM `sc_aggregate_ratings` WHERE `id` = 0 OR `item_reviewed_uuid` = UNHEX('00000000000000000000000000000000')");
            $db->exec("DELETE FROM `sc_organizations` WHERE `organization_id` = 0 OR `organization_uuid` = UNHEX('00000000000000000000000000000000')");

            $db->exec("INSERT INTO `sc_organizations` (`organization_id`, `organization_uuid`, `common_name`, `organization_type`, `date_created`) VALUES (0, UNHEX('00000000000000000000000000000000'), 'GreatFood', 'Restaurant', UTC_TIMESTAMP())");
            $db->exec("INSERT INTO `sc_aggregate_ratings` (`item_reviewed_uuid`, `rating_value`, `review_count`) VALUES (UNHEX('00000000000000000000000000000000'), 4, 250)");
            $db->exec("UPDATE `sc_aggregate_ratings` SET `id` = 0 WHERE `item_reviewed_uuid` = UNHEX('00000000000000000000000000000000')");
        } else {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    }


    #[Group('hmvc')]
    #[TestDox('AggregateRatings class is concrete')]
    public function testAggregateRatingsClassIsConcrete(): void
    {
        $class = new ReflectionClass(AggregateRatings::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AggregateRatings class is a database model')]
    public function testAggregateRatingsClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(AggregateRatings::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    /**
     * @see https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
     */
    #[Depends('testAggregateRatingsClassIsDatabaseModel')]
    #[TestDox('AggregateRatings class implements PSR-11 ContainerInterface')]
    public function testAggregateRatingsClassImplementsPsr11ContainerInterface()
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));

        $registry = Registry::getInstance();
        $container = new AggregateRatings($registry);
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $container);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AggregateRatings::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AggregateRatings::VERSION);
        $this->assertIsString(AggregateRatings::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AggregateRatings::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AggregateRatings::get() exists')]
    public function testAggregateRatingsGetExists(): void
    {
        $class = new ReflectionClass(AggregateRatings::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testAggregateRatingsGetExists')]
    #[TestDox('AggregateRatings::get() is public')]
    public function testAggregateRatingsGetIsPublic(): void
    {
        $method = new ReflectionMethod(AggregateRatings::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testAggregateRatingsGetExists')]
    #[TestDox('AggregateRatings::get() has one REQUIRED parameter')]
    public function testAggregateRatingsGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AggregateRatings::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAggregateRatingsGetExists')]
    #[Depends('testAggregateRatingsGetIsPublic')]
    #[Depends('testAggregateRatingsGetHasOneRequiredParameter')]
    #[TestDox('AggregateRatings::get() throws \TypeError on integer')]
    public function testAggregateRatingsGetThrowsTypeErrorOnInteger(): void
    {
        $registry = Registry::getInstance();
        $container = new AggregateRatings($registry);

        $this->expectException(\TypeError::class);
        $failure = $container->get(13);
    }

    #[Depends('testAggregateRatingsGetExists')]
    #[Depends('testAggregateRatingsGetIsPublic')]
    #[Depends('testAggregateRatingsGetHasOneRequiredParameter')]
    #[TestDox('AggregateRatings::get() throws PSR-11 ContainerExceptionInterface on invalid id')]
    public function testAggregateRatingsGetThrowsPsr11ContainerExceptionInterfaceOnInvalidId(): void
    {
        $registry = Registry::getInstance();
        $container = new AggregateRatings($registry);

        $this->assertTrue(interface_exists(\Psr\Container\ContainerExceptionInterface::class));
        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        $failure = $container->get('Sesame');
    }

    #[Depends('testAggregateRatingsGetExists')]
    #[Depends('testAggregateRatingsGetIsPublic')]
    #[Depends('testAggregateRatingsGetHasOneRequiredParameter')]
    #[TestDox('AggregateRatings::get() returns JSON serializable `AggregateRating` object')]
    public function testAggregateRatingsGetReturnsJsonSerializableAggregateRatingObject(): void
    {
        $registry = Registry::getInstance();
        $container = new AggregateRatings($registry);
        $rating = $container->get('00000000-0000-0000-0000-000000000000');

        $this->assertIsObject($rating);
        $this->assertInstanceOf(\JsonSerializable::class, $rating);
        $this->assertInstanceOf(\StoreCore\Types\AggregateRating::class, $rating);

        // Property values as set for an `AggregateRating` in the test `setUp()`:
        $this->assertEquals(4, $rating->ratingValue);
        $this->assertNull($rating->ratingCount);
        $this->assertEquals(250, $rating->reviewCount);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "AggregateRating",
              "bestRating": 5,
              "ratingValue": 4,
              "reviewCount": 250,
              "worstRating": 1
            }
        ';
        $actualJson = json_encode($rating, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('AggregateRatings::has() exists')]
    public function testAggregateRatingsHasExists(): void
    {
        $class = new ReflectionClass(AggregateRatings::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testAggregateRatingsHasExists')]
    #[TestDox('AggregateRatings::has() is public')]
    public function testAggregateRatingsHasIsPublic(): void
    {
        $method = new ReflectionMethod(AggregateRatings::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testAggregateRatingsHasExists')]
    #[TestDox('AggregateRatings::has() has one REQUIRED parameter')]
    public function testAggregateRatingsHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AggregateRatings::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAggregateRatingsHasExists')]
    #[Depends('testAggregateRatingsHasIsPublic')]
    #[Depends('testAggregateRatingsHasHasOneRequiredParameter')]
    #[Group('distro')]
    #[TestDox('AggregateRatings::has() returns bool')]
    public function testAggregateRatingsHasReturnsBool(): void
    {
        $registry = Registry::getInstance();
        $aggregateRatings = new AggregateRatings($registry);

        // Test with ID 0 and nil UUID.
        $this->assertTrue($aggregateRatings->has('0'));
        $this->assertTrue($aggregateRatings->has('00000000-0000-0000-0000-000000000000'));

        $registry->get('Database')->exec(
            "DELETE
               FROM `sc_aggregate_ratings`
              WHERE `id` = 0
                 OR `item_reviewed_uuid` = UNHEX('00000000000000000000000000000000')"
        );
        $this->assertFalse($aggregateRatings->has('0'));
        $this->assertFalse($aggregateRatings->has('00000000-0000-0000-0000-000000000000'));
    }
}
