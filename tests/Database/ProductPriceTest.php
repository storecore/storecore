<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\PIM\Product;
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\ProductPrice::class)]
#[CoversClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
class ProductPriceTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ProductPrice class is concrete')]
    public function testProductPriceClassIsConcrete(): void
    {
        $class = new ReflectionClass(ProductPrice::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ProductPrice is a database model')]
    public function testProductPriceIsDatabaseModel(): void
    {
        $class = new ReflectionClass(ProductPrice::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ProductPrice::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ProductPrice::VERSION);
        $this->assertIsString(ProductPrice::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ProductPrice::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('ProductPrice::$productUUID exists')]
    public function testProductPriceProductUUIDExists(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $price = new ProductPrice($registry);
        $this->assertObjectHasProperty('productUUID', $price);
    }

    #[Depends('testProductPriceProductUUIDExists')]
    #[TestDox('ProductPrice::$productUUID is public')]
    public function testProductPriceProductUUIDIsPublic(): void
    {
        $property = new ReflectionProperty(ProductPrice::class, 'productUUID');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testProductPriceProductUUIDExists')]
    #[TestDox('ProductPrice::$productUUID is null by default')]
    public function testProductPriceProductUUIDIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $price = new ProductPrice($registry);
        $this->assertNull($price->productUUID);
    }


    #[TestDox('ProductPrice::setProduct exists')]
    public function testProductPriceSetProductExists(): void
    {
        $class = new ReflectionClass(ProductPrice::class);
        $this->assertTrue($class->hasMethod('setProduct'));
    }

    #[Depends('testProductPriceSetProductExists')]
    #[TestDox('ProductPrice::setProduct is public')]
    public function testProductPriceSetProductIsPublic(): void
    {
        $method = new ReflectionMethod(ProductPrice::class, 'setProduct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testProductPriceSetProductExists')]
    #[TestDox('ProductPrice::setProduct has one REQUIRED parameter')]
    public function testProductPriceSetProductHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ProductPrice::class, 'setProduct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testProductPriceSetProductHasOneRequiredParameter')]
    #[TestDox('ProductPrice::setProduct has one REQUIRED parameter')]
    public function testPriceSetProductSetsProductPriceProductUUIDAsUUID(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $price = new ProductPrice($registry);
        $uuid_factory = new UUIDFactory($registry);
        $uuid = $uuid_factory->createUUID();

        $this->assertNull($price->productUUID);
        $price->setProduct($uuid);
        $this->assertNotNull($price->productUUID);
        $this->assertIsObject($price->productUUID);
        $this->assertInstanceOf(UUID::class, $price->productUUID);
        $this->assertSame($uuid, $price->productUUID);
    }

    #[Depends('testPriceSetProductSetsProductPriceProductUUIDAsUUID')]
    #[TestDox('ProductPrice::setProduct accepts StoreCore\PIM\Product with UUID')]
    public function testProductPriceSetProductAcceptsStoreCorePimProductWithUUID(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $product = new Product();
        $uuid_factory = new UUIDFactory($registry);
        $uuid = $uuid_factory->createUUID();
        $product->setIdentifier($uuid);

        $price = new ProductPrice($registry);
        $this->assertNull($price->productUUID);
        $this->assertTrue($product->hasIdentifier());

        $price->setProduct($product);
        $this->assertNotNull($price->productUUID);
        $this->assertIsObject($price->productUUID);
        $this->assertInstanceOf(UUID::class, $price->productUUID);
        $this->assertSame($uuid, $price->productUUID);
    }
}
