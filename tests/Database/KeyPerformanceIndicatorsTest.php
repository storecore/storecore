<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \count;
use function \version_compare;

#[CoversClass(\StoreCore\Database\KeyPerformanceIndicators::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\IdentityMap::class)]
#[UsesClass(\StoreCore\Database\ProductMapper::class)]
#[UsesClass(\StoreCore\Database\StoreRepository::class)]
#[UsesClass(\StoreCore\Database\UserRepository::class)]
#[UsesClass(\StoreCore\PIM\ProductRepository::class)]
final class KeyPerformanceIndicatorsTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    }

    #[Group('hmvc')]
    #[TestDox('KeyPerformanceIndicators is a database model')]
    public function testKeyPerformanceIndicatorsIsDatabaseModel(): void
    {
        $class = new ReflectionClass(KeyPerformanceIndicators::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(KeyPerformanceIndicators::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(KeyPerformanceIndicators::VERSION);
        $this->assertIsString(KeyPerformanceIndicators::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(KeyPerformanceIndicators::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('KeyPerformanceIndicators::__construct exists')]
    public function testKeyPerformanceIndicatorsConstructorExists(): void
    {
        $class = new ReflectionClass(KeyPerformanceIndicators::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testKeyPerformanceIndicatorsConstructorExists')]
    #[TestDox('KeyPerformanceIndicators::__construct is public constructor')]
    public function testKeyPerformanceIndicatorsConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testKeyPerformanceIndicatorsConstructorExists')]
    #[TestDox('KeyPerformanceIndicators::__construct has one REQUIRED parameter')]
    public function testKeyPerformanceIndicatorsConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('KeyPerformanceIndicators::getDimensions exists')]
    public function testKeyPerformanceIndicatorsGetDimensionsExists(): void
    {
        $class = new ReflectionClass(KeyPerformanceIndicators::class);
        $this->assertTrue($class->hasMethod('getDimensions'));
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsExists')]
    #[TestDox('KeyPerformanceIndicators::getDimensions is public')]
    public function testKeyPerformanceIndicatorsGetDimensionsIsPublic(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, 'getDimensions');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsExists')]
    #[TestDox('KeyPerformanceIndicators::getDimensions has no parameters')]
    public function testKeyPerformanceIndicatorsGetDimensionsHasNoParameters(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, 'getDimensions');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsExists')]
    #[Depends('testKeyPerformanceIndicatorsGetDimensionsIsPublic')]
    #[Depends('testKeyPerformanceIndicatorsGetDimensionsHasNoParameters')]
    #[TestDox('KeyPerformanceIndicators::getDimensions returns non-empty array')]
    public function testKeyPerformanceIndicatorsGetDimensionsReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $kpis = new KeyPerformanceIndicators($registry);
        $this->assertNotEmpty($kpis->getDimensions());
        $this->assertIsArray($kpis->getDimensions());
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getDimensions returns indexed array starting with key 1')]
    public function testKeyPerformanceIndicatorsGetDimensionsReturnsIndexedArrayStartingWithKey1(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $dimensions = $kpis->getDimensions();
        $this->assertIsInt(array_key_first($dimensions));
        $this->assertEquals(1, array_key_first($dimensions));
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getDimensions returns indexed array with non-empty string values')]
    public function testKeyPerformanceIndicatorsGetDimensionsReturnsIndexedArrayWithNonEmptyStringValues()
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $dimensions = $kpis->getDimensions();
        foreach ($dimensions as $key => $dimension) {
            $this->assertIsInt($key);
            $this->assertNotEmpty($dimension);
            $this->assertIsString($dimension);
        }
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getDimensions returns no more than ten (10) dimensions')]
    public function testKeyPerformanceIndicatorsGetDimensionsReturnsNoMoreThanTenDimensions(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $dimensions = $kpis->getDimensions();
        $this->assertLessThanOrEqual(10, count($dimensions));
    }

    #[Depends('testKeyPerformanceIndicatorsGetDimensionsReturnsNonEmptyArray')]
    #[Depends('testKeyPerformanceIndicatorsGetDimensionsReturnsNoMoreThanTenDimensions')]
    #[TestDox('KeyPerformanceIndicators::getDimensions currently returns six or seven dimensions')]
    public function testKeyPerformanceIndicatorsGetDimensionsCurrentlyReturnsSixOrSevenDimensions(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $dimensions = $kpis->getDimensions();
        $this->assertTrue(count($dimensions) === 6 || count($dimensions) === 7);
    }


    #[TestDox('KeyPerformanceIndicators::getMetrics exists')]
    public function testKeyPerformanceIndicatorsGetMetricsExists(): void
    {
        $class = new ReflectionClass(KeyPerformanceIndicators::class);
        $this->assertTrue($class->hasMethod('getMetrics'));
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsExists')]
    #[TestDox('KeyPerformanceIndicators::getMetrics is public')]
    public function testKeyPerformanceIndicatorsGetMetricsIsPublic(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, 'getMetrics');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsExists')]
    #[TestDox('KeyPerformanceIndicators::getMetrics has no parameters')]
    public function testKeyPerformanceIndicatorsGetMetricsHasNoParameters(): void
    {
        $method = new ReflectionMethod(KeyPerformanceIndicators::class, 'getMetrics');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsExists')]
    #[Depends('testKeyPerformanceIndicatorsGetMetricsIsPublic')]
    #[Depends('testKeyPerformanceIndicatorsGetMetricsHasNoParameters')]
    #[TestDox('KeyPerformanceIndicators::getMetrics returns non-empty array')]
    public function testKeyPerformanceIndicatorsGetMetricsReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_stores`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }

        $kpis = new KeyPerformanceIndicators($registry);
        $this->assertNotEmpty($kpis->getMetrics());
        $this->assertIsArray($kpis->getMetrics());
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getMetrics returns indexed array starting with key integer 1')]
    public function testKeyPerformanceIndicatorsGetMetricsReturnsIndexedArrayStartingWithKeyInteger1(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $metrics = $kpis->getMetrics();
        $this->assertIsInt(array_key_first($metrics));
        $this->assertEquals(1, array_key_first($metrics));
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getMetrics returns indexed array with integer values')]
    public function testKeyPerformanceIndicatorsGetMetricsReturnsIndexedArrayWithIntegerValues(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $metrics = $kpis->getMetrics();
        foreach ($metrics as $key => $metric) {
            $this->assertIsInt($key);
            $this->assertIsInt($metric);
        }
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getMetrics no more than ten (10) metrics')]
    public function testKeyPerformanceIndicatorsGetMetricsNoMoreThanTenMetrics(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $metrics = $kpis->getMetrics();
        $this->assertTrue(count($metrics) <= 10);
    }

    #[Depends('testKeyPerformanceIndicatorsGetMetricsReturnsNonEmptyArray')]
    #[TestDox('KeyPerformanceIndicators::getMetrics currently returns three (3) metrics')]
    public function testKeyPerformanceIndicatorsGetMetricsCurrentlyReturnsThreeMetrics(): void
    {
        $registry = Registry::getInstance();
        $kpis = new KeyPerformanceIndicators($registry);
        $metrics = $kpis->getMetrics();
        $this->assertEquals(3, count($metrics));
    }
}
