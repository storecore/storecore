<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\CRM\FoodEstablishment;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\OrganizationMapper::class)]
#[CoversClass(\StoreCore\Database\OrganizationProperties::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\CRM\FoodEstablishment::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\EmailAddressMapper::class)]
#[UsesClass(\StoreCore\Database\EmailAddressRepository::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Types\EmailAddress::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
#[UsesClass(\StoreCore\Types\Varchar::class)]
final class OrganizationMapperTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');

            $db->query('SELECT 1 FROM `sc_email_addresses`');
            $db->query('SELECT 1 FROM `sc_organizations`');

            $db->query("DELETE FROM `sc_email_addresses` WHERE `email_address` = 'secretariat(at)google.org' LIMIT 1");
            $db->query("DELETE FROM `sc_organizations` WHERE `common_name` = 'GreatFood'");
            $db->query("DELETE FROM `sc_organizations` WHERE `common_name` = 'Google.org (GOOG)'");
            $db->query("DELETE FROM `sc_organizations` WHERE `common_name` = 'National Public Radio'");
        } catch (\PDOException $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationMapper class is concrete')]
    public function testOrganizationMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationMapper is a database model')]
    public function testOrganizationMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationMapper implements PSR-20 ClockInterface')]
    public function testOrganizationMapperImplementsPSR20ClockInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));

        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }


    #[Group('hmvc')]
    #[TestDox('OrganizationMapper implements DataMapperInterface')]
    public function testOrganizationMapperImplementsDataMapperInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Database\\DataMapperInterface'));

        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\Database\DataMapperInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrganizationMapper::VERSION);
        $this->assertIsString(OrganizationMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrganizationMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    // OrganizationMapper::create

    #[TestDox('OrganizationMapper::create exists')]
    public function testOrganizationMapperCreateExists(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->hasMethod('create'));
    }

    #[Depends('testOrganizationMapperCreateExists')]
    #[TestDox('OrganizationMapper::create is public')]
    public function testOrganizationMapperCreateIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'create');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testOrganizationMapperCreateExists')]
    #[TestDox('OrganizationMapper::create has one REQUIRED parameter')]
    public function testOrganizationMapperCreateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'create');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationMapperCreateExists')]
    #[Depends('testOrganizationMapperCreateIsPublic')]
    #[Depends('testOrganizationMapperCreateHasOneRequiredParameter')]
    #[TestDox('OrganizationMapper::create requires Organization object')]
    public function testOrganizationMapperCreateRequiresOrganizationObject(): void
    {
        $registry = Registry::getInstance();
        $mapper = new OrganizationMapper($registry);

        // `Person` implements `IdentityInterface` but is not an `Organization`:
        $person = new Person();
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $person);

        $this->expectException(\TypeError::class);
        $failure = $mapper->create($person);
    }

    #[Depends('testOrganizationMapperCreateHasOneRequiredParameter')]
    #[Depends('testOrganizationMapperCreateRequiresOrganizationObject')]
    #[TestDox('OrganizationMapper::create returns bool')]
    public function testOrganizationMapperCreateReturnsBool(): void
    {
        $registry = Registry::getInstance();
        $mapper = new OrganizationMapper($registry);

        $organization = new FoodEstablishment();
        $organization->name = 'GreatFood';
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);

        // A new `Organization` does not yet have a known identity.
        $this->assertFalse($organization->hasIdentifier());
        $this->assertEmpty($organization->metadata->id);

        // Persist the new `Organization`.
        $this->assertTrue($mapper->create($organization));

        // The `Organization` MUST now have a public UUID and local integer ID.
        $this->assertTrue(
            $organization->hasIdentifier(),
            'The persisted `Organization` MUST now have a unique public UUID.'
        );
        $this->assertNotEmpty(
            $organization->metadata->id,
            'The persisted `Organization` metadata MUST now contain a local integer ID.'
        );

        $this->assertFalse(
            $mapper->create($organization),
            'If an `Organization` already exists it SHOULD NOT be created again.'
        );
    }


    // OrganizationMapper::delete

    #[TestDox('OrganizationMapper::delete exists')]
    public function testOrganizationMapperDeleteExists(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testOrganizationMapperDeleteExists')]
    #[TestDox('OrganizationMapper::delete is public')]
    public function testOrganizationMapperDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'delete');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testOrganizationMapperDeleteExists')]
    #[TestDox('OrganizationMapper::delete has one REQUIRED parameter')]
    public function testOrganizationMapperDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationMapperDeleteHasOneRequiredParameter')]
    #[TestDox('OrganizationMapper::delete does not accepts objects')]
    public function testOrganizationMapperDeleteDoesNotAcceptObjects(): void
    {
        $organization = new Organization();
        $organization->name = 'National Public Radio';
        $organization->url = 'https://www.npr.org/';
        $this->assertFalse($organization->hasIdentifier());

        $registry = Registry::getInstance();
        $mapper = new OrganizationMapper($registry);
        $this->assertTrue($mapper->create($organization));

        $this->expectException(\TypeError::class);
        $failure = $mapper->delete($organization);
    }


    // OrganizationMapper::toArray

    #[TestDox('OrganizationMapper::toArray() exists')]
    public function testOrganizationMapperToArrayExists(): void
    {
        $class = new ReflectionClass(OrganizationMapper::class);
        $this->assertTrue($class->hasMethod('toArray'));
    }

    #[Depends('testOrganizationMapperToArrayExists')]
    #[TestDox('OrganizationMapper::toArray() is final public')]
    public function testOrganizationMapperToArrayIsFinalPublic(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'toArray');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationMapperToArrayExists')]
    #[TestDox('OrganizationMapper::toArray() has one REQUIRED parameter')]
    public function testOrganizationMapperToArrayHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationMapper::class, 'toArray');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationMapperToArrayExists')]
    #[Depends('testOrganizationMapperToArrayIsFinalPublic')]
    #[Depends('testOrganizationMapperToArrayHasOneRequiredParameter')]
    #[TestDox('OrganizationMapper::toArray() returns non-empty array')]
    public function testOrganizationMapperToArrayReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $organization = new Organization('Example Organization');
        $mapper = new OrganizationMapper($registry);

        $this->assertNotEmpty($mapper->toArray($organization));
        $this->assertIsArray($mapper->toArray($organization));
    }

    #[Depends('testOrganizationMapperToArrayReturnsNonEmptyArray')]
    #[TestDox('OrganizationMapper::toArray() returns metadata')]
    public function testOrganizationMapperToArrayReturnsMetadata(): void
    {
        $registry = Registry::getInstance();
        $organization = new Organization('Example Organization');
        $mapper = new OrganizationMapper($registry);

        $this->assertArrayHasKey('date_created', $mapper->toArray($organization));
        $this->assertNotNull($mapper->toArray($organization)['date_created']);
        $this->assertIsString($mapper->toArray($organization)['date_created']);
        $this->assertStringStartsWith(gmdate('Y-m-d'), $mapper->toArray($organization)['date_created']);

        $this->assertArrayHasKey('date_modified', $mapper->toArray($organization));
        $this->assertNull($mapper->toArray($organization)['date_modified']);

        $this->assertArrayHasKey('version', $mapper->toArray($organization));
        $this->assertIsInt($mapper->toArray($organization)['version']);
    }

    #[Depends('testOrganizationMapperToArrayReturnsNonEmptyArray')]
    #[TestDox('OrganizationMapper::toArray() returns organization property columns')]
    public function testOrganizationMapperToArrayReturnsOrganizationPropertyColumns(): void
    {
        $registry = Registry::getInstance();
        $organization = new Organization('Example Organization');
        $mapper = new OrganizationMapper($registry);
        $array = $mapper->toArray($organization);

        $this->assertArrayHasKey('common_name', $array);
        $this->assertArrayHasKey('legal_name', $array);
        $this->assertArrayHasKey('alternate_name', $array);

        $this->assertNotEmpty($array['common_name']);
        $this->assertIsString($array['common_name']);
        $this->assertEquals('Example Organization', $array['common_name']);

        $this->assertArrayHasKey('telephone_number', $array);
        $this->assertArrayHasKey('fax_number', $array);
        $this->assertArrayHasKey('website_url', $array);

        $this->assertArrayHasKey('duns_number', $array);
        $this->assertArrayHasKey('isic_code', $array);
        $this->assertArrayHasKey('iso_6523_code', $array);
        $this->assertArrayHasKey('lei_code', $array);
        $this->assertArrayHasKey('naics_code', $array);

        $this->assertArrayHasKey('tax_id', $array);
        $this->assertArrayHasKey('vat_id', $array);

        $this->assertArrayHasKey('founding_date', $array);
        $this->assertArrayHasKey('dissolution_date', $array);

        // Properties moved out to `OrganizationProperties` data mapper.
        $this->assertArrayNotHasKey('additional_type', $array);
        $this->assertArrayNotHasKey('opening_hours', $array);
        $this->assertArrayNotHasKey('ownership_funding_info', $array);
    }


    /**
     * In April 2024, this Schema.org example caused “Uncategorized Errors” for
     * “Duplicate key found” on the repeated `iso6523Code` key.  We therefore
     * rewrote the `iso6523Code` property to an array with two values.
     *
     * @see https://schema.org/Organization#eg-0007
     *      Schema.org `Organization` example 1
     * 
     * @see https://schema.org/iso6523Code
     *      Schema.org `iso6523Code` property of an `Organization`
     */
    #[TestDox('Schema.org acceptance test for an Organization with two ISO 6523 codes')]
    public function testSchemaOrgAcceptanceTestForAnOrganizationWithTwoISO6523Codes(): void
    {
        $expectedJson = trim('
            {
              "@context": "https://schema.org",
              "@type": "Organization",
              "address": {
                "@type": "PostalAddress",
                "addressLocality": "Paris, France",
                "postalCode": "F-75002",
                "streetAddress": "38 avenue de l\'Opéra"
              },
              "email": "secretariat@google.org",
              "faxNumber": "+33142685301",
              "iso6523Code": [
                "0009:44306184100039",
                "9957:FR64443061841"
              ],
              "name": "Google.org (GOOG)",
              "telephone": "+33 1 42 68 53 00"
            }
        ');
        $this->assertJson($expectedJson);

        $organization = new Organization();
        $organization->name = 'Google.org (GOOG)';

        $organization->telephone = "+33 1 42 68 53 00";
        $organization->faxNumber = "+33142685301";
        $organization->email = "secretariat@google.org";

        $organization->iso6523Code = array("0009:44306184100039", "9957:FR64443061841");
        $this->assertNotEmpty($organization->iso6523Code);
        $this->assertIsArray($organization->iso6523Code);
        $this->assertCount(2, $organization->iso6523Code);

        // Unsaved `Organization` has no identity.
        $this->assertEmpty($organization->metadata->id);
        $this->assertFalse($organization->hasIdentifier());

        $registry = Registry::getInstance();
        $mapper = new OrganizationMapper($registry);
        $this->assertTrue($mapper->create($organization));

        // Saved `Organization` has an identity.
        $this->assertNotEmpty($organization->metadata->id);
        $this->assertTrue($organization->hasIdentifier());
    }
}
