<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2017, 2019–2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\Backup::class)]
#[CoversClass(\StoreCore\FileSystem\Backup::class)]
class BackupTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Backup class is concrete')]
    public function testBackupClassIsConcrete(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Backup class file is an admin controller')]
    public function testBackupClassFileIsAdminController(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Admin\Controller::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Backup::VERSION);
        $this->assertIsString(Backup::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Backup::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('Backup::__construct exists')]
    public function testBackupConstructorExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testBackupConstructorExists')]
    #[TestDox('Backup::__construct is public constructor')]
    public function testBackupConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Backup::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testBackupConstructorExists')]
    #[TestDox('Backup::__construct has no parameters')]
    public function testBackupConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Backup::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfParameters());
    }


    #[TestDox('Backup::download exists')]
    public function testBackupDownloadExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('download'));
    }

    #[Depends('testBackupDownloadExists')]
    #[TestDox('Backup::download is public')]
    public function testBackupDownloadIsPublic(): void
    {
        $method = new ReflectionMethod(Backup::class, 'download');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBackupDownloadExists')]
    #[TestDox('Backup::download has no parameters')]
    public function testBackupDownloadHasNoParameters(): void
    {
        $method = new ReflectionMethod(Backup::class, 'download');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('Backup::getFiles exists')]
    public function testBackupGetFilesExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('getFiles'));
    }

    #[Depends('testBackupGetFilesExists')]
    #[TestDox('Backup::getFiles is public static')]
    public function testBackupGetFilesIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Backup::class, 'getFiles');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testBackupGetFilesExists')]
    #[TestDox('Backup::getFiles has no parameters')]
    public function testBackupGetFilesHasNoParameters(): void
    {
        $method = new ReflectionMethod(Backup::class, 'getFiles');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBackupGetFilesExists')]
    #[Depends('testBackupGetFilesIsPublicStatic')]
    #[Depends('testBackupGetFilesHasNoParameters')]
    #[TestDox('Backup::getFiles returns array')]
    public function testBackupGetFilesReturnsArray(): void
    {
        $this->assertIsArray(Backup::getFiles());
    }


    #[TestDox('Backup::save exists')]
    public function testBackupSaveExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('save'));
    }

    #[Depends('testBackupSaveExists')]
    #[TestDox('Backup::save is public static')]
    public function testBackupSaveIsPublic(): void
    {
        $method = new ReflectionMethod(Backup::class, 'save');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testBackupSaveExists')]
    #[TestDox('Backup::save has two OPTIONAL parameters')]
    public function testBackupSaveHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(Backup::class, 'save');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }
}
