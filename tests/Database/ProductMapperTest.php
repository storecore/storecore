<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\PIM\Product;
use StoreCore\Types\UUID;

use \ReflectionClass, \ReflectionClassConstant;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\IdentityMap::class)]
#[CoversClass(\StoreCore\Database\ProductAssociations::class)]
#[CoversClass(\StoreCore\Database\ProductMapper::class)]
#[CoversClass(\StoreCore\PIM\ItemAvailability::class)]
#[CoversClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ProductMapperTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ProductMapper class is concrete')]
    public function testProductMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(ProductMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ProductMapper is a database model')]
    public function testProductMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(ProductMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPsr11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPsr11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ProductMapper implements PSR-11 ContainerInterface')]
    public function testProductMapperImplementsPsr11ContainerInterface(): void
    {
        $class = new ReflectionClass(ProductMapper::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('ProductMapper is countable')]
    public function testProductMapperIsCountable(): void
    {
        $class = new ReflectionClass(ProductMapper::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ProductMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ProductMapper::VERSION);
        $this->assertIsString(ProductMapper::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ProductMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ProductMapper::get returns stored product')]
    public function testProductMapperGetReturnsStoredProduct(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        /*
         * @see https://www.php.net/manual/en/pdo.exec.php
         *   `PDO::exec()` returns `(int) 0` or `(int) 1` on success
         *   and `(bool) false` on failure, so we can run these return
         *   values to run small unit tests on database setup.
         */
        $db = $registry->get('Database');
        $this->assertIsInt(
            $db->exec(
                "DELETE FROM `sc_products`
                  WHERE `product_id` = 1
                     OR `product_uuid` = UNHEX('487e37719ce03694aab13f1b0e135a86')"
            )
        );

        /*
         * Insert a simple product with an UUIDv3, a GTIN (ISBN),
         * a release date and the current time as creation data.
         */
        $this->assertIsInt(
            $db->exec(
                "INSERT INTO `sc_products`
                     (`product_id`, `product_uuid`, `gtin`, `release_date`, `date_created`)
                   VALUES
                     (1, UNHEX('487e37719ce03694aab13f1b0e135a86'), '9789043017121', '2008-11-15', UTC_TIMESTAMP())"
            )
        );

        $mapper = new ProductMapper($registry);
        $this->assertGreaterThanOrEqual(1, $mapper->count());

        $product = $mapper->get(1);
        $this->assertInstanceOf(\StoreCore\PIM\Product::class, $product);

        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $product);
        $this->assertTrue($product->hasIdentifier());
        $this->assertEquals('487e3771-9ce0-3694-aab1-3f1b0e135a86', (string) $product->getIdentifier());

        $this->assertNotEmpty($product->availability);
        $this->assertInstanceOf(\StoreCore\PIM\ItemAvailability::class, $product->availability);
        $this->assertEquals(
            'https://schema.org/InStock', $product->availability->value,
            'Default product availability MUST be `https://schema.org/InStock`.'
        );

        $this->assertNotEmpty($product->itemCondition);
        $this->assertInstanceOf(\StoreCore\PIM\OfferItemCondition::class, $product->itemCondition);
        $this->assertEquals(
            'https://schema.org/NewCondition', $product->itemCondition->value,
            'Default product condition MUST be `https://schema.org/NewCondition`.'
        );

        $this->assertNotEmpty($product->releaseDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->releaseDate);
        $this->assertEquals('2008-11-15', $product->releaseDate->format('Y-m-d'));

        $this->assertInstanceOf(\JsonSerializable::class, $product);
        $expectedJson = '
            {
              "@id": "/api/v1/products/487e3771-9ce0-3694-aab1-3f1b0e135a86",
              "@context": "https://schema.org",
              "@type": "Product",
              "gtin13": "9789043017121",
              "identifier": "487e3771-9ce0-3694-aab1-3f1b0e135a86",
              "itemCondition": "https://schema.org/NewCondition",
              "releaseDate": "2008-11-15"
            }
        ';
        $actualJson = json_encode($product, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[TestDox('ProductMapper::get throws PSR-11 NotFoundExceptionInterface if product does not exist')]
    public function testProductMapperGetThrowsPsr11NotFoundExceptionInterfaceIfProductDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $mapper = new ProductMapper($registry);
        $this->assertFalse($mapper->has('0'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $mapper->get('0');
    }


    #[Depends('testProductMapperGetReturnsStoredProduct')]
    #[TestDox('ProductMapper::has returns (bool) true if product exists')]
    public function testProductMapperHasReturnsBoolTrueIfProductExists(): void
    {
        $registry = Registry::getInstance();
        $mapper = new ProductMapper($registry);
        $this->assertTrue(
            $mapper->has('1'),
            'Productmapper::has MUST accept primary key as numeric string.'
        );
        $this->assertTrue(
            $mapper->has(1),
            'Productmapper::has SHOULD accept primary key as integer.'
        );

        $uuid = new UUID('487e3771-9ce0-3694-aab1-3f1b0e135a86');
        $this->assertTrue(
            $mapper->has($uuid->__toString()),
            'Productmapper::has MUST accept UUID as string.'
        );
    }

    #[Depends('testProductMapperHasReturnsBoolTrueIfProductExists')]
    #[TestDox('ProductMapper::has returns (bool) false if product does not exist')]
    public function testProductMapperHasReturnsBoolFalseIfProductDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $mapper = new ProductMapper($registry);
        $this->assertFalse($mapper->has('0'));
        $this->assertFalse($mapper->has(0));
        $this->assertFalse($mapper->has('e863a010-3537-11ef-9454-0242ac120002'));
    }
}
