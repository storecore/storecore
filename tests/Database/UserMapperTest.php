<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \strtolower;
use function \version_compare;

#[CoversClass(\StoreCore\Database\UserMapper::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
final class UserMapperTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('UserMapper is a database model')]
    public function testUserMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Depends('testUserMapperIsDatabaseModel')]
    #[Group('hmvc')]
    #[TestDox('UserMapper is abstract data access object (DAO)')]
    public function testUserMapperIsAbstractDataAccessObjectDAO(): void
    {
        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractDataAccessObject::class));
    }

    #[Group('hmvc')]
    #[TestDox('UserMapper implements CRUD interface')]
    public function testUserMapperImplementsCrudInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Database\\CRUDInterface'));

        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\Database\CRUDInterface::class));
    }


    #[Group('hmvc')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UserMapper::VERSION);
        $this->assertIsString(UserMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('hmvc')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UserMapper::VERSION, '0.4.3', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('TABLE_NAME constant is defined')]
    public function testTableNameConstantIsDefined(): void
    {
        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->hasConstant('TABLE_NAME'));
    }

    #[Depends('testTableNameConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('TABLE_NAME constant is non-empty lowercase string')]
    public function testTableNameConstantIsNonEmptyLowercaseString(): void
    {
        $this->assertNotEmpty(UserMapper::TABLE_NAME);
        $this->assertIsString(UserMapper::TABLE_NAME);
        $this->assertEquals(strtolower(UserMapper::TABLE_NAME), UserMapper::TABLE_NAME);
    }

    #[Depends('testTableNameConstantIsNonEmptyLowercaseString')]
    #[Group('hmvc')]
    #[TestDox('TABLE_NAME is sc_users')]
    public function testTableNameIsScUsers(): void
    {
        $this->assertEquals('sc_users', UserMapper::TABLE_NAME);
    }


    #[Group('hmvc')]
    #[TestDox('PRIMARY_KEY constant is defined')]
    public function testPrimaryKeyConstantIsDefined(): void
    {
        $class = new ReflectionClass(UserMapper::class);
        $this->assertTrue($class->hasConstant('PRIMARY_KEY'));
    }

    #[Depends('testPrimaryKeyConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('PRIMARY_KEY constant is non-empty lowercase string')]
    public function testPrimaryKeyConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UserMapper::PRIMARY_KEY);
        $this->assertIsString(UserMapper::PRIMARY_KEY);
        $this->assertSame(strtolower(UserMapper::PRIMARY_KEY), UserMapper::PRIMARY_KEY);
    }

    #[Depends('testPrimaryKeyConstantIsNonEmptyString')]
    #[Group('hmvc')]
    #[TestDox('PRIMARY_KEY is user_uuid')]
    public function testPrimaryKeyIsUserUUID(): void
    {
        $this->assertEquals('user_uuid', UserMapper::PRIMARY_KEY);
    }
}
