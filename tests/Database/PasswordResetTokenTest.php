<?php

declare(strict_types=1);

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[Group('security')]
#[CoversClass(\StoreCore\Database\PasswordResetToken::class)]
final class PasswordResetTokenTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PasswordResetToken class exists')]
    public function testPasswordResetTokenClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'PasswordResetToken.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'PasswordResetToken.php');

        $this->assertTrue(class_exists('\\StoreCore\\Database\\PasswordResetToken'));
        $this->assertTrue(class_exists(PasswordResetToken::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PasswordResetToken::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PasswordResetToken::VERSION);
        $this->assertIsString(PasswordResetToken::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PasswordResetToken::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('PasswordResetToken::CHARACTER_SET constant is defined')]
    public function testPasswordResetTokenCharacterSetConstantIsDefined(): void
    {
        $class = new ReflectionClass(PasswordResetToken::class);
        $this->assertTrue($class->hasConstant('CHARACTER_SET'));
    }

    #[Depends('testPasswordResetTokenCharacterSetConstantIsDefined')]
    #[TestDox('PasswordResetToken::CHARACTER_SET is a string')]
    public function testPasswordResetTokenCharacterSetConstantIsString(): void
    {
        $this->assertIsString(PasswordResetToken::CHARACTER_SET);
    }

    #[Depends('testPasswordResetTokenCharacterSetConstantIsDefined')]
    #[TestDox('PasswordResetToken::CHARACTER_SET is an uppercase string')]
    public function testPasswordResetTokenCharacterSetConstantIsUppercaseString(): void
    {
        $this->assertEquals(
            strtoupper(PasswordResetToken::CHARACTER_SET),
            PasswordResetToken::CHARACTER_SET
        );
        
        if (function_exists('mb_strtoupper')) {
            $this->assertEquals(
                mb_strtoupper(PasswordResetToken::CHARACTER_SET),
                PasswordResetToken::CHARACTER_SET
            );
        }
    }

    #[Depends('testPasswordResetTokenCharacterSetConstantIsString')]
    #[TestDox('PasswordResetToken::CHARACTER_SET does not contain digits 0 and 1')]
    public function testPasswordResetTokenCharacterSetDoesNotContainDigits0And1(): void
    {
        $this->assertStringNotContainsString('0', PasswordResetToken::CHARACTER_SET);
        $this->assertStringNotContainsString('1', PasswordResetToken::CHARACTER_SET);
    }

    #[Depends('testPasswordResetTokenCharacterSetConstantIsString')]
    #[TestDox('PasswordResetToken::CHARACTER_SET does not contain letters I and O')]
    public function testPasswordResetTokenCharacterSetDoesNotContainLettersIAndO(): void
    {
        $this->assertStringNotContainsString('I', PasswordResetToken::CHARACTER_SET);
        $this->assertStringNotContainsString('O', PasswordResetToken::CHARACTER_SET);
    }


    #[TestDox('PasswordResetToken::getInstance exists')]
    public function testPasswordResetTokenGetInstanceExists(): void
    {
        $class = new ReflectionClass(PasswordResetToken::class);
        $this->assertTrue($class->hasMethod('getInstance'));
    }

    #[Depends('testPasswordResetTokenGetInstanceExists')]
    #[TestDox('PasswordResetToken::getInstance is public static')]
    public function testPasswordResetTokenGetInstanceIsPublicStatic(): void
    {
        $method = new ReflectionMethod(PasswordResetToken::class, 'getInstance');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testPasswordResetTokenGetInstanceExists')]
    #[TestDox('PasswordResetToken::getInstance has one OPTIONAL parameter')]
    public function testPasswordResetTokenGetInstanceHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(PasswordResetToken::class, 'getInstance');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PasswordResetToken::getInstance returns uppercase string')]
    public function testPasswordResetTokenGetInstanceReturnsUppercaseString(): void
    {
        $passkey = PasswordResetToken::getInstance();
        $this->assertEquals(strtoupper($passkey), $passkey);

        if (function_exists('mb_strtoupper')) {
            $this->assertEquals(mb_strtoupper($passkey), $passkey);
        }
    }

    #[TestDox('PasswordResetToken::getInstance returns sixteen (16) characters by default')]
    public function testPasswordResetTokenGetInstanceReturnsSixteenCharactersByDefault(): void
    {
        $passkey = PasswordResetToken::getInstance();
        $this->assertEquals(16, strlen($passkey));

        if (function_exists('mb_strlen')) {
            $this->assertEquals(16, mb_strlen($passkey));
        }
    }

    #[Depends('testPasswordResetTokenGetInstanceHasOneOptionalParameter')]
    #[Depends('testPasswordResetTokenGetInstanceReturnsSixteenCharactersByDefault')]
    #[TestDox('PasswordResetToken::getInstance accepts string length as integer')]
    public function testPasswordResetTokenGetInstanceAcceptsStringLengthAsInteger(): void
    {
        $passkey = PasswordResetToken::getInstance(25);
        $this->assertNotEquals(16, strlen($passkey));
        $this->assertEquals(25, strlen($passkey));

        if (function_exists('mb_strlen')) {
            $this->assertEquals(25, mb_strlen($passkey));
        }
    }
}
