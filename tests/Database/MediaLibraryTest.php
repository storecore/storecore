<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\MediaLibrary::class)]
class MediaLibraryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('MediaLibrary class is concrete')]
    public function testMediaLibraryClassIsConcrete(): void
    {
        $class = new ReflectionClass(MediaLibrary::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MediaLibrary class is a database model')]
    public function testMediaLibraryClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(MediaLibrary::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MediaLibrary::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MediaLibrary::VERSION);
        $this->assertIsString(MediaLibrary::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MediaLibrary::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('MediaLibrary::getFileNumber exists')]
    public function testMediaLibraryGetFileNumberExists(): void
    {
        $class = new ReflectionClass(MediaLibrary::class);
        $this->assertTrue($class->hasMethod('getFileNumber'));
    }

    #[Depends('testMediaLibraryGetFileNumberExists')]
    #[TestDox('MediaLibrary::getFileNumber is public')]
    public function testMediaLibraryGetFileNumberIsPublic(): void
    {
        $method = new ReflectionMethod(MediaLibrary::class, 'getFileNumber');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testMediaLibraryGetFileNumberExists')]
    #[TestDox('MediaLibrary::getFileNumber has one REQUIRED parameter')]
    public function testMediaLibraryGetFileNumberHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(MediaLibrary::class, 'getFileNumber');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
