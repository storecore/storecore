<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Database\Connection;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\Large;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\Maintenance::class)]
#[Large()]
#[UsesClass(\StoreCore\Registry::class)]
final class MaintenanceTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        try {
            $registry = Registry::getInstance();
            if (!$registry->has('Database')) {
                $registry->set('Database', new Connection());
            }
            $registry->get('Database')->query('SELECT 1 FROM `sc_orders`');
        } catch (\Exception $e) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    }


    #[Group('hmvc')]
    #[TestDox('Database Maintenance class is concrete')]
    public function testDatabaseMaintenanceClassIsConcrete(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Database Maintenance class is a StoreCore database model')]
    public function testDatabaseMaintenanceClassIsStoreCoreDatabaseModel(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Maintenance::VERSION);
        $this->assertIsString(Maintenance::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches development branch')]
    public function testVersionConstantMatchesDevelopmentBranch(): void
    {
        $this->assertTrue(
            version_compare(Maintenance::VERSION, '1.0.0-alpha.1', '==')
        );
    }


    #[TestDox('Maintenance::__construct exists')]
    public function testMaintenanceConstructorExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testMaintenanceConstructorExists')]
    #[TestDox('Maintenance::__construct is public constructor')]
    public function testMaintenanceConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Maintenance::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testMaintenanceConstructorExists')]
    #[TestDox('Maintenance::__construct has one REQUIRED parameter')]
    public function testMaintenanceConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Maintenance::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Maintenance::$updateAvailable exists')]
    public function testMaintenanceUpdateAvailableExists(): void
    {
        $registry = Registry::getInstance();
        $maintance = new Maintenance($registry);
        $this->assertObjectHasProperty('updateAvailable', $maintance);
    }

    #[Depends('testMaintenanceUpdateAvailableExists')]
    #[TestDox('Maintenance::$updateAvailable is public readonly')]
    public function testMaintenanceUpdateAvailableIsPublic(): void
    {
        $property = new ReflectionProperty(Maintenance::class, 'updateAvailable');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testMaintenanceUpdateAvailableExists')]
    #[TestDox('Maintenance::$updateAvailable is (bool) true by default')]
    public function testMaintenanceUpdateAvailableIsBoolTrueByDefault(): void
    {
        $registry = Registry::getInstance();
        $maintance = new Maintenance($registry);
        $this->assertIsBool($maintance->updateAvailable);
        $this->assertTrue($maintance->updateAvailable);
    }


    #[TestDox('Maintenance::emptyRecycleBin exists')]
    public function testMaintenanceEmptyRecycleBinExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('emptyRecycleBin'));
    }

    #[Depends('testMaintenanceEmptyRecycleBinExists')]
    #[TestDox('Maintenance::emptyRecycleBin is public')]
    public function testMaintenanceEmptyRecycleBinIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'emptyRecycleBin');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceEmptyRecycleBinExists')]
    #[TestDox('Maintenance::emptyRecycleBin has one OPTIONAL parameter')]
    public function testPublicEmptyRecycleBinHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'emptyRecycleBin');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMaintenanceEmptyRecycleBinExists')]
    #[TestDox('Maintenance::emptyRecycleBin returns integer')]
    public function testPublicEmptyRecycleBinReturnsInteger(): void
    {
        $registry = Registry::getInstance();
        $maintance = new Maintenance($registry);

        $this->assertIsInt($maintance->emptyRecycleBin());
        $this->assertEquals(
            0, $maintance->emptyRecycleBin(),
            'The first call to Maintenance::emptyRecycleBin SHOULD have cleared all deleted rows.'
        );
    }


    #[TestDox('Maintenance::getSize exists')]
    public function testMaintenanceGetSizeExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('getSize'));
    }

    #[Depends('testMaintenanceGetSizeExists')]
    #[TestDox('Maintenance::getSize is public')]
    public function testMaintenanceGetSizeIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'getSize');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceGetSizeExists')]
    #[TestDox('Maintenance::getSize has no parameters')]
    public function testMaintenanceGetSizeHasNoParameters(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'getSize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMaintenanceGetSizeExists')]
    #[Depends('testMaintenanceGetSizeIsPublic')]
    #[Depends('testMaintenanceGetSizeHasNoParameters')]
    #[TestDox('Maintenance::getSize returns integer or false')]
    public function testMaintenanceGetSizeReturnsIntegerOrFalse(): void
    {
        $registry = Registry::getInstance();
        $db = new Maintenance($registry);
        $size = $db->getSize();
        $this->assertTrue(\is_int($size) || $size === false);
    }

    #[Depends('testMaintenanceGetSizeReturnsIntegerOrFalse')]
    #[TestDox('Maintenance::getSize returns database size in bytes as integer')]
    public function testMaintenanceGetSizeReturnsDatabaseSizeInBytesAsInteger(): void
    {
        $registry = Registry::getInstance();
        $db = new Maintenance($registry);
        $this->assertNotFalse($db->getSize());
        $this->assertIsInt($db->getSize());
        $this->assertGreaterThan(0, $db->getSize());
    }


    #[TestDox('Maintenance::getTables exists')]
    public function testMaintenanceGetTablesExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('getTables'));
    }

    #[Depends('testMaintenanceGetTablesExists')]
    #[TestDox('Maintenance::getTables is public')]
    public function testMaintenanceGetTablesIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'getTables');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceGetTablesExists')]
    #[TestDox('Maintenance::getTables has no parameters')]
    public function testMaintenanceGetTablesHasNoParameters(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'getTables');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMaintenanceGetTablesExists')]
    #[TestDox('Maintenance::getTables returns array')]
    public function testMaintenanceGetReturnsArray(): void
    {
        $registry = Registry::getInstance();
        $db = new Maintenance($registry);
        $this->assertIsArray($db->getTables());

        if (!empty($db->getTables())) {
            $this->assertCount(
                131, $db->getTables(),
                'The StoreCore database SHOULD contain 131 tables once it is installed.'
            );
        }
    }


    #[TestDox('Maintenance::optimize exists')]
    public function testMaintenanceOptimizeExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('optimize'));
    }

    #[Depends('testMaintenanceOptimizeExists')]
    #[TestDox('Maintenance::optimize is public')]
    public function testMaintenanceOptimizeIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'optimize');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceOptimizeExists')]
    #[TestDox('Maintenance::optimize has one OPTIONAL parameter')]
    public function testMaintenanceOptimizeHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'optimize');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMaintenanceOptimizeExists')]
    #[TestDox('Maintenance::optimize returns (bool) true on success')]
    public function testMaintenanceOptimizeReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $db = new Maintenance($registry);
        $this->assertTrue($db->optimize());
    }


    #[TestDox('Maintenance::restore exists')]
    public function testMaintenanceRestoreExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('restore'));
    }

    #[Depends('testMaintenanceRestoreExists')]
    #[TestDox('Maintenance::restore is public')]
    public function testMaintenanceRestoreIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'restore');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceRestoreExists')]
    #[TestDox('Maintenance::restore has one OPTIONAL parameter')]
    public function testMaintenanceRestoreHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'restore');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMaintenanceRestoreExists')]
    #[TestDox('Maintenance::restore returns (bool) true on success')]
    public function testMaintenanceRestoreReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $db = new Maintenance($registry);
        $this->assertTrue($db->restore());
    }


    #[TestDox('Maintenance::update exists')]
    public function testMaintenanceUpdateExists(): void
    {
        $class = new ReflectionClass(Maintenance::class);
        $this->assertTrue($class->hasMethod('update'));
    }

    #[Depends('testMaintenanceUpdateExists')]
    #[TestDox('Maintenance::update is public')]
    public function testMaintenanceUpdateIsPublic(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'update');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMaintenanceUpdateExists')]
    #[TestDox('Maintenance::update has no parameters')]
    public function testMaintenanceUpdateHasNoParameters(): void
    {
        $method = new ReflectionMethod(Maintenance::class, 'update');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
