<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \defined;
use function \version_compare;

#[CoversClass(\StoreCore\Database\DataSourceName::class)]
final class DataSourceNameTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Data Source Name class is concrete')]
    public function testDataSourceNameClassIsConcrete(): void
    {
        $class = new ReflectionClass(DataSourceName::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Data Source Name is stringable')]
    public function testDataSourceNameIsStringable(): void
    {
        $class = new ReflectionClass(DataSourceName::class);
        $this->assertTrue($class->hasMethod('__toString'));
        $this->assertTrue($class->implementsInterface(\Stringable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DataSourceName::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DataSourceName::VERSION);
        $this->assertIsString(DataSourceName::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DataSourceName::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('DataSourceName::__construct exists')]
    public function testDataSourceNameConstructorExists(): void
    {
        $class = new ReflectionClass(DataSourceName::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testDataSourceNameConstructorExists')]
    #[TestDox('DataSourceName::__construct is public constructor')]
    public function testDataSourceNameConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(DataSourceName::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testDataSourceNameConstructorExists')]
    #[TestDox('DataSourceName::__construct has no parameters')]
    public function testDataSourceNameConstructorHasNoParameters(): void
    {
        $method = new ReflectionMethod(DataSourceName::class, '__construct');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testDataSourceNameConstructorHasNoParameters')]
    #[TestDox('DataSourceName::__construct sets global database configuration constants')]
    public function testDataSourceNameConstructorSetsGlobalDatabaseConfigurationConstants(): void
    {
        $dsn = new DataSourceName();

        if (defined('STORECORE_DATABASE_DRIVER')) {
            $this->assertSame(STORECORE_DATABASE_DRIVER, $dsn->prefix);
        }

        if (defined('STORECORE_DATABASE_DEFAULT_HOST')) {
            $this->assertSame(STORECORE_DATABASE_DEFAULT_HOST, $dsn->host);
        }

        if (defined('STORECORE_DATABASE_DEFAULT_DATABASE')) {
            $this->assertSame(STORECORE_DATABASE_DEFAULT_DATABASE, $dsn->dbname);
        }
    }


    #[TestDox('DataSourceName::__toString exists')]
    public function testDataSourceNameToStringExists(): void
    {
        $class = new ReflectionClass(DataSourceName::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testDataSourceNameToStringExists')]
    #[TestDox('DataSourceName::__toString is public')]
    public function testDataSourceNameToStringIsPublic(): void
    {
        $method = new ReflectionMethod(DataSourceName::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testDataSourceNameToStringExists')]
    #[TestDox('DataSourceName::__toString has no parameters')]
    public function testDataSourceNameToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(DataSourceName::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testDataSourceNameToStringExists')]
    #[Depends('testDataSourceNameToStringIsPublic')]
    #[Depends('testDataSourceNameToStringHasNoParameters')]
    #[TestDox('DataSourceName::__toString returns non-empty string')]
    public function testDataSourceNameToStringReturnsNonEmptyString(): void
    {
        $dsn = new DataSourceName();
        $this->assertNotEmpty((string) $dsn);
        $this->assertIsString((string) $dsn);
    }


    #[TestDox('DataSourceName::$prefix exists')]
    public function testDataSourceNamePrefixExists(): void
    {
        $dsn = new DataSourceName();
        $this->assertObjectHasProperty('prefix', $dsn);
    }

    #[Depends('testDataSourceNamePrefixExists')]
    #[TestDox('DataSourceName::$prefix is string "mysql" or "mysql:"')]
    public function testDataSourceNameIsStringMysql(): void
    {
        $dsn = new DataSourceName();
        $this->assertNotEmpty($dsn->prefix);
        $this->assertIsString($dsn->prefix);
        $this->assertTrue($dsn->prefix === 'mysql' || $dsn->prefix === 'mysql:');
    }

    #[Depends('testDataSourceNamePrefixExists')]
    #[Depends('testDataSourceNameToStringReturnsNonEmptyString')]
    #[TestDox('DataSourceName::__toString starts with DataSourceName::$prefix')]
    public function testDataSourceNameToStringStartsWithDataSourceNamePrefix(): void
    {
        $dsn = new DataSourceName();
        $this->assertStringStartsWith($dsn->prefix, (string) $dsn);
    }


    #[TestDox('DataSourceName::$host exists')]
    public function testDataSourceNameHostExists(): void
    {
        $dsn = new DataSourceName();
        $this->assertObjectHasProperty('host', $dsn);
    }

    #[Depends('testDataSourceNameHostExists')]
    #[TestDox('DataSourceName::$host is string "localhost" by default')]
    public function testDataSourceNameHostIsStringLocalhostByDefault(): void
    {
        $dsn = new DataSourceName();
        $this->assertIsString($dsn->host);
        $this->assertEquals('localhost', $dsn->host);
    }

    #[Depends('testDataSourceNameToStringReturnsNonEmptyString')]
    #[TestDox('DataSourceName::__toString returns DataSourceName::$host')]
    public function testDataSourceNameToStringReturnsDataSourceNameHost(): void
    {
        $dsn = new DataSourceName();
        $this->assertStringContainsString('host=' . $dsn->host, (string) $dsn);

        $dsn->host = 'localhost';
        $this->assertStringContainsString('host=localhost', (string) $dsn);

        $dsn->host = '127.0.0.1';
        $this->assertStringContainsString('host=127.0.0.1', (string) $dsn);
    }


    #[TestDox('DataSourceName::$port exists')]
    public function testDataSourceNamePortExists(): void
    {
        $dsn = new DataSourceName();
        $this->assertObjectHasProperty('port', $dsn);
    }

    #[Depends('testDataSourceNamePortExists')]
    #[TestDox('DataSourceName::$port is null by default')]
    public function testDataSourceNamePortIsNullByDefault(): void
    {
        $dsn = new DataSourceName();
        $this->assertNull($dsn->port);
    }

    #[Depends('testDataSourceNamePortExists')]
    #[TestDox('DataSourceName::$port must be set as string')]
    public function testDataSourceNamePortMustBeSetAsString(): void
    {
        $dsn = new DataSourceName();
        $dsn->port = '3307';
        $this->assertIsString($dsn->port);
        $this->assertEquals('3307', $dsn->port);

        $dsn = new DataSourceName();
        $this->expectException(\TypeError::class);
        $dsn->port = 3307;
    }

    #[Depends('testDataSourceNamePortExists')]
    #[Depends('testDataSourceNameToStringReturnsNonEmptyString')]
    #[TestDox('DataSourceName::toString returns DataSourceName::$port')]
    public function testDataSourceNameToStringReturnsDataSourceNamePort(): void
    {
        $dsn = new DataSourceName();
        $this->assertStringNotContainsString('port=', (string) $dsn);

        $dsn->port = '3307';
        $this->assertStringContainsString('port=', (string) $dsn);
        $this->assertStringContainsString('port=3307', (string) $dsn);
    }


    #[TestDox('DataSourceName.dbname exists')]
    public function testDataSourceNameDbnameExists(): void
    {
        $dsn = new DataSourceName();
        $this->assertObjectHasProperty('dbname', $dsn);
    }

    #[TestDox('DataSourceName.dbname is null by default')]
    public function testDataSourceNameDbnameIsNullByDefault(): void
    {
        $property = new ReflectionProperty(DataSourceName::class, 'dbname');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isDefault());
        $this->assertNull($property->getDefaultValue());
    }

    #[Depends('testDataSourceNameDbnameExists')]
    #[Depends('testDataSourceNameDbnameIsNullByDefault')]
    #[TestDox('DataSourceName::toString returns DataSourceName.dbname')]
    public function testDataSourceNameToStringReturnsDataSourceNameDbname(): void
    {
        $dsn = new DataSourceName();

        if (defined('STORECORE_DATABASE_DEFAULT_DATABASE')) {
            $this->assertSame(STORECORE_DATABASE_DEFAULT_DATABASE, $dsn->dbname);
            $this->assertStringContainsString('dbname=' . STORECORE_DATABASE_DEFAULT_DATABASE, (string) $dsn);
        }

        $dsn->dbname = 'castor';
        $this->assertStringContainsString('dbname=castor', (string) $dsn);

        $dsn->dbname = 'pollux';
        $this->assertStringContainsString('dbname=pollux', (string) $dsn);
    }


    #[TestDox('DataSourceName::$charset exists')]
    public function testDataSourceNameCharsetExists(): void
    {
        $dsn = new DataSourceName();
        $this->assertObjectHasProperty('charset', $dsn);
    }

    #[Depends('testDataSourceNameCharsetExists')]
    #[TestDox('DataSourceName::$charset is string "utf8mb4" by default')]
    public function testDataSourceNameCharsetIsStringUtf8mb4ByDefault(): void
    {
        $dsn = new DataSourceName();
        $this->assertNotEmpty($dsn->charset);
        $this->assertIsString($dsn->charset);
        $this->assertEquals('utf8mb4', $dsn->charset);
    }

    #[Depends('testDataSourceNameCharsetExists')]
    #[Depends('testDataSourceNameToStringReturnsNonEmptyString')]
    #[TestDox('DataSourceName::toString returns DataSourceName::$charset')]
    public function testDataSourceNameToStringReturnsDataSourceNameCharset(): void
    {
        $dsn = new DataSourceName();
        $this->assertStringContainsString('charset=' . $dsn->charset, (string) $dsn);
    }

    #[Depends('testDataSourceNameCharsetIsStringUtf8mb4ByDefault')]
    #[Depends('testDataSourceNameToStringReturnsNonEmptyString')]
    #[TestDox('DataSourceName::toString returns DataSourceName::$charset')]
    public function testDataSourceNameToStringReturnsCharsetIsUtf8mb4(): void
    {
        $dsn = new DataSourceName();
        $this->assertStringContainsString('charset=utf8mb4', (string) $dsn);
    }

    #[TestDox('DataSourceName::$charset can be set to other character set')]
    public function testDataSourceNameCharsetCanBeSetToOtherCharacterSet(): void
    {
        $dsn = new DataSourceName();
        $this->assertNotSame('utf8', $dsn->charset);

        $dsn->charset = 'utf8';
        $this->assertSame('utf8', $dsn->charset);
        $this->assertStringContainsString('charset=utf8', (string) $dsn);
    }
}
