<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\Blocklist::class)]
#[CoversClass(\StoreCore\Database\BlocklistReader::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
class BlocklistTest extends TestCase
{
    protected function setUp(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');
            $db->query('SELECT 1 FROM `sc_ip_blocklist`');
            $db->query('SELECT 1 FROM `sc_ip_blocklist_comments`');

            $db->exec("DELETE FROM `sc_ip_blocklist_comments` WHERE `ip_address` = '203.0.113.255'");
            $db->exec("DELETE FROM `sc_ip_blocklist` WHERE `ip_address` = '203.0.113.255'");

            $db->exec("DELETE FROM `sc_ip_blocklist` WHERE `ip_address` IN ('198.51.100.0', '2001:db8:ffff:ffff:ffff:ffff:ffff:ffff')");
            $db->exec("INSERT INTO `sc_ip_blocklist` (`ip_address`, `from_date`) VALUES ('198.51.100.0', '2023-02-01 01:02:03')");
            $db->exec("INSERT INTO `sc_ip_blocklist` (`ip_address`, `from_date`) VALUES ('2001:db8:ffff:ffff:ffff:ffff:ffff:ffff', '2024-03-02 12:34:56')");
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('Blocklist class is concrete')]
    public function testBlocklistClassIsConcrete(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Blocklist class is a database model')]
    public function testBlocklistClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('Blocklist is a BlocklistReader')]
    public function testBlocklistIsABlocklistReader(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->isSubclassOf(BlocklistReader::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Blocklist::VERSION);
        $this->assertIsString(Blocklist::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Blocklist::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Blocklist::clear exists')]
    public function testBlocklistClearExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[Depends('testBlocklistClearExists')]
    #[TestDox('Blocklist::clear is public')]
    public function testBlocklistClearIsPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'clear');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testBlocklistClearExists')]
    #[TestDox('Blocklist::clear has no parameters')]
    public function testBlocklistClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBlocklistClearExists')]
    #[Depends('testBlocklistClearIsPublic')]
    #[Depends('testBlocklistClearHasNoParameters')]
    #[TestDox('Blocklist::clear returns (bool) true on success')]
    public function testBlocklistClearReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);
        $result = $blocklist->clear();
        $this->assertIsBool($result);
        $this->assertTrue($result);
    }


    #[TestDox('Blocklist::create exists')]
    public function testBlocklistCreateExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('create'));
    }

    #[Depends('testBlocklistCreateExists')]
    #[TestDox('Blocklist::create is public')]
    public function testBlocklistCreateIsPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'create');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testBlocklistCreateExists')]
    #[TestDox('Blocklist::create has two parameters')]
    public function testBlocklistCreateHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'create');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testBlocklistCreateHasTwoParameters')]
    #[TestDox('Blocklist::create has one REQUIRED parameter')]
    public function testBlocklistCreateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'create');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBlocklistCreateIsPublic')]
    #[Depends('testBlocklistCreateHasOneRequiredParameter')]
    #[TestDox('Blocklist::create throws value error exception on invalid IP address')]
    public function testBlocklistCreateThrowsValueErrorExceptionOnInvalidIPAddress(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);
        $this->expectException(\ValueError::class);
        $failure = $blocklist->create('altostrat.com');
    }


    #[TestDox('Blocklist::delete exists')]
    public function testBlocklistDeleteExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testBlocklistDeleteExists')]
    #[TestDox('Blocklist::delete is public')]
    public function testBlocklistDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'delete');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testBlocklistDeleteExists')]
    #[TestDox('Blocklist::delete has one REQUIRED parameter')]
    public function testBlocklistDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBlocklistDeleteExists')]
    #[Depends('testBlocklistDeleteIsPublic')]
    #[Depends('testBlocklistDeleteHasOneRequiredParameter')]
    #[TestDox('Blocklist::delete throws value error exception on invalid IP address')]
    public function testBlocklistDeleteThrowsValueErrorExceptionOnInvalidIPAddress(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);
        $this->expectException(\ValueError::class);
        $failure = $blocklist->delete('altostrat.com');
    }

    #[Depends('testBlocklistDeleteExists')]
    #[Depends('testBlocklistDeleteIsPublic')]
    #[Depends('testBlocklistDeleteHasOneRequiredParameter')]
    #[TestDox('Blocklist::delete returns (bool) true on success')]
    public function testBlocklistDeleteReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);

        $this->assertTrue($blocklist->delete('198.51.100.0'));
        $this->assertFalse($blocklist->has('198.51.100.0'));

        $this->assertTrue($blocklist->delete('2001:db8:4:4:4:4:4:4'));
        $this->assertFalse($blocklist->has('2001:db8:4:4:4:4:4:4'));

        $this->assertTrue($blocklist->clear());
    }


    #[TestDox('Blocklist::has exists')]
    public function testBlocklistHasExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testBlocklistHasExists')]
    #[TestDox('Blocklist::has is public')]
    public function testBlocklistHasIsPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'has');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBlocklistHasExists')]
    #[TestDox('Blocklist::has has one REQUIRED parameter')]
    public function testBlocklistHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBlocklistHasExists')]
    #[TestDox('Blocklist::has returns (bool) true and false')]
    public function testBlocklistHasReturnsBoolTrueAndFalse(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);

        $this->assertFalse($blocklist->has('203.0.113.255'));
        $this->assertTrue($blocklist->create('203.0.113.255', 'Test IP address added by unit test'));

        // IPv4 and IPv6 address added in setUp.
        $this->assertTrue($blocklist->has('198.51.100.0'));
        $this->assertTrue($blocklist->has('2001:db8:ffff:ffff:ffff:ffff:ffff:ffff'));
    }


    #[TestDox('Blocklist::list exists')]
    public function testBlocklistListExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[Depends('testBlocklistListExists')]
    #[TestDox('Blocklist::list is public')]
    public function testBlocklistListIsPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'list');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testBlocklistListExists')]
    #[TestDox('Blocklist::list has no parameters')]
    public function testBlocklistListHasNoParameters(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'list');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBlocklistListExists')]
    #[TestDox('Blocklist::list returns key-value array')]
    public function testBlocklistListReturnsKeyValueArray(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);

        $list = $blocklist->list();
        $this->assertNotEmpty($list);
        $this->assertIsArray($list);

        // The IPv4 and IPv6 adddress from `setUp()` MUST be listed.
        $this->assertArrayHasKey('198.51.100.0', $list);
        $this->assertArrayHasKey('2001:db8:ffff:ffff:ffff:ffff:ffff:ffff', $list);

        // The test IP address added in unit testing MUST be listed.
        $this->assertArrayHasKey('198.51.100.0', $list);
    }


    #[TestDox('BlocklistReader::log exists')]
    public function testBlocklistReaderLogExists(): void
    {
        $class = new ReflectionClass(Blocklist::class);
        $this->assertTrue($class->hasMethod('log'));
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[TestDox('BlocklistReader::log is final public')]
    public function testBlocklistReaderLogIsFinalPublic(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'log');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[TestDox('BlocklistReader::log has one REQUIRED parameter')]
    public function testBlocklistReaderLogHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Blocklist::class, 'log');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[Depends('testBlocklistReaderLogIsFinalPublic')]
    #[Depends('testBlocklistReaderLogHasOneRequiredParameter')]
    #[TestDox('BlocklistReader::log returns (bool) false on invalid IP address')]
    public function testBlocklistReaderLogReturnsBoolFalseOnInvalidIpAddress(): void
    {
        $registry = Registry::getInstance();

        $blocklist = new Blocklist($registry);
        $this->assertFalse($blocklist->log('42'));

        $blocklist = new BlocklistReader($registry);
        $this->assertFalse($blocklist->log('666'));
    }

    #[Depends('testBlocklistReaderLogExists')]
    #[Depends('testBlocklistReaderLogIsFinalPublic')]
    #[Depends('testBlocklistReaderLogHasOneRequiredParameter')]
    #[TestDox('BlocklistReader::log returns (bool) true on success')]
    public function testBlocklistReaderLogReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $blocklist = new Blocklist($registry);
        $this->assertTrue($blocklist->log('198.51.100.0'));
        $this->assertTrue($blocklist->log('2001:db8:ffff:ffff:ffff:ffff:ffff:ffff'));
    }
}
