<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\PersonRepository::class)]
#[CoversClass(\StoreCore\CRM\Person::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class PersonRepositoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('PersonRepository class is concrete')]
    public function testPersonRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PersonRepository is a database model')]
    public function testPersonRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('PersonRepository implements PSR-11 ContainerInterface')]
    public function testPersonRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('PersonRepository is countable')]
    public function testPersonRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PersonRepository::VERSION);
        $this->assertIsString(PersonRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PersonRepository::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('PersonRepository::count() exists')]
    public function testPersonRepositoryCountExists(): void
    {
        $class = new ReflectionClass(PersonRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('PersonRepository::count() is public')]
    public function testPersonRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(PersonRepository::class, 'count');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PersonRepository::count() has no parameters')]
    public function testPersonRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(PersonRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testPersonRepositoryCountExists')]
    #[Depends('testPersonRepositoryCountIsPublic')]
    #[Depends('testPersonRepositoryCountHasNoParameters')]
    #[TestDox('PersonRepository::count() returns (int) 0 or greater integer')]
    public function testPersonRepositoryCountReturnsInt0OrGreaterInteger(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new PersonRepository($registry);
        $this->assertIsInt($repository->count());
        $this->assertGreaterThanOrEqual(0, $repository->count());
    }
}
