<?php

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\CreativeWorkTextMapper::class)]
final class CreativeWorkTextMapperTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CreativeWorkTextMapper class file exists')]
    public function testCreativeWorkTextMapperClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkTextMapper.php'
        );
    }

    #[Depends('testCreativeWorkTextMapperClassFileExists')]
    #[Group('distro')]
    #[TestDox('CreativeWorkTextMapper class file exists')]
    public function testCreativeWorkTextMapperClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkTextMapper.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('CreativeWorkTextMapper is a database model')]
    public function testCreativeWorkTextMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CreativeWorkTextMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('CreativeWorkTextMapper implements CRUDInterface')]
    public function testCreativeWorkTextMapperImplementsCRUDInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Database\\CRUDInterface'));

        $class = new ReflectionClass(CreativeWorkTextMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\CRUDInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkTextMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkTextMapper::VERSION);
        $this->assertIsString(CreativeWorkTextMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkTextMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWorkTextMapper::filterKeywords() exists')]
    public function testCreativeWorkTextMapperFilterKeywordsExists(): void
    {
        $class = new ReflectionClass(CreativeWorkTextMapper::class);
        $this->assertTrue($class->hasMethod('filterKeywords'));
    }

    #[Depends('testCreativeWorkTextMapperFilterKeywordsExists')]
    #[TestDox('CreativeWorkTextMapper::filterKeywords() is public static')]
    public function testCreativeWorkTextMapperFilterKeywordsIsPublicStatic(): void
    {
        $method = new ReflectionMethod(CreativeWorkTextMapper::class, 'filterKeywords');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testCreativeWorkTextMapperFilterKeywordsExists')]
    #[Depends('testCreativeWorkTextMapperFilterKeywordsIsPublicStatic')]
    #[TestDox('CreativeWorkTextMapper::filterKeywords() accepts array and returns comma-separated string')]
    public function testCreativeWorkTextMapperFilterKeywordsAcceptsArrayAndReturnsCommaSeparatedString(): void
    {
        $keywords = [
            "backpacks",
            "bomber jackets",
            "dirndl dresses",
            "dresses",
            "football boots",
            "handbags",
            "jeans",
            "kids' clothing",
            "kids' shoes",
            "men's chinos",
            "men's clothing",
            "men's coats",
            "men's shoes",
            "necklaces",
            "purses",
            "shirt dresses",
            "snow boots",
            "thigh high boots",
            "trainers",
            "uggs",
            "wellies",
            "women's ankle boots",
            "women's boots",
            "women's clothing",
            "women's coats",
            "women's shirts",
            "women's shoes",
        ];

        $this->assertNotEmpty(CreativeWorkTextMapper::filterKeywords($keywords));
        $this->assertIsString(CreativeWorkTextMapper::filterKeywords($keywords));

        $this->assertStringContainsString(', ', CreativeWorkTextMapper::filterKeywords($keywords));
        $this->assertStringStartsWith('backpacks, ', CreativeWorkTextMapper::filterKeywords($keywords));
        $this->assertStringEndsWith(', women’s shoes', CreativeWorkTextMapper::filterKeywords($keywords));
    }

    #[Depends('testCreativeWorkTextMapperFilterKeywordsExists')]
    #[Depends('testCreativeWorkTextMapperFilterKeywordsIsPublicStatic')]
    #[TestDox('CreativeWorkTextMapper::filterKeywords() accepts string and returns string')]
    public function testCreativeWorkTextMapperFilterKeywordsAcceptsStringAndReturnsString(): void
    {
        // String with malformed whitespace:
        $keywords = ' Abendkleider,  Brautkleider,  Abiballkleider,Abschlussballkleider,, Hochzeitskleider ';
        $expected = 'Abendkleider, Brautkleider, Abiballkleider, Abschlussballkleider, Hochzeitskleider';
        $this->assertEquals($expected, CreativeWorkTextMapper::filterKeywords($keywords));
    }

    #[Depends('testCreativeWorkTextMapperFilterKeywordsAcceptsArrayAndReturnsCommaSeparatedString')]
    #[TestDox('CreativeWorkTextMapper::filterKeywords() returns unique keywords')]
    public function testCreativeWorkTextMapperFilterKeywordsReturnsUniqueKeywords(): void
    {
        $keywords = [
            'backpacks',
            'handbags',
            'backpacks', // duplicate
            'purses',
        ];

        $this->assertEquals(
            'backpacks, handbags, purses', 
            CreativeWorkTextMapper::filterKeywords($keywords)
        );
    }

    #[TestDox('CreativeWorkTextMapper::filterKeywords() maximum string length is 767 characters')]
    public function testCreativeWorkTextMapperFilterKeywordsMaximumStringLengthIs767Characters(): void
    {
        if (!function_exists('mb_strlen')) {
            $this->markTestSkipped('PHP function `mb_strlen()` does not exist.');
        }

        $keywords = 'Lorem, ipsum, dolor, sit, amet, consectetur, adipiscing, elit, sed, do, eiusmod, tempor, incididunt, ut, labore, et, dolore, magna, aliqua, Sit, amet, mattis, vulputate, enim, nulla, aliquet, Elementum, curabitur, vitae, nunc, velit, dignissim, sodales, eu, Egestas, quis, suspendisse, ultrices, gravida, dictum, Nibh, cras, pulvinar, nunc, Adipiscing, diam, donec, tristique, risus, nec, Neque, laoreet, interdum, libero, id, faucibus, nisl, Aliquam, sem, tortor, consequat, porta, nibh, Fringilla, morbi, tincidunt, augue, interdum, arcu, dictum, varius, duis, at, consectetur, Massa, massa, ultricies, mi, hendrerit, magna, in, cursus, turpis, dui, ornare, lectus, sit, ac, egestas, maecenas, pharetra, convallis, posuere, semper, viverra, nam, justo, urna, eget, scelerisque, mauris, in, aliquam, Eget, nullam, non, nisi, est, facilisis, etiam, Praesent, semper, feugiat, nibh, pulvinar, dui, sapien, proin, enim, nulla, facilisi, vehicula, a, vel, commodo, accumsan, lacus';
        $this->assertGreaterThan(767, \mb_strlen($keywords));
        $this->assertGreaterThan(767, \strlen($keywords));

        $this->assertStringContainsString(' at, ', $keywords);
        $this->assertStringContainsString(' do, ', $keywords);
        $this->assertStringContainsString(' in, ', $keywords);
        $this->assertStringContainsString(' ut, ', $keywords);

        $keywords = CreativeWorkTextMapper::filterKeywords($keywords);
        $this->assertLessThanOrEqual(767, \mb_strlen($keywords));
        $this->assertLessThanOrEqual(767, \strlen($keywords));

        $this->assertStringNotContainsString(' at, ', $keywords);
        $this->assertStringNotContainsString(' do, ', $keywords);
        $this->assertStringNotContainsString(' in, ', $keywords);
        $this->assertStringNotContainsString(' ut, ', $keywords);
    }

    #[TestDox('CreativeWorkTextMapper::filterKeywords() drops duplicate keywords')]
    public function testCreativeWorkTextMapperFilterKeywordsDropsDuplicateKeywords(): void
    {
        if (!function_exists('mb_strlen')) {
            $this->markTestSkipped('PHP function `mb_strlen()` does not exist.');
        }

        $keywords = "women's shoes, men's shoes, boots, ankle boots, knee-high boots, over-the-knee boots, combat boots, platform boots, heeled boots, lace-up boots, sandals, loafers, brogues, Oxfords, strappy sandals, gladiator sandals, thong sandals, espadrille sandals, slide sandals, flip-flops, casual shoes, dress shoes, sneakers, boots, men's boots, men’s work boots, women’s work boots, safety toe boots, athletic shoes, heels, pumps, wedges, ballet flats, mules, slides, espadrilles, slide sandals, flip-flops, Crocs, Birkenstocks, Sperry Top-Siders, Ugg boots, Crocs clogs, Minnetonka moccasins, Frye boots, Doc Martens, Stuart Weitzman boots, Manolo Blahnik shoes, Christian Louboutin shoes, Jimmy Choo shoes, Tory Burch shoes, Gucci shoes, Prada shoes, Chanel shoes, Louis Vuitton shoes, Hermès shoes";
        $this->assertGreaterThan(767, \mb_strlen($keywords));
        $this->assertGreaterThan(767, \strlen($keywords));

        // The method should drop the keywords “boots” and “sandals” because
        // these are included in “ankle boots”, “slide sandals”, etc.
        $this->assertStringContainsString(', boots, ', $keywords);
        $this->assertStringContainsString(', sandals, ', $keywords);

        $keywords = CreativeWorkTextMapper::filterKeywords($keywords);
        $this->assertStringNotContainsString(', boots, ', $keywords);
        $this->assertStringNotContainsString(', sandals, ', $keywords);

        // The string “men's shoes” is included in “women's shoes”
        // but SHOULD NOT be removed.
        $this->assertStringContainsString(', men’s shoes, ', $keywords);
        $this->assertStringContainsString(', women’s shoes, ', $keywords);

        $this->assertLessThanOrEqual(767, \mb_strlen($keywords));
        $this->assertLessThanOrEqual(767, \strlen($keywords));
    }
}
