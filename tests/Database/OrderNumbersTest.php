<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\OML\OrderProxy;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Clock::class)]
#[CoversClass(\StoreCore\Database\OrderNumbers::class)]
#[CoversClass(\StoreCore\OML\OrderProxy::class)]
#[CoversClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class OrderNumbersTest extends TestCase
{
    protected function setUp(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');
            $db->query('SELECT 1 FROM `sc_orders`');

            $db->exec(
                "DELETE
                   FROM `sc_orders`
                  WHERE `order_number` = '12345'
                     OR `confirmation_number` = '123-1234567-1234567'"
            );

            $db->exec(
                "INSERT INTO `sc_orders`
                     (`order_uuid`, `date_confirmed`, `order_number`, `confirmation_number`)
                   VALUES
                     (
                       UNHEX(LOWER(REPLACE(UUID(),'-',''))),
                       UTC_TIMESTAMP(),
                       '12345',
                       '123-1234567-1234567'
                     )"
            );
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('OrderNumbers class is concrete')]
    public function testOrderNumbersClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderNumbers is a database model')]
    public function testOrderNumbersIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('OrderNumbers implements PSR-20 ClockInterface')]
    public function testOrderNumbersImplementsPSR20ClockInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));

        $registry = Registry::getInstance();
        $orderNumbers = new OrderNumbers($registry);
        $this->assertInstanceOf(\Psr\Clock\ClockInterface::class, $orderNumbers);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderNumbers::VERSION);
        $this->assertIsString(OrderNumbers::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderNumbers::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderNumbers::get exists')]
    public function testOrderNumbersGetExists(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testOrderNumbersGetExists')]
    #[TestDox('OrderNumbers::get is public')]
    public function testOrderNumbersGetIsPublic(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderNumbersGetExists')]
    #[TestDox('OrderNumbers::get has one REQUIRED parameter')]
    public function testOrderNumbersGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderNumbersGetExists')]
    #[Depends('testOrderNumbersGetIsPublic')]
    #[Depends('testOrderNumbersGetHasOneRequiredParameter')]
    #[TestDox('OrderNumbers::get requires a string parameter')]
    public function testOrderNumbersGetRequiresStringParameter(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->expectException(\TypeError::class);
        $failure = $container->get(12345);
    }

    #[Depends('testOrderNumbersGetRequiresStringParameter')]
    #[TestDox('OrderNumbers::get throws PSR-11 NotFoundExceptionInterface if order does not exist')]
    public function testOrderNumbersGetThrowsPSR11NotFoundExceptionInterfaceIfOrderDoesNotExist(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));

        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('123-4567890-1234567');
    }

    #[Depends('testOrderNumbersGetThrowsPSR11NotFoundExceptionInterfaceIfOrderDoesNotExist')]
    #[TestDox('OrderNumbers::get returns OrderProxy if order does exist')]
    public function testOrderNumbersGetReturnsOrderProxyIfOrderDoesExist(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);

        // Order.orderNumber set in `setUp()`:
        $this->assertTrue($container->has('12345'));

        $order = $container->get('12345');
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $order);
        $this->assertInstanceOf(\StoreCore\OML\OrderProxy::class, $order);
        $this->assertEquals('12345', $order->orderNumber);

        // Order.confirmationNumber set in `setUp()`:
        $this->assertTrue($container->has('123-1234567-1234567'));

        $order = $container->get('123-1234567-1234567');
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $order);
        $this->assertInstanceOf(\StoreCore\OML\OrderProxy::class, $order);
        $this->assertEquals('12345', $order->orderNumber);
        $this->assertEquals('123-1234567-1234567', $order->confirmationNumber);

        $json = json_encode($order, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($json);
        $this->assertStringContainsString('"orderNumber": "12345"', $json);
        $this->assertStringContainsString('"confirmationNumber": "123-1234567-1234567"', $json);
    }


    #[TestDox('OrderNumbers::getConfirmationNumber exists')]
    public function testOrderNumbersGetConfirmationNumberExists(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->hasMethod('getConfirmationNumber'));
    }

    #[Depends('testOrderNumbersGetConfirmationNumberExists')]
    #[TestDox('OrderNumbers::getConfirmationNumber is public')]
    public function testOrderNumbersGetConfirmationNumberIsPublic(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'getConfirmationNumber');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderNumbersGetConfirmationNumberExists')]
    #[TestDox('OrderNumbers::getConfirmationNumber has no parameters')]
    public function testOrderNumbersGetConfirmationNumberHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'getConfirmationNumber');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderNumbersGetConfirmationNumberExists')]
    #[Depends('testOrderNumbersGetConfirmationNumberIsPublic')]
    #[Depends('testOrderNumbersGetConfirmationNumberHasNoParameters')]
    #[TestDox('OrderNumbers::getConfirmationNumber returns pseudo-random string')]
    public function testOrderNumbersGetConfirmationReturnsPseudoRandomString(): void
    {
        $registry = Registry::getInstance();
        $orderNumbers = new OrderNumbers($registry);
        $result = $orderNumbers->getConfirmationNumber();
        $this->assertNotEmpty($result);
        $this->assertIsString($result);

        $this->assertStringContainsString('-', $result);
        $result = explode('-', $result);
        $this->assertIsArray($result);
        $this->assertCount(3, $result);
    }


    #[TestDox('OrderNumbers::getRandomOrderNumber exists')]
    public function testOrderNumbersGetRandomOrderNumberExists(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->hasMethod('getRandomOrderNumber'));
    }

    #[Depends('testOrderNumbersGetRandomOrderNumberExists')]
    #[TestDox('OrderNumbers::getRandomOrderNumber is public')]
    public function testOrderNumbersGetRandomOrderNumberIsPublic(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'getRandomOrderNumber');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderNumbersGetRandomOrderNumberExists')]
    #[TestDox('OrderNumbers::getRandomOrderNumber has no parameters')]
    public function testOrderNumbersGetRandomOrderNumberHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'getRandomOrderNumber');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderNumbersGetRandomOrderNumberExists')]
    #[TestDox('OrderNumbers::getRandomOrderNumber returns random integer as string')]
    public function testOrderNumbersGetRandomOrderNumberReturnsRandomIntegerAsString(): void
    {
        $registry = Registry::getInstance();
        $model = new OrderNumbers($registry);
        $result = $model->getRandomOrderNumber();
        $this->assertNotEmpty($result);
        $this->assertIsString($result);
        $this->assertTrue(ctype_digit($result));

        $result = (int) $result;
        $this->assertGreaterThanOrEqual(5000, $result);
        $this->assertLessThanOrEqual(2147483647, $result);
    }


    #[TestDox('OrderNumbers::has exists')]
    public function testOrderNumbersHasExists(): void
    {
        $class = new ReflectionClass(OrderNumbers::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testOrderNumbersHasExists')]
    #[TestDox('OrderNumbers::has is public')]
    public function testOrderNumbersHasIsPublic(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderNumbersHasExists')]
    #[TestDox('OrderNumbers::has has one REQUIRED parameter')]
    public function testOrderNumbersHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderNumbers::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderNumbersHasExists')]
    #[Depends('testOrderNumbersHasIsPublic')]
    #[Depends('testOrderNumbersHasHasOneRequiredParameter')]
    #[TestDox('OrderNumbers::has requires a string parameter')]
    public function testOrderNumbersHasRequiresStringParameter(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->expectException(\TypeError::class);
        $failure = $container->has(12345);
    }

    #[Depends('testOrderNumbersHasRequiresStringParameter')]
    #[TestDox('OrderNumbers::has returns (bool) false on empty string')]
    public function testOrderNumbersHasReturnsBooFalseOnEmptyString(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->assertIsBool($container->has(''));
        $this->assertFalse($container->has(''));
    }

    #[Depends('testOrderNumbersHasRequiresStringParameter')]
    #[TestDox('OrderNumbers::has returns (bool) false if order number does not exist')]
    public function testOrderNumbersHasReturnsBoolFalseIfOrderNumberDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);

        // Random order number consists of four digits, so we can test with three.
        $order_number = (string) random_int(100, 999);
        $this->assertIsBool($container->has($order_number));
        $this->assertFalse($container->has($order_number));
    }

    #[Depends('testOrderNumbersHasReturnsBoolFalseIfOrderNumberDoesNotExist')]
    #[TestDox('OrderNumbers::has returns (bool) true if order number does exist')]
    public function testOrderNumbersHasReturnsBoolTrueIfOrderNumberDoesExist(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->assertTrue($container->has('12345'));
    }

    #[Depends('testOrderNumbersHasRequiresStringParameter')]
    #[TestDox('OrderNumbers::has returns (bool) false if confirmation number does not exist')]
    public function testOrderNumbersHasReturnsBoolFalseIfConfirmationNumberDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);

        // Confirmation number format is `###-#######-#######`.
        $conformation_number = random_int(100, 999) . '-' . random_int(1000000, 9999999) . '-' . random_int(1000000, 9999999);
        $this->assertIsBool($container->has($conformation_number));
        $this->assertFalse($container->has($conformation_number));
    }

    #[Depends('testOrderNumbersHasReturnsBoolFalseIfConfirmationNumberDoesNotExist')]
    #[TestDox('OrderNumbers::has returns (bool) true if confirmation number does exist')]
    public function testOrderNumbersHasReturnsBoolTrueIfConfirmationNumberDoesExist(): void
    {
        $registry = Registry::getInstance();
        $container = new OrderNumbers($registry);
        $this->assertTrue($container->has('123-1234567-1234567'));
    }
}
