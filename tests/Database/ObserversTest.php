<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\Observers::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class ObserversTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Observers class is concrete')]
    public function testObserversClassIsConcrete(): void
    {
        $class = new ReflectionClass(Observers::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Observers class is a database model')]
    public function testObserversClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Observers::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new \ReflectionClass(Observers::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Observers::VERSION);
        $this->assertIsString(Observers::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Observers::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Observers::getSubjectObservers exists')]
    public function testObserversGetSubjectObserversExists(): void
    {
        $class = new ReflectionClass(Observers::class);
        $this->assertTrue($class->hasMethod('getSubjectObservers'));
    }

    #[Depends('testObserversGetSubjectObserversExists')]
    #[TestDox('Observers::getSubjectObservers is public')]
    public function testObserversGetSubjectObserversIsPublic(): void
    {
        $method = new ReflectionMethod(Observers::class, 'getSubjectObservers');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testObserversGetSubjectObserversExists')]
    #[TestDox('Observers::getSubjectObservers has one REQUIRED parameter')]
    public function testObserversGetSubjectObserversHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Observers::class, 'getSubjectObservers');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObserversGetSubjectObserversExists')]
    #[Depends('testObserversGetSubjectObserversIsPublic')]
    #[Depends('testObserversGetSubjectObserversHasOneRequiredParameter')]
    #[TestDox('Observers::getSubjectObservers() returns null if subject class does not exist')]
    public function testObserversGetSubjectObserversReturnsNullIfSubjectClassDoesNotExist()
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $class_name = 'StoreCore\Foo\Bar';
        $this->assertFalse(class_exists($class_name));

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_observers`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }

        $observers = new \StoreCore\Database\Observers($registry);
        $this->assertNull($observers->getSubjectObservers($class_name));
    }
}
