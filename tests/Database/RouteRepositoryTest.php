<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Route;
use StoreCore\RoutingQueue;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[Group('hmvc')]
#[CoversClass(\StoreCore\Database\RouteContainer::class)]
#[CoversClass(\StoreCore\Database\RouteRepository::class)]
#[CoversClass(\StoreCore\Route::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\RoutingQueue::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class RouteRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_routes`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('distro')]
    #[TestDox('RouteRepository class exists')]
    public function testRouteRepositoryClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Database\\RouteRepository'));
        $this->assertTrue(class_exists(RouteRepository::class));
    }

    #[TestDox('RouteRepository class is concrete')]
    public function testRouteRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('RouteRepository is a database model')]
    public function testRouteRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[TestDox('RouteRepository implements PSR-11 ContainerInterface')]
    public function testRouteRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RouteRepository::VERSION);
        $this->assertIsString(RouteRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RouteRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('RouteRepository::deleteController exists')]
    public function testRouteRepositoryDeleteControllerExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('deleteController'));
    }

    #[Depends('testRouteRepositoryDeleteControllerExists')]
    #[TestDox('RouteRepository::deleteController is public')]
    public function testRouteRepositoryDeleteControllerIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deleteController');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRouteRepositoryDeleteControllerExists')]
    #[TestDox('RouteRepository::deleteController has one REQUIRED parameter')]
    public function testRouteRepositoryDeleteControllerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deleteController');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('RouteRepository::deleteNamespace exists')]
    public function testRouteRepositoryDeleteNamespaceExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('deleteNamespace'));
    }

    #[Depends('testRouteRepositoryDeleteNamespaceExists')]
    #[TestDox('RouteRepository::deleteNamespace is public')]
    public function testRouteRepositoryDeleteNamespaceIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deleteNamespace');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRouteRepositoryDeleteNamespaceExists')]
    #[TestDox('RouteRepository::deleteNamespace has one REQUIRED parameter')]
    public function testRouteRepositoryDeleteNamespaceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deleteNamespace');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRouteRepositoryDeleteNamespaceExists')]
    #[TestDox('Namespace `StoreCore` cannot be deleted')]
    public function testNamespaceStoreCoreCannotBeDeleted(): void
    {
        $registry = Registry::getInstance();
        $repository = new RouteRepository($registry);
        $this->expectException(\InvalidArgumentException::class);
        $failure = $repository->deleteNamespace('StoreCore');
    }


    #[TestDox('RouteRepository::deletePath exists')]
    public function testRouteRepositoryDeletePathExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('deletePath'));
    }

    #[Depends('testRouteRepositoryDeletePathExists')]
    #[TestDox('RouteRepository::deletePath is public')]
    public function testRouteRepositoryDeletePathIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deletePath');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRouteRepositoryDeletePathExists')]
    #[TestDox('RouteRepository::deletePath has one REQUIRED parameter')]
    public function testRouteRepositoryDeletePathHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'deletePath');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRouteRepositoryDeletePathExists')]
    #[TestDox('RouteRepository::deletePath returns (bool) true on success')]
    public function testRouteRepositoryDeletePathReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $routing = new RouteRepository($registry);
        $this->assertTrue($routing->has('/robots.txt'));

        $this->assertTrue($routing->deletePath('/robots.txt'));
        $this->assertFalse($routing->has('/robots.txt'));
    }


    #[TestDox('RouteRepository::get exists')]
    public function testRouteRepositoryGetExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testRouteRepositoryGetExists')]
    #[TestDox('RouteRepository::get is public')]
    public function testRouteRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRouteRepositoryGetExists')]
    #[TestDox('RouteRepository::get has one REQUIRED parameter')]
    public function testRouteRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRouteRepositoryGetExists')]
    #[Depends('testRouteRepositoryGetHasOneRequiredParameter')]
    #[TestDox('RouteRepository::get returns RoutingQueue when route exists')]
    public function testRouteRepositoryGetReturnsRoutingQueueWhenRouteExists(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $routing = new RouteRepository($registry);
        $this->assertTrue($routing->has('/admin/lock'));

        $routes = $routing->get('/admin/lock');
        $this->assertInstanceOf(\StoreCore\RoutingQueue::class, $routes);
        $this->assertInstanceOf(\Countable::class, $routes);
        $this->assertTrue(count($routes) >= 1);
    }

    #[Depends('testRouteRepositoryGetHasOneRequiredParameter')]
    #[TestDox('RouteRepository::get throws PSR-11 NotFoundExceptionInterface when route does not exist')]
    public function testRouteRepositoryGetThrowsPSR11NotFoundExceptionInterfaceWhenRouteDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $routing = new RouteRepository($registry);
        $this->assertFalse($routing->has('/yellow/brick/road'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $routes = $routing->get('/yellow/brick/road');
    }


    #[TestDox('RouteRepository::has exists')]
    public function testRouteRepositoryHasExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testRouteRepositoryHasExists')]
    #[TestDox('RouteRepository::has is public')]
    public function testRouteRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'has');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRouteRepositoryHasExists')]
    #[TestDox('RouteRepository::has has one REQUIRED parameter')]
    public function testRouteRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRouteRepositoryHasExists')]
    #[Depends('testRouteRepositoryHasIsPublic')]
    #[Depends('testRouteRepositoryHasHasOneRequiredParameter')]
    #[TestDox('RouteRepository::has returns true if path exists')]
    public function testRouteRepositoryHasReturnsTrueIfPathExists(): void
    {    
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $routing = new RouteRepository($registry);
        $this->assertTrue($routing->has('/admin/lock'));
        $this->assertTrue($routing->has('/admin/lock/'));
        $this->assertTrue($routing->has('admin/lock'));
    }

    #[Depends('testRouteRepositoryHasReturnsTrueIfPathExists')]
    #[TestDox('RouteRepository::has returns false if path does not exist')]
    public function testRouteRepositoryHasReturnsFalseIfPathDoesNotExist(): void
    {    
        $registry = Registry::getInstance();
        $routing = new RouteRepository($registry);
        $this->assertFalse($routing->has('/admin/foo/bar'));
    }


    #[TestDox('RouteRepository::saveQueue exists')]
    public function testRouteRepositorySaveQueueExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('saveQueue'));
    }

    #[Depends('testRouteRepositorySaveQueueExists')]
    #[TestDox('RouteRepository::saveQueue is public')]
    public function testRouteRepositorySaveQueueIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'saveQueue');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRouteRepositorySaveQueueExists')]
    #[TestDox('RouteRepository::saveQueue has one REQUIRED parameter')]
    public function testRouteRepositorySaveQueueHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'saveQueue');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('RouteRepository::saveRoute exists')]
    public function testRouteRepositorySaveRouteExists(): void
    {
        $class = new ReflectionClass(RouteRepository::class);
        $this->assertTrue($class->hasMethod('saveRoute'));
    }

    #[Depends('testRouteRepositorySaveRouteExists')]
    #[TestDox('RouteRepository::saveRoute is public')]
    public function testRouteRepositorySaveRouteIsPublic(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'saveRoute');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRouteRepositorySaveRouteExists')]
    #[TestDox('RouteRepository::saveRoute has two parameters')]
    public function testRouteRepositorySaveRouteHasTwoParameters(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'saveRoute');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testRouteRepositorySaveRouteHasTwoParameters')]
    #[TestDox('RouteRepository::saveRoute has one REQUIRED parameter')]
    public function testRouteRepositorySaveRouteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RouteRepository::class, 'saveRoute');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRouteRepositoryDeletePathReturnsBoolTrueOnSuccess')]
    #[Depends('testRouteRepositorySaveRouteExists')]
    #[Depends('testRouteRepositorySaveRouteIsPublic')]
    #[Depends('testRouteRepositorySaveRouteHasOneRequiredParameter')]
    #[TestDox('RouteRepository::saveRoute returns (bool) true on success')]
    public function testRouteRepositorySaveRouteReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $routing = new RouteRepository($registry);
        $this->assertFalse($routing->has('/robots.txt'));

        $route = new Route('/robots.txt', 'StoreCore\\FileSystem\\Robots', 'handle');
        $this->assertTrue($routing->saveRoute($route));
        $this->assertTrue($routing->has('/robots.txt'));
    }
}
