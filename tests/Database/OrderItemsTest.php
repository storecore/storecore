<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Types\UUID;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\OrderItems::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class OrderItemsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OrderItems class is concrete')]
    public function testOrderItemsClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderItems is a database model')]
    public function testOrderItemsIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderItems::VERSION);
        $this->assertIsString(OrderItems::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderItems::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderItems::count exists')]
    public function testOrderItemsCountNumberExists(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testOrderItemsCountNumberExists')]
    #[TestDox('OrderItems::count is public')]
    public function testOrderItemsCountNumberIsPublic(): void
    {
        $method = new ReflectionMethod(OrderItems::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderItemsCountNumberExists')]
    #[TestDox('OrderItems::count has one REQUIRED parameter')]
    public function testOrderItemsCountNumberHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderItems::class, 'count');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderItemsCountNumberExists')]
    #[Depends('testOrderItemsCountNumberIsPublic')]
    #[Depends('testOrderItemsCountNumberHasOneRequiredParameter')]
    #[TestDox('OrderItems::count requires UUID as object')]
    public function testOrderItemsCountRequiresUuidAsObject(): void
    {
        $registry = Registry::getInstance();
        $model = new OrderItems($registry);
        $uuid = '018e7e12-14f6-7c8e-9030-2a941f94c5dd';
        $this->assertIsString($uuid);

        $this->expectException(\TypeError::class);
        $failure = $model->count($uuid);
    }

    #[Depends('testOrderItemsCountRequiresUuidAsObject')]
    #[TestDox('OrderItems::count returns integer on success')]
    public function testOrderItemsCountNumberReturnsIntegerOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $model = new OrderItems($registry);
        $uuid = new UUID('018e7e12-14f6-7c8e-9030-2a941f94c5dd');
        $this->assertIsObject($uuid);

        $result = $model->count($uuid);
        $this->assertIsInt($result);
        $this->assertLessThanOrEqual(0, $result);
    }
}
