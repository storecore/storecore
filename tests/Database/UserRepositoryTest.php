<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\User;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\TestCase;

use function \bin2hex;
use function \interface_exists;
use function \random_bytes;
use function \random_int;
use function \version_compare;

#[CoversClass(\StoreCore\User::class)]
#[CoversClass(\StoreCore\Database\Password::class)]
#[CoversClass(\StoreCore\Database\Salt::class)]
#[CoversClass(\StoreCore\Database\UserMapper::class)]
#[CoversClass(\StoreCore\Database\UserRepository::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\EmailAddressMapper::class)]
#[UsesClass(\StoreCore\Database\EmailAddressRepository::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\EmailAddress::class)]
#[UsesClass(\StoreCore\Types\LanguageCode::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
#[UsesClass(\StoreCore\Types\Varchar::class)]
final class UserRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_users`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('UserRepository is a user mapper database model')]
    public function testUserRepositoryIsUserMapperDatabaseModel(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\UserMapper::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('UserRepository implements PSR-11 ContainerInterface')]
    public function testUserRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('hmvc')]
    #[TestDox('UserRepository implements CRUD interface')]
    public function testUserRepositoryImplementsCrudInterface(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\Database\CRUDInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UserRepository::VERSION);
        $this->assertIsString(UserRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UserRepository::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('UserRepository::ban exists')]
    public function testUserRepositoryBanExists(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasMethod('ban'));
    }

    #[Depends('testUserRepositoryBanExists')]
    #[TestDox('UserRepository::ban is public')]
    public function testUserRepositoryBanIsPublic(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'ban');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testUserRepositoryBanExists')]
    #[TestDox('UserRepository::ban has one REQUIRED parameter')]
    public function testUserRepositoryBanHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'ban');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UserRepository::count exists')]
    public function testUserRepositoryCountExists(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('UserRepository::count is public')]
    public function testUserRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('UserRepository::count has no parameters')]
    public function testUserRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUserRepositoryCountExists')]
    #[Depends('testUserRepositoryCountIsPublic')]
    #[Depends('testUserRepositoryCountHasNoParameters')]
    #[TestDox('UserRepository::count returns integer greater than or equal to 0 (zero)')]
    public function testUserRepositoryCountReturnsIntegerGreaterThanOrEqualToZero(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $user_repository = new UserRepository($registry);
            $this->assertIsInt($user_repository->count());
            $this->assertGreaterThanOrEqual(0, $user_repository->count());
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Depends('testUserRepositoryCountReturnsIntegerGreaterThanOrEqualToZero')]
    #[TestDox('UserRepository is Countable')]
    public function testUserRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));

        $registry = Registry::getInstance();
        $user_repository = new UserRepository($registry);
        $this->assertEquals($user_repository->count(), count($user_repository));
        $this->assertGreaterThanOrEqual(0, count($user_repository));
    }


    #[TestDox('UserRepository::get exists')]
    public function testUserRepositoryGetExists(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testUserRepositoryGetExists')]
    #[TestDox('UserRepository::get is public')]
    public function testUserRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUserRepositoryGetExists')]
    #[TestDox('UserRepository::get has one REQUIRED parameter')]
    public function testUserRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UserRepository::has exists')]
    public function testUserRepositoryHasExists(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testUserRepositoryHasExists')]
    #[TestDox('UserRepository::has is public')]
    public function testUserRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUserRepositoryHasExists')]
    #[TestDox('UserRepository::has has one REQUIRED parameter')]
    public function testUserRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('UserRepository::has returns (bool) false on empty string')]
    public function testUserRepositoryHasReturnsBoolFalseOnEmptyString(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $id = '';
        $this->assertEmpty($id);

        $repository = new UserRepository($registry);
        $this->assertIsBool($repository->has($id));
        $this->assertFalse($repository->has($id));
    }

    #[TestDox('UserRepository::has returns (bool) false if UUID does not exist')]
    public function testUserRepositoryHasReturnsBoolFalseIfUuidDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $uuid = UUIDFactory::pseudoRandomUUID();
        $uuid = (string) $uuid;

        $repository = new UserRepository($registry);
        $this->assertIsBool($repository->has($uuid));
        $this->assertFalse($repository->has($uuid));
    }


    #[TestDox('UserRepository::save exists')]
    public function testUserRepositorySaveExists(): void
    {
        $class = new ReflectionClass(UserRepository::class);
        $this->assertTrue($class->hasMethod('save'));
    }

    #[Depends('testUserRepositorySaveExists')]
    #[TestDox('UserRepository::save is public')]
    public function testUserRepositorySaveIsPublic(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'save');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testUserRepositorySaveExists')]
    #[TestDox('UserRepository::save has one REQUIRED parameter')]
    public function testUserRepositorySaveHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UserRepository::class, 'save');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUserRepositorySaveExists')]
    #[Depends('testUserRepositorySaveIsPublic')]
    #[Depends('testUserRepositorySaveHasOneRequiredParameter')]
    #[TestDox('UserRepository::save creates new user record')]
    public function testUserRepositorySaveCreatesNewUserRecord(): void
    {
        // Begin transaction for tests:
        $registry = Registry::getInstance();
        $registry->get('Database')->beginTransaction();

        // User does not yet exist:
        $user_repository = new UserRepository($registry);
        $this->assertFalse($user_repository->has('John.Doe@example.com'));

        // Create a new user:
        $user = new User();
        $user->setEmailAddress('John.Doe@example.com');
        $user->setDateTimeZone(new \DateTimeZone('Europe/Amsterdam'));
        $user->setLanguageID('nl-NL');
        $user->setUserGroupID(254);

        // New user has no user ID yet:
        $this->assertFalse($user->hasIdentifier());

        // Create a new random password:
        $password = bin2hex(random_bytes(random_int(5, 8)));
        $password = new Password($password);
        $password->encrypt();

        // Set the user password:
        $user->setPasswordSalt($password->getSalt());
        $user->setPasswordHash($password->getHash());
        $user->setHashAlgorithm($password->getAlgorithm());

        // Save new user:
        $this->assertTrue($user_repository->save($user));

        // New user now has a user ID:
        $this->assertTrue($user->hasIdentifier());

        // Rollback transaction for tests:
        $registry->get('Database')->rollback();
    }
}
