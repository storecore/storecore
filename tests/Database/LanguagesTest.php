<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \PDOException as DatabaseException;
use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\Languages::class)]
#[CoversClass(\StoreCore\I18N\Language::class)]
#[CoversClass(\StoreCore\Types\LanguageCode::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class LanguagesTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_languages`');
            $registry->get('Database')->query('SELECT 1 FROM `sc_translation_memory`');
            $registry->get('Database')->exec('FLUSH TABLES `sc_languages`');
        } catch (DatabaseException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[Group('distro')]
    #[TestDox('Languages class is concrete')]
    public function testLanguagesClassIsConcrete(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Languages is a database model')]
    public function testLanguagesIsDatabaseModel(): void
    {
        $registry = Registry::getInstance();
        $languages = new Languages($registry);
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $languages);
        $this->assertInstanceOf(\StoreCore\Database\AbstractModel::class, $languages);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Languages::VERSION);
        $this->assertIsString(Languages::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Languages::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Languages::disable exists')]
    public function testLanguagesDisableExists(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasMethod('disable'));
    }

    #[TestDox('Languages::disable is public')]
    public function testLanguagesDisableIsPublic(): void
    {
        $method = new ReflectionMethod(Languages::class, 'disable');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Languages::disable has one REQUIRED parameter')]
    public function testLanguagesDisableHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Languages::class, 'disable');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Languages::enable exists')]
    public function testLanguagesEnableExists(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasMethod('enable'));
    }

    #[TestDox('Languages::enable is public')]
    public function testLanguagesEnableIsPublic(): void
    {
        $method = new ReflectionMethod(Languages::class, 'enable');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Languages::enable has one REQUIRED parameter')]
    public function testLanguagesEnableHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Languages::class, 'enable');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Languages::getAvailableLanguages exists')]
    public function testLanguagesGetAvailableLanguagesExists(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasMethod('getAvailableLanguages'));
    }

    #[TestDox('Languages::getAvailableLanguages is public')]
    public function testLanguagesGetAvailableLanguagesIsPublic(): void
    {
        $method = new ReflectionMethod(Languages::class, 'getAvailableLanguages');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Languages::getAvailableLanguages has no parameters')]
    public function testLanguagesGetAvailableLanguagesHasNoParameters(): void
    {
        $method = new ReflectionMethod(Languages::class, 'getAvailableLanguages');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLanguagesGetAvailableLanguagesExists')]
    #[Depends('testLanguagesGetAvailableLanguagesIsPublic')]
    #[Depends('testLanguagesGetAvailableLanguagesHasNoParameters')]
    #[TestDox('Languages::getAvailableLanguages returns non-empty array')]
    public function testLanguagesGetAvailableLanguagesReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        $languages = new Languages($registry);
        $this->assertNotEmpty($languages->getAvailableLanguages());
        $this->assertIsArray($languages->getAvailableLanguages());
    }

    #[Depends('testLanguagesGetAvailableLanguagesReturnsNonEmptyArray')]
    #[TestDox('Languages::getAvailableLanguages returns default languages')]
    public function testLanguagesGetAvailableLanguagesReturnsDefaultLanguages(): void
    {
        $registry = Registry::getInstance();
        $languages = new Languages($registry);

        $this->assertArrayHasKey('en-GB', $languages->getAvailableLanguages());
        $this->assertArrayHasKey('en-US', $languages->getAvailableLanguages());

        $this->assertArrayHasKey('de-DE', $languages->getAvailableLanguages());
        $this->assertArrayHasKey('fr-FR', $languages->getAvailableLanguages());
        $this->assertArrayHasKey('nl-BE', $languages->getAvailableLanguages());
        $this->assertArrayHasKey('nl-NL', $languages->getAvailableLanguages());
    }


    #[TestDox('Languages::has exists')]
    public function testLanguagesHasExists(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('Languages::has is public')]
    public function testLanguagesHasIsPublic(): void
    {
        $method = new ReflectionMethod(Languages::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Languages::has has one REQUIRED parameter')]
    public function testLanguagesHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Languages::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLanguagesGetAvailableLanguagesReturnsNonEmptyArray')]
    #[TestDox('Languages::has returns (bool) true if language exists')]
    public function testLanguagesHasReturnsBoolTrueIfLanguageExists(): void
    {
        $registry = Registry::getInstance();
        $languages = new Languages($registry);
        $this->assertIsBool($languages->has('en-GB'));
        $this->assertTrue($languages->has('en-GB'));

        $this->assertIsBool($languages->has('xx-XX'));
        $this->assertFalse($languages->has('xx-XX'));
    }

    #[Depends('testLanguagesHasReturnsBoolTrueIfLanguageExists')]
    #[TestDox('Languages::has accepts two-letter language code')]
    public function testLanguagesHasAcceptsTwoLetterLanguageCode(): void
    {
        $registry = Registry::getInstance();
        $languages = new Languages($registry);

        $this->assertIsBool($languages->has('en'));
        $this->assertTrue($languages->has('en'));
        $this->assertFalse($languages->has('xx'));
    }

    /**
     * @see https://www.loc.gov/standards/iso639-2/php/English_list.php
     */
    #[Depends('testLanguagesHasReturnsBoolTrueIfLanguageExists')]
    #[TestDox('Languages::has accepts three-letter language codes')]
    public function testLanguagesHasAcceptsThreeLetterLanguageCodes(): void
    {
        $registry = Registry::getInstance();
        $repository = new Languages($registry);

        $languages = [
            'deu' => 'German',
            'dut' => 'Dutch',
            'eng' => 'English',
            'fra' => 'French',
            'fre' => 'French',
            'ger' => 'German',
            'nld' => 'Dutch',
        ];
        foreach ($languages as $language_code => $language_name) {
            $this->assertTrue(
                $repository->has($language_code),
                'Language code "' . $language_code . '" for ' . $language_name . ' MUST be supported.'
            );
        }
    }


    #[TestDox('Languages::isEnabled exists')]
    public function testLanguagesIsEnabledExists(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasMethod('isEnabled'));
    }

    #[TestDox('Languages::isEnabled is public')]
    public function testLanguagesIsEnabledIsPublic(): void
    {
        $method = new ReflectionMethod(Languages::class, 'isEnabled');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Languages::isEnabled has one REQUIRED parameter')]
    public function testLanguagesIsEnabledHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Languages::class, 'isEnabled');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Individual languages can be enabled and disabled')]
    public function testIndividualLanguagesCanBeEnabledAndDisabled(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
        $languages = new Languages($registry);

        $this->assertTrue($languages->isEnabled('en-GB'));

        $this->assertTrue($languages->disable('en-GB'));
        $this->assertFalse($languages->isEnabled('en-GB'));
        $this->assertArrayNotHasKey('en-GB', $languages->getAvailableLanguages());

        $this->assertTrue($languages->enable('en-GB'));
        $this->assertTrue($languages->isEnabled('en-GB'));
        $this->assertArrayHasKey('en-GB', $languages->getAvailableLanguages());
    }
}
