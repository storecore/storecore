<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Database\OrderMapper::class)]
#[CoversClass(\StoreCore\Database\OrderItems::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class OrderMapperTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OrderMapper class is concrete')]
    public function testOrderMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderMapper is a database model')]
    public function testOrderMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrderMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('OrderMapper implements PSR-20 ClockInterface')]
    public function testOrderMapperImplementsPSR20ClockInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));

        $class = new ReflectionClass(OrderMapper::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('OrderMapper implements DataMapperInterface')]
    public function testOrderMapperImplementsDataMapperInterface(): void
    {
        $class = new ReflectionClass(OrderMapper::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\Database\DataMapperInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderMapper::VERSION);
        $this->assertIsString(OrderMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderMapper::delete exists')]
    public function testOrderMapperDeleteExists(): void
    {
        $class = new ReflectionClass(OrderMapper::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testOrderMapperDeleteExists')]
    #[TestDox('OrderMapper::delete is public')]
    public function testOrderMapperDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(OrderMapper::class, 'delete');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderMapperDeleteExists')]
    #[TestDox('OrderMapper::delete has one REQUIRED parameter')]
    public function testOrderMapperDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderMapper::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderMapperDeleteExists')]
    #[Depends('testOrderMapperDeleteIsPublic')]
    #[Depends('testOrderMapperDeleteHasOneRequiredParameter')]
    #[TestDox('OrderMapper::delete returns (bool) true on success')]
    public function testOrderMapperDeleteReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $mapper = new OrderMapper($registry);
        $mediumint = random_int(1, 16777215);
        $this->assertTrue($mapper->delete($mediumint));
    }
}
