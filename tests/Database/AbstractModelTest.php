<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\Database\AbstractModel::class)]
#[Group('hmvc')]
final class AbstractModelTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractModel class exists')]
    public function testAbstractModelClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'AbstractModel.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'AbstractModel.php');

        $this->assertTrue(class_exists('\\StoreCore\\Database\\AbstractModel'));
        $this->assertTrue(class_exists(AbstractModel::class));
    }

    #[TestDox('AbstractModel is abstract')]
    public function testAbstractModelIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractModel::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('Database\AbstractModel extends core AbstractModel')]
    public function testDatabaseAbstractModelExtendsCoreAbstractModel(): void
    {
        $class = new ReflectionClass(AbstractModel::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
    }


    #[TestDox('AbstractModel::__construct exists')]
    public function testAbstractModelConstructorExists(): void
    {
        $class = new ReflectionClass(AbstractModel::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testAbstractModelConstructorExists')]
    #[TestDox('AbstractModel::__construct is public constructor')]
    public function testAbstractModelConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(AbstractModel::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testAbstractModelConstructorExists')]
    #[TestDox('AbstractModel::__construct has one REQUIRED parameter')]
    public function testAbstractModelConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractModel::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
