<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
final class ContainerExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ContainerException class exists')]
    public function testContainerExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'ContainerException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'ContainerException.php');

        $this->assertTrue(class_exists('\\StoreCore\\Database\\ContainerException'));
        $this->assertTrue(class_exists(\StoreCore\Database\ContainerException::class));
    }

    #[Group('hmvc')]
    #[TestDox('ContainerException class is concrete')]
    public function testContainerExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ContainerException is Throwable RuntimeException')]
    public function testContainerExceptionIsThrowableRuntimeException(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-11 ContainerExceptionInterface exists')]
    public function testImplementedPSR11ContainerExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerExceptionInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerExceptionInterfaceExists')]
    #[TestDox('ContainerException implements PSR-11 ContainerExceptionInterface')]
    public function testContainerExceptionImplementsPsr11ContainerExceptionInterface(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerExceptionInterface::class));
    }
}
