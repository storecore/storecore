<?php

declare(strict_types=1);

namespace StoreCore\Database;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
class CRUDInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CRUDInterface interface file exists')]
    public function testCRUDInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CRUDInterface.php'
        );
    }

    #[Depends('testCRUDInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('CRUDInterface interface file is readable')]
    public function testCRUDInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CRUDInterface.php'
        );
    }

    #[Depends('testCRUDInterfaceInterfaceFileExists')]
    #[Depends('testCRUDInterfaceInterfaceFileIsReadable')]
    #[Group('distro')]
    #[TestDox('CRUDInterface interface exists')]
    public function testCRUDInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Database\\CRUDInterface'));
        $this->assertTrue(interface_exists(CRUDInterface::class));
    }
}
