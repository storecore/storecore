<?php

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\CreativeWorkMapper::class)]
final class CreativeWorkMapperTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CreativeWorkMapper class file exists')]
    public function testCreativeWorkMapperClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkMapper.php'
        );
    }

    #[Depends('testCreativeWorkMapperClassFileExists')]
    #[Group('distro')]
    #[TestDox('CreativeWorkMapper class file exists')]
    public function testCreativeWorkMapperClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'CreativeWorkMapper.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('CreativeWorkMapper is a database model')]
    public function testCreativeWorkMapperIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CreativeWorkMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('CreativeWorkMapper implements PSR-11 ContainerInterface')]
    public function testCreativeWorkMapperImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(CreativeWorkMapper::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkMapper::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkMapper::VERSION);
        $this->assertIsString(CreativeWorkMapper::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkMapper::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
