<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Route;
use StoreCore\RoutingQueue;
use StoreCore\PIM\Brand;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\BrandMapper::class)]
#[CoversClass(\StoreCore\Database\BrandRepository::class)]
#[CoversClass(\StoreCore\PIM\Brand::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\RoutingQueue::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class BrandRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_brands`');

            // Add three fictional brands with a UUID v1, v4, and v7:
            $registry->get('Database')->exec(
                "DELETE
                   FROM `sc_brands`
                  WHERE `global_brand_name` IN ('Octan', 'Alchemax', 'Buy-n-Large')"
            );

            $registry->get('Database')->exec(
                "INSERT INTO `sc_brands` 
                     (`brand_uuid`, `global_brand_name`) 
                   VALUES
                     (UNHEX('11ad4874312611ef94540242ac120002'), 'Octan'),
                     (UNHEX('1ca34aef243044f0b0dd61e7b690a3ba'), 'Alchemax'),
                     (UNHEX('019043aee96770848be7996929a2629b'), 'Buy-n-Large')"
            );

        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[TestDox('BrandRepository class is concrete')]
    public function testBrandRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('BrandRepository is a database model')]
    public function testBrandRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[TestDox('BrandRepository implements PSR-11 ContainerInterface')]
    public function testBrandRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BrandRepository::VERSION);
        $this->assertIsString(BrandRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BrandRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('BrandRepository::count exists')]
    public function testBrandRepositoryCountExists(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testBrandRepositoryCountExists')]
    #[TestDox('BrandRepository::count is public')]
    public function testBrandRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandRepositoryCountExists')]
    #[TestDox('BrandRepository::count has no parameters')]
    public function testBrandRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBrandRepositoryCountExists')]
    #[Depends('testBrandRepositoryCountIsPublic')]
    #[Depends('testBrandRepositoryCountHasNoParameters')]
    #[TestDox('BrandRepository::count returns integer')]
    public function testBrandRepositoryCountReturnsInteger(): void
    {
        $registry = Registry::getInstance();
        $brands = new BrandRepository($registry);
        $this->assertIsInt($brands->count());
        $this->assertGreaterThanOrEqual(3, $brands->count());
    }

    #[Depends('testBrandRepositoryCountReturnsInteger')]
    #[TestDox('BrandRepository is countable')]
    public function testBrandRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->isSubclassOf(\Countable::class));
    }


    #[TestDox('BrandRepository::get exists')]
    public function testBrandRepositoryGetExists(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testBrandRepositoryGetExists')]
    #[TestDox('BrandRepository::get is public')]
    public function testBrandRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandRepositoryGetExists')]
    #[TestDox('BrandRepository::get has one REQUIRED parameters')]
    public function testBrandRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrandRepositoryGetExists')]
    #[TestDox('BrandRepository::get returns Brand')]
    public function testBrandRepositoryGetReturnsBrand(): void
    {
        $registry = Registry::getInstance();
        $brands = new BrandRepository($registry);
        $actual = $brands->get('1ca34aef-2430-44f0-b0dd-61e7b690a3ba');
        $this->assertInstanceOf(\StoreCore\PIM\Brand::class, $actual);
        $this->assertInstanceOf(Brand::class, $actual);

        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $actual);
        $this->assertTrue($actual->hasIdentifier());
        $this->assertEquals('1ca34aef-2430-44f0-b0dd-61e7b690a3ba', $actual->getIdentifier()->__toString());
    }


    #[TestDox('BrandRepository::getLastModified exists')]
    public function testBrandRepositoryGetLastModifiedExists(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->hasMethod('getLastModified'));
    }

    #[Depends('testBrandRepositoryGetLastModifiedExists')]
    #[TestDox('BrandRepository::getLastModified is public')]
    public function testBrandRepositoryGetLastModifiedIsPublic(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'getLastModified');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandRepositoryGetLastModifiedExists')]
    #[TestDox('BrandRepository::getLastModified has no parameters')]
    public function testBrandRepositoryGetLastModifiedHasNoParameters(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'getLastModified');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBrandRepositoryGetLastModifiedExists')]
    #[TestDox('BrandRepository::getLastModified returns \DateTimeInterface')]
    public function testBrandRepositoryGetLastModifiedReturnsDateTimeInterface(): void
    {
        $registry = Registry::getInstance();
        $brands = new BrandRepository($registry);
        $actual = $brands->getLastModified();
        $this->assertInstanceOf(\DateTimeInterface::class, $actual);
    }


    #[TestDox('BrandRepository::list exists')]
    public function testBrandRepositoryListExists(): void
    {
        $class = new ReflectionClass(BrandRepository::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[Depends('testBrandRepositoryListExists')]
    #[TestDox('BrandRepository::list is public')]
    public function testBrandRepositoryListIsPublic(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'list');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testBrandRepositoryListExists')]
    #[TestDox('BrandRepository::list has no REQUIRED parameters')]
    public function testBrandRepositoryListHasNoRequiredParameters(): void
    {
        $method = new ReflectionMethod(BrandRepository::class, 'list');
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testBrandRepositoryListExists')]
    #[Depends('testBrandRepositoryListIsPublic')]
    #[Depends('testBrandRepositoryListHasNoRequiredParameters')]
    #[TestDox('BrandRepository::list returns iterable key-value array')]
    public function testBrandRepositoryListReturnsIterableKeyValueArray(): void
    {
        $registry = Registry::getInstance();
        $brands = new BrandRepository($registry);
        $result = $brands->list();
        $this->assertIsIterable($result);
        
        $this->assertNotEmpty($result);
        $this->assertContains('Alchemax', $result);
        $this->assertContains('Buy-n-Large', $result);
        $this->assertContains('Octan', $result);
    }
}
