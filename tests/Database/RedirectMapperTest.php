<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Engine\Redirect;
use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\Database\RedirectMapper::class)]
#[CoversClass(\StoreCore\Database\RedirectCache::class)]
#[CoversClass(\StoreCore\Engine\Redirect::class)]
#[CoversClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class RedirectMapperTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('RedirectMapper class exists')]
    public function testRedirectMapperClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Database\\RedirectMapper'));
        $this->assertTrue(class_exists(RedirectMapper::class));
    }


    #[Group('hmvc')]
    #[TestDox('RedirectMapper class is concrete')]
    public function testRedirectMapperClassIsConcrete(): void
    {
        $class = new ReflectionClass(RedirectMapper::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('RedirectMapper is a data access object (DAO)')]
    public function testRedirectMapperIsDataAccessObjectDAO(): void
    {
        $class = new ReflectionClass(RedirectMapper::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractDataAccessObject::class));
    }


    #[TestDox('RedirectMapper::save() exists')]
    public function testRedirectMapperSaveExists(): void
    {
        $class = new ReflectionClass(RedirectMapper::class);
        $this->assertTrue($class->hasMethod('save'));
    }

    #[Depends('testRedirectMapperSaveExists')]
    #[TestDox('RedirectMapper::save() is public')]
    public function testRedirectMapperSaveIsPublic(): void
    {
        $method = new ReflectionMethod(RedirectMapper::class, 'save');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[Depends('testRedirectMapperSaveExists')]
    #[TestDox('RedirectMapper::save() has one REQUIRED parameter')]
    public function testRedirectMapperSaveHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RedirectMapper::class, 'save');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRedirectMapperSaveExists')]
    #[Depends('testRedirectMapperSaveIsPublic')]
    #[Depends('testRedirectMapperSaveHasOneRequiredParameter')]
    #[TestDox('RedirectMapper::save() persists redirect')]
    public function testRedirectMapperSavePersistsRedirect(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $redirect = new Redirect('//localhost/kb', 'https://www.storecore.io/knowledge-base');

        $mapper = new RedirectMapper($registry);
        $this->assertTrue($mapper->save($redirect));

        $cache = new RedirectCache($registry);
        $key = RedirectCache::createKey('//localhost/kb');
        $this->assertTrue($cache->has($key));
    }
}
