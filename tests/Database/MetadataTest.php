<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\CRM\Organization;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class MetadataTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Metadata class is concrete')]
    public function testMetadataClassIsConcrete(): void
    {
        $class = new ReflectionClass(Metadata::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Metadata::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Metadata::VERSION);
        $this->assertIsString(Metadata::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Metadata::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Metadata.dateCreated exists')]
    public function testMetadataDateCreatedExists(): void
    {
        $metadata = new Metadata();
        $this->assertObjectHasProperty('dateCreated', $metadata);
    }

    #[Depends('testMetadataDateCreatedExists')]
    #[TestDox('Metadata.dateCreated is public read-only')]
    public function testMetadataDateCreatedIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Metadata::class, 'dateCreated');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testMetadataDateCreatedIsPublicReadOnly')]
    #[TestDox('Metadata.dateCreated implements DateTimeInterface')]
    public function testMetadataDateCreatedImplementsDateTimeInterface(): void
    {
        $metadata = new Metadata();
        $this->assertNotEmpty($metadata->dateCreated);
        $this->assertInstanceOf(\DateTimeInterface::class, $metadata->dateCreated);
    }

    #[Depends('testMetadataDateCreatedImplementsDateTimeInterface')]
    #[TestDox('Metadata.dateCreated is StoreCore\Types\DateTime with current date and time by default')]
    public function testMetadataDateCreatedIsStoreCoreTypesDateTimeWithCurrentDateAndTimeByDefault(): void
    {
        $metadata = new Metadata();
        $this->assertInstanceOf(\StoreCore\Types\DateTime::class, $metadata->dateCreated);
        $this->assertSame(
            gmdate('Y-m-d H:i:s'),
            $metadata->dateCreated->format('Y-m-d H:i:s')
        );
    }

    #[Depends('testMetadataDateCreatedImplementsDateTimeInterface')]
    #[TestDox('Metadata.dateCreated can be set by Metadata::__construct')]
    public function testMetadataDateCreatedCanBeSetByMetadataConstructor(): void
    {
        $metadata = new Metadata('2022-04-01 12:34');
        $this->assertEquals(
            '2022-04-01 12:34:00',
            $metadata->dateCreated->format('Y-m-d H:i:s')
        );
    }


    #[TestDox('Metadata.dateModified exists')]
    public function testMetadataDateModifiedExists(): void
    {
        $metadata = new Metadata();
        $this->assertObjectHasProperty('dateModified', $metadata);
    }

    #[Depends('testMetadataDateModifiedExists')]
    #[TestDox('Metadata.dateModified is public')]
    public function testMetadataDateModifiedIsPublic(): void
    {
        $property = new ReflectionProperty(Metadata::class, 'dateModified');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMetadataDateModifiedIsPublic')]
    #[TestDox('Metadata.dateModified is null by default')]
    public function testMetadataDateModifiedIsNullByDefault(): void
    {
        $metadata = new Metadata();
        $this->assertEmpty($metadata->dateModified);
        $this->assertNull($metadata->dateModified);
    }

    #[Depends('testMetadataDateModifiedIsNullByDefault')]
    #[TestDox('Metadata.dateModified accepts DateTimeInterface')]
    public function testMetadataDateModifiedAcceptsDateTimeInterface(): void
    {
        $metadata = new Metadata();
        $metadata->dateModified = new \DateTime('now');
        $this->assertNotEmpty($metadata->dateModified);
        $this->assertInstanceOf(\DateTimeInterface::class, $metadata->dateModified);
    }


    #[TestDox('Metadata.hash exists')]
    public function testMetadataHashExists(): void
    {
        $metadata = new Metadata();
        $this->assertObjectHasProperty('hash', $metadata);
    }

    #[Depends('testMetadataHashExists')]
    #[TestDox('Metadata.hash is public')]
    public function testMetadataHashIsPublic(): void
    {
        $property = new ReflectionProperty(Metadata::class, 'hash');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testMetadataHashExists')]
    #[Depends('testMetadataHashIsPublic')]
    #[TestDox('Metadata.hash is string')]
    public function testMetadataHashIsString(): void
    {
        $metadata = new Metadata();
        $this->assertIsString($metadata->hash);
    }

    #[Depends('testMetadataHashIsString')]
    #[TestDox('Metadata.hash is empty by default')]
    public function testMetadataHashIsEmptyByDefault(): void
    {
        $metadata = new Metadata();
        $this->assertNotNull($metadata->hash);
        $this->assertEmpty($metadata->hash);
    }

    #[Depends('testMetadataHashIsString')]
    #[Depends('testMetadataHashIsEmptyByDefault')]
    #[TestDox('Metadata.hash accepts MD5 hash')]
    public function testMetadataHashAcceptsMD5Hash(): void
    {
        $uuid = UUIDFactory::pseudoRandomUUID();
        $organization = new Organization('Acme Corporation');
        $organization->setIdentifier($uuid);

        $metadata = new Metadata();
        $hash = md5(serialize($organization));
        $metadata->hash = $hash;

        $this->assertNotEmpty($metadata->hash);
        $this->assertIsString($metadata->hash);
        $this->assertEquals($hash, $metadata->hash);
        $this->assertSame(md5(serialize($organization)), $metadata->hash);
    }


    #[TestDox('Metadata.id exists')]
    public function testMetadataIdExists(): void
    {
        $metadata = new Metadata();
        $this->assertObjectHasProperty('id', $metadata);
    }

    #[Depends('testMetadataIdExists')]
    #[TestDox('Metadata.id is public')]
    public function testMetadataIdIsPublic(): void
    {
        $property = new ReflectionProperty(Metadata::class, 'id');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMetadataIdExists')]
    #[Depends('testMetadataIdIsPublic')]
    #[TestDox('Metadata.id is null by default')]
    public function testMetadataIdIsNullByDefault(): void
    {
        $metadata = new Metadata();
        $this->assertNull($metadata->id);
    }

    /**
     * @see https://dev.mysql.com/doc/refman/8.0/en/integer-types.html
     */
    #[Depends('testMetadataIdIsNullByDefault')]
    #[TestDox('Metadata.id can be set to unsigned integer')]
    public function testMetadataIdCanBeSetToUnsignedInteger(): void
    {
        $unsigned_integers = [
            'TINYINT'   =>          255,
            'SMALLINT'  =>        65535,
            'MEDIUMINT' =>     16777215,
            'INT'       =>   4294967295,
            'BIGINT'    => \PHP_INT_MAX,
        ];

        $metadata = new Metadata();
        foreach ($unsigned_integers as $value) {
            $metadata->id = $value;
            $this->assertNotNull($metadata->id);
            $this->assertIsInt($metadata->id);
            $this->assertEquals($value, $metadata->id);
        }
    }

    #[Depends('testMetadataIdIsNullByDefault')]
    #[Depends('testMetadataIdCanBeSetToUnsignedInteger')]
    #[TestDox('Metadata.id can be set to UUID')]
    public function testMetadataIdCanBeSetToUuid(): void
    {
        $metadata = new Metadata();
        $metadata->id = UUIDFactory::pseudoRandomUUID();
        $this->assertNotNull($metadata->id);
        $this->assertIsNotInt($metadata->id);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $metadata->id);
    }


    #[TestDox('Metadata.version exists')]
    public function testMetadataVersionExists(): void
    {
        $metadata = new Metadata();
        $this->assertObjectHasProperty('version', $metadata);
    }

    #[Depends('testMetadataVersionExists')]
    #[TestDox('Metadata.version is public')]
    public function testMetadataVersionIsPublic(): void
    {
        $property = new ReflectionProperty(Metadata::class, 'version');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMetadataVersionExists')]
    #[Depends('testMetadataVersionIsPublic')]
    #[TestDox('Metadata.version is non-empty integer')]
    public function testMetadataVersionIsNonEmptyInteger(): void
    {
        $metadata = new Metadata();
        $this->assertNotEmpty($metadata->version);
        $this->assertIsInt($metadata->version);
    }

    #[Depends('testMetadataVersionIsNonEmptyInteger')]
    #[TestDox('Metadata.version is 1 by default')]
    public function testMetadataVersionIs1ByDefault(): void
    {
        $metadata = new Metadata();
        $this->assertEquals(1, $metadata->version);
    }

    #[Depends('testMetadataVersionIs1ByDefault')]
    #[TestDox('Metadata.version is writable')]
    public function testMetadataVersionIsWritable(): void
    {
        $metadata = new Metadata();
        $metadata->version = 42;
        $this->assertEquals(42, $metadata->version);
    }


    #[TestDox('Metadata::__construct exists')]
    public function testMetadataConstructorExists(): void
    {
        $class = new ReflectionClass(Metadata::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testMetadataConstructorExists')]
    #[TestDox('Metadata::__construct is public constructor')]
    public function testMetadataConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Metadata::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testMetadataConstructorExists')]
    #[TestDox('Metadata::__construct has two OPTIONAL parameters')]
    public function testMetadataConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(Metadata::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMetadataConstructorHasTwoOptionalParameters')]
    #[TestDox('Metadata::__construct sets Metadata.dateCreated and Metadata.dateModified')]
    public function testMetadataConstructorSetsMetadataDateCreatedAndMetadataDateModified(): void
    {
        $metadata = new Metadata('2022-04-01 12:34:56', '2022-04-02 01:02:34');

        $this->assertInstanceOf(\DateTimeInterface::class, $metadata->dateCreated);
        $this->assertSame('2022-04-01 12:34:56', $metadata->dateCreated->format('Y-m-d H:i:s'));

        $this->assertInstanceOf(\DateTimeInterface::class, $metadata->dateModified);
        $this->assertSame('2022-04-02 01:02:34', $metadata->dateModified->format('Y-m-d H:i:s'));
    }


    #[TestDox('Metadata::hash method exists')]
    public function testMetadataHashMethodExists(): void
    {
        $class = new ReflectionClass(Metadata::class);
        $this->assertTrue($class->hasMethod('hash'));
    }

    #[Depends('testMetadataHashMethodExists')]
    #[TestDox('Metadata::hash is public static')]
    public function testMetadataHashIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Metadata::class, 'hash');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testMetadataHashMethodExists')]
    #[Depends('testMetadataHashIsPublicStatic')]
    #[TestDox('Metadata::hash returns hash strings for JSON serializable objects')]
    public function testMetadataHashReturnsHashStringsForJsonSerializableObjects(): void
    {
        $organization = new Organization('Acme Corporation');
        $this->assertInstanceOf(\JsonSerializable::class, $organization);
        $this->assertIsObject($organization);

        $this->assertNotEmpty(Metadata::hash($organization));
        $this->assertIsString(Metadata::hash($organization));
        $this->assertTrue(\ctype_xdigit(Metadata::hash($organization)));
        $this->assertEquals(32, \strlen(Metadata::hash($organization)));
    }


    #[TestDox('Metadata::toArray() exists')]
    public function testMetadataToArrayExists(): void
    {
        $class = new ReflectionClass(Metadata::class);
        $this->assertTrue($class->hasMethod('toArray'));
    }

    #[Depends('testMetadataToArrayExists')]
    #[TestDox('Metadata::toArray() is public')]
    public function testMetadatatoArrayIsPublic(): void
    {
        $method = new ReflectionMethod(Metadata::class, 'toArray');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMetadataToArrayExists')]
    #[TestDox('Metadata::toArray() has no parameters')]
    public function testMetadataToArrayHasNoParameters(): void
    {
        $method = new ReflectionMethod(Metadata::class, 'toArray');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMetadataToArrayHasNoParameters')]
    #[TestDox('Metadata::toArray() returns non-empty array')]
    public function testMetadataToArrayReturnsNonEmptyArray(): void
    {
        $metadata = new Metadata();
        $this->assertNotEmpty($metadata->toArray());
        $this->assertIsArray($metadata->toArray());
    }

    #[Depends('testMetadataToArrayReturnsNonEmptyArray')]
    #[TestDox('Metadata::toArray() returns date_created and date_modified')]
    public function testMetadataToArrayReturnsDateCreatedAndDateModified(): void
    {
        $metadata = new Metadata();
        $this->assertArrayHasKey('date_created', $metadata->toArray());
        $this->assertArrayHasKey('date_modified', $metadata->toArray());

        $metadata = new Metadata('2021-02-03T04:56:00+00:00');
        $this->assertSame('2021-02-03 04:56:00', $metadata->toArray()['date_created']);
        $this->assertNull($metadata->toArray()['date_modified']);

        $metadata = new Metadata('2021-02-03T04:56:00+00:00', '2022-03-04T05:43:21+00:00');
        $this->assertSame('2021-02-03 04:56:00', $metadata->toArray()['date_created']);
        $this->assertSame('2022-03-04 05:43:21', $metadata->toArray()['date_modified']);
    }

    #[Depends('testMetadataToArrayReturnsNonEmptyArray')]
    #[TestDox('Metadata::toArray() returns (int) version')]
    public function testMetadataToArrayReturnsIntVersion(): void
    {
        $metadata = new Metadata();
        $this->assertArrayHasKey('version', $metadata->toArray());
        $this->assertIsInt($metadata->toArray()['version']);
        $this->assertEquals(1, $metadata->toArray()['version']);
    }

    #[Depends('testMetadataToArrayReturnsNonEmptyArray')]
    #[TestDox('Metadata::toArray() returns int|null id')]
    public function testMetadataToArrayReturnsIntOrNullId(): void
    {
        $metadata = new Metadata();
        $this->assertArrayHasKey('id', $metadata->toArray());
        $this->assertIsNotInt($metadata->toArray()['id']);
        $this->assertNull($metadata->toArray()['id']);

        $metadata->id = 42;
        $this->assertNotNull($metadata->toArray()['id']);
        $this->assertIsInt($metadata->toArray()['id']);
        $this->assertEquals(42, $metadata->toArray()['id']);
    }

    #[Depends('testMetadataToArrayReturnsNonEmptyArray')]
    #[Depends('testMetadataToArrayReturnsIntOrNullId')]
    #[TestDox('Metadata::toArray() returns UUID id as lowercase string without hyphens')]
    public function testMetadataToArrayReturnsUuidIdAsLowercaseStringWithoutHyphens(): void
    {
        $metadata = new Metadata();
        $metadata->id = UUIDFactory::pseudoRandomUUID();

        $this->assertArrayHasKey('id', $metadata->toArray());
        $this->assertNotNull($metadata->toArray()['id']);
        $this->assertIsNotInt($metadata->toArray()['id']);

        $this->assertIsString($metadata->toArray()['id']);
        $this->assertStringNotContainsString('-', $metadata->toArray()['id']);
        $this->assertEquals(\strtolower($metadata->toArray()['id']), $metadata->toArray()['id']);
        $this->assertTrue(\ctype_xdigit($metadata->toArray()['id']));
    }
}
