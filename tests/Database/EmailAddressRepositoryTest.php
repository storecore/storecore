<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Types\EmailAddress;

use \PDOException as DatabaseException;
use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Database\EmailAddressRepository::class)]
#[CoversClass(\StoreCore\Database\EmailAddressMapper::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\EmailAddress::class)]
final class EmailAddressRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_email_addresses`');
        } catch (DatabaseException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function tearDown(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->exec(
                "DELETE
                   FROM `sc_email_addresses`
                  WHERE `email_address` = 'info@example.com'
                     OR `email_address` = 'noreply@example.com'"
            );
        }
    }


    #[Group('hmvc')]
    #[TestDox('EmailAddressRepository class is concrete')]
    public function testEmailAddressRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('EmailAddressRepository is a database model')]
    public function testEmailAddressRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('EmailAddressRepository implements PSR-11 ContainerInterface')]
    public function testEmailAddressRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EmailAddressRepository::VERSION);
        $this->assertIsString(EmailAddressRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EmailAddressRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('EmailAddressRepository::clear exists')]
    public function testEmailAddressRepositoryClearExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[TestDox('EmailAddressRepository::clear is public')]
    public function testEmailAddressRepositoryClearIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'clear');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('EmailAddressRepository::clear has no parameters')]
    public function testEmailAddressRepositoryClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('EmailAddressRepository::clear returns (bool) true by default')]
    public function testEmailAddressRepositoryClearReturnsBoolTrueByDefault(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);
        $this->assertIsBool($repository->clear());
        $this->assertTrue($repository->clear());
    }


    #[TestDox('EmailAddressRepository::count exists')]
    public function testEmailAddressRepositoryCountExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testEmailAddressRepositoryCountExists')]
    #[TestDox('EmailAddressRepository::count exists')]
    public function testEmailAddressRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testEmailAddressRepositoryCountExists')]
    #[TestDox('EmailAddressRepository::count has no parameters')]
    public function testEmailAddressRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testEmailAddressRepositoryCountExists')]
    #[Depends('testEmailAddressRepositoryCountIsPublic')]
    #[Depends('testEmailAddressRepositoryCountHasNoParameters')]
    #[TestDox('EmailAddressRepository::count returns positive integer')]
    public function testEmailAddressRepositoryCountReturnsPositiveInteger(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);
        $this->assertIsInt($repository->count());
        $this->assertGreaterThanOrEqual(0, $repository->count());
    }

    #[Depends('testEmailAddressRepositoryCountReturnsPositiveInteger')]
    #[TestDox('EmailAddressRepository is Countable')]
    public function testEmailAddressRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));

        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);
        $this->assertGreaterThanOrEqual(0, count($repository));
        $this->assertEquals($repository->count(), count($repository));
    }


    #[TestDox('EmailAddressRepository::get exists')]
    public function testEmailAddressRepositoryGetExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('EmailAddressRepository::get is public')]
    public function testEmailAddressRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('EmailAddressRepository::get has one REQUIRED parameter')]
    public function testEmailAddressRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('EmailAddressRepository::get returns email address as object')]
    public function testEmailAddressRepositoryGetReturnsEmailAddressAsObject(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);

        $address = $repository->createFromString('noreply@example.com');
        $this->assertIsString($repository->set($address));
        $this->assertTrue($repository->has('noreply@example.com'));

        $this->assertIsObject($repository->get('noreply@example.com'));
        $this->assertInstanceOf(\StoreCore\Types\EmailAddress::class, $repository->get('noreply@example.com'));

        $this->assertTrue($repository->unset('noreply@example.com'));
        $this->assertFalse($repository->has('noreply@example.com'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $repository->get('noreply@example.com');
    }


    #[TestDox('EmailAddressRepository::has exists')]
    public function testEmailAddressRepositoryHasExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('EmailAddressRepository::has is public')]
    public function testEmailAddressRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('EmailAddressRepository::has has one REQUIRED parameter')]
    public function testEmailAddressRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('EmailAddressRepository::has returns (bool) true if email address exists')]
    public function testEmailAddressRepositoryHasReturnsTrueIfEmailAddressExists(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);
        $this->assertFalse($repository->has('noreply@example.com'));

        $address = 'noreply@example.com';
        $this->assertFalse($repository->has($address));

        $address = $repository->createFromString($address);
        $this->assertIsNotString($address);
        $this->assertIsObject($address);
        $this->assertInstanceOf(\Stringable::class, $address);
        $this->assertInstanceOf(\StoreCore\Types\EmailAddress::class, $address);

        $this->assertIsString($repository->set($address));
        $this->assertTrue($repository->has('noreply@example.com'));
        $this->assertIsInt($address->metadata->id);

        $this->assertTrue($repository->unset('noreply@example.com'));
        $this->assertFalse($repository->has('noreply@example.com'));
    }


    #[TestDox('EmailAddressRepository::set exists')]
    public function testEmailAddressRepositorySetExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('EmailAddressRepository::set is public')]
    public function testEmailAddressRepositorySetIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('EmailAddressRepository::set has one REQUIRED parameter')]
    public function testEmailAddressRepositorySetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('EmailAddressRepository::set saves a new email address')]
    public function testEmailAddressRepositorySetSavesNewEmailAddress(): void
    {
        $registry = Registry::getInstance();

        $address = new EmailAddress('info@example.com');
        $mapper = new EmailAddressMapper($registry);
        $this->assertTrue($mapper->delete($address->metadata->hash));
        unset($address, $mapper);
        
        $repository = new EmailAddressRepository($registry);
        $this->assertFalse($repository->has('info@example.com'));
        $count = $repository->count();

        $address = new EmailAddress('info@example.com');
        $this->assertIsString($repository->set($address));
        $this->assertTrue($repository->has('info@example.com'));
        $this->assertEquals($count + 1, $repository->count());
    }

    #[Depends('testEmailAddressRepositorySetSavesNewEmailAddress')]
    #[TestDox('EmailAddressRepository::set updates an existing email address')]
    public function testEmailAddressRepositorySetUpdatesExistingEmailAddress(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);

        $address = new EmailAddress('info@example.com');
        $repository->set($address);
        $this->assertTrue($repository->has('info@example.com'));

        $address = $repository->get('info@example.com');
        $this->assertInstanceOf(EmailAddress::class, $address);
        $this->assertEmpty($address->metadata->dateModified);

        $address->displayName = 'Example.com';
        $this->assertIsString($repository->set($address));
        $this->assertNotEmpty($address->metadata->dateModified);

        $address = $repository->get('info@example.com');
        $this->assertIsInt($address->metadata->id);
        $this->assertInstanceOf(\DateTimeInterface::class, $address->metadata->dateCreated);
        $this->assertInstanceOf(\DateTimeInterface::class, $address->metadata->dateModified);
        $this->assertEquals('info@example.com', (string) $address);
        $this->assertEquals('Example.com', $address->displayName);

        $this->assertTrue($repository->unset((string) $address->metadata->id));
    }


    #[TestDox('EmailAddressRepository::unset exists')]
    public function testEmailAddressRepositoryUnsetExists(): void
    {
        $class = new ReflectionClass(EmailAddressRepository::class);
        $this->assertTrue($class->hasMethod('unset'));
    }

    #[TestDox('EmailAddressRepository::unset is public')]
    public function testEmailAddressRepositoryUnsetIsPublic(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'unset');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('EmailAddressRepository::unset has one REQUIRED parameter')]
    public function testEmailAddressRepositoryUnsetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(EmailAddressRepository::class, 'unset');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('EmailAddressRepository::unset accepts numeric strings')]
    public function testEmailAddressRepositoryUnsetAcceptsNumericStrings(): void
    {
        $registry = Registry::getInstance();
        $repository = new EmailAddressRepository($registry);

        // INT UNSIGNED range
        $this->assertFalse($repository->unset('0'));
        $this->assertTrue($repository->unset('1'));
        $this->assertTrue($repository->unset('4294967295'));
        $this->assertFalse($repository->unset('4294967296'));

        $this->expectException(\TypeError::class);
        $failure = $repository->unset(1);
    }
}
