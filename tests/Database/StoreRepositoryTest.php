<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\SessionFactory;
use StoreCore\Store;
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Database\StoreMapper::class)]
#[CoversClass(\StoreCore\Database\StoreRepository::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Store::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class StoreRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        } else {
            $registry->get('Database')->exec(
                "DELETE FROM `sc_stores` WHERE `store_uuid` = UNHEX('00000000000000000000000000000000')"
            );
        }
    }


    #[Group('hmvc')]
    #[TestDox('StoreRepository class is concrete')]
    public function testStoreRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('StoreRepository is a database model')]
    public function testStoreRepositoryIsADatabaseModel(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('StoreRepository is countable')]
    public function testStoreRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('StoreRepository implements PSR-11 ContainerInterface')]
    public function testStoreRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('get'));
        $this->assertTrue($class->hasMethod('has'));
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(StoreRepository::VERSION);
        $this->assertIsString(StoreRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(StoreRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('StoreRepository::set exists')]
    public function testStoreRepositorySetExists(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testStoreRepositorySetExists')]
    #[TestDox('StoreRepository::set is public')]
    public function testStoreRepositorySetIsPublic(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreRepositorySetExists')]
    #[TestDox('StoreRepository::set has one REQUIRED parameter')]
    public function testStoreRepositorySetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'set');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testStoreRepositorySetExists')]
    #[Depends('testStoreRepositorySetIsPublic')]
    #[Depends('testStoreRepositorySetHasOneRequiredParameter')]
    #[TestDox('StoreRepository::set persists Store objects')]
    public function testStoreRepositorySetPersistsStoreObjects(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $store = new Store($registry);
        $store->name = 'Altostrat.com';

        /*
         * Even if the new `Store` already has a UUID identifier, possibly
         * created by a remote client or server, we still have to be able
         * to add it to the local repository.
         */
        $this->assertFalse($store->hasIdentifier());
        $store->setIdentifier('00000000-0000-0000-0000-000000000000');
        $this->assertTrue($store->hasIdentifier());

        $repository = new StoreRepository($registry);
        $this->assertFalse($repository->has('00000000-0000-0000-0000-000000000000'));

        $this->assertTrue(UUID::validate($repository->set($store)));
        $this->assertTrue($repository->has('00000000-0000-0000-0000-000000000000'));
    }


    #[TestDox('StoreRepository::count exists')]
    public function testStoreRepositoryCountExists(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testStoreRepositoryCountExists')]
    #[TestDox('StoreRepository::count is public')]
    public function testStoreRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreRepositoryCountExists')]
    #[TestDox('StoreRepository::count has no parameters')]
    public function testStoreRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testStoreRepositoryCountExists')]
    #[Depends('testStoreRepositoryCountIsPublic')]
    #[Depends('testStoreRepositoryCountHasNoParameters')]
    #[TestDox('StoreRepository::count returns unsigned smallint')]
    public function testStoreRepositoryCountReturnsUnsignedSmallint(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $stores = new StoreRepository($registry);
        $this->assertIsInt($stores->count());
        $this->assertGreaterThanOrEqual(0, $stores->count());
        $this->assertLessThanOrEqual(65535, $stores->count());
    }


    #[TestDox('StoreRepository::getCurrentStore exists')]
    public function testStoreRepositoryGetCurrentStoreExists(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('getCurrentStore'));
    }

    #[Depends('testStoreRepositoryGetCurrentStoreExists')]
    #[TestDox('StoreRepository::getCurrentStore is public')]
    public function testStoreRepositoryGetCurrentStoreIsPublic(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'getCurrentStore');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreRepositoryGetCurrentStoreExists')]
    #[TestDox('StoreRepository::getCurrentStore has no parameters')]
    public function testStoreRepositoryGetCurrentStoreHasNoParameters(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'getCurrentStore');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testStoreRepositoryGetCurrentStoreExists')]
    #[Depends('testStoreRepositoryGetCurrentStoreIsPublic')]
    #[Depends('testStoreRepositoryGetCurrentStoreHasNoParameters')]
    #[TestDox('StoreRepository::getCurrentStore returns registered Store')]
    public function testStoreRepositoryGetCurrentStoreReturnsRegisteredStore(): void
    {
        $registry = Registry::getInstance();
        $this->assertFalse($registry->has('Store'));

        $store = new Store($registry);
        $registry->set('Store', $store);
        $this->assertTrue($registry->has('Store'));

        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
        $repository = new StoreRepository($registry);
        $this->assertInstanceOf(Store::class, $repository->getCurrentStore());
        $this->assertSame($store, $repository->getCurrentStore());
    }


    #[TestDox('StoreRepository::has exists')]
    public function testStoreRepositoryHasExists(): void
    {
        $class = new ReflectionClass(StoreRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testStoreRepositoryHasExists')]
    #[TestDox('StoreRepository::has is public')]
    public function testStoreRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testStoreRepositoryHasExists')]
    #[TestDox('StoreRepository::has has one REQUIRED parameter')]
    public function testStoreRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StoreRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testStoreRepositoryHasHasOneRequiredParameter')]
    #[TestDox('StoreRepository::has throws TypeError exception on integer')]
    public function testStoreRepositoryHasThrowsTypeErrorExceptionOnInteger(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $stores = new StoreRepository($registry);
        $this->expectException(\TypeError::class);
        $failure = $stores->has(42);
    }
}
