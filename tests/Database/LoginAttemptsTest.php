<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Database\LoginAttempts::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
class LoginAttemptsTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->exec(
                'TRUNCATE `sc_login_attempts`'
            );
        }
    }

    #[Group('hmvc')]
    #[TestDox('LoginAttempts class is concrete')]
    public function testLoginAttemptsClassIsConcrete(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LoginAttempts class is a database model')]
    public function testLoginAttemptsClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-20 ClockInterface exists')]
    public function testImplementedPSR20ClockInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));
        $this->assertTrue(interface_exists(\Psr\Clock\ClockInterface::class));
    }

    #[Depends('testImplementedPSR20ClockInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('LoginAttempts implements PSR-20 ClockInterface')]
    public function testLoginAttemptsImplementsPsr20ClockInterface(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LoginAttempts::VERSION);
        $this->assertIsString(LoginAttempts::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LoginAttempts::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('LoginAttempts::count exists')]
    public function testLoginAttemptsCountExists(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('LoginAttempts::count is public')]
    public function testLoginAttemptscountIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAttempts::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('LoginAttempts::count has one OPTIONAL parameter')]
    public function testLoginAttemptsCountHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(LoginAttempts::class, 'count');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('LoginAttempts::count returns integer greater than or equal to 0 (zero)')]
    public function testLoginAttemptsCountReturnsIntegerGreaterThanOrEqualToZero(): void
    {
        $registry = Registry::getInstance();
        $loginAudit = new LoginAttempts($registry);
        $this->assertIsInt($loginAudit->count());
        $this->assertGreaterThanOrEqual(0, $loginAudit->count());

        // `60` minutes for 1 hour
        $this->assertIsInt($loginAudit->count(60));
        $this->assertGreaterThanOrEqual(0, $loginAudit->count(60));
    }

    #[Depends('testLoginAttemptsCountReturnsIntegerGreaterThanOrEqualToZero')]
    #[TestDox('LoginAttempts is Countable')]
    public function testLoginAttemptsIsCountable(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));

        $registry = Registry::getInstance();
        $loginAudit = new LoginAttempts($registry);
        $this->assertIsInt(count($loginAudit));
        $this->assertEquals($loginAudit->count(), count($loginAudit));
        $this->assertGreaterThanOrEqual(0, count($loginAudit));
    }


    #[TestDox('LoginAttempts::storeAttempt exists')]
    public function testLoginAttemptsStoreAttemptExists(): void
    {
        $class = new ReflectionClass(LoginAttempts::class);
        $this->assertTrue($class->hasMethod('storeAttempt'));
    }

    #[TestDox('LoginAttempts::storeAttempt is public')]
    public function testLoginAttemptsstoreAttemptIsPublic(): void
    {
        $method = new ReflectionMethod(LoginAttempts::class, 'storeAttempt');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('LoginAttempts::storeAttempt has three OPTIONAL parameters')]
    public function testLoginAttemptsStoreAttemptHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(LoginAttempts::class, 'storeAttempt');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLoginAttemptsIsCountable')]
    #[TestDox('LoginAttempts::storeAttempt stores failed login attempts')]
    public function testLoginAttemptsStoreAttemptStoresFailedLoginAttempts(): void
    {
        $registry = Registry::getInstance();
        $loginAudit = new LoginAttempts($registry);
        $count = $loginAudit->count();

        $this->assertTrue(
            $loginAudit->storeAttempt('noreply@example.com'),
            'LoginAttempts::storeAttempt MUST accept only user credentials.'
        );
        $this->assertEquals($count + 1, $loginAudit->count());

        $this->assertTrue(
            $loginAudit->storeAttempt('noreply@example.com', '198.51.100.255'),
            'LoginAttempts::storeAttempt SHOULD accept user credentials and IPv4 address.'
        );
        $this->assertEquals($count + 2, $loginAudit->count());

        $this->assertTrue(
            $loginAudit->storeAttempt('noreply@example.com', '2001:db8:3:3:3:3:3:3'),
            'LoginAttempts::storeAttempt SHOULD accept user credentials and IPv6 address.'
        );
        $this->assertEquals($count + 3, $loginAudit->count());
    }

    #[Depends('testLoginAttemptsIsCountable')]
    #[Depends('testLoginAttemptsStoreAttemptHasThreeOptionalParameters')]
    #[Depends('testLoginAttemptsStoreAttemptStoresFailedLoginAttempts')]
    #[TestDox('LoginAttempts::storeAttempt stores successful login attempt')]
    public function testLoginAttemptsStoreAttemptStoresSuccessfulLoginAttempt(): void
    {
        $registry = Registry::getInstance();
        $loginAudit = new LoginAttempts($registry);
        $count = $loginAudit->count();

        $this->assertTrue(
            $loginAudit->storeAttempt('info@example.com', '203.0.113.0', true),
            'Successful attempts MUST explicitly be set with a boolean flag.'
        );

        $this->assertNotEquals(
            $count + 1, $loginAudit->count(),
            'Successful login attempts SHOULD NOT be counted.'
        );
        $this->assertEquals(
            $count, $loginAudit->count(),
            'Successful login attempts SHOULD NOT be counted.'
        );
    }
}
