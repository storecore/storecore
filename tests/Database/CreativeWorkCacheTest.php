<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Database\CreativeWorkCache::class)]
#[CoversClass(\StoreCore\Database\CreativeWorkMapper::class)]
#[CoversClass(\StoreCore\FileSystem\ObjectCacheReader::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class CreativeWorkCacheTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CreativeWorkCache is a database model')]
    public function testCreativeWorkCacheIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CreativeWorkCache::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
        $this->assertTrue($class->isSubclassOf(AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('CreativeWorkCache implements PSR-16 SimpleCache')]
    public function testCreativeWorkCacheImplementsPSR16SimpleCache(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));

        $class = new ReflectionClass(CreativeWorkCache::class);
        $this->assertTrue($class->implementsInterface(\Psr\SimpleCache\CacheInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('CreativeWorkCache implements PSR-20 ClockInterface')]
    public function testCreativeWorkCacheImplementsPSR20ClockInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));

        $class = new ReflectionClass(CreativeWorkCache::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkCache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkCache::VERSION);
        $this->assertIsString(CreativeWorkCache::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkCache::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWorkCache::clear exists')]
    public function testCreativeWorkCacheClearExists(): void
    {
        $class = new ReflectionClass(CreativeWorkCache::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[Depends('testCreativeWorkCacheClearExists')]
    #[TestDox('CreativeWorkCache::clear is final public')]
    public function testCreativeWorkCacheClearIsFinalPublic(): void
    {
        $method = new ReflectionMethod(CreativeWorkCache::class, 'clear');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCreativeWorkCacheClearExists')]
    #[TestDox('CreativeWorkCache::clear has no parameters')]
    public function testCreativeWorkCacheClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(CreativeWorkCache::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testCreativeWorkCacheClearExists')]
    #[Depends('testCreativeWorkCacheClearIsFinalPublic')]
    #[Depends('testCreativeWorkCacheClearHasNoParameters')]
    #[TestDox('CreativeWorkCache::clear returns bool')]
    public function testCreativeWorkCacheClearReturnsBool(): void
    {
        $registry = Registry::getInstance();
        $cache = new CreativeWorkCache($registry);
        $this->assertIsBool($cache->clear());
    }

    #[Depends('testCreativeWorkCacheClearReturnsBool')]
    #[TestDox('CreativeWorkCache::clear returns true by default')]
    public function testCreativeWorkCacheClearReturnsTrueByDefault(): void
    {
        $registry = Registry::getInstance();
        $cache = new CreativeWorkCache($registry);
        $this->assertTrue($cache->clear());
    }

    /**
     * @see https://mariadb.com/kb/en/mediumint/
     * @see https://dev.mysql.com/doc/refman/8.0/en/integer-types.html
     */
    #[TestDox('CreativeWorkCache::has throws Psr\SimpleCache\InvalidArgumentException on invalid cache key')]
    public function testCreativeWorkCacheHasThrowsPsrSimpleCacheInvalidArgumentExceptionOnInvalidCacheKey(): void
    {
        // `InvalidArgumentException` is NOT an `Exception` or `class`.
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\InvalidArgumentException'));

        $registry = Registry::getInstance();
        $cache = new CreativeWorkCache($registry);

        // Cache key is `MEDIUMINT UNSIGNED` with a maximum of 16777215.
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $failure = $cache->has('16777216');
    }
}
