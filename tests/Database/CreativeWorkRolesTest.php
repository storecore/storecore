<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;

use \PDOException as DatabaseException;
use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Database\CreativeWorkRoles::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
final class CreativeWorkRolesTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    
        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_creative_work_roles`');
        } catch (DatabaseException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[Group('hmvc')]
    #[TestDox('CreativeWorkRoles is a database model')]
    public function testCreativeWorkRolesIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CreativeWorkRoles::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkRoles::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkRoles::VERSION);
        $this->assertIsString(CreativeWorkRoles::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkRoles::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWorkRoles::list() exists')]
    public function testCreativeWorkRolesListExists(): void
    {
        $class = new ReflectionClass(CreativeWorkRoles::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[Depends('testCreativeWorkRolesListExists')]
    #[TestDox('CreativeWorkRoles::list() is public')]
    public function testCreativeWorkRolesListIsPublic(): void
    {
        $method = new ReflectionMethod(CreativeWorkRoles::class, 'list');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCreativeWorkRolesListExists')]
    #[TestDox('CreativeWorkRoles::list() has no parameters')]
    public function testCreativeWorkRolesListHasNoParameters(): void
    {
        $method = new ReflectionMethod(CreativeWorkRoles::class, 'list');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testCreativeWorkRolesListExists')]
    #[Depends('testCreativeWorkRolesListIsPublic')]
    #[Depends('testCreativeWorkRolesListHasNoParameters')]
    #[TestDox('CreativeWorkRoles::list() returns SplFixedArray')]
    public function testCreativeWorkRolesListReturnsSplFixedArray(): void
    {
        $registry = Registry::getInstance();
        $creativeWorkRoles = new CreativeWorkRoles($registry);
        $roles = $creativeWorkRoles->list();

        $this->assertInstanceOf(\SplFixedArray::class, $roles);

        $this->assertInstanceOf(\Countable::class, $roles);
        $this->assertGreaterThanOrEqual(16, count($roles));
    }
}
