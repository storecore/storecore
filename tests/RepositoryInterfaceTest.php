<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
#[Group('hmvc')]
final class RepositoryInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('RepositoryInterface exists')]
    public function testRepositoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\RepositoryInterface'));
        $this->assertTrue(interface_exists(RepositoryInterface::class));
    }

    #[Group('distro')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[TestDox('RepositoryInterface extends PSR-11 ContainerInterface')]
    public function testRepositoryInterfaceExtendsPSR11ContainerInterface(): void
    {
        $repository = new class implements RepositoryInterface {
            public function get(string $id): object {}
            public function has(string $id): bool {}
            public function set(IdentityInterface &$entity): string {}
            public function unset(string $id): bool {}
        };

        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $repository);
        $this->assertInstanceOf(\StoreCore\RepositoryInterface::class, $repository);
    }

    #[TestDox('Repositories that implement the RepositoryInterface')]
    public function testRepositoriesThatImplementTheRepositoryInterface(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\OrganizationRepository'));
        $class = new ReflectionClass(\StoreCore\CRM\OrganizationRepository::class);
        $this->assertTrue($class->implementsInterface(RepositoryInterface::class));

        $this->assertTrue(class_exists('\\StoreCore\\Database\\EmailAddressRepository'));
        $class = new ReflectionClass(\StoreCore\Database\EmailAddressRepository::class);
        $this->assertTrue($class->implementsInterface(RepositoryInterface::class));

        $this->assertTrue(class_exists('\\StoreCore\\Database\\StoreRepository'));
        $class = new ReflectionClass(\StoreCore\Database\StoreRepository::class);
        $this->assertTrue($class->implementsInterface(RepositoryInterface::class));

        $this->assertTrue(class_exists('\\StoreCore\\OML\\OrderRepository'));
        $class = new ReflectionClass(\StoreCore\OML\OrderRepository::class);
        $this->assertTrue($class->implementsInterface(RepositoryInterface::class));
    }
}
