<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \StoreCore\NotFoundException;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('hmvc')]
final class NotFoundExceptionTest extends TestCase
{
    #[TestDox('NotFoundException class is concrete')]
    public function testNotFoundExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('NotFoundException is a throwable exception')]
    public function testNotFoundExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
    }

    #[Depends('testNotFoundExceptionIsThrowableException')]
    #[TestDox('NotFoundException is a runtime exception')]
    public function testNotFoundExceptionIsRuntimeException(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));
    }

    #[Depends('testNotFoundExceptionIsThrowableException')]
    #[TestDox('NotFoundException is a container exception')]
    public function testNotFoundExceptionIsContainerException(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\ContainerException::class));
    }

    #[TestDox('NotFoundException implements PSR-11 NotFoundExceptionInterface')]
    public function testNotFoundExceptionImplementsPsr11NotFoundExceptionInterface(): void
    {
        $class = new ReflectionClass(NotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\NotFoundExceptionInterface::class));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = throw new NotFoundException();
    }
}
