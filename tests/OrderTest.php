<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\Registry;
use StoreCore\CRM\{Organization, Person};
use StoreCore\OML\OrderStatus;
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Order::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Proxy::class)]
#[UsesClass(\StoreCore\ProxyFactory::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\OrganizationRepository::class)]
#[UsesClass(\StoreCore\CRM\Parties::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Database\OrderItems::class)]
#[UsesClass(\StoreCore\Database\PersonRepository::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\OML\OrderStatus::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrderTest extends TestCase
{
    #[TestDox('Order class is concrete')]
    public function testOrderClassIsConcrete(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[TestDox('Order is a model')]
    public function testOrderIsModel(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
    }

    #[Depends('testOrderIsModel')]
    #[TestDox('Order is not a database model')]
    public function testOrderIsNotDatabaseModel(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertFalse($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[TestDox('Order is countable')]
    public function testOrderIsCountable(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }

    #[Group('seo')]
    #[TestDox('Order is JSON serializable')]
    public function testOrderIsJsonSerializable(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }

    #[TestDox('Order implements visitor design pattern VisitableInterface')]
    public function testOrderImplementsVisitorDesignPatternVisitableInterface(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\VisitableInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Order::VERSION);
        $this->assertIsString(Order::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Order::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Order.isGift exists')]
    public function testOrderIsGiftExists(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertObjectHasProperty('isGift', $order);
    }

    #[Depends('testOrderIsGiftExists')]
    #[TestDox('Order.isGift is (bool) false by default')]
    public function testOrderIsGiftIsBoolFalseByDefault(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertIsBool($order->isGift);
        $this->assertFalse($order->isGift);

        $order->isGift = true;
        $this->assertTrue($order->isGift);
    }

    #[Depends('testOrderIsGiftIsBoolFalseByDefault')]
    #[TestDox('Order.isGift is included in JSON-LD only if true')]
    public function testOrderIsGiftIsIncludedInJsonLdOnlyIfTrue(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $json = json_encode($order, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($json);
        $this->assertStringNotContainsString('"isGift": false', $json);
        $this->assertStringNotContainsString('isGift', $json);

        $order->isGift = true;
        $json = json_encode($order, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($json);
        $this->assertStringContainsString('"isGift": true', $json);
    }


    #[TestDox('Order.orderNumber exists')]
    public function testOrderOrderNumberExists(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertObjectHasProperty('orderNumber', $order);
    }

    #[Depends('testOrderOrderNumberExists')]
    #[TestDox('Order.orderNumber is not read-only')]
    public function testOrderOrderNumberIsNotReadOnly(): void
    {
        $property = new ReflectionProperty(Order::class, 'orderNumber');
        $this->assertFalse($property->isPublic());
        $this->assertTrue($property->isProtected());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrderOrderNumberExists')]
    #[TestDox('Order.orderNumber is null by default')]
    public function testOrderOrderNumberIsNullByDefault(): void
    {
        $property = new ReflectionProperty(Order::class, 'orderNumber');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertNull($property->getDefaultValue());

        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new Order($registry);
        $this->assertNull($order->orderNumber);
    }

    #[Depends('testOrderOrderNumberIsNotReadOnly')]
    #[Depends('testOrderOrderNumberIsNullByDefault')]
    #[TestDox('Order.orderNumber accepts integer')]
    public function testOrderOrderNumberAcceptsInteger(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = (int) 176057;
        $this->assertNotNull($order->orderNumber);
        $this->assertIsInt($order->orderNumber);
        $this->assertEquals(176057, $order->orderNumber);
    }

    #[Depends('testOrderOrderNumberAcceptsInteger')]
    #[TestDox('Order.orderNumber accepts unsigned int')]
    public function testOrderOrderNumberAcceptsUnsignedInt(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = 4294967295;
        $this->assertIsInt($order->orderNumber);
        $this->assertEquals(4294967295, $order->orderNumber);
    }

    #[Depends('testOrderOrderNumberAcceptsInteger')]
    #[TestDox('Order.orderNumber accepts numeric string as integer')]
    public function testOrderOrderNumberAcceptsNumericStringAsInteger(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = (string) '176057';
        $this->assertNotNull($order->orderNumber);
        $this->assertIsNotString($order->orderNumber);
        $this->assertIsInt($order->orderNumber);
        $this->assertEquals(176057, $order->orderNumber);
    }

    
    #[Depends('testOrderOrderNumberAcceptsInteger')]
    #[Depends('testOrderOrderNumberAcceptsUnsignedInt')]
    #[TestDox('Order.orderNumber is string (text) in Schema.org JSON-LD')]
    public function testOrderOrderNumberIsStringTextInSchemaOrgJsonLd(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = 4294967295;
        $this->assertIsInt($order->orderNumber);

        $json = json_encode($order, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"orderNumber":"4294967295"', $json);
    }

    #[Depends('testOrderOrderNumberAcceptsInteger')]
    #[Depends('testOrderOrderNumberAcceptsNumericStringAsInteger')]
    #[TestDox('Order.orderNumber leading zero is not removed')]
    public function testOrderOrderNumberLeadingZeroIsNotRemoved(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = (string) '09832';
        $this->assertIsNotInt($order->orderNumber);
        $this->assertIsString($order->orderNumber);
        $this->assertEquals('09832', $order->orderNumber);
    }

    #[Depends('testOrderOrderNumberAcceptsNumericStringAsInteger')]
    #[TestDox('Order.orderNumber accepts float as string')]
    public function testOrderOrderNumberAcceptsFloatAsString(): void
    {
        // Order numbers with a year prefix will look like a float:
        $order_number = 2023.0987;
        $this->assertIsFloat($order_number);

        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = $order_number;

        $this->assertIsNotFloat($order->orderNumber);
        $this->assertIsString($order->orderNumber);
        $this->assertEquals('2023.0987', $order->orderNumber);
    }

    #[Depends('testOrderOrderNumberAcceptsNumericStringAsInteger')]
    #[TestDox('Order.orderNumber accepts Amazon order ID')]
    public function testOrderOrderNumberAcceptsAmazonOrderID(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = '026-1520163-6049104';
        $this->assertEquals('026-1520163-6049104', $order->orderNumber);
        $this->assertStringContainsString('"orderNumber":"026-1520163-6049104"', json_encode($order, \JSON_UNESCAPED_SLASHES));
    }

    /**
     * @see https://api.bol.com/retailer/public/redoc/v9/retailer.html#tag/Orders/operation/get-order
     * @see https://api.bol.com/retailer/public/redoc/v9/retailer.html#tag/Shipments/operation/get-shipment
     */
    #[Depends('testOrderOrderNumberAcceptsNumericStringAsInteger')]
    #[TestDox('Order.orderNumber accepts Bol order ID')]
    public function testOrderOrderNumberAcceptsBolOrderID(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);

        $order->orderNumber = 'A2K8290LP8';
        $this->assertIsString($order->orderNumber);
        $this->assertEquals('A2K8290LP8', $order->orderNumber);
        $this->assertStringContainsString('"orderNumber":"A2K8290LP8"', json_encode($order, \JSON_UNESCAPED_SLASHES));

        $order->orderNumber = '4123456789';
        $this->assertIsInt($order->orderNumber);
        $this->assertEquals(4123456789, $order->orderNumber);
        $this->assertStringContainsString('"orderNumber":"4123456789"', json_encode($order, \JSON_UNESCAPED_SLASHES));
    }


    /**
     * @see https://schema.org/orderStatus
     */
    #[TestDox('Order.orderStatus exists')]
    public function testOrderOrderStatusExists(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertObjectHasProperty('orderStatus', $order);
    }

    #[Depends('testOrderOrderStatusExists')]
    #[TestDox('Order.orderStatus is null by default')]
    public function testOrderOrderStatusIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertNull($order->orderStatus);
    }

    #[Depends('testOrderOrderStatusExists')]
    #[TestDox('Order.orderStatus accepts OrderStatus enumerarion member')]
    public function testOrderOrderStatusAcceptsOrderStatusEnumerarionMember(): void
    {
        $this->assertTrue(enum_exists(OrderStatus::class));

        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderStatus = OrderStatus::OrderProcessing;

        $this->assertNotNull($order->orderStatus);
        $this->assertEquals('OrderProcessing', $order->orderStatus->name);
        $this->assertEquals('https://schema.org/OrderProcessing', $order->orderStatus->value);

        $order->orderStatus = OrderStatus::OrderCancelled;
        $this->assertNotEquals('https://schema.org/OrderProcessing', $order->orderStatus->value);
        $this->assertEquals('https://schema.org/OrderCancelled', $order->orderStatus->value);
    }


    #[TestDox('Order::accept exists')]
    public function testOrderAcceptExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('accept'));
    }

    #[Depends('testOrderAcceptExists')]
    #[TestDox('Order::accept is public')]
    public function testOrderAcceptIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'accept');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderAcceptExists')]
    #[TestDox('Order::accept has one REQUIRED parameter')]
    public function testOrderAcceptHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'accept');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderAcceptExists')]
    #[Depends('testOrderAcceptIsPublic')]
    #[Depends('testOrderAcceptHasNoParameters')]
    #[TestDox('Order::accept requires visitor design pattern VisitorInterface')]
    public function testOrderAcceptRequiresVisitorDesignPatternVisitor(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);

        $this->assertTrue(interface_exists('\\StoreCore\\VisitorInterface'));

        $visitor = new class implements \StoreCore\VisitorInterface {
            public function visitOrder(Order $order): void {}
        };
        $order->accept($visitor);

        $visitor = new \stdClass();
        $this->expectException(\TypeError::class);
        $failure = $order->accept($visitor);
    }

    #[Depends('testOrderAcceptRequiresVisitorDesignPatternVisitor')]
    #[TestDox('Order::accept implements visitor design pattern VisitableInterface')]
    public function testOrderAcceptImplementsVisitorDesignPatternVisitableInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\VisitableInterface'));

        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertInstanceOf(VisitableInterface::class, $order);
    }


    #[TestDox('Order::count exists')]
    public function testOrderCountExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testOrderCountExists')]
    #[TestDox('Order::count is public')]
    public function testOrderCountIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderCountExists')]
    #[TestDox('Order::count has no parameters')]
    public function testOrderCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderIsCountable')]
    #[Depends('testOrderCountExists')]
    #[Depends('testOrderCountIsPublic')]
    #[Depends('testOrderCountHasNoParameters')]
    #[TestDox('Order::count returns (int) 0 on new order without items')]
    public function testOrderCountReturnsIntZeroOnNewOrderWithoutItems(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new Order($registry);
        $this->assertIsInt($order->count());
        $this->assertEquals(0, $order->count());
        $this->assertEquals(0, count($order));
        $this->assertCount(0, $order);

        $order->setIdentifier('2d309c8b-a83f-464c-8559-8eccc073b288');
        $this->assertTrue($order->hasIdentifier());
        $this->assertEquals(0, $order->count());
    }


    #[TestDox('Order::getCustomer exists')]
    public function testOrderGetCustomerExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('getCustomer'));
    }

    #[Depends('testOrderGetCustomerExists')]
    #[TestDox('Order::getCustomer is public')]
    public function testOrderGetCustomerIsPrivate(): void
    {
        $method = new ReflectionMethod(Order::class, 'getCustomer');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderGetCustomerExists')]
    #[TestDox('Order::getCustomer has no parameters')]
    public function testOrderGetCustomerHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'getCustomer');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderGetCustomerExists')]
    #[Depends('testOrderGetCustomerIsPrivate')]
    #[Depends('testOrderGetCustomerHasNoParameters')]
    #[TestDox('Order::$customer is null by default')]
    public function testOrderCustomerIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertNull($order->customer);
    }


    #[TestDox('Order::getCustomerIdentifier exists')]
    public function testOrderGetCustomerIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('getCustomerIdentifier'));
    }

    #[Depends('testOrderGetCustomerIdentifierExists')]
    #[TestDox('Order::getCustomerIdentifier is public')]
    public function testOrderGetCustomerIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'getCustomerIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderGetCustomerIdentifierExists')]
    #[TestDox('Order::getCustomerIdentifier has no parameters')]
    public function testOrderGetCustomerIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'getCustomerIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('Order::getIdentifier exists')]
    public function testOrderGetIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[Depends('testOrderGetIdentifierExists')]
    #[TestDox('Order::getIdentifier is public')]
    public function testOrderGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'getIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderGetIdentifierExists')]
    #[TestDox('Order::getIdentifier has no parameters')]
    public function testOrderGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderGetIdentifierExists')]
    #[TestDox('Order::getIdentifier returns null by default')]
    public function testOrderGetIdentifierReturnsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new Order($registry);
        $this->assertNull($order->getIdentifier());
        $this->assertFalse($order->hasIdentifier());
    }


    #[TestDox('Order::getSellerIdentifier exists')]
    public function testOrderGetSellerIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('getSellerIdentifier'));
    }

    #[Depends('testOrderGetSellerIdentifierExists')]
    #[TestDox('Order::getSellerIdentifier is public')]
    public function testOrderGetSellerIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'getSellerIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderGetSellerIdentifierExists')]
    #[TestDox('Order::getSellerIdentifier has no parameters')]
    public function testOrderGetSellerIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'getSellerIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderGetSellerIdentifierExists')]
    #[Depends('testOrderGetSellerIdentifierIsPublic')]
    #[Depends('testOrderGetSellerIdentifierHasNoParameters')]
    #[TestDox('Order::getSellerIdentifier returns null by default')]
    public function testOrderGetSellerIdentifierReturnsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new Order($registry);
        $this->assertNull($order->getSellerIdentifier());
    }


    #[TestDox('Order::hasIdentifier exists')]
    public function testOrderHasIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('hasIdentifier'));
    }

    #[Depends('testOrderHasIdentifierExists')]
    #[TestDox('Order::hasIdentifier is public')]
    public function testOrderHasIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'hasIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderHasIdentifierExists')]
    #[TestDox('Order::hasIdentifier has no parameters')]
    public function testOrderHasIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'hasIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('Order::setIdentifier exists')]
    public function testOrderSetIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('setIdentifier'));
    }

    #[Depends('testOrderSetIdentifierExists')]
    #[TestDox('Order::setIdentifier is public')]
    public function testOrderSetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'setIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderSetIdentifierExists')]
    #[TestDox('Order::setIdentifier has one REQUIRED parameter')]
    public function testOrderSetIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Order::class, 'setIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderSetIdentifierHasOneRequiredParameter')]
    #[TestDox('Order::setIdentifier sets order identifier as UUID value object')]
    public function testOrderSetIdentifierSetsOrderIdentifierAsUUIDValueObject(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertFalse($order->hasIdentifier());

        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();
        $order->setIdentifier($uuid);
        $this->assertTrue($order->hasIdentifier());
        $this->assertIsObject($order->getIdentifier());
        $this->assertSame($uuid, $order->getIdentifier());
    }

    #[Depends('testOrderSetIdentifierHasOneRequiredParameter')]
    #[Depends('testOrderSetIdentifierSetsOrderIdentifierAsUUIDValueObject')]
    #[Depends('testOrderIsJsonSerializable')]
    #[TestDox('Order::setIdentifier accepts order UUID as string')]
    public function testOrderSetIdentifierAcceptsOrderUUIDAsString(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertFalse($order->hasIdentifier());

        $order->setIdentifier('ae75100b-4ca7-423e-bc75-e6b897a4cc4a');
        $this->assertTrue($order->hasIdentifier());
        $this->assertEquals('ae75100b-4ca7-423e-bc75-e6b897a4cc4a', $order->getIdentifier()->__toString());

        $expectedJson = <<<'JSON'
            {
              "@id": "/api/v1/orders/ae75100b-4ca7-423e-bc75-e6b897a4cc4a",
              "@context": "https://schema.org",
              "@type": "Order",
              "identifier": "ae75100b-4ca7-423e-bc75-e6b897a4cc4a"
            }
        JSON;

        $actualJson = json_encode($order, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Order::setSellerIdentifier exists')]
    public function testOrderSetSellerIdentifierExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('setSellerIdentifier'));
    }

    #[Depends('testOrderSetSellerIdentifierExists')]
    #[TestDox('Order::setSellerIdentifier is public')]
    public function testOrderSetSellerIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'setSellerIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderSetSellerIdentifierExists')]
    #[TestDox('Order::setSellerIdentifier has one REQUIRED parameter')]
    public function testOrderSetSellerIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Order::class, 'setSellerIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderSetSellerIdentifierHasOneRequiredParameter')]
    #[TestDox('Order::setSellerIdentifier sets seller identifier as UUID value object')]
    public function testOrderSetSellerIdentifierSetsSellerIdentifierAsUUIDValueObject(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $order = new Order($registry);
        $this->assertNull($order->getSellerIdentifier());

        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();
        $this->assertIsObject($uuid);

        $order->setSellerIdentifier($uuid);
        $this->assertNotNull($order->getSellerIdentifier());
        $this->assertIsObject($order->getSellerIdentifier());
        $this->assertEquals($uuid, $order->getSellerIdentifier());
    }

    #[Depends('testOrderSetSellerIdentifierSetsSellerIdentifierAsUUIDValueObject')]
    #[TestDox('Order::setSellerIdentifier accepts seller identifier as UUID string')]
    public function testOrderSetSellerIdentifierAcceptsSellerIdentifierAsUUIDString(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertNull($order->getSellerIdentifier());
        $this->assertNull($order->getSeller());

        $order->setSellerIdentifier('7dad9ce8-6e8b-479f-ba61-5e0b94c8f483');
        $this->assertIsObject($order->getSellerIdentifier());
        $this->assertEquals('7dad9ce8-6e8b-479f-ba61-5e0b94c8f483', (string) $order->getSellerIdentifier());

        // The seller MUST now be an identifiable entity.
        $this->assertIsObject($order->getSeller());
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $order->getSeller());
        $this->assertTrue($order->getSeller()->hasIdentifier());

        $this->assertEquals(
            '7dad9ce8-6e8b-479f-ba61-5e0b94c8f483',
            $order->getSeller()->getIdentifier()->__toString()
        );

        $this->assertEquals(
            '7dad9ce8-6e8b-479f-ba61-5e0b94c8f483',
            $order->seller->identifier
        );
    }


    #[TestDox('Order::setCustomer exists')]
    public function testOrderSetCustomerExists(): void
    {
        $class = new ReflectionClass(Order::class);
        $this->assertTrue($class->hasMethod('setCustomer'));
    }

    #[Depends('testOrderSetCustomerExists')]
    #[TestDox('Order::setCustomer is public')]
    public function testOrderSetCustomerIsPublic(): void
    {
        $method = new ReflectionMethod(Order::class, 'setCustomer');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderSetCustomerExists')]
    #[TestDox('Order::setCustomer has one REQUIRED parameter')]
    public function testOrderSetCustomerHasNoParameters(): void
    {
        $method = new ReflectionMethod(Order::class, 'setCustomer');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderSetCustomerExists')]
    #[Depends('testOrderSetCustomerIsPublic')]
    #[Depends('testOrderSetCustomerHasNoParameters')]
    #[TestDox('Order::setCustomer accepts Person')]
    public function testOrderSetCustomerAcceptsPerson(): void
    {
        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertNull($order->customer);
        $this->assertNull($order->getCustomer());
        $this->assertNull($order->getCustomerIdentifier());

        /* @see https://developers.google.com/gmail/markup/reference/order#order_with_billing_details
         *      “Order with billing details”, Google Workspace Gmail reference
         */
        $customer = new Person();
        $customer->name = 'John Smith';
        $customer->setIdentifier('0fb3d9e7-cc62-4aa1-b17e-e74580b576c3');

        $order->setCustomer($customer);
        $this->assertNotNull($order->customer);
        $this->assertNotNull($order->getCustomer());
        $this->assertNotNull($order->getCustomerIdentifier());

        $this->assertEquals(
            '0fb3d9e7-cc62-4aa1-b17e-e74580b576c3',
            $order->getCustomer()->getIdentifier()->__toString()
        );

        $this->assertEquals(
            'John Smith',
            $order->customer->name
        );
    }
}
