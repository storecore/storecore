<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
#[Group('hmvc')]
final class VisitorInterfaceTest extends TestCase
{
    #[TestDox('VisitorInterface interface file exists')]
    public function testVisitorInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'VisitorInterface.php');
    }

    #[Depends('testVisitorInterfaceInterfaceFileExists')]
    #[TestDox('VisitorInterface interface file is readable')]
    public function testVisitorInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'VisitorInterface.php');
    }

    #[TestDox('VisitorInterface is an interface')]
    public function testVisitorInterfaceIsInterface(): void
    {
        $interface = new ReflectionClass(VisitorInterface::class);
        $this->assertTrue($interface->isInterface());
    }
}
