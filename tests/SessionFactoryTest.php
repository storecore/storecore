<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \isset;
use function \random_int;
use function \version_compare;

#[CoversClass(\StoreCore\Session::class)]
#[CoversClass(\StoreCore\SessionFactory::class)]
#[CoversClass(\StoreCore\SubjectObservers::class)]
#[CoversClass(\StoreCore\Database\Observers::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
final class SessionFactoryTest extends TestCase
{
    protected function setUp(): void
    {
        if (!isset($_SESSION)) {
            $_SESSION = array();
        }
    }

    #[Group('hmvc')]
    #[TestDox('SessionFactory class is concrete')]
    public function testSessionFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(SessionFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SessionFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SessionFactory::VERSION);
        $this->assertIsString(SessionFactory::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SessionFactory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('SessionFactory::__construct exists')]
    public function testSessionFactoryConstructorExists(): void
    {
        $class = new ReflectionClass(SessionFactory::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('SessionFactory::__construct is public constructor')]
    public function testSessionFactoryConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('SessionFactory::__construct has one REQUIRED parameter')]
    public function testSessionFactoryConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('SessionFactory::createSession exists')]
    public function testSessionFactoryCreateSessionExists(): void
    {
        $class = new ReflectionClass(SessionFactory::class);
        $this->assertTrue($class->hasMethod('createSession'));
    }

    #[TestDox('SessionFactory::createSession is public')]
    public function testSessionFactoryCreateSessionIsPublic(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, 'createSession');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('SessionFactory::createSession has no parameters')]
    public function testSessionFactoryCreateSessionHasNoParameters(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, 'createSession');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('SessionFactory::createSession returns Session object')]
    public function testSessionFactoryCreateSessionReturnsSessionObject(): void
    {
        $registry = Registry::getInstance();
        $factory = new SessionFactory($registry);
        $session = $factory->createSession();
        $this->assertIsObject($session);
        $this->assertInstanceOf(\StoreCore\Session::class, $session);
    }

    #[TestDox('SessionFactory::createSession returns existing Session')]
    public function testSessionFactoryCreateSessionReturnsExistingSession(): void
    {
        $registry = Registry::getInstance();
        $factory = new SessionFactory($registry);
        $session = $factory->createSession();
        $registry->set('Session', $session);

        $var = random_int(0, 2147483647);
        $session->set('SharedValue', $var);

        unset($session, $factory);

        $factory = new SessionFactory($registry);
        $session = $factory->createSession();
        $this->assertTrue($session->has('SharedValue'));
        $this->assertSame($var, $session->get('SharedValue'));
    }


    #[TestDox('SessionFactory::setSessionTimeout exists')]
    public function testSessionFactorySetSessionTimeoutExists(): void
    {
        $class = new ReflectionClass(SessionFactory::class);
        $this->assertTrue($class->hasMethod('setSessionTimeout'));
    }

    #[TestDox('SessionFactory::setSessionTimeout is public')]
    public function testSessionFactorySetSessionTimeoutIsPublic(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, 'setSessionTimeout');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('SessionFactory::setSessionTimeout has one REQUIRED parameter')]
    public function testSessionFactorySetSessionTimeoutHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(SessionFactory::class, 'setSessionTimeout');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('SessionFactory::setSessionTimeout throws value error on timeout less than 1 (one) minute')]
    public function testSessionFactorySetSessionTimeoutThrowsValueErrorOnTimeoutLessThanOneMinute(): void
    {
        $registry = Registry::getInstance();
        $factory = new SessionFactory($registry);

        $this->expectException(\ValueError::class);
        $failure = $factory->setSessionTimeout(0);
    }
}
