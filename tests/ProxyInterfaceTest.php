<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class ProxyInterfaceTest extends TestCase
{
    #[TestDox('ProxyInterface interface exists')]
    public function testProxyInterfaceInterfaceExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'ProxyInterface.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'ProxyInterface.php');

        $this->assertTrue(interface_exists('\\StoreCore\\ProxyInterface'));
        $this->assertTrue(interface_exists(\StoreCore\ProxyInterface::class));
        $this->assertTrue(interface_exists(ProxyInterface::class));
    }


    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ProxyInterface::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ProxyInterface::VERSION);
        $this->assertIsString(ProxyInterface::VERSION);
    }

    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ProxyInterface::VERSION, '1.0.0', '>=')
        );
    }
}
