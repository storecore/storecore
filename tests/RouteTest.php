<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Route::class)]
#[CoversClass(\StoreCore\Database\RouteRepository::class)]
#[Group('hmvc')]
final class RouteTest extends TestCase
{
    #[TestDox('Route class is concrete')]
    public function testRouteClassIsConcrete(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Route is read-only')]
    public function testRouteIsReadOnly(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Route::VERSION);
        $this->assertIsString(Route::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Route::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Route::__construct exists')]
    public function testRouteConstructorExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Route::__construct is public constructor')]
    public function testRouteConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Route::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Route::__construct has four parameters')]
    public function testRouteConstructorHasFourParameters(): void
    {
        $method = new ReflectionMethod(Route::class, '__construct');
        $this->assertEquals(4, $method->getNumberOfParameters());
    }

    #[Depends('testRouteConstructorHasFourParameters')]
    #[TestDox('Route::__construct has two required parameters')]
    public function testRouteConstructorHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Route::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Route::dispatch exists')]
    public function testRouteDispatchExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasMethod('dispatch'));
    }

    #[TestDox('Route::dispatch is public')]
    public function testRouteDispatchIsPublic(): void
    {
        $method = new ReflectionMethod(Route::class, 'dispatch');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Route::dispatch has no parameters')]
    public function testRouteDispatchHasNoParameters(): void
    {
        $method = new ReflectionMethod(Route::class, 'dispatch');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('Route::$controller exists')]
    public function testRouteControllerExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertFalse($class->hasMethod('getController'));
        $this->assertTrue($class->hasProperty('controller'));
    }

    #[TestDox('Route::$controller is non-empty string')]
    public function testRouteControllerIsNonEmptyString(): void
    {
        $route = new Route('/foo/bar/', 'StoreCore\Foo\Bar');
        $this->assertNotEmpty($route->controller);
        $this->assertIsString($route->controller);
    }

    #[TestDox('Route::$controller is set controller')]
    public function testRouteGetControllerReturnsSetController(): void
    {
        $route = new Route('/foo/bar/', 'StoreCore\Foo\Bar');
        $this->assertEquals('StoreCore\Foo\Bar', $route->controller);
    }


    #[TestDox('Route::$method exists')]
    public function testRouteMethodExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertFalse($class->hasMethod('getMethod'));
        $this->assertTrue($class->hasProperty('method'));
    }

    #[TestDox('Route::$method is public read-only')]
    public function testRouteMethodIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Route::class, 'method');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[TestDox('Route::$method is set method')]
    public function testRouteMethodIsSetMethod(): void
    {
        $route = new Route('/foo/bar/bazQux', '\StoreCore\Foo\Bar', 'bazQux');
        $this->assertEquals('bazQux', $route->method);
    }

    #[TestDox('Route::$method is null if method was not set')]
    public function testRouteMethodIsNullIfMethodWasNotSet(): void
    {
        $route = new Route('/foo/bar/bazQux', '\StoreCore\Foo\Bar');
        $this->assertNull($route->method);
    }


    #[TestDox('Route::$parameter exists')]
    public function testRouteParameterExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasProperty('parameter'));
    }

    #[Depends('testRouteParameterExists')]
    #[TestDox('Route::$parameter is public read-only')]
    public function testRouteParameterIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Route::class, 'parameter');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testRouteParameterIsPublicReadOnly')]
    #[TestDox('Route::$parameter is set parameter')]
    public function testRouteParameterIsSetParameter(): void
    {
        $params = array(
            'foo' => random_int(1, 65535),
            'bar' => random_bytes(8),
        );

        $route = new Route('/foo/bar/', '\StoreCore\Foo\Bar', 'setFooBar', $params);
        $this->assertEquals($params, $route->parameter);
    }


    #[TestDox('Route::$path exists')]
    public function testRoutePathExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertFalse($class->hasMethod('getPath'));
        $this->assertTrue($class->hasProperty('path'));
    }

    #[Depends('testRoutePathExists')]
    #[TestDox('Route::$path is public read-only')]
    public function testRoutePathIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Route::class, 'path');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testRoutePathIsPublicReadOnly')]
    #[TestDox('Route::$path is non-empty string')]
    public function testRoutePathIsNonEmptyString(): void
    {
        $route = new Route('/foo/bar/', '\StoreCore\FooBar');
        $this->assertNotEmpty($route->path);
        $this->assertIsString($route->path);
    }

    #[Depends('testRoutePathIsNonEmptyString')]
    #[TestDox('Route::$path is set path')]
    public function testRoutePathIsSetPath(): void
    {
        $route = new Route('/foo/bar', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);
    }

    #[Depends('testRoutePathIsNonEmptyString')]
    #[TestDox('Route::$path is lowercase path')]
    public function testRoutePathIsLowercasePath(): void
    {
        $route = new Route('/Foo/Bar', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);
    }

    #[Depends('testRoutePathIsLowercasePath')]
    #[TestDox('Route::$path has leading forward slash')]
    public function testRoutePathHasLeadingForwardSlash(): void
    {
        $route = new Route('Foo/Bar/', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);

        $route = new Route('Foo/bar', '\StoreCore\Foo', 'bar');
        $this->assertEquals('/foo/bar', $route->path);
    }

    #[Depends('testRoutePathIsLowercasePath')]
    #[TestDox('Route::$path has no trailing forward slash')]
    public function testRoutePathHasNoTrailingForwardSlash(): void
    {
        $route = new Route('Foo/Bar/', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);

        $route = new Route('/Foo/Bar/', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);

        $route = new Route('/Foo/Bar', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);

        $route = new Route('Foo/Bar', '\StoreCore\Foo\Bar');
        $this->assertEquals('/foo/bar', $route->path);
    }


    #[TestDox('Route::setController exists')]
    public function testRouteSetControllerExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasMethod('setController'));
    }

    #[TestDox('Route::setController is not public')]
    public function testRouteSetControllerIsNotPublic(): void
    {
        $method = new ReflectionMethod(Route::class, 'setController');
        $this->assertFalse($method->isPublic());
    }

    #[TestDox('Route::setController has one REQUIRED parameter')]
    public function testRouteSetControllerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Route::class, 'setController');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Route::setController ignores leading backslash')]
    public function testRouteSetControllerIgnoresLeadingBackslash(): void
    {
        $route = new Route('/api/v1', '\StoreCore\API\RequestHandler');
        $this->assertEquals('StoreCore\API\RequestHandler', $route->controller);
    }

    #[TestDox('Route::setController ignores trailing backslash')]
    public function testRouteSetControllerIgnoresTrailingBackslash(): void
    {
        $route = new Route('/api/v1', 'StoreCore\API\RequestHandler\\');
        $this->assertEquals('StoreCore\API\RequestHandler', $route->controller);
    }


    #[TestDox('Route::setPath exists')]
    public function testRouteSetPathExists(): void
    {
        $class = new ReflectionClass(Route::class);
        $this->assertTrue($class->hasMethod('setPath'));
    }

    #[Depends('testRouteSetPathExists')]
    #[TestDox('Route::setPath is not public')]
    public function testRouteSetPathIsPublic(): void
    {
        $method = new ReflectionMethod(Route::class, 'setPath');
        $this->assertFalse($method->isPublic());
    }

    #[Depends('testRouteSetPathExists')]
    #[TestDox('Route::setPath has one REQUIRED parameter')]
    public function testRouteSetPathHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Route::class, 'setPath');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
