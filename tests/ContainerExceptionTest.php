<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversClass(\StoreCore\ContainerException::class)]
final class ContainerExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ContainerException class exists')]
    public function testContainerExceptionClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'ContainerException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'ContainerException.php');

        $this->assertTrue(class_exists('\\StoreCore\\ContainerException'));
        $this->assertTrue(class_exists(\StoreCore\ContainerException::class));
    }


    #[Group('hmvc')]
    #[TestDox('ContainerException class is concrete')]
    public function testContainerExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ContainerException is throwable exception')]
    public function testContainerExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));

        $this->expectException(\Exception::class);
        throw new ContainerException();
    }

    #[Depends('testContainerExceptionIsThrowableException')]
    #[Group('hmvc')]
    #[TestDox('ContainerException is a runtime exception')]
    public function testContainerExceptionIsRuntimeException(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertTrue($class->isSubclassOf(\Exception::class));
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));

        $this->expectException(\RuntimeException::class);
        throw new ContainerException();
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerExceptionInterface exists')]
    public function testImplementedPSR11ContainerExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerExceptionInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ContainerException implements PSR-11 ContainerExceptionInterface')]
    public function testContainerExceptionImplementsPsr11ContainerExceptionInterface(): void
    {
        $class = new ReflectionClass(ContainerException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerExceptionInterface::class));

        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        throw new ContainerException();
    }
}
