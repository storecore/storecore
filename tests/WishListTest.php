<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\WishList::class)]
#[UsesClass(\StoreCore\Order::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\ShoppingList::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class WishListTest extends TestCase
{
    private WishList $wishList;

    public function setUp(): void
    {
        $registry = Registry::getInstance();
        $this->wishList = new WishList($registry);
    }

    #[Group('hmvc')]
    #[TestDox('WishList class is concrete')]
    public function testWishListClassIsConcrete(): void
    {
        $class = new ReflectionClass(WishList::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('WishList is a ShoppingList')]
    public function testWishListIsShoppingList(): void
    {
        $this->assertInstanceOf(ShoppingList::class, $this->wishList);
    }

    #[Group('hmvc')]
    #[TestDox('WishList is an Order')]
    public function testWishListIsOrder(): void
    {
        $this->assertInstanceOf(Order::class, $this->wishList);
    }

    #[Group('hmvc')]
    #[TestDox('WishList implements StoreCore IdentityInterface')]
    public function testWishListImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $this->wishList);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WishList::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WishList::VERSION);
        $this->assertIsString(WishList::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WishList::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
