<?php

declare(strict_types=1);

namespace StoreCore;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;

#[CoversNothing]
#[Group('distro')]
final class LanguageInterfaceTest extends TestCase
{
    #[TestDox('LanguageInterface interface file exists')]
    public function testLanguageInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'LanguageInterface.php');
    }

    #[Depends('testLanguageInterfaceInterfaceFileExists')]
        #[TestDox('LanguageInterface interface file is readable')]
    public function testLanguageInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'LanguageInterface.php');
    }

    #[Depends('testLanguageInterfaceInterfaceFileExists')]
    #[Depends('testLanguageInterfaceInterfaceFileIsReadable')]
    #[TestDox('LanguageInterface interface exists')]
    public function testLanguageInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\LanguageInterface'));
        $this->assertTrue(interface_exists(LanguageInterface::class));
    }
}
