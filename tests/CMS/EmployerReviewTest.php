<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\EmployerReview::class)]
final class EmployerReviewTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('EmployerReview class file exists')]
    public function testEmployerReviewClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'EmployerReview.php'
        );
    }

    #[Depends('testEmployerReviewClassFileExists')]
    #[Group('distro')]
    #[TestDox('EmployerReview class file is readable')]
    public function testEmployerReviewClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'EmployerReview.php'
        );
    }

    #[Depends('testEmployerReviewClassFileExists')]
    #[Depends('testEmployerReviewClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('EmployerReview class exists')]
    public function testEmployerReviewClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\EmployerReview'));
        $this->assertTrue(class_exists(EmployerReview::class));
    }


    #[Group('hmvc')]
    #[TestDox('EmployerReview implements IdentityInterface')]
    public function testEmployerReviewImplementsIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new EmployerReview());
    }

    #[Group('hmvc')]
    #[TestDox('EmployerReview is JSON serializable')]
    public function testEmployerReviewIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new EmployerReview());
    }
}
