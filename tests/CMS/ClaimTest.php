<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\CMS\AbstractCreativeWork::class)]
final class ClaimTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Claim class exists')]
    public function testClaimClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Claim.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Claim.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Claim'));
        $this->assertTrue(class_exists(Claim::class));
    }


    #[TestDox('Claim class is concrete')]
    public function testClaimClassIsConcrete(): void
    {
        $class = new ReflectionClass(Claim::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Claim class is a CreativeWork')]
    public function testClaimIsCreativeWork(): void
    {
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, new Claim());
        $this->assertInstanceOf(AbstractCreativeWork::class, new Claim());
    }

    #[Group('hmvc')]
    #[TestDox('Claim class is JSON serializable')]
    public function testClaimIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Claim());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Claim::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Claim::VERSION);
        $this->assertIsString(Claim::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Claim::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    #[TestDox('Claim.appearance exists')]
    public function testClaimAppearanceExists(): void
    {
        $this->assertObjectHasProperty('appearance', new Claim());
    }

    #[Depends('testClaimAppearanceExists')]
    #[TestDox('Claim.appearance is null by default')]
    public function testClaimAppearanceIsNullByDefault(): void
    {
        $claim = new Claim();
        $this->assertNull($claim->appearance);
    }

    #[Depends('testClaimAppearanceExists')]
    #[TestDox('Claim.appearance accepts CreativeWork')]
    public function testClaimAppearanceAcceptsCreativeWork(): void
    {
        $claim = new Claim();
        $claim->appearance = new MedicalScholarlyArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $claim->appearance);
    }


    #[TestDox('Claim.firstAppearance exists')]
    public function testClaimFirstAppearanceExists(): void
    {
        $this->assertObjectHasProperty('firstAppearance', new Claim());
    }

    #[Depends('testClaimFirstAppearanceExists')]
    #[TestDox('Claim.firstAppearance is null by default')]
    public function testClaimFirstAppearanceIsNullByDefault(): void
    {
        $claim = new Claim();
        $this->assertNull($claim->firstAppearance);
    }

    #[Depends('testClaimFirstAppearanceExists')]
    #[TestDox('Claim.firstAppearance accepts CreativeWork')]
    public function testClaimFirstAppearanceAcceptsCreativeWork(): void
    {
        $claim = new Claim();
        $claim->firstAppearance = new MedicalScholarlyArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $claim->firstAppearance);
    }
}
