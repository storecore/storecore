<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(WebPage::class)]
final class WebPageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('WebPage class exists')]
    public function testWebPageClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'WebPage.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'WebPage.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\WebPage'));
        $this->assertTrue(class_exists(WebPage::class));
    }

    #[TestDox('WebPage class is concrete')]
    public function testWebPageClassIsConcrete(): void
    {
        $class = new ReflectionClass(WebPage::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('WebPage is a CreativeWork Thing')]
    public function testWebPageIsCreativeWorkThing(): void
    {
        $webpage = new WebPage();
        $this->assertInstanceOf(AbstractCreativeWork::class, $webpage);
        $this->assertInstanceOf(AbstractThing::class, $webpage);
    }

    #[Group('hmvc')]
    #[TestDox('WebPage is JSON serializable')]
    public function testWebPageIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new WebPage());
    }

    /**
     * @see https://schema.org/WebPage#subtypes
     */
    #[Group('hmvc')]
    #[TestDox('WebPage subtypes')]
    public function testWebPageSubtypes(): void
    {
        $subtypes = [
            'AboutPage',
            'CheckoutPage',
            'CollectionPage',
            'ContactPage',
            'FAQPage',
            'ImageGallery',
            'ItemPage',
            'MediaGallery',
            'MedicalWebPage',
            'ProfilePage',
            'QAPage',
            'RealEstateListing',
            'SearchResultsPage',
            'VideoGallery',
        ];
        $this->assertCount(14, $subtypes);

        foreach ($subtypes as $type) {
            $this->assertTrue(class_exists('\\StoreCore\\CMS\\' . $type));
        }
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WebPage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WebPage::VERSION);
        $this->assertIsString(WebPage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WebPage::VERSION, '0.2.0', '>=')
        );
    }
}
