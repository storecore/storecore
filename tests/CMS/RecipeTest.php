<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Duration;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\Recipe::class)]
#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[CoversClass(\StoreCore\CMS\HowTo::class)]
#[CoversClass(\StoreCore\Types\Duration::class)]
final class RecipeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Recipe class exists')]
    public function testRecipeClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Recipe.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Recipe.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Recipe'));
        $this->assertTrue(class_exists(Recipe::class));
    }

    #[TestDox('Recipe class is concrete')]
    public function testRecipeClassIsConcrete(): void
    {
        $class = new ReflectionClass(Recipe::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Recipe is a HowTo')]
    public function testRecipeIsHowTo(): void
    {
        $this->assertInstanceOf(HowTo::class, new Recipe());
    }

    #[Group('hmvc')]
    #[TestDox('Recipe is a CreativeWork Thing')]
    public function testRecipeIsCreativeWorkThing(): void
    {
        $recipe = new Recipe();
        $this->assertInstanceOf(AbstractCreativeWork::class, $recipe);
        $this->assertInstanceOf(AbstractThing::class, $recipe);
    }

    #[Group('hmvc')]
    #[TestDox('Recipe is JSON serializable')]
    public function testRecipeIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Recipe());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Recipe::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Recipe::VERSION);
        $this->assertIsString(Recipe::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Recipe::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('Recipe.cookTime exists')]
    public function testRecipeCookTimeExists(): void
    {
        $this->assertObjectHasProperty('cookTime', new Recipe());
    }

    #[Depends('testRecipeCookTimeExists')]
    #[TestDox('Recipe.cookTime is public')]
    public function testRecipeCookTimeIsPublic(): void
    {
        $property = new ReflectionProperty(Recipe::class, 'cookTime');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testRecipeCookTimeExists')]
    #[TestDox('Recipe.cookTime is null by default')]
    public function testRecipeCookTimeIsNullByDefault(): void
    {
        $recipe = new Recipe();
        $this->assertNull($recipe->cookTime);
    }

    #[Depends('testRecipeCookTimeExists')]
    #[TestDox('Recipe.cookTime accepts Duration')]
    public function testRecipeCookTimeAcceptsDuration(): void
    {
        $duration = new Duration('PT1H');
        $this->assertInstanceOf(\DateInterval::class, $duration);

        $recipe = new Recipe("Mom's World Famous Banana Bread");
        $recipe->cookTime = $duration;
        $this->assertInstanceOf(\DateInterval::class, $recipe->cookTime);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Recipe",
              "cookTime": "PT1H",
              "name": "Mom's World Famous Banana Bread"
            }
        JSON;
        $actualJson = json_encode($recipe, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Recipe.prepTime exists')]
    public function testRecipePrepTimeExists(): void
    {
        $this->assertObjectHasProperty('prepTime', new Recipe());
    }

    #[Depends('testRecipePrepTimeExists')]
    #[TestDox('Recipe.prepTime is public')]
    public function testRecipePrepTimeIsPublic(): void
    {
        $property = new ReflectionProperty(Recipe::class, 'prepTime');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testRecipePrepTimeExists')]
    #[TestDox('Recipe.prepTime is null by default')]
    public function testRecipePrepTimeIsNullByDefault(): void
    {
        $recipe = new Recipe();
        $this->assertNull($recipe->prepTime);
    }

    #[Depends('testRecipeCookTimeExists')]
    #[Depends('testRecipePrepTimeIsPublic')]
    #[Depends('testRecipePrepTimeIsNullByDefault')]
    #[TestDox('Recipe.prepTime accepts Duration')]
    public function testRecipePrepTimeAcceptsDuration(): void
    {
        $recipe = new Recipe('Mom’s World Famous Banana Bread');
        $recipe->prepTime = new Duration('PT15M');
        $recipe->cookTime = new Duration('PT1H');

        $this->assertInstanceOf(\DateInterval::class, $recipe->prepTime);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Recipe",
              "cookTime": "PT1H",
              "name": "Mom’s World Famous Banana Bread",
              "prepTime": "PT15M"
            }
        JSON;
        $actualJson = json_encode($recipe, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Recipe.totalTime exists')]
    public function testRecipeTotalTimeExists(): void
    {
        $this->assertObjectHasProperty('totalTime', new Recipe());
    }

    #[Depends('testRecipeTotalTimeExists')]
    #[TestDox('Recipe.totalTime is public')]
    public function testRecipeTotalTimeIsPublic(): void
    {
        $property = new ReflectionProperty(Recipe::class, 'totalTime');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testRecipeTotalTimeExists')]
    #[TestDox('Recipe.totalTime is null by default')]
    public function testRecipeTotalTimeIsNullByDefault(): void
    {
        $recipe = new Recipe();
        $this->assertNull($recipe->totalTime);
    }

    #[Depends('testRecipeTotalTimeExists')]
    #[Depends('testRecipeTotalTimeIsPublic')]
    #[Depends('testRecipeTotalTimeIsNullByDefault')]
    #[TestDox('Recipe.totalTime accepts Duration')]
    public function testRecipeTotalTimeAcceptsDuration(): void
    {
        $recipe = new Recipe('Mom’s World Famous Banana Bread');
        $recipe->prepTime = new Duration('PT15M');
        $recipe->cookTime = new Duration('PT1H');
        $recipe->totalTime = new Duration('PT1H15M');

        $this->assertInstanceOf(\DateInterval::class, $recipe->prepTime);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Recipe",
              "cookTime": "PT1H",
              "name": "Mom’s World Famous Banana Bread",
              "prepTime": "PT15M",
              "totalTime": "PT1H15M"
            }
        JSON;
        $actualJson = json_encode($recipe, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        // Set durations using strings:
        $recipe = new Recipe('Mom’s World Famous Banana Bread');
        $recipe->prepTime = Duration::createFromDateString('15 minutes');
        $recipe->cookTime = Duration::createFromDateString('1 hour');
        $recipe->totalTime = Duration::createFromDateString('1 hour, 15 minutes');

        $actualJson = json_encode($recipe, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
