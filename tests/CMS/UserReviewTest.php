<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class UserReviewTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('UserReview class exists')]
    public function testUserReviewClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'UserReview.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'UserReview.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\UserReview'));
        $this->assertTrue(class_exists(UserReview::class));
    }


    #[TestDox('UserReview class is concrete')]
    public function testUserReviewClassIsConcrete(): void
    {
        $class = new ReflectionClass(UserReview::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('UserReview is a Review CreativeWork')]
    public function testUserReviewIsReviewCreativeWork(): void
    {
        $review = new UserReview();
        $this->assertInstanceOf(\StoreCore\CMS\AbstractThing::class, $review);
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $review);
        $this->assertInstanceOf(\StoreCore\CMS\Review::class, $review);
    }

    #[Group('hmvc')]
    #[TestDox('UserReview is JSON serializable')]
    public function testUserReviewIsJsonSerializable(): void
    {
        $review = new UserReview();
        $this->assertInstanceOf(\JsonSerializable::class, $review);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UserReview::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UserReview::VERSION);
        $this->assertIsString(UserReview::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UserReview::VERSION, '1.0.0-rc.1', '>=')
        );
    }
}
