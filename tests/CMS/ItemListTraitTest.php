<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ItemListTraitTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ItemListTrait trait exists')]
    public function testItemListTraitTraitExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ItemListTrait.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ItemListTrait.php'
        );

        $this->assertTrue(trait_exists('\\StoreCore\\CMS\\ItemListTrait'));
        $this->assertTrue(trait_exists(ItemListTrait::class));
    }
}
