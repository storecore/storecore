<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;
use StoreCore\Types\URL;
use StoreCore\Types\Rating;

use \ReflectionClass;
use \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(AbstractCreativeWork::class)]
#[CoversClass(AbstractThing::class)]
#[CoversClass(ClaimReview::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Types\Rating::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\CMS\MediaObject::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
final class ClaimReviewTest extends TestCase
{
    #[TestDox('ClaimReview class is concrete')]
    public function testClaimReviewClassIsConcrete(): void
    {
        $class = new \ReflectionClass(ClaimReview::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ClaimReview is a Review CreativeWork')]
    public function testClaimReviewIsReviewCreativeWork(): void
    {
        $review = new ClaimReview();

        $this->assertTrue(class_exists(\StoreCore\CMS\AbstractCreativeWork::class));
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $review);

        $this->assertTrue(class_exists(AbstractCreativeWork::class));
        $this->assertInstanceOf(AbstractCreativeWork::class, $review);

        $this->assertTrue(class_exists(\StoreCore\CMS\Review::class));
        $this->assertInstanceOf(\StoreCore\CMS\Review::class, $review);

        $this->assertTrue(class_exists(Review::class));
        $this->assertInstanceOf(Review::class, $review);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ClaimReview::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ClaimReview::VERSION);
        $this->assertIsString(ClaimReview::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ClaimReview::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://schema.org/claimReviewed
     *      Schema.org `claimReviewed` property of a `ClaimReview`
     */
    #[TestDox('ClaimReview.claimReviewed exists')]
    public function testClaimReviewClaimReviewedExists(): void
    {
        $claimReview = new ClaimReview();
        $this->assertObjectHasProperty('claimReviewed', $claimReview);
    }

    #[Depends('testClaimReviewClaimReviewedExists')]
    #[TestDox('ClaimReview.claimReviewed is public')]
    public function testClaimReviewClaimReviewedIsPublic(): void
    {
        $property = new ReflectionProperty(ClaimReview::class, 'claimReviewed');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testClaimReviewClaimReviewedExists')]
    #[TestDox('ClaimReview.claimReviewed is null by default')]
    public function testClaimReviewClaimReviewedIsNullByDefault(): void
    {
        $review = new ClaimReview();
        $this->assertNull($review->claimReviewed);
    }

    #[Depends('testClaimReviewClaimReviewedExists')]
    #[Depends('testClaimReviewClaimReviewedIsPublic')]
    #[Depends('testClaimReviewClaimReviewedIsNullByDefault')]
    #[TestDox('ClaimReview.claimReviewed accepts string')]
    public function testClaimReviewClaimReviewedAcceptsString(): void
    {
        $review = new ClaimReview();
        $review->claimReviewed = 'The world is flat';
        $this->assertNotNull($review->claimReviewed);
        $this->assertNotEmpty($review->claimReviewed);
        $this->assertIsString($review->claimReviewed);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/factcheck#example
     *      Fact Check (`ClaimReview`) structured data example by Google
     */
    #[Group('seo')]
    #[Test]
    #[TestDox('Google fact check acceptance test')]
    public function GoogleFactCheckAcceptanceTest(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "ClaimReview",
            "url": "https://example.com/news/science/worldisflat.html",
            "claimReviewed": "The world is flat",
            "itemReviewed": {
              "@type": "Claim",
              "author": {
                "@type": "Organization",
                "name": "Square World Society",
                "sameAs": "https://example.flatworlders.com/we-know-that-the-world-is-flat"
              },
              "datePublished": "2016-06-20",
              "appearance": {
                "@type": "OpinionNewsArticle",
                "url": "https://example.com/news/a122121",
                "headline": "Square Earth - Flat earthers for the Internet age",
                "datePublished": "2016-06-22",
                "author": {
                  "@type": "Person",
                  "name": "T. Tellar"
                },
                "image": "https://example.com/photos/1x1/photo.jpg",
                "publisher": {
                  "@type": "Organization",
                  "name": "Skeptical News",
                  "logo": {
                    "@type": "ImageObject",
                    "url": "https://example.com/logo.jpg"
                  }
                }
              }
            },
            "author": {
              "@type": "Organization",
              "name": "Example.com science watch"
            },
            "reviewRating": {
              "@type": "Rating",
              "ratingValue": 1,
              "bestRating": 5,
              "worstRating": 1,
              "alternateName": "False"
            }
          }
        ';

        $factCheck = new ClaimReview();
        $factCheck->author = new Organization('Example.com science watch');
    
        // Required properties:
        $factCheck->claimReviewed = 'The world is flat';
        $factCheck->url = 'https://example.com/news/science/worldisflat.html';
        $factCheck->reviewRating = new Rating(1);
        $factCheck->reviewRating->alternateName = 'False';
    
        // Recommended properties:
        $claim = new Claim();
        $claim->author = new Organization('Square World Society');
        $claim->author->sameAs = new URL('https://example.flatworlders.com/we-know-that-the-world-is-flat');
        $claim->datePublished = '2016-06-20';
    
        // The `Claim` appeared in an `Article`:
        $article = new OpinionNewsArticle();
        $article->url = 'https://example.com/news/a122121';
        $article->headline = 'Square Earth - Flat earthers for the Internet age';
        $article->datePublished = '2016-06-22';
        $article->author = new Person('T. Tellar');
        $article->image = new URL('https://example.com/photos/1x1/photo.jpg');
        $article->publisher = new Organization('Skeptical News');
        $article->publisher->logo = new ImageObject();
        $article->publisher->logo->url = new URL('https://example.com/logo.jpg');

        // Put all three together.
        $claim->appearance = $article;
        $factCheck->itemReviewed = $claim;

        $actualJson = (string) $factCheck;
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
