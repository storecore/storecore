<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CMS\AbstractHTMLElement::class)]
#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[CoversClass(\StoreCore\CMS\HTMLImageElement::class)]
final class HTMLImageElementTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('HTMLImageElement class is concrete')]
    public function testHTMLImageElementClassIsConcrete(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('HTMLImageElement is stringable HTML element')]
    public function testHTMLImageElementIsStringableHTMLElement(): void
    {
        $image = new HTMLImageElement();
        $this->assertInstanceOf(\Stringable::class, $image);
        $this->assertInstanceOf(AbstractHTMLElement::class, $image);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(HTMLImageElement::VERSION);
        $this->assertIsString(HTMLImageElement::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(HTMLImageElement::VERSION, '0.5.0', '>=')
        );
    }


    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/tagName
     */
    #[TestDox('Element.tagName is uppercase (string) IMG')]
    public function testElementTagNameIsUppercaseStringIMG(): void
    {
        $img = new HTMLImageElement();
        $this->assertNotEmpty($img->tagName);
        $this->assertTrue(ctype_upper($img->tagName));
        $this->assertEquals('IMG', $img->tagName);
    }

    #[Depends('testElementTagNameIsUppercaseStringIMG')]
    #[TestDox('Element.tagName is read-only')]
    public function testElementTagNameIsReadOnly(): void
    {
        $image = new HTMLImageElement();
        $this->expectException(\Throwable::class);
        $image->tagName = 'amp-img';
    }


    #[TestDox('HTMLImageElement::__construct exists')]
    public function testHTMLImageElementConstructorExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testHTMLImageElementConstructorExists')]
    #[TestDox('HTMLImageElement::__construct is public constructor')]
    public function testHTMLImageElementConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/Image
     */
    #[Depends('testHTMLImageElementConstructorExists')]
    #[TestDox('HTMLImageElement::__construct has two OPTIONAL parameters')]
    public function testHTMLImageElementConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementConstructorHasTwoOptionalParameters')]
    #[TestDox('HTMLImageElement::__construct sets image width')]
    public function testHTMLImageElementConstructorSetsImageWidth(): void
    {
        $img = new HTMLImageElement(100);
        $this->assertEquals(100, $img->width);
        $this->assertEmpty($img->height);

        $html = (string) $img;
        $this->assertStringContainsString(' width="100"', $html);
        $this->assertStringNotContainsString('height', $html);
    }

    #[Depends('testHTMLImageElementConstructorHasTwoOptionalParameters')]
    #[Depends('testHTMLImageElementConstructorSetsImageWidth')]
    #[TestDox('HTMLImageElement::__construct sets image width and height')]
    public function testHTMLImageElementConstructorSetsImageWidthAndHeight(): void
    {
        $img = new HTMLImageElement(100, 200);
        $this->assertEquals(100, $img->width);
        $this->assertEquals(200, $img->height);

        $html = (string) $img;
        $this->assertStringContainsString(' width="100"', $html);
        $this->assertStringContainsString(' height="200"', $html);
    }


    #[TestDox('HTMLImageElement::__toString exists')]
    public function testHTMLImageElementToStringExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testHTMLImageElementToStringExists')]
    #[TestDox('HTMLImageElement::__toString is public')]
    public function testHTMLImageElementToStringIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementToStringExists')]
    #[TestDox('HTMLImageElement::__toString returns non-empty string')]
    public function testHTMLImageElementToStringReturnsNonEmptyString(): void
    {
        $image = new HTMLImageElement();

        $this->assertNotEmpty($image->__toString());
        $this->assertIsString($image->__toString());

        $this->assertNotEmpty((string) $image);
        $this->assertIsString((string) $image);
    }

    #[Depends('testHTMLImageElementToStringReturnsNonEmptyString')]
    #[TestDox('HTMLImageElement::__toString returns <img> tag')]
    public function testHTMLImageElementToStringReturnsImgTag(): void
    {
        $image = new HTMLImageElement();
        $this->assertStringStartsWith('<img ', (string) $image);
        $this->assertStringEndsWith('>', (string) $image);
        $this->assertEmpty(strip_tags((string) $image));
    }


    #[TestDox('HTMLImageElement.alt exists')]
    public function testHTMLImageElementAltExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('alt', $image);
    }

    #[TestDox('HTMLImageElement::getAlt exists')]
    public function testHTMLImageElementGetAltExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getAlt'));
    }

    #[Depends('testHTMLImageElementGetAltExists')]
    #[TestDox('HTMLImageElement::getAlt is public')]
    public function testHTMLImageElementGetAltIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getAlt');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementGetAltExists')]
    #[TestDox('HTMLImageElement::getAlt has no parameters')]
    public function testHTMLImageElementGetAltHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getAlt');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetAltExists')]
    #[Depends('testHTMLImageElementGetAltIsPublic')]
    #[Depends('testHTMLImageElementGetAltHasNoParameters')]
    #[TestDox('HTMLImageElement::getAlt returns empty string by default')]
    public function testHTMLImageElementGetAltReturnsEmptyStringByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertEmpty($image->getAlt());
        $this->assertIsString($image->getAlt());
    }

    #[TestDox('HTMLImageElement.alt is empty string by default')]
    public function testHTMLImageElementAltIsEmptyStringByDefault(): void
    {
        $img = new HTMLImageElement();
        $img->src = '/files/16861/margin-flourish.svg';
        $html = (string) $img;
        $this->assertStringContainsString(' alt=""', $html);
    }


    #[TestDox('HTMLImageElement.decoding exists')]
    public function testHTMLImageElementDecodingExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('decoding', $image);
    }

    #[TestDox('HTMLImageElement::getDecoding exists')]
    public function testHTMLImageElementGetDecodingExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getDecoding'));
    }

    #[Depends('testHTMLImageElementGetDecodingExists')]
    #[TestDox('HTMLImageElement::getDecoding is public')]
    public function testHTMLImageElementGetDecodingIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getDecoding');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testHTMLImageElementGetDecodingExists')]
    #[TestDox('HTMLImageElement::getDecoding has no parameters')]
    public function testHTMLImageElementGetDecodingHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getDecoding');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetDecodingExists')]
    #[Depends('testHTMLImageElementGetDecodingIsPublic')]
    #[Depends('testHTMLImageElementGetDecodingHasNoParameters')]
    #[TestDox('HTMLImageElement::getDecoding has no parameters')]
    public function testHTMLImageElementDecodingIsAsyncByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertEquals('async', $image->getDecoding());
        $this->assertEquals('async', $image->decoding);

        $html = (string) $image;
        $html = strip_tags($html, '<img>');
        $this->assertStringContainsString(' decoding="async"', $html);
    }

    #[TestDox('HTMLImageElement::setDecoding exists')]
    public function testHTMLImageElementSetDecodingExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setDecoding'));
    }

    #[Depends('testHTMLImageElementSetDecodingExists')]
    #[TestDox('HTMLImageElement::setDecoding is public')]
    public function testHTMLImageElementSetDecodingIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setDecoding');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetDecodingExists')]
    #[TestDox('HTMLImageElement::setDecoding has one REQUIRED parameter')]
    public function testHTMLImageElementSetDecodingHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setDecoding');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementSetDecodingExists')]
    #[Depends('testHTMLImageElementSetDecodingIsPublic')]
    #[Depends('testHTMLImageElementSetDecodingHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setDecoding sets HTMLImageElement.decoding')]
    public function testHTMLImageElementSetDecodingSetsHTMLImageElementDecoding(): void
    {
        $image = new HTMLImageElement();

        $image->setDecoding('sync');
        $this->assertEquals('sync', $image->getDecoding());
        $this->assertEquals('sync', $image->decoding);

        $image->setDecoding('async');
        $this->assertEquals('async', $image->getDecoding());
        $this->assertEquals('async', $image->decoding);

        $image->setDecoding('auto');
        $this->assertEquals('auto', $image->getDecoding());
        $this->assertEquals('auto', $image->decoding);
    }


    #[TestDox('HTMLImageElement.height exists')]
    public function testHTMLImageElementHeightExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('height', $image);
    }

    #[TestDox('HTMLImageElement::getHeight exists')]
    public function testHTMLImageElementGetHeightExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getHeight'));
    }

    #[Depends('testHTMLImageElementGetHeightExists')]
    #[TestDox('HTMLImageElement::getHeight is public')]
    public function testHTMLImageElementGetHeightIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getHeight');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testHTMLImageElementGetHeightExists')]
    #[TestDox('HTMLImageElement::getHeight has no parameters')]
    public function testHTMLImageElementGetHeightHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getHeight');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetHeightExists')]
    #[Depends('testHTMLImageElementGetHeightIsPublic')]
    #[Depends('testHTMLImageElementGetHeightHasNoParameters')]
    #[TestDox('HTMLImageElement::getHeight returns null by default')]
    public function testHTMLImageElementGetHeightReturnsNullByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertNull($image->getHeight());
        $this->assertNull($image->height);
    }


    #[TestDox('HTMLImageElement.loading exists')]
    public function testHTMLImageElementLoadingExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('loading', $image);
    }

    #[TestDox('HTMLImageElement::getLoading exists')]
    public function testHTMLImageElementGetLoadingExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getLoading'));
    }

    #[Depends('testHTMLImageElementGetLoadingExists')]
    #[TestDox('HTMLImageElement::getLoading is public')]
    public function testHTMLImageElementGetLoadingIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getLoading');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementGetLoadingExists')]
    #[TestDox('HTMLImageElement::getLoading has no parameters')]
    public function testHTMLImageElementGetLoadingHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getLoading');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetLoadingExists')]
    #[Depends('testHTMLImageElementGetLoadingIsPublic')]
    #[Depends('testHTMLImageElementGetLoadingHasNoParameters')]
    #[TestDox('HTMLImageElement::getLoading has no parameters')]
    public function testHTMLImageElementGetLoadingReturnsNullByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertNotNull($image->getLoading());
        $this->assertIsString($image->getLoading());
        $this->assertSame('lazy', $image->getLoading());

        $html = (string) $image;
        $this->assertStringContainsString(' loading="lazy" ',  $html);
    }

    #[TestDox('HTMLImageElement::setLoading exists')]
    public function testHTMLImageElementSetLoadingExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setLoading'));
    }

    #[Depends('testHTMLImageElementSetLoadingExists')]
    #[TestDox('HTMLImageElement::setLoading is public')]
    public function testHTMLImageElementSetLoadingIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setLoading');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetLoadingExists')]
    #[TestDox('HTMLImageElement::setLoading has one REQUIRED parameter')]
    public function testHTMLImageElementSetLoadingHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setLoading');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementSetLoadingExists')]
    #[Depends('testHTMLImageElementSetLoadingIsPublic')]
    #[Depends('testHTMLImageElementSetLoadingHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setLoading accepts `eager` and `lazy` loading')]
    public function testHTMLImageElementSetLoadingAcceptsEagerAndLazyLoading(): void
    {
        $image = new HTMLImageElement();

        $image->setLoading('eager');
        $this->assertSame('eager', $image->getLoading());

        $image->setLoading('lazy');
        $this->assertSame('lazy', $image->getLoading());
    }

    #[Depends('testHTMLImageElementSetLoadingHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setLoading accepts null to remove `loading` attribute')]
    public function testHTMLImageElementSetLoadingAcceptsNullToRemoveLoadingAttribute(): void
    {
        $image = new HTMLImageElement();
        $this->assertNotNull($image->getLoading());
        $image->setLoading(null);
        $this->assertNull($image->getLoading());
    }

    #[Depends('testHTMLImageElementSetLoadingHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setLoading accepts empty string to remove loading attribute')]
    public function testHTMLImageElementSetLoadingAcceptsEmptyStringToRemoveLoadingAttribute(): void
    {
        $image = new HTMLImageElement();
        $this->assertNotNull($image->getLoading());
        $image->setLoading('');
        $this->assertNull($image->getLoading());
    }


    #[TestDox('HTMLImageElement.microdata exists')]
    public function testHTMLImageElementMicrodataExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('microdata', $image);
    }

    #[Depends('testHTMLImageElementMicrodataExists')]
    #[TestDox('HTMLImageElement.microdata is public')]
    public function testHTMLImageElementMicrodataIsPublic(): void
    {
        $property = new ReflectionProperty(HTMLImageElement::class, 'microdata');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testHTMLImageElementMicrodataExists')]
    #[TestDox('HTMLImageElement.microdata is null by default')]
    public function testHTMLImageElementMicrodataIsNullByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertNull($image->microdata);
    }

    #[Depends('testHTMLImageElementMicrodataExists')]
    #[Depends('testHTMLImageElementMicrodataIsPublic')]
    #[TestDox('HTMLImageElement.microdata accepts StoreCore\CMS\ImageObject')]
    public function testHTMLImageElementMicrodataAcceptsStoreCoreCmsImageObject(): void
    {
        $image = new HTMLImageElement();
        $image->microdata = new \StoreCore\CMS\ImageObject();
        $this->assertNotNull($image->microdata);
        $this->assertIsObject($image->microdata);
        $this->assertInstanceOf(\StoreCore\CMS\ImageObject::class, $image->microdata);
    }


    #[TestDox('HTMLImageElement.src exists')]
    public function testHTMLImageElementSrcExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('src', $image);
    }

    #[TestDox('HTMLImageElement::getSource exists')]
    public function testHTMLImageElementGetSourceExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getSource'));
    }

    #[Depends('testHTMLImageElementGetSourceExists')]
    #[TestDox('HTMLImageElement::getSource is public')]
    public function testmethodGetSourceIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getSource');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementGetSourceExists')]
    #[TestDox('HTMLImageElement::getSource has no parameters')]
    public function testHTMLImageElementGetSourceHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getSource');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetSourceExists')]
    #[Depends('testmethodGetSourceIsPublic')]
    #[Depends('testHTMLImageElementGetSourceHasNoParameters')]
    #[TestDox('HTMLImageElement::getSource returns non-empty string by default')]
    public function testHTMLImageElementGetSourceReturnsNonEmptyStringByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertNotEmpty($image->getSource());
        $this->assertIsString($image->getSource());
    }


    #[TestDox('HTMLImageElement.width exists')]
    public function testHTMLImageElementWidthExists(): void
    {
        $image = new HTMLImageElement();
        $this->assertObjectHasProperty('width', $image);
    }

    #[TestDox('HTMLImageElement::getWidth exists')]
    public function testHTMLImageElementGetWidthExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('getWidth'));
    }

    #[Depends('testHTMLImageElementGetWidthExists')]
    #[TestDox('HTMLImageElement::getWidth is public')]
    public function testHTMLImageElementGetWidthIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getWidth');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementGetWidthExists')]
    #[TestDox('HTMLImageElement::getWidth has no parameters')]
    public function testHTMLImageElementGetWidthHasNoParameters(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'getWidth');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testHTMLImageElementGetWidthHasNoParameters')]
    #[TestDox('HTMLImageElement::getWidth returns null by default')]
    public function testHTMLImageElementGetWidthReturnsNullByDefault(): void
    {
        $image = new HTMLImageElement();
        $this->assertNull($image->getWidth());
    }


    #[TestDox('HTMLImageElement::setAlt exists')]
    public function testHTMLImageElementSetAltExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setAlt'));
    }

    #[Depends('testHTMLImageElementSetAltExists')]
    #[TestDox('HTMLImageElement::setAlt is public')]
    public function testHTMLImageElementSetAltIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setAlt');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetAltExists')]
    #[TestDox('HTMLImageElement::setAlt has one REQUIRED parameter')]
    public function testHTMLImageElementSetAltHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setAlt');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('HTMLImageElement::setHeight exists')]
    public function testHTMLImageElementSetHeightExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setHeight'));
    }

    #[Depends('testHTMLImageElementSetHeightExists')]
    #[TestDox('HTMLImageElement::setHeight is public')]
    public function testHTMLImageElementSetHeightIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setHeight');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetHeightExists')]
    #[TestDox('HTMLImageElement::setHeight has one REQUIRED parameter')]
    public function testHTMLImageElementSetHeightHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setHeight');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementSetHeightHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setHeight sets HTML `height` attribute')]
    public function testHTMLImageElementSetHeightSetsHtmlHeightAttribute(): void
    {
        $image = new HTMLImageElement();
        $this->assertStringNotContainsString('height', (string)$image);
        $this->assertNull($image->getHeight());

        $image->setHeight(480);
        $this->assertStringContainsString(' height="480"', (string)$image);
        $this->assertNotNull($image->getHeight());
        $this->assertEquals(480, $image->getHeight());
    }

    #[Depends('testHTMLImageElementSetHeightHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setHeight throws \DomainException on 0 (zero)')]
    public function testHTMLImageElementSetHeightThrowsDomainExceptionOnZero(): void
    {
        $image = new HTMLImageElement();
        $this->expectException(\DomainException::class);
        $image->setHeight(0);
    }


    #[TestDox('HTMLImageElement::setSource exists')]
    public function testHTMLImageElementSetSourceExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setSource'));
    }

    #[Depends('testHTMLImageElementSetSourceExists')]
    #[TestDox('HTMLImageElement::setSource is public')]
    public function testHTMLImageElementSetSourceIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setSource');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetSourceExists')]
    #[TestDox('HTMLImageElement::setSource has one REQUIRED parameter')]
    public function testHTMLImageElementSetSourceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setSource');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementSetSourceExists')]
    #[Depends('testHTMLImageElementSetSourceIsPublic')]
    #[Depends('testHTMLImageElementSetSourceHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setSource sets HTML `src` attribute')]
    public function testHTMLImageElementSetSourceSetsHtmlSrcAttribute(): void
    {
        $image = new HTMLImageElement();
        $this->assertStringNotContainsString(' src="https://example.com/photos/4x3/photo.jpg"', (string)$image);
        $image->setSource('https://example.com/photos/4x3/photo.jpg');
        $this->assertStringContainsString(' src="https://example.com/photos/4x3/photo.jpg"', (string)$image);
    }


    #[TestDox('HTMLImageElement::setWidth exists')]
    public function testHTMLImageElementSetWidthExists(): void
    {
        $class = new ReflectionClass(HTMLImageElement::class);
        $this->assertTrue($class->hasMethod('setWidth'));
    }

    #[Depends('testHTMLImageElementSetWidthExists')]
    #[TestDox('HTMLImageElement::setWidth exists')]
    public function testHTMLImageElementSetWidthIsPublic(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setWidth');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testHTMLImageElementSetWidthExists')]
    #[TestDox('HTMLImageElement::setWidth has one REQUIRED parameter')]
    public function testHTMLImageElementSetWidthHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(HTMLImageElement::class, 'setWidth');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testHTMLImageElementSetWidthExists')]
    #[Depends('testHTMLImageElementSetWidthIsPublic')]
    #[Depends('testHTMLImageElementSetWidthHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setWidth sets HTML `width` attribute')]
    public function testHTMLImageElementSetWidthSetsHtmlWidthAttribute(): void
    {
        $image = new HTMLImageElement();
        $this->assertStringNotContainsString('width', (string)$image);
        $this->assertNull($image->getWidth());

        $image->setWidth(640);
        $this->assertStringContainsString(' width="640"', (string)$image);
        $this->assertNotNull($image->getWidth());
        $this->assertEquals(640, $image->getWidth());
    }

    #[Depends('testHTMLImageElementSetWidthHasOneRequiredParameter')]
    #[TestDox('HTMLImageElement::setWidth throws \DomainException on 0 (zero)')]
    public function testHTMLImageElementSetWidthThrowsDomainExceptionOnZero()
    {
        $image = new HTMLImageElement();
        $this->expectException(\DomainException::class);
        $image->setWidth(0);
    }


    #[TestDox('HTMLImageElement supports 8K UHD image resolutions')]
    public function testHTMLImageElementSupports8KUhdHTMLImageElementResolutions()
    {
        $image = new HTMLImageElement();
        $image->setWidth(7680);
        $image->setHeight(4320);
        $this->assertSame(7680, $image->getWidth());
        $this->assertSame(4320, $image->getHeight());
        $this->assertStringContainsString(' width="7680"', (string) $image);
        $this->assertStringContainsString(' height="4320"', (string) $image);
    }

    #[TestDox('HTMLImageElement::setWidth throws \DomainException on width over 7680')]
    public function testHTMLImageElementSetWidthThrowsDomainExceptionOnWidthOver7680(): void
    {
        $image = new HTMLImageElement();
        $this->expectException(\DomainException::class);
        $image->setWidth(7680 + 1);
    }

    #[TestDox('HTMLImageElement::setHeight throws \DomainException on height over 4320')]
    public function testHTMLImageElementSetHeightThrowsDomainExceptionOnHeightOver4320(): void
    {
        $image = new HTMLImageElement();
        $this->expectException(\DomainException::class);
        $image->setHeight(4320 + 1);
    }
}
