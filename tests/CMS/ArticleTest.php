<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ArticleTest extends TestCase
{
    #[TestDox('Article class is concrete')]
    public function testArticleClassIsConcrete(): void
    {
        $class = new ReflectionClass(Article::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Article is a CreativeWork Thing')]
    public function testArticleIsCreativeWorkThing(): void
    {
        $article = new Article();
        $this->assertInstanceOf(AbstractCreativeWork::class, $article);
        $this->assertInstanceOf(AbstractThing::class, $article);
    }

    #[Group('hmvc')]
    #[TestDox('Article is JSON serializable')]
    public function testArticleIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Article());
    }

    /**
     * @see https://schema.org/docs/full.html Full Schema.org hierarchy
     */
    #[Group('hmvc')]
    #[TestDox('Article subtypes')]
    public function testArticleSubtypes(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\AdvertiserContentArticle'));
        $this->assertInstanceOf(Article::class, new AdvertiserContentArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\AnalysisNewsArticle'));
        $this->assertInstanceOf(Article::class, new AnalysisNewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\APIReference'));
        $this->assertInstanceOf(Article::class, new APIReference());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\AskPublicNewsArticle'));
        $this->assertInstanceOf(Article::class, new AskPublicNewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\BackgroundNewsArticle'));
        $this->assertInstanceOf(Article::class, new BackgroundNewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\BlogPosting'));
        $this->assertInstanceOf(Article::class, new BlogPosting());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\DiscussionForumPosting'));
        $this->assertInstanceOf(Article::class, new DiscussionForumPosting());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\MedicalScholarlyArticle'));
        $this->assertInstanceOf(Article::class, new MedicalScholarlyArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\NewsArticle'));
        $this->assertInstanceOf(Article::class, new NewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\OpinionNewsArticle'));
        $this->assertInstanceOf(Article::class, new OpinionNewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Report'));
        $this->assertInstanceOf(Article::class, new Report());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ReportageNewsArticle'));
        $this->assertInstanceOf(Article::class, new ReportageNewsArticle());

        /* `ReviewNewsArticle` is NOT an `Article` but a `CriticReview` that
         * implements `Article` properties through the `ArticleTrait`.
         */
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ReviewNewsArticle'));
        $this->assertNotInstanceOf(Article::class, new ReviewNewsArticle());
        $this->assertInstanceOf(Review::class, new ReviewNewsArticle());
        $this->assertInstanceOf(CriticReview::class, new ReviewNewsArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\SatiricalArticle'));
        $this->assertInstanceOf(Article::class, new SatiricalArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ScholarlyArticle'));
        $this->assertInstanceOf(Article::class, new ScholarlyArticle());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\SocialMediaPosting'));
        $this->assertInstanceOf(Article::class, new SocialMediaPosting());

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\TechArticle'));
        $this->assertInstanceOf(Article::class, new TechArticle());
    }

    /**
     * @see https://schema.org/Article Schema.org type `Article`
     */
    #[Group('hmvc')]
    #[TestDox('Article has all ArticleTrait properties')]
    public function testArticleHasAllArticleTraitProperties(): void
    {
        $this->assertTrue(
            trait_exists('\\StoreCore\\CMS\\ArticleTrait'),
            '`ArticleTrait` implements all `Article` properties.'
        );

        $article = new Article();

        $this->assertObjectHasProperty('articleBody', $article);
        $this->assertNull($article->articleBody);

        $this->assertObjectHasProperty('articleSection', $article);
        $this->assertNull($article->articleSection);

        $this->assertObjectHasProperty('backstory', $article);
        $this->assertNull($article->backstory);

        $this->assertObjectHasProperty('pageEnd', $article);
        $this->assertNull($article->pageEnd);

        $this->assertObjectHasProperty('pageStart', $article);
        $this->assertNull($article->pageStart);

        $this->assertObjectHasProperty('pagination', $article);
        $this->assertNull($article->pagination);

        $this->assertObjectHasProperty('speakable', $article);
        $this->assertNull($article->speakable);

        $this->assertObjectHasProperty('wordCount', $article);
        $this->assertNull($article->wordCount);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Article::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Article::VERSION);
        $this->assertIsString(Article::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Article::VERSION, '1.0.0', '>=')
        );
    }
}
