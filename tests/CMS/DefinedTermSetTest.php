<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(DefinedTermSet::class)]
#[UsesClass(\StoreCore\Types\DefinedTerm::class)]
final class DefinedTermSetTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DefinedTermSet class exists')]
    public function testDefinedTermSetClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'DefinedTermSet.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'DefinedTermSet.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\DefinedTermSet'));
        $this->assertTrue(class_exists(DefinedTermSet::class));
    }


    #[Group('hmvc')]
    #[TestDox('DefinedTermSet class is concrete')]
    public function testDefinedTermSetClassIsConcrete(): void
    {
        $class = new ReflectionClass(DefinedTermSet::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('DefinedTermSet is a CreativeWork Thing')]
    public function testDefinedTermSetIsCreativeWorkThing(): void
    {
        $definedTermSet = new DefinedTermSet();
        $this->assertInstanceOf(AbstractCreativeWork::class, $definedTermSet);
        $this->assertInstanceOf(AbstractThing::class, $definedTermSet);
    }

    #[Group('seo')]
    #[TestDox('DefinedTermSet is JSON serializable')]
    public function testDefinedTermSetIsJsonSerializable(): void
    {
        $definedTermSet = new DefinedTermSet();
        $this->assertInstanceOf(\JsonSerializable::class, $definedTermSet);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DefinedTermSet::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DefinedTermSet::VERSION);
        $this->assertIsString(DefinedTermSet::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DefinedTermSet::VERSION, '1.0.0', '>=')
        );
    }
}
