<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CMS\Article::class)]
#[CoversClass(\StoreCore\CMS\NewsArticle::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class NewsArticleTest extends TestCase
{
    #[TestDox('NewsArticle class is concrete')]
    public function testNewsArticleClassIsConcrete(): void
    {
        $class = new ReflectionClass(NewsArticle::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('NewsArticle is an Article')]
    public function testNewsArticleIsArticle(): void
    {
        $this->assertInstanceOf(Article::class, new NewsArticle());
    }

    #[Group('hmvc')]
    #[TestDox('NewsArticle is a CreativeWork Thing')]
    public function testNewsArticleIsCreativeWorkThing(): void
    {
        $article = new NewsArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $article);
        $this->assertInstanceOf(AbstractThing::class, $article);
    }

    #[Group('hmvc')]
    #[TestDox('NewsArticle is JSON serializable')]
    public function testNewsArticleIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new NewsArticle());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(NewsArticle::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(NewsArticle::VERSION);
        $this->assertIsString(NewsArticle::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(NewsArticle::VERSION, '0.1.0', '>=')
        );
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/article
     *      “Article (`Article`, `NewsArticle`, `BlogPosting`) structured data”, Google Search Central
     *
     * @see https://search.google.com/test/rich-results/result
     *      Rich Results Test for Google Search
     */
    #[Depends('testNewsArticleIsJsonSerializable')]
    #[Group('seo')]
    #[Test]
    #[TestDox('Google Search structured data for Article acceptance test')]
    public function GoogleSearchStructuredDataForArticleAcceptanceTest(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "headline": "Title of a News Article",
            "image": [
              "https://example.com/photos/1x1/photo.jpg",
              "https://example.com/photos/4x3/photo.jpg",
              "https://example.com/photos/16x9/photo.jpg"
             ],
            "datePublished": "2015-02-05T08:00:00+08:00",
            "dateModified": "2015-02-05T09:20:00+08:00",
            "author": [{
                "@type": "Person",
                "name": "Jane Doe",
                "url": "https://example.com/profile/janedoe123"
              },{
                "@type": "Person",
                "name": "John Doe",
                "url": "https://example.com/profile/johndoe123"
            }]
          }
        ';

        $article = new NewsArticle();
        $this->assertInstanceOf(Article::class, $article);

        $article->headline = 'Title of a News Article';
        $this->assertSame('Title of a News Article', $article->headline);

        $article->image = [
            'https://example.com/photos/1x1/photo.jpg',
            'https://example.com/photos/4x3/photo.jpg',
            'https://example.com/photos/16x9/photo.jpg'
        ];
        $this->assertIsArray($article->image);
        $this->assertCount(3, $article->image);

        $article->datePublished = '2015-02-05T08:00:00+08:00';

        $article->dateModified = '2015-02-05T09:20:00+08:00';

        $author = array();

        $author[0] = new Person();
        $author[0]->name = 'Jane Doe';
        $author[0]->url = 'https://example.com/profile/janedoe123';

        $author[1] = new Person();
        $author[1]->name = 'John Doe';
        $author[1]->url = 'https://example.com/profile/johndoe123';

        $article->author = $author;
        $this->assertIsArray($article->author);
        $this->assertCount(2, $article->author);

        $this->assertInstanceOf(\Stringable::class, $article);
        $actualJson = (string) $article;
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/article
     */
    #[Group('seo')]
    #[TestDox('Google Search author markup best practices')]
    public function testGoogleSearchAuthorMarkupBestPractices(): void
    {
        /*
            ```json
            "author":
              [
                {
                  "name": "Echidna Jones",
                  "honorificPrefix": "Dr",
                  "jobTitle": "Editor in Chief"
                }
              ],
            "publisher":
              [
                {
                  "name": "Bugs Daily"
                }
              ]
            }
            ```
         */
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Article",
            "author": {
              "@type": "Person",
              "name": "Echidna Jones",
              "honorificPrefix": "Dr",
              "jobTitle": "Editor in Chief"
            },
            "publisher": {
              "@type": "Organization",
              "name": "Bugs Daily"
            }
          }
        ';

        $person = new Person('Echidna Jones');
        $person->honorificPrefix = 'Dr';
        $person->jobTitle = 'Editor in Chief';

        $organization = new Organization('Bugs Daily');

        $article = new Article();
        $article->author = $person;
        $article->publisher = $organization;

        $actualJson = (string) $article;
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
