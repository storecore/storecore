<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;
use StoreCore\Types\AggregateRating;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[CoversClass(\StoreCore\CMS\CreativeWorkRepository::class)]
#[CoversClass(\StoreCore\Database\CreativeWorkCache::class)]
#[CoversClass(\StoreCore\Database\CreativeWorkMapper::class)]
#[CoversClass(\StoreCore\Database\CreativeWorkTextMapper::class)]
#[CoversClass(\StoreCore\Database\CreativeWorkTypes::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\FileSystem\ObjectCacheReader::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\AggregateRating::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class CreativeWorkRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        } else {
            $db = $registry->get('Database');

            // CreativeWork.author is a Person
            $db->exec(
                "DELETE
                   FROM `sc_persons`
                  WHERE `family_name` = 'Freeman'
                    AND `given_name`  = 'Emily'"
            );

            // CreativeWork is an Organization
            $db->exec(
                "DELETE
                   FROM `sc_organizations`
                  WHERE `common_name`    = 'John Wiley & Sons'
                    AND `legal_name`     = 'John Wiley & Sons, Inc.'
                    AND `alternate_name` = 'Wiley'"
            );

            // CreativeWork
            $db->exec(
                "DELETE
                   FROM `sc_creative_works`
                  WHERE `creative_work_id`
                     IN 
                        (
                            SELECT `creative_work_id`
                              FROM `sc_creative_work_texts`
                             WHERE `name` = 'DevOps for Dummies'
                        )"
            );
        }
    }


    #[TestDox('CreativeWorkRepository class is concrete')]
    public function testCreativeWorkRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(CreativeWorkRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('CreativeWorkRepository implements PSR-11 ContainerInterface')]
    public function testCreativeWorkRepositoryImplementsPSR11ContainerInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));

        $registry = Registry::getInstance();
        $repository = new CreativeWorkRepository($registry);
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $repository);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreativeWorkRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreativeWorkRepository::VERSION);
        $this->assertIsString(CreativeWorkRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreativeWorkRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWorkRepository stores aggregates')]
    public function testCreativeWorkRepositoryStoresAggregates(): void
    {
        $author = new Person();
        $author->givenName = 'Emily';
        $author->familyName = 'Freeman';

        $publisher = new Organization();
        $publisher->name = 'John Wiley & Sons';
        $publisher->legalName = 'John Wiley & Sons, Inc.';
        $publisher->alternateName = 'Wiley';

        $book = new Book();
        $book->additionalType = 'https://schema.org/Product';
        $book->author = $author;
        $book->inLanguage = 'en-US';
        $book->isbn = '978-1-119-55222-2';
        $book->name = 'DevOps for Dummies';
        $book->numberOfPages = 338;
        $book->publisher = $publisher;

        $book->aggregateRating = new AggregateRating();
        $book->aggregateRating->ratingValue = 4.4;
        $book->aggregateRating->reviewCount = 273;

        $this->assertInstanceOf(AbstractCreativeWork::class, $book);

        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $book);
        $this->assertFalse($book->hasIdentifier());

        $registry = Registry::getInstance();
        $repository = new CreativeWorkRepository($registry);
        // @todo Fix failing tests
        // $this->assertTrue($repository->set($book));
        // $this->assertTrue($book->hasIdentifier());
    }
}
