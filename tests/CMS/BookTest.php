<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class BookTest extends TestCase
{
    #[TestDox('Book class is concrete')]
    public function testBookClassIsConcrete(): void
    {
        $class = new ReflectionClass(Book::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Book is a CreativeWork Thing')]
    public function testBookIsCreativeWorkThing(): void
    {
        $book = new Book();
        $this->assertInstanceOf(AbstractCreativeWork::class, $book);
        $this->assertInstanceOf(AbstractThing::class, $book);
    }

    #[Group('hmvc')]
    #[TestDox('Book is JSON serializable')]
    public function testBookIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Book());
    }
}
