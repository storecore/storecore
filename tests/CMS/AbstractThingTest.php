<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[Group('hmvc')]
final class AbstractThingTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractThing class file exists')]
    public function testAbstractThingClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR .  'AbstractThing.php'
        );
    }

    #[Depends('testAbstractThingClassFileExists')]
    #[Group('distro')]
    #[TestDox('AbstractThing class file exists')]
    public function testAbstractThingClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR .  'AbstractThing.php'
        );
    }

    #[Depends('testAbstractThingClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('AbstractThing class exists')]
    public function testAbstractThingClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\AbstractThing', false));
        $this->assertTrue(class_exists(AbstractThing::class));
    }


    #[TestDox('AbstractThing is abstract')]
    public function testAbstractThingIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractThing::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractThing::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AbstractThing::VERSION);
        $this->assertIsString(AbstractThing::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AbstractThing::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
