<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ReviewNewsArticleTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ReviewNewsArticle class exists')]
    public function testReviewNewsArticleClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ReviewNewsArticle.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ReviewNewsArticle.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ReviewNewsArticle'));
        $this->assertTrue(class_exists(ReviewNewsArticle::class));
    }

    #[TestDox('ReviewNewsArticle class is concrete')]
    public function testReviewNewsArticleClassIsConcrete(): void
    {
        $class = new ReflectionClass(ReviewNewsArticle::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ReviewNewsArticle is a CreativeWork Thing')]
    public function testReviewNewsArticleIsCreativeWorkThing(): void
    {
        $article = new ReviewNewsArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $article);
        $this->assertInstanceOf(AbstractThing::class, $article);
    }

    #[Group('hmvc')]
    #[TestDox('ReviewNewsArticle is a CriticReview')]
    public function testReviewNewsArticleIsCriticReview(): void
    {
        $article = new ReviewNewsArticle();
        $this->assertInstanceOf(CriticReview::class, $article);
        $this->assertInstanceOf(Review::class, $article);
    }

    #[TestDox('ReviewNewsArticle is JSOON serializable')]
    public function testReviewNewsArticleIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ReviewNewsArticle());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ReviewNewsArticle::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ReviewNewsArticle::VERSION);
        $this->assertIsString(ReviewNewsArticle::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ReviewNewsArticle::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('ReviewNewsArticle.author exists')]
    public function testReviewNewsArticleAuthorExists(): void
    {
        $article = new ReviewNewsArticle();
        $this->assertObjectHasProperty('author', $article);
    }


    #[TestDox('ReviewNewsArticle has all properties from Article')]
    public function testReviewNewsArticleHasAllPropertiesFromArticle(): void
    {
        $article = new ReviewNewsArticle();
        $this->assertObjectHasProperty('articleBody', $article);
        $this->assertObjectHasProperty('articleSection', $article);
        $this->assertObjectHasProperty('backstory', $article);
        $this->assertObjectHasProperty('pageEnd', $article);
        $this->assertObjectHasProperty('pageStart', $article);
        $this->assertObjectHasProperty('pagination', $article);
        $this->assertObjectHasProperty('speakable', $article);
        $this->assertObjectHasProperty('wordCount', $article);
    }

    #[Depends('testReviewNewsArticleHasAllPropertiesFromArticle')]
    #[TestDox('ReviewNewsArticle properties from Article are public and null')]
    public function testReviewNewsArticlePropertiesFromArticleArePublicAndNull(): void
    {
        $article = new ReviewNewsArticle();

        $properties = [
            'articleBody',
            'articleSection',
            'backstory',
            'pageEnd',
            'pageStart',
            'pagination',
            'speakable',
            'wordCount',
        ];

        foreach ($properties as $propertyName) {
            $property = new ReflectionProperty(ReviewNewsArticle::class, $propertyName);
            $this->assertTrue($property->isPublic());
            $this->assertFalse($property->isReadOnly());
            $this->assertTrue($property->isDefault());
            $this->assertNull($property->getValue($article));
        }
    }
}
