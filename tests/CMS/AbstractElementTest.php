<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractElementTest extends TestCase
{
    #[TestDox('AbstractElement class is abstract')]
    public function testAbstractElementClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractElement::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractElement::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AbstractElement::VERSION);
        $this->assertIsString(AbstractElement::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AbstractElement::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/className
     */
    #[TestDox('Element.className exists')]
    public function testElementClassNameExists(): void
    {
        $element = new class extends AbstractElement {};
        $this->assertObjectHasProperty('className', $element);
    }

    #[Depends('testElementClassNameExists')]
    #[TestDox('Element.className is empty by default')]
    public function testElementClassNameIsEmptyByDefault(): void
    {
        $element = new class extends AbstractElement {};
        $this->assertEmpty($element->className);
    }


    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/id
     */
    #[TestDox('Element.id exists')]
    public function testElementIdExists(): void
    {
        $element = new class extends AbstractElement {};
        $this->assertObjectHasProperty('id', $element);
    }
}
