<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class RecommendationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Recommendation class exists')]
    public function testRecommendationClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Recommendation.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Recommendation.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Recommendation'));
        $this->assertTrue(class_exists(Recommendation::class));
    }

    #[TestDox('Recommendation class is concrete')]
    public function testRecommendationClassIsConcrete(): void
    {
        $class = new ReflectionClass(Recommendation::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Recommendation is a Review CreativeWork')]
    public function testRecommendationIsReviewCreativeWork(): void
    {
        $place = new Recommendation();
        $this->assertInstanceOf(\StoreCore\CMS\Review::class, $place);
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $place);
        $this->assertInstanceOf(\StoreCore\CMS\AbstractThing::class, $place);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Recommendation::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Recommendation::VERSION);
        $this->assertIsString(Recommendation::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Recommendation::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    #[TestDox('Recommendation.category exists')]
    public function testRecommendationCategoryExists(): void
    {
        $this->assertObjectHasProperty('category', new Recommendation());
    }

    #[Depends('testRecommendationCategoryExists')]
    #[TestDox('Recommendation.category is public')]
    public function testRecommendationCategoryIsPublic(): void
    {
        $property = new ReflectionProperty(Recommendation::class, 'category');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testRecommendationCategoryExists')]
    #[TestDox('Recommendation.category is null by default')]
    public function testRecommendationCategoryIsNullByDefault()
    {
        $recommendation = new Recommendation();
        $this->assertNull($recommendation->category);
    }
}
