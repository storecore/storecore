<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class EmailMessageTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('EmailMessage class exists')]
    public function testEmailMessageClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'EmailMessage.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'EmailMessage.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\EmailMessage'));
        $this->assertTrue(class_exists(EmailMessage::class));
    }


    #[Group('hmvc')]
    #[TestDox('EmailMessage class is concrete')]
    public function testEmailMessageClassIsConcrete(): void
    {
        $class = new ReflectionClass(EmailMessage::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testEmailMessageClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('EmailMessage class is final')]
    public function testEmailMessageClassIsFinal(): void
    {
        $class = new ReflectionClass(EmailMessage::class);
        $this->assertTrue($class->isFinal());
    }

    #[Group('hmvc')]
    #[TestDox('EmailMessage is a Message')]
    public function testEmailMessageIsAMessage(): void
    {
        $this->assertInstanceOf(Message::class, new EmailMessage());
    }

    #[Group('hmvc')]
    #[TestDox('EmailMessage is a CreativeWork Thing')]
    public function testEmailMessageIsCreativeWorkThing(): void
    {
        $message = new EmailMessage();
        $this->assertInstanceOf(AbstractThing::class, $message);
        $this->assertInstanceOf(AbstractCreativeWork::class, $message);
    }

    #[Group('hmvc')]
    #[TestDox('EmailMessage is JSON serializable')]
    public function testEmailMessageIsJsonSerializable(): void
    {
        $message = new EmailMessage();
        $this->assertInstanceOf(\JsonSerializable::class, new EmailMessage());
    }

    #[Group('hmvc')]
    #[TestDox('EmailMessage is stringable')]
    public function testEmailMessageIsStringable(): void
    {
        $message = new EmailMessage();
        $this->assertInstanceOf(\Stringable::class, new EmailMessage());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EmailMessage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EmailMessage::VERSION);
        $this->assertIsString(EmailMessage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EmailMessage::VERSION, '1.0.0', '>=')
        );
    }
}
