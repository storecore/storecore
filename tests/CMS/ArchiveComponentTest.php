<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\ArchiveOrganization;
use StoreCore\Geo\Place;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\CMS\ArchiveComponent::class)]
#[UsesClass(\StoreCore\CRM\ArchiveOrganization::class)]
#[UsesClass(\StoreCore\Geo\Place::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class ArchiveComponentTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ArchiveComponent class file exists')]
    public function testArchiveComponentClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ArchiveComponent.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ArchiveComponent.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ArchiveComponent'));
        $this->assertTrue(class_exists(ArchiveComponent::class));
    }

    #[TestDox('ArchiveComponent class is concrete')]
    public function testArchiveComponentClassIsConcrete(): void
    {
        $class = new ReflectionClass(ArchiveComponent::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveComponent is a CreativeWork Thing')]
    public function testArchiveComponentIsCreativeWorkThing(): void
    {
        $archive = new ArchiveComponent();
        $this->assertInstanceOf(AbstractCreativeWork::class, $archive);
        $this->assertInstanceOf(AbstractThing::class, $archive);
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveComponent is JSON serializable')]
    public function testArchiveComponentIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ArchiveComponent());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ArchiveComponent::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ArchiveComponent::VERSION);
        $this->assertIsString(ArchiveComponent::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ArchiveComponent::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    /**
     * @see https://schema.org/holdingArchive
     *      Schema.org `holdingArchive` property of an `ArchiveComponent`
     */
    #[TestDox('ArchiveComponent.holdingArchive exists')]
    public function testArchiveComponentHoldingArchiveExists(): void
    {
        $archiveComponent = new ArchiveComponent();
        $this->assertObjectHasProperty('holdingArchive', $archiveComponent);
    }

    #[Depends('testArchiveComponentHoldingArchiveExists')]
    #[TestDox('ArchiveComponent.holdingArchive is public')]
    public function testArchiveComponentHoldingArchiveIsPublic(): void
    {
        $property = new ReflectionProperty(ArchiveComponent::class, 'holdingArchive');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testArchiveComponentHoldingArchiveExists')]
    #[TestDox('ArchiveComponent.holdingArchive is null by default')]
    public function testArchiveComponentHoldingArchiveIsNullByDefault(): void
    {
        $archiveComponent = new ArchiveComponent();
        $this->assertNull($archiveComponent->holdingArchive);
    }

    #[Depends('testArchiveComponentHoldingArchiveIsNullByDefault')]
    #[TestDox('ArchiveComponent.holdingArchive accepts ArchiveOrganization')]
    public function testArchiveComponentHoldingArchiveAcceptsArchiveOrganization(): void
    {
        $this->assertTrue(class_exists(ArchiveOrganization::class));
        $organization = new ArchiveOrganization();
        $organization->name = 'Library of Congress';
        $organization->sameAs = new URL('https://en.wikipedia.org/wiki/Library_of_Congress');

        $archiveComponent = new ArchiveComponent();
        $archiveComponent->holdingArchive = $organization;
        $this->assertNotNull($archiveComponent->holdingArchive);
        $this->assertSame('Library of Congress', $archiveComponent->holdingArchive->name);
    }


    /**
     * @see https://schema.org/itemLocation
     *      Schema.org `itemLocation` property of an `ArchiveComponent`
     */
    #[TestDox('ArchiveComponent.itemLocation exists')]
    public function testArchiveComponentItemLocationExists(): void
    {
        $archiveComponent = new ArchiveComponent();
        $this->assertObjectHasProperty('itemLocation', $archiveComponent);
    }

    #[Depends('testArchiveComponentItemLocationExists')]
    #[TestDox('ArchiveComponent.itemLocation is public')]
    public function testArchiveComponentItemLocationIsPublic(): void
    {
        $property = new ReflectionProperty(ArchiveComponent::class, 'itemLocation');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testArchiveComponentItemLocationExists')]
    #[TestDox('ArchiveComponent.itemLocation is null by default')]
    public function testArchiveComponentItemLocationIsNullByDefault(): void
    {
        $archiveComponent = new ArchiveComponent();
        $this->assertNull($archiveComponent->itemLocation);
    }

    #[Depends('testArchiveComponentItemLocationIsNullByDefault')]
    #[TestDox('ArchiveComponent.itemLocation accepts Place')]
    public function testArchiveComponentItemLocationAcceptsPlace(): void
    {
        $this->assertTrue(class_exists(Place::class));
        $place = new Place();

        $archiveComponent = new ArchiveComponent();
        $archiveComponent->itemLocation = $place;
        $this->assertNotNull($archiveComponent->itemLocation);
        $this->assertIsObject($archiveComponent->itemLocation);
        $this->assertInstanceOf(Place::class, $archiveComponent->itemLocation);
    }
}
