<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Actions\WatchAction;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\Movie::class)]
#[CoversClass(\StoreCore\Actions\WatchAction::class)]
final class MovieTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Movie class exists')]
    public function testMovieClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Movie.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'Movie.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Movie'));
        $this->assertTrue(class_exists(Movie::class));
    }

    #[TestDox('Movie class is concrete')]
    public function testMovieClassIsConcrete(): void
    {
        $class = new \ReflectionClass(Movie::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Movie is a CreativeWork Thing')]
    public function testMovieIsCreativeWorkThing(): void
    {
        $action = new Movie();
        $this->assertInstanceOf(AbstractCreativeWork::class, $action);
        $this->assertInstanceOf(AbstractThing::class, $action);
    }

    #[TestDox('Movie is JSON serializable')]
    public function testMovieIsJsonSerializable(): void
    {
        $action = new Movie();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }

    #[TestDox('Movie is stringable')]
    public function testMovieIsStringable(): void
    {
        $action = new Movie();
        $this->assertInstanceOf(\Stringable::class, $action);
    }



    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Movie::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Movie::VERSION);
        $this->assertIsString(Movie::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Movie::VERSION, '0.2.0', '>=')
        );
    }

    /**
     * @see https://schema.org/Movie#eg-0187 Schema.org `Movie` example 2
     * 
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "Movie",
     *       "name": "Footloose",
     *       "potentialAction": {
     *         "@type": "WatchAction",
     *         "target": "http://example.com/player?id=123"
     *       }
     *     }
     *     ```
     * 
     * The `target` URL string SHOULD be expanded to an `EntryPoint` object
     * with an `EntryPoint.urlTemplate` property for the URL:
     * 
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "Movie",
     *       "name": "Footloose",
     *       "potentialAction": {
     *         "@type": "WatchAction",
     *         "target": {
     *           "@type": "EntryPoint",
     *           "urlTemplate": "http://example.com/player?id=123"
     *         }
     *       }
     *     }
     *     ```
     */
    #[TestDox('`Movie` named “Footloose” with `WatchAction` as `potentialAction`')]
    public function testMovieNamedFootlooseWithWatchActionAsPotentialAction(): void
    {
        $movie = new Movie('Footloose');
        $this->assertSame('Footloose', $movie->name);

        // `potentialAction.target` is mapped to `potentialAction.target.urlTemplate`.
        $movie->potentialAction = new WatchAction();
        $movie->potentialAction->target = 'http://example.com/player?id=123';
        $this->assertSame('http://example.com/player?id=123', $movie->potentialAction->target->urlTemplate);

        $json = (string) $movie;
        $this->assertStringContainsString('"name":"Footloose"', $json);
        $this->assertStringContainsString('"potentialAction":{"@type":"WatchAction","target":{"@type":"EntryPoint","urlTemplate":"http://example.com/player?id=123"}}', $json);
    }
}
