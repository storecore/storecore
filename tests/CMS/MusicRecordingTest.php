<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class MusicRecordingTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('MusicRecording class exists')]
    public function testMusicRecordingClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'MusicRecording.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'MusicRecording.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\MusicRecording'));
        $this->assertTrue(class_exists(MusicRecording::class));
    }

    #[Group('hmvc')]
    #[TestDox('MusicRecording class is concrete')]
    public function testMusicRecordingClassIsConcrete(): void
    {
        $class = new ReflectionClass(MusicRecording::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MusicRecording is a CreativeWork Thing')]
    public function testMusicRecordingIsCreativeWorkThing()
    {
        $action = new MusicRecording();
        $this->assertInstanceOf(AbstractCreativeWork::class, $action);
        $this->assertInstanceOf(AbstractThing::class, $action);
    }

    #[TestDox('MusicRecording is JSON serializable')]
    public function testMusicRecordingIsJsonSerializable()
    {
        $this->assertInstanceOf(\JsonSerializable::class, new MusicRecording());
    }

    #[TestDox('MusicRecording is stringable')]
    public function testMusicRecordingIsStringable()
    {
        $this->assertInstanceOf(\Stringable::class, new MusicRecording());
    }

    
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MusicRecording::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MusicRecording::VERSION);
        $this->assertIsString(MusicRecording::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MusicRecording::VERSION, '0.2.0', '>=')
        );
    }
}
