<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class AmpStoryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AmpStory class exists')]
    public function testAmpStoryClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'AmpStory.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'AmpStory.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\AmpStory'));
        $this->assertTrue(class_exists(AmpStory::class));
    }


    #[TestDox('AmpStory class is concrete')]
    public function testAmpStoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(AmpStory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AmpStory is a MediaObject')]
    public function testAmpStoryIsMediaObject(): void
    {
        $this->assertInstanceOf(MediaObject::class, new AmpStory());
    }

    #[Group('hmvc')]
    #[TestDox('AmpStory is a CreativeWork Thing')]
    public function testAmpStoryIsCreativeWorkThing(): void
    {
        $story = new AmpStory();
        $this->assertInstanceOf(AbstractCreativeWork::class, $story);
        $this->assertInstanceOf(AbstractThing::class, $story);
    }

    #[Group('hmvc')]
    #[TestDox('AmpStory is JSON serializable')]
    public function testAmpStoryIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new AmpStory());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AmpStory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AmpStory::VERSION);
        $this->assertIsString(AmpStory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AmpStory::VERSION, '1.0.0-rc.1', '>=')
        );
    }
}
