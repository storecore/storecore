<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\Message::class)]
final class MessageTest extends TestCase
{
    #[TestDox('Message class is concrete')]
    public function testMessageClassIsConcrete(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Message is a CreativeWork Thing')]
    public function testMessageIsCreativeWorkThing(): void
    {
        $message = new Message();
        $this->assertInstanceOf(AbstractThing::class, $message);
        $this->assertInstanceOf(AbstractCreativeWork::class, $message);
    }

    #[Group('hmvc')]
    #[TestDox('Message is JSON serializable')]
    public function testMessageIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Message());
    }

    #[Group('hmvc')]
    #[TestDox('Message is stringable')]
    public function testMessageIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Message());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Message::VERSION);
        $this->assertIsString(Message::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Message::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Message.dateRead exists')]
    public function testMessageDateReadExists(): void
    {
        $message = new Message();
        $this->assertObjectHasProperty('dateRead', $message);
    }

    #[Depends('testMessageDateReadExists')]
    #[TestDox('Message.dateRead is null by default')]
    public function testMessageDateReadIsNullByDefault(): void
    {
        $message = new Message();
        $this->assertNull($message->dateRead);
    }


    #[TestDox('Message.recipient exists')]
    public function testMessageRecipientExists(): void
    {
        $message = new Message();
        $this->assertObjectHasProperty('recipient', $message);
    }

    #[Depends('testMessageRecipientExists')]
    #[TestDox('Message.recipient is null by default')]
    public function testMessageRecipientIsNullByDefault(): void
    {
        $message = new Message();
        $this->assertNull($message->sender);
    }


    #[TestDox('Message.sender exists')]
    public function testMessageSenderExists(): void
    {
        $message = new Message();
        $this->assertObjectHasProperty('sender', $message);
    }

    #[Depends('testMessageSenderExists')]
    #[TestDox('Message.sender is null by default')]
    public function testMessageSenderIsNullByDefault(): void
    {
        $message = new Message();
        $this->assertNull($message->sender);
    }
}
