<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[CoversClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[CoversClass(\StoreCore\CMS\CreativeWorkSeries::class)]
#[CoversClass(\StoreCore\CMS\BookSeries::class)]
#[CoversClass(\StoreCore\CRM\Person::class)]
#[CoversClass(\StoreCore\Google\People\Name::class)]
#[CoversClass(\StoreCore\Types\Date::class)]
#[CoversClass(\StoreCore\Types\DateTime::class)]
final class BookSeriesTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('BookSeries class is concrete')]
    public function testBookSeriesClassIsConcrete(): void
    {
        $class = new ReflectionClass(BookSeries::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    /**
     * @see https://schema.org/BookSeries#eg-0330
     *      Schema.org `BookSeries` example 1
     */
    #[Group('seo')]
    #[Test]
    #[TestDox('BookSeries acceptance test')]
    public function BookSeriesAcceptanceTest(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "BookSeries",
              "name": "The Hitchhiker’s Guide to the Galaxy",
              "genre": "comedy science fiction",
              "startDate": "1979-10-12",
              "endDate": "1992-10-12",
              "abstract": "Earthman Arthur Dent is saved by his friend, Ford Prefect—an alien researcher for the titular Hitchhiker’s Guide to the Galaxy, which provides info on every planet in the galaxy—from the Earth just before it is destroyed by the alien Vogons.",
              "author": {
                "@type": "Person",
                "name": "Douglas Adams"
              }
            }
        JSON;
        $this->assertJson($expectedJson);

        $bookSeries = new BookSeries('The Hitchhiker’s Guide to the Galaxy');
        $bookSeries->genre = 'comedy science fiction';
        $bookSeries->startDate = '1979-10-12';
        $bookSeries->endDate = '1992-10-12';
        $bookSeries->abstract = 'Earthman Arthur Dent is saved by his friend, Ford Prefect—an alien researcher for the titular Hitchhiker’s Guide to the Galaxy, which provides info on every planet in the galaxy—from the Earth just before it is destroyed by the alien Vogons.';

        $bookSeries->author = new Person();
        $bookSeries->author->givenName = 'Douglas';
        $bookSeries->author->familyName = 'Adams';

        $actualJson = json_encode($bookSeries, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
