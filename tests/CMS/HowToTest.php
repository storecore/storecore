<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Duration;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(HowTo::class)]
final class HowToTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('HowTo class exists')]
    public function testHowToClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'HowTo.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'HowTo.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\HowTo'));
        $this->assertTrue(class_exists(HowTo::class));
    }

    #[TestDox('HowTo class is concrete')]
    public function testHowToClassIsConcrete(): void
    {
        $class = new ReflectionClass(HowTo::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('HowTo is a CreativeWork Thing')]
    public function testHowToIsCreativeWorkThing(): void
    {
        $how_to = new HowTo();
        $this->assertInstanceOf(AbstractCreativeWork::class, $how_to);
        $this->assertInstanceOf(AbstractThing::class, $how_to);
    }

    #[Group('hmvc')]
    #[TestDox('HowTo is JSON serializable')]
    public function testHowToIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new HowTo());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(HowTo::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(HowTo::VERSION);
        $this->assertIsString(HowTo::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(HowTo::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('HowTo.name exists')]
    public function testHowToPrepNameExists(): void
    {
        $this->assertObjectHasProperty('name', new HowTo());
    }


    #[TestDox('HowTo.prepTime exists')]
    public function testHowToPrepTimeExists(): void
    {
        $this->assertObjectHasProperty('prepTime', new HowTo());
    }

    #[Depends('testHowToPrepTimeExists')]
    #[TestDox('HowTo.prepTime is public')]
    public function testHowToPrepTimeIsPublic(): void
    {
        $property = new ReflectionProperty(HowTo::class, 'prepTime');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testHowToPrepTimeExists')]
    #[TestDox('HowTo.prepTime is null by default')]
    public function testHowToPrepTimeIsNullByDefault(): void
    {
        $instructions = new HowTo();
        $this->assertNull($instructions->prepTime);
    }


    #[TestDox('HowTo.totalTime exists')]
    public function testHowToTotalTimeExists(): void
    {
        $howTo = new HowTo();
        $this->assertObjectHasProperty('totalTime', $howTo);
    }

    #[Depends('testHowToTotalTimeExists')]
    #[TestDox('HowTo.totalTime is public')]
    public function testHowToTotalTimeIsPublic(): void
    {
        $property = new ReflectionProperty(HowTo::class, 'totalTime');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testHowToTotalTimeExists')]
    #[TestDox('HowTo.totalTime is null by default')]
    public function testHowToTotalTimeIsNullByDefault(): void
    {
        $instructions = new HowTo();
        $this->assertNull($instructions->totalTime);
    }
}
