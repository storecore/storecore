<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CMS;

use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\CMS\Article::class)]
#[UsesClass(\StoreCore\CMS\BlogPosting::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class AbstractCreativeWorkTest extends TestCase
{
    #[TestDox('AbstractCreativeWork is abstract')]
    public function testAbstractCreativeWorkIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractCreativeWork::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[Depends('testAbstractCreativeWorkIsAbstract')]
    #[TestDox('AbstractCreativeWork is an AbstractThing')]
    public function testAbstractCreativeWorkIsAbstractThing(): void
    {
        $class = new ReflectionClass(AbstractCreativeWork::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\CMS\AbstractThing::class));
    }


    #[TestDox('CreativeWork subtypes')]
    public function testCreativeWorkSubtypes(): void
    {
        $types = [
            'ThreeDModel',

            'AboutPage',
            'AdvertiserContentArticle',
            'AmpStory',
            'AnalysisNewsArticle',
            'Answer',
            'APIReference',
            'ArchiveComponent',
            'Article',
            'AskPublicNewsArticle',
            'Atlas',
            'Audiobook',
            'AudioObject',
            'AudioObjectSnapshot',

            'BackgroundNewsArticle',
            'Barcode',
            'Blog',
            'BlogPosting',
            'Book',
            'BookSeries',

        //  'CategoryCodeSet',
            'Chapter',
            'CheckoutPage',
            'Claim',
            'ClaimReview',
            'Clip',
            'Code',
            'Collection',
            'CollectionPage',
            'Comment',
            'ComicCoverArt',
            'ComicIssue',
            'ComicSeries',
            'ComicStory',
            'CompleteDataFeed',
            'ContactPage',
            'Conversation',
            'CorrectionComment',
            'Course',
            'CoverArt',
            'CreativeWorkSeason',
            'CreativeWorkSeries',
            'CriticReview',

            'DataCatalog',
            'DataDownload',
            'DataFeed',
            'Dataset',
            'DefinedTermSet',
            'Diet',
            'DigitalDocument',
            'DiscussionForumPosting',
            'Drawing',

            'EmailMessage',
            'EmployerReview',
            'Episode',
            'EducationalOccupationalCredential',
            'ExercisePlan',

            'FAQPage',

            'Game',
            'Guide',

            'HealthTopicContent',
            'HowTo',
            'HowToDirection',
            'HowToSection',
            'HowToStep',
            'HowToTip',
            'HyperToc',
            'HyperTocEntry',

            'ImageGallery',
            'ImageObjectSnapshot',
            'ItemPage',

            'LearningResource',
            'Legislation',
            'LegislationObject',
            'LiveBlogPosting',

            'Manuscript',
            'Map',
            'MathSolver',
            'MediaGallery',
            'MediaReview',
            'MediaReviewItem',
            'MedicalScholarlyArticle',
            'MedicalWebPage',
            'Menu',
            'MenuSection',
            'Message',
            'MobileApplication',
            'Movie',
            'MovieClip',
            'MovieSeries',
            'MusicAlbum',
            'MusicComposition',
            'MusicPlaylist',
            'MusicRecording',
            'MusicRelease',
            'MusicVideoObject',

            'NewsArticle',
            'Newspaper',
            'NoteDigitalDocument',

            'OpinionNewsArticle',

            'Painting',
            'Periodical',
            'Photograph',
            'Play',
            'PodcastEpisode',
            'PodcastSeason',
            'PodcastSeries',
            'Poster',
            'PresentationDigitalDocument',
        //  'ProductCollection',
            'ProfilePage',
            'PublicationIssue',
            'PublicationVolume',

            'QAPage',
            'Question',
            'Quiz',
            'Quotation',

            'RadioClip',
            'RadioEpisode',
            'RadioSeason',
            'RadioSeries',
            'RealEstateListing',
            'Recommendation',
            'Recipe',
            'Report',
            'ReportageNewsArticle',
            'Review',
            'ReviewNewsArticle',

            'SatiricalArticle',
            'ScholarlyArticle',
            'Sculpture',
            'SearchResultsPage',
            'SheetMusic',
            'ShortStory',
            'SiteNavigationElement',
            'SocialMediaPosting',
            'SoftwareApplication',
            'SoftwareSourceCode',
            'SpecialAnnouncement',
            'SpreadsheetDigitalDocument',
            'Statement',

            'Table',
            'TechArticle',
            'TextDigitalDocument',
            'Thesis',
            'TVClip',
            'TVEpisode',
            'TVSeason',
            'TVSeries',

            'UserReview',

            'VideoGallery',
            'VideoGame',
            'VideoGameClip',
            'VideoGameSeries',
            'VideoObject',
            'VideoObjectSnapshot',
            'VisualArtwork',

            'WebApplication',
            'WebContent',
            'WebPage',
            'WebPageElement',
            'WebSite',
            'WPAdBlock',
            'WPFooter',
            'WPHeader',
            'WPSideBar',
        ];

        foreach ($types as $type) {
            $this->assertTrue(
                class_exists('\\StoreCore\\CMS\\' . $type),
                'Missing CreativeWork subtype: ' . $type
            );
        }
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractCreativeWork::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(AbstractCreativeWork::VERSION);
        $this->assertIsString(AbstractCreativeWork::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(AbstractCreativeWork::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreativeWork.author exists')]
    public function testCreativeWorkAuthorExists(): void
    {
        $creativeWork = new ReflectionClass(AbstractCreativeWork::class);
        $this->assertTrue($creativeWork->hasProperty('author'));
    }

    #[Depends('testCreativeWorkAuthorExists')]
    #[TestDox('CreativeWork.author is null by default')]
    public function testCreativeWorkAuthorIsNullByDefault(): void
    {
        $article = new Article();
        $this->assertInstanceOf(AbstractCreativeWork::class, $article);
        $this->assertNull($article->author);
    }

    #[Depends('testCreativeWorkAuthorIsNullByDefault')]
    #[TestDox('CreativeWork.author accepts Person')]
    public function testCreativeWorkAuthorAcceptsPerson(): void
    {
        $article = new Article();
        $article->author = new Person('John Doe');
        $this->assertNotNull($article->author);
        $this->assertIsObject($article->author);
        $this->assertEquals('John Doe', $article->author->name);

        $json = json_encode($article);
        $this->assertJson($json);
        $this->assertStringContainsString('"author":{"@type":"Person","name":"John Doe"}', $json);
    }

    #[Depends('testCreativeWorkAuthorIsNullByDefault')]
    #[Depends('testCreativeWorkAuthorAcceptsPerson')]
    #[TestDox('CreativeWork.author accepts Person.name')]
    public function testCreativeWorkAuthorAcceptsPersonName(): void
    {
        $article = new Article();
        $article->author = 'Jane Roe';
        $this->assertNotNull($article->author);
        $this->assertIsNotString($article->author);
        $this->assertIsObject($article->author);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $article->author);
        $this->assertEquals('Jane Roe', $article->author->name);

        $json = json_encode($article);
        $this->assertJson($json);
        $this->assertStringContainsString('"author":{"@type":"Person","name":"Jane Roe"}', $json);
    }


    #[TestDox('CreativeWork.expires exists')]
    public function testCreativeWorkExpiresExists(): void
    {
        $creativeWork = new class extends AbstractCreativeWork {};
        $this->assertObjectHasProperty('expires', $creativeWork);
    }

    #[Depends('testCreativeWorkExpiresExists')]
    #[TestDox('CreativeWork.expires is null by default')]
    public function testCreativeWorkExpiresIsNullByDefault(): void
    {
        $blogPosting = new BlogPosting();
        $this->assertInstanceOf(AbstractCreativeWork::class, $blogPosting);
        $this->assertNull($blogPosting->expires);
    }

    #[Depends('testCreativeWorkExpiresIsNullByDefault')]
    #[Group('seo')]
    #[TestDox('CreativeWork.expires accepts DateTimeInterface')]
    public function testCreativeWorkExpiresacceptsDateTimeInterface(): void
    {
        $blogPosting = new BlogPosting();
        $blogPosting->expires = new \DateTime('tomorrow', new \DateTimeZone('UTC'));
        $this->assertNotNull($blogPosting->expires);
        $this->assertInstanceOf(\DateTimeInterface::class, $blogPosting->expires);

        $blogPosting->expires = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
        $this->assertInstanceOf(\DateTimeInterface::class, $blogPosting->expires);

        $blogPosting->expires = new \DateTime('2025-04-01 17:00', new \DateTimeZone('Europe/Amsterdam'));
        $json = json_encode($blogPosting, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"expires":"2025-04-01 17:00:00+02:00"', $json);
    }


    #[Group('seo')]
    #[TestDox('CreativeWork.keywords exists')]
    public function testCreativeWorkKeywordsExists(): void
    {
        $creativeWork = new class extends AbstractCreativeWork {};
        $this->assertObjectHasProperty('keywords', $creativeWork);
    }

    #[Depends('testCreativeWorkKeywordsExists')]
    #[Group('seo')]
    #[TestDox('CreativeWork.keywords is public')]
    public function testCreativeWorkKeywordsIsPublic(): void
    {
        $property = new ReflectionProperty(AbstractCreativeWork::class, 'keywords');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }
}
