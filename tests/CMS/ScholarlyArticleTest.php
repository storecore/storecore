<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class ScholarlyArticleTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ScholarlyArticle class exists')]
    public function testScholarlyArticleClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ScholarlyArticle.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ScholarlyArticle.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ScholarlyArticle', false));
        $this->assertTrue(class_exists(ScholarlyArticle::class));
    }


    #[TestDox('ScholarlyArticle class is concrete')]
    public function testScholarlyArticleClassIsConcrete(): void
    {
        $class = new ReflectionClass(ScholarlyArticle::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ScholarlyArticle is an Article')]
    public function testScholarlyArticleIsArticle(): void
    {
        $this->assertInstanceOf(Article::class, new ScholarlyArticle());
    }

    #[Group('hmvc')]
    #[TestDox('ScholarlyArticle is a CreativeWork Thing')]
    public function testScholarlyArticleIsCreativeWorkThing(): void
    {
        $article = new ScholarlyArticle();
        $this->assertInstanceOf(AbstractCreativeWork::class, $article);
        $this->assertInstanceOf(AbstractThing::class, $article);
    }

    #[Group('hmvc')]
    #[TestDox('MedicalScholarlyArticle subtype exists')]
    public function testMedicalScholarlyArticleSubtypeExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\MedicalScholarlyArticle'));
        $this->assertInstanceOf(ScholarlyArticle::class, new MedicalScholarlyArticle());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ScholarlyArticle::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ScholarlyArticle::VERSION);
        $this->assertIsString(ScholarlyArticle::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ScholarlyArticle::VERSION, '1.0.0', '>=')
        );
    }
}
