<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Person;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\CMS\ImageObject::class)]
#[UsesClass(\StoreCore\CMS\MediaObject::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Types\PropertyValue::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class ImageObjectTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ImageObject class exists')]
    public function testImageObjectClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ImageObject.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'ImageObject.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ImageObject'));
        $this->assertTrue(class_exists(ImageObject::class));
    }

    #[TestDox('ImageObject class is concrete')]
    public function testImageObjectClassIsConcrete(): void
    {
        $class = new ReflectionClass(ImageObject::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ImageObject is a MediaObject')]
    public function testImageObjectIsMediaObject(): void
    {
        $this->assertInstanceOf(MediaObject::class, new ImageObject());
    }

    #[Group('hmvc')]
    #[TestDox('ImageObject is a CreativeWork Thing')]
    public function testImageObjectIsCreativeWorkThing(): void
    {
        $image = new ImageObject();
        $this->assertInstanceOf(AbstractCreativeWork::class, $image);
        $this->assertInstanceOf(AbstractThing::class, $image);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ImageObject::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ImageObject::VERSION);
        $this->assertIsString(ImageObject::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ImageObject::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ImageObject.caption exists')]
    public function testImageObjectCaptionExists(): void
    {
        $this->assertObjectHasProperty('caption', new ImageObject());
    }

    #[Depends('testImageObjectCaptionExists')]
    #[TestDox('ImageObject.caption is public')]
    public function testImageObjectCaptionIsPublic(): void
    {
        $property = new ReflectionProperty(ImageObject::class, 'caption');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testImageObjectCaptionExists')]
    #[TestDox('ImageObject.caption is empty by default')]
    public function testImageObjectCaptionIsEmptyByDefault(): void
    {
        $image = new ImageObject();
        $this->assertEmpty($image->caption);
    }

    #[Depends('testImageObjectCaptionExists')]
    #[Depends('testImageObjectCaptionIsEmptyByDefault')]
    #[TestDox('ImageObject.caption accepts string')]
    public function testImageObjectCaptionAcceptsString(): void
    {
        $image = new ImageObject();
        $image->caption = 'Beach in Mexico';
        $this->assertNotEmpty($image->caption);
        $this->assertIsString($image->caption);
    }


    #[TestDox('ImageObject.embeddedTextCaption exists')]
    public function testImageObjectEmbeddedTextCaptionExists(): void
    {
        $this->assertObjectHasProperty('embeddedTextCaption', new ImageObject());
    }

    #[Depends('testImageObjectEmbeddedTextCaptionExists')]
    #[TestDox('ImageObject.embeddedTextCaption is public')]
    public function testImageObjectEmbeddedTextCaptionIsPublic(): void
    {
        $property = new ReflectionProperty(ImageObject::class, 'embeddedTextCaption');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testImageObjectEmbeddedTextCaptionExists')]
    #[TestDox('ImageObject.embeddedTextCaption is empty by default')]
    public function testImageObjectEmbeddedTextCaptionIsEmptyByDefault(): void
    {
        $image = new ImageObject();
        $this->assertEmpty($image->embeddedTextCaption);
    }

    #[Depends('testImageObjectEmbeddedTextCaptionIsEmptyByDefault')]
    #[TestDox('ImageObject.embeddedTextCaption accepts string')]
    public function testImageObjectEmbeddedTextCaptionAcceptsString(): void
    {
        $image = new ImageObject();
        $image->embeddedTextCaption = 'Luck is what happens when preparation meets opportunity.';
        $this->assertNotEmpty($image->embeddedTextCaption);
        $this->assertIsString($image->embeddedTextCaption);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/image-license-metadata#single-image
     *      Structured data example for a single image in “Image metadata in Google Images”
     */
    #[Group('seo')]
    #[TestDox('Google Images SEO image metadata acceptance test')]
    public function testGoogleImagesSeoImageMetadataAcceptanceTest(): void
    {
        $required_properties = [
            'contentUrl',
            'creator',
            'creditText',
            'copyrightNotice',
            'license',
        ];

        foreach ($required_properties as $property) {
            $this->assertObjectHasProperty(
                $property, 
                new ImageObject(),
                'Required property `ImageObject.' . $property . '` is missing.'
            );
        }


        $recommended_properties = [
            'acquireLicensePage',
            'creator',
            'creditText',
            'copyrightNotice',
            'license',
        ];

        foreach ($recommended_properties as $property) {
            $this->assertObjectHasProperty(
                $property,
                new ImageObject(),
                'Recommended property `ImageObject.' . $property . '` is missing.'
            );
        }


        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "ImageObject",
              "contentUrl": "https://example.com/photos/1x1/black-labrador-puppy.jpg",
              "license": "https://example.com/license",
              "acquireLicensePage": "https://example.com/how-to-use-my-images",
              "creditText": "Labrador PhotoLab",
              "creator": {
                "@type": "Person",
                "name": "Brixton Brownstone"
               },
              "copyrightNotice": "Clara Kent"
            }
        JSON;

        $image = new ImageObject();

        $this->assertTrue(class_exists(URL::class));
        $image->contentUrl = new URL('https://example.com/photos/1x1/black-labrador-puppy.jpg');
        $this->assertNotEmpty($image->contentUrl);

        $image->license = new URL('https://example.com/license');
        $this->assertNotEmpty($image->license);

        $image->acquireLicensePage = new URL('https://example.com/how-to-use-my-images');
        $this->assertNotEmpty($image->acquireLicensePage);

        $this->assertEmpty($image->creditText);
        $image->creditText = 'Labrador PhotoLab';
        $this->assertNotEmpty($image->creditText);

        $this->assertTrue(class_exists(Person::class));
        $image->creator = new Person('Brixton Brownstone');
        $this->assertNotEmpty($image->creator->name);

        $this->assertEmpty($image->copyrightNotice);
        $image->copyrightNotice = 'Clara Kent';
        $this->assertNotEmpty($image->copyrightNotice);
        $this->assertIsString($image->copyrightNotice);

        $actualJson = json_encode($image, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
