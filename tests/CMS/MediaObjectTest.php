<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Date;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\MediaObject::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
final class MediaObjectTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('MediaObject class exists')]
    public function testMediaObjectClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'MediaObject.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'MediaObject.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\MediaObject'));
        $this->assertTrue(class_exists(MediaObject::class));
    }

    #[TestDox('MediaObject class is concrete')]
    public function testMediaObjectClassIsConcrete(): void
    {
        $class = new ReflectionClass(MediaObject::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MediaObject is a CreativeWork Thing')]
    public function testMediaObjectIsCreativeWorkThing()
    {
        $media = new MediaObject();
        $this->assertInstanceOf(AbstractCreativeWork::class, $media);
        $this->assertInstanceOf(AbstractThing::class, $media);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MediaObject::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MediaObject::VERSION);
        $this->assertIsString(MediaObject::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MediaObject::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('MediaObject.associatedArticle exists')]
    public function testMediaObjectAssociatedArticleExists(): void
    {
        $this->assertObjectHasProperty('associatedArticle', new MediaObject());
    }

    #[Depends('testMediaObjectAssociatedArticleExists')]
    #[TestDox('MediaObject.associatedArticle is public')]
    public function testMediaObjectAssociatedArticleIsPublic(): void
    {
        $property = new ReflectionProperty(MediaObject::class, 'associatedArticle');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMediaObjectAssociatedArticleExists')]
    #[TestDox('MediaObject.associatedArticle is null by default')]
    public function testMediaObjectAssociatedArticleIsNullByDefault(): void
    {
        $media = new MediaObject();
        $this->assertNull($media->associatedArticle);
    }


    #[TestDox('MediaObject.sha256 exists')]
    public function testMediaObjectCaptionExists(): void
    {
        $media = new MediaObject();
        $this->assertObjectHasProperty('sha256', $media);
    }

    #[Depends('testMediaObjectCaptionExists')]
    #[TestDox('MediaObject.sha256 is public')]
    public function testMediaObjectCaptionIsPublic(): void
    {
        $property = new ReflectionProperty(MediaObject::class, 'sha256');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMediaObjectCaptionExists')]
    #[TestDox('MediaObject.sha256 is null by default')]
    public function testMediaObjectCaptionIsNullByDefault(): void
    {
        $media = new MediaObject();
        $this->assertNull($media->sha256);
    }

    #[Depends('testMediaObjectCaptionExists')]
    #[Depends('testMediaObjectCaptionIsPublic')]
    #[Depends('testMediaObjectCaptionIsNullByDefault')]
    #[TestDox('MediaObject.sha256 accepts string')]
    public function testMediaObjectCaptionAcceptsString(): void
    {
        $media = new MediaObject();
        $media->sha256 = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';
        $this->assertNotNull($media->sha256);
        $this->assertNotEmpty($media->sha256);
        $this->assertIsString($media->sha256);
    }

    
    #[TestDox('MediaObject.uploadDate exists')]
    public function testMediaObjectUploadDateExists(): void
    {
        $this->assertObjectHasProperty('uploadDate', new MediaObject());
    }

    #[Depends('testMediaObjectUploadDateExists')]
    #[TestDox('MediaObject.uploadDate is not public')]
    public function testMediaObjectUploadDateIsNotPublic(): void
    {
        $property = new ReflectionProperty(MediaObject::class, 'uploadDate');
        $this->assertTrue($property->isProtected());
        $this->assertFalse($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testMediaObjectUploadDateExists')]
    #[TestDox('MediaObject.uploadDate accepts stringable Date')]
    public function testMediaObjectUploadDateAcceptsStringableDate(): void
    {
        $today = new Date('now');
        $media = new MediaObject();
        $media->uploadDate = $today;
        $this->assertInstanceOf(\Stringable::class, $media->uploadDate);
        $this->assertInstanceOf(\StoreCore\Types\Date::class, $media->uploadDate);
        $this->assertSame($today, $media->uploadDate);
    }

    #[Depends('testMediaObjectUploadDateExists')]
    #[TestDox('MediaObject.uploadDate can be set to \DateInterface')]
    public function testMediaObjectUploadDateCanBeSetToDateInterface(): void
    {
        $media = new MediaObject();
        $media->uploadDate = new \DateTimeImmutable('now');
        $this->assertInstanceOf(\DateTimeInterface::class, $media->uploadDate);
        $this->assertEquals( gmdate('Y-m-d'), $media->uploadDate->format('Y-m-d'));
    }

    #[Depends('testMediaObjectUploadDateCanBeSetToDateInterface')]
    #[TestDox('MediaObject.uploadDate is NOT a DateTime but a Date')]
    public function testMediaObjectUploadDateIsNotDateTimeButDate(): void
    {
        $media = new MediaObject();
        $media->uploadDate = new \DateTimeImmutable('now');
        $this->assertNotInstanceOf(\DateTimeImmutable::class, $media->uploadDate);
        $this->assertInstanceOf(\StoreCore\Types\Date::class, $media->uploadDate);
    }

    #[Depends('testMediaObjectUploadDateExists')]
    #[TestDox('MediaObject.uploadDate can be set to date string')]
    public function testMediaObjectUploadDateCanBeSetToDateString(): void
    {
        $media = new MediaObject();
        $media->uploadDate = '2024-06-13';
        $this->assertInstanceOf(\DateTimeInterface::class, $media->uploadDate);
    }
}
