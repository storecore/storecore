<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class HowToStepTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('HowToStep class exists')]
    public function testHowToStepClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'HowToStep.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'HowToStep.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\CMS\\HowToStep'));
        $this->assertTrue(class_exists(HowToStep::class));
    }

    #[TestDox('HowToStep class is concrete')]
    public function testHowToStepClassIsConcrete(): void
    {
        $class = new ReflectionClass(HowToStep::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('HowToStep is a CreativeWork Thing')]
    public function testHowToStepIsCreativeWorkThing(): void
    {
        $step = new HowToStep();
        $this->assertInstanceOf(AbstractCreativeWork::class, $step);
        $this->assertInstanceOf(AbstractThing::class, $step);
    }

    #[Group('hmvc')]
    #[TestDox('HowToStep is JSON serializable')]
    public function testHowToStepIsJsonSerializable(): void
    {
        $step = new HowToStep();
        $this->assertInstanceOf(\JsonSerializable::class, $step);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(HowToStep::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(HowToStep::VERSION);
        $this->assertIsString(HowToStep::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(HowToStep::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('HowToStep.itemListElement exists')]
    public function testHowToStepItemListElementExists(): void
    {
        $howToStep = new HowToStep();
        $this->assertObjectHasProperty('itemListElement', $howToStep);
    }

    #[Depends('testHowToStepItemListElementExists')]
    #[TestDox('HowToStep.itemListElement is empty by default')]
    public function testHowToStepItemListElementIsEmptyByDefault(): void
    {
        $step = new HowToStep();
        $this->assertEmpty($step->itemListElement);
    }


    #[TestDox('HowToStep.position exists')]
    public function testHowToStepPositionExists(): void
    {
        $howToStep = new HowToStep();
        $this->assertObjectHasProperty('position', $howToStep);
    }

    #[Depends('testHowToStepPositionExists')]
    #[TestDox('HowToStep.position is null by default')]
    public function testHowToStepPositionIsNullByDefault(): void
    {
        $step = new HowToStep();
        $this->assertNull($step->position);
    }

    #[Depends('testHowToStepPositionExists')]
    #[Depends('testHowToStepPositionIsNullByDefault')]
    #[TestDox('HowToStep.position accepts integer')]
    public function testHowToStepPositionAcceptsInteger(): void
    {
        $step = new HowToStep();
        $step->position = 2;

        $this->assertNotNull($step->position);
        $this->assertIsInt($step->position);
    }
}
