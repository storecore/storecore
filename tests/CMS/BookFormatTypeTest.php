<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use \ReflectionClass, \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class BookFormatTypeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('BookFormatType enumeration exists')]
    public function testBookFormatTypeEnumerationExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'BookFormatType.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR . 'BookFormatType.php'
        );

        $this->assertTrue(enum_exists('\\StoreCore\\CMS\\BookFormatType'));
        $this->assertTrue(enum_exists(BookFormatType::class));
    }


    #[TestDox('BookFormatType is an enumeration')]
    public function testBookFormatTypeIsEnumeration(): void
    {
        $class = new ReflectionClass(BookFormatType::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testBookFormatTypeIsEnumeration')]
    #[TestDox('BookFormatType is a backed enumeration')]
    public function testBookFormatTypeIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(BookFormatType::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BookFormatType::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BookFormatType::VERSION);
        $this->assertIsString(BookFormatType::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org release')]
    public function testVersionMatchesSchemaOrgRelease(): void
    {
        $this->assertTrue(
            version_compare(BookFormatType::VERSION, '27.0', '>=')
        );
    }
}
