<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CMS;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractHTMLElementTest extends TestCase
{
    #[TestDox('AbstractHTMLElement class is abstract')]
    public function testAbstractHTMLElementClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractHTMLElement::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractHTMLElement is an AbstractElement')]
    public function testAbstractHTMLElementIsAbstractElement(): void
    {
        $HTMLElement = new class extends AbstractHTMLElement {};
        $this->assertInstanceOf(AbstractElement::class, $HTMLElement);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractHTMLElement::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AbstractHTMLElement::VERSION);
        $this->assertIsString(AbstractHTMLElement::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AbstractHTMLElement::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
