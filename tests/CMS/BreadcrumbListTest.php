<?php

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\{ItemList, ListItem};

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CMS\BreadcrumbList::class)]
final class BreadcrumbListTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('BreadcrumbList class exists')]
    public function testBreadcrumbListClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR .  'BreadcrumbList.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'CMS' . DIRECTORY_SEPARATOR .  'BreadcrumbList.php');
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\BreadcrumbList'));
        $this->assertTrue(class_exists(BreadcrumbList::class));
    }

    #[TestDox('BreadcrumbList class is concrete')]
    public function testBreadcrumbListClassIsConcrete(): void
    {
        $class = new ReflectionClass(BreadcrumbList::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('BreadcrumbList class is an Intangible Thing')]
    public function testBreadcrumbListIsIntangibleThing(): void
    {
        $list = new BreadcrumbList();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $list);
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('BreadcrumbList is an ItemList')]
    public function testBreadcrumbListIsItemList(): void
    {
        $list = new BreadcrumbList();
        $this->assertInstanceOf(ItemList::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('BreadcrumbList implements PHP ArrayAccess interface')]
    public function testBreadcrumbListImplementsPhpArrayAccessInterface(): void
    {
        $list = new BreadcrumbList();
        $this->assertInstanceOf(\ArrayAccess::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('BreadcrumbList implements PHP Countable interface')]
    public function testBreadcrumbListImplementsPhpCountableInterface(): void
    {
        $list = new BreadcrumbList();
        $this->assertInstanceOf(\Countable::class, $list);
    }

    #[Group('hmvc')]
    #[TestDox('BreadcrumbList is JSON serializable')]
    public function testBreadcrumbListIsJsonSerializable(): void
    {
        $list = new BreadcrumbList();
        $this->assertInstanceOf(\JsonSerializable::class, $list);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BreadcrumbList::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BreadcrumbList::VERSION);
        $this->assertIsString(BreadcrumbList::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BreadcrumbList::VERSION, '0.1.0', '>=')
        );
    }
}
