<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Person;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\{Rating, Thing};

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[CoversClass(\StoreCore\CMS\AbstractThing::class)]
#[CoversClass(\StoreCore\CMS\Review::class)]
#[CoversClass(\StoreCore\Types\Rating::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class ReviewTest extends TestCase
{
    #[TestDox('Review class is concrete')]
    public function testReviewClassIsConcrete(): void
    {
        $class = new ReflectionClass(Review::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Review is a CreativeWork Thing')]
    public function testReviewIsACreativeWorkThing(): void
    {
        $review = new Review();

        $this->assertTrue(class_exists(\StoreCore\CMS\AbstractCreativeWork::class));
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $review);

        $this->assertTrue(class_exists(AbstractCreativeWork::class));
        $this->assertInstanceOf(AbstractCreativeWork::class, $review);

        $this->assertTrue(class_exists(\StoreCore\CMS\AbstractThing::class));
        $this->assertInstanceOf(\StoreCore\CMS\AbstractThing::class, $review);

        $this->assertTrue(class_exists(AbstractThing::class));
        $this->assertInstanceOf(AbstractThing::class, $review);
    }

    #[Group('seo')]
    #[TestDox('Review is JSON serializable')]
    public function testReviewIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Review());
    }

    #[TestDox('Review implements IdentityInterface')]
    public function testReviewImplementsIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Review());
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Review subtypes')]
    public function testReviewSubtypes(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\EmployerReview', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\CriticReview', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\EmployerReview', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\MediaReview', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\Recommendation', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\ReviewNewsArticle', true));
        $this->assertTrue(class_exists('\\StoreCore\\CMS\\UserReview', true));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Review::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Review::VERSION);
        $this->assertIsString(Review::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Review::VERSION, '0.4.0', '>=')
        );
    }


    #[TestDox('Review.author exists')]
    public function testReviewAuthorExists(): void
    {
        $review = new Review();
        $this->assertObjectHasProperty('author', $review);
    }

    #[Depends('testReviewAuthorExists')]
    #[TestDox('Review.author exists')]
    public function testReviewAuthorIsPublic(): void
    {
        $property = new ReflectionProperty(Review::class, 'author');
        $this->assertFalse($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product#product-review-page-example
     */
    #[Depends('testReviewAuthorIsPublic')]
    #[TestDox('Review.author accepts Person')]
    public function testReviewAuthorAcceptsPerson(): void
    {
        $review = new Review();
        $this->assertNull($review->author);

        $person = new Person();
        $person->name = 'Fred Benson';

        $review->author = $person;
        $this->assertSame('Fred Benson', (string) $review->author->name);
    }

    /**
     * @see https://schema.org/Review#eg-0010 Schema.org `Review` example 1
     *   `Review.author` (inherited from `CreativeWork.author`) SHOULD be
     *   an `Organization` or `Person`.  The Schema.org examples often use
     *   a string, but this is an invalid object type that is reported as
     *   a non-critical issue by the Google Rich Results Test.
     */
    #[Depends('testReviewAuthorAcceptsPerson')]
    #[TestDox('Review.author accepts string as Person.name')]
    public function testReviewAuthorAcceptsStringAsPersonName(): void
    {
        $review = new Review();
        $this->assertNull($review->author);

        // When we set the `Review.author` to a `string` …
        $review->author = 'Ellie';
        $this->assertNotNull($review->author);

        // … then the `author` is no longer a string …
        $this->assertIsNotString($review->author);

        // … but an `object` of type `Person` …
        $this->assertIsObject($review->author);
        $this->assertInstanceOf(Person::class, $review->author);

        // … with a `Stringable` `name`:
        $this->assertInstanceOf(\Stringable::class, $review->author->name);
        $this->assertSame('Ellie', (string) $review->author->name);
    }


    #[TestDox('Review.itemReviewed exists')]
    public function testReviewItemReviewedExists(): void
    {
        $this->assertObjectHasProperty('itemReviewed', new Review());
    }

    #[Depends('testReviewItemReviewedExists')]
    #[TestDox('Review.itemReviewed is public')]
    public function testReviewItemReviewedIsPublic(): void
    {
        $property = new ReflectionProperty(Review::class, 'itemReviewed');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testReviewItemReviewedExists')]
    #[TestDox('Review.itemReviewed is null by default')]
    public function testReviewItemReviewedIsNullByDefault(): void
    {
        $review = new Review();
        $this->assertNull($review->itemReviewed);
    }

    #[Depends('testReviewItemReviewedExists')]
    #[TestDox('Review.itemReviewed accepts Thing')]
    public function testReviewItemReviewedAcceptsThing(): void
    {
        $thing = new Thing();
        $thing->name = 'Sampo';

        $review = new Review();
        $review->itemReviewed = $thing;
        $this->assertInstanceOf(Thing::class, $review->itemReviewed);
        $this->assertSame('Sampo', $review->itemReviewed->name);
    }


    /**
     * @see https://schema.org/reviewBody
     *      Schema.org `reviewBody` property of a Review
     */
    #[TestDox('Review.reviewBody exists')]
    public function testReviewReviewBodyExists(): void
    {
        $this->assertObjectHasProperty('reviewBody', new Review());
    }

    #[Depends('testReviewReviewBodyExists')]
    #[TestDox('Review.reviewBody is public')]
    public function testReviewReviewBodyIsPublic(): void
    {
        $property = new ReflectionProperty(Review::class, 'reviewBody');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testReviewReviewBodyExists')]
    #[TestDox('Review.reviewBody is null by default')]
    public function testReviewReviewBodyIsNullByDefault(): void
    {
        $review = new Review();
        $this->assertNull($review->reviewBody);
    }


    #[TestDox('Review.reviewRating exists')]
    public function testReviewReviewRatingExists(): void
    {
        $this->assertObjectHasProperty('reviewRating', new Review());
    }

    #[Depends('testReviewReviewRatingExists')]
    #[TestDox('Review.reviewRating is public')]
    public function testReviewReviewRatingIsPublic(): void
    {
        $property = new ReflectionProperty(Review::class, 'reviewRating');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testReviewReviewRatingExists')]
    #[TestDox('Review.reviewRating is null by default')]
    public function testReviewReviewRatingIsNullByDefault(): void
    {
        $review = new Review();
        $this->assertNull($review->itemReviewed);
    }

    /**
     * @see https://schema.org/Review#eg-0010
     */
    #[TestDox('Reviews with a reviewRating')]
    public function testReviewsWithReviewRating(): void
    {
        /*
            ```json
            {
              "@type": "Review",
              "author": "Ellie",
              "datePublished": "2011-04-01",
              "reviewBody": "The lamp burned out and now I have to replace it.",
              "name": "Not a happy camper",
              "reviewRating": {
                "@type": "Rating",
                "bestRating": "5",
                "ratingValue": "1",
                "worstRating": "1"
              }
            }
            ```
         */
        $review = new Review();
        $review->author = 'Ellie';
        $review->datePublished = '2011-04-01';
        $review->reviewBody = 'The lamp burned out and now I have to replace it.';
        $review->name = 'Not a happy camper';
        $review->reviewRating = new Rating(1);

        $this->assertInstanceOf(Rating::class, $review->reviewRating);
        $this->assertEquals(5, $review->reviewRating->bestRating);
        $this->assertEquals(1, $review->reviewRating->ratingValue);
        $this->assertEquals(1, $review->reviewRating->worstRating);

        /*
            ```json
            {
              "@type": "Review",
              "author": "Lucas",
              "datePublished": "2011-03-25",
              "reviewBody": "Great microwave for the price. It is small and fits in my apartment.",
              "name": "Value purchase",
              "reviewRating": {
                "@type": "Rating",
                "bestRating": "5",
                "ratingValue": "4,
                "worstRating": "1"
              }
            }
            ```
         */
        $review = new Review();
        $review->author = 'Lucas';
        $review->datePublished = '2011-03-25';
        $review->reviewBody = 'Great microwave for the price. It is small and fits in my apartment.';
        $review->name = 'Value purchase';
        $review->reviewRating = new Rating(4);

        $this->assertInstanceOf(Rating::class, $review->reviewRating);
        $this->assertEquals(5, $review->reviewRating->bestRating);
        $this->assertEquals(4, $review->reviewRating->ratingValue);
        $this->assertEquals(1, $review->reviewRating->worstRating);
    }
}
