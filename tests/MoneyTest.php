<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\Currency;
use StoreCore\Database\Currencies;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

#[CoversClass(\StoreCore\Money::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Database\Currencies::class)]
final class MoneyTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Money class is concrete')]
    public function testMoneyClassIsConcrete(): void
    {
        $class = new ReflectionClass(Money::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testMoneyClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('Money class is read-only')]
    public function testMoneyClassIsReadOnly(): void
    {
        $class = new ReflectionClass(Money::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('Money class is stringable')]
    public function testMoneyClassIsStringable(): void
    {
        $money = new Money(599);
        $this->assertInstanceOf(\Stringable::class, $money);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Money::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Money::VERSION);
        $this->assertIsString(Money::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches master branch')]
    public function testVersionConstantMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Money::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('Money::__construct exists')]
    public function testMoneyConstructorExists(): void
    {
        $class = new ReflectionClass(Money::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testMoneyConstructorExists')]
    #[TestDox('Money::__construct is public constructor')]
    public function testMoneyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Money::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testMoneyConstructIsPublicConstructor')]
    #[TestDox('Money::__construct has two parameters')]
    public function testMoneyConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Money::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testMoneyConstructorHasTwoParameters')]
    #[TestDox('Money::__construct has one REQUIRED parameter')]
    public function testMoneyConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Money::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMoneyConstructorHasOneRequiredParameter')]
    #[TestDox('Money::__construct accepts float')]
    public function testMoneyConstructorAcceptsFloat(): void
    {
        $amount = 99.95;
        $this->assertIsFloat($amount);

        $money = new Money($amount);
        $this->assertIsFloat($money->getAmount());
        $this->assertEquals(99.95, $money->getAmount());
        $this->assertEquals('99.95', (string) $money);
    }

    #[Depends('testMoneyConstructorHasOneRequiredParameter')]
    #[TestDox('Money::__construct accepts integer')]
    public function testMoneyConstructorAcceptsInteger(): void
    {
        $amount = 599;
        $this->assertIsInt($amount);

        $money = new Money($amount);
        $this->assertIsFloat($money->getAmount());
        $this->assertEquals(599.00, $money->getAmount());
        $this->assertEquals('599.00', (string) $money);
    }


    #[Depends('testMoneyConstructorAcceptsFloat')]
    #[Depends('testMoneyConstructorAcceptsInteger')]
    #[TestDox('Money is JSON serializable')]
    public function testMoneyIsJsonSerializable(): void
    {
        $money = new Money(69.95);
        $this->assertInstanceOf(\JsonSerializable::class, $money);

        $json = json_encode($money);
        $this->assertNotEmpty($json);
        $this->assertJson($json);
        $this->assertEquals('"EUR 69.95"', $json);

        $pound = Currencies::createFromString('GBP');
        $money = new Money(3, $pound);
        $this->assertEquals('"GBP 3.00"', json_encode($money));
    }
}
