<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\LoggerFactory::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\FileSystem\Logger::class)]
class LoggerFactoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('LoggerFactory class is concrete')]
    public function testLoggerFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(LoggerFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LoggerFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LoggerFactory::VERSION);
        $this->assertIsString(LoggerFactory::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LoggerFactory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('LoggerFactory::createLogger exists')]
    public function testLoggerFactoryCreateLoggerExists(): void
    {
        $class = new ReflectionClass(LoggerFactory::class);
        $this->assertTrue($class->hasMethod('createLogger'));
    }

    #[Depends('testLoggerFactoryCreateLoggerExists')]
    #[TestDox('LoggerFactory::createLogger is public')]
    public function testLoggerFactoryCreateLoggerIsPublic(): void
    {
        $method = new ReflectionMethod(LoggerFactory::class, 'createLogger');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testLoggerFactoryCreateLoggerExists')]
    #[TestDox('LoggerFactory::createLogger has no parameters')]
    public function testLoggerFactoryCreateLoggerHasNoParameters(): void
    {
        $method = new ReflectionMethod(LoggerFactory::class, 'createLogger');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLoggerFactoryCreateLoggerIsPublic')]
    #[Depends('testLoggerFactoryCreateLoggerHasNoParameters')]
    #[TestDox('LoggerFactory::createLogger returns PSR-3 compliant logger')]
    public function testLoggerFactoryCreateLoggerReturnsPsr3CompliantLogger(): void
    {
        $factory = new LoggerFactory();
        $logger = $factory->createLogger();
        $this->assertInstanceOf(\Psr\Log\LoggerInterface::class, $logger);
    }

    #[Depends('testLoggerFactoryCreateLoggerReturnsPsr3CompliantLogger')]
    #[TestDox('LoggerFactory::createLogger returns file system logger by default')]
    public function testLoggerFactoryCreateLoggerReturnsFileSystemLoggerByDefault(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Logger')) {
            $registry->set('Logger', new \stdClass());
        }

        $factory = new LoggerFactory();
        $logger = $factory->createLogger();
        $this->assertInstanceOf(\Psr\Log\LoggerInterface::class, $logger);
        $this->assertInstanceOf(\StoreCore\FileSystem\Logger::class, $logger);
    }

    #[Depends('testLoggerFactoryCreateLoggerReturnsPsr3CompliantLogger')]
    #[TestDox('LoggerFactory::createLogger MAY return PSR-3 NullLogger')]
    public function testLoggerFactoryCreateLoggerMayReturnPSR3NullLogger(): void
    {
        $this->assertTrue(class_exists('\\Psr\\Log\\NullLogger'));
        $this->assertTrue(class_exists(\Psr\Log\NullLogger::class));

        if (!defined('STORECORE_NULL_LOGGER')) {
            define('STORECORE_NULL_LOGGER', true);
            $factory = new LoggerFactory();
            $logger = $factory->createLogger();
        }
    }
}
