<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(Sidebar::class)]
#[UsesClass(AbstractComponent::class)]
final class SidebarTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP Sidebar class is concrete')]
    public function testAmpSidebarClassIsConcrete(): void
    {
        $class = new ReflectionClass(Sidebar::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Sidebar is an AMP component')]
    public function testAmpSidebarIsAnAmpComponent(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\AbstractComponent::class, new Sidebar());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Sidebar implements stringable AMP BlockInterface')]
    public function testAmpSidebarImplementsStringableAmpBlockInterface(): void
    {
        $sidebar = new Sidebar();
        $this->assertInstanceOf(\Stringable::class, $sidebar);
        $this->assertInstanceOf(\StoreCore\AMP\BlockInterface::class, $sidebar);
    }

    #[Group('hmvc')]
    #[TestDox('AMP Sidebar implements AMP LayoutInterface')]
    public function testAmpSidebarImplementsAmpLayoutInterface(): void
    {
        $sidebar = new Sidebar();
        $this->assertInstanceOf(\StoreCore\AMP\LayoutInterface::class, $sidebar);
        $this->assertInstanceOf(LayoutInterface::class, $sidebar);
    }

    #[Depends('testAmpSidebarImplementsStringableAmpBlockInterface')]
    #[Depends('testAmpSidebarImplementsAmpLayoutInterface')]
    #[TestDox('AMP Sidebar implements AMP LayoutInterface')]
    public function testAmpSidebarDefaultLayoutIsNodisplay(): void
    {
        $sidebar = new Sidebar();
        $this->assertEquals(\StoreCore\AMP\LayoutInterface::LAYOUT_NODISPLAY, $sidebar->getLayout());
        $this->assertStringContainsString(' layout="nodisplay"', (string) $sidebar);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Sidebar::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Sidebar::VERSION);
        $this->assertIsString(Sidebar::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Sidebar::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
