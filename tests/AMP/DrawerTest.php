<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(Drawer::class)]
final class DrawerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP Drawer class is concrete')]
    public function testAmpDrawerClassIsConcrete(): void
    {
        $class = new ReflectionClass(Drawer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Drawer is an AMP component')]
    public function testAmpDrawerIsAnAmpComponent(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\AbstractComponent::class, new Drawer());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Drawer is an AMP Sidebar')]
    public function testAmpDrawerIsAnAmpSidebar(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\Sidebar::class, new Drawer());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Drawer implements stringable AMP BlockInterface')]
    public function testAmpDrawerImplementsStringableAmpBlockInterface(): void
    {
        $drawer = new Drawer();
        $this->assertInstanceOf(\Stringable::class, $drawer);
        $this->assertInstanceOf(\StoreCore\AMP\BlockInterface::class, $drawer);
    }


    #[Group('hmvc')]
    #[TestDox('AMP Drawer implements AMP LayoutInterface')]
    public function testAmpDrawerImplementsAmpLayoutInterface(): void
    {
        $drawer = new Drawer();
        $this->assertInstanceOf(\StoreCore\AMP\LayoutInterface::class, $drawer);
        $this->assertInstanceOf(LayoutInterface::class, $drawer);
    }

    #[Depends('testAmpDrawerImplementsStringableAmpBlockInterface')]
    #[Depends('testAmpDrawerImplementsAmpLayoutInterface')]
    #[TestDox('AMP Drawer implements AMP LayoutInterface')]
    public function testAmpDrawerDefaultLayoutIsNodisplay(): void
    {
        $drawer = new Drawer();
        $this->assertEquals(LayoutInterface::LAYOUT_NODISPLAY, $drawer->getLayout());
        $this->assertStringContainsString(' layout="nodisplay"', (string) $drawer);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Drawer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Drawer::VERSION);
        $this->assertIsString(Drawer::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Drawer::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
