<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(CustomStyle::class)]
final class CustomStyleTest extends TestCase
{
   
    #[Group('hmvc')]
    #[TestDox('CustomStyle class is concrete')]
    public function testCustomStyleClassIsConcrete(): void
    {
        $class = new ReflectionClass(CustomStyle::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CustomStyle is stringable')]
    public function testCustomStyleIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CustomStyle());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CustomStyle::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CustomStyle::VERSION);
        $this->assertIsString(CustomStyle::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CustomStyle::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('CustomStyle::__toString exists')]
    public function testCustomStyleToStringExists(): void
    {
        $class = new ReflectionClass(CustomStyle::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testCustomStyleToStringExists')]
    #[TestDox('CustomStyle::__toString returns non-empty string')]
    public function testCustomStyleToStringReturnsNonEmptyString(): void
    {
        $object = new CustomStyle();
        $html = (string) $object;
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
    }

    #[Depends('testCustomStyleToStringReturnsNonEmptyString')]
    #[TestDox('CustomStyle::__toString returns <style amp-custom> tag')]
    public function testCustomStyleToStringReturnsStyleAmpCustomTag(): void
    {
        $object = new CustomStyle();
        $html = (string) $object;
        $this->assertStringStartsWith('<style amp-custom>', $html);
        $this->assertStringEndsWith('</style>', $html);
    }


    #[TestDox('CustomStyle::append exists')]
    public function testCustomStyleAppendExists(): void
    {
        $class = new ReflectionClass('\\StoreCore\\AMP\\CustomStyle');
        $this->assertTrue($class->hasMethod('append'));
    }

    #[Depends('testCustomStyleAppendExists')]
    #[TestDox('CustomStyle::append is public')]
    public function testCustomStyleAppendIsPublic(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'append');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testCustomStyleAppendExists')]
    #[TestDox('CustomStyle::append has one REQUIRED parameter')]
    public function testCustomStyleAppendHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'append');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CustomStyle::getRoot exists')]
    public function testCustomStyleGetRootExists(): void
    {
        $class = new ReflectionClass(CustomStyle::class);
        $this->assertTrue($class->hasMethod('getRoot'));
    }

    #[Depends('testCustomStyleGetRootExists')]
    #[TestDox('CustomStyle::getRoot exists')]
    public function testCustomStyleGetRootIsPublic(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'getRoot');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCustomStyleGetRootExists')]
    #[TestDox('CustomStyle::getRoot has one OPTIONAL parameter')]
    public function testCustomStyleGetRootHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'getRoot');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCustomStyleGetRootIsPublic')]
    #[Depends('testCustomStyleGetRootHasOneOptionalParameter')]
    #[TestDox('CustomStyle::getRoot returns non-empty string')]
    public function testCustomStyleGetRootReturnsNonEmptyString(): void
    {
        $object = new CustomStyle();
        $html = $object->getRoot();
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
    }

    #[Depends('testCustomStyleGetRootReturnsNonEmptyString')]
    #[TestDox('CustomStyle::getRoot returns CSS :root{…} element')]
    public function testCustomStyleGetRootReturnsCssRootElement(): void
    {
        $object = new CustomStyle();
        $html = $object->getRoot();
        $this->assertStringStartsWith(':root{', $html);
        $this->assertStringEndsWith('}', $html);
    }


    #[TestDox('CustomStyle::getTheme exists')]
    public function testCustomStyleGetThemeExists(): void
    {
        $class = new ReflectionClass('\StoreCore\AMP\CustomStyle');
        $this->assertTrue($class->hasMethod('getTheme'));
    }

    #[Depends('testCustomStyleGetThemeExists')]
    #[TestDox('CustomStyle::getTheme is public')]
    public function testCustomStyleGetThemeIsPublic(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'getTheme');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testCustomStyleGetThemeExists')]
    #[TestDox('CustomStyle::getTheme has no parameters')]
    public function testCustomStyleGetThemeHasNoParameters(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'getTheme');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testCustomStyleGetThemeHasNoParameters')]
    #[TestDox('CustomStyle::getTheme returns non-empty string')]
    public function testCustomStyleGetThemeReturnsNonEmptyString(): void
    {
        $object = new CustomStyle();
        $this->assertNotEmpty($object->getTheme());
        $this->assertIsString($object->getTheme());
    }

    #[Depends('testCustomStyleGetThemeReturnsNonEmptyString')]
    #[TestDox('CustomStyle::getTheme returns JSON')]
    public function testCustomStyleGetThemeReturnsJson(): void
    {
        $object = new CustomStyle();
        $this->assertJson($object->getTheme());
    }


    #[TestDox('CustomStyle::includeComponent exists')]
    public function testCustomStyleIncludeComponentExists(): void
    {
        $class = new ReflectionClass('\StoreCore\AMP\CustomStyle');
        $this->assertTrue($class->hasMethod('includeComponent'));
    }

    #[Depends('testCustomStyleIncludeComponentExists')]
    #[TestDox('CustomStyle::includeComponent is public')]
    public function testCustomStyleIncludeComponentIsPublic(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'includeComponent');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testCustomStyleIncludeComponentExists')]
    #[TestDox('CustomStyle::includeComponent has one REQUIRED parameter')]
    public function testCustomStyleSetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'includeComponent');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CustomStyle::set exists')]
    public function testCustomStyleSetExists(): void
    {
        $class = new ReflectionClass('\StoreCore\AMP\CustomStyle');
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testCustomStyleSetExists')]
    #[TestDox('CustomStyle::set is public')]
    public function testCustomStyleSetIsPublic(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'set');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testCustomStyleSetExists')]
    #[TestDox('CustomStyle::set has two REQUIRED parameters')]
    public function testCustomStyleSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(CustomStyle::class, 'set');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }
}
