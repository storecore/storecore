<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(GoogleAnalytics::class)]
#[UsesClass(AbstractAnalytics::class)]
final class GoogleAnalyticsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP Google Analytics class is concrete')]
    public function testAmpGoogleAnalyticsClassIsConcrete(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Google Analytics class extends AMP Abstract Analytics')]
    public function testAmpGoogleAnalyticsClassExtendsAmpAbstractAnalytics(): void
    {
        $this->assertInstanceOf(AbstractAnalytics::class, new GoogleAnalytics());
    }

    #[Group('hmvc')]
    #[TestDox('AMP GoogleAnalytics class implements AMP BlockInterface')]
    public function testAmpGoogleAnalyticsClassImplementsAmpBlockInterface(): void
    {
        $this->assertInstanceOf(BlockInterface::class, new GoogleAnalytics());
    }

    #[Group('hmvc')]
    #[TestDox('AMP GoogleAnalytics is JSON serializable')]
    public function testAmpGoogleAnalyticsIsJsonSerializable(): void
    {
        $object = new GoogleAnalytics();
        $this->assertInstanceOf(\JsonSerializable::class, $object);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(GoogleAnalytics::VERSION);
        $this->assertIsString(GoogleAnalytics::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(GoogleAnalytics::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('GoogleAnalytics::TYPE class constant is (string) `gtag`')]
    public function testTypeClassConstantIsStringGtag(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertTrue($class->hasConstant('TYPE'));

        $this->assertNotEmpty(GoogleAnalytics::TYPE);
        $this->assertIsString(GoogleAnalytics::TYPE);
        $this->assertEquals('gtag', GoogleAnalytics::TYPE);

        $analytics = new GoogleAnalytics();
        $this->assertNotEmpty($analytics::TYPE);
        $this->assertIsString($analytics::TYPE);
        $this->assertEquals('gtag', $analytics::TYPE);
    }


    #[TestDox('GoogleAnalytics::$dataCredentials class property exists')]
    public function testGoogleAnalyticsDataCredentialsClassPropertyExists(): void
    {
        $this->assertObjectHasProperty('dataCredentials', new GoogleAnalytics());
    }

    #[Depends('testGoogleAnalyticsDataCredentialsClassPropertyExists')]
    #[TestDox('GoogleAnalytics::$dataCredentials is (bool) true by default')]
    public function testGoogleAnalyticsDataCredentialsBoolTrueByDefault(): void
    {
        $object = new GoogleAnalytics();
        $this->assertIsBool($object->dataCredentials);
        $this->assertTrue($object->dataCredentials);
    }


    #[TestDox('GoogleAnalytics::__construct exists')]
    public function testGoogleAnalyticsConstructorExists(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testGoogleAnalyticsConstructorExists')]
    #[TestDox('GoogleAnalytics::__construct is public constructor')]
    public function testGoogleAnalyticsConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(GoogleAnalytics::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testGoogleAnalyticsConstructorExists')]
    #[TestDox('GoogleAnalytics::__construct has one OPTIONAL parameter')]
    public function testGoogleAnalyticsConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(GoogleAnalytics::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAmpGoogleAnalyticsClassImplementsAmpBlockInterface')]
    #[Depends('testGoogleAnalyticsConstructorHasOneOptionalParameter')]
    #[TestDox('GoogleAnalytics::__construct sets gtag_id')]
    public function testGoogleAnalyticsConstructorSetsGtagId(): void
    {
        $google_analytics = new GoogleAnalytics();
        $amp_html = (string) $google_analytics;
        $this->assertStringContainsString('G-XXXXXXXXX', $amp_html);

        $google_analytics = new GoogleAnalytics('G-123456789');
        $amp_html = (string) $google_analytics;
        $this->assertStringContainsString('G-123456789', $amp_html);
    }


    #[TestDox('GoogleAnalytics::__toString exists')]
    public function testGoogleAnalyticsToStringExists(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testGoogleAnalyticsToStringExists')]
    #[TestDox('GoogleAnalytics::__toString is public')]
    public function testGoogleAnalyticsToStringIsPublic(): void
    {
        $method = new ReflectionMethod(GoogleAnalytics::class, '__toString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testGoogleAnalyticsToStringExists')]
    #[TestDox('GoogleAnalytics::__toString returns non-empty string')]
    public function testGoogleAnalyticsToStringReturnsNonEmptyString(): void
    {
        $object = new GoogleAnalytics();
        $amp_html = (string) $object;
        $this->assertNotEmpty($amp_html);
        $this->assertIsString($amp_html);
    }

    #[Depends('testGoogleAnalyticsToStringReturnsNonEmptyString')]
    #[TestDox('GoogleAnalytics::__toString returns <amp-analytics> tag')]
    public function testGoogleAnalyticsToStringReturnsAmpAnalyticsTag(): void
    {
        $object = new GoogleAnalytics();
        $amp_html = (string) $object;
        $this->assertStringStartsWith('<amp-analytics ', $amp_html);
        $this->assertStringEndsWith('</amp-analytics>', $amp_html);
    }

    #[Depends('testGoogleAnalyticsToStringReturnsAmpAnalyticsTag')]
    #[TestDox('GoogleAnalytics::__toString returns type="gtag" attribute')]
    public function testGoogleAnalyticsToStringReturnsTypeGtagAttribute(): void
    {
        $object = new GoogleAnalytics();
        $amp_html = (string) $object;
        $this->assertStringStartsWith('<amp-analytics type="gtag"', $amp_html);
    }

    #[Depends('testGoogleAnalyticsToStringReturnsTypeGtagAttribute')]
    #[TestDox('GoogleAnalytics::__toString returns data-credentials="include" attribute')]
    public function testGoogleAnalyticsToStringReturnsDataCredentialsIncludeAttribute(): void
    {
        $googleAnalytics = new GoogleAnalytics();
        $this->assertStringStartsWith(
            '<amp-analytics type="gtag" data-credentials="include"', 
            (string) $googleAnalytics
        );
    }

    #[Depends('testGoogleAnalyticsToStringReturnsAmpAnalyticsTag')]
    #[TestDox('GoogleAnalytics::__toString returns <script type="application/json"> tag')]
    public function testGoogleAnalyticsToStringReturnsScriptTypeApplicationJsonTag(): void
    {
        $object = new GoogleAnalytics();
        $amp_html = (string) $object;
        $this->assertStringContainsString('<script type="application/json">', $amp_html);
        $this->assertStringContainsString('</script>', $amp_html);

        $amp_html = strip_tags($amp_html, '<script>');
        $this->assertStringStartsWith('<script type="application/json">', $amp_html);
        $this->assertStringEndsWith('</script>', $amp_html);
    }

    #[Depends('testGoogleAnalyticsToStringReturnsScriptTypeApplicationJsonTag')]
    #[TestDox('GoogleAnalytics::__toString returns JSON')]
    public function testGoogleAnalyticsToStringReturnsJson(): void
    {
        $object = new GoogleAnalytics();
        $string = (string) $object;
        $string = strip_tags($string);
        $this->assertNotEmpty($string);
        $this->assertJson($string);
    }


    #[TestDox('GoogleAnalytics::setSiteSpeedSampleRate exists')]
    public function testGoogleAnalyticsSetSiteSpeedSampleRateExist(): void
    {
        $class = new ReflectionClass(GoogleAnalytics::class);
        $this->assertTrue($class->hasMethod('setSiteSpeedSampleRate'));
    }

    #[Depends('testGoogleAnalyticsSetSiteSpeedSampleRateExist')]
    #[TestDox('GoogleAnalytics::setSiteSpeedSampleRate is public')]
    public function testGoogleAnalyticsSetSiteSpeedSampleRateIsPublic(): void
    {
        $method = new ReflectionMethod(GoogleAnalytics::class, 'setSiteSpeedSampleRate');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testGoogleAnalyticsSetSiteSpeedSampleRateExist')]
    #[TestDox('GoogleAnalytics::setSiteSpeedSampleRate has one REQUIRED parameter')]
    public function testGoogleAnalyticsSetSiteSpeedSampleRateHasOneParameter(): void
    {
        $method = new ReflectionMethod(GoogleAnalytics::class, 'setSiteSpeedSampleRate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testGoogleAnalyticsSetSiteSpeedSampleRateHasOneParameter')]
    #[TestDox('GoogleAnalytics::setSiteSpeedSampleRate sets site_speed_sample_rate')]
    public function testGoogleAnalyticsSetSiteSpeedSampleRateSetsSiteSpeedSampleRate(): void
    {
        $google_analytics = new GoogleAnalytics();

        $google_analytics->setSiteSpeedSampleRate(100);
        $amp_html = (string) $google_analytics;
        $this->assertStringContainsString('"site_speed_sample_rate":100', $amp_html);

        $google_analytics->setSiteSpeedSampleRate(10);
        $amp_html = (string) $google_analytics;
        $this->assertStringNotContainsString('"site_speed_sample_rate":100', $amp_html);
        $this->assertStringContainsString('"site_speed_sample_rate":10', $amp_html);
    }
}
