<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionClassConstant, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[Group('hmvc')]
#[CoversClass(AbstractComponent::class)]
final class AbstractComponentTest extends TestCase
{
    private AbstractComponent $abstractComponent;

    public function setUp(): void
    {
        $this->abstractComponent = new class extends AbstractComponent
        {
            public function __toString(): string
            {
                return '';
            }
        };
    }

    #[TestDox('AMP AbstractComponent class is abstract')]
    public function testAmpAbstractComponentClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented AMP LayoutInterface interface exists')]
    public function testImplementedAmpLayoutInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\LayoutInterface'));
        $this->assertTrue(interface_exists(LayoutInterface::class));
    }

    #[Depends('testImplementedAmpLayoutInterfaceInterfaceExists')]
    #[Group('distro')]
    #[TestDox('AbstractComponent implements AMP LayoutInterface')]
    public function testAmpAbstractComponentImplementsAmpLayoutInterface(): void
    {
        $this->assertInstanceOf(LayoutInterface::class, $this->abstractComponent);
    }


    #[Depends('testImplementedAmpLayoutInterfaceInterfaceExists')]
    #[TestDox('AMP component MUST be stringable for AMP HTML')]
    public function testAmpComponentMustBeStringableForAmpHtml(): void
    {
        $this->assertInstanceOf(\Stringable::class, $this->abstractComponent);
    }


    #[TestDox('AbstractComponent.SUPPORTED_LAYOUTS exists')]
    public function testAbstractComponentSupportedLayoutsExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasConstant('SUPPORTED_LAYOUTS'));
    }

    #[Depends('testAbstractComponentSupportedLayoutsExists')]
    #[TestDox('AbstractComponent.SUPPORTED_LAYOUTS is protected')]
    public function testAbstractComponentSupportedLayoutsIsProtected(): void
    {
        $constant = new ReflectionClassConstant(AbstractComponent::class, 'SUPPORTED_LAYOUTS');
        $this->assertTrue($constant->isProtected());
    }


    #[TestDox('AbstractComponent.attributes exists')]
    public function testAbstractComponentAttributesExists(): void
    {
        $this->assertObjectHasProperty('attributes', $this->abstractComponent);
    }

    #[Depends('testAbstractComponentAttributesExists')]
    #[TestDox('AbstractComponent.attributes is protected')]
    public function testAbstractComponentAttributesIsProtected(): void
    {
        $property = new ReflectionProperty(AbstractComponent::class, 'attributes');
        $this->assertTrue($property->isProtected());
    }


    #[TestDox('AbstractComponent::getAttribute() exists')]
    public function testAbstractComponentGetAttributeExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasMethod('getAttribute'));
    }

    #[Depends('testAbstractComponentGetAttributeExists')]
    #[TestDox('AbstractComponent::getAttribute() is public')]
    public function testAbstractComponentGetAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'getAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractComponentGetAttributeExists')]
    #[TestDox('AbstractComponent::getAttribute() has one REQUIRED parameter')]
    public function testAbstractComponentGetAttributeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'getAttribute');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractComponentGetAttributeExists')]
    #[Depends('testAbstractComponentGetAttributeIsPublic')]
    #[Depends('testAbstractComponentGetAttributeHasOneRequiredParameter')]
    #[TestDox('AbstractComponent::getAttribute() returns attribute value')]
    public function testAbstractComponentGetAttributeReturnsAttributeValue(): void
    {
        $this->assertTrue($this->abstractComponent->hasAttribute('layout'));
        $this->assertNotNull($this->abstractComponent->getAttribute('layout'));
        $this->assertNotEmpty($this->abstractComponent->getAttribute('layout'));
    }


    #[TestDox('AbstractComponent::getLayout() exists')]
    public function testAbstractComponentGetLayoutExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasMethod('getLayout'));
    }

    #[Depends('testAbstractComponentGetLayoutExists')]
    #[TestDox('AbstractComponent::getLayout() is final public')]
    public function testAbstractComponentGetLayoutIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'getLayout');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractComponentGetLayoutExists')]
    #[TestDox('AbstractComponent::getLayout() has no parameters')]
    public function testAbstractComponentGetLayoutHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'getLayout');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractComponentGetLayoutExists')]
    #[Depends('testAbstractComponentGetLayoutIsFinalPublic')]
    #[Depends('testAbstractComponentGetLayoutHasNoParameters')]
    #[TestDox('AbstractComponent::getLayout() returns non-empty string')]
    public function testAbstractComponentGetLayoutReturnsNonEmptyString(): void
    {
        $this->assertNotEmpty($this->abstractComponent->getLayout());
        $this->assertIsString($this->abstractComponent->getLayout());
    }

    #[Depends('testAbstractComponentGetLayoutReturnsNonEmptyString')]
    #[TestDox('AbstractComponent::getLayout() returns string "responsive" by default')]
    public function testAbstractComponentGetLayoutReturnsStringResponsiveByDefault(): void
    {
        $this->assertEquals('responsive', $this->abstractComponent->getLayout());
    }


    #[TestDox('AbstractComponent::hasAttribute() exists')]
    public function testAbstractComponentHasAttributeExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasMethod('hasAttribute'));
    }

    #[Depends('testAbstractComponentHasAttributeExists')]
    #[TestDox('AbstractComponent::hasAttribute() is public')]
    public function testAbstractComponentHasAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'hasAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractComponentHasAttributeExists')]
    #[TestDox('AbstractComponent::hasAttribute() has one REQUIRED parameter')]
    public function testAbstractComponentHasAttributeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'hasAttribute');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractComponentHasAttributeExists')]
    #[Depends('testAbstractComponentHasAttributeIsPublic')]
    #[Depends('testAbstractComponentHasAttributeHasOneRequiredParameter')]
    #[TestDox('AbstractComponent::hasAttribute() returns bool')]
    public function testAbstractComponentHasAttributeReturnsBool(): void
    {
        $this->assertIsBool($this->abstractComponent->hasAttribute('layout'));
        $this->assertTrue($this->abstractComponent->hasAttribute('layout'));
    }


    #[TestDox('AbstractComponent::setHTML() exists')]
    public function testAbstractComponentSetHtmlExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasMethod('setHTML'));
    }

    #[Depends('testAbstractComponentSetHtmlExists')]
    #[TestDox('AbstractComponent::setHTML() is public')]
    public function testAbstractComponentSetHtmlIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'setHTML');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractComponentSetHtmlExists')]
    #[TestDox('AbstractComponent::setHTML() has one REQUIRED parameter')]
    public function testAbstractComponentSetHtmlHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'setHTML');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractComponentSetHtmlExists')]
    #[TestDox('AbstractComponent::setHTML() sets innerHTML and innerText')]
    public function testAbstractComponentSetHtmlSetsInnerHtmlAndInnerText(): void
    {
        $this->assertEmpty($this->abstractComponent->innerHTML);
        $this->assertEmpty($this->abstractComponent->innerText);

        $this->abstractComponent->setHTML('<h1>Hello world</h1>');

        $this->assertNotEmpty($this->abstractComponent->innerHTML);
        $this->assertIsString($this->abstractComponent->innerHTML);
        $this->assertEquals('<h1>Hello world</h1>', $this->abstractComponent->innerHTML);

        $this->assertNotEmpty($this->abstractComponent->innerText);
        $this->assertIsString($this->abstractComponent->innerText);
        $this->assertEquals('Hello world', $this->abstractComponent->innerText);
    }

    #[Depends('testAbstractComponentSetHtmlSetsInnerHtmlAndInnerText')]
    #[TestDox('AbstractComponent::setHTML() adds new HTML to exisiting HTML')]
    public function testAbstractComponentSetHtmlAddsNewHtmlToExisitingHtml(): void
    {
        $this->abstractComponent->setHTML('<h1>Lorem ipsum</h1>');
        $this->abstractComponent->setHTML('<p>In publishing and graphic design, <dfn>Lorem ipsum</dfn> is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>');

        $this->assertNotEquals('<h1>Lorem ipsum</h1>', $this->abstractComponent->innerHTML);
        $this->assertStringStartsWith('<h1>Lorem ipsum</h1><p>In publishing and graphic design, ', $this->abstractComponent->innerHTML);
        $this->assertStringEndsWith(' meaningful content.</p>', $this->abstractComponent->innerHTML);
    }


    #[TestDox('AbstractComponent::setLayout() exists')]
    public function testAbstractComponentSetLayoutExists(): void
    {
        $class = new ReflectionClass(AbstractComponent::class);
        $this->assertTrue($class->hasMethod('setLayout'));
    }

    #[Depends('testAbstractComponentSetLayoutExists')]
    #[TestDox('AbstractComponent::setLayout() is public')]
    public function testAbstractComponentSetLayoutIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'setLayout');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractComponentSetLayoutExists')]
    #[TestDox('AbstractComponent::setLayout() has one REQUIRED parameter')]
    public function testAbstractComponentSetLayoutHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractComponent::class, 'setLayout');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractComponentSetLayoutExists')]
    #[Depends('testAbstractComponentSetLayoutIsPublic')]
    #[Depends('testAbstractComponentSetLayoutHasOneRequiredParameter')]
    #[TestDox('AbstractComponent::setLayout() throws \ValueError exception on invalid string value')]
    public function testAbstractComponentSetLayoutThrowsValueErrorExceptionOnInvalidStringValue(): void
    {
        $this->expectException(\ValueError::class);
        $this->abstractComponent->setLayout('Oops');
    }

    /**
     * @see https://amp.dev/documentation/guides-and-tutorials/learn/amp-html-layout/#layout
     */
    #[Depends('testAbstractComponentSetLayoutExists')]
    #[Depends('testAbstractComponentSetLayoutIsPublic')]
    #[Depends('testAbstractComponentSetLayoutHasOneRequiredParameter')]
    #[TestDox('AbstractComponent::setLayout() accepts all AMP layout values')]
    public function testAbstractComponentSetLayoutAcceptsAllAmpLayoutValues(): void
    {
        $values = [
            'container',
            'fill',
            'fixed',
            'fixed-height',
            'flex-item',
            'intrinsic',
            'nodisplay',
            'responsive',
        ];

        foreach ($values as $value) {
            $this->abstractComponent->setLayout($value);
            $this->assertEquals($value, $this->abstractComponent->getLayout());
        }
    }
}
