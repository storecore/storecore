<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(Carousel::class)]
#[UsesClass(\StoreCore\AMP\Image::class)]
#[UsesClass(\StoreCore\CMS\HTMLImageElement::class)]
final class CarouselTest extends TestCase
{
    private readonly ReflectionClass $class;

    public function setUp(): void
    {
        $this->class = new ReflectionClass(Carousel::class);
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented StoreCore\AMP\LayoutInterface interface exists')]
    public function testImplementedStoreCoreAmpLayoutInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\LayoutInterface'));
        $this->assertTrue(interface_exists(LayoutInterface::class));
    }

    #[Depends('testImplementedStoreCoreAmpLayoutInterfaceInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Implemented StoreCore\AMP\LayoutInterface interface file exists')]
    public function testAmpImageImplementsStoreCoreAmpLayoutInterface(): void
    {
        $object = new Carousel();
        $this->assertInstanceOf(\StoreCore\AMP\LayoutInterface::class, $object);
        $this->assertInstanceOf(LayoutInterface::class, $object);
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented StoreCore\AMP\LightboxGalleryInterface interface exists')]
    public function testImplementedStoreCoreAmpLightboxGalleryInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\LightboxGalleryInterface'));
        $this->assertTrue(interface_exists(LightboxGalleryInterface::class));
    }

    #[Depends('testImplementedStoreCoreAmpLightboxGalleryInterfaceInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('AMP carousel implements \StoreCore\AMP\LightboxGalleryInterface')]
    public function testAmpCarouselImplementsStoreCoreAmpLightboxGalleryInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\LightboxGalleryInterface::class, new Carousel());
    }


    #[Group('hmvc')]
    #[TestDox('AMP carousel implements StoreCore\AMP\BlockInterface')]
    public function testAmpCarouselImplementsStoreCoreAmpBlockInterface()
    {
        $this->assertInstanceOf(\StoreCore\AMP\BlockInterface::class, new Carousel());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $this->assertTrue($this->class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Carousel::VERSION);
        $this->assertIsString(Carousel::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Carousel::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Carousel::__construct exists')]
    public function testCarouselConstructorExists(): void
    {
        $this->assertTrue($this->class->hasMethod('__construct'));
    }

    #[TestDox('Carousel::__construct has one OPTIONAL parameter')]
    public function testCarouselConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Carousel::__toString exists')]
    public function testCarouselToStringExists(): void
    {
        $this->assertTrue($this->class->hasMethod('__toString'));
    }

    #[Depends('testCarouselToStringExists')]
    #[TestDox('Carousel::__toString returns non-empty string')]
    public function testCarouselToStringReturnsNonEmptyString(): void
    {
        $carousel = new Carousel();
        $html = (string) $carousel;
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
    }

    #[Depends('testCarouselToStringReturnsNonEmptyString')]
    #[TestDox('Carousel::__toString returns HTML tag')]
    public function testCarouselToStringReturnsHtmlTag(): void
    {
        $carousel = new Carousel();
        $html = (string) $carousel;
        $this->assertNotEmpty($html);
        $html = strip_tags($html);
        $this->assertEmpty($html);
    }

    #[Depends('testCarouselToStringReturnsNonEmptyString')]
    #[Depends('testCarouselToStringReturnsHtmlTag')]
    #[TestDox('Carousel::__toString returns <amp-carousel> tag')]
    public function testCarouselToStringReturnsAmpCarouselTag(): void
    {
        $carousel = new Carousel();
        $html = (string) $carousel;
        $this->assertStringStartsWith('<amp-carousel ', $html);
        $this->assertStringEndsWith('</amp-carousel>', $html);
    }


    #[TestDox('Carousel.autoplay exists')]
    public function testCarouselAutoplayExists(): void
    {
        $carousel = new Carousel();
        $this->assertObjectHasProperty('autoplay', $carousel);
    }

    #[TestDox('Carousel::setAutoplay exists')]
    public function testCarouselSetAutoplayExists(): void
    {
        $this->assertTrue($this->class->hasMethod('setAutoplay'));
    }

    #[Depends('testCarouselSetAutoplayExists')]
    #[TestDox('Carousel::setAutoplay is public')]
    public function testCarouselSetAutoplayIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setAutoplay');
        $this->assertTrue($method->isPublic());
    }


    #[TestDox('Carousel.children exists')]
    public function testCarouselChildrenExists(): void
    {
        $this->assertObjectHasProperty('children', new Carousel());
    }

    #[TestDox('Carousel::append exists')]
    public function testCarouselAppendExists(): void
    {
        $this->assertTrue($this->class->hasMethod('append'));
    }

    #[Depends('testCarouselAppendExists')]
    #[TestDox('Carousel::append is public')]
    public function testCarouselAppendIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'append');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCarouselAppendExists')]
    #[TestDox('Carousel::append has one REQUIRED parameter')]
    public function testCarouselAppendHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'append');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCarouselAppendHasOneRequiredParameter')]
    #[TestDox('Carousel::append returns node count')]
    public function testCarouselAppendReturnsNodeCount(): void
    {
        $child_node = '<div><!-- ... --></div>';
        $carousel = new Carousel();
        $this->assertEquals(1, $carousel->append($child_node));
        $this->assertEquals(2, $carousel->append($child_node));
        $this->assertEquals(3, $carousel->append($child_node));
    }


    #[TestDox('Carousel.delay exists')]
    public function testCarouselDelayExists(): void
    {
        $this->assertObjectHasProperty('delay', new Carousel());
    }

    #[TestDox('Carousel::setDelay exists')]
    public function testCarouselSetDelayExists(): void
    {
        $this->assertTrue($this->class->hasMethod('setDelay'));
    }

    #[Depends('testCarouselSetDelayExists')]
    #[TestDox('Carousel::setDelay is public')]
    public function testCarouselSetDelayIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setDelay');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCarouselSetDelayExists')]
    #[TestDox('Carousel::setDelay has one REQUIRED parameter')]
    public function testCarouselSetDelayHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setDelay');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCarouselSetDelayHasOneRequiredParameter')]
    #[TestDox('Carousel::setDelay accepts integers')]
    public function testCarouselSetDelayAcceptsIntegers(): void
    {
        $carousel = new Carousel();
        $carousel->setDelay(2500);

        $reflection_class = new ReflectionClass($carousel);
        $reflection_property = $reflection_class->getProperty('delay');
        $reflection_property->setAccessible(true);
        $this->assertSame(2500, $reflection_property->getValue($carousel));
    }

    #[Depends('testCarouselSetDelayHasOneRequiredParameter')]
    #[TestDox('Carousel::setDelay accepts \DateInterval in seconds')]
    public function testCarouselSetDelayAcceptsDateIntervalInSeconds(): void
    {
        $three_seconds = new \DateInterval('PT3S');
        $carousel = new Carousel();
        $carousel->setDelay($three_seconds);

        $reflection_class = new ReflectionClass($carousel);
        $reflection_property = $reflection_class->getProperty('delay');
        $reflection_property->setAccessible(true);
        $this->assertSame(3000, $reflection_property->getValue($carousel));
    }

    #[Depends('testCarouselSetDelayHasOneRequiredParameter')]
    #[TestDox('Carousel::setDelay accepts \DateInterval in minutes')]
    public function testCarouselSetDelayAcceptsDateIntervalInMinutes(): void
    {
        $one_minute = new \DateInterval('PT1M');
        $carousel = new Carousel();
        $carousel->setDelay($one_minute);

        $reflection_class = new ReflectionClass($carousel);
        $reflection_property = $reflection_class->getProperty('delay');
        $reflection_property->setAccessible(true);
        $this->assertSame(60000, $reflection_property->getValue($carousel));
    }


    #[TestDox('Carousel.height exists')]
    public function testCarouselHeightExists(): void
    {
        $carousel = new Carousel();
        $this->assertObjectHasProperty('height', $carousel);
    }

    #[TestDox('Carousel::setHeight exists')]
    public function testCarouselSetHeightExists(): void
    {
        $this->assertTrue($this->class->hasMethod('setHeight'));
    }

    #[Depends('testCarouselSetHeightExists')]
    #[TestDox('Carousel::setHeight is public')]
    public function testCarouselSetHeightIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setHeight');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCarouselSetHeightExists')]
    #[TestDox('Carousel::setHeight has one REQUIRED parameter')]
    public function testCarouselSetHeightHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setHeight');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Carousel.type exists')]
    public function testCarouselTypeExists(): void
    {
        $this->assertObjectHasProperty('type', new Carousel());
    }

    #[TestDox('Carousel::setType exists')]
    public function testCarouselSetTypeExists(): void
    {
        $this->assertTrue($this->class->hasMethod('setType'));
    }

    #[Depends('testCarouselSetTypeExists')]
    #[TestDox('Carousel::setType is public')]
    public function testCarouselSetTypeIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setType');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCarouselSetTypeExists')]
    #[TestDox('Carousel::setType has one REQUIRED parameter')]
    public function testCarouselSetTypeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setType');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCarouselSetTypeHasOneRequiredParameter')]
    #[TestDox('Carousel::setType accepts two class constants')]
    public function testCarouselSetTypeAcceptsTwoClassConstants(): void
    {
        $carousel = new Carousel();
        $this->assertEquals(
            Carousel::TYPE_CAROUSEL,
            $carousel->type,
            'Default caurosel type is `type=carousel`.'
        );
        $this->assertEquals('carousel', $carousel->type);

        $carousel->setType(Carousel::TYPE_SLIDES);
        $this->assertNotEquals('carousel', $carousel->type);
        $this->assertEquals('slides', $carousel->type);

        $carousel->setType(Carousel::TYPE_CAROUSEL);
        $this->assertNotEquals('slides', $carousel->type);
        $this->assertEquals('carousel', $carousel->type);
    }

    #[Depends('testCarouselSetTypeHasOneRequiredParameter')]
    #[TestDox('Carousel::setType throws \ValueError exception on empty string')]
    public function testCarouselSetTypeThrowsValueErrorExceptionOnEmptyString(): void
    {
        $carousel = new Carousel();
        $this->expectException(\ValueError::class);
        $carousel->setType('');
    }


    #[TestDox('Carousel.width exists')]
    public function testCarouselWidthExists(): void
    {
        $carousel = new Carousel();
        $this->assertObjectHasProperty('width', $carousel);
    }

    #[TestDox('Carousel.width is empty by default')]
    public function testCarouselWidthIsEmptyByDefault(): void
    {
        $carousel = new Carousel();
        $this->assertEmpty($carousel->width);
    }

    #[TestDox('Carousel::setWidth exists')]
    public function testCarouselSetWidthExists(): void
    {
        $this->assertTrue($this->class->hasMethod('setWidth'));
    }

    #[Depends('testCarouselSetWidthExists')]
    #[TestDox('Carousel::setWidth is public')]
    public function testCarouselSetWidthIsPublic(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setWidth');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCarouselSetWidthExists')]
    #[TestDox('Carousel::setWidth has one REQUIRED parameter')]
    public function testCarouselSetWidthHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Carousel::class, 'setWidth');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCarouselSetWidthExists')]
    #[Depends('testCarouselSetWidthHasOneRequiredParameter')]
    #[Depends('testCarouselWidthIsEmptyByDefault')]
    #[TestDox('Carousel::setWidth sets width as integer')]
    public function testCarouselSetWidtSetsWidthAsInteger(): void
    {
        $carousel = new Carousel();
        $carousel->setWidth(450);
        $this->assertEquals(450, $carousel->width);

        $this->expectException(\ValueError::class);
        $carousel->setWidth(0);
    }

    #[Depends('testCarouselSetWidtSetsWidthAsInteger')]
    #[TestDox('Carousel::setWidth accepts width as numeric string')]
    public function testCarouselSetWidthAcceptsWidthAsNumericString(): void
    {
        $carousel = new Carousel();
        $carousel->setWidth("450");
        $this->assertIsInt($carousel->width);
        $this->assertEquals(450, $carousel->width);
    }


    /**
     * @see https://amp.dev/documentation/examples/components/amp-carousel/
     *
     * ```html
     * <amp-carousel height="300" layout="fixed-height" type="carousel" role="region" aria-label="Basic usage carousel">
     *   <amp-img src="/static/samples/img/image1.jpg" width="400" height="300" alt="a sample image"></amp-img>
     *   <amp-img src="/static/samples/img/image2.jpg" width="400" height="300" alt="another sample image"></amp-img>
     *   <amp-img src="/static/samples/img/image3.jpg" width="400" height="300" alt="and another sample image"></amp-img>
     * </amp-carousel>
     * ```
     */
    #[TestDox('Basic usage example')]
    public function testBasicUsageExample(): void
    {
        $carousel = new Carousel();
        $carousel->height = 300;
        $carousel->layout = LayoutInterface::LAYOUT_FIXED_HEIGHT;

        $html = (string) $carousel;
        $this->assertStringStartsWith('<amp-carousel ', $html);
        $this->assertStringContainsString(' height="300"', $html);
        $this->assertStringNotContainsString('width', $html, 'The `width` attribute was not set.');
        $this->assertStringContainsString(' layout="fixed-height"', $html);
        $this->assertStringContainsString(' role="region"', $html);
        $this->assertStringEndsWith('</amp-carousel>', $html);

        $this->assertStringNotContainsString('aria-label', $html);
        $carousel->ariaLabel = 'Basic usage carousel';
        $html = (string) $carousel;
        $this->assertStringContainsString('aria-label', $html);
        $this->assertStringContainsString(' aria-label="Basic usage carousel"', $html);
    }
}
