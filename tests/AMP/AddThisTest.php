<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \strip_tags;

#[CoversClass(AddThis::class)]
final class AddThisTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AddThis class is concrete')]
    public function testAddThisClassIsConcrete(): void
    {
        $class = new ReflectionClass(AddThis::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP AddThis is an AMP component')]
    public function testAmpAddThisIsAmpComponent(): void
    {
        $this->assertInstanceOf(AbstractComponent::class, new AddThis());
    }

    #[Group('hmvc')]
    #[TestDox('AMP AddThis class implements Stringable BlockInterface')]
    public function testAmpAddThisClassImplementsStringableBlockInterface(): void
    {
        $this->assertTrue(interface_exists(BlockInterface::class));

        $object = new AddThis();
        $this->assertInstanceOf(\Stringable::class, $object);
        $this->assertInstanceOf(BlockInterface::class, $object);
    }

    #[Group('hmvc')]
    #[TestDox('AMP AddThis class implements AMP LayoutInterface')]
    public function testAmpAddThisClassImplementsAmpLayoutInterface(): void
    {
        $this->assertTrue(interface_exists(LayoutInterface::class));
        $this->assertInstanceOf(LayoutInterface::class, new AddThis());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AddThis::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AddThis::VERSION);
        $this->assertIsString(AddThis::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AddThis::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AddThis::__toString() exists')]
    public function testAddThisToStringExists(): void
    {
        $class = new ReflectionClass(AddThis::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testAddThisToStringExists')]
    #[TestDox('AddThis::__toString() returns non-empty string')]
    public function testAddThisToStringReturnsNonEmptyString(): void
    {
        $add_this = new AddThis();
        $amp_html = (string) $add_this;
        $this->assertNotEmpty($amp_html);
        $this->assertIsString($amp_html);
    }

    #[Depends('testAddThisToStringReturnsNonEmptyString')]
    #[TestDox('AddThis::__toString() returns HTML tags only')]
    public function testAddThisToStringReturnsHtmlTagsOnly(): void
    {
        $add_this = new AddThis();
        $amp_html = (string) $add_this;
        $this->assertNotEmpty($amp_html);

        $amp_html = strip_tags($amp_html);
        $this->assertEmpty($amp_html);
    }

    #[Depends('testAddThisToStringReturnsHtmlTagsOnly')]
    #[TestDox('AddThis::__toString returns `amp-addthis` tag')]
    public function testAddThisToStringReturnsAmpAddThisTag(): void
    {
        $add_this = new AddThis();
        $amp_html = (string) $add_this;
        $this->assertStringStartsWith('<amp-addthis ', $amp_html);
        $this->assertStringEndsWith('</amp-addthis>', $amp_html);
    }

    #[Depends('testAddThisToStringReturnsHtmlTagsOnly')]
    #[TestDox('AddThis `layout` attribute is `responsive` by default')]
    public function testAddThisLayoutAttributeIsResponsiveByDefault(): void
    {
        $add_this = new AddThis();
        $amp_html = (string) $add_this;
        $this->assertStringContainsString(' layout="responsive"', $amp_html);
    }


    #[TestDox('AddThis::setPublisherID() exists')]
    public function testAddThisSetPublisherIdExists(): void
    {
        $class = new ReflectionClass(AddThis::class);
        $this->assertTrue($class->hasMethod('setPublisherID'));
    }

    #[Depends('testAddThisSetPublisherIdExists')]
    #[TestDox('AddThis::setPublisherID() is public')]
    public function testAddThisSetPublisherIdIsPublic(): void
    {
        $method = new ReflectionMethod(AddThis::class, 'setPublisherID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAddThisSetPublisherIdExists')]
    #[TestDox('AddThis::setPublisherID() has one REQUIRED parameter')]
    public function testAddThisSetPublisherIdHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AddThis::class, 'setPublisherID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAddThisSetPublisherIdExists')]
    #[Depends('testAddThisSetPublisherIdIsPublic')]
    #[Depends('testAddThisSetPublisherIdHasOneRequiredParameter')]
    #[TestDox('AddThis::setPublisherID() sets `data-pub-id` attribute')]
    public function testAddThisSetPublisherIdSetsDataPubIdAttribute(): void
    {
        $add_this_component = new AddThis();
        $this->assertEmpty($add_this_component->__get('data-pub-id'));

        $add_this_component->setPublisherID('ra-59c2c366435ef478');
        $this->assertNotEmpty($add_this_component->__get('data-pub-id'));
        $this->assertEquals('ra-59c2c366435ef478', $add_this_component->__get('data-pub-id'));

        $add_this_amp_html = (string) $add_this_component;
        $this->assertStringContainsString(' data-pub-id="ra-59c2c366435ef478"', $add_this_amp_html);
    }


    #[TestDox('AddThis::setWidgetID() exists')]
    public function testAddThisSetWidgetIdExists(): void
    {
        $class = new ReflectionClass(AddThis::class);
        $this->assertTrue($class->hasMethod('setWidgetID'));
    }

    #[Depends('testAddThisSetWidgetIdExists')]
    #[TestDox('AddThis::setWidgetID() is public')]
    public function testAddThisSetWidgetIdIsPublic(): void
    {
        $method = new ReflectionMethod(AddThis::class, 'setWidgetID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAddThisSetWidgetIdExists')]
    #[TestDox('AddThis::setWidgetID() has one REQUIRED parameter')]
    public function testAddThisSetWidgetIdHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AddThis::class, 'setWidgetID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAddThisSetWidgetIdExists')]
    #[Depends('testAddThisSetWidgetIdIsPublic')]
    #[Depends('testAddThisSetWidgetIdHasOneRequiredParameter')]
    #[TestDox('AddThis::setWidgetID() method sets `data-widget-id` attribute')]
    public function testAddThisSetWidgetIdSetsDataWidgetIdAttribute(): void
    {
        $add_this_component = new AddThis();
        $this->assertEmpty($add_this_component->__get('data-widget-id'));

        $add_this_component->setWidgetID('0fyg');
        $this->assertNotEmpty($add_this_component->__get('data-widget-id'));
        $this->assertEquals('0fyg', $add_this_component->__get('data-widget-id'));

        $add_this_amp_html = (string)$add_this_component;
        $this->assertStringContainsString(' data-widget-id="0fyg"', $add_this_amp_html);
    }


    #[TestDox('AddThis::__construct() has two OPTIONAL parameters')]
    public function testAddThisConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(AddThis::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAddThisConstructorHasTwoOptionalParameters')]
    #[TestDox('AddThis::__construct() accepts publisher ID and widget ID')]
    public function testAddThisConstructorAcceptsPublisherIdAndWidgetId(): void
    {
        $object = new AddThis('ra-59c2c366435ef478', '0fyg');
        $string = (string) $object;
        $this->assertStringContainsString(' data-pub-id="ra-59c2c366435ef478"', $string);
        $this->assertStringContainsString(' data-widget-id="0fyg"', $string);
    }
}
