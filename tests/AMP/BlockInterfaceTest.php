<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class BlockInterfaceTest extends TestCase
{
    #[TestDox('BlockInterface interface file exists')]
    public function testBlockInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'BlockInterface.php'
        );
    }

    #[Depends('testBlockInterfaceInterfaceFileExists')]
    #[TestDox('BlockInterface interface file is readable')]
    public function testBlockInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'BlockInterface.php'
        );
    }

    #[Depends('testBlockInterfaceInterfaceFileExists')]
    #[Depends('testBlockInterfaceInterfaceFileIsReadable')]
    #[TestDox('BlockInterface interface exists')]
    public function testBlockInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\BlockInterface'));
        $this->assertTrue(interface_exists(BlockInterface::class));
    }
}
