<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(PlaceholderImage::class)]
final class PlaceholderImageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP placeholder image class is concrete')]
    public function testAmpPlaceholderImageClassIsConcrete(): void
    {
        $class = new ReflectionClass(PlaceholderImage::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP placeholder image is an AMP image')]
    public function testAmpPlaceholderImageIsAnAmpImage(): void
    {
        $amp_placeholder_image = new PlaceholderImage();
        $this->assertInstanceOf(\StoreCore\AMP\Image::class, $amp_placeholder_image);
        $this->assertInstanceOf(Image::class, $amp_placeholder_image);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PlaceholderImage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PlaceholderImage::VERSION);
        $this->assertIsString(PlaceholderImage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PlaceholderImage::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('PlaceholderImage::__toString exists')]
    public function testPlaceholderImageToStringExists(): void
    {
        $class = new ReflectionClass(PlaceholderImage::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testPlaceholderImageToStringExists')]
    #[TestDox('PlaceholderImage::__toString is public')]
    public function testPlaceholderImageToStringIsPublic(): void
    {
        $method = new \ReflectionMethod(PlaceholderImage::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testPlaceholderImageToStringExists')]
    #[Depends('testPlaceholderImageToStringIsPublic')]
    #[TestDox('PlaceholderImage::__toString returns <amp-img> tag')]
    public function testPlaceholderImageToStringReturnsAmpImgTag(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringStartsWith('<amp-img ', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString returns </amp-img> closing tag')]
    public function testPlaceholderImageToStringReturnsAmpImgClosingTag(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringContainsString('</amp-img>', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString returns `placeholder` attribute by default')]
    public function testPlaceholderImageToStringReturnsPlaceholderAttribute()
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringContainsString(' placeholder', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString returns layout="fill" attribute')]
    public function testPlaceholderImageToStringReturnsLayoutIsFillAttribute(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringContainsString(' layout="fill"', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString does not return `alt` attribute')]
    public function testPlaceholderImageToStringDoesNotReturnAltAttribute(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringNotContainsString(' alt=', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString does not return `height` attribute')]
    public function testPlaceholderImageToStringDoesNotReturnHeightAttribute(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringNotContainsString(' height=', (string) $placeholder_image);
    }

    #[Depends('testPlaceholderImageToStringReturnsAmpImgTag')]
    #[TestDox('PlaceholderImage::__toString does not return `width` attribute')]
    public function testPlaceholderImageToStringDoesNotReturnWidthAttribute(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertStringNotContainsString(' width=', (string) $placeholder_image);
    }


    #[TestDox('PlaceholderImage::getLayout returns `fill` by default')]
    public function testPlaceholderImageGetLayoutReturnsFillByDefault(): void
    {
        $placeholder_image = new PlaceholderImage();
        $this->assertEquals('fill', $placeholder_image->getLayout());
    }
}
