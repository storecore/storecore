<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(Layout::class)]
final class LayoutTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP Layout class is concrete')]
    public function testAmpLayoutClassIsConcrete(): void
    {
        $class = new ReflectionClass(Layout::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Layout class extends AbstractComponent')]
    public function testAmpLayoutClassExtendsAbstractComponent(): void
    {
        $this->assertInstanceOf(AbstractComponent::class, new Layout());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Layout class implements LayoutInterface')]
    public function testAmpLayoutClassImplementsLayoutInterface(): void
    {
        $this->assertInstanceOf(LayoutInterface::class, new Layout());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Layout::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Layout::VERSION);
        $this->assertIsString(Layout::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Layout::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Layout::__toString() exists')]
    public function testLayoutToStringExists(): void
    {
        $class = new ReflectionClass('\StoreCore\AMP\Layout');
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testLayoutToStringExists')]
    #[TestDox('Layout::__toString() returns non-empty string')]
    public function testLayoutToStringReturnsNonEmptyString(): void
    {
        $component = new Layout();
        $string = (string) $component;
        $this->assertNotEmpty($string);
        $this->assertIsString($string);
    }

    #[Depends('testLayoutToStringReturnsNonEmptyString')]
    #[TestDox('Layout::__toString() returns <amp-layout> HTML container')]
    public function testLayoutToStringReturnsAmpLayoutHtmlContainer(): void
    {
        $component = new Layout();
        $amp_html = (string) $component;
        $this->assertStringStartsWith('<amp-layout ', $amp_html);
        $this->assertStringEndsWith('</amp-layout>', $amp_html);
        $this->assertEmpty(strip_tags($amp_html));
    }


    #[Group('hmvc')]
    #[TestDox('AMP LayoutInterface::LAYOUT_RESPONSIVE constant exists')]
    public function testAmpLayoutInterfaceLayoutResponsiveConstantExists(): void
    {
        $this->assertTrue(interface_exists(LayoutInterface::class));
        $this->assertNotEmpty(LayoutInterface::LAYOUT_RESPONSIVE);
    }

    #[Depends('testAmpLayoutInterfaceLayoutResponsiveConstantExists')]
    #[TestDox('AMP Layout component has responsive layout by default')]
    public function testAmpLayoutComponentHasResponsiveLayoutByDefault(): void
    {
        $component = new Layout();
        $this->assertEquals(LayoutInterface::LAYOUT_RESPONSIVE, $component->getLayout());
    }


    #[Group('hmvc')]
    #[TestDox('AMP LayoutInterface::LAYOUT_NODISPLAY constant exists')]
    public function testAmpLayoutInterfaceLayoutNodisplayConstantExists(): void
    {
        $this->assertNotEmpty(\StoreCore\AMP\LayoutInterface::LAYOUT_NODISPLAY);
        $this->assertNotEmpty(LayoutInterface::LAYOUT_NODISPLAY);
    }

    #[Depends('testAmpLayoutComponentHasResponsiveLayoutByDefault')]
    #[Depends('testAmpLayoutInterfaceLayoutNodisplayConstantExists')]
    #[TestDox('Layout::setLayout() supports `nodisplay` layout')]
    public function testLayoutSetLayoutSupportsNodisplayLayout(): void
    {
        $component = new Layout();
        $component->setLayout(LayoutInterface::LAYOUT_NODISPLAY);
        $this->assertEquals(LayoutInterface::LAYOUT_NODISPLAY, $component->getLayout());
        $this->assertEquals(LayoutInterface::LAYOUT_NODISPLAY, $component->layout);
    }


    #[TestDox('AMP Layout component default width is (string) 1')]
    public function testAmpLayoutComponentDefaultWidthIsStringOne(): void
    {
        $layout = new Layout();
        $this->assertIsNotString($layout->width);
        $this->assertIsInt($layout->width);

        $this->assertEquals('1', $layout->width);
        $this->assertStringContainsString(' width="1"', (string) $layout);
    }

    #[TestDox('AMP Layout component default height is (string) 1')]
    public function testAmpLayoutComponentDefaultHeightIsOne(): void
    {
        $layout = new Layout();
        $this->assertIsNotString($layout->height);
        $this->assertIsInt($layout->height);

        $this->assertEquals('1', $layout->height);
        $this->assertStringContainsString(' height="1"', (string) $layout);
    }
}
