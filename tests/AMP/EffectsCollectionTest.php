<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(EffectsCollection::class)]
#[UsesClass(Image::class)]
final class EffectsCollectionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AMP Effects Collection class exists')]
    public function testAmpEffectsCollectionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'EffectsCollection.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'EffectsCollection.php');

        $this->assertTrue(class_exists('\\StoreCore\\AMP\\EffectsCollection'));
        $this->assertTrue(class_exists(EffectsCollection::class));
    }

    #[Group('hmvc')]
    #[TestDox('EffectsCollection class is concrete')]
    public function testEffectsCollectionClassIsConcrete(): void
    {
        $class = new ReflectionClass(EffectsCollection::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('EffectsCollection class is final')]
    public function testEffectsCollectionClassIsFinal(): void
    {
        $class = new ReflectionClass(EffectsCollection::class);
        $this->assertTrue($class->isFinal());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EffectsCollection::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EffectsCollection::VERSION);
        $this->assertIsString(EffectsCollection::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EffectsCollection::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('`parallax` and `fly-in-bottom` cannot be combined')]
    public function testParallaxAndFlyInBottomCannotBeCombined(): void
    {
        $fx = new EffectsCollection();
        $fx->parallax = true;
        $fx->flyInBottom = true;

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }

    #[TestDox('`parallax` and `fly-in-top` cannot be combined')]
    public function testParallaxAndFlyInTopCannotBeCombined(): void
    {
        $fx = new EffectsCollection();
        $fx->parallax = true;
        $fx->flyInTop = true;

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }

    #[TestDox('`fly-in-left` and `fly-in-right` cannot be combined')]
    public function testFlyInLeftAndFlyInRightCannotBeCombined(): void
    {
        $fx = new EffectsCollection();
        $fx->flyInLeft = true;
        $fx->flyInRight = true;

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }

    #[TestDox('`fade-in` and `fade-in-scroll` cannot be combined')]
    public function testFadeInAndFadeInScrollCannotBeCombined(): void
    {
        $fx = new EffectsCollection();
        $fx->fadeIn = true;
        $fx->fadeInScroll = true;

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }


    #[TestDox('EffectsCollection.dataDuration exists')]
    public function testEffectsCollectionDataDurationExists(): void
    {
        $this->assertObjectHasProperty('dataDuration', new EffectsCollection());
    }

    #[Depends('testEffectsCollectionDataDurationExists')]
    #[TestDox('EffectsCollection.dataDuration is (string) `1000ms` by default')]
    public function testEffectsCollectionDataDurationIsString1000msByDefault(): void
    {
        $fx = new EffectsCollection();
        $this->assertNotEmpty($fx->dataDuration);
        $this->assertIsString($fx->dataDuration);
        $this->assertEquals('1000ms', $fx->dataDuration);
    }

    #[Depends('testEffectsCollectionDataDurationIsString1000msByDefault')]
    #[TestDox('EffectsCollection.dataDuration can be set to string')]
    public function testEffectsCollectionDataDurationCanBeSetToString(): void
    {
        $fx = new EffectsCollection();
        $fx->dataDuration = '2000ms';
        $this->assertNotEquals('1000ms', $fx->dataDuration);
        $this->assertEquals('2000ms', $fx->dataDuration);
    }

    #[Depends('testEffectsCollectionDataDurationIsString1000msByDefault')]
    #[TestDox('EffectsCollection.dataDuration can be set using integer')]
    public function testEffectsCollectionDataDurationCanBeSetUsingInteger(): void
    {
        $fx = new EffectsCollection();
        $fx->dataDuration = 2500;
        $this->assertNotEquals('1000ms', $fx->dataDuration);
        $this->assertEquals('2500ms', $fx->dataDuration);
    }


    #[TestDox('EffectsCollection.dataMarginStart exists')]
    public function testEffectsCollectionDataMarginStartExists(): void
    {
        $this->assertObjectHasProperty('dataMarginStart', new EffectsCollection());
    }

    #[Depends('testEffectsCollectionDataMarginStartExists')]
    #[TestDox('EffectsCollection.dataMarginStart must be a percentage value')]
    public function testEffectsCollectionDataMarginStartBustBePercentageValue(): void
    {
        $fx = new EffectsCollection();
        $this->assertNotEmpty($fx->dataMarginStart);
        $this->assertIsString($fx->dataMarginStart);
        $this->assertStringEndsWith('%', $fx->dataMarginStart);
        $this->assertEquals(1, preg_match('/^\d+(?:\.\d+)?%$/', $fx->dataMarginStart));
    }

    #[Depends('testEffectsCollectionDataMarginStartBustBePercentageValue')]
    #[TestDox('EffectsCollection.dataMarginStart is 0% by default')]
    public function testEffectsCollectionDataMarginStartIsZerpPercentByDefault(): void
    {
        $fx = new EffectsCollection();
        $this->assertEquals('0%', $fx->dataMarginStart);
    }

    #[Depends('testEffectsCollectionDataMarginStartBustBePercentageValue')]
    #[TestDox('EffectsCollection.dataMarginStart can be set using integer')]
    public function testEffectsCollectionDataMarginStartCanBeSetUsingInteger(): void
    {
        $fx = new EffectsCollection();
        $fx->dataMarginStart = 15;
        $this->assertNotEquals('0%', $fx->dataMarginStart);
        $this->assertEquals('15%', $fx->dataMarginStart);
    }

    #[Depends('testEffectsCollectionDataMarginStartBustBePercentageValue')]
    #[Depends('testEffectsCollectionDataMarginStartCanBeSetUsingInteger')]
    #[TestDox('EffectsCollection.dataMarginStart must be a percentage value between 0% and 100%')]
    public function testEffectsCollectionDataMarginStartMustBePercentageValueBetweenZeroPercentAndHundredPercent()
    {
        $fx = new EffectsCollection();

        $fx->dataMarginStart = -10;
        $this->assertEquals('0%', $fx->dataMarginStart);

        $fx->dataMarginStart = 110;
        $this->assertEquals('100%', $fx->dataMarginStart);
    }

    #[TestDox('`data-margin-end` must be greater than `data-margin-start`')]
    public function testDataMarginEndMustBeGreaterThanDataMarginStart(): void
    {
        $fx = new EffectsCollection();
        $fx->fadeInScroll = true;
        $fx->dataMarginStart = 60;
        $fx->dataMarginEnd = 40;

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }

    #[Depends('testDataMarginEndMustBeGreaterThanDataMarginStart')]
    #[TestDox('`data-margin-end` and `data-margin-start` MUST NOT be equal')]
    public function testDataMarginEndAndDataMarginStartMustNotBeEqual(): void
    {
        $fx = new EffectsCollection();
        $fx->fadeInScroll = true;
        $fx->dataMarginStart = '50%';

        $this->expectException(BlockRenderException::class);
        $failure = $fx->__toString();
    }


    /**
     * @see https://amp.dev/documentation/components/amp-fx-collection
     */
    #[TestDox('AMP `parallax` visual effect acceptance test')]
    public function testAmpParallaxVisualEffectAcceptanceTest(): void
    {
        $expected
            = '<h1 amp-fx="parallax" data-parallax-factor="1.5">'
            .   'A title that moves faster than other content.'
            . '</h1>';

        $html = new EffectsCollection();
        $html->outerHTML = '<h1>A title that moves faster than other content.</h1>';
        $html->setParallax(1.5);

        $this->assertTrue($html->parallax);
        $this->assertEquals(1.5, $html->dataParallaxFactor);
        $this->assertEquals($expected, (string) $html);
    }

    #[TestDox('AMP `fade-in` visual effect acceptance test')]
    public function testAmpFadeInVisualEffectAcceptanceTest(): void
    {
        $expected 
            = '<div amp-fx="fade-in" data-duration="2000ms">'
            .   '<amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img>'
            . '</div>';

        $html = new EffectsCollection();
        $html->outerHTML = '<div><amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img></div>';
        $html->fadeIn = true;
        $html->dataDuration = 2000;

        $this->assertTrue($html->fadeIn);
        $this->assertEquals('2000ms', $html->dataDuration);
        $this->assertEquals($expected, (string) $html);
    }

    #[TestDox('AMP `fade-in-scroll` visual effect acceptance tests')]
    public function testAmpFadeInScrollVisualEffectAcceptanceTests(): void
    {
        $expected 
            = '<div amp-fx="fade-in-scroll" data-margin-end="80%">'
            . '<amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img>'
            . '</div>';

        $html = new EffectsCollection();
        $html->outerHTML = '<div><amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img></div>';
        $html->fadeInScroll = true;
        $html->dataMarginEnd = '80%';

        $this->assertTrue($html->fadeInScroll);
        $this->assertEquals('0%', $html->dataMarginStart);
        $this->assertEquals('80%', $html->dataMarginEnd);
        $this->assertFalse($html->dataRepeat);
        $this->assertEquals($expected, (string) $html);

        // Similar animation with `data-repeat`:
        $expected 
            = '<div amp-fx="fade-in-scroll" data-repeat>'
            . '<amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img>'
            . '</div>';

        $html = new EffectsCollection();
        $html->outerHTML = '<div><amp-img width="1600" height="900" layout="responsive" src="https://picsum.photos/1600/900?image=1069"></amp-img></div>';
        $html->fadeInScroll = true;
        $html->dataRepeat = true;

        $this->assertTrue($html->fadeInScroll);
        $this->assertTrue($html->dataRepeat);
        $this->assertEquals($expected, (string) $html);
    }

    /**
     * @see https://amp.dev/documentation/examples/components/amp-fx-collection/#title-parallax
     */
    #[TestDox('Title parallax acceptance test')]
    public function testTitleParallaxAcceptanceTest(): void
    {
        $expected 
            = '<h1 amp-fx="parallax" data-parallax-factor="1.3">'
            . '<span class="title">Lorem Ipsum</span>'
            . '</h1>';

        $title = new EffectsCollection();
        $title->outerHTML = '<h1><span class="title">Lorem Ipsum</span></h1>';
        $title->setParallax(1.3);

        $this->assertTrue($title->parallax);
        $this->assertEquals(1.3, $title->dataParallaxFactor);
        $this->assertEquals($expected, (string) $title);
    }

    #[Depends('testTitleParallaxAcceptanceTest')]
    #[TestDox('Text without tags creates a `<div>`')]
    public function testTextWithoutTagsCreatesDiv(): void
    {
        $expected 
            = '<div amp-fx="parallax" data-parallax-factor="1.3">'
            . 'Lorem Ipsum'
            . '</div>';

        $title = new EffectsCollection();
        $title->outerHTML = 'Lorem Ipsum';
        $title->setParallax(1.3);
        $this->assertEquals($expected, (string) $title);
    }

    /**
     * @see https://amp.dev/documentation/examples/components/amp-fx-collection/#scroll-triggered-fade-in
     */
    #[TestDox('Scroll-triggered fade-in acceptance test')]
    public function testScrollTriggeredFadeInAcceptanceTest(): void
    {
        $expected = '<amp-img layout="responsive" alt="" width="1600" height="900" loading="lazy" src="https://images.unsplash.com/photo-1524495195760-3266feca5b15?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7a508f55ae88c8eac7bab9380acded01&auto=format&fit=crop&w=1567&q=80" decoding="async" amp-fx="fade-in"></amp-img>';

        $image = new Image();
        $image->width = 1600;
        $image->height = 900;
        $image->src = 'https://images.unsplash.com/photo-1524495195760-3266feca5b15?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7a508f55ae88c8eac7bab9380acded01&auto=format&fit=crop&w=1567&q=80';

        $html = new EffectsCollection();
        $html->outerHTML = $image;
        $html->fadeIn = true;
        $this->assertEquals($expected, (string) $html);
    }

    /**
     * @see https://amp.dev/documentation/examples/components/amp-fx-collection/#slow-fade-in-animation
     */
    #[TestDox('Slow fade-in animation acceptance test')]
    public function testSlowFadeInAnimationAcceptanceTest(): void
    {
        $expected = '<amp-img layout="responsive" alt="Picture of a road" width="1280" height="873" loading="lazy" src="https://images.unsplash.com/photo-1524063853276-ab560795d106?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9bc985b4c36017cfaf3e7359254b1e93&auto=format&fit=crop&w=1950&q=80" decoding="async" amp-fx="fade-in" data-margin-start="50%"></amp-img>';

        $image = new Image();
        $image->width = 1280;
        $image->height = 873;
        $image->src = 'https://images.unsplash.com/photo-1524063853276-ab560795d106?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9bc985b4c36017cfaf3e7359254b1e93&auto=format&fit=crop&w=1950&q=80';
        $image->alt = 'Picture of a road';

        $html = new EffectsCollection();
        $html->outerHTML = $image;
        $html->fadeIn = true;
        $html->dataMarginStart = '50%';
        $this->assertEquals($expected, (string) $html);
    }
}
