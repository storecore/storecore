<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(AbstractAnalytics::class)]
#[Group('hmvc')]
final class AbstractAnalyticsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AbstractAnalytics class is abstract')]
    public function testAbstractAnalyticsClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->isAbstract());
    }

    #[TestDox('AbstractAnalytics is JSON serializable')]
    public function testAbstractAnalyticsIsJsonSerializable(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));

    }

    #[TestDox('AbstractAnalytics is stringable')]
    public function testAbstractAnalyticsIsStringable(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->implementsInterface(\Stringable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AbstractAnalytics::VERSION);
        $this->assertIsString(AbstractAnalytics::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AbstractAnalytics::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('TYPE constant is defined')]
    public function testTypeConstantIsDefined(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->hasConstant('TYPE'));
    }

    #[Depends('testTypeConstantIsDefined')]
    #[TestDox('TYPE constant is null by default')]
    public function testTypeConstantIsNullByDefault(): void
    {
        $this->assertNull(AbstractAnalytics::TYPE);
    }


    #[TestDox('AbstractAnalytics.dataCredentials exists')]
    public function testAbstractAnalyticsDataCredentialsExists(): void
    {
        $class = new ReflectionClass(AbstractAnalytics::class);
        $this->assertTrue($class->hasProperty('dataCredentials'));
    }

    #[Depends('testAbstractAnalyticsDataCredentialsExists')]
    #[TestDox('AbstractAnalytics::$dataCredentials is (bool) false by default')]
    public function testAbstractAnalyticsDataCredentialsBoolFalseByDefault(): void
    {
        $property = new ReflectionProperty(AbstractAnalytics::class, 'dataCredentials');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertIsBool($property->getDefaultValue());
        $this->assertFalse($property->getDefaultValue());
    }


    #[TestDox('AbstractAnalytics::__toString returns <amp-analytics> element')]
    public function testAbstractAnalyticsToStringReturnsAmpAnalyticsElement(): void
    {
        $analytics = new class extends AbstractAnalytics {
            public const TYPE = 'nielsen';
            protected array $data = [
                "vars" => [
                    "apid" => "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                    "apv" => "1.0",
                    "apn" => "My AMP Website",
                    "section" => "Entertainment",
                    "segA" => "Music",
                    "segB" => "News",
                    "segC" => "Google AMP"
                ]
            ];
        };

        $html = $analytics->__toString();
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
        $this->assertStringStartsWith('<amp-analytics', $html);
        $this->assertStringStartsWith('<amp-analytics type="nielsen">', $html);
        $this->assertStringEndsWith('</amp-analytics>', $html);
    }
}
