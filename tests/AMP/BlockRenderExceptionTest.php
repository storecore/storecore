<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class BlockRenderExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('BlockRenderException class file exists')]
    public function testBlockRenderExceptionClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'BlockRenderException.php'
        );
    }

    #[Depends('testBlockRenderExceptionClassFileExists')]
    #[Group('distro')]
    #[TestDox('BlockRenderException class file is readable')]
    public function testBlockRenderExceptionClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'BlockRenderException.php'
        );
    }

    #[Group('distro')]
    #[TestDox('BlockRenderException class exists')]
    public function testBlockRenderExceptionClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\AMP\\BlockRenderException'));
        $this->assertTrue(class_exists(BlockRenderException::class));
    }


    #[Group('hmvc')]
    #[TestDox('BlockRenderException is an Exception')]
    public function testBlockRenderExceptionIsAnException(): void
    {
        $this->expectException(\Exception::class);
        throw new BlockRenderException();
    }

    #[Group('hmvc')]
    #[TestDox('BlockRenderException is an SPL LogicException')]
    public function testBlockRenderExceptionIsAnSplLogicException(): void
    {
        $this->expectException(\LogicException::class);
        throw new BlockRenderException();
    }
}
