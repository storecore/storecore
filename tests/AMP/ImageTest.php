<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\AMP\Image::class)]
#[CoversClass(\StoreCore\AMP\FallbackImage::class)]
#[UsesClass(\StoreCore\CMS\HTMLImageElement::class)]
final class ImageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AMP Image class is concrete')]
    public function testAmpImageClassIsConcrete(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AMP Image is an HTML image')]
    public function testAmpImageIsHtmlImage(): void
    {
        $image = new Image();
        $this->assertInstanceOf(\StoreCore\AMP\Image::class, $image);
        $this->assertInstanceOf(\StoreCore\CMS\HTMLImageElement::class, $image);
    }

    #[Group('hmvc')]
    #[TestDox('AMP image implements AMP BlockInterface')]
    public function testAmpImageImplementsAmpBlockInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\BlockInterface::class, new Image());
    }

    #[Group('hmvc')]
    #[TestDox('AMP image implements AMP LayoutInterface')]
    public function testAmpImageImplementsAmpLayoutInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\LayoutInterface'));
        $this->assertTrue(interface_exists(LayoutInterface::class));

        $this->assertInstanceOf(LayoutInterface::class, new Image());
    }

    #[Depends('testAmpImageImplementsAmpBlockInterface')]
    #[Depends('testAmpImageImplementsAmpLayoutInterface')]
    #[TestDox('AMP image default `layout` is `responsive`')]
    public function testAmpImageDefaultLayoutIsResponsive(): void
    {
        $image = new Image();
        $this->assertEquals(LayoutInterface::LAYOUT_RESPONSIVE, $image->getLayout());
        $this->assertStringContainsString(' layout="responsive"', (string) $image);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Image::VERSION);
        $this->assertIsString(Image::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Image::VERSION, '0.2.3', '>=')
        );
    }


    #[TestDox('Image::__toString exists')]
    public function testImageToStringExists(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testImageToStringExists')]
    #[TestDox('Image::__toString is public')]
    public function testImageToStringIsPublic(): void
    {
        $method = new ReflectionMethod(Image::class, '__toString');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testImageToStringExists')]
    #[Depends('testImageToStringIsPublic')]
    #[TestDox('Image::__toString returns non-empty string')]
    public function testImageToStringReturnsNonEmptyString(): void
    {
        $image = new Image();
        $this->assertNotEmpty($image->__toString());
        $this->assertIsString($image->__toString());
    }

    #[Depends('testImageToStringReturnsNonEmptyString')]
    #[TestDox('Image::__toString returns <amp-img> tag')]
    public function testImageToStringReturnsAmpImgTag(): void
    {
        $img = new Image();
        $this->assertStringStartsWith('<amp-img ', (string) $img);
        $this->assertStringEndsWith('</amp-img>', (string) $img);
    }

    #[Depends('testImageToStringReturnsAmpImgTag')]
    #[TestDox('Image::__toString returns layout="responsive" attribute by default')]
    public function testImageToStringReturnsLayoutIsResponsiveAttributeByDefault(): void
    {
        $img = new Image();
        $this->assertStringContainsString(' layout="responsive"', (string)$img);
    }


    #[TestDox('Image::fallback exists')]
    public function testImageFallbackExists(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertTrue($class->hasMethod('fallback'));
    }

    #[Depends('testImageFallbackExists')]
    #[TestDox('Image::fallback is public')]
    public function testImageFallbackIsPublic(): void
    {
        $method = new ReflectionMethod(Image::class, 'fallback');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testImageFallbackExists')]
    #[TestDox('Image::fallback has one OPTIONAL parameter')]
    public function testImageFallbackHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Image::class, 'fallback');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImageFallbackExists')]
    #[Depends('testImageFallbackIsPublic')]
    #[Depends('testImageFallbackHasOneOptionalParameter')]
    #[TestDox('Image::fallback returns null by default')]
    public function testImageFallbackReturnsNullByDefault(): void
    {
        $image = new Image();
        $this->assertNull($image->fallback());
        $this->assertNull($image->fallback);
    }

    /**
     * @see https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/placeholders#example:-serve-different-image-formats
     *      “Example: Serve different image formats” in “Placeholders & fallbacks”, AMP Guides & Tutorials
     */
    #[TestDox('Image::fallback sets AMP FallbackImage')]
    public function testImageFallbackSetsAmpFallbackImage(): void
    {
        $fallback = new FallbackImage();
        $fallback->alt = 'Mountains';
        $fallback->width = 550;
        $fallback->height = 368;
        $fallback->src = '/static/inline-examples/images/mountains.jpg"';

        $image = new Image();
        $image->alt = 'Mountains';
        $image->width = 550;
        $image->height = 368;
        $image->src = '/static/inline-examples/images/mountains.webp"';

        $this->assertNull($image->fallback);

        $image->fallback = $fallback;
        $this->assertNotNull($image->fallback);
        $this->assertInstanceOf(FallbackImage::class, $image->fallback);

        $html = $image->__toString();
        $this->assertStringStartsWith('<amp-img ', $html);
        $this->assertStringContainsString('<amp-img fallback ', $html);
        $this->assertStringStartsNotWith('<amp-img fallback ', $html);
    }


    #[TestDox('Image::getLayout exists')]
    public function testImageGetLayoutExists(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertTrue($class->hasMethod('getLayout'));
    }

    #[Depends('testImageGetLayoutExists')]
    #[TestDox('Image::getLayout is public')]
    public function testImageGetLayoutIsPublic(): void
    {
        $method = new ReflectionMethod(Image::class, 'getLayout');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testImageGetLayoutExists')]
    #[TestDox('Image::getLayout has no parameters')]
    public function testImageGetLayoutHasNoParameters(): void
    {
        $method = new ReflectionMethod(Image::class, 'getLayout');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testImageGetLayoutExists')]
    #[Depends('testImageGetLayoutIsPublic')]
    #[Depends('testImageGetLayoutHasNoParameters')]
    #[TestDox('Image::getLayout returns `responsive` by default')]
    public function testImageGetLayoutReturnsResponsiveByDefault(): void
    {
        $image = new Image();
        $this->assertEquals('responsive', $image->getLayout());
    }


    #[TestDox('Image::setLightbox exists')]
    public function testImageSetLightboxExists(): void
    {
        $class = new ReflectionClass(Image::class);
        $this->assertTrue($class->hasMethod('setLightbox'));
    }

    #[Depends('testImageSetLightboxExists')]
    #[TestDox('Image::setLightbox is public')]
    public function testImageSetLightboxIsPublic(): void
    {
        $method = new ReflectionMethod(Image::class, 'setLightbox');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testImageSetLightboxExists')]
    #[TestDox('Image::setLightbox has one OPTIONAL parameter')]
    public function testImageSetLightboxHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Image::class, 'setLightbox');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }
}
