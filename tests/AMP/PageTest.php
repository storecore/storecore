<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(Page::class)]
final class PageTest extends TestCase
{
    #[TestDox('Page class is concrete')]
    public function testPageClassIsAConcrete(): void
    {
        $class = new ReflectionClass(Page::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Page class is stringable StoreCore\View')]
    public function testPageClassIsStringableStoreCoreView(): void
    {
        $page = new Page();
        $this->assertInstanceOf(\Stringable::class, $page);
        $this->assertInstanceOf(\StoreCore\View::class, $page);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Page::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Page::VERSION);
        $this->assertIsString(Page::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Page::VERSION, '0.1.1', '>=')
        );
    }


    #[TestDox('Page::write exists')]
    public function testPageWriteExists(): void
    {
        $class = new ReflectionClass(Page::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testPageWriteExists')]
    #[TestDox('Page::write is public')]
    public function testPageWriteIsPublic(): void
    {
        $method = new ReflectionMethod(Page::class, 'write');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testPageWriteExists')]
    #[TestDox('Page::write has one REQUIRED parameter')]
    public function testPageWriteHasNoParameters(): void
    {
        $method = new ReflectionMethod(Page::class, 'write');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfParameters());
    }
}
