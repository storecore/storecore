<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class LayoutInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('LayoutInterface interface file exists')]
    public function testLayoutInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'LayoutInterface.php'
        );
    }

    #[Depends('testLayoutInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('LayoutInterface interface file is readable')]
    public function testLayoutInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AMP' . DIRECTORY_SEPARATOR . 'LayoutInterface.php'
        );
    }

    #[Depends('testLayoutInterfaceInterfaceFileExists')]
    #[Depends('testLayoutInterfaceInterfaceFileIsReadable')]
    #[Group('distro')]
    #[TestDox('LayoutInterface interface exists')]
    public function testLayoutInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\AMP\\LayoutInterface'));
        $this->assertTrue(interface_exists(LayoutInterface::class));
    }
}
