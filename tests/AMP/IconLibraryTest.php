<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2021, 2023–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass;
use \ReflectionClassConstant;
use \ReflectionMethod;
use \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(IconLibrary::class)]
#[UsesClass(\StoreCore\AMP\Image::class)]
#[UsesClass(\StoreCore\CMS\HTMLImageElement::class)]
final class IconLibraryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(IconLibrary::VERSION);
        $this->assertIsString(IconLibrary::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(IconLibrary::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('IconLibrary::CODE_POINTS exists')]
    public function testIconLibraryCodePointsExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasConstant('CODE_POINTS'));
    }

    #[Depends('testIconLibraryCodePointsExists')]
    #[TestDox('IconLibrary::CODE_POINTS is protected')]
    public function testIconLibraryCodePointsIsProtected(): void
    {
        $constant = new ReflectionClassConstant(IconLibrary::class, 'CODE_POINTS');
        $this->assertTrue($constant->isProtected());
    }


    #[TestDox('IconLibrary::PATHS exists')]
    public function testIconLibraryPathsExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasConstant('PATHS'));
    }

    #[Depends('testIconLibraryPathsExists')]
    #[TestDox('IconLibrary::PATHS is protected')]
    public function testIconLibraryPathsIsProtected(): void
    {
        $constant = new ReflectionClassConstant(IconLibrary::class, 'PATHS');
        $this->assertTrue($constant->isProtected());
    }


    #[TestDox('IconLibrary::getCharacter exists')]
    public function testIconLibraryGetCharacterExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasMethod('getCharacter'));
    }

    #[Depends('testIconLibraryGetCharacterExists')]
    #[TestDox('IconLibrary::getCharacter is public')]
    public function testIconLibraryGetCharacterIsPublic(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getCharacter');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testIconLibraryGetCharacterExists')]
    #[TestDox('IconLibrary::getCharacter has one REQUIRED parameter')]
    public function testIconLibraryGetCharacterHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getCharacter');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testIconLibraryGetCharacterExists')]
    #[TestDox('IconLibrary::getCharacter returns non-empty string')]
    public function testIconLibraryGetCharacterReturnsNonEmptyString(): void
    {
        $library = new IconLibrary();
        $this->assertNotEmpty($library->getCharacter('home'));
        $this->assertIsString($library->getCharacter('home'));
    }

    #[Depends('testIconLibraryGetCharacterReturnsNonEmptyString')]
    #[TestDox('IconLibrary::getCharacter returns HTML character entity')]
    public function testGetCharacterMethodReturnsHtmlCharacterEntity(): void
    {
        $charmap = [
            'home'          => '&#xE88A;',
            'search'        => '&#xE8B6;',
            'shopping_cart' => '&#xE8CC;',
        ];

        $library = new IconLibrary();
        foreach ($charmap as $input => $output) {
            $this->assertEquals($output, $library->getCharacter($input));
        }
    }

    #[Depends('testGetCharacterMethodReturnsHtmlCharacterEntity')]
    #[TestDox('IconLibrary::getCharacter is case-insensitive')]
    public function testIconLibraryGetCharacterIsCaseInsensitive(): void
    {
        $library = new IconLibrary();
        $this->assertEquals($library->getCharacter('Shopping_Cart'), $library->getCharacter('shopping_cart'));
        $this->assertEquals($library->getCharacter('SHOPPING_CART'), $library->getCharacter('Shopping_Cart'));
        $this->assertEquals($library->getCharacter('shopping_cart'), $library->getCharacter('SHOPPING_CART'));
    }

    #[Depends('testGetCharacterMethodReturnsHtmlCharacterEntity')]
    #[TestDox('IconLibrary::getCharacter accepts underscores and spaces')]
    public function testIconLibraryGetCharacterAcceptsUnderscoresAndSpaces(): void
    {
        $library = new IconLibrary();
        $this->assertEquals($library->getCharacter('shopping basket'), $library->getCharacter('shopping_basket'));
        $this->assertEquals($library->getCharacter('shopping cart'), $library->getCharacter('shopping_cart'));
    }

    #[Depends('testGetCharacterMethodReturnsHtmlCharacterEntity')]
    #[TestDox('IconLibrary::getCharacter throws ou of range exception if icon does not exist')]
    public function testIconLibraryGetCharacterThrowsOutOfRangeExceptionIfIconDoesNotExist(): void
    {
        $iconLibrary = new IconLibrary();
        $this->expectException(\OutOfRangeException::class);
        $failure = $iconLibrary->getCharacter('no_fo_bar');
    }


    #[TestDox('IconLibrary::getDataURI exists')]
    public function testIconLibraryGetDataUriExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasMethod('getDataURI'));
    }

    #[Depends('testIconLibraryGetDataUriExists')]
    #[TestDox('IconLibrary::getDataURI is public')]
    public function testIconLibraryGetDataUriIsPublic(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getDataURI');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testIconLibraryGetDataUriExists')]
    #[TestDox('IconLibrary::getDataURI has one REQUIRED parameter')]
    public function testIconLibraryGetDataUriHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getDataURI');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testIconLibraryGetDataUriExists')]
    #[Depends('testIconLibraryGetDataUriIsPublic')]
    #[Depends('testIconLibraryGetDataUriHasOneRequiredParameter')]
    #[TestDox('IconLibrary::getDataURI returns non-empty string')]
    public function testIconLibraryGetDataUriReturnsNonEmptyString(): void
    {
        $library = new IconLibrary();
        $this->assertNotEmpty($library->getDataURI('Home'));
        $this->assertIsString($library->getDataURI('Home'));
    }

    #[Depends('testIconLibraryGetDataUriReturnsNonEmptyString')]
    #[TestDox('IconLibrary::getDataURI returns SVG image data URI')]
    public function testIconLibraryGetDataUriReturnsSvgImageDataUri(): void
    {
        $library = new IconLibrary();
        $this->assertStringStartsWith('data:image/svg+xml;', $library->getDataURI('Home'));
    }

    #[Depends('testIconLibraryGetDataUriReturnsNonEmptyString')]
    #[Depends('testIconLibraryGetDataUriReturnsSvgImageDataUri')]
    #[TestDox('IconLibrary::getDataURI returns Base64 encoded data URI')]
    public function testIconLibraryGetDataUriReturnsBase64EncodedDataUri(): void
    {
        $library = new IconLibrary();
        $icon = $library->getDataURI('Home');
        $this->assertStringStartsWith('data:', $icon);
        $this->assertStringContainsString(';base64,', $icon);
    }

    #[Depends('testIconLibraryGetDataUriReturnsNonEmptyString')]
    #[Depends('testIconLibraryGetDataUriReturnsSvgImageDataUri')]
    #[TestDox('IconLibrary::getDataURI returns null if icon does not exist')]
    public function testIconLibraryGetDataUriReturnsNullIfIconDoesNotExist(): void
    {
        $library = new IconLibrary();
        $this->assertNull($library->getDataURI('icon_that_does_not_exist'));
    }


    #[TestDox('IconLibrary::getImage exists')]
    public function testIconLibraryGetImageExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasMethod('getImage'));
    }

    #[Depends('testIconLibraryGetImageExists')]
    #[TestDox('IconLibrary::getImage is public')]
    public function testIconLibraryGetImageIsPublic(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getImage');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testIconLibraryGetImageExists')]
    #[TestDox('IconLibrary::getImage has one REQUIRED parameter')]
    public function testIconLibraryGetImageHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getImage');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testIconLibraryGetImageExists')]
    #[Depends('testIconLibraryGetImageIsPublic')]
    #[Depends('testIconLibraryGetImageHasOneRequiredParameter')]
    #[TestDox('IconLibrary::getImage returns stringable AMP image object')]
    public function testIconLibraryGetImageReturnsStringableAmpImageObject(): void
    {
        $library = new IconLibrary();
        $image = $library->getImage('shopping_basket');
        $this->assertIsObject($image);
        $this->assertInStanceOf(\Stringable::class, $image);
        $this->assertInStanceOf(\StoreCore\AMP\Image::class, $image);
    }

    #[Depends('testIconLibraryGetImageHasOneRequiredParameter')]
    #[TestDox('IconLibrary::getImage returns image with fixed size of 24 by 24 pixels')]
    public function testIconLibraryGetReturnsImageWithFixedSizeOf24By24Pixels(): void
    {
        $library = new IconLibrary();
        $image = $library->getImage('shopping_basket');
        $this->assertEquals('fixed', $image->getLayout());
        $this->assertEquals(24, $image->width);
        $this->assertEquals(24, $image->height);
    }


    #[TestDox('IconLibrary::getSVG exists')]
    public function testIconLibraryGetSvgExists(): void
    {
        $class = new ReflectionClass(IconLibrary::class);
        $this->assertTrue($class->hasMethod('getSVG'));
    }

    #[Depends('testIconLibraryGetSvgExists')]
    #[TestDox('IconLibrary::getSVG is public')]
    public function testIconLibraryGetSvgIsPublic(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getSVG');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testIconLibraryGetSvgExists')]
    #[TestDox('IconLibrary::getSVG has one REQUIRED parameter')]
    public function testIconLibraryGetSvgHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(IconLibrary::class, 'getSVG');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testIconLibraryGetSvgExists')]
    #[Depends('testIconLibraryGetSvgIsPublic')]
    #[Depends('testIconLibraryGetSvgHasOneRequiredParameter')]
    #[TestDox('IconLibrary::getSVG returns non-empty string')]
    public function testIconLibraryGetSvgReturnsNonEmptyString(): void
    {
        $library = new IconLibrary();
        $this->assertNotEmpty($library->getSVG('Home'));
        $this->assertIsString($library->getSVG('Home'));
    }

    #[Depends('testIconLibraryGetSvgReturnsNonEmptyString')]
    #[TestDox('IconLibrary::getSVG parameter is case insensitive')]
    public function testIconLibraryGetSvgParameterIsCaseInsensitive(): void
    {
        $library = new IconLibrary();
        $this->assertSame($library->getSVG('home'), $library->getSVG('HOME'));
        $this->assertSame($library->getSVG('HOME'), $library->getSVG('Home'));
        $this->assertSame($library->getSVG('Home'), $library->getSVG('home'));
    }

    #[Depends('testIconLibraryGetSvgReturnsNonEmptyString')]
    #[TestDox('IconLibrary::getSVG returns SVG tag')]
    public function testIconLibraryGetSvgReturnsSvgTag(): void
    {
        $library = new IconLibrary();
        $icon = $library->getSVG('Home');
        $this->assertStringStartsWith('<svg ', $icon);
        $this->assertStringContainsString('xmlns="http://www.w3.org/2000/svg"', $icon);
        $this->assertStringEndsWith('</svg>', $icon);
    }
}
