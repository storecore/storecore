<?php

declare(strict_types=1);

namespace StoreCore\AMP;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(FallbackImage::class)]
final class FallbackImageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('FallbackImage class is concrete')]
    public function testFallbackImageClassIsConcrete(): void
    {
        $class = new ReflectionClass(FallbackImage::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('FallbackImage class is final')]
    public function testFallbackImageClassIsFinal(): void
    {
        $class = new ReflectionClass(FallbackImage::class);
        $this->assertTrue($class->isFinal());
    }

    #[Group('hmvc')]
    #[TestDox('AMP fallback image is an AMP image')]
    public function testAmpFallbackImageIsAnAmpImage(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\Image::class, new FallbackImage());
    }

    #[Group('hmvc')]
    #[TestDox('AMP fallback image implements Stringable BlockInterface')]
    public function testAmoFallbackImageImplementsStringableBlockInterface(): void
    {
        $fallback_image = new FallbackImage();
        $this->assertInstanceOf(\Stringable::class, $fallback_image);
        $this->assertInstanceOf(\StoreCore\AMP\BlockInterface::class, $fallback_image);
    }

    #[Group('hmvc')]
    #[TestDox('AMP fallback image implements AMP LayoutInterface')]
    public function testAmoFallbackImageImplementsAmpLayoutInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\AMP\LayoutInterface::class, new FallbackImage());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FallbackImage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FallbackImage::VERSION);
        $this->assertIsString(FallbackImage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FallbackImage::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('FallbackImage::__toString exists')]
    public function testFallbackImageToStringExists(): void
    {
        $class = new ReflectionClass(FallbackImage::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('FallbackImage::__toString is public')]
    public function testFallbackImageToStringIsPublic(): void
    {
        $method = new ReflectionMethod(FallbackImage::class, '__toString');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testFallbackImageToStringExists')]
    #[Depends('testFallbackImageToStringIsPublic')]
    #[TestDox('FallbackImage::__toString returns non-empty string')]
    public function testFallbackImageToStringReturnsNonEmptyString(): void
    {
        $image = new FallbackImage();
        $this->assertNotEmpty((string) $image);
        $this->assertIsString((string) $image);
    }

    #[Depends('testFallbackImageToStringReturnsNonEmptyString')]
    #[TestDox('FallbackImage::__toString returns <amp-img> tag')]
    public function testFallbackImageToStringreturnsAmpImgTag(): void
    {
        $image = new FallbackImage();
        $this->assertStringStartsWith('<amp-img ', (string) $image);
        $this->assertStringEndsWith('</amp-img>', (string) $image);
    }

    #[Depends('testFallbackImageToStringreturnsAmpImgTag')]
    #[TestDox('FallbackImage::__toString returns `placeholder` attribute by default')]
    public function testFallbackImageToStringReturnsFallbackAttributeByDefault(): void
    {
        $image = new FallbackImage();
        $this->assertStringContainsString(' fallback', (string) $image);
    }
}
