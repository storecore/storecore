<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\RoutingQueue::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Route::class)]
class RoutingQueueTest extends TestCase
{
    #[TestDox('RoutingQueue class is concrete')]
    public function testRoutingQueueClassIsConcrete(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('RoutingQueue is countable')]
    public function testRoutingQueueIsCountable(): void
    {
        $queue = new RoutingQueue();
        $this->assertInstanceOf(\Countable::class, $queue);
    }

    #[TestDox('RoutingQueue implements PHP Iterator interface')]
    public function testRoutingQueueImplementsPhpIteratorInterface()
    {
        $queue = new RoutingQueue();
        $this->assertInstanceOf(\Iterator::class, $queue);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RoutingQueue::VERSION);
        $this->assertIsString(RoutingQueue::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RoutingQueue::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('RoutingQueue::count exists')]
    public function testRoutingQueueCountExists(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('RoutingQueue::count is public')]
    public function testRoutingQueueCountIsPublic(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'count');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('RoutingQueue::count has no parameters')]
    public function testRoutingQueueCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('RoutingQueue::dispatch exists')]
    public function testRoutingQueueDispatchExists(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertTrue($class->hasMethod('dispatch'));
    }

    #[TestDox('RoutingQueue::dispatch is public')]
    public function testRoutingQueueDispatchIsPublic(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'dispatch');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('RoutingQueue::dispatch has no parameters')]
    public function testRoutingQueueDispatchHasNoParameters(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'dispatch');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('RoutingQueue::insert exists')]
    public function testRoutingQueueInsertExists(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertTrue($class->hasMethod('insert'));
    }

    #[TestDox('RoutingQueue::insert is public')]
    public function testRoutingQueueInsertIsPublic(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'insert');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('RoutingQueue::insert has two parameters')]
    public function testRoutingQueueInsertHasTwoParameters(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'insert');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testRoutingQueueInsertHasTwoParameters')]
    #[TestDox('RoutingQueue::insert has one REQUIRED parameter')]
    public function testRoutingQueueInsertHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'insert');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('RoutingQueue::setExtractFlags exists')]
    public function testRoutingQueueSetExtractFlagsExists(): void
    {
        $class = new ReflectionClass(RoutingQueue::class);
        $this->assertTrue($class->hasMethod('setExtractFlags'));
    }

    #[TestDox('RoutingQueue::setExtractFlags is public')]
    public function testRoutingQueueSetExtractFlagsIsPublic(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'setExtractFlags');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('RoutingQueue::setExtractFlags has one REQUIRED parameter')]
    public function testRoutingQueueSetExtractFlagsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RoutingQueue::class, 'setExtractFlags');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
