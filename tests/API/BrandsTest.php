<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use StoreCore\Registry;
use StoreCore\Engine\ServerRequestFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\API\Brands::class)]
#[CoversClass(\StoreCore\API\RequestHandler::class)]
#[CoversClass(\StoreCore\Database\BrandMapper::class)]
#[CoversClass(\StoreCore\Database\BrandRepository::class)]
#[CoversClass(\StoreCore\PIM\Brand::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Engine\AbstractStream::class)]
#[UsesClass(\StoreCore\Engine\Message::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\StreamFactory::class)]
#[UsesClass(\StoreCore\Engine\TemporaryStream::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class BrandsTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_brands`');

            // Add three fictional brands with a UUID v1, v4, and v7:
            $registry->get('Database')->exec(
                "DELETE
                   FROM `sc_brands`
                  WHERE `global_brand_name` IN ('Octan', 'Alchemax', 'Buy-n-Large')"
            );

            $registry->get('Database')->exec(
                "INSERT INTO `sc_brands` 
                     (`brand_uuid`, `global_brand_name`) 
                   VALUES
                     (UNHEX('11ad4874312611ef94540242ac120002'), 'Octan'),
                     (UNHEX('1ca34aef243044f0b0dd61e7b690a3ba'), 'Alchemax'),
                     (UNHEX('019043aee96770848be7996929a2629b'), 'Buy-n-Large')"
            );

        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('Brands API class is concrete')]
    public function testBrandsApiClassIsConcrete(): void
    {
        $class = new ReflectionClass(Brands::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Brands API class is a controller')]
    public function testBrandsApiClassIsController(): void
    {
        $registry = Registry::getInstance();
        $controller = new Brands($registry);
        $this->assertInstanceOf(\StoreCore\AbstractController::class, $controller);
    }

    #[Group('hmvc')]
    #[TestDox('Brands API controller implements PSR-15 RequestHandlerInterface')]
    public function testBrandsApiControllerImplementsPsr15RequestHandlerInterface(): void
    {
        $registry = Registry::getInstance();
        $controller = new Brands($registry);
        $this->assertInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class, $controller);
    }

    #[Depends('testBrandsApiControllerImplementsPsr15RequestHandlerInterface')]
    #[TestDox('Brands::handle returns PSR-7 ResponseInterface')]
    public function testBrandsHandleReturnsPsr7ResponseInterface(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/api/v1/brands');

        $registry = Registry::getInstance();
        $controller = new Brands($registry);

        $this->assertInstanceOf(
            \Psr\Http\Message\ResponseInterface::class,
            $controller->handle($request)
        );
    }

    #[TestDox('Brands::handle returns response with Schema.org Brand in JSON-LD')]
    public function testBrandsHandleReturnsResponseWithSchemaOrgBrandInJsonLd(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/api/v1/brands/1ca34aef-2430-44f0-b0dd-61e7b690a3ba');

        $registry = Registry::getInstance();
        $controller = new Brands($registry);

        $response = $controller->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }
}
