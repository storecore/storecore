<?php

declare(strict_types=1);

namespace StoreCore\API;

use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\Engine\{RequestFactory, ServerRequestFactory};

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\{Group, Large};
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[Large]
#[CoversClass(\StoreCore\API\RequestHandler::class)]
#[CoversClass(\StoreCore\API\NotFoundException::class)]
#[CoversClass(\StoreCore\Engine\RequestFactory::class)]
#[CoversClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
final class RequestHandlerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('API request handler class is concrete')]
    public function testApiRequestHandlerClassIsConcrete(): void
    {
        $class = new ReflectionClass(RequestHandler::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('API request handler is a controller')]
    public function testApiRequestHandlerIsController(): void
    {
        $registry = Registry::getInstance();
        $api = new RequestHandler($registry);
        $this->assertInstanceOf(\StoreCore\AbstractController::class, $api);
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-15 RequestHandlerInterface exists')]
    public function testImplementedPSR15RequestHandlerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
    }

    #[Depends('testImplementedPSR15RequestHandlerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('API request handler implements PSR-15 RequestHandlerInterface')]
    public function testApiRequestHandlerImplementsPsr15RequestHandlerInterface(): void
    {
        $registry = Registry::getInstance();
        $api = new RequestHandler($registry);
        $this->assertInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class, $api);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RequestHandler::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RequestHandler::VERSION);
        $this->assertIsString(RequestHandler::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RequestHandler::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('RequestHandler::__construct exists')]
    public function testRequestHandlerConstructorExists(): void
    {
        $class = new ReflectionClass(RequestHandler::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testRequestHandlerConstructorExists')]
    #[TestDox('RequestHandler::__construct is public constructor')]
    public function testRequestHandlerConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testRequestHandlerConstructorExists')]
    #[TestDox('RequestHandler::__construct has one REQUIRED parameter')]
    public function testRequestHandlerConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('RequestHandler::handle exists')]
    public function testRequestHandlerHandleExists(): void
    {
        $class = new ReflectionClass(RequestHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testRequestHandlerHandleExists')]
    #[TestDox('RequestHandler::handle is public')]
    public function testRequestHandlerHandleIsPublic(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, 'handle');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestHandlerHandleExists')]
    #[TestDox('RequestHandler::handle has one REQUIRED parameter')]
    public function testRequestHandlerHandleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestHandlerHandleHasOneRequiredParameter')]
    #[TestDox('RequestHandler::handle throws type error exception if parameter is not a server request')]
    public function testRequestHandlerHandleThrowsTypeErrorIfParameterIsNotAServerRequest(): void
    {
        $factory = new RequestFactory();
        $request = $factory->createRequest('GET', '/api/v1/');
        $api = new RequestHandler(Registry::getInstance());

        $this->expectException(\TypeError::class);
        $api->handle($request);
    }

    #[Depends('testRequestHandlerHandleHasOneRequiredParameter')]
    #[TestDox('RequestHandler::handle returns instance of PSR-7 Psr\Http\Message\ResponseInterface')]
    public function testRequestHandlerHandleReturnsInstanceOfPsr7PsrHttpMessageResponseInterface(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', '/api/v1');
        $api = new RequestHandler(Registry::getInstance());
        $response = $api->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }

    #[TestDox('RequestHandler::handle returns 405 Method Not Allowed response on invalid HTTP method')]
    public function testRequestHandlerHandleReturns405MethodNotAllowedResponseOnInvalidHttpMethod(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('TRACE', '/api/v1');
        $api = new RequestHandler(Registry::getInstance());
        $response = $api->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
        $this->assertEquals(405, $response->getStatusCode());
        $this->assertEquals('Method Not Allowed', $response->getReasonPhrase());

        if ($response->getStatusCode() === 405) {
            $this->assertTrue(
                $response->hasHeader('Allow'),
                'A 405 Method Not Allowed response MUST contain an Allow header.'
            );
        }
    }

    #[TestDox('RequestHandler::handle throws 404 Not Found client exception on invalid API root')]
    public function testndlerHandleThrows404NotFoundClientExceptionOnInvalidApiRoot(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', '/api/v0');
        $api = new RequestHandler(Registry::getInstance());

        $this->expectException(\Psr\Http\Client\ClientExceptionInterface::class);
        $this->expectException(NotFoundException::class);
        $failure = $api->handle($request);
    }

    #[TestDox('RequestHandler::handle throws 404 Not Found client exception if API endpoint does not exist')]
    public function testRequestHandlerHandleThrows404NotFoundClientExceptionIfApiEndpointDoesNotExist(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', '/api/v1/foo/bar');
        $api = new RequestHandler(Registry::getInstance());

        $this->expectException(NotFoundException::class);
        $failure = $api->handle($request);
    }


    #[Test]
    #[TestDox('Public API endpoints test')]
    public function testPublicApiEndPointsTest(): void
    {
        $api = new RequestHandler(Registry::getInstance());
        $requestFactory = new ServerRequestFactory();

        $endpoints = [
            '/api/v1',
            '/api/v1/brands',
            '/api/v1/brands:count',
            '/api/v1/brands/1ca34aef-2430-44f0-b0dd-61e7b690a3ba',
        ];

        foreach ($endpoints as $endpoint) {
            $request = $requestFactory->createServerRequest('GET', $endpoint);
            $response = $api->handle($request);
            $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
            $this->assertEquals(200, $response->getStatusCode());
        }
    }


    #[TestDox('RequestHandler::jsonEncode exists')]
    public function testRequestHandlerJsonEncodeExists(): void
    {
        $class = new ReflectionClass(RequestHandler::class);
        $this->assertTrue($class->hasMethod('jsonEncode'));
    }

    #[Depends('testRequestHandlerJsonEncodeExists')]
    #[TestDox('RequestHandler::jsonEncode is public static')]
    public function testRequestHandlerJsonEncodeIsPublicStatic(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, 'jsonEncode');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testRequestHandlerJsonEncodeExists')]
    #[TestDox('RequestHandler::jsonEncode has one REQUIRED parameter')]
    public function testRequestHandlerJsonEncodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(RequestHandler::class, 'jsonEncode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestHandlerJsonEncodeHasOneRequiredParameter')]
    #[TestDox('RequestHandler::jsonEncode converts JSON serializable object to JSON-LD string')]
    public function testRequestHandlerJsonEncodeConvertsJsonSerializableObjectToJsonLdString(): void
    {
        $api = new RequestHandler(Registry::getInstance());
        $provider = new Organization('StoreCore');
        $this->assertInstanceOf(\JsonSerializable::class, $provider);

        $json = RequestHandler::jsonEncode($provider);
        $this->assertNotEmpty($json);
        $this->assertIsString($json);
    }
}
