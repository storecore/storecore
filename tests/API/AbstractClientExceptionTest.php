<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\API;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractClientExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractClientException class exists')]
    public function testAbstractClientExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'API' . DIRECTORY_SEPARATOR . 'AbstractClientException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'API' . DIRECTORY_SEPARATOR . 'AbstractClientException.php');

        $this->assertTrue(class_exists('\\StoreCore\\API\\AbstractClientException'));
        $this->assertTrue(class_exists(AbstractClientException::class));
    }

    #[TestDox('AbstractClientException is abstract')]
    public function testAbstractClientExceptionIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractClientException::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractClientException is a throwable exception')]
    public function testAbstractClientExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(AbstractClientException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
    }

    #[Depends('testAbstractClientExceptionIsThrowableException')]
    #[TestDox('AbstractClientException is a runtime exception')]
    public function testAbstractClientExceptionIsRuntimeException(): void
    {
        $class = new ReflectionClass(AbstractClientException::class);
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-18 ClientExceptionInterface exists')]
    public function testImplementedPsr18ClientExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Client\\ClientExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Client\ClientExceptionInterface::class));
    }

    #[Depends('testImplementedPsr18ClientExceptionInterfaceExists')]
    #[TestDox('AbstractClientException implements PSR-18 ClientExceptionInterface')]
    public function testAbstractClientExceptionImplementsPsr18ClientExceptionInterface(): void
    {
        $class = new ReflectionClass(AbstractClientException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Client\ClientExceptionInterface::class));
    }
}
