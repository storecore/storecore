<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\SubjectObservers::class)]
final class SubjectObserversTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SubjectObservers class exists')]
    public function testSubjectObserversClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'SubjectObservers.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'SubjectObservers.php');

        $this->assertTrue(class_exists('\\StoreCore\\SubjectObservers'));
        $this->assertTrue(class_exists(SubjectObservers::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SubjectObservers::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SubjectObservers::VERSION);
        $this->assertIsString(SubjectObservers::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SubjectObservers::VERSION, '2.0.0', '>=')
        );
    }


    #[TestDox('SubjectObservers::populate() exists')]
    public function testSubjectObserversPopulateExists(): void
    {
        $class = new ReflectionClass(SubjectObservers::class);
        $this->assertTrue($class->hasMethod('populate'));
    }

    #[TestDox('SubjectObservers::populate() is public static')]
    public function testSubjectObserversPopulateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(SubjectObservers::class, 'populate');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('SubjectObservers::populate() has one REQUIRED parameter')]
    public function testSubjectObserversPopulateHasOneRequiredParameter(): void
    {
        $method = new \ReflectionMethod(\StoreCore\SubjectObservers::class, 'populate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
