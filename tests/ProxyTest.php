<?php

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Proxy::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ProxyTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Proxy class is concrete')]
    public function testProxyClassIsConcrete(): void
    {
        $class = new ReflectionClass(Proxy::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Proxy class is read-only')]
    public function testProxyClassIsReadOnly(): void
    {
        $class = new ReflectionClass(Proxy::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('Proxy class implements StoreCore ProxyInterface')]
    public function testProxyClassImplementsProxyInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertTrue(interface_exists('\\StoreCore\\ProxyInterface'));

        $proxy = new Proxy('00000000-0000-0000-0000-000000000000', 'Thing');
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Proxy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Proxy::VERSION);
        $this->assertIsString(Proxy::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Proxy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Proxy::__construct exists')]
    public function testProxyConstructExists(): void
    {
        $class = new ReflectionClass(Proxy::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Proxy::__construct is public constructor')]
    public function testProxyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Proxy::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Proxy::__construct has three parameters')]
    public function testProxyConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Proxy::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testProxyConstructorHasThreeParameters')]
    #[TestDox('Proxy::__construct has one REQUIRED parameter')]
    public function testProxyConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Proxy::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Proxy::__construct accepts identifier as UUID string')]
    public function testProxyConstructorAcceptsIdentifierAsUuidString(): void
    {
        $proxy = new Proxy('4f9bf80e-b794-45d3-87e5-321dc68d5747');
        $this->assertTrue($proxy->hasIdentifier());
        $this->assertEquals('4f9bf80e-b794-45d3-87e5-321dc68d5747', $proxy->identifier);
        $this->assertEquals('4f9bf80e-b794-45d3-87e5-321dc68d5747', (string) $proxy->getIdentifier());
        $this->assertEquals('4f9bf80e-b794-45d3-87e5-321dc68d5747', $proxy->getIdentifier()->__toString());
        $this->assertEquals('4f9bf80e-b794-45d3-87e5-321dc68d5747', (string) $proxy);
    }

    #[TestDox('Proxy::__construct accepts identifier as UUID object')]
    public function testProxyConstructorAcceptsIdentifierAsUuidObject(): void
    {
        $uuid = UUIDFactory::randomUUID();
        $proxy = new Proxy($uuid);
        $this->assertSame((string) $uuid, $proxy->identifier);
        $this->assertSame((string) $uuid, (string) $proxy);

        $this->assertTrue($proxy->hasIdentifier());
        $this->assertEquals($uuid, $proxy->getIdentifier());
    }


    #[TestDox('Proxy::jsonSerialize exists')]
    public function testProxyJsonSerializeExists(): void
    {
        $class = new ReflectionClass(Proxy::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[TestDox('Proxy::jsonSerialize is public')]
    public function testProxyJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(Proxy::class, 'jsonSerialize');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Proxy::jsonSerialize has no parameters')]
    public function testProxyJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(Proxy::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testProxyJsonSerializeExists')]
    #[Depends('testProxyJsonSerializeIsPublic')]
    #[Depends('testProxyJsonSerializeHasNoParameters')]
    #[TestDox('Proxy::jsonSerialize returns non-empty array')]
    public function testProxyJsonSerializeReturnsNonEmptyArray(): void
    {
        $proxy = new Proxy('4f9bf80e-b794-45d3-87e5-321dc68d5747');
        $this->assertNotEmpty($proxy->jsonSerialize());
        $this->assertIsArray($proxy->jsonSerialize());
    }

    #[Depends('testProxyJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('Proxy is JSON serializable')]
    public function testProxyIsJsonSerializable(): void
    {
        $expectedJson = trim('
            {
              "@id": "4f9bf80e-b794-45d3-87e5-321dc68d5747",
              "@context": "https://schema.org",
              "@type": "Thing",
              "identifier": "4f9bf80e-b794-45d3-87e5-321dc68d5747"
            }
        ');

        $proxy = new Proxy('4f9bf80e-b794-45d3-87e5-321dc68d5747');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        $expectedJson = trim('
            {
              "@id": "0724e406-4f42-edfd-df80-f0f22f00b545",
              "@context": "https://schema.org",
              "@type": "Book",
              "identifier": "0724e406-4f42-edfd-df80-f0f22f00b545",
              "name": "The Catcher in the Rye"
            }
        ');

        $proxy = new Proxy(md5('9787543321724'), 'Book', 'The Catcher in the Rye');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
