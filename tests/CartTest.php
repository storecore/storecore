<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\OML\CartID;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Cart::class)]
#[UsesClass(\StoreCore\OML\CartID::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class CartTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Cart class is concrete')]
    public function testCartClassIsConcrete(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Cart is a model')]
    public function testCartIsModel(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
        $this->assertFalse($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Depends('testCartIsModel')]
    #[Group('hmvc')]
    #[TestDox('Cart is an order model')]
    public function testCartIsOrderModel(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Order::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Cart::VERSION);
        $this->assertIsString(Cart::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Cart::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Cart::getCartID() exists')]
    public function testCartGetCartIDExists(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertTrue($class->hasMethod('getCartID'));
    }

    #[TestDox('Cart::getCartID() is public')]
    public function testCartGetCartIDIsPublic(): void
    {
        $method = new ReflectionMethod(Cart::class, 'getCartID');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Cart::getCartID() has no parameters')]
    public function testCartGetCartIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(Cart::class, 'getCartID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Cart::getCartID() returns null by default')]
    public function testCartGetCartIDReturnsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $cart = new Cart($registry);
        $this->assertNull($cart->getCartID());
    }


    #[TestDox('Cart::setCartID() exists')]
    public function testCartSetCartIDExists(): void
    {
        $class = new ReflectionClass(Cart::class);
        $this->assertTrue($class->hasMethod('setCartID'));
    }

    #[TestDox('Cart::setCartID() is public')]
    public function testCartSetCartIDIsPublic(): void
    {
        $method = new ReflectionMethod(Cart::class, 'setCartID');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Cart::setCartID() has one REQUIRED parameter')]
    public function testCartSetCartIDHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Cart::class, 'setCartID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Cart::setCartID() sets StoreCore\OML\CartID object')]
    public function testCartSetCartIDSetsStoreCoreOMLCartIDObject(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $cart = new Cart($registry);
        $cart_id = new CartID();
        $cart->setCartID($cart_id);

        $this->assertNotNull($cart->getCartID());
        $this->assertIsObject($cart->getCartID());
        $this->assertInstanceOf(\StoreCore\OML\CartID::class, $cart->getCartID());
        $this->assertSame($cart_id, $cart->getCartID());
    }
}
