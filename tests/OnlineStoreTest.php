<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \json_encode;
use function \version_compare;

#[CoversClass(\StoreCore\OnlineStore::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class OnlineStoreTest extends TestCase
{
    #[TestDox('OnlineStore class is concrete')]
    public function testOnlineStoreClassIsConcrete(): void
    {
        $class = new ReflectionClass(OnlineStore::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OnlineStore is an OnlineBusiness Organization')]
    public function testOnlineStoreIsOnlineBusinessOrganization(): void
    {
        $online_store = new OnlineStore();
        $this->assertInstanceOf(\StoreCore\OnlineBusiness::class, $online_store);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $online_store);
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('OnlineStore.additionalType is Schema.org OnlineBusiness')]
    public function testOnlineStoreAdditionalTypeIsSchemaOrgOnlineBusiness(): void
    {
        $online_store = new OnlineStore();
        $this->assertNotNull($online_store->additionalType);
        $this->assertInstanceOf(\Stringable::class, $online_store->additionalType);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $online_store->additionalType);
        $this->assertEquals('https://schema.org/OnlineBusiness', (string) $online_store->additionalType);
    }


    #[Group('hmvc')]
    #[TestDox('OnlineStore class implements StoreCore IdentityInterface')]
    public function testOnlineStoreClassImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new OnlineStore());
    }


    #[Group('hmvc')]
    #[TestDox('OnlineStore is JSON serializable')]
    public function testOnlineStoreIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new OnlineStore());
    }

    #[Depends('testOnlineStoreIsJsonSerializable')]
    #[TestDox('Schema.org @type is OnlineStore')]
    public function testSchemaOrgTypeIsOnlineStore(): void
    {
        $organization = new OnlineStore();
        $json = json_encode($organization);
        $this->assertStringContainsString('"@type":"OnlineStore"', $json);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OnlineStore::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OnlineStore::VERSION);
        $this->assertIsString(OnlineStore::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OnlineStore::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    #[TestDox('OnlineStore::hasIdentifier() exists')]
    public function testOnlineStoreHasIdentifierExists(): void
    {
        $class = new ReflectionClass(OnlineStore::class);
        $this->assertTrue($class->hasMethod('hasIdentifier'));
    }

    #[TestDox('OnlineStore::hasIdentifier is public')]
    public function testOnlineStoreHasIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(OnlineStore::class, 'hasIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OnlineStore::hasIdentifier has no parameters')]
    public function testOnlineStoreHasIdentifierNoParameters(): void
    {
        $method = new ReflectionMethod(OnlineStore::class, 'hasIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('OnlineStore::hasIdentifier returns bool')]
    public function testOnlineStoreHasIdentifierReturnsBool(): void
    {
        $store = new OnlineStore();
        $this->assertIsBool($store->hasIdentifier());
    }

    #[Depends('testOnlineStoreHasIdentifierReturnsBool')]
    #[TestDox('OnlineStore::hasIdentifier returns false by default')]
    public function testOnlineStoreHasIdentifierReturnsFalseByDefault(): void
    {
        $store = new OnlineStore();
        $this->assertNull($store->identifier);
        $this->assertFalse($store->hasIdentifier());
    }


    #[TestDox('OnlineStore.metadata exists')]
    public function testOnlineStoreMetadataExists(): void
    {
        $this->assertObjectHasProperty('metadata', new OnlineStore());
    }

    #[Depends('testOnlineStoreMetadataExists')]
    #[TestDox('OnlineStore.metadata is non-empty value object')]
    public function testOnlineStoreMetadataIsNonEmptyValueObject(): void
    {
        $store = new OnlineStore();
        $this->assertNotEmpty($store->metadata);
        $this->assertIsObject($store->metadata);
        $this->assertInstanceOf(\StoreCore\Database\Metadata::class, $store->metadata);
    }

    #[Depends('testOnlineStoreMetadataIsNonEmptyValueObject')]
    #[TestDox('OnlineStore.metadata.dateCreated is set to DateTimeInterface')]
    public function testOnlineStoreMetadataDateCreatedIsSetToDateTimeInterface(): void
    {
        $store = new OnlineStore();
        $this->assertNotEmpty($store->metadata->dateCreated);
        $this->assertInstanceOf(\DateTimeInterface::class, $store->metadata->dateCreated);
    }


    #[TestDox('OnlineStore.name exists')]
    public function testOnlineStoreNameExists(): void
    {
        $store = new OnlineStore();
        $this->assertEmpty($store->name);

        $store->name = 'Protocabulators incorporated';
        $this->assertNotEmpty($store->name);
        $this->assertIsString($store->name);
        $this->assertEquals('Protocabulators incorporated', $store->name);
    }


    #[TestDox('Examples')]
    public function testExamples(): void
    {
        $store = new OnlineStore();
        $store->name = 'Protocabulators incorporated';
        $store->description = 'Your premium source for first class protocabulators';
        $store->image = 'http://example.com/marketplace/sellerA.png';
        $store->logo = 'http://example.com/marketplace/sellerA_logo.png';

        $this->assertEquals('Protocabulators incorporated', $store->name);
        $this->assertEquals('Your premium source for first class protocabulators', $store->description);
        $this->assertEquals('http://example.com/marketplace/sellerA.png', (string) $store->image);
        $this->assertEquals('http://example.com/marketplace/sellerA_logo.png', (string) $store->logo);
    }
}
