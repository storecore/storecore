<?php

/**
 * Bootstrap for PHPUnit
 *
 * @version 0.6.0
 * 
 * @see https://help.zend.com/zend/zend-server-2019.0/content/configuring_php_for_performance.htm
 *      Configuring PHP for Performance
 */

declare(strict_types=1);

ini_set('error_reporting', 'E_ALL & ~E_NOTICE');
ini_set('max_execution_time', '300');
ini_set('memory_limit', '-1');
ini_set('output_buffering', '4096');
ini_set('realpath_cache_size', '256K');
ini_set('realpath_cache_ttl', '3600');
ini_set('register_argc_argv', 'Off');

ini_set('opcache.enable_cli', '1');
ini_set('opcache.fast_shutdown', '1');
ini_set('opcache.interned_strings_buffer', '8');
ini_set('opcache.max_accelerated_files', '4000');
ini_set('opcache.memory_consumption', '256');
ini_set('opcache.revalidate_freq', '600');
ini_set('opcache.save_comments', '0');

// Load configuration
require __DIR__ . '/../config/version.php';
require __DIR__ . '/../config/config.php';

// Load the StoreCore bootstrap and autoloader
require __DIR__ . '/../src/bootloader.php';

// Error reporting for dev environment
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

// Try to connect to a database
if (!$registry->has('Database')) {
    try {
        $registry->set('Database', new \StoreCore\Database\Connection());
    } catch (\PDOException $e) {
        // Log and ignore PDO exception
        $logger->debug('Executing unit tests without database.');
    }
}
