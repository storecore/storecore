<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\Large;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Autoloader::class)]
#[Group('hmvc')]
final class AutoloaderTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Autoloader class file exists')]
    public function testAutoloaderClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Autoloader.php');
    }

    #[Depends('testAutoloaderClassFileExists')]
    #[Group('distro')]
    #[TestDox('Autoloader class file is readable')]
    public function testAutoloaderClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Autoloader.php');
    }


    #[TestDox('Autoloader class is concrete')]
    public function testAutoloaderClassIsConcrete(): void
    {
        $class = new ReflectionClass(Autoloader::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Core bootloader.php file exists')]
    public function testCoreBootloaderPhpFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'bootloader.php');
    }

    #[Depends('testCoreBootloaderPhpFileExists')]
    #[Group('distro')]
    #[TestDox('Core bootloader.php file exists')]
    public function testCoreBootloaderPhpFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'bootloader.php');
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Autoloader::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Autoloader::VERSION);
        $this->assertIsString(Autoloader::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Autoloader::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Autoloader::addNamespace exists')]
    public function testAutoloaderAddNamespaceExists(): void
    {
        $class = new ReflectionClass(Autoloader::class);
        $this->assertTrue($class->hasMethod('addNamespace'));
    }

    #[Depends('testAutoloaderAddNamespaceExists')]
    #[TestDox('Autoloader::addNamespace is public')]
    public function tesAutoloaderAddNamespaceIsPublic(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'addNamespace');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[Depends('testAutoloaderAddNamespaceExists')]
    #[TestDox('Autoloader::addNamespace has three parameters')]
    public function testAutoloaderAddNamespaceThreeParameters(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'addNamespace');
        $this->assertEquals(3, $method->getNumberOfParameters());

    }

    #[Depends('testAutoloaderAddNamespaceThreeParameters')]
    #[TestDox('Autoloader::addNamespace has two REQUIRED parameters')]
    public function testAutoloaderAddNamespaceTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'addNamespace');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[Depends('testAutoloaderAddNamespaceThreeParameters')]
    #[TestDox('Autoloader::loadClass exists')]
    public function testAutoloaderLoadClassExists(): void
    {
        $class = new ReflectionClass(Autoloader::class);
        $this->assertTrue($class->hasMethod('loadClass'));
    }

    #[Depends('testAutoloaderLoadClassExists')]
    #[TestDox('Autoloader::loadClass is public')]
    public function tesAutoloaderLoadClassIsPublic(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'loadClass');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAutoloaderLoadClassExists')]
    #[TestDox('Autoloader::loadClass has one REQUIRED parameter')]
    public function testAutoloaderLoadClassHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'loadClass');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Autoloader::register exists')]
    public function testAutoloaderRegisterExists(): void
    {
        $class = new ReflectionClass(Autoloader::class);
        $this->assertTrue($class->hasMethod('register'));
    }

    #[Depends('testAutoloaderRegisterExists')]
    #[TestDox('Autoloader::register is public')]
    public function tesAutoloaderRegisterIsPublic(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'register');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAutoloaderRegisterExists')]
    #[TestDox('Autoloader::register has no parameters')]
    public function testAutoloaderAddNamespaceNoParameters(): void
    {
        $method = new ReflectionMethod(Autoloader::class, 'register');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
