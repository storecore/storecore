<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Document::class)]
final class DocumentTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Core Document class is concrete')]
    public function testCoreDocumentClassIsConcrete(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Core Document class is stringable')]
    public function testDocumentClassIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Document());
    }


    #[Group('hmvc')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Document::VERSION);
        $this->assertIsString(Document::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('hmvc')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(Document::VERSION, '0.1.0', '>='));
    }


    #[Group('seo')]
    #[TestDox('Document always has a <title>')]
    public function testDocumentAlwaysHasATitle(): void
    {
        $document = new Document();
        $this->assertStringContainsString('<title>', $document->getHead());
        $this->assertStringContainsString('</title>', $document->getHead());
    }

    #[Group('seo')]
    #[TestDox('Document <title> can be set')]
    public function testDocumentTitleCanBeSet(): void
    {
        $title = 'Foo Bar';

        $document = new Document();
        $this->assertStringNotContainsString('Foo Bar', $document->getHead());
        $document->setTitle($title);
        $this->assertStringContainsString('<title>Foo Bar</title>', $document->getHead());

        $document = null;

        $document = new Document($title);
        $this->assertStringContainsString('<title>Foo Bar</title>', $document->getHead());
    }


    #[TestDox('Empty <body> has closing tag')]
    public function testEmptyBodyHasClosingTag(): void
    {
        $document = new Document();
        $this->assertEquals('<body></body>', $document->getBody());
    }
}
