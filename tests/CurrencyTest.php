<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Currency::class)]
final class CurrencyTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Currency class is concrete')]
    public function testCurrencyClassIsConcrete(): void
    {
        $class = new ReflectionClass(Currency::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Currency class is read-only')]
    public function testCurrencyClassIsReadOnly(): void
    {
        $class = new ReflectionClass(Currency::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('hmvc')]
    #[TestDox('Currency class is stringable')]
    public function testCurrencyClassIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Currency());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Currency::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Currency::VERSION);
        $this->assertIsString(Currency::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Currency::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('Currency::__construct exists')]
    public function testCurrencyConstructorExists(): void
    {
        $class = new ReflectionClass(Currency::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testCurrencyConstructorExists')]
    #[TestDox('Currency::__construct is public constructor')]
    public function testCurrencyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Currency::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testCurrencyConstructorExists')]
    #[TestDox('Currency::__construct has four OPTIONAL parameters')]
    public function testCurrencyConstructorHasFourOptionalParameters(): void
    {
        $method = new ReflectionMethod(Currency::class, '__construct');
        $this->assertEquals(4, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCurrencyConstructorHasFourOptionalParameters')]
    #[TestDox('Currency::__construct creates euro by default')]
    public function testCurrencyConstructorCreatesEuroByDefault(): void
    {
        $euro = new Currency(978, 'EUR', 2, '€');
        $currency = new Currency();
        $this->assertEquals($euro, $currency);
    }


    #[TestDox('Currency::__toString exists')]
    public function testCurrencyToStringExists(): void
    {
        $class = new ReflectionClass(Currency::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testCurrencyToStringExists')]
    #[TestDox('Currency::__toString returns non-empty string')]
    public function testCurrencyToStringReturnsNonEmptyString(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->__toString());
        $this->assertIsString($currency->__toString());
    }

    #[Depends('testCurrencyToStringReturnsNonEmptyString')]
    #[TestDox('Currency::__toString returns EUR currency code by default')]
    public function testCurrencyToStringReturnsEurCurrencyCodeByDefault(): void
    {
        $currency = new Currency();
        $this->assertEquals('EUR', $currency->__toString());
        $this->assertEquals('EUR', (string) $currency);
    }


    #[TestDox('Currency.code exists')]
    public function testCurrencyCodeExists(): void
    {
        $this->assertObjectHasProperty('code', new Currency());
    }

    #[Depends('testCurrencyCodeExists')]
    #[TestDox('Currency.code is public read-only')]
    public function testCurrencyCodeIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Currency::class, 'code');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testCurrencyCodeExists')]
    #[Depends('testCurrencyCodeIsPublicReadOnly')]
    #[TestDox('Currency.code is non-empty string')]
    public function testCurrencyCodeIsNonEmptyString(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->code);
        $this->assertIsString($currency->code);
    }

    #[Depends('testCurrencyCodeIsNonEmptyString')]
    #[TestDox('Currency.code is `EUR` currency code by default')]
    public function testCurrencyCodeIsEURCurrencyCodeByDefault(): void
    {
        $currency = new Currency();
        $this->assertEquals('EUR', $currency->code);
    }

    #[Depends('testCurrencyCodeIsNonEmptyString')]
    #[TestDox('Currency::__construct sets currency code')]
    public function testCurrencyConstructSetsCurrencyCode(): void
    {
        $currency = new Currency(124, 'CAD', 2, '$');
        $this->assertEquals('CAD', $currency->code);

        $this->expectException(\ValueError::class);
        $currency = new Currency(840, 'US$', 2, '$');
    }


    #[TestDox('Currency.id exists')]
    public function testCurrencyIdExists(): void
    {
        $this->assertObjectHasProperty('id', new Currency());
    }

    #[Depends('testCurrencyIdExists')]
    #[TestDox('Currency.id is public read-only')]
    public function testCurrencyIdIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Currency::class, 'id');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testCurrencyIdIsPublicReadOnly')]
    #[TestDox('Currency.id is a small integer')]
    public function testCurrencyIdIsSmallInteger(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->id);
        $this->assertIsNotString($currency->id);
        $this->assertIsInt($currency->id);

        $this->assertGreaterThanOrEqual(8, $currency->id);
        $this->assertLessThan(1000, $currency->id);
    }

    #[Depends('testCurrencyIdIsSmallInteger')]
    #[TestDox('Currency.id is (int) 978 by default')]
    public function testCurrencyIdIsInt978ByDefault(): void
    {
        $currency = new Currency();
        $this->assertEquals(978, $currency->id);
    }


    #[TestDox('Currency.number exists')]
    public function testCurrencyNumberExists(): void
    {
        $this->assertObjectHasProperty('number', new Currency());
    }

    #[Depends('testCurrencyNumberExists')]
    #[TestDox('Currency.number is public read-only')]
    public function testCurrencyNumberIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Currency::class, 'number');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testCurrencyNumberIsPublicReadOnly')]
    #[TestDox('Currency.number is a non-empty string')]
    public function testCurrencyNumberIsNonEmptyString(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->number);
        $this->assertIsString($currency->number);
    }

    #[Depends('testCurrencyNumberIsNonEmptyString')]
    #[TestDox('Currency.number is three-character number')]
    public function testCurrencyNumberIsThreeCharacterNumber(): void
    {
        $currency = new Currency();
        $this->assertTrue(is_numeric($currency->number));
        $this->assertTrue(ctype_digit($currency->number));
        $this->assertEquals(3, strlen($currency->number));
    }

    #[Depends('testCurrencyNumberIsNonEmptyString')]
    #[TestDox('Currency.number is string `978` for Euro by default')]
    public function testCurrencyNumberIsString978ForEuroByDefault(): void
    {
        $currency = new Currency();
        $this->assertIsString($currency->number);
        $this->assertEquals('978', $currency->number);
    }

    #[TestDox('Currency.number accepts integer as number')]
    public function testCurrencyNumberAcceptsIntegerAsNumber(): void
    {
        $currency = new Currency(8, 'ALL', 2, 'L');
        $this->assertEquals(8, $currency->id);
        $this->assertEquals('008', $currency->number);

        $currency = new Currency('008', 'ALL', 2, 'L');
        $this->assertEquals(8, $currency->id);
        $this->assertEquals('008', $currency->number);

        $this->expectException(\TypeError::class);
        $failure = new Currency(8.0, 'ALL', 2, 'L');
    }

    #[Depends('testCurrencyNumberAcceptsIntegerAsNumber')]
    #[TestDox('Currency.number can not be less than 8')]
    public function testCurrencyNumberCanNotBeLessThan8(): void
    {
        $this->expectException(\ValueError::class);
        $failure = new Currency(7, 'FOO', 2, '¤');
    }

    #[Depends('testCurrencyNumberAcceptsIntegerAsNumber')]
    #[TestDox('Currency.number can not be greater than 999')]
    public function testCurrencyNumberCanNotBeGreaterThan999(): void
    {
        $currency = new Currency(999, 'XXX', 0, '¤');
        $this->assertEquals(999, $currency->id);
        $this->assertEquals('999', $currency->number);

        $this->expectException(\ValueError::class);
        $failure = new Currency(1000, 'QUX', 2, '¤');
    }


    #[TestDox('Currency.precision exists')]
    public function testCurrencyPrecisionExists(): void
    {
        $this->assertObjectHasProperty('precision', new Currency());
    }

    #[Depends('testCurrencyPrecisionExists')]
    #[TestDox('Currency.precision is public read-only')]
    public function testCurrencyPrecisionIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Currency::class, 'precision');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testCurrencyPrecisionIsPublicReadOnly')]
    #[TestDox('Currency.precision is a small integer')]
    public function testCurrencyPrecisionIsSmallInteger(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->precision);
        $this->assertIsInt($currency->precision);

        $this->assertGreaterThanOrEqual(0, $currency->precision);
        $this->assertLessThanOrEqual(4, $currency->precision);
    }

    #[Depends('testCurrencyPrecisionIsSmallInteger')]
    #[TestDox('Currency.precision is 2 by default')]
    public function testCurrencyPrecisionIs2ByDefault(): void
    {
        $currency = new Currency();
        $this->assertEquals(2, $currency->precision);
    }

    #[Depends('testCurrencyPrecisionIsSmallInteger')]
    #[TestDox('Currency.precision can not be less than 0')]
    public function testCurrencyPrecisionCanNotBeLessThan0(): void
    {
        $currency = new Currency(978, 'EUR', 0, '€');
        $this->assertEquals(0, $currency->precision);

        $this->expectException(\ValueError::class);
        $failure = new Currency(978, 'EUR', -1, '€');
    }

    #[Depends('testCurrencyPrecisionIsSmallInteger')]
    #[TestDox('Currency.precision can not be greater than 4')]
    public function testCurrencyPrecisionCanNotBeGreaterThan4(): void
    {
        $currency = new Currency(978, 'EUR', 4, '€');
        $this->assertEquals(4, $currency->precision);

        $this->expectException(\ValueError::class);
        $failure = new Currency(978, 'EUR', 5, '€');
    }


    #[TestDox('Currency.symbol exists')]
    public function testCurrencySymbolExists(): void
    {
        $this->assertObjectHasProperty('symbol', new Currency());
    }

    #[Depends('testCurrencySymbolExists')]
    #[TestDox('Currency.symbol is public read-only')]
    public function testCurrencySymbolIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(Currency::class, 'symbol');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }

    #[Depends('testCurrencySymbolIsPublicReadOnly')]
    #[TestDox('Currency.symbol is non-empty string')]
    public function testCurrencySymbolIsNonEmptyString(): void
    {
        $currency = new Currency();
        $this->assertNotEmpty($currency->symbol);
        $this->assertIsString($currency->symbol);
    }

    #[Depends('testCurrencySymbolIsNonEmptyString')]
    #[TestDox('Currency.symbol is euro sign by default')]
    public function testCurrencySymbolIsEuroSignByDefault(): void
    {
        $currency = new Currency();
        $this->assertEquals('€', $currency->symbol);
    }

    #[Depends('testCurrencySymbolIsNonEmptyString')]
    #[TestDox('Currency::__construct sets symbol')]
    public function testCurrencyConstructorSetsSymbol(): void
    {
        $currency = new Currency(208, 'DKK', 2, 'DKr');
        $this->assertEquals('DKr', $currency->symbol);

        $currency = new Currency(981, 'GEL', 2, '₾');
        $this->assertEquals('₾', $currency->symbol);


        $currency = new Currency(578, 'NOK', 2, 'kr');
        $this->assertEquals('kr', $currency->symbol);

        $this->expectException(\ValueError::class);
        $currency = new Currency(578, 'NOKr', 2, 'kr');
    }
}
