<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\Logger::class)]
final class LoggerTest extends TestCase
{
    private ReflectionClass $reflectionClass;

    protected function setUp(): void
    {
        $this->reflectionClass = new ReflectionClass(Logger::class);
    }

    #[Group('hmvc')]
    #[TestDox('Logger class is concrete')]
    public function testLoggerClassIsConcrete(): void
    {
        $this->assertFalse($this->reflectionClass->isAbstract());
        $this->assertFalse($this->reflectionClass->isFinal());
        $this->assertTrue($this->reflectionClass->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Extended PSR-3 AbstractLogger class exists')]
    public function testExtendedPSR3AbstractLoggerClassExists(): void
    {
        $this->assertTrue(class_exists('\\Psr\\Log\\AbstractLogger'));
    }

    #[Depends('testExtendedPSR3AbstractLoggerClassExists')]
    #[Group('hmvc')]
    #[TestDox('Logger extends PSR-3 AbstractLogger')]
    public function testLoggerExtendsPSR3AbstractLogger(): void
    {
        $abstract_logger = new ReflectionClass(\Psr\Log\AbstractLogger::class);
        $this->assertTrue($abstract_logger->isAbstract());

        $logger = new Logger();
        $this->assertInstanceOf(\Psr\Log\AbstractLogger::class, $logger);
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-3 LoggerInterface exists')]
    public function testImplementedPSR3LoggerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Log\\LoggerInterface'));
        $this->assertTrue(interface_exists(\Psr\Log\LoggerInterface::class));
    }

    #[Depends('testImplementedPSR3LoggerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Logger implements PSR-3 LoggerInterface')]
    public function testLoggerImplementsPSR3LoggerInterface(): void
    {
        $this->assertInstanceOf(\Psr\Log\LoggerInterface::class, new Logger());
    }


    #[Group('hmvc')]
    #[TestDox('Logger implements SplSubject interface')]
    public function testLoggerImplementsSplSubjectInterface(): void
    {
        $this->assertInstanceOf(\SplSubject::class, new Logger());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Logger::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Logger::VERSION);
        $this->assertIsString(Logger::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Logger::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Logger::__construct exists')]
    public function testLoggerConstructorExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('__construct'));
    }

    #[Depends('testLoggerConstructorExists')]
    #[TestDox('Logger::__construct is public constructor')]
    public function testLoggerConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Logger::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Logger::__construct has one OPTIONAL parameter')]
    public function testLoggerConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Logger::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('All RFC 5424 Syslog log levels have public methods')]
    public function testAllRFC5424SyslogLogLevelsHaveMethods(): void
    {
        $log_levels = ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug'];
        foreach ($log_levels as $log_level_method) {
            $this->assertTrue($this->reflectionClass->hasMethod($log_level_method));

            $method = new ReflectionMethod(Logger::class, $log_level_method);
            $this->assertFalse($method->isAbstract());
            $this->assertFalse($method->isFinal());
            $this->assertTrue($method->isPublic());
            $this->assertFalse($method->isStatic());
        }
    }


    #[TestDox('Logger::attach exists')]
    public function testLoggerAttachExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('attach'));
    }

    #[Depends('testLoggerAttachExists')]
    #[TestDox('Logger::attach is public')]
    public function testLoggerAttachIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'attach');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerAttachExists')]
    #[TestDox('Logger::attach has one REQUIRED parameter')]
    public function testLoggerAttachHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Logger::class, 'attach');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Logger::detach exists')]
    public function testLoggerDetachExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('detach'));
    }

    #[Depends('testLoggerDetachExists')]
    #[TestDox('Logger::detach is public')]
    public function testLoggerDetachIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'detach');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerDetachExists')]
    #[TestDox('Logger::detach has one REQUIRED parameter')]
    public function testLoggerDetachHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Logger::class, 'detach');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Logger::getFilename exists')]
    public function testLoggerGetFilenameExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('getFilename'));
    }

    #[Depends('testLoggerGetFilenameExists')]
    #[TestDox('Logger::getFilename is public')]
    public function testLoggerGetFilenameIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getFilename');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerGetFilenameExists')]
    #[TestDox('Logger::getFilename has no parameters')]
    public function testLoggerGetFilenameHasNoParameters(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getFilename');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLoggerGetFilenameHasNoParameters')]
    #[TestDox('Logger::getFilename returns non-empty string')]
    public function testLoggerGetFilenameReturnsNonEmptyString(): void
    {
        $logger = new \StoreCore\FileSystem\Logger();
        $this->assertNotEmpty($logger->getFilename());
        $this->assertIsString($logger->getFilename());
    }

    #[Depends('testLoggerGetFilenameHasNoParameters')]
    #[TestDox('Logger::getFilename returns string ending in .log extension')]
    public function testLoggerGetFilenameReturnsStringEndingInLogExtension(): void
    {
        $logger = new \StoreCore\FileSystem\Logger();
        $this->assertStringEndsWith('.log', $logger->getFilename());
    }


    #[TestDox('Logger::getLogLevel exists')]
    public function testLoggerGetLogLevelExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('getLogLevel'));
    }

    #[Depends('testLoggerGetLogLevelExists')]
    #[TestDox('Logger::getLogLevel is public')]
    public function testLoggerGetLogLevelIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getLogLevel');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerGetLogLevelExists')]
    #[TestDox('Logger::getLogLevel has no parameters')]
    public function testLoggerGetLogLevelHasNoParameters(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getLogLevel');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLoggerGetLogLevelExists')]
    #[Depends('testLoggerGetLogLevelIsPublic')]
    #[Depends('testLoggerGetLogLevelHasNoParameters')]
    #[TestDox('Logger::getLogLevel returns null by default')]
    public function testLoggerGetLogLevelReturnsNullByDefault(): void
    {
        $logger = new \StoreCore\FileSystem\Logger();
        $this->assertNull($logger->getLogLevel());
    }


    #[TestDox('Logger::getMessage exists')]
    public function testLoggerGetMessageExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('getMessage'));
    }

    #[Depends('testLoggerGetMessageExists')]
    #[TestDox('Logger::getMessage is public')]
    public function testLoggerGetMessageIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getMessage');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerGetMessageExists')]
    #[TestDox('Logger::getMessage has no parameters')]
    public function testLoggerGetMessageHasNoParameters(): void
    {
        $method = new ReflectionMethod(Logger::class, 'getMessage');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLoggerGetMessageExists')]
    #[Depends('testLoggerGetMessageIsPublic')]
    #[Depends('testLoggerGetMessageHasNoParameters')]
    #[TestDox('Logger::getMessage returns null by default')]
    public function testLoggerGetMessageReturnsNullByDefault(): void
    {
        $logger = new \StoreCore\FileSystem\Logger();
        $this->assertNull($logger->getMessage());
    }


    #[TestDox('Logger::notify exists')]
    public function testLoggerNotifyExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('notify'));
    }

    #[Depends('testLoggerNotifyExists')]
    #[TestDox('Logger::notify is public')]
    public function testLoggerNotifyIsPublic(): void
    {
        $method = new ReflectionMethod(Logger::class, 'notify');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testLoggerNotifyExists')]
    #[TestDox('Logger::notify has no parameters')]
    public function testLoggerNotifyHasNoParameters(): void
    {
        $method = new ReflectionMethod(Logger::class, 'notify');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
