<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\Backup::class)]
final class BackupTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Backup::VERSION);
        $this->assertIsString(Backup::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Backup::VERSION, '1.0.0', '>=')
        );
    }

    
    #[TestDox('Backup::getDirectory exists')]
    public function testBackupGetDirectoryExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('getDirectory'));
    }

    #[Depends('testBackupGetDirectoryExists')]
    #[TestDox('Backup::getDirectory is public static')]
    public function testBackupGetDirectoryIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Backup::class, 'getDirectory');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testBackupGetDirectoryExists')]
    #[TestDox('Backup::getDirectory has no parameters')]
    public function testBackupGetDirectoryHasNoParameters(): void
    {
        $method = new ReflectionMethod(Backup::class, 'getDirectory');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testBackupGetDirectoryExists')]
    #[Depends('testBackupGetDirectoryIsPublicStatic')]
    #[Depends('testBackupGetDirectoryHasNoParameters')]
    #[TestDox('Backup::getDirectory returns existing directory as string')]
    public function testBackupGetDirectoryReturnsExistingDirectoryAsString(): void
    {
        $directory = Backup::getDirectory();
        $this->assertNotEmpty($directory);
        $this->assertIsString($directory);

        $this->assertTrue(
            \is_dir($directory),
            'Backup directory "' . $directory . '" does not exist.'
        );

        $this->assertTrue(
            \is_readable($directory),
            'Backup directory "' . $directory . '" is not readable.'
        );
    }


    #[TestDox('Backup::save exists')]
    public function testBackupSaveExists(): void
    {
        $class = new ReflectionClass(Backup::class);
        $this->assertTrue($class->hasMethod('save'));
    }

    #[Depends('testBackupSaveExists')]
    #[TestDox('Backup::save is public static')]
    public function testBackupSaveIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Backup::class, 'save');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testBackupSaveExists')]
    #[TestDox('Backup::save has one OPTIONAL parameter')]
    public function testBackupSaveHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Backup::class, 'save');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }
}
