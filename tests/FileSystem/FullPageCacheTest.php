<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \StoreCore\Engine\UriFactory;
use StoreCore\Types\CacheKey;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\FullPageCache::class)]
#[CoversClass(\StoreCore\FileSystem\CacheDirectory::class)]
#[UsesClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class FullPageCacheTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('FullPageCache class is concrete')]
    public function testFullPageCacheClassIsConcrete(): void
    {
        $class = new ReflectionClass(FullPageCache::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('FullPageCache is a CacheDirectory')]
    public function testFullPageCacheIsCacheDirectory(): void
    {
        $this->assertInstanceOf(
            \StoreCore\FileSystem\CacheDirectory::class,
            new FullPageCache()
        );
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-16 CacheInterface exists')]
    public function testImplementedPSR16CacheInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));
    }

    #[Depends('testImplementedPSR16CacheInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('FullPageCache implements PSR-16 CacheInterface')]
    public function testFullPageCacheImplementsPSR16CacheInterface(): void
    {
        $this->assertInstanceOf(
            \Psr\SimpleCache\CacheInterface::class,
            new FullPageCache()
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FullPageCache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FullPageCache::VERSION);
        $this->assertIsString(FullPageCache::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FullPageCache::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('FullPageCache::has exists')]
    public function testFullPageCacheHasExists(): void
    {
        $class = new ReflectionClass(FullPageCache::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testFullPageCacheHasExists')]
    #[TestDox('FullPageCache::has is public')]
    public function testFullPageCacheHasIsPublic(): void
    {
        $method = new ReflectionMethod(FullPageCache::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFullPageCacheHasExists')]
    #[TestDox('FullPageCache::has has one REQUIRED parameter')]
    public function testFullPageCacheHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FullPageCache::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFullPageCacheHasHasOneRequiredParameter')]
    #[TestDox('FullPageCache::has accepts cache key as string')]
    public function testFullPageCacheHasAcceptsCacheKeyAsString(): void
    {
        $key = new CacheKey();
        $key = (string) $key;
        $this->assertIsString($key);
        $this->assertNotEmpty($key);

        $cache = new FullPageCache();
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
    }

    #[Depends('testFullPageCacheHasAcceptsCacheKeyAsString')]
    #[TestDox('FullPageCache::has accepts URL as stringable cache key object')]
    public function testFullPageCacheHasAcceptsUrlAsStringableCacheKeyObject(): void
    {
        // Create cache key from URL string
        $url = 'https://www.example.com/shoes/nike/air-force-1';
        $this->assertIsString($url);

        $key = new CacheKey($url);
        $this->assertInstanceOf(\Stringable::class, $key);
        $this->assertIsObject($key);

        $cache = new FullPageCache();
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));

        // Create cache key from URL object
        $factory = new UriFactory();
        $uri = $factory->createUri($url);
        $this->assertIsObject($uri);

        $key = new CacheKey($uri);
        $this->assertInstanceOf(\Stringable::class, $key);
        $this->assertIsObject($key);

        $cache = new FullPageCache();
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
    }

    #[Depends('testFullPageCacheHasExists')]
    #[TestDox('FullPageCache::has accepts URL as string')]
    public function testFullPageCacheHasAcceptsUrlAsString(): void
    {
        $url = 'https://www.example.com/shoes/nike/air-force-1';
        $cache = new FullPageCache();
        $this->assertIsBool($cache->has($url));
        $this->assertFalse($cache->has($url));
    }


    /**
     * @see https://amp.dev/documentation/examples/introduction/hello_world/
     *      Hello world example in AMP HTML
     */
    #[TestDox('Full-page cache basic operations')]
    public function testFullPageCacheBasicOperations(): void
    {
        $page = 
<<<HTML
<!doctype html>
<html ⚡ lang="en">
<head>
  <meta charset="utf-8">
  <title>Hello World</title>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <link rel="canonical" href="https://amp.dev/documentation/examples/introduction/hello_world/index.html">
  <meta name="viewport" content="width=device-width">
  <style amp-custom>
    h1 {
      color: red;
    }
  </style>
</head>
<body>
  <h1>Hello World!</h1>
  <amp-img src="/static/samples/img/amp.jpg" width="1080" height="610" layout="responsive"></amp-img>
</body>
</html>
HTML;

        $cache = new FullPageCache();
        $this->assertIsBool($cache->has('https://www.example.com/hello-world'));
        $this->assertFalse($cache->has('https://www.example.com/hello-world'));

        $factory = new UriFactory();
        $url = $factory->createUri('https://www.example.com/hello-world');
        unset($factory);
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $url);
        $this->assertIsBool($cache->has($url));
        $this->assertFalse($cache->has($url));

        $this->assertTrue($cache->set($url, $page));

        $this->assertIsBool($cache->has($url));
        $this->assertTrue($cache->has($url));

        $this->assertNotNull($cache->get($url));
        $this->assertNotEmpty($cache->get($url));
        $this->assertIsString($cache->get($url));
        $this->assertSame($cache->get($url), $page);

        $this->assertTrue($cache->delete($url));

        $this->assertFalse($cache->has($url));
    }
}
