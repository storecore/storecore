<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class CacheExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CacheException class exists')]
    public function testCacheExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'FileSystem' . DIRECTORY_SEPARATOR . 'CacheException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'FileSystem' . DIRECTORY_SEPARATOR . 'CacheException.php');

        $this->assertTrue(class_exists('\\StoreCore\\FileSystem\\CacheException'));
        $this->assertTrue(class_exists(CacheException::class));
    }

    #[Group('hmvc')]
    #[TestDox('CacheException class is concrete')]
    public function testCacheExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CacheException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CacheException class is a throwable exception')]
    public function testCacheExceptionClassIsThrowableException(): void
    {
        $exception = new CacheException();
        $this->assertInstanceOf(\Throwable::class, $exception);
        $this->assertInstanceOf(\Exception::class, $exception);

        $this->expectException(\StoreCore\FileSystem\CacheException::class);
        throw new CacheException();
    }

    /**
     * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-16-simple-cache.md#22-cacheexception
     */
    #[Group('hmvc')]
    #[TestDox('CacheException is a PSR-16 SimpleCache CacheException')]
    public function testCacheExceptionIsPsr16SimpleCacheCacheException()
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $exception = new \StoreCore\FileSystem\CacheException();
        $this->assertInstanceOf(\Psr\SimpleCache\CacheException::class, $exception);

        $this->expectException(\Psr\SimpleCache\CacheException::class);
        throw new CacheException();
    }
}
