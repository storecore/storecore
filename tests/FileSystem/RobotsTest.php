<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use StoreCore\Registry;
use StoreCore\Engine\ServerRequestFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\FileSystem\Robots::class)]
#[Group('seo')]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\StreamFactory::class)]
#[UsesClass(\StoreCore\Engine\TemporaryStream::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
#[UsesClass(\StoreCore\FileSystem\FileStream::class)]
final class RobotsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Robots class is concrete')]
    public function testRobotsClassIsConcrete(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Robots is a controller')]
    public function testRobotsIsAController(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Robots::VERSION);
        $this->assertIsString(Robots::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches master branch')]
    public function testVersionConstantMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Robots::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Robots::__construct exists')]
    public function testRobotsConstructorExists(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testRobotsConstructorExists')]
    #[TestDox('Robots::__construct is public constructor')]
    public function testRobotsConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Robots::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testRobotsConstructorExists')]
    #[TestDox('Robots::__construct has one REQUIRED parameter')]
    public function testRobotsConstructorHasOneRequiredParameters(): void
    {
        $method = new ReflectionMethod(Robots::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Robots::getCrawlDelay exists')]
    public function testRobotsGetCrawlDelayExists(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasMethod('getCrawlDelay'));
    }

    #[Depends('testRobotsGetCrawlDelayExists')]
    #[TestDox('Robots::getCrawlDelay is public')]
    public function testRobotsGetCrawlDelayIsPublic(): void
    {
        $method = new ReflectionMethod(Robots::class, 'getCrawlDelay');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRobotsGetCrawlDelayExists')]
    #[TestDox('Robots::getCrawlDelay has no parameters')]
    public function testRobotsGetCrawlDelayHasNoParameters(): void
    {
        $method = new ReflectionMethod(Robots::class, 'getCrawlDelay');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRobotsGetCrawlDelayExists')]
    #[Depends('testRobotsGetCrawlDelayIsPublic')]
    #[Depends('testRobotsGetCrawlDelayHasNoParameters')]
    #[TestDox('Robots::getCrawlDelay returns integer greater than 0')]
    public function testRobotsGetCrawlDelayReturnsIntegerGreaterThanZero(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $this->assertIsInt($controller->getCrawlDelay());
        $this->assertGreaterThan(0, $controller->getCrawlDelay());
    }

    #[Depends('testRobotsGetCrawlDelayReturnsIntegerGreaterThanZero')]
    #[TestDox('Robots::getCrawlDelay returns (int) 10 by default')]
    public function testRobotsGetCrawlDelayReturnsInt10ByDefault(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $this->assertEquals(10, $controller->getCrawlDelay());
    }

    #[TestDox('Robots::setCrawlDelay exists')]
    public function testRobotsSetCrawlDelayExists(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasMethod('setCrawlDelay'));
    }

    #[Depends('testRobotsSetCrawlDelayExists')]
    #[TestDox('Robots::setCrawlDelay is public')]
    public function testRobotsSetCrawlDelayIsPublic(): void
    {
        $method = new ReflectionMethod(Robots::class, 'setCrawlDelay');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRobotsSetCrawlDelayExists')]
    #[TestDox('Robots::setCrawlDelay has one REQUIRED parameter')]
    public function testRobotsSetCrawlDelayHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Robots::class, 'setCrawlDelay');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRobotsSetCrawlDelayExists')]
    #[Depends('testRobotsSetCrawlDelayIsPublic')]
    #[Depends('testRobotsSetCrawlDelayHasOneRequiredParameter')]
    #[TestDox('Robots::setCrawlDelay sets Crawl-delay')]
    public function testRobotsSetCrawlDelaySetsCrawlDelay(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);

        $controller->setCrawlDelay(1);
        $this->assertIsInt($controller->getCrawlDelay());
        $this->assertEquals(1, $controller->getCrawlDelay());

        $controller->setCrawlDelay(2);
        $this->assertIsInt($controller->getCrawlDelay());
        $this->assertEquals(2, $controller->getCrawlDelay());
    }

    #[Depends('testRobotsSetCrawlDelaySetsCrawlDelay')]
    #[TestDox('Crawl-delay minimum is 1 second')]
    public function testCrawlDelayMinimumIsOneSecond(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $this->assertEquals(10, $controller->getCrawlDelay());

        $controller->setCrawlDelay(-2);
        $this->assertEquals(1, $controller->getCrawlDelay());

        $controller->setCrawlDelay(-1);
        $this->assertEquals(1, $controller->getCrawlDelay());

        $controller->setCrawlDelay(0);
        $this->assertEquals(1, $controller->getCrawlDelay());

        $controller->setCrawlDelay(20);
        $this->assertEquals(20, $controller->getCrawlDelay());
    }

    #[Depends('testRobotsSetCrawlDelaySetsCrawlDelay')]
    #[TestDox('Crawl-delay maximum is 300 seconds')]
    public function testCrawlDelayMinimumIsThreeHundredSeconds(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $this->assertEquals(10, $controller->getCrawlDelay());

        $controller->setCrawlDelay(299);
        $this->assertEquals(299, $controller->getCrawlDelay());

        $controller->setCrawlDelay(300);
        $this->assertEquals(300, $controller->getCrawlDelay());

        $controller->setCrawlDelay(301);
        $this->assertNotEquals(301, $controller->getCrawlDelay());
        $this->assertEquals(300, $controller->getCrawlDelay());

        $controller->setCrawlDelay(302);
        $this->assertNotEquals(302, $controller->getCrawlDelay());
        $this->assertEquals(300, $controller->getCrawlDelay());
    }


    #[TestDox('Robots::getSitemap exists')]
    public function testRobotsGetSitemapExists(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasMethod('getSitemap'));
    }

    #[Depends('testRobotsGetSitemapExists')]
    #[TestDox('Robots::getSitemap is public')]
    public function testRobotsGetSitemapIsPublic(): void
    {
        $method = new ReflectionMethod(Robots::class, 'getSitemap');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRobotsGetSitemapExists')]
    #[TestDox('Robots::getSitemap has no parameters')]
    public function testRobotsGetSitemapHasNoParameters(): void
    {
        $method = new ReflectionMethod(Robots::class, 'getSitemap');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Robots::getSitemap returns sitemap.xml URL as string')]
    public function testRobotsGetSitemapReturnsSitemapXmlUrlAsString(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);

        $this->assertNotEmpty($controller->getSitemap());
        $this->assertIsString($controller->getSitemap());
        $this->assertStringStartsWith('https://', $controller->getSitemap());
        $this->assertStringEndsWith('/sitemap.xml', $controller->getSitemap());
    }


    #[TestDox('Robots::handle exists')]
    public function testRobotsHandleExists(): void
    {
        $class = new ReflectionClass(Robots::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testRobotsGetSitemapExists')]
    #[TestDox('Robots::handle is public')]
    public function testRobotsHandleIsPublic(): void
    {
        $method = new ReflectionMethod(Robots::class, 'handle');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRobotsGetSitemapExists')]
    #[TestDox('Robots::handle has one OPTIONAL parameter')]
    public function testRobotsHandleHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Robots::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRobotsGetSitemapExists')]
    #[Depends('testRobotsHandleIsPublic')]
    #[Depends('testRobotsHandleHasOneOptionalParameter')]
    #[TestDox('Robots::handle accepts ServerRequest and returns Response')]
    public function testRobotsHandleAcceptsServerRequestAndReturnsResponse(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', '/robots.txt');
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ServerRequestInterface'));
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $request);

        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
        $this->assertInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class, $controller);

        $response = $controller->handle($request);
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ResponseInterface'));
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }

    #[Depends('testRobotsHandleAcceptsServerRequestAndReturnsResponse')]
    #[TestDox('Robots::handle returns Response even if ServerRequest is not set')]
    public function testRobotsHandleReturnsResponseEvenIfServerRequestIsNotSet(): void
    {
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $response = $controller->handle();
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }

    #[Depends('testRobotsHandleAcceptsServerRequestAndReturnsResponse')]
    #[TestDox('Robots::handle returns custom robots.txt if it exists')]
    public function testRobotsHandleReturnsCustomRobotsTxtFileIfItExists(): void
    {
        $path = __DIR__ . '/../../public/txt';
        $this->assertTrue(is_dir($path));
        $this->assertTrue(is_readable($path));

        $filename = $path . '/robots.txt';
        $this->assertFalse(
            is_file($filename),
            'In the default setup there MUST NOT be a custom robots.txt file.'
        );

        /* @see https://webmasters.stackexchange.com/questions/56720/what-is-a-minimum-valid-robots-txt-file
         *      What is a minimum valid robots.txt file?
         */
        $data = 'User-agent: *' . "\n" . 'Disallow: ' . "\n";

        $this->assertNotFalse(
            file_put_contents($filename, $data),
            'Unable to create a custom robots.txt file.'
        );

        $this->assertTrue(
            is_file($filename),
            'Unable to read the custom robots.txt file.'
        );

        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', '/robots.txt');
        $registry = Registry::getInstance();
        $controller = new Robots($registry);
        $response = $controller->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
        $this->assertEquals($data, (string) $response->getBody());

        $this->assertTrue(
            unlink($filename),
            'Unable to delete the custom robots.txt file: please remove the file manually.'
        );
    }
}
