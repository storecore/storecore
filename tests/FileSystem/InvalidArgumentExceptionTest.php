<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\InvalidArgumentException::class)]
final class InvalidArgumentExceptionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('InvalidArgumentException class is concrete')]
    public function testInvalidArgumentExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(InvalidArgumentException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('InvalidArgumentException class is a throwable exception')]
    public function testInvalidArgumentExceptionClassIsThrowableException(): void
    {
        $exception = new InvalidArgumentException();
        $this->assertInstanceOf(\Throwable::class, $exception);
        $this->assertInstanceOf(\Exception::class, $exception);

        $this->expectException(\Exception::class);
        throw new InvalidArgumentException();
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-16 SimpleCache exception interfaces exist')]
    public function testImplementedPSR16SimpleCacheExceptionInterfacesExist(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\InvalidArgumentException'));
    }

    #[Depends('testImplementedPSR16SimpleCacheExceptionInterfacesExist')]
    #[Group('hmvc')]
    #[TestDox('InvalidArgumentException is a PSR-16 SimpleCache InvalidArgumentException CacheException')]
    public function testInvalidArgumentExceptionIsPsr16SimpleCacheInvalidArgumentExceptionCacheException()
    {
        $exception = new \StoreCore\FileSystem\InvalidArgumentException();
        $this->assertInstanceOf(\Psr\SimpleCache\InvalidArgumentException::class, $exception);
        $this->assertInstanceOf(\Psr\SimpleCache\CacheException::class, $exception);
    }
}
