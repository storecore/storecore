<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use StoreCore\Types\CacheKey;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\FileSystem\CacheDirectory::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Types\CacheKey::class)]
final class CacheDirectoryTest extends TestCase
{
    #[TestDox('CacheDirectory class is concrete')]
    public function testCacheDirectoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(CacheDirectory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-16 CacheInterface exists')]
    public function testImplementedPSR16CacheInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));
    }

    #[Depends('testCacheDirectoryClassIsConcrete')]
    #[Depends('testImplementedPSR16CacheInterfaceExists')]
    #[TestDox('CacheDirectory implements PSR-16 CacheInterface')]
    public function testCacheDirectoryImplementsPsr16CacheInterface(): void
    {
        $this->assertInstanceOf(
            \Psr\SimpleCache\CacheInterface::class,
            new CacheDirectory()
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CacheDirectory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CacheDirectory::VERSION);
        $this->assertIsString(CacheDirectory::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CacheDirectory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('CacheDirectory::get exists')]
    public function testCacheDirectoryGetExists(): void
    {
        $class = new ReflectionClass(CacheDirectory::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testCacheDirectoryGetExists')]
    #[TestDox('CacheDirectory::get is public')]
    public function testCacheDirectoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCacheDirectoryGetExists')]
    #[TestDox('CacheDirectory::get has two parameters')]
    public function testCacheDirectoryGetHasTwoParameters(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'get');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testCacheDirectoryGetHasTwoParameters')]
    #[TestDox('CacheDirectory::get has one REQUIRED parameter')]
    public function testCacheDirectoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'get');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CacheDirectory::get throws PSR-16 Psr\SimpleCache\InvalidArgumentException on empty string')]
    public function testCacheDirectoryGetThrowsPsr16PsrSimpleCacheInvalidArgumentExceptionOnEmptyString()
    {
        $cache = new CacheDirectory();
        $empty_cache_key = '';
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $failure = $cache->get($empty_cache_key);
    }

    #[TestDox('CacheDirectory::get throws PSR-16 Psr\SimpleCache\InvalidArgumentException on invalid cache key')]
    public function testCacheDirectoryGetThrowsPsr16PsrSimpleCacheInvalidArgumentExceptionOnInvalidCacheKey(): void
    {
        $cache = new CacheDirectory();
        $invalid_cache_key = 'Sesame';
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $cache->get($invalid_cache_key);
    }


    #[TestDox('CacheDirectory::getFileModificationTime exists')]
    public function testCacheDirectoryGetFileModificationTimeExists(): void
    {
        $class = new ReflectionClass(CacheDirectory::class);
        $this->assertTrue($class->hasMethod('getFileModificationTime'));
    }

    #[Depends('testCacheDirectoryGetFileModificationTimeExists')]
    #[TestDox('CacheDirectory::getFileModificationTime is public')]
    public function testCacheDirectoryGetFileModificationTimeIsPublic()
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'getFileModificationTime');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testCacheDirectoryGetFileModificationTimeExists')]
    #[TestDox('CacheDirectory::getFileModificationTime has one REQUIRED parameter')]
    public function testCacheDirectoryGetFileModificationTimeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'getFileModificationTime');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CacheDirectory::has exists')]
    public function testCacheDirectoryHasExists(): void
    {
        $class = new ReflectionClass(CacheDirectory::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testCacheDirectoryHasExists')]
    #[TestDox('CacheDirectory::has is public')]
    public function testCacheDirectoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCacheDirectoryHasExists')]
    #[TestDox('CacheDirectory::has has one REQUIRED parameter')]
    public function testCacheDirectoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CacheDirectory::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCacheDirectoryHasExists')]
    #[Depends('testCacheDirectoryHasIsPublic')]
    #[Depends('testCacheDirectoryHasHasOneRequiredParameter')]
    #[TestDox('CacheDirectory::has returns bool')]
    public function testCacheDirectoryHasReturnsBool(): void
    {
        $cache = new CacheDirectory();
        $key = new CacheKey();
        $key = (string) $key;
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
        $this->assertIsString($key);
    }

    #[Depends('testCacheDirectoryHasHasOneRequiredParameter')]
    #[Depends('testCacheDirectoryHasReturnsBool')]
    #[TestDox('CacheDirectory::has accepts StoreCore\Types\CacheKey object')]
    public function testCacheDirectoryAcceptsStoreCoreTypesCacheKeyObject(): void
    {
        $cache = new CacheDirectory();
        $key = new CacheKey();
        $this->assertIsObject($key);
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
    }

    #[Depends('testCacheDirectoryHasHasOneRequiredParameter')]
    #[TestDox('CacheDirectory::has throws Psr\SimpleCache\InvalidArgumentException on empty string')]
    public function testCacheDirectoryHasThrowsPsrSimpleCacheInvalidArgumentExceptionOnEmptyString(): void
    {
        $cache = new CacheDirectory();
        $key = '';
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $failure = $cache->has($key);
    }
}
