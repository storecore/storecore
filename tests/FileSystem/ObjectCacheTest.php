<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\FileSystem;

use StoreCore\CRM\Organization;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\FileSystem\ObjectCache::class)]
#[CoversClass(\StoreCore\FileSystem\ObjectCacheReader::class)]
#[CoversClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ObjectCacheTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ObjectCache class is concrete')]
    public function testObjectCacheClassIsConcrete(): void
    {
        $class = new ReflectionClass(ObjectCache::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ObjectCache is an ObjectCacheReader')]
    public function testObjectCacheIsObjectCacheReader(): void
    {
        $class = new ReflectionClass(ObjectCache::class);
        $this->assertTrue($class->isSubclassOf(ObjectCacheReader::class));
    }

    #[Group('hmvc')]
    #[TestDox('ObjectCache implements PSR-16 CacheInterface')]
    public function testObjectCacheImplementsPsr16CacheInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheInterface::class));

        $class = new ReflectionClass(ObjectCache::class);
        $this->assertTrue($class->implementsInterface(\Psr\SimpleCache\CacheInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ObjectCache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ObjectCache::VERSION);
        $this->assertIsString(ObjectCache::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ObjectCache::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ObjectCache::delete() exists')]
    public function testObjectCacheDeleteExists(): void
    {
        $class = new ReflectionClass(ObjectCache::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testObjectCacheDeleteExists')]
    #[TestDox('ObjectCache::delete() is public')]
    public function testObjectCacheDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(ObjectCache::class, 'delete');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testObjectCacheDeleteExists')]
    #[TestDox('ObjectCache::delete() has one REQUIRED parameter')]
    public function testObjectCacheDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ObjectCache::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObjectCacheDeleteExists')]
    #[TestDox('ObjectCache::delete() accepts UUID string or UUID object')]
    public function testObjectCacheDeleteAcceptsUuidStringOrUuidObject(): void
    {
        $cache = new ObjectCache();

        $key = (string) UUIDFactory::randomUUID();
        $this->assertNotEmpty($key);
        $this->assertIsString($key);
        $this->assertIsBool($cache->delete($key));
        $this->assertFalse($cache->delete($key));

        $uuid = UUIDFactory::randomUUID();
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertIsBool($cache->delete($uuid));
        $this->assertFalse($cache->delete($uuid));

        $invalid_key = random_int(1, 65535);
        $this->assertIsNotString($invalid_key);
        $this->expectException(\TypeError::class);
        $failure = $cache->delete($invalid_key);
    }


    #[TestDox('ObjectCache::get() exists')]
    public function testObjectCacheGetExists(): void
    {
        $class = new ReflectionClass(ObjectCache::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testObjectCacheGetExists')]
    #[TestDox('ObjectCache::get() exists')]
    public function testObjectCacheGetIsPublic(): void
    {
        $method = new ReflectionMethod(ObjectCache::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testObjectCacheGetExists')]
    #[TestDox('ObjectCache::get() has two parameters')]
    public function testObjectCacheGetHasTwoParameters(): void
    {
        $method = new ReflectionMethod(ObjectCache::class, 'get');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testObjectCacheGetHasTwoParameters')]
    #[TestDox('ObjectCache::get() has one REQUIRED parameter')]
    public function testObjectCacheGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ObjectCache::class, 'get');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObjectCacheGetHasOneRequiredParameter')]
    #[TestDox('ObjectCache::get() requires cache key as string or UUID')]
    public function testObjectCacheGetRequiresCacheKeyAsStringOrUUID(): void
    {
        $cache = new ObjectCache();
        $uuid = UUIDFactory::randomUUID();

        $this->assertIsNotString($uuid);
        $this->assertNull($cache->get($uuid));

        $key = (string) $uuid;
        $this->assertIsString($key);
        $this->assertNull($cache->get($key));

        $invalid_key = random_int(1, 65535);
        $this->expectException(\TypeError::class);
        $failure = $cache->get($invalid_key);
    }

    #[Depends('testObjectCacheGetHasOneRequiredParameter')]
    #[TestDox('ObjectCache::get() returns null on unknown key')]
    public function testObjectCacheGetReturnsNullOnUnknownKey(): void
    {
        $cache = new ObjectCache();
        $uuid = UUIDFactory::randomUUID();
        $key = (string) $uuid;

        $this->assertIsString($key);
        $this->assertNotEmpty($key);
        $this->assertNull($cache->get($key));
    }

    #[Depends('testObjectCacheGetHasTwoParameters')]
    #[TestDox('ObjectCache::get() returns second parameter on unknown key')]
    public function testObjectCacheGetReturnsSecondParameterOnUnknownKey(): void
    {
        $cache = new ObjectCache();
        $uuid = UUIDFactory::randomUUID();
        $key = (string) $uuid;

        $this->assertNull($cache->get($key));
        $this->assertNotNull($cache->get($key, 42));
        $this->assertEquals(42, $cache->get($key, 42));

        $seller = new Organization();
        $seller->name = 'ACME Supplies';
        $this->assertSame($seller, $cache->get($key, $seller));
        $this->assertEquals('ACME Supplies', $cache->get($key, $seller)->name);
    }

    #[Depends('testObjectCacheGetReturnsNullOnUnknownKey')]
    #[TestDox('ObjectCache::get() throws Psr\SimpleCache\InvalidArgumentException on empty key')]
    public function testObjectCacheGetReturnsNullOnEmptyKey(): void
    {
        // @see https://github.com/php-fig/simple-cache/blob/master/src/InvalidArgumentException.php
        $this->assertTrue(
            interface_exists('\\Psr\\SimpleCache\\InvalidArgumentException'),
            'PSR-16 interface `InvalidArgumentException` does not exist.'
        );

        $cache = new ObjectCache();
        $key = '';

        $this->assertIsString($key);
        $this->assertEmpty($key);
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $failure = $cache->get($key);
    }


    #[TestDox('Reading and writing Organization objects')]
    public function testReadingAndWritingOrganizationObjects(): void
    {
        $organization = new Organization();
        $organization->name = 'ACME Supplies';

        $uuid = UUIDFactory::randomUUID();
        $organization->setIdentifier($uuid);

        $cache = new ObjectCache();
        $this->assertFalse($cache->has($uuid));

        $cache->set($organization->getIdentifier(), $organization);
        $this->assertTrue($cache->has($uuid));

        // Clear all objects and keep only a string copy of the cache key.
        $key = (string) $uuid;
        unset($cache, $organization, $uuid);

        // Use simple cache reader instead of full cache implementation.
        $cache = new ObjectCacheReader();
        $this->assertTrue($cache->has($key));

        // Cache has a copy of the Organization object …
        $organization = $cache->get($key);
        $this->assertIsObject($organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);

        // … with the right identity …
        $this->assertTrue($organization->hasIdentifier());
        $this->assertSame($key, (string) $organization->getIdentifier());

        // … and other object properties like the entity name.
        $this->assertEquals('ACME Supplies', $organization->name);

        // Finally, delete the cached object …
        $cache = new ObjectCache();
        $this->assertTrue($cache->delete($key));
        $this->assertFalse($cache->has($key));

        // … or clear the entire cache.
        $this->assertTrue($cache->clear());
        $this->assertFalse($cache->has($key));
    }
}
