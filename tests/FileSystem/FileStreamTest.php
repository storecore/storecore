<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\FileStream::class)]
#[CoversClass(\StoreCore\Engine\AbstractStream::class)]
final class FileStreamTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('FileStream class is concrete')]
    public function testFileStreamClassIsConcrete(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('FileStream extends AbstractStream')]
    public function testFileStreamExtendsAbstractStream(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Engine\AbstractStream::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testImplementedPSR7StreamInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\StreamInterface::class));
    }

    #[Depends('testImplementedPSR7StreamInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Stream implements PSR-7 StreamInterface')]
    public function testFileStreamImplementsPSR7StreamInterface(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Http\Message\StreamInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FileStream::VERSION);
        $this->assertIsString(FileStream::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches master branch')]
    public function testVersionConstantMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FileStream::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('FileStream::__construct exists')]
    public function testFileStreamConstructorExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testFileStreamConstructorExists')]
    #[TestDox('FileStream::__construct is public constructor')]
    public function testFileStreamConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(FileStream::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testFileStreamConstructorExists')]
    #[TestDox('FileStream::__construct has two parameters')]
    public function testFileStreamConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testFileStreamConstructorHasTwoParameters')]
    #[TestDox('FileStream::__construct has one REQUIRED parameter')]
    public function testFileStreamConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FileStream::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('FileStream implements PHP Stringable interface')]
    public function testFileStreamImplementsPhpStringableInterface(): void
    {
        $this->assertTrue(
            version_compare(PHP_VERSION, '8.0', '>='),
            'PHP 8.0+ is required for the native Stringable interface.'
        );

        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->isSubclassOf(\Stringable::class));
    }


    #[TestDox('FileStream::__toString exists')]
    public function testFileStreamToStringExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('FileStream::__toString is public')]
    public function testFileStreamToStringIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, '__toString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[TestDox('FileStream::close exists')]
    public function testFileStreamCloseExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('close'));
    }

    #[TestDox('FileStream::close is public')]
    public function testFileStreamCloseIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'close');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::close has no parameters')]
    public function testFileStreamCloseHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'close');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::detach exists')]
    public function testFileStreamDetachExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('detach'));
    }

    #[TestDox('FileStream::detach is public')]
    public function testFileStreamDetachIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'detach');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('FileStream::detach has no parameters')]
    public function testFileStreamDetachHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'detach');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::eof exists')]
    public function testFileStreamEofExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('eof'));
    }

    #[TestDox('FileStream::eof is public')]
    public function testFileStreamEofIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'eof');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('FileStream::eof has no parameters')]
    public function testFileStreamEofHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'eof');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::getContents exists')]
    public function testFileStreamGetContentsExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('getContents'));
    }

    #[TestDox('FileStream::getContents is public')]
    public function testFileStreamGetContentsIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getContents');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::getContents has no parameters')]
    public function testFileStreamGetContentsHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getContents');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::getMetadata exists')]
    public function testFileStreamGetMetadataExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('getMetadata'));
    }

    #[TestDox('FileStream::getMetadata is public')]
    public function testFileStreamGetMetadataIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getMetadata');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::getMetadata has one OPTIONAL parameter')]
    public function testFileStreamGetMetadataHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getMetadata');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('FileStream::getSize exists')]
    public function testFileStreamGetSizeExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('getSize'));
    }

    #[TestDox('FileStream::getSize is public')]
    public function testFileStreamGetSizeIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getSize');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('FileStream::getSize has no parameters')]
    public function testFileStreamGetSizeHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'getSize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::isReadable exists')]
    public function testFileStreamIsReadableExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('isReadable'));
    }

    #[TestDox('FileStream::isReadable is public')]
    public function testFileStreamIsReadableIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isReadable');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('FileStream::isReadable has no parameters')]
    public function testFileStreamIsReadableHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isReadable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::isSeekable exists')]
    public function testFileStreamIsSeekableExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('isSeekable'));
    }

    #[TestDox('FileStream::isSeekable is final public')]
    public function testFileStreamIsSeekableIsFinalPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isSeekable');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::isSeekable has no parameters')]
    public function testFileStreamIsSeekableHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isSeekable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::isWritable exists')]
    public function testFileStreamIsWritableExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('isWritable'));
    }

    #[Depends('testFileStreamIsWritableExists')]
    #[TestDox('FileStream::isWritable is final public')]
    public function testFileStreamIsWritableIsFinalPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isWritable');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFileStreamIsWritableExists')]
    #[TestDox('FileStream::isWritable has no parameters')]
    public function testFileStreamIsWritableHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'isWritable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::read exists')]
    public function testFileStreamReadExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('read'));
    }

    #[TestDox('FileStream::read is public')]
    public function testFileStreamReadIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'read');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::read has one REQUIRED parameter')]
    public function testFileStreamReadHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'read');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('FileStream::rewind exists')]
    public function testFileStreamRewindExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('rewind'));
    }

    #[TestDox('FileStream::rewind is public')]
    public function testFileStreamRewindIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'rewind');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::rewind has no parameters')]
    public function testFileStreamRewindHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'rewind');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::seek exists')]
    public function testFileStreamSeekExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('seek'));
    }

    #[TestDox('FileStream::seek is public')]
    public function testFileStreamSeekIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'seek');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('FileStream::seek has two parameters')]
    public function testFileStreamSeekHasTwoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'seek');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testFileStreamSeekHasTwoParameters')]
    #[TestDox('FileStream::seek has one REQUIRED parameter')]
    public function testFileStreamSeekHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'seek');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('FileStream::tell exists')]
    public function testFileStreamTellExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('tell'));
    }

    #[TestDox('FileStream::tell is public')]
    public function testFileStreamTellIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'tell');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('FileStream::tell has no parameters')]
    public function testFileStreamTellHasNoParameters(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'tell');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('FileStream::write exists')]
    public function testFileStreamWriteExists(): void
    {
        $class = new ReflectionClass(FileStream::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testFileStreamWriteExists')]
    #[TestDox('FileStream::write is public')]
    public function testFileStreamWriteIsPublic(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'write');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFileStreamWriteExists')]
    #[TestDox('FileStream::write has one REQUIRED parameter')]
    public function testFileStreamWriteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FileStream::class, 'write');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
