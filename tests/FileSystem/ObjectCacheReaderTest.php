<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use StoreCore\Types\CacheKey;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\FileSystem\ObjectCacheReader::class)]
#[UsesClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ObjectCacheReaderTest extends TestCase
{
    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-3 LoggerAwareInterface exists')]
    public function testImplementedPSR3LoggerAwareInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Log\\LoggerAwareInterface'));
    }

    #[Depends('testImplementedPSR3LoggerAwareInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ObjectCacheReader implements PSR-3 LoggerAwareInterface')]
    public function testObjectCacheReaderImplementsPsr3LoggerAwareInterface(): void
    {
        $object = new ObjectCacheReader();
        $this->assertInstanceOf(\Psr\Log\LoggerAwareInterface::class, $object);
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ObjectCacheReader implements PSR-11 ContainerInterface')]
    public function testObjectCacheReaderImplementsPSR11ContainerInterface(): void
    {
        $object = new ObjectCacheReader();
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $object);
    }


    #[Group('hmvc')]
    #[TestDox('ObjectCacheReader class is concrete')]
    public function testObjectCacheReaderClassIsConcrete(): void
    {
        $class = new ReflectionClass(ObjectCacheReader::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ObjectCacheReader::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ObjectCacheReader::VERSION);
        $this->assertIsString(ObjectCacheReader::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ObjectCacheReader::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ObjectCacheReader::get() exists')]
    public function testObjectCacheReaderGetExists(): void
    {
        $class = new ReflectionClass(ObjectCacheReader::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testObjectCacheReaderGetExists')]
    #[TestDox('ObjectCacheReader::get() is public')]
    public function testObjectCacheReaderGetIsPublic(): void
    {
        $method = new ReflectionMethod(ObjectCacheReader::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testObjectCacheReaderGetExists')]
    #[TestDox('ObjectCacheReader::get() has one REQUIRED parameter')]
    public function testObjectCacheReaderGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ObjectCacheReader::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObjectCacheReaderGetExists')]
    #[Depends('testObjectCacheReaderGetIsPublic')]
    #[Depends('testObjectCacheReaderGetHasOneRequiredParameter')]
    #[TestDox('ObjectCacheReader::get() throws PSR-11 Psr\Container\NotFoundExceptionInterface on empty string')]
    public function testObjectCacheReaderGetThrowsPsr11PsrContainerNotFoundExceptionInterfaceOnEmptyString(): void
    {
        $cache = new ObjectCacheReader();
        $empty_cache_key = '';
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $cache->get($empty_cache_key);
    }

    #[Depends('testObjectCacheReaderGetExists')]
    #[Depends('testObjectCacheReaderGetIsPublic')]
    #[Depends('testObjectCacheReaderGetHasOneRequiredParameter')]
    #[TestDox('ObjectCacheReader::get() throws PSR-11 Psr\Container\NotFoundExceptionInterface on invalid cache key')]
    public function testObjectCacheReaderGetThrowsPsr11PsrContainerNotFoundExceptionInterfaceOnInvalidCacheKey(): void
    {
        $cache = new ObjectCacheReader();
        $invalid_cache_key = 'Sesame';
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $cache->get($invalid_cache_key);
    }


    #[TestDox('ObjectCacheReader::has() exists')]
    public function testObjectCacheReaderHasExists(): void
    {
        $class = new ReflectionClass(ObjectCacheReader::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testObjectCacheReaderHasExists')]
    #[TestDox('ObjectCacheReader::has() is public')]
    public function testObjectCacheReaderHasIsPublic(): void
    {
        $method = new ReflectionMethod(ObjectCacheReader::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testObjectCacheReaderHasExists')]
    #[TestDox('ObjectCacheReader::has() has one REQUIRED parameter')]
    public function testObjectCacheReaderHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ObjectCacheReader::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObjectCacheReaderHasExists')]
    #[Depends('testObjectCacheReaderHasIsPublic')]
    #[Depends('testObjectCacheReaderHasHasOneRequiredParameter')]
    #[TestDox('ObjectCacheReader::has() returns bool')]
    public function testObjectCacheReaderHasReturnsBool(): void
    {
        $cache = new ObjectCacheReader();
        $uuid = UUIDFactory::randomUUID();
        $key = (string) $uuid;

        $this->assertIsString($key);
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
    }

    #[Depends('testObjectCacheReaderHasHasOneRequiredParameter')]
    #[Depends('testObjectCacheReaderHasReturnsBool')]
    #[TestDox('ObjectCacheReader::has() accepts UUID object')]
    public function testObjectCacheReaderHasAcceptsUuidObject(): void
    {
        $cache = new ObjectCacheReader();
        $uuid = UUIDFactory::randomUUID();

        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $uuid);
        $this->assertIsBool($cache->has($uuid));
        $this->assertFalse($cache->has($uuid));

        $key = 4711;
        $this->expectException(\TypeError::class);
        $failure = $cache->has($key);
    }

    #[Depends('testObjectCacheReaderHasHasOneRequiredParameter')]
    #[TestDox('ObjectCacheReader::has() throws TypeError exception on CacheKey object')]
    public function testObjectCacheReaderHasThrowsTypeErrorExceptionOnCacheKeyObject(): void
    {
        $cache = new ObjectCacheReader();
        $key = new CacheKey(random_bytes(20));

        $this->assertIsObject($key);
        $this->expectException(\TypeError::class);
        $failure = $cache->has($key);
    }

    #[Depends('testObjectCacheReaderHasHasOneRequiredParameter')]
    #[Depends('testObjectCacheReaderHasReturnsBool')]
    #[TestDox('ObjectCacheReader::has() returns (bool) false on empty string')]
    public function testObjectCacheReaderHasReturnsBoolFalseOnEmptyString(): void
    {
        $cache = new ObjectCacheReader();
        $key = '';

        $this->assertEmpty($key);
        $this->assertIsBool($cache->has($key));
        $this->assertFalse($cache->has($key));
    }
}
