<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\LogFileManager::class)]
#[Group('security')]
final class LogFileManagerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('LogFileManager class is concrete')]
    public function testLogFileManagerClassIsConcrete(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LogFileManager is countable')]
    public function testLogFileManagerImplementsPsr7StreamInterface(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->isSubclassOf(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LogFileManager::VERSION);
        $this->assertIsString(LogFileManager::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LogFileManager::VERSION, '0.2.1', '>=')
        );
    }


    #[TestDox('LogFileManager::__construct exists')]
    public function testLogFileManagerConstructorExists(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testLogFileManagerConstructorExists')]
    #[TestDox('LogFileManager::__construct is public constructor')]
    public function testLogFileManagerConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testLogFileManagerConstructorExists')]
    #[TestDox('LogFileManager::__construct has no parameters')]
    public function testLogFileManagerConstructorHasNoParameters(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, '__construct');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('LogFileManager::clear exists')]
    public function testLogFileManagerClearExists(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[Depends('testLogFileManagerClearExists')]
    #[TestDox('LogFileManager::clear is public')]
    public function testLogFileManagerClearIsPublic(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'clear');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testLogFileManagerClearExists')]
    #[TestDox('LogFileManager::clear has one OPTIONAL parameter')]
    public function testLogFileManagerClearHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'clear');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('LogFileManager::count exists')]
    public function testLogFileManagerCountExists(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testLogFileManagerCountExists')]
    #[TestDox('LogFileManager::count exists')]
    public function testLogFileManagerCountIsPublic(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'count');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testLogFileManagerCountExists')]
    #[TestDox('LogFileManager::count has no parameters')]
    public function testLogFileManagerCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLogFileManagerCountExists')]
    #[Depends('testLogFileManagerCountIsPublic')]
    #[Depends('testLogFileManagerCountHasNoParameters')]
    #[TestDox('LogFileManager::count returns integer')]
    public function testLogFileManagerCountReturnsInteger(): void
    {
        $logbook = new LogFileManager();

        $this->assertIsInt($logbook->count());
        $this->assertIsInt(count($logbook));

        $this->assertGreaterThanOrEqual(0, $logbook->count());
        $this->assertGreaterThanOrEqual(0, count($logbook));
    }


    #[TestDox('LogFileManager::read exists')]
    public function testLogFileManagerReadExists(): void
    {
        $class = new ReflectionClass(LogFileManager::class);
        $this->assertTrue($class->hasMethod('read'));
    }

    #[Depends('testLogFileManagerReadExists')]
    #[TestDox('LogFileManager::read exists')]
    public function testLogFileManagerReadIsPublic(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'read');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testLogFileManagerReadExists')]
    #[TestDox('LogFileManager::read has no parameters')]
    public function testLogFileManagerReadHasNoParameters(): void
    {
        $method = new ReflectionMethod(LogFileManager::class, 'read');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testLogFileManagerReadExists')]
    #[Depends('testLogFileManagerReadIsPublic')]
    #[Depends('testLogFileManagerReadHasNoParameters')]
    #[Depends('testLogFileManagerCountReturnsInteger')]
    #[TestDox('LogFileManager::read returns null or non-empty string')]
    public function testLogFileManagerReadReturnsNullOrNonEmptyString(): void
    {
        $logbook = new LogFileManager();

        if ($logbook->count() === 0) {
            $this->assertNull($logbook->read());
        } else {
            $this->assertNotEmpty($logbook->read());
            $this->assertIsString($logbook->read());
        }
    }
}
