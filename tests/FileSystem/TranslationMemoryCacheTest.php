<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\FileSystem\TranslationMemoryCache::class)]
class TranslationMemoryCacheTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TranslationMemoryCache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TranslationMemoryCache::VERSION);
        $this->assertIsString(TranslationMemoryCache::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TranslationMemoryCache::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('TranslationMemoryCache::rebuild exists')]
    public function testTranslationMemoryCacheRebuildExists(): void
    {
        $class = new ReflectionClass(TranslationMemoryCache::class);
        $this->assertTrue($class->hasMethod('rebuild'));
    }

    #[Depends('testTranslationMemoryCacheRebuildExists')]
    #[TestDox('TranslationMemoryCache::rebuild is public static')]
    public function testTranslationMemoryCacheRebuildIsPublicStatic(): void
    {
        $method = new ReflectionMethod(TranslationMemoryCache::class, 'rebuild');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testTranslationMemoryCacheRebuildExists')]
    #[TestDox('TranslationMemoryCache::rebuild has no parameters')]
    public function testTranslationMemoryCacheRebuildHasNoParameters(): void
    {
        $method = new ReflectionMethod(TranslationMemoryCache::class, 'rebuild');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
