<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\CRM\Organization;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\OnlineBusiness::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class OnlineBusinessTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OnlineBusiness class is concrete')]
    public function testOnlineBusinessClassIsConcrete(): void
    {
        $class = new ReflectionClass(OnlineBusiness::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Depends('testOnlineBusinessClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('OnlineBusiness is an Organization')]
    public function testOnlineBusinessIsOrganization(): void
    {
        $this->assertInstanceOf(Organization::class, new OnlineBusiness());
    }

    #[Depends('testOnlineBusinessIsOrganization')]
    #[Group('hmvc')]
    #[TestDox('OnlineBusiness.OrganizationType is OnlineBusiness')]
    public function testOnlineBusinessOrganizationTypeIsOnlineBusiness(): void
    {
        $organization = new OnlineBusiness();
        $this->assertObjectHasProperty('type', $organization);

        $this->assertTrue(enum_exists('\\StoreCore\\CRM\\OrganizationType'));
        $this->assertEquals(\StoreCore\CRM\OrganizationType::OnlineBusiness, $organization->type);
        $this->assertEquals('https://schema.org/OnlineBusiness', $organization->type->value);
    }


    #[Group('hmvc')]
    #[TestDox('OnlineBusiness implements StoreCore IdentityInterface')]
    public function testOnlineBusinessImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new OnlineBusiness());
    }


    #[Group('hmvc')]
    #[TestDox('OnlineBusiness is JSON serializable')]
    public function testOnlineBusinessIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new OnlineBusiness());
    }

    #[Depends('testOnlineBusinessIsJsonSerializable')]
    #[Depends('testOnlineBusinessOrganizationTypeIsOnlineBusiness')]
    #[Group('hmvc')]
    #[TestDox('Schema.org @type is OnlineBusiness')]
    public function testSchemaOrgTypeIsOnlineBusiness(): void
    {
        $organization = new OnlineBusiness();
        $json = json_encode($organization);
        $this->assertStringContainsString('"@type":"OnlineBusiness"', $json);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OnlineBusiness::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OnlineBusiness::VERSION);
        $this->assertIsString(OnlineBusiness::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OnlineBusiness::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    /**
     * @see https://schema.org/OnlineBusiness#examples
     *      Schema.org OnlineBusiness Examples
     */
    #[TestDox('Examples')]
    public function testExamples(): void
    {
        $organization = new OnlineBusiness();
        $organization->name = 'Awesome Marketplace';
        $organization->url = 'http://example.com/';

        $json = json_encode($organization, \JSON_UNESCAPED_SLASHES);

        $this->assertIsString($json);
        $this->assertNotEmpty($json);
        $this->assertStringContainsString('"@context":"https://schema.org"', $json);
        $this->assertStringContainsString('"@type":"OnlineBusiness"', $json);
        $this->assertStringContainsString('"name":"Awesome Marketplace"', $json);
        $this->assertStringContainsString('"url":"http://example.com/"', $json);
    }
}
