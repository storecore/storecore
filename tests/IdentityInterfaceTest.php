<?php

declare(strict_types=1);

namespace StoreCore;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
#[Group('hmvc')]
final class IdentityInterfaceTest extends TestCase
{
    #[TestDox('IdentityInterface interface file exists')]
    public function testIdentityInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'IdentityInterface.php');
    }

    #[Depends('testIdentityInterfaceInterfaceFileExists')]
    #[TestDox('IdentityInterface interface file is readable')]
    public function testIdentityInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'IdentityInterface.php');
    }

    #[Depends('testIdentityInterfaceInterfaceFileExists')]
    #[Depends('testIdentityInterfaceInterfaceFileIsReadable')]
    #[TestDox('IdentityInterface interface exists')]
    public function testIdentityInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertTrue(interface_exists(IdentityInterface::class));
    }
}
