<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversNothing]
#[Group('hmvc')]
class DependencyExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DependencyException class exists')]
    public function testDependencyExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'DependencyException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'DependencyException.php');

        $this->assertTrue(class_exists('\\StoreCore\\DependencyException'));
        $this->assertTrue(class_exists(DependencyException::class));
    }

    #[TestDox('DependencyException class is concrete')]
    public function testDependencyExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DependencyException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('DependencyException is throwable logic exception')]
    public function testDependencyExceptionIsThrowableLogicException(): void
    {
        $class = new ReflectionClass(DependencyException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\LogicException::class));

        $this->expectException(\StoreCore\LogicException::class);
        throw new DependencyException();
    }
}
