<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2017, 2019–2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;
use function \random_int;
use function \version_compare;

use const \PHP_INT_MAX;

#[CoversClass(\StoreCore\Registry::class)]
#[CoversClass(\StoreCore\AbstractContainer::class)]
#[Group('hmvc')]
final class RegistryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Registry class file exists')]
    public function testRegistryClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Registry.php');
    }

    #[Depends('testRegistryClassFileExists')]
    #[Group('distro')]
    #[TestDox('Registry class file is readable')]
    public function testRegistryClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Registry.php');
    }

    #[Depends('testRegistryClassFileExists')]
    #[Depends('testRegistryClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('Registry class exists')]
    public function testRegistryClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Registry'));
        $this->assertTrue(class_exists(Registry::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Registry::VERSION);
        $this->assertIsString(Registry::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Registry::VERSION, '1.0.0', '>=')
        );
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPsr11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPsr11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Registry implements PSR-11 ContainerInterface')]
    public function testRegistryImplementsPsr11ContainerInterface(): void
    {
        $registry = Registry::getInstance();
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $registry);
    }

    #[Group('hmvc')]
    #[TestDox('Registry implements SingletonInterface')]
    public function testRegistryImplementsSingletonInterface(): void
    {
        $registry = Registry::getInstance();
        $this->assertInstanceOf(\StoreCore\SingletonInterface::class, $registry);
    }

    #[Group('hmvc')]
    #[TestDox('Registry singleton cannot be instantiated')]
    public function testRegistrySingletonCannotBeInstantiated(): void
    {
        $reflection = new ReflectionClass(Registry::class);
        $this->assertFalse($reflection->isInstantiable());

        $constructor = $reflection->getConstructor();
        $this->assertFalse($constructor->isPublic());
        $this->assertFalse($constructor->isProtected());
    }

    #[Group('hmvc')]
    #[TestDox('Registry singleton cannot be cloned')]
    public function testRegistrySingletonCannotBeCloned(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertFalse($class->isCloneable());
    }


    #[Group('hmvc')]
    #[TestDox('Registry consuming AbstractController class file exists')]
    public function testRegistryConsumingAbstractControllerClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractController.php'
        );
    }

    #[Depends('testRegistryConsumingAbstractControllerClassFileExists')]
    #[Group('hmvc')]
    #[TestDox('AbstractController class file is readable')]
    public function testAbstractControllerClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractController.php'
        );
    }

    #[Depends('testRegistryConsumingAbstractControllerClassFileExists')]
    #[Depends('testAbstractControllerClassFileIsReadable')]
    #[Group('hmvc')]
    #[TestDox('AbstractController class is abstract')]
    public function testAbstractControllerClassIsAbstract(): void
    {
        $class = new ReflectionClass(\StoreCore\AbstractController::class);
        $this->assertTrue($class->isAbstract());
    }


    #[Group('hmvc')]
    #[TestDox('Registry consuming AbstractModel class file exists')]
    public function testRegistryConsumingAbstractModelClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractModel.php'
        );
    }

    #[Depends('testRegistryConsumingAbstractModelClassFileExists')]
    #[Group('hmvc')]
    #[TestDox('AbstractModel class file is readable')]
    public function testAbstractModelClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractModel.php'
        );
    }

    #[Depends('testRegistryConsumingAbstractModelClassFileExists')]
    #[Depends('testAbstractModelClassFileIsReadable')]
    #[Group('hmvc')]
    #[TestDox('AbstractModel class is abstract')]
    public function testAbstractModelClassIsAbstract(): void
    {
        $class = new ReflectionClass(\StoreCore\AbstractModel::class);
        $this->assertTrue($class->isAbstract());
    }


    #[TestDox('Registry::get() exists')]
    public function testRegistryGetExists(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('Registry::get() is public')]
    public function testRegistryGetIsPublic(): void
    {
        $method = new ReflectionMethod(Registry::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Registry::get() has one REQUIRED parameter')]
    public function testRegistryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Registry::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRegistryGetIsPublic')]
    #[Depends('testRegistryGetHasOneRequiredParameter')]
    #[TestDox('Registry::get() throws \Psr\Container\ContainerExceptionInterface on invalid id')]
    public function testRegistryGetThrowsPsrContainerContainerExceptionInterfaceOnInvalidId(): void
    {
        $registry = Registry::getInstance();
        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        $failure = $registry->get('');
    }

    #[Depends('testRegistryGetIsPublic')]
    #[Depends('testRegistryGetHasOneRequiredParameter')]
    #[TestDox('Registry::get() throws \Psr\Container\NotFoundExceptionInterface on missing object')]
    public function testRegistryGetThrowsPsrContainerNotFoundExceptionInterfaceOnMissingObject(): void
    {
        $registry = Registry::getInstance();
        $this->assertFalse($registry->has('FooBar'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $registry->get('FooBar');
    }


    #[TestDox('Registry::getInstance() exists')]
    public function testRegistryGetInstanceExists(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertTrue($class->hasMethod('getInstance'));
    }

    #[Depends('testRegistryGetInstanceExists')]
    #[TestDox('Registry::getInstance() is public static')]
    public function testRegistryGetInstanceIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Registry::class, 'getInstance');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testRegistryGetInstanceExists')]
    #[TestDox('Registry::getInstance() has no parameters')]
    public function testRegistryGetInstanceHasNoParameters(): void
    {
        $method = new ReflectionMethod(Registry::class, 'getInstance');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRegistryGetInstanceIsPublicStatic')]
    #[Depends('testRegistryGetInstanceHasNoParameters')]
    #[TestDox('Registry::getInstance() returns self')]
    public function testRegistryGetInstanceReturnsSelf(): void
    {
        $registry = Registry::getInstance();
        $this->assertSame($registry::getInstance(), $registry);
    }


    #[TestDox('Registry::has() exists')]
    public function testRegistryHasExists(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('Registry::has() is public')]
    public function testRegistryHasIsPublic(): void
    {
        $method = new ReflectionMethod(Registry::class, 'has');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Registry::has() has one REQUIRED parameter')]
    public function testRegistryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Registry::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRegistryHasIsPublic')]
    #[Depends('testRegistryHasHasOneRequiredParameter')]
    #[TestDox('Registry::has() returns false if object does not exist')]
    public function testRegistryHasReturnsFalseIfObjectDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $this->assertIsBool($registry->has('Foo'));
        $this->assertFalse($registry->has('Foo'));
    }

    #[Depends('testRegistryHasReturnsFalseIfObjectDoesNotExist')]
    #[TestDox('Registry::has() returns true if object exists')]
    public function testRegistryHasReturnsTrueIfObjectExists(): void
    {
        $registry = Registry::getInstance();
        $object = new \stdClass();
        $registry->set('Foo', $object);
        $this->assertIsBool($registry->has('Foo'));
        $this->assertTrue($registry->has('Foo'));
    }


    #[TestDox('Registry::set() exists')]
    public function testRegistrySetExists(): void
    {
        $class = new ReflectionClass(Registry::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testRegistrySetExists')]
    #[TestDox('Registry::set() is public')]
    public function testRegistrySetIsPublic(): void
    {
        $method = new ReflectionMethod(Registry::class, 'set');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRegistrySetExists')]
    #[TestDox('Registry::set() has two required parameters')]
    public function testRegistrySetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Registry::class, 'set');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Group('distro')]
    #[TestDox('Implemented PSR-11 ContainerExceptionInterface exists')]
    public function testImplementedPSR11ContainerExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerExceptionInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerExceptionInterfaceExists')]
    #[TestDox('Registry::set() throws PSR-11 ContainerExceptionInterface on empty value')]
    public function testRegistrySetThrowsPSR11ContainerExceptionInterfaceOnEmptyValue(): void
    {
        $unknown = '';
        $this->assertEmpty($unknown);

        $registry = Registry::getInstance();
        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        $registry->set('FooBar', $unknown);
    }

    #[Depends('testImplementedPSR11ContainerExceptionInterfaceExists')]
    #[Depends('testRegistrySetThrowsPSR11ContainerExceptionInterfaceOnEmptyValue')]
    #[TestDox('Registry::set() does not accept null value')]
    public function testRegistrySetDoesNotAcceptNullValue(): void
    {
        $registry = Registry::getInstance();
        $this->expectException(\Psr\Container\ContainerExceptionInterface::class);
        $registry->set('Order', null);
    }


    #[TestDox('Registry container stores readable objects')]
    public function testRegistryContainerStoresReadableObjects(): void
    {
        $registry = Registry::getInstance();
        $object = new \stdClass();
        $this->assertFalse($registry->has('Baz'));

        $registry->set('Baz', $object);
        $this->assertTrue($registry->has('Baz'));
        $this->assertSame($object, $registry->get('Baz'));

        $object->qux = random_int(0, PHP_INT_MAX);
        $this->assertSame($object->qux, $registry->get('Baz')->qux);
    }
}
