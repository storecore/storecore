<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\State::class)]
final class StateTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('State class exists')]
    public function testStateClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'State.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'State.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\State'));
        $this->assertTrue(class_exists(State::class));
    }

    #[Group('hmvc')]
    #[TestDox('State class is concrete')]
    public function testStateClassIsConcrete(): void
    {
        $class = new ReflectionClass(State::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('State is JSON serializable')]
    public function testStateIsJsonSerializable(): void
    {
        $class = new ReflectionClass(State::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(State::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(State::VERSION);
        $this->assertIsString(State::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(State::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('State::__construct() exists')]
    public function testStateConstructorExists(): void
    {
        $class = new ReflectionClass(State::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('State::__construct() is public constructor')]
    public function testStateConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(State::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('State::__construct() has three parameters')]
    public function testStateConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(State::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testStateConstructorHasThreeParameters')]
    #[TestDox('State::__construct() has one REQUIRED parameter')]
    public function testStatesConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(State::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[Depends('testStatesConstructorHasOneRequiredParameter')]
    #[TestDox('State.identifier exists')]
    public function testStateIdentifierExists(): void
    {
        $province = new State('BE-VLA');
        $this->assertNotNull($province->identifier);
        $this->assertNotEmpty($province->identifier);
        $this->assertIsString($province->identifier);
    }

    #[Depends('testStatesConstructorHasOneRequiredParameter')]
    #[TestDox('State::__construct() sets State.identifier as case-insensitive string')]
    public function testStateConstructorSetsStateIdentifierAsCaseInsensitiveString(): void
    {
        $region = new State('it-52');
        $this->assertEquals('IT-52', $region->identifier);

        $province = new State('NL-nb');
        $this->assertEquals('NL-NB', $province->identifier);

        $bundesland = new State('de-nw');
        $this->assertEquals('DE-NW', $bundesland->identifier);
    }

    #[TestDox('Underscore in State.identifier is replaced with hyphen')]
    public function testUnderscoreInStateIdentifierIsReplacedWithHyphen(): void
    {
        $region = new State('FR_IDF');
        $this->assertSame('FR-IDF', $region->identifier);
    }

    #[TestDox('Third character in State.identifier must be hyphen')]
    public function testThirdCharacterInStateIdentifierMustBeHyphen(): void
    {
        $this->expectException(\ValueError::class);
        $state = new State('USA-MT');
    }

    #[TestDox('State.identifier be at least four characters')]
    public function testStateIdentifierBeAtLeastFourCharacters(): void
    {
        $this->expectException(\ValueError::class);
        $state = new State('NLD');
    }


    #[Depends('testStateConstructorHasThreeParameters')]
    #[TestDox('OPTIONAL State.name exists')]
    public function testOptionalStateNameExists(): void
    {
        $state = new State('US-MT');
        $this->assertNull($state->name);

        $state = new State('US-MT', 'Montana');
        $this->assertNotNull($state->name);
        $this->assertIsString($state->name);
    }

    #[Depends('testStateConstructorHasThreeParameters')]
    #[Depends('testOptionalStateNameExists')]
    #[TestDox('State::__construct() sets State.name as string')]
    public function testStateConstructorSetsStateNameAsString(): void
    {
        $state = new State('US-MT');
        $this->assertEmpty($state->name);
        $this->assertIsNotString($state->name);

        $state = new State('US-MT', 'Montana');
        $this->assertIsString($state->name);
        $this->assertEquals('Montana', $state->name);
    }


    #[Depends('testOptionalStateNameExists')]
    #[TestDox('OPTIONAL State.alternateName exists')]
    public function testOptionalStateAlternateNameExists(): void
    {
        $state = new State('BD-13', 'ঢাকা');
        $this->assertNull($state->alternateName);

        $state->alternateName = 'Dhaka';
        $this->assertNotNull($state->alternateName);
        $this->assertNotEmpty($state->alternateName);
        $this->assertIsString($state->alternateName);
        $this->assertNotEquals($state->name, $state->alternateName);
    }

    #[Depends('testStateConstructorHasThreeParameters')]
    #[TestDox('State::__construct() sets State.alternateName as string')]
    public function testStateConstructorSetsStateAlternateNameAsString(): void
    {
        $region = new State('IT-52', 'Toscana', 'Tuscany');

        $this->assertEquals('IT-52',   $region->identifier);
        $this->assertEquals('Toscana', $region->name);
        $this->assertEquals('Tuscany', $region->alternateName);
    }


    #[TestDox('State::jsonSerialize() exists')]
    public function testStateJsonSerializeExists(): void
    {
        $class = new \ReflectionClass(State::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[TestDox('State::jsonSerialize() is public')]
    public function testStateGsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(State::class, 'jsonSerialize');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('State::jsonSerialize() has no parameters')]
    public function testStateJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(State::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('State::jsonSerialize() returns non-empty array')]
    public function testStateJsonSerializeReturnsNonEmptyArray(): void
    {
        $state = new State('US-MT', 'Montana');
        $this->assertNotEmpty($state->jsonSerialize());
        $this->assertIsArray($state->jsonSerialize());
    }

    #[TestDox('State::jsonSerialize() returns Schema.org @type State')]
    public function testStateJsonSerializeReturnsSchemaOrgTypeState(): void
    {
        $state = new State('US-MT', 'Montana');
        $this->assertArrayHasKey('@context', $state->jsonSerialize());
        $this->assertEquals('https://schema.org', $state->jsonSerialize()['@context']);
        $this->assertArrayHasKey('@type', $state->jsonSerialize());
        $this->assertEquals('State', $state->jsonSerialize()['@type']);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "State",
              "identifier": "US-MT",
              "name": "Montana"
            }
        ';
        $actualJson = json_encode($state, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Depends('testStateJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('State::jsonSerialize() returns State.identifier')]
    public function testStateJsonSerializeReturnsStateIdentifier(): void
    {
        $state = new State('US-MT', 'Montana');
        $this->assertArrayHasKey('identifier', $state->jsonSerialize());
        $this->assertSame('US-MT', $state->jsonSerialize()['identifier']);

        $json = json_encode($state, \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"identifier":"US-MT"', $json);
    }

    #[Depends('testStateJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('State::jsonSerialize() returns optional State.name')]
    public function testStateJsonSerializeReturnsOptionalStateName(): void
    {
        $state = new State('US-MT', 'Montana');
        $this->assertSame('Montana', $state->name);
        $this->assertArrayHasKey('name', $state->jsonSerialize());
        $this->assertSame('Montana', $state->jsonSerialize()['name']);

        $json = json_encode($state, \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"name":"Montana"', $json);
    }


    #[TestDox('State::jsonSerialize() returns optional State.alternateName')]
    public function testStateJsonSerializeReturnsOptionalStateAlternateName(): void
    {
        $state = new State('BD-13', 'ঢাকা');
        $state->alternateName = 'Dhaka';

        $this->assertSame('Dhaka', $state->alternateName);
        $this->assertArrayHasKey('alternateName', $state->jsonSerialize());
        $this->assertSame('Dhaka', $state->jsonSerialize()['alternateName']);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "State",
              "identifier": "BD-13",
              "name": "ঢাকা",
              "alternateName": "Dhaka"
            }
        ';
        $actualJson = json_encode($state, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[TestDox('Optional State.alternateName is not returned if it equals State.name')]
    public function testOptionalStateAlternateNameIsNotReturnedIfItEqualsStateName(): void
    {
        $state = new State('US-CA', 'California');
        $state->alternateName = 'California';

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "State",
              "identifier": "US-CA",
              "name": "California"
            }
        ';
        $actualJson = json_encode($state, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
