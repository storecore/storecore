<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use StoreCore\Registry;
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\PostalAddress::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\CountryRepository::class)]
#[UsesClass(\StoreCore\Geo\State::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class PostalAddressTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PostalAddress class exists')]
    public function testPostalAddressClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'PostalAddress.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'PostalAddress.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\PostalAddress'));
        $this->assertTrue(class_exists(PostalAddress::class));
    }

    #[TestDox('PostalAddress class is concrete')]
    public function testPostalAddressClassIsConcrete(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\Psr\Container\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('PostalAddress implements PSR-11 ContainerInterface')]
    public function testCountryRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress implements StoreCore IdentityInterface')]
    public function testCountryRepositoryImplementsStoreCoreIdentityInterface(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\IdentityInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PostalAddress::VERSION);
        $this->assertIsString(PostalAddress::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PostalAddress::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('PostalAddress::__construct() exists')]
    public function testPostalAddressConstructorExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('PostalAddress::__construct() is public constructor')]
    public function testPostalAddressConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('PostalAddress::__construct() has one OPTIONAL parameter')]
    public function testPostalAddressConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('PostalAddress has non-empty dateCreated property')]
    public function testPostalAddressHasNonEmptyDateCreatedProperty(): void
    {
        $address = new PostalAddress();
        $this->assertNotNull($address->dateCreated);
        $this->assertNotEmpty($address->dateCreated);
        $this->assertIsString($address->dateCreated);

        $timestamp = strtotime($address->dateCreated);
        $this->assertNotFalse($timestamp);
        $this->assertIsInt($timestamp);
    }


    #[TestDox('PostalAddress::__get() exists')]
    public function testPostalAddressMagicGetExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('__get'));
    }

    #[Depends('testPostalAddressMagicGetExists')]
    #[TestDox('PostalAddress::__get() returns null if property does not exist')]
    public function testPostalAddressMagicGetReturnsNullIIfPropertyDoesNotExist(): void
    {
        $address = new PostalAddress();
        $this->assertNull($address->__get('fooBar'));
        $this->assertNull($address->fooBar);

        $address->fooBar = 42;
        $this->assertNotNull($address->__get('fooBar'));
        $this->assertNotNull($address->fooBar);
    }


    #[TestDox('PostalAddress::__set() exists')]
    public function testPostalAddressMagicSetExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('__set'));
    }


    #[TestDox('PostalAddress::get() exists')]
    public function testPostalAddressGetExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('PostalAddress::get() is public')]
    public function testPostalAddressGetIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'get');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::get() has one REQUIRED parameter')]
    public function testPostalAddressGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PostalAddress::get() throws Psr\Container\NotFoundExceptionInterface if property does not exist')]
    public function testPostalAddressGetThrowsPsrContainerNotFoundExceptionInterfaceIfPropertyDoesNotExist(): void
    {
        $address = new PostalAddress();
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $foo_bar = $address->get('fooBar');
    }


    #[TestDox('PostalAddress::getCountry() exists')]
    public function testPostalAddressGetCountryExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('getCountry'));
    }

    #[TestDox('PostalAddress::getCountry() is public')]
    public function testPostalAddressGetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getCountry');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::getCountry() has no parameters')]
    public function testPostalAddressGetCountryHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getCountry');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('PostalAddress::getCountry() returns null by default')]
    public function testPostalAddressGetCountryReturnsNullByDefault(): void
    {
        $address = new PostalAddress();
        $this->assertNull($address->getCountry());
    }

    #[TestDox('PostalAddress::getCountry() returns Country object')]
    public function testPostalAddressGetCountryReturnsCountryObject(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $address = new PostalAddress();
        $address->country_id = 'NL';
        $this->assertNotNull($address->getCountry());
        $this->assertIsObject($address->getCountry());
        $this->assertInstanceOf(Country::class, $address->getCountry());
    }


    #[TestDox('PostalAddress::getIdentifier() exists')]
    public function testPostalAddressGetIdentifierExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[TestDox('PostalAddress::getIdentifier() is public')]
    public function testPostalAddressGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::getIdentifier() has no parameters')]
    public function testPostalAddressGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('PostalAddress::getIdentifier() returns null by default')]
    public function testPostalAddressGetIdentifierReturnsNullByDefault(): void
    {
        $address = new PostalAddress();
        $this->assertNull($address->getIdentifier());
        $this->assertNull($address->identifier);
    }

    #[TestDox('PostalAddress::getIdentifier() returns UUID object if address identifier is set')]
    public function testPostalAddressGetIdentifierReturnsUUIDObjectIfPostalAddressIdentifierIsSet(): void
    {
        $address = new PostalAddress();
        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();
        $address->setIdentifier($uuid);

        $this->assertNotNull($address->getIdentifier());
        $this->assertIsObject($address->getIdentifier());
        $this->assertSame($uuid, $address->getIdentifier());
        $this->assertSame($address->identifier, (string) $address->getIdentifier());
    }


    #[TestDox('PostalAddress::has() exists')]
    public function testPostalAddressHasExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('PostalAddress::has() is public')]
    public function testPostalAddressHasIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'has');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::has() has one REQUIRED parameter')]
    public function testPostalAddressHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PostalAddress::has() returns false if property does not exist')]
    public function testPostalAddressHasReturnsFalseIfPropertyDoesNotExist(): void
    {
        $address = new PostalAddress();
        $this->assertFalse($address->has('fooBar'));
    }

    #[TestDox('PostalAddress::has() returns true if property does exist')]
    public function testPostalAddressHasReturnsTrueIfPropertyDoesExist(): void
    {
        $address = new PostalAddress(['fooBar' => 'Baz Qux']);
        $this->assertTrue($address->has('fooBar'));
    }


    #[TestDox('PostalAddress::setCountrySubdivision() exists')]
    public function testPostalAddressSetCountrySubdivisionExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('setCountrySubdivision'));
    }

    #[TestDox('PostalAddress::setCountrySubdivision() is public')]
    public function testPostalAddressSetCountrySubdivisionIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setCountrySubdivision');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('PostalAddress::setCountrySubdivision() has one REQUIRED parameter')]
    public function testPostalAddressSetCountrySubdivisionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setCountrySubdivision');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PostalAddress::setCountrySubdivision() sets PostalAddress.country_subdivision property')]
    public function testPostalAddressSetCountrySubdivisionSetsPostalAddressCountrySubdivisionProperty(): void
    {
        $address = new PostalAddress();
        $this->assertNull($address->country_subdivision);

        $address->setCountrySubdivision('NB');
        $this->assertNotNull($address->country_subdivision);
        $this->assertSame('NB', $address->country_subdivision);
    }

    #[Depends('testPostalAddressSetCountrySubdivisionHasOneRequiredParameter')]
    #[Depends('testPostalAddressSetCountrySubdivisionSetsPostalAddressCountrySubdivisionProperty')]
    #[TestDox('PostalAddress::setCountrySubdivision() sets one, two, or three uppercase letters')]
    public function testPostalAddressSetCountrySubdivisionSetsOneTwoOrThreeUppercaseLetters(): void
    {
        $address = new PostalAddress();

        $address->setCountrySubdivision('A');
        $this->assertSame('A', $address->country_subdivision);

        $address->setCountrySubdivision('a');
        $this->assertSame('A', $address->country_subdivision);


        $address->setCountrySubdivision('AB');
        $this->assertSame('AB', $address->country_subdivision);

        $address->setCountrySubdivision('ab');
        $this->assertSame('AB', $address->country_subdivision);


        $address->setCountrySubdivision('ABC');
        $this->assertSame('ABC', $address->country_subdivision);

        $address->setCountrySubdivision('abc');
        $this->assertSame('ABC', $address->country_subdivision);


        $this->expectException(\DomainException::class);
        $address->setCountrySubdivision('ABCD');
    }

    #[Depends('testPostalAddressSetCountrySubdivisionHasOneRequiredParameter')]
    #[Depends('testPostalAddressSetCountrySubdivisionSetsPostalAddressCountrySubdivisionProperty')]
    #[TestDox('PostalAddress::setCountrySubdivision() accepts short integers and numbers')]
    public function testPostalAddressSetCountrySubdivisionAcceptsShortIntegersAndNumbers(): void
    {
        $address = new PostalAddress();

        $address->setCountrySubdivision(1);
        $this->assertSame('01', $address->country_subdivision);

        $address->setCountrySubdivision('1');
        $this->assertSame('01', $address->country_subdivision);

        $address->setCountrySubdivision('01');
        $this->assertSame('01', $address->country_subdivision);


        $address->setCountrySubdivision(23);
        $this->assertSame('23', $address->country_subdivision);

        $address->setCountrySubdivision('23');
        $this->assertSame('23', $address->country_subdivision);


        $address->setCountrySubdivision(99);
        $this->assertSame('99', $address->country_subdivision);

        $this->expectException(\DomainException::class);
        $address->setCountrySubdivision(100);
    }

    #[Depends('testPostalAddressSetCountrySubdivisionHasOneRequiredParameter')]
    #[Depends('testPostalAddressSetCountrySubdivisionSetsPostalAddressCountrySubdivisionProperty')]
    #[TestDox('PostalAddress::setCountrySubdivision() accepts StoreCore\Geo\State object')]
    public function testPostalAddressSetCountrySubdivisionAcceptsStoreCoreGeoStateObject(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $address = new PostalAddress();
        $this->assertNull($address->country_subdivision);

        $state = new State('US-MT', 'Montana');
        $address->setCountrySubdivision($state);
        $this->assertNotNull($address->country_subdivision);
    }


    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-benelux/documentation-rest/
     *      PostNL Benelux address check documentation
     */
    #[Depends('testPostalAddressGetCountryReturnsCountryObject')]
    #[TestDox('PostalAddress models common JSON messages')]
    public function testPostalAddressModelsCommonJsonMessages(): void
    {
        $json = '
            {
              "City": "Antwerpen",
              "CountryIso": "BEL",
              "PostalCode": "2000",
              "Street": "Oude Koornmarkt",
              "HouseNumber": "39",
              "HouseNumberAddition": ""
            }
        ';

        $params = json_decode($json, true);
        $this->assertNotNull($params);
        $this->assertIsArray($params);

        $address = new PostalAddress($params);

        $this->assertSame('Antwerpen', $address->City);
        $this->assertSame('Antwerpen', $address->locality);

        $this->assertSame(56, $address->country_id);
        $this->assertNotNull($address->getCountry());
        $this->assertSame('BE', $address->getCountry()->identifier);
        $this->assertSame('Belgium', $address->getCountry()->name);

        $this->assertSame('2000', $address->PostalCode);
        $this->assertSame('2000', $address->postalCode);

        $this->assertSame('Oude Koornmarkt', $address->Street);
        $this->assertSame('Oude Koornmarkt', $address->streetName);

        $this->assertSame('39', $address->HouseNumber);
        $this->assertSame('39', $address->houseNumber);
        
        // Empty strings should be ignored.
        $this->assertFalse($address->has('HouseNumberAddition'));
        $this->assertNull($address->HouseNumberAddition);
        $this->assertFalse($address->has('houseNumberAddition'));
        $this->assertNull($address->houseNumberAddition);
    }


    #[TestDox('Indexed array with just one address is handled as full address')]
    public function testIndexedArrayWithJustOneAddressIsHandledAsFullAddress(): void
    {
        $json = '[{"PostalCode": "1507TN", "HouseNumber": "6", "HouseNumberAddition": null, "Street": "Wals", "City": "ZAANDAM", "FormattedAddress": [ "Wals 6", "1507TN ZAANDAM" ], "Coordinates": { "Latitude": 52.439944, "Longitude": 4.806337, "RdCoordinateX": 115503, "RdCoordinateY": 494843, "VierkantNetCodeX": 1155, "VierkantNetCodeY": 4948}}]';
        $params = json_decode($json, true);
        $this->assertNotNull($params);
        $this->assertNotFalse($params);
        $this->assertIsArray($params);
        $this->assertNotEmpty($params);
        
        $address = new PostalAddress($params);
        $this->assertSame('1507TN', $address->postalCode);
        $this->assertSame('6', $address->houseNumber);
        $this->assertNull($address->houseNumberAddition);
    }
}
