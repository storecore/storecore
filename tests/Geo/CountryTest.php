<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class CountryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Country class exists')]
    public function testCountryClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Country.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Country.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\Country'));
        $this->assertTrue(class_exists(Country::class));
    }

    #[Group('hmvc')]
    #[TestDox('Country class is concrete')]
    public function testCountryClassIsConcrete(): void
    {
        $class = new ReflectionClass(Country::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Country class is read-only')]
    public function testCountryClassIsReadOnly(): void
    {
        $class = new ReflectionClass(Country::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Country::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Country::VERSION);
        $this->assertIsString(Country::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Country::VERSION, '1.0.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Country is JSON serializable')]
    public function testCountryIsJsonSerializable(): void
    {
        $object = new Country('NL', 'Netherlands');
        $this->assertInstanceOf(\JsonSerializable::class, $object);
    }

    #[TestDox('Country::jsonSerialize() exists')]
    public function testCountryJsonSerializeExists(): void
    {
        $class = new ReflectionClass(Country::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[Depends('testCountryJsonSerializeExists')]
    #[TestDox('Country::jsonSerialize() is public')]
    public function testCountryJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(Country::class, 'jsonSerialize');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCountryJsonSerializeExists')]
    #[TestDox('Country::jsonSerialize() has no parameters')]
    public function testCountryJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(Country::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testCountryJsonSerializeExists')]
    #[Depends('testCountryJsonSerializeIsPublic')]
    #[Depends('testCountryJsonSerializeHasNoParameters')]
    #[TestDox('Country::jsonSerialize() returns non-empty array')]
    public function testCountryJsonSerializeReturnsNonEmptyArray(): void
    {
        $country = new Country('DK', 'Denmark');
        $this->assertNotEmpty($country->jsonSerialize());
        $this->assertIsArray($country->jsonSerialize());
    }

    #[Depends('testCountryJsonSerializeReturnsNonEmptyArray')]
    #[Group('seo')]
    #[TestDox('Country::jsonSerialize() returns Schema.org type Country')]
    public function testCountryJsonSerializeReturnsSchemaOrgTypeCountry(): void
    {
        $country = new Country('LU', 'Luxembourg');

        $this->assertArrayHasKey('@context', $country->jsonSerialize());
        $this->assertContains('https://schema.org', $country->jsonSerialize());
        $this->assertEquals('https://schema.org', $country->jsonSerialize()['@context']);

        $this->assertArrayHasKey('@type', $country->jsonSerialize());
        $this->assertContains('Country', $country->jsonSerialize());
        $this->assertEquals('Country', $country->jsonSerialize()['@type']);
    }


    #[TestDox('Country::__construct() exists')]
    public function testCountryConstructorExists(): void
    {
        $class = new ReflectionClass(Country::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Country::__construct() is public constructor')]
    public function testCountryConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Country::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Country::__construct() has three parameters')]
    public function testCountryConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Country::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testCountryConstructorHasThreeParameters')]
    #[TestDox('Country::__construct() has two REQUIRED parameters')]
    public function testCountryConstructorHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Country::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Country.name exists')]
    public function testCountryNameExists(): void
    {
        $country = new Country('BE', 'Belgium');
        $this->assertObjectHasProperty('name', $country);
        $this->assertNotEmpty($country->name);
        $this->assertIsString($country->name);
        $this->assertSame('Belgium', $country->name);
    }

    #[TestDox('Country.name is immutable')]
    public function testCountryNameIsImmutable(): void
    {
        $country = new Country('NL', 'Netherlands');
        $this->assertEquals('Netherlands', $country->name);

        $this->expectException(\Error::class);
        $country->name = 'Nederland';
    }

    #[TestDox('Country.alternateName exists')]
    public function testCountryAlternateNameExists(): void
    {
        $country = new Country('BE', 'Belgium');
        $this->assertObjectHasProperty('alternateName', $country);
        $this->assertNull($country->alternateName);
    }

    #[TestDox('Country::__construct() sets Country.alternateName')]
    public function testCountryConstructorSetsCountryAlternateName(): void
    {
        $country = new Country('BE', 'Belgium', 'Belgique');
        $this->assertEquals('Belgium', $country->name);
        $this->assertEquals('Belgique', $country->alternateName);
    }

    #[TestDox('Country.alternateName is immutable')]
    public function testCountryAlternateNameIsImmutable(): void
    {
        $country = new Country('BE', 'Belgium');

        $this->expectException(\Error::class);
        $country->alternateName = 'Belgique';
    }

    #[TestDox('Country.alternateName differs from Country.name')]
    public function testCountryAlternateDiffersFromCountryName(): void
    {
        // When `France` is set in English and French, for example …
        $country = new Country('FR', 'France', 'France');

        // … then `name` exists but `alternateName` is `null`:
        $this->assertEquals('France', $country->name);
        $this->assertNull($country->alternateName);
    }


    #[TestDox('Country.identifier exists')]
    public function testCountryIdentifierExists(): void
    {
        $country = new Country('BE', 'Belgium');
        $this->assertObjectHasProperty('identifier', $country);
    }

    #[TestDox('Country.identifier is non-empty string')]
    public function testCountryIdentifierIsNonEmptyString(): void
    {
        $country = new Country('BE', 'Belgium');
        $this->assertNotEmpty($country->identifier);
        $this->assertIsString($country->identifier);
    }

    #[Depends('testCountryIdentifierIsNonEmptyString')]
    #[TestDox('Country.identifier is two uppercase letters')]
    public function testCountryIdentifierIsTwoUppercaseLetters(): void
    {
        $country = new Country('Fr', 'France');
        $this->assertTrue(strlen($country->identifier) === 2);
        $this->assertTrue(ctype_upper($country->identifier));
        $this->assertSame('FR', $country->identifier);

        $country = new Country('DEU', 'Deutschland');
        $this->assertNotSame('DEU', $country->identifier);
        $this->assertSame('DE', $country->identifier);

        $country = new Country('012', 'Algeria');
        $this->assertNotSame('012', $country->identifier);
        $this->assertSame('DZ', $country->identifier);

        $country = new Country(40, 'Austria');
        $this->assertNotSame(40, $country->identifier);
        $this->assertIsNotInt($country->identifier);
        $this->assertIsString($country->identifier);
        $this->assertSame('AT', $country->identifier);
    }

    #[Depends('testCountryIdentifierIsTwoUppercaseLetters')]
    #[TestDox('Country.identifier is immutable')]
    public function testCountryIdentifierIsImmutable(): void
    {
        $country = new Country('NE', 'Niger');
        $this->assertSame('NE', $country->identifier);

        $this->expectException(\Error::class);
        $country->identifier = 'NG';
    }
}
