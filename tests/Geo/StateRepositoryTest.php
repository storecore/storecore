<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversClass(\StoreCore\Geo\StateRepository::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\State::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class StateRepositoryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('StateRepository class exists')]
    public function testStateRepositoryClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'StateRepository.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'StateRepository.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\StateRepository'));
        $this->assertTrue(class_exists(StateRepository::class));
    }

    #[TestDox('StateRepository class is concrete')]
    public function testStateRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('StateRepository is a database model')]
    public function testStateRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('StateRepository implements PSR-11 ContainerInterface')]
    public function testStateRepositoryImplementsPsr11ContainerInterface(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[TestDox('StateRepository::find() exists')]
    public function testStateRepositoryFindExists(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertTrue($class->hasMethod('find'));
    }

    #[TestDox('StateRepository::find() is public')]
    public function testStateRepositoryFindIsPublic(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'find');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StateRepository::find() has one REQUIRED parameter')]
    public function testStateRepositoryFindHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'find');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('StateRepository::find() returns StoreCore\Geo\State object')]
    public function testStateRepositoryFindReturnsStoreCoreGeoStateObject(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new StateRepository($registry);
        $state = $repository->find('Noord-Brabant');
        $this->assertIsObject($state);
        $this->assertInstanceOf(\StoreCore\Geo\State::class, $state);
        $this->assertSame('NL-NB', $state->identifier);
    }

    #[Group('distro')]
    #[TestDox('Implemented PSR-11 NotFoundExceptionInterface exists')]
    public function testImplementedPSR11NotFoundExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\NotFoundExceptionInterface::class));
    }

    #[Depends('testImplementedPSR11NotFoundExceptionInterfaceExists')]
    #[TestDox('Implemented PSR-11 NotFoundExceptionInterface exists')]
    public function testStateRepositoryFindThrowsPsr11NotFoundExceptionInterfaceOnStateNotFound(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new StateRepository($registry);
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $state = $repository->find('Foo Bar');
    }

    #[TestDox('StateRepository::find() throws PSR-11 NotFoundExceptionInterface on multiple matches')]
    public function testStateRepositoryFindThrowsPsr11NotFoundExceptionInterfaceOnMultipleMatches(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new StateRepository($registry);

        // Given that there are at least two provinces called “Brabant” …
        $state = $repository->find('Noord-Brabant');
        $this->assertInstanceOf(State::class, $state);
        $this->assertSame('NL-NB', $state->identifier);

        $state = $repository->find('Vlaams-Brabant');
        $this->assertInstanceOf(State::class, $state);
        $this->assertSame('BE-VBR', $state->identifier);

        // … a “Not Found” container interface exception is thrown 
        // because there are multiple instances of a `Brabant`.
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $state = $repository->find('Brabant');
    }


    #[TestDox('StateRepository::getAlternateName() exists')]
    public function testStateRepositoryGetAlternateNameExists(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertTrue($class->hasMethod('getAlternateName'));
    }

    #[TestDox('StateRepository::getAlternateName() is public')]
    public function testStateRepositoryGetAlternateNameIsPublic(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'getAlternateName');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StateRepository::getAlternateName() has two REQUIRED parameters')]
    public function testStateRepositoryGetAlternateNameHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'getAlternateName');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('StateRepository::getAlternateName() returns string or null')]
    public function testStateRepositoryGetAlternateNameReturnsStringOrNull(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        } else {
            $repository = new StateRepository($registry);
        }

        /* Province `NL-FR` is called `Friesland` in Dutch (`nl`)
         * and `Fryslân` in Western Frisian (`fy`).
         */
        $state = new State('NL-FR', 'Friesland');
        $alternate_name = $repository->getAlternateName($state->identifier, $state->name);
        $this->assertNotEmpty($alternate_name);
        $this->assertIsString($alternate_name);
        $this->assertNotEquals('Friesland', $alternate_name);
        $this->assertEquals('Fryslân', $alternate_name);

        /* Ontaria (`CA-ON`) is called `Ontario` in English (`en`) as well as
         * French (`fr`), so we do not expect to get an alternate name.
         */
        $state = new State('CA-ON', 'Ontario');
        $this->assertNull($repository->getAlternateName($state->identifier, $state->name));
    }


    #[TestDox('StateRepository::list() exists')]
    public function testStateRepositoryListExists(): void
    {
        $class = new ReflectionClass(StateRepository::class);
        $this->assertTrue($class->hasMethod('list'));
    }

    #[TestDox('StateRepository::list() is public')]
    public function testStateRepositoryListIsPublic(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'list');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StateRepository::list() has one REQUIRED parameter')]
    public function testStateRepositoryListHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StateRepository::class, 'list');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('StateRepository::list() returns array with provinces')]
    public function testStateRepositoryListReturnsArrayWithProvinces(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $country = new Country('NL', 'Nederland', 'Netherlands');
        $repository = new StateRepository($registry);
        $list = $repository->list($country);

        $this->assertNotEmpty($list);
        $this->assertIsArray($list);
        $this->assertCount(12, $list, 'The Netherlands have 12 provinces.');
        foreach ($list as $object) {
            $this->assertInstanceOf(\StoreCore\Geo\State::class, $object);
        }
    }
}
