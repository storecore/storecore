<?php

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Geo\Map::class)]
#[UsesClass(\StoreCore\Geo\MapCategoryType::class)]
#[UsesClass(\StoreCore\Geo\Place::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class MapTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Map class exists')]
    public function testMapClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Map.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Map.php');
        $this->assertTrue(class_exists('\\StoreCore\\Geo\\Map'));
        $this->assertTrue(class_exists(Map::class));
    }

    #[Group('hmvc')]
    #[TestDox('Map class is concrete')]
    public function testMapClassIsConcrete(): void
    {
        $class = new ReflectionClass(Map::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Map::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Map::VERSION);
        $this->assertIsString(Map::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Map::VERSION, '0.2.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('Map is a CreativeWork Thing')]
    public function testMapIsCreativeWorkThing(): void
    {
        $map = new Map();
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $map);
        $this->assertInstanceOf(\StoreCore\CMS\AbstractThing::class, $map);
    }

    #[Group('seo')]
    #[TestDox('Map is JSON serializable')]
    public function testMapIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Map());
    }


    #[TestDox('Map.mapType exists')]
    public function testMapMapTypeExists(): void
    {
        $this->assertObjectHasProperty('mapType', new Map());
    }

    #[TestDox('Map.mapType is null by default')]
    public function testMapMapTypeIsNullByDefault(): void
    {
        $map = new Map();
        $this->assertNull($map->mapType);
    }

    #[TestDox('Map.mapType accepts MapCategoryType enumeration member')]
    public function testMapMapTypeAcceptsMapCategoryTypeEnumerationMember(): void
    {
        $this->assertTrue(enum_exists('\\StoreCore\\Geo\\MapCategoryType'));
        $this->assertTrue(enum_exists(MapCategoryType::class));

        $map = new Map();
        $map->mapType = MapCategoryType::TransitMap;
        $this->assertNotNull($map->mapType);
        $this->assertIsObject($map->mapType);
        $this->assertEquals('TransitMap', $map->mapType->name);
        $this->assertEquals('https://schema.org/TransitMap', $map->mapType->value);
    }


    #[Group('seo')]
    #[TestDox('Map.url exists')]
    public function testMapUrlExists(): void
    {
        $this->assertObjectHasProperty('url', new Map());
    }

    #[Group('seo')]
    #[TestDox('Map.url is null by default')]
    public function testMapUrlIsNullByDefault(): void
    {
        $map = new Map();
        $this->assertNull($map->url);
    }

    #[Group('seo')]
    #[TestDox('Map.url accepts URL')]
    public function testMapUrlAcceptsURL(): void
    {
        $map = new Map();
        $map->url = new URL('http://art-sf.example.com/map1234/');
        $this->assertNotNull($map->url);
    }


    #[Depends('testMapIsJsonSerializable')]
    #[Group('seo')]
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest(): void
    {
        $place = new Museum();
        $place->name = 'SF art museum';

        $map = new Map();
        $map->mapType = MapCategoryType::VenueMap;
        $map->url = 'http://art-sf.example.com/map1234/';

        $place->hasMap = $map;

        /*
         * Note that the Schema.org example uses the enumeration
         * member’s URL as the `@id` of the `Map`.
         *
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "Museum",
         *   "name": "SF art museum",
         *   "hasMap": {
         *     "@type": "Map",
         *     "mapType": {
         *       "@id": "https://schema.org/VenueMap"
         *     },
         *     "url":  "http://art-sf.example.com/map1234/"
         *   }
         * }
         */
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Museum",
              "name": "SF art museum",
              "hasMap": {
                "@type": "Map",
                "mapType": "https://schema.org/VenueMap",
                "url":  "http://art-sf.example.com/map1234/"
              }
            }
        ';
        $actualJson = json_encode($place, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
