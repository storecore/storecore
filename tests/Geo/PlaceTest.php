<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use StoreCore\Types\Thing;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\GeoCoordinates::class)]
#[CoversClass(\StoreCore\Geo\Place::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class PlaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Place class exists')]
    public function testPlaceClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Place.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'Place.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\Place'));
        $this->assertTrue(class_exists(Place::class));
    }

    #[Group('hmvc')]
    #[TestDox('Place class is concrete')]
    public function testPlaceClassIsConcrete(): void
    {
        $class = new ReflectionClass(Place::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Place is a Thing')]
    public function testPlaceIsThing(): void
    {
        $place = new Place();
        $this->assertInstanceOf(Thing::class, $place);
    }

    #[Group('hmvc')]
    #[TestDox('Place is JSON serializable')]
    public function testPlaceIsJsonSerializable(): void
    {
        $place = new Place();
        $this->assertInstanceOf(\JsonSerializable::class, $place);
    }


    #[Group('hmvc')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Place::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Place::VERSION);
        $this->assertIsString(Place::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Place::VERSION, '0.5.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('Place.geo exists')]
    public function testPlaceGeoExists(): void
    {
        $place = new Place();
        $this->assertObjectHasProperty('geo', $place);
    }

    #[Group('hmvc')]
    #[TestDox('Place.geo is public')]
    public function testPlaceGeoIsPublic(): void
    {
        $property = new ReflectionProperty(Place::class, 'geo');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('Place.geo is null by default')]
    public function testPlaceGeoIsNull(): void
    {
        $place = new Place();
        $this->assertNull($place->geo);
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Place.geo accepts GeoCoordinates for latitude and longitude')]
    public function testPlaceGeoAcceptsGeoCoordinates(): void
    {
        $place = new Place('Empire State Building');
        $place->geo = new GeoCoordinates(40.75, -73.98);

        $this->assertInstanceOf(\StoreCore\Geo\GeoCoordinates::class, $place->geo);
        $this->assertEquals(40.75, $place->geo->latitude);
        $this->assertEquals(-73.98, $place->geo->longitude);
    }


    #[Group('hmvc')]
    #[TestDox('Place.hasMap exists')]
    public function testPlaceHasMapExists(): void
    {
        $this->assertObjectHasProperty('hasMap', new Place());
    }

    #[Group('hmvc')]
    #[TestDox('Place.hasMap is public')]
    public function testPlaceHasMapIsPublic(): void
    {
        $property = new ReflectionProperty(Place::class, 'hasMap');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Group('hmvc')]
    #[TestDox('Place.hasMap is null by default')]
    public function testPlaceHasMapIsNullByDefault(): void
    {
        $place = new Place();
        $this->assertNull($place->hasMap);
    }

    /**
     * @see https://schema.org/hasMap#eg-0362
     *      Schema.org property `hasMap` example 1
     */
    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Place.hasMap accepts URL')]
    public function testPlaceHasMapAcceptsURL(): void
    {
        $hotel = new Place();
        $hotel->name = 'ACME Hotel Innsbruck';
        $hotel->hasMap = new URL('https://www.google.com/maps?ie=UTF8&hq&ll=47.1234,11.1234&z=13');

        $this->assertNotNull($hotel->hasMap);
        $this->assertIsObject($hotel->hasMap);
    }
}
