<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \StoreCore\Types\CountryCode;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(CountryCodes::class)]
#[CoversClass(Country::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class CountryCodesTest extends TestCase
{
    #[TestDox('CountryCodes class is not instantiable')]
    public function testCountryCodesClassIsNotInstantiable(): void
    {
        $class = new ReflectionClass(CountryCodes::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('CountryCodes class is read-only')]
    public function testCountryCodesClassIsReadOnly(): void
    {
        $class = new ReflectionClass(CountryCodes::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CountryCodes::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CountryCodes::VERSION);
        $this->assertIsString(CountryCodes::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CountryCodes::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('CountryCodes::createCountryCode() exists')]
    public function testCountryCodesCreateCountryCodeExists(): void
    {
        $class = new ReflectionClass(CountryCodes::class);
        $this->assertTrue($class->hasMethod('createCountryCode'));
    }

    #[TestDox('CountryCodes::createCountryCode() is public static')]
    public function testCountryCodesCreateCountryCodeIsPublicStatic(): void
    {
        $method = new ReflectionMethod(CountryCodes::class, 'createCountryCode');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('CountryCodes::createCountryCode() has one REQUIRED parameter')]
    public function testCountryCodesCreateCountryCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CountryCodes::class, 'createCountryCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCountryCodesCreateCountryCodeIsPublicStatic')]
    #[Depends('testCountryCodesCreateCountryCodeHasOneRequiredParameter')]
    #[TestDox('CountryCodes::createCountryCode() returns CountryCode object on ISO alpha-2 string')]
    public function testCountryCodesCreateCountryCodeReturnsCountryCodeObjectOnIsoAlpha2String(): void
    {
        $countryCode = CountryCodes::createCountryCode('NL');
        $this->assertIsObject($countryCode);
        $this->assertInstanceOf(\StoreCore\Types\CountryCode::class, $countryCode);
    }

    #[Depends('testCountryCodesCreateCountryCodeReturnsCountryCodeObjectOnIsoAlpha2String')]
    #[TestDox('CountryCodes::createCountryCode() is not case-sensitive')]
    public function testCountryCodesCreateCountryCodeIsNotCaseSensitive(): void
    {
        $this->assertNotSame('FR', CountryCodes::createCountryCode('FR'));
        $this->assertEquals('FR', CountryCodes::createCountryCode('FR'));
        $this->assertEquals('FR', CountryCodes::createCountryCode('Fr'));
        $this->assertEquals('FR', CountryCodes::createCountryCode('fr'));
    }

    #[Depends('testCountryCodesCreateCountryCodeReturnsCountryCodeObjectOnIsoAlpha2String')]
    #[TestDox('CountryCodes::createCountryCode() accepts "UK" for "GB"')]
    public function testCountryCodesCreateCountryCodeAcceptsUkForGb(): void
    {
        $united_kingdom = CountryCodes::createCountryCode('UK');
        $great_britain  = CountryCodes::createCountryCode('GB');
        $this->assertEquals($united_kingdom, $great_britain);
        $this->assertEquals('GB', (string) $united_kingdom);
    }

    #[Depends('testCountryCodesCreateCountryCodeIsPublicStatic')]
    #[Depends('testCountryCodesCreateCountryCodeHasOneRequiredParameter')]
    #[TestDox('CountryCodes::createCountryCode() returns CountryCode object on ISO alpha-3 string')]
    public function testCountryCodesCreateCountryCodeReturnsCountryCodeObjectOnIsoAlpha3String(): void
    {
        $countryCode = CountryCodes::createCountryCode('NLD');
        $this->assertIsObject($countryCode);
        $this->assertInstanceOf(CountryCode::class, $countryCode);
    }

    #[Depends('testCountryCodesCreateCountryCodeReturnsCountryCodeObjectOnIsoAlpha3String')]
    #[TestDox('CountryCoCountryCodes::createCountryCode() maps ISO alpha-3 to ISO alpha-2')]
    public function testCountryCodesCreateCountryCodeMapsIsoAlpha3ToIsoAlpha2(): void
    {
        $countryCode = CountryCodes::createCountryCode('BEL');
        $this->assertSame('BE', (string) $countryCode);

        $countryCode = CountryCodes::createCountryCode('DEU');
        $this->assertSame('DE', (string) $countryCode);

        $countryCode = CountryCodes::createCountryCode('NLD');
        $this->assertSame('NL', (string) $countryCode);
    }

    #[Depends('testCountryCodesCreateCountryCodeIsPublicStatic')]
    #[Depends('testCountryCodesCreateCountryCodeHasOneRequiredParameter')]
    #[TestDox('CountryCodes::createCountryCode() accepts country number as integer')]
    public function testCountryCodesCreateCountryCodeAcceptsCountryNumberAsInteger(): void
    {
        $countryCode = CountryCodes::createCountryCode(840);
        $this->assertInstanceOf(CountryCode::class, $countryCode);
    }

    #[Depends('testCountryCodesCreateCountryCodeIsPublicStatic')]
    #[Depends('testCountryCodesCreateCountryCodeHasOneRequiredParameter')]
    #[TestDox('CountryCodes::createCountryCode() accepts country number as numeric string')]
    public function testCountryCodesCreateCountryCodeAcceptsCountryNumberAsNumericString(): void
    {
        $countryCode = CountryCodes::createCountryCode('840');
        $this->assertInstanceOf(CountryCode::class, $countryCode);
    }

    #[Depends('testCountryCodesCreateCountryCodeIsPublicStatic')]
    #[Depends('testCountryCodesCreateCountryCodeHasOneRequiredParameter')]
    #[TestDox('CountryCodes::createCountryCode() returns null if country does not exist')]
    public function testCountryCodesCreateCountryCodeReturnsNullIfCountryDoesNotExist(): void
    {
        $this->assertNull(CountryCodes::createCountryCode('OO'));
        $this->assertNull(CountryCodes::createCountryCode(0));
        $this->assertNull(CountryCodes::createCountryCode('000'));
    }


    #[TestDox('CountryCodes::getCountryNumber() exists')]
    public function testCountryCodesGetCountryNumberExists(): void
    {
        $class = new ReflectionClass(CountryCodes::class);
        $this->assertTrue($class->hasMethod('getCountryNumber'));
    }

    #[TestDox('CountryCodes::getCountryNumber() is public static')]
    public function testCountryCodesGetCountryNumberIsPublicStatic(): void
    {
        $method = new ReflectionMethod(CountryCodes::class, 'getCountryNumber');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('CountryCodes::getCountryNumber() has one REQUIRED parameter')]
    public function testCountryCodesGetCountryNumberHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CountryCodes::class, 'getCountryNumber');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCountryCodesGetCountryNumberIsPublicStatic')]
    #[Depends('testCountryCodesGetCountryNumberHasOneRequiredParameter')]
    #[TestDox('CountryCodes::getCountryNumber() maps Country object to integer')]
    public function testCountryCodesGetCountryNumberMapsCountryObjectToInteger(): void
    {
        $country = new Country('DE', 'Deutschland');
        $this->assertIsInt(CountryCodes::getCountryNumber($country));
        $this->assertEquals(276, CountryCodes::getCountryNumber($country));

        $country = new Country('NL', 'Netherlands', 'Nederland');
        $this->assertIsInt(CountryCodes::getCountryNumber($country));
        $this->assertEquals(528, CountryCodes::getCountryNumber($country));
    }
}
