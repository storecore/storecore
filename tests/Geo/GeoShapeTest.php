<?php

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\Types\Intangible;
use StoreCore\Types\StructuredValue;
use StoreCore\Types\Thing;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(GeoShape::class)]
final class GeoShapeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('GeoShape class exists')]
    public function testGeoShapeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'GeoShape.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'GeoShape.php');
        $this->assertTrue(class_exists('\\StoreCore\\Geo\\GeoShape'));
        $this->assertTrue(class_exists(GeoShape::class));
    }

    #[TestDox('GeoShape class is concrete')]
    public function testGeoShapeClassIsConcrete(): void
    {
        $class = new ReflectionClass(GeoShape::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('GeoShape is a StructuredValue')]
    public function testGeoShapeIsStructuredValue(): void
    {
        $geoShape = new GeoShape();
        $this->assertInstanceOf(StructuredValue::class, $geoShape);
    }

    #[Group('hmvc')]
    #[TestDox('GeoShape is an Intangible Thing')]
    public function testGeoShapeIsIntangibleThing(): void
    {
        $geoShape = new GeoShape();
        $this->assertInstanceOf(Intangible::class, $geoShape);
        $this->assertInstanceOf(Thing::class, $geoShape);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(GeoShape::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(GeoShape::VERSION);
        $this->assertIsString(GeoShape::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(GeoShape::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('GeoShape.address exists')]
    public function testGeoShapeAddressExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('address', $geoShape);
    }

    #[TestDox('GeoShape.address is public')]
    public function testGeoShapeAddressIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'address');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.address is null by default')]
    public function testGeoShapeAddressIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->address);
    }


    #[TestDox('GeoShape.addressCountry exists')]
    public function testGeoShapeAddressCountryExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('addressCountry', $geoShape);
    }

    #[TestDox('GeoShape.addressCountry is public')]
    public function testGeoShapeAddressCountryIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'addressCountry');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.addressCountry is null by default')]
    public function testGeoShapeAddressCountryIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->addressCountry);
    }


    #[TestDox('GeoShape.box exists')]
    public function testGeoShapeBoxExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('box', $geoShape);
    }

    #[TestDox('GeoShape.box is public')]
    public function testGeoShapeBoxIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'box');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.box is null by default')]
    public function testGeoShapeBoxIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->box);
    }


    #[TestDox('GeoShape.circle exists')]
    public function testGeoShapeCircleExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('circle', $geoShape);
    }

    #[TestDox('GeoShape.circle is public')]
    public function testGeoShapeCircleIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'circle');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.circle is null by default')]
    public function testGeoShapeCircleIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->circle);
    }


    #[TestDox('GeoShape.elevation exists')]
    public function testGeoShapeElevationExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('elevation', $geoShape);
    }

    #[TestDox('GeoShape.elevation is public')]
    public function testGeoShapeElevationIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'elevation');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.elevation is null by default')]
    public function testGeoShapeElevationIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->elevation);
    }


    #[TestDox('GeoShape.line exists')]
    public function testGeoShapeLineExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('line', $geoShape);
    }

    #[TestDox('GeoShape.line is public')]
    public function testGeoShapeLineIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'line');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.line is null by default')]
    public function testGeoShapeLineIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->line);
    }


    #[TestDox('GeoShape.polygon exists')]
    public function testGeoShapePolygonExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('polygon', $geoShape);
    }

    #[TestDox('GeoShape.polygon is public')]
    public function testGeoShapePolygonIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'polygon');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.polygon is null by default')]
    public function testGeoShapePolygonIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->polygon);
    }


    #[TestDox('GeoShape.postalCode exists')]
    public function testGeoShapePostalCodeExists(): void
    {
        $geoShape = new GeoShape();
        $this->assertObjectHasProperty('postalCode', $geoShape);
    }

    #[TestDox('GeoShape.postalCode is public')]
    public function testGeoShapePostalCodeIsPublic(): void
    {
        $property = new ReflectionProperty(GeoShape::class, 'postalCode');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('GeoShape.postalCode is null by default')]
    public function testGeoShapePostalCodeIsNullByDefault(): void
    {
        $geoShape = new GeoShape();
        $this->assertNull($geoShape->postalCode);
    }
}
