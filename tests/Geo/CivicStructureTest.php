<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use StoreCore\Types\Thing;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\CivicStructure::class)]
#[UsesClass(\StoreCore\Geo\Place::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class CivicStructureTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('CivicStructure class exists')]
    public function testCivicStructureClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'CivicStructure.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'CivicStructure.php');
        $this->assertTrue(class_exists('\\StoreCore\\Geo\\CivicStructure'));
        $this->assertTrue(class_exists(CivicStructure::class));
    }

    #[Group('hmvc')]
    #[TestDox('CivicStructure class is concrete')]
    public function testCivicStructureClassIsConcrete(): void
    {
        $class = new ReflectionClass(CivicStructure::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CivicStructure is a Place')]
    public function testCivicStructureIsPlace(): void
    {
        $place = new CivicStructure();
        $this->assertInstanceOf(Place::class, $place);
    }

    #[Group('hmvc')]
    #[TestDox('CivicStructure is a Thing')]
    public function testCivicStructureIsThing(): void
    {
        $place = new CivicStructure();
        $this->assertInstanceOf(Thing::class, $place);
    }

    #[Group('seo')]
    #[TestDox('CivicStructure is JSON serializable')]
    public function testCivicStructureIsJsonSerializable(): void
    {
        $place = new CivicStructure();
        $this->assertInstanceOf(\JsonSerializable::class, $place);
    }


    /**
     * These subtypes of a `CivicStructure` are included because they
     * are a geographic `Place` that often has some kind of store,
     * for example a `Museum` shop or a `Hospital` bookstore.
     */
    #[Group('distro')]
    #[TestDox('Supported CivicStructure subtypes')]
    public function testSupportedCivicStructureSubtypes(): void
    {
        $places = [
            'Airport',
            'BusStation',
            'Hospital',
            'Museum',
            'SubwayStation',
            'TrainStation',
            'Zoo',
        ];

        foreach($places as $place) {
            $this->assertTrue(
                class_exists('\\StoreCore\\Geo\\' . $place),
                'Class StoreCore\\Geo\\' . $place . ' does not exist.'
            );

            $this->assertInstanceOf(
                CivicStructure::class,
                new ('\\StoreCore\\Geo\\' . $place),
                'Class StoreCore\\Geo\\' . $place . ' MUST be an instance of StoreCore\\Geo\\CivicStructure.'
            );
        }
    }


    #[Group('hmvc')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CivicStructure::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CivicStructure::VERSION);
        $this->assertIsString(CivicStructure::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CivicStructure::VERSION, '0.1.0', '>=')
        );
    }
}
