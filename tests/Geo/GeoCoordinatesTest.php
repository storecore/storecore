<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Geo\GeoCoordinates::class)]
final class GeoCoordinatesTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('GeoCoordinates class exists')]
    public function testGeoCoordinatesClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'GeoCoordinates.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'GeoCoordinates.php');

        $this->assertTrue(class_exists('\\StoreCore\\Geo\\GeoCoordinates'));
        $this->assertTrue(class_exists(GeoCoordinates::class));
    }

    #[Group('hmvc')]
    #[TestDox('GeoCoordinates class is concrete')]
    public function testGeoCoordinatesClassIsConcrete(): void
    {
        $class = new ReflectionClass(GeoCoordinates::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('GeoCoordinates is JSON serializable')]
    public function testGeoCoordinatesIsJsonSerializable(): void
    {
        $class = new ReflectionClass(GeoCoordinates::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(GeoCoordinates::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(GeoCoordinates::VERSION);
        $this->assertIsString(GeoCoordinates::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue( 
            version_compare(GeoCoordinates::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('GeoCoordinates::__construct() exists')]
    public function testGeoCoordinatesConstructorExists(): void
    {
        $class = new ReflectionClass(GeoCoordinates::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('GeoCoordinates::__construct() is public constructor')]
    public function testGeoCoordinatesConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(GeoCoordinates::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('GeoCoordinates::__construct() has three parameters')]
    public function testGeoCoordinatesConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(GeoCoordinates::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[TestDox('GeoCoordinates::__construct() has two REQUIRED parameters')]
    public function testGeoCoordinatesConstructorHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(GeoCoordinates::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('GeoCoordinates::__construct() sets latitude and longitude properties')]
    public function testGeoCoordinatesConstructorSetsLatitudeAndLongitudeProperties(): void
    {
        $geo = new GeoCoordinates(40.75, 73.98);

        $this->assertObjectHasProperty('latitude', $geo);
        $this->assertIsFloat($geo->latitude);
        $this->assertSame(40.75, $geo->latitude);

        $this->assertObjectHasProperty('longitude', $geo);
        $this->assertIsFloat($geo->longitude);
        $this->assertSame(73.98, $geo->longitude);
    }


    #[TestDox('GeoCoordinates::jsonSerialize() exists')]
    public function testGeoCoordinatesJsonSerializeExists(): void
    {
        $class = new ReflectionClass(GeoCoordinates::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[TestDox('GeoCoordinates::jsonSerialize() is public')]
    public function testGeoCoordinatesJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(GeoCoordinates::class, 'jsonSerialize');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('GeoCoordinates::jsonSerialize() has no parameters')]
    public function testGeoCoordinatesJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(GeoCoordinates::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('GeoCoordinates::jsonSerialize() returns non-empty array')]
    public function testGeoCoordinatesJsonSerializeReturnsNonEmptyArray(): void
    {
        $geo = new GeoCoordinates(40.75, 73.98);
        $this->assertIsArray($geo->jsonSerialize());
        $this->assertNotEmpty($geo->jsonSerialize());
    }

    #[TestDox('GeoCoordinates::jsonSerialize() array includes Schema.org `@type` set to `GeoCoordinates`')]
    public function testGeoCoordinatesJsonSerializeArrayIncludesSchemaOrgTypeSetToGeoCoordinates(): void
    {
        $geo = new GeoCoordinates(40.75, 73.98);
        $this->assertArrayHasKey('@type', $geo->jsonSerialize());
        $this->assertSame('GeoCoordinates', $geo->jsonSerialize()['@type']);
    }

    #[TestDox('GeoCoordinates::jsonSerialize() returns `latitude` and `longitude`')]
    public function testGeoCoordinatesJsonSerializeReturnsLatitudeAndLongitude(): void
    {
        $geo = new GeoCoordinates(47.2649990, 11.3428720);

        $this->assertArrayHasKey('latitude', $geo->jsonSerialize());
        $this->assertSame(47.2649990, $geo->jsonSerialize()['latitude']);

        $this->assertArrayHasKey('longitude', $geo->jsonSerialize());
        $this->assertSame(11.3428720, $geo->jsonSerialize()['longitude']);
    }


    #[TestDox('GeoCoordinates.postalCode exists')]
    public function testGeoCoordinatesPostalCodeExists(): void
    {
        $geo = new GeoCoordinates(47.2649990, 11.3428720);
        $this->assertObjectHasProperty('postalCode', $geo);
    }

    #[TestDox('GeoCoordinates.postalCode is null by default')]
    public function testGeoCoordinatesPostalCodeIsNullByDefault(): void
    {
        $geo = new GeoCoordinates(47.2649990, 11.3428720);
        $this->assertNull($geo->postalCode);
    }

    #[TestDox('GeoCoordinates.postalCode MAY be set in constructor')]
    public function testGeoCoordinatesPostalCodeMayBeSetInConstructor(): void
    {
        $geo = new GeoCoordinates(52.3377838, 4.8694234, '1082 MD');
        $this->assertNotNull($geo->postalCode);
        $this->assertNotEmpty($geo->postalCode);

        $expectedJson = '
            {
              "@type": "GeoCoordinates",
              "latitude": 52.3377838,
              "longitude": 4.8694234,
              "postalCode": "1082 MD"
            }
        ';
        $actualJson = json_encode($geo, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[TestDox('GeoCoordinates::jsonSerialize() array includes postalCode as string')]
    public function testGeoCoordinatesJsonSerializeArrayIncludesPostalCodeAsString(): void
    {
        $geo = new GeoCoordinates(47.2649990, 11.3428720);
        $geo->postalCode = 6020;
        $this->assertArrayHasKey('postalCode', $geo->jsonSerialize());
        $this->assertIsString($geo->jsonSerialize()['postalCode']);
        $this->assertSame('6020', $geo->jsonSerialize()['postalCode']);

        $expectedJson = '
            {
              "@type": "GeoCoordinates",
              "latitude": 47.2649990,
              "longitude": 11.3428720,
              "postalCode": "6020"
            }
        ';
        $actualJson = json_encode($geo, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
