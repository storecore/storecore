<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Geo;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Geo\CountryRepository::class)]
#[CoversClass(\StoreCore\Geo\Country::class)]
#[CoversClass(\StoreCore\Types\CountryCode::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
final class CountryRepositoryTest extends TestCase
{
    #[TestDox('CountryRepository class is concrete')]
    public function testCountryRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(CountryRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CountryRepository is a database model')]
    public function testCountryRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CountryRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('CountryRepository implements PSR-11 ContainerInterface')]
    public function testCountryRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(CountryRepository::class);
        $this->assertTrue($class->isSubclassOf(\Psr\Container\ContainerInterface::class));
    }


    #[TestDox('CountryRepository::find() exists')]
    public function testCountryRepositoryFindExists(): void
    {
        $class = new ReflectionClass(CountryRepository::class);
        $this->assertTrue($class->hasMethod('find'));
    }

    #[TestDox('CountryRepository::find() is public')]
    public function testCountryRepositoryFindIsPublic(): void
    {
        $method = new ReflectionMethod(CountryRepository::class, 'find');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('CountryRepository::find() has one REQUIRED parameter')]
    public function testCountryRepositoryFindHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CountryRepository::class, 'find');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CountryRepository::find() returns Country object')]
    public function testCountryRepositoryFindReturnsCountryObject(): void
    {    
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new CountryRepository($registry);
        $country = $repository->find('Belgium');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);
        $this->assertEquals('Belgium', $country->name);
    }

    #[Depends('testCountryRepositoryFindReturnsCountryObject')]
    #[TestDox('CountryRepository::find() supports English country names')]
    public function testCountryRepositoryFindSupportsEnglishCountryNames(): void
    {
        $registry = Registry::getInstance();
        $repository = new CountryRepository($registry);

        $country = $repository->find('Belgium');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);

        $country = $repository->find('Germany');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('DE', $country->identifier);

        $country = $repository->find('Netherlands');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('NL', $country->identifier);
    }

    #[Depends('testCountryRepositoryFindReturnsCountryObject')]
    #[Depends('testCountryRepositoryFindSupportsEnglishCountryNames')]
    #[TestDox('CountryRepository::find() supports country names in other languages')]
    public function testCountryRepositoryFindSupportsCountryNamesInOtherLanguages(): void
    {    
        $registry = Registry::getInstance();
        $repository = new CountryRepository($registry);

        // French (Français)
        $country = $repository->find('Belgique');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);

        // German (Deutsch)
        $country = $repository->find('Deutschland');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('DE', $country->identifier);

        // Dutch (Nederlands)
        $country = $repository->find('Nederland');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('NL', $country->identifier);
    }

    #[Depends('testCountryRepositoryFindSupportsCountryNamesInOtherLanguages')]
    #[TestDox('CountryRepository::find() supports partial country names')]
    public function testCountryRepositoryFindSupportsPartialCountryNames(): void
    {
        $registry = Registry::getInstance();
        $repository = new CountryRepository($registry);

        // `België` entered as `belgie`
        $country = $repository->find('belgie');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);
        $this->assertEquals('Belgium', $country->name);
        $this->assertEquals('België', $country->alternateName);

        // `Lëtzebuerg` entered as `letzebuerg`
        $country = $repository->find('letzebuerg');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('LU', $country->identifier);
        $this->assertEquals('Luxembourg', $country->name);
        $this->assertEquals('Lëtzebuerg', $country->alternateName);

        $country = $repository->find('belg');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);
        $this->assertEquals('Belgium', $country->name);
        $this->assertNotEquals('Belgium', $country->alternateName);

        $country = $repository->find('deutsch');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('DE', $country->identifier);
        $this->assertEquals('Germany', $country->name);
        $this->assertEquals('Deutschland', $country->alternateName);
    }

    #[TestDox('CountryRepository::find() supports ISO country codes')]
    public function testCountryRepositoryFindSupportsIsoCountryCodes(): void
    {    
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new CountryRepository($registry);

        $country = $repository->find('BE');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);
        $this->assertEquals('Belgium', $country->name);

        $country = $repository->find('BEL');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BE', $country->identifier);
        $this->assertEquals('Belgium', $country->name);
    }

    /**
     * @see https://en.wikipedia.org/wiki/List_of_fictional_countries
     *      List of fictional countries
     */
    #[Depends('testCountryRepositoryImplementsPSR11ContainerInterface')]
    #[TestDox('CountryRepository::find() throws PSR-11 NotFoundExceptionInterface if country name does not exist')]
    public function testCountryRepositoryFindThrowsPsr11NotFoundExceptionInterfaceIfCountryNameDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $repository = new CountryRepository($registry);
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $country = $repository->find('Absurdistan');
    }
}
