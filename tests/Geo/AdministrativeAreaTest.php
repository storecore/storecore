<?php


declare(strict_types=1);

namespace StoreCore\Geo;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Geo\AdministrativeArea::class)]
class AdministrativeAreaTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AdministrativeArea class exists')]
    public function testAdministrativeAreaClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'AdministrativeArea.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Geo' . DIRECTORY_SEPARATOR . 'AdministrativeArea.php');
    }

    #[Group('hmvc')]
    #[TestDox('AdministrativeArea class is concrete')]
    public function testAdministrativeAreaClassIsConcrete(): void
    {
        $class = new ReflectionClass(AdministrativeArea::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AdministrativeArea is a Place')]
    public function testAdministrativeAreaIsPlace(): void
    {
        $area = new AdministrativeArea();
        $this->assertInstanceOf(\StoreCore\Geo\Place::class, $area);
    }

    #[Group('hmvc')]
    #[TestDox('AdministrativeArea is JSON serializable')]
    public function testAdministrativeAreaIsJsonSerializable(): void
    {
        $area = new AdministrativeArea();
        $this->assertInstanceOf(\JsonSerializable::class, $area);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AdministrativeArea::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AdministrativeArea::VERSION);
        $this->assertIsString(AdministrativeArea::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AdministrativeArea::VERSION, '1.0.0', '>=')
        );
    }
}
