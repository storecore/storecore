<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class PricingContextInterfaceTest extends TestCase
{
    #[TestDox('PricingContextInterface interface exists')]
    public function testPricingContextInterfaceInterfaceExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PricingContextInterface.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PricingContextInterface.php');

        $this->assertTrue(interface_exists('\\StoreCore\\PricingContextInterface'));
        $this->assertTrue(interface_exists(PricingContextInterface::class));
    }


    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PricingContextInterface::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PricingContextInterface::VERSION);
        $this->assertIsString(PricingContextInterface::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PricingContextInterface::VERSION, '1.0.0', '>=')
        );
    }
}
