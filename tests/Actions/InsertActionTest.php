<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\InsertAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\UpdateAction::class)]
#[UsesClass(\StoreCore\Actions\AddAction::class)]
final class InsertActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('InsertAction class is concrete')]
    public function testInsertActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(InsertAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('InsertAction is an Action')]
    public function testInsertActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new InsertAction());
    }

    #[Depends('testInsertActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('InsertAction is an UpdateAction')]
    public function testInsertActionIsUpdateAction(): void
    {
        $this->assertInstanceOf(UpdateAction::class, new InsertAction());
    }

    #[Depends('testInsertActionIsUpdateAction')]
    #[Group('hmvc')]
    #[TestDox('InsertAction is an AddAction')]
    public function testInsertActionIsAddAction(): void
    {
        $this->assertInstanceOf(
            AddAction::class,
            new InsertAction(),
            'InsertAction class hierarchy is Thing > Action > UpdateAction > AddAction > InsertAction.'
        );
    }

    /**
     * @see https://schema.org/InsertAction#subtypes
     *      More specific Schema.org types of `InsertAction`
     */
    #[Group('hmvc')]
    #[TestDox('InsertAction subtypes')]
    public function testInsertActionSubtypes(): void
    {
        $this->assertTrue(class_exists(AppendAction::class));
        $this->assertInstanceOf(InsertAction::class, new AppendAction());

        $this->assertTrue(class_exists(PrependAction::class));
        $this->assertInstanceOf(InsertAction::class, new PrependAction());
    }


    #[Group('hmvc')]
    #[TestDox('InsertAction is JSON serializable')]
    public function testInsertActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new InsertAction());
    }

    #[Group('hmvc')]
    #[TestDox('InsertAction is stringable')]
    public function testInsertActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new InsertAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(InsertAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(InsertAction::VERSION);
        $this->assertIsString(InsertAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(InsertAction::VERSION, '0.1.0', '>=')
        );
    }
}
