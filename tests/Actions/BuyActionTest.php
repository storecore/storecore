<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Book;
use StoreCore\CRM\{Organization, Person};

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\BuyAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\TradeAction::class)]
#[UsesClass(\StoreCore\CMS\Book::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class BuyActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('BuyAction class is concrete')]
    public function testBuyActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(BuyAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('BuyAction is an Action')]
    public function testBuyActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new BuyAction());
    }

    #[Depends('testBuyActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('BuyAction is a TradeAction')]
    public function testBuyActionIsTradeAction(): void
    {
        $this->assertInstanceOf(TradeAction::class, new BuyAction());
    }


    #[Group('hmvc')]
    #[TestDox('BuyAction is JSON serializable')]
    public function testBuyActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new BuyAction());
    }

    #[Group('hmvc')]
    #[TestDox('BuyAction is stringable')]
    public function testBuyActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new BuyAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BuyAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BuyAction::VERSION);
        $this->assertIsString(BuyAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BuyAction::VERSION, '0.1.0', '>=')
        );
    }


    /**
     * @see https://schema.org/BuyAction#eg-0141
     *      Schema.org `BuyAction` Example 1
     */
    #[TestDox('John bought a book on Amazon')]
    public function testJohnBoughtABookOnAmazon(): void
    {
        $action = new BuyAction();
        $action->agent = new Person('John');
        $action->object = new Book('Outliers');
        $action->seller = new Organization('Amazon');

        $this->assertEquals('John', (string) $action->agent->name);
        $this->assertEquals('Outliers', $action->object->name);
        $this->assertEquals('Amazon', $action->seller->name);

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "BuyAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "object": {
         *     "@type": "Book",
         *     "name": "Outliers"
         *   },
         *   "seller": {
         *     "@type": "Organization",
         *     "name": "Amazon"
         *   }
         * }
         * ```
         */
        $json = (string) $action;
        $this->assertNotEmpty($json);
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"BuyAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"object":{"@type":"Book","name":"Outliers"', $json);
        $this->assertStringContainsString('"seller":{"@type":"Organization","name":"Amazon"}', $json);
    }
}
