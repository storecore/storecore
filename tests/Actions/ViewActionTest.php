<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\EmailMessage;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ViewAction::class)]
#[CoversClass(\StoreCore\CMS\EmailMessage::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class ViewActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ViewAction class is concrete')]
    public function testViewActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(ViewAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ViewAction is an Action')]
    public function testViewActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ViewAction());
    }

    #[Depends('testViewActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('ViewAction is a ConsumeAction')]
    public function testViewActionIsConsumeAction(): void
    {
        $this->assertInstanceOf(ConsumeAction::class, new ViewAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ViewAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ViewAction::VERSION);
        $this->assertIsString(ViewAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ViewAction::VERSION, '0.1.0', '>=')
        );
    }


    /**
     * @see https://developers.google.com/gmail/markup/actions/declaring-actions#go-to_actions
     *      Go-To Actions - Declare Actions - Email Markup for Gmail
     *
     * JavaScript in HTML (example by Google):
     * 
     *     ```html
     *     <script type="application/ld+json">
     *     {
     *       "@context": "http://schema.org",
     *       "@type": "EmailMessage",
     *       "potentialAction": {
     *         "@type": "ViewAction",
     *         "target": "https://watch-movies.com/watch?movieId=abc123",
     *         "name": "Watch movie"
     *       },
     *       "description": "Watch the 'Avengers' movie online"
     *     }
     *     </script>
     *     ```
     *
     * In Schema.org JSON the `target` node is expanded to an `entryPoint`
     * with an `urlTemplate`:
     *
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "EmailMessage",
     *       "description": "Watch the 'Avengers' movie online",
     *       "potentialAction": {
     *         "@type": "ViewAction",
     *         "name": "Watch movie",
     *         "target": {
     *           "@type": "EntryPoint",
     *           "urlTemplate": "https://watch-movies.com/watch?movieId=abc123"
     *         }
     *       }
     *     }
     *     ```
     */
    #[TestDox('Go-To Action in Gmail')]
    public function testGoToActionInGmail(): void
    {
        $email = new EmailMessage();
        $email->description = "Watch the 'Avengers' movie online";
        $email->potentialAction = new ViewAction(
            [
                'name' => 'Watch movie',
                'target' => 'https://watch-movies.com/watch?movieId=abc123',
            ]
        );

        $this->assertIsObject($email->potentialAction);

        $this->assertIsObject($email->potentialAction->target);
        $this->assertInstanceOf(\StoreCore\Types\EntryPoint::class, $email->potentialAction->target);

        $json = json_encode($email, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"potentialAction":', $json);
        $this->assertStringContainsString('"@type": "ViewAction"', $json);
        $this->assertStringContainsString('"urlTemplate": "https://watch-movies.com/watch?movieId=abc123"', $json);
    }

    /**
     * @see https://developers.google.com/gmail/markup/actions/declaring-actions#mobile_deep_linking
     *
     *     ```html
     *     <script type="application/ld+json">
     *     {
     *       "@context": "http://schema.org",
     *       "@type": "EmailMessage",
     *       "name": "Watch movie",
     *       … information about the movie …
     *       "potentialAction": {
     *         "@type": "ViewAction",
     *         "target": [
     *           "https://watch-movies.com/watch?movieId=abc123",
     *           "android-app://com.watchmovies.app/http/watch-movies.com/watch?movieId=abc123",
     *           "ios-app://12345/movieapp/watch-movies.com/watch?movieId=abc123"
     *         ]
     *       }
     *     }
     *     </script>
     *     ```
     */
    #[TestDox('Mobile deep linking requires multiple targets')]
    public function testMobileDeepLinkingRequiresMultipleTargets(): void
    {
        $email = new EmailMessage('Watch movie');
        $email->potentialAction = new ViewAction();
        $email->potentialAction->target = array(
            'https://watch-movies.com/watch?movieId=abc123',
            'android-app://com.watchmovies.app/http/watch-movies.com/watch?movieId=abc123',
            'ios-app://12345/movieapp/watch-movies.com/watch?movieId=abc123',
        );

        $this->assertNotNull($email->potentialAction);
        $this->assertIsObject($email->potentialAction);

        $this->assertNotNull($email->potentialAction->target);
        $this->assertNotEmpty($email->potentialAction->target);
        $this->assertIsArray($email->potentialAction->target);
        $this->assertCount(3, $email->potentialAction->target);
    }
}
