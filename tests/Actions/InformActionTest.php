<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\Actions\InformAction::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
final class InformActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('InformAction class is concrete')]
    public function testInformActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(InformAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('InformAction is CommunicateAction')]
    public function testInformActionIsCommunicateAction(): void
    {
        $this->assertInstanceOf(Action::class, new InformAction());
        $this->assertInstanceOf(InteractAction::class, new InformAction());
        $this->assertInstanceOf(
            CommunicateAction::class,
            new InformAction(),
            'InformAction class hierarchy is Thing > Action > InteractAction > CommunicateAction > InformAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('InformAction has two subtypes')]
    public function testInformActionhasTwoSubtypes(): void
    {
        $this->assertTrue(class_exists(ConfirmAction::class));
        $this->assertInstanceOf(InformAction::class, new ConfirmAction());

        $this->assertTrue(class_exists(RsvpAction::class));
        $this->assertInstanceOf(InformAction::class, new RsvpAction());
    }


    #[Group('hmvc')]
    #[TestDox('InformAction is JSON serializable')]
    public function testInformActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new InformAction());
    }

    #[Group('hmvc')]
    #[TestDox('InformAction is stringable')]
    public function testInformActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new InformAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(InformAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(InformAction::VERSION);
        $this->assertIsString(InformAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(InformAction::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('InformAction.event exists')]
    public function testInformActionEventExists(): void
    {
        $informAction = new InformAction();
        $this->assertObjectHasProperty('event', $informAction);
    }

    #[Depends('testInformActionEventExists')]
    #[TestDox('InformAction.event is public')]
    public function testInformActionEventIsPublic(): void
    {
        $property = new ReflectionProperty(InformAction::class, 'event');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testInformActionEventExists')]
    #[TestDox('InformAction.event is null by default')]
    public function testInformActionEventIsNullByDefault(): void
    {
        $action = new InformAction();
        $this->assertNull($action->event);
    }
}
