<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\WatchAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class WatchActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('WatchAction class is concrete')]
    public function testWatchActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(WatchAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('WatchAction is an Action')]
    public function testWatchActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new WatchAction());
    }

    #[Depends('testWatchActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('WatchAction is a ConsumeAction')]
    public function testWatchActionIsConsumeAction(): void
    {
        $this->assertInstanceOf(ConsumeAction::class, new WatchAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WatchAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WatchAction::VERSION);
        $this->assertIsString(WatchAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WatchAction::VERSION, '1.0.0', '>=')
        );
    }
}
