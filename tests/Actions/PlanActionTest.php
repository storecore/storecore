<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;
use StoreCore\CRM\Person;
use StoreCore\Types\Event;
use StoreCore\Types\Thing;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\PlanAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class PlanActionTest extends TestCase
{
    #[TestDox('PlanAction class is concrete')]
    public function testPlanActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(PlanAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PlanAction is an OrganizeAction')]
    public function testPlanActionIsOrganizeAction(): void
    {
        $action = new PlanAction();
        $this->assertInstanceOf(PlanAction::class, $action);
        $this->assertInstanceOf(OrganizeAction::class, $action);
        $this->assertInstanceOf(Action::class, $action);
        $this->assertInstanceOf(Thing::class, $action);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new \ReflectionClass(PlanAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PlanAction::VERSION);
        $this->assertIsString(PlanAction::VERSION);
    }

     #[Depends('testVersionConstantIsNonEmptyString')]
     #[Group('distro')]
     #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PlanAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/scheduledTime
     *      Schema.org `scheduledTime` property of a `PlanAction`
     */
    #[TestDox('PlanAction.scheduledTime exists')]
    public function testPlanActionScheduledTimeExists(): void
    {
        $planAction = new PlanAction();
        $this->assertObjectHasProperty('scheduledTime', $planAction);
    }

    #[Depends('testPlanActionScheduledTimeExists')]
    #[TestDox('PlanAction.scheduledTime is null by default')]
    public function testPlanActionScheduledTimeIsNullByDefault(): void
    {
        $planAction = new PlanAction();
        $this->assertNull($planAction->scheduledTime);
    }


    /**
     * @see https://schema.org/PlanAction#eg-0129
     *      Schema.org `PlanAction` example 1
     */
    #[TestDox('John planned a trip with a travel agency')]
    public function testJohnPlannedATripWithATravelAgency(): void
    {
        $action = new PlanAction();
        $action->description = 'John planned a trip with a travel agency.';

        $action->agent = new Person();
        $action->agent->givenName = 'John';

        $action->object = new Event();
        $action->object->name = 'John’s trip';

        $action->instrument = new Organization();
        $action->instrument->type = OrganizationType::TravelAgency;
        $action->instrument->name = 'AFS';

        $json = (string) $action;

        // John …
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        // … planned …
        $this->assertStringContainsString('"@context":"https://schema.org","@type":"PlanAction"', $json);
        // … a trip …
        $this->assertStringContainsString('"object":{"@type":"Event","name":"John’s trip"}', $json);
        // … with a travel agency.
        $this->assertStringContainsString('"instrument":{"@type":"TravelAgency","name":"AFS"}', $json);
    }
}
