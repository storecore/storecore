<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\CheckInAction::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
final class CheckInActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CheckInAction class is concrete')]
    public function testCheckInActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CheckInAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CheckInAction is a CommunicateAction')]
    public function testCheckInActionIsCommunicateAction(): void
    {
        $this->assertInstanceOf(Action::class, new CheckInAction());
        $this->assertInstanceOf(InteractAction::class, new CheckInAction());
        $this->assertInstanceOf(
            CommunicateAction::class,
            new CheckInAction(),
            'CheckInAction class hierarchy is Thing > Action > InteractAction > CommunicateAction > CheckInAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('CheckInAction is JSON serializable')]
    public function testCheckInActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new CheckInAction());
    }

    #[Group('hmvc')]
    #[TestDox('CheckInAction is stringable')]
    public function testCheckInActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CheckInAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CheckInAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CheckInAction::VERSION);
        $this->assertIsString(CheckInAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CheckInAction::VERSION, '0.1.0', '>=')
        );
    }
}
