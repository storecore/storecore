<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Person;
use StoreCore\Types\ItemList;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\UpdateAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\ItemList::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class UpdateActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('UpdateAction class is concrete')]
    public function testUpdateActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(UpdateAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('UpdateAction is an Action')]
    public function testUpdateActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class,new UpdateAction());
    }

    #[Group('hmvc')]
    #[TestDox('UpdateAction is JSON serializable')]
    public function testUpdateActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new UpdateAction());
    }

    #[Group('hmvc')]
    #[TestDox('UpdateAction is stringable')]
    public function testUpdateActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new UpdateAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UpdateAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UpdateAction::VERSION);
        $this->assertIsString(UpdateAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UpdateAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/UpdateAction#eg-0158 Example 1
     *
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "UpdateAction",
     *   "agent": {
     *     "@type": "Person",
     *     "name": "John"
     *   },
     *   "targetCollection": {
     *     "@type": "ItemList",
     *     "name": "List of my favorite movies",
     *     "url": "http://netflix.com/john/favorite"
     *   }
     * }
     * ```
     */
    #[TestDox('John updated his movie collection')]
    public function testJohnUpdatedHisMovieCollection(): void
    {
        $customer = new Person('John');
        $favorites = new ItemList('List of my favorite movies');
        $favorites->url = 'http://netflix.com/john/favorite';

        $action = new UpdateAction();
        $action->agent = $customer;
        $this->assertSame('John', (string) $action->agent->name);

        $this->assertNull($action->targetCollection);
        $action->targetCollection = $favorites;
        $this->assertNotNull($action->targetCollection);

        $json = (string) $action;
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"targetCollection":{"@type":"ItemList","name":"List of my favorite movies","url":"http://netflix.com/john/favorite"}', $json);
    }
}
