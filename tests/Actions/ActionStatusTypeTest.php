<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ActionStatusType::class)]
final class ActionStatusTypeTest extends TestCase
{
    #[TestDox('ActionStatusType enumeration exists')]
    public function testActionStatusTypeEnumerationExists(): void
    {
        $this->assertTrue(enum_exists('\\StoreCore\\Actions\\ActionStatusType'));
        $this->assertTrue(enum_exists(ActionStatusType::class));
    }

    #[Depends('testActionStatusTypeEnumerationExists')]
    #[TestDox('ActionStatusType enumeration exists')]
    public function testActionStatusTypeIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(ActionStatusType::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $enum = new ReflectionEnum(ActionStatusType::class);
        $this->assertTrue($enum->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ActionStatusType::VERSION);
        $this->assertIsString(ActionStatusType::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org version')]
    public function testVersionMatchesSchemaOrgVersion(): void
    {
        $this->assertTrue(
            version_compare(ActionStatusType::VERSION, '23.0', '>=')
        );
    }


    #[Depends('testActionStatusTypeIsBackedEnumeration')]
    #[TestDox('ActionStatusType enumeration has four cases')]
    public function testActionStatusTypeEnumerationHasFourCases(): void
    {
        $enum = new ReflectionEnum(ActionStatusType::class);
        $this->assertCount(4, $enum->getCases());
    }
}
