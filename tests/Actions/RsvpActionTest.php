<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\Actions\InformAction::class)]
#[UsesClass(\StoreCore\Actions\RsvpAction::class)]
final class RsvpActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('RsvpAction class is concrete')]
    public function testRsvpActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(RsvpAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('RsvpAction is an InformAction')]
    public function testRsvpActionIsformAction(): void
    {
        $confirmAction = new RsvpAction();
        $this->assertInstanceOf(Action::class, $confirmAction);
        $this->assertInstanceOf(InteractAction::class, $confirmAction);
        $this->assertInstanceOf(CommunicateAction::class, $confirmAction);
        $this->assertInstanceOf(
            InformAction::class,
            $confirmAction,
            'RsvpAction class hierarchy is Thing > Action > InteractAction > CommunicateAction > InformAction > RsvpAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('RsvpAction is JSON serializable')]
    public function testRsvpActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new RsvpAction());
    }

    #[Group('hmvc')]
    #[TestDox('RsvpAction is stringable')]
    public function testRsvpActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new RsvpAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RsvpAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RsvpAction::VERSION);
        $this->assertIsString(RsvpAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RsvpAction::VERSION, '1.0.0', '>=')
        );
    }
}
