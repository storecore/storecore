<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\Types\Thing;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\AchieveAction::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class AchieveActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AchieveAction class is concrete')]
    public function testAchieveActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(AchieveAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AchieveAction is an Action Thing')]
    public function testAchieveActionIsActionThing(): void
    {
        $action = new AchieveAction();
        $this->assertInstanceOf(Action::class, $action);
        $this->assertInstanceOf(Thing::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('AchieveAction subtypes')]
    public function testAchieveActionSubtypes(): void
    {
        $this->assertInstanceOf(AchieveAction::class, new LoseAction());
        $this->assertInstanceOf(AchieveAction::class, new TieAction());
        $this->assertInstanceOf(AchieveAction::class, new WinAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AchieveAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AchieveAction::VERSION);
        $this->assertIsString(AchieveAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(AchieveAction::VERSION, '1.0.0', '>='));
    }
}
