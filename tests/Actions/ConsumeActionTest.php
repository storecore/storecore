<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ConsumeAction::class)]
final class ConsumeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ConsumeAction class is concrete')]
    public function testConsumeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ConsumeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ConsumeAction is an Action')]
    public function testConsumeActionIsAction(): void
    {
        $action = new ConsumeAction();
        $this->assertInstanceOf(Action::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('ConsumeAction subtypes')]
    public function testConsumeActionSubtypes(): void
    {
        $this->assertTrue(class_exists(DrinkAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new DrinkAction());

        $this->assertTrue(class_exists(EatAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new EatAction());

        $this->assertTrue(class_exists(InstallAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new InstallAction());

        $this->assertTrue(class_exists(ListenAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new ListenAction());

        $this->assertTrue(class_exists(PlayGameAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new PlayGameAction());

        $this->assertTrue(class_exists(ReadAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new ReadAction());

        // Thing > Action > ConsumeAction > UseAction
        $this->assertTrue(class_exists(UseAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new UseAction());

        // Thing > Action > ConsumeAction > UseAction > WearAction
        $this->assertTrue(class_exists(WearAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new WearAction());
        $this->assertInstanceOf(UseAction::class, new WearAction());

        $this->assertTrue(class_exists(ViewAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new ViewAction());

        $this->assertTrue(class_exists(WatchAction::class));
        $this->assertInstanceOf(ConsumeAction::class, new WatchAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ConsumeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ConsumeAction::VERSION);
        $this->assertIsString(ConsumeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ConsumeAction::VERSION, '0.2.0', '>=')
        );
    }


    /**
     * @see https://schema.org/actionAccessibilityRequirement
     *      Schema.org `actionAccessibilityRequirement` of a `ConsumeAction`
     */
    #[TestDox('ConsumeAction.actionAccessibilityRequirement exists')]
    public function testConsumeActionActionAccessibilityRequirementExists(): void
    {
        $consumeAction = new ConsumeAction();
        $this->assertObjectHasProperty('actionAccessibilityRequirement', $consumeAction);
    }

    #[Depends('testConsumeActionActionAccessibilityRequirementExists')]
    #[TestDox('ConsumeAction.actionAccessibilityRequirement is public')]
    public function testConsumeActionActionAccessibilityRequirementIsPublic(): void
    {
        $property = new ReflectionProperty(ConsumeAction::class, 'actionAccessibilityRequirement');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testConsumeActionActionAccessibilityRequirementExists')]
    #[TestDox('ConsumeAction.actionAccessibilityRequirement is null by default')]
    public function testConsumeActionActionAccessibilityRequirementIsNullByDefault(): void
    {
        $consumeAction = new ConsumeAction();
        $this->assertNull($consumeAction->actionAccessibilityRequirement);
    }


    /**
     * @see https://schema.org/expectsAcceptanceOf
     */
    #[TestDox('ConsumeAction.expectsAcceptanceOf exists')]
    public function testConsumeActionExpectsAcceptanceOfExists(): void
    {
        $this->assertObjectHasProperty('expectsAcceptanceOf', new ConsumeAction());
    }

    #[Depends('testConsumeActionExpectsAcceptanceOfExists')]
    #[TestDox('ConsumeAction.expectsAcceptanceOf is public')]
    public function testConsumeActionExpectsAcceptanceOfIsPublic(): void
    {
        $property = new ReflectionProperty(ConsumeAction::class, 'expectsAcceptanceOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testConsumeActionExpectsAcceptanceOfExists')]
    #[TestDox('ConsumeAction.expectsAcceptanceOf is null by default')]
    public function testConsumeActionExpectsAcceptanceOfIsNullByDefault(): void
    {
        $consumeAction = new ConsumeAction();
        $this->assertNull($consumeAction->expectsAcceptanceOf);
    }
}
