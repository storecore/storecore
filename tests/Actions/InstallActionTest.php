<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ConsumeAction::class)]
#[UsesClass(\StoreCore\Actions\InstallAction::class)]
final class InstallActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('InstallAction class is concrete')]
    public function testInstallActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(InstallAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('InstallAction is a ConsumeAction')]
    public function testInstallActionIsConsumeAction(): void
    {
        $this->assertInstanceOf(Action::class, new InstallAction());
        $this->assertInstanceOf(ConsumeAction::class, new InstallAction());
    }

    #[Group('hmvc')]
    #[TestDox('InstallAction is JSON serializable')]
    public function testInstallActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new InstallAction());
    }

    #[Group('hmvc')]
    #[TestDox('InstallAction is stringable')]
    public function testInstallActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new InstallAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(InstallAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(InstallAction::VERSION);
        $this->assertIsString(InstallAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(InstallAction::VERSION, '1.0.0', '>=')
        );
    }
}
