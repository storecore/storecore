<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\WinAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AchieveAction::class)]
final class WinActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('WinAction class is concrete')]
    public function testWinActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(WinAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('WinAction is an Action')]
    public function testWinActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new WinAction());
    }

    #[Depends('testWinActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('WinAction is an AchieveAction')]
    public function testWinActionIsAchieveAction(): void
    {
        $this->assertInstanceOf(AchieveAction::class, new WinAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WinAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WinAction::VERSION);
        $this->assertIsString(WinAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WinAction::VERSION, '1.0.0', '>=')
        );
    }
}
