<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\BackupGlobals;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\UnRegisterAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class UnRegisterActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('UnRegisterAction class is concrete')]
    public function testUnRegisterActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(UnRegisterAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('UnRegisterAction is an Action')]
    public function testUnRegisterActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new UnRegisterAction());
    }

    #[Group('testUnRegisterActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('UnRegisterAction is an InteractAction')]
    public function testUnRegisterActionIsInteractAction(): void
    {
        $this->assertInstanceOf(InteractAction::class, new UnRegisterAction());
    }


    #[Group('hmvc')]
    #[TestDox('UnRegisterAction is JSON serializable')]
    public function testUnRegisterActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new UnRegisterAction());
    }

    #[Group('hmvc')]
    #[TestDox('UnRegisterAction is stringable')]
    public function testUnRegisterActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new UnRegisterAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UnRegisterAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UnRegisterAction::VERSION);
        $this->assertIsString(UnRegisterAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UnRegisterAction::VERSION, '0.1.0', '>=')
        );
    }
}
