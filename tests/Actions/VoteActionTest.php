<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\VoteAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ChooseAction::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class VoteActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('VoteAction class is concrete')]
    public function testVoteActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(VoteAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('VoteAction is an Action')]
    public function testVoteActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new VoteAction());
    }

    #[Depends('testVoteActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('VoteAction is an AssessAction')]
    public function testVoteActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new VoteAction());
    }

    #[Depends('testVoteActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('VoteAction is a ChooseAction')]
    public function testVoteActionIsChooseAction(): void
    {
        $this->assertInstanceOf(ChooseAction::class, new VoteAction());
    }


    #[Group('hmvc')]
    #[TestDox('VoteAction is JSON serializable')]
    public function testVoteActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new VoteAction());
    }

    #[Group('hmvc')]
    #[TestDox('VoteAction is stringable')]
    public function testVoteActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new VoteAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(VoteAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(VoteAction::VERSION);
        $this->assertIsString(VoteAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(VoteAction::VERSION, '0.1.1', '>=')
        );
    }


    /**
     * @see https://schema.org/candidate
     *      Schema.org `candidate` property of a `VoteAction`
     */
    #[TestDox('VoteAction.candidate exists')]
    public function testVoteActionCandidateExists(): void
    {
        $this->assertObjectHasProperty('candidate', new VoteAction());
    }

    #[Depends('testVoteActionCandidateExists')]
    #[TestDox('VoteAction.candidate is public')]
    public function testVoteActionCandidateIsPublic(): void
    {
        $property = new ReflectionProperty(VoteAction::class, 'candidate');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testVoteActionCandidateExists')]
    #[Depends('testVoteActionCandidateIsPublic')]
    #[TestDox('VoteAction.candidate is null by default')]
    public function testVoteActionCandidateIsNullByDefault(): void
    {
        $vote = new VoteAction();
        $this->assertNull($vote->candidate);
    }


    /**
     * @see https://schema.org/VoteAction#eg-0041 Schema.org Example 1
     */
    #[TestDox('John voted on Steve')]
    public function testJohnVotedOnSteve(): void
    {
        $vote = new VoteAction();
        $vote->agent = new Person('John');
        $vote->candidate = new Person('Steve');

        $this->assertNotEmpty($vote->candidate);
        $this->assertIsObject($vote->candidate);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $vote->candidate);

        $this->assertIsObject($vote->candidate->name);
        $this->assertInstanceOf(\Stringable::class, $vote->candidate->name);
        $this->assertSame('Steve', (string) $vote->candidate->name);

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "VoteAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "candidate": {
         *     "@type": "Person",
         *     "name": "Steve"
         *   }
         * }
         * ```
         */
        $json = (string) $vote;
        $this->assertNotEmpty($json);
        $this->assertStringContainsString('"@type":"VoteAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"candidate":{"@type":"Person","name":"Steve"}', $json);
    }
}
