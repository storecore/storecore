<?php declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\FindAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class FindActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('FindAction class is concrete')]
    public function testFindActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(FindAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('FindAction is an Action')]
    public function testFindActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new FindAction());
    }

    #[Group('hmvc')]
    #[TestDox('FindAction subtypes')]
    public function testFindActionSubtypes(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Actions\\CheckAction'));
        $this->assertInstanceOf(FindAction::class, new CheckAction());

        $this->assertTrue(class_exists('\\StoreCore\\Actions\\DiscoverAction'));
        $this->assertInstanceOf(FindAction::class, new DiscoverAction());

        $this->assertTrue(class_exists('\\StoreCore\\Actions\\TrackAction'));
        $this->assertInstanceOf(FindAction::class, new TrackAction());
    }

    #[Group('hmvc')]
    #[TestDox('FindAction is JSON serializable')]
    public function testFindActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new FindAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FindAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FindAction::VERSION);
        $this->assertIsString(FindAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FindAction::VERSION, '1.0.0', '>=')
        );
    }
}
