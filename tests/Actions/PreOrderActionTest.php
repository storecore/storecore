<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\PreOrderAction::class)]
#[UsesClass(\StoreCore\Actions\TradeAction::class)]
final class PreOrderActionTest extends TestCase
{
    #[TestDox('PreOrderAction class is concrete')]
    public function testPreOrderActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(PreOrderAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('PreOrderAction is a TradeAction')]
    public function testPreOrderActionIsTradeAction(): void
    {
        $this->assertInstanceOf(Action::class, new PreOrderAction());

        $this->assertInstanceOf(
            TradeAction::class,
            new PreOrderAction(),
            'PreOrderAction class hierarchy is Thing > Action > TradeAction > PreOrderAction.'
        );
    }

    #[Depends('testPreOrderActionIsTradeAction')]
    #[Group('hmvc')]
    #[TestDox('PreOrderAction is NOT a type of OrderAction')]
    public function testPreOrderActionIsNotATypeOfOrderAction(): void
    {
        $this->assertNotInstanceOf(
            OrderAction::class,
            new PreOrderAction(),
            'In Schema.org `PreOrderAction` is NOT a subtype of an `OrderAction`.'
        );
    }


    #[Group('hmvc')]
    #[TestDox('PreOrderAction is JSON serializable')]
    public function testPreOrderActionIsJsonSerializable(): void
    {
        $action = new PreOrderAction();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('PreOrderAction is stringable')]
    public function testPreOrderActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new PreOrderAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PreOrderAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PreOrderAction::VERSION);
        $this->assertIsString(PreOrderAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PreOrderAction::VERSION, '1.0.0-rc.1', '>=')
        );
    }
}
