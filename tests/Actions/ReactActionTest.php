<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ReactAction::class)]
final class ReactActionTest extends TestCase
{
    #[TestDox('ReactAction class is concrete')]
    public function testReactActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ReactAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ReactAction is an AssessAction')]
    public function testReactActionIsAssessAction(): void
    {
        $this->assertInstanceOf(Action::class, new ReactAction());

        $this->assertInstanceOf(
            AssessAction::class,
            new ReactAction(),
            'ReactAction class hierarchy is Thing > Action > AssessAction > ReactAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('ReactAction subtypes')]
    public function testReactActionSubtypes(): void
    {
        $this->assertTrue(class_exists(AgreeAction::class));
        $this->assertInstanceOf(ReactAction::class, new AgreeAction());

        $this->assertTrue(class_exists(DisagreeAction::class));
        $this->assertInstanceOf(ReactAction::class, new DisagreeAction());

        $this->assertTrue(class_exists(DislikeAction::class));
        $this->assertInstanceOf(ReactAction::class, new DislikeAction());

        $this->assertTrue(class_exists(EndorseAction::class));
        $this->assertInstanceOf(ReactAction::class, new EndorseAction());

        $this->assertTrue(class_exists(LikeAction::class));
        $this->assertInstanceOf(ReactAction::class, new LikeAction());

        $this->assertTrue(class_exists(WantAction::class));
        $this->assertInstanceOf(ReactAction::class, new WantAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ReactAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ReactAction::VERSION);
        $this->assertIsString(ReactAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ReactAction::VERSION, '1.0.0', '>=')
        );
    }
}
