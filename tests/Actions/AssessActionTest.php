<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class AssessActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AssessAction class is concrete')]
    public function testAssessActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(AssessAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AssessAction class is concrete')]
    public function testAssessActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new AssessAction());
    }

    #[Group('hmvc')]
    #[TestDox('AssessAction subtypes')]
    public function testAssessActionSubtypes(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Actions\\ChooseAction'));
        $this->assertInstanceOf(AssessAction::class, new ChooseAction());

        $this->assertTrue(class_exists('\\StoreCore\\Actions\\IgnoreAction'));
        $this->assertInstanceOf(AssessAction::class, new IgnoreAction());

        $this->assertTrue(class_exists('\\StoreCore\\Actions\\ReactAction'));
        $this->assertInstanceOf(AssessAction::class, new ReactAction());

        $this->assertTrue(class_exists('\\StoreCore\\Actions\\ReviewAction'));
        $this->assertInstanceOf(AssessAction::class, new ReviewAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AssessAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AssessAction::VERSION);
        $this->assertIsString(AssessAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AssessAction::VERSION, '1.0.0', '>=')
        );
    }
}
