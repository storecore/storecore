<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\AgreeAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ReactAction::class)]
final class AgreeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AgreeAction class is concrete')]
    public function testAgreeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(AgreeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('AgreeAction is an Action')]
    public function testAgreeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new AgreeAction());
    }

    #[Depends('testAgreeActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('AgreeAction is an AssessAction')]
    public function testAgreeActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new AgreeAction());
    }

    #[Depends('testAgreeActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('AgreeAction is a ReactAction')]
    public function testAgreeActionIsAReactAction()
    {
        $this->assertInstanceOf(ReactAction::class, new AgreeAction());
    }


    #[Group('hmvc')]
    #[TestDox('AgreeAction is JSON serializable')]
    public function testAgreeActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new AgreeAction());
    }

    #[Group('hmvc')]
    #[TestDox('AgreeAction is stringable')]
    public function testAgreeActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new AgreeAction());
    }


    
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AgreeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AgreeAction::VERSION);
        $this->assertIsString(AgreeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AgreeAction::VERSION, '0.2.0', '>=')
        );
    }
}
