<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
#[UsesClass(\StoreCore\Actions\RegisterAction::class)]
final class RegisterActionTest extends TestCase
{
    #[TestDox('RegisterAction class is concrete')]
    public function testRegisterActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(RegisterAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('RegisterAction is a InteractAction')]
    public function testRegisterActionIsInteractAction(): void
    {
        $this->assertInstanceOf(Action::class, new RegisterAction());

        $this->assertInstanceOf(
            InteractAction::class,
            new RegisterAction(),
            'RegisterAction class hierarchy is Thing > Action > InteractAction > RegisterAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('RegisterAction is JSON serializable')]
    public function testRegisterActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new RegisterAction());
    }

    #[Group('hmvc')]
    #[TestDox('RegisterAction is stringable')]
    public function testRegisterActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new RegisterAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RegisterAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RegisterAction::VERSION);
        $this->assertIsString(RegisterAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RegisterAction::VERSION, '0.1.0', '>=')
        );
    }
}
