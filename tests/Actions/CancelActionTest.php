<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\CancelAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\OrganizeAction::class)]
#[UsesClass(\StoreCore\Actions\PlanAction::class)]
#[UsesClass(\StoreCore\Actions\CancelAction::class)]
final class CancelActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CancelAction class is concrete')]
    public function testCancelActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CancelAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CancelAction is PlanAction')]
    public function testCancelActionIsPlanAction(): void
    {
        $this->assertInstanceOf(Action::class, new CancelAction());
        $this->assertInstanceOf(OrganizeAction::class, new CancelAction());
        $this->assertInstanceOf(
            PlanAction::class,
            new CancelAction(),
            'CancelAction class hierarchy is Thing > Action > OrganizeAction > PlanAction > CancelAction.'
        );
        
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CancelAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CancelAction::VERSION);
        $this->assertIsString(CancelAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CancelAction::VERSION, '1.0.0', '>=')
        );
    }
}
