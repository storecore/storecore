<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\MedicalAudience;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\CRM\MedicalAudience::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
final class CommunicateActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CommunicateAction class is concrete')]
    public function testCommunicateActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CommunicateAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('CommunicateAction is an Action')]
    public function testCommunicateActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new CommunicateAction());
    }

    #[Depends('testCommunicateActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('CommunicateAction is an InteractAction')]
    public function testCommunicateActionIsAnInteractAction()
    {
        $this->assertInstanceOf(InteractAction::class, new CommunicateAction());
    }

    #[Group('hmvc')]
    #[TestDox('CommunicateAction subtypes')]
    public function testCommunicateActionSubtypes(): void
    {
        $this->assertTrue(class_exists(AskAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new AskAction());

        $this->assertTrue(class_exists(CheckInAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new CheckInAction());

        $this->assertTrue(class_exists(CheckOutAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new CheckOutAction());

        $this->assertTrue(class_exists(CommentAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new CommentAction());

        $this->assertTrue(class_exists(InformAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new InformAction());

        $this->assertTrue(class_exists(InviteAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new InviteAction());

        $this->assertTrue(class_exists(ReplyAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new ReplyAction());

        $this->assertTrue(class_exists(ShareAction::class));
        $this->assertInstanceOf(CommunicateAction::class, new ShareAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CommunicateAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CommunicateAction::VERSION);
        $this->assertIsString(CommunicateAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CommunicateAction::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('CommunicateAction.recipient exists')]
    public function testCommunicateActionRecipientExists(): void
    {
        $this->assertObjectHasProperty('recipient', new CommunicateAction());
    }

    #[Depends('testCommunicateActionRecipientExists')]
    #[TestDox('CommunicateAction.recipient is public')]
    public function testCommunicateActionRecipientIsPublic(): void
    {
        $property = new ReflectionProperty(CommunicateAction::class, 'recipient');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testCommunicateActionRecipientExists')]
    #[TestDox('CommunicateAction.recipient is null by default')]
    public function testCommunicateActionRecipientIsNullByDefault(): void
    {
        $communicateAction = new CommunicateAction();
        $this->assertNull($communicateAction->recipient);
    }

    #[Depends('testCommunicateActionRecipientExists')]
    #[Depends('testCommunicateActionRecipientIsNullByDefault')]
    #[TestDox('CommunicateAction.recipient accepts Organization')]
    public function testCommunicateActionRecipientAcceptsOrganization(): void
    {
        $communicateAction = new CommunicateAction();
        $communicateAction->recipient = new Organization('Acme Corporation');

        $this->assertNotNull($communicateAction->recipient);
        $this->assertIsObject($communicateAction->recipient);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $communicateAction->recipient);
        $this->assertEquals('Acme Corporation', $communicateAction->recipient->name);
    }

    #[Depends('testCommunicateActionRecipientExists')]
    #[Depends('testCommunicateActionRecipientIsNullByDefault')]
    #[TestDox('CommunicateAction.recipient accepts Person')]
    public function testCommunicateActionRecipientAcceptsPerson(): void
    {
        $communicateAction = new CommunicateAction();
        $communicateAction->recipient = new Person('Elmer J. Fudd');

        $this->assertNotNull($communicateAction->recipient);
        $this->assertIsObject($communicateAction->recipient);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $communicateAction->recipient);
        $this->assertSame('Elmer J. Fudd', (string) $communicateAction->recipient->name);
    }


    /**
     * @see https://schema.org/CommunicateAction#eg-0088 Example 1
     *
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "CommunicateAction",
     *   "agent": {
     *     "@type": "Person",
     *     "name": "John"
     *    },
     *    "recipient": {
     *      "@type": "Person",
     *      "name": "Steve"
     *    }
     * }
     * ```
     */
    #[TestDox('John communicated with Steve')]
    public function testJohnCommunicatedWithSteve(): void
    {
        $communicateAction = new CommunicateAction();
        $communicateAction->agent = new Person('John');
        $communicateAction->recipient = new Person('Steve');

        $this->assertEquals('John',  (string) $communicateAction->agent->name);
        $this->assertEquals('Steve', (string) $communicateAction->recipient->name);

        $json = (string) $communicateAction;
        $this->assertJson($json);
        $this->assertStringContainsString('"@type":"CommunicateAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"recipient":{"@type":"Person","name":"Steve"}', $json);
    }

    /**
     * @see https://schema.org/CommunicateAction#eg-0089 Example 2
     *
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "CommunicateAction",
     *   "agent": {
     *     "@type": "Person",
     *     "name": "John"
     *   },
     *   "recipient": {
     *     "@type": "MedicalAudience",
     *     "name": "Brain surgeons"
     *   }
     * }
     * ```
     */
    #[TestDox('John communicated to the medical community his retirement')]
    public function testJohnCommunicatedToTheMedicalCommunityHisRetirement()
    {
        $communicateAction = new CommunicateAction();

        $communicateAction->agent = new Person('John');
        $this->assertIsObject($communicateAction->agent);
        $this->assertIsObject($communicateAction->agent->name);
        $this->assertEquals('John', (string) $communicateAction->agent->name);

        $communicateAction->recipient = new MedicalAudience('Brain surgeons');
        $this->assertIsObject($communicateAction->recipient);
        $this->assertEquals('Brain surgeons', $communicateAction->recipient->name);
    }
}
