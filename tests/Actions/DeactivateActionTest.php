<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ControlAction::class)]
#[UsesClass(\StoreCore\Actions\DeactivateAction::class)]
final class DeactivateActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DeactivateAction class is concrete')]
    public function testDeactivateActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DeactivateAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('DeactivateAction is an Action')]
    public function testDeactivateActionIsAction(): void
    {
        $action = new DeactivateAction();
        $this->assertInstanceOf(Action::class, $action);
    }

    #[Depends('testDeactivateActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('DeactivateAction is an Action')]
    public function testDeactivateActionIsControlAction(): void
    {
        $action = new DeactivateAction();
        $this->assertInstanceOf(ControlAction::class, $action);
    }


    #[Group('hmvc')]
    #[TestDox('DeactivateAction is JSON serializable')]
    public function testDeactivateActionIsJsonSerializable(): void
    {
        $action = new DeactivateAction();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('DeactivateAction is stringable')]
    public function testDeactivateActionIsStringable(): void
    {
        $action = new DeactivateAction();
        $this->assertInstanceOf(\Stringable::class, $action);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DeactivateAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DeactivateAction::VERSION);
        $this->assertIsString(DeactivateAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DeactivateAction::VERSION, '1.0.0', '>=')
        );
    }
}
