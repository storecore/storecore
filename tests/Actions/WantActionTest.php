<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Person;
use StoreCore\PIM\Product;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Actions\WantActionTest::class)]
#[CoversClass(\StoreCore\Actions\Action::class)]
#[CoversClass(\StoreCore\Actions\AssessAction::class)]
#[CoversClass(\StoreCore\Actions\ReactAction::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\PIM\AbstractProduct::class)]
#[UsesClass(\StoreCore\PIM\AbstractProductOrService::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
final class WantActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('WantAction class is concrete')]
    public function testWantActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(WantAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    // Thing > Action > AssessAction > ReactAction > WantAction

    #[Group('hmvc')]
    #[TestDox('WantAction is an Action')]
    public function testWantActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new WantAction());
    }

    #[Depends('testWantActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('WantAction is an AssessAction')]
    public function testWantActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new WantAction());
    }

    #[Depends('testWantActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('WantAction is a ReactAction')]
    public function testWantActionIsReactAction(): void
    {
        $this->assertInstanceOf(
            ReactAction::class,
            new WantAction(),
            'WantAction class hierarchy is: Thing > Action > AssessAction > ReactAction > WantAction'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WantAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WantAction::VERSION);
        $this->assertIsString(WantAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WantAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/WantAction#eg-0051
     *      Schema.org `WantAction` Example 1
     */
    #[TestDox('John and Steve want a new iPod')]
    public function testJohnAndSteveWantANewIpod(): void
    {
        $product = new Product();
        $product->name = 'iPod';

        $action = new WantAction();
        $action->agent = new Person('John');
        $action->participant = new Person('Steve');
        $action->object = $product;

        $this->assertNotSame($action->agent, $action->participant);

        $this->assertSame('iPod', $action->object->name);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "WantAction",
              "agent": {
                "@type": "Person",
                "name": "John"
              },
              "object": {
                "@type": "Product",
                "itemCondition": "https://schema.org/NewCondition",
                "name": "iPod"
              },
              "participant": {
                "@type": "Person",
                "name": "Steve"
              }
            }
        ';
        $actualJson = json_encode($action, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        $json = (string) $action;
        $this->assertNotEmpty($json);
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"WantAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"participant":{"@type":"Person","name":"Steve"}', $json);
        $this->assertStringContainsString('"object":{"@type":"Product","itemCondition":"https://schema.org/NewCondition","name":"iPod"}', $json);
    }
}
