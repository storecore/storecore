<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\Actions\InformAction::class)]
#[UsesClass(\StoreCore\Actions\ConfirmAction::class)]
final class ConfirmActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ConfirmAction class is concrete')]
    public function testConfirmActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ConfirmAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ConfirmAction is an InformAction')]
    public function testConfirmActionIsformAction(): void
    {
        $confirmAction = new ConfirmAction();
        $this->assertInstanceOf(Action::class, $confirmAction);
        $this->assertInstanceOf(InteractAction::class, $confirmAction);
        $this->assertInstanceOf(CommunicateAction::class, $confirmAction);
        $this->assertInstanceOf(
            InformAction::class,
            $confirmAction,
            'ConfirmAction class hierarchy is Thing > Action > InteractAction > CommunicateAction > InformAction > ConfirmAction.'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ConfirmAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ConfirmAction::VERSION);
        $this->assertIsString(ConfirmAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ConfirmAction::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://developers.google.com/gmail/markup/actions/declaring-actions#in-app_actions
     *      In-App Actions - Declare Actions - Email Markup for Gmail - Google Developers
     */
    #[TestDox('ConfirmAction.handler SHALL NOT be set to HttpActionHandler')]
    public function testConfirmActionHandlerShallNotBeSetToHttpActionHandler(): void
    {
        // `ConfirmAction` does not have a `handler` property in Schema.org.
        $this->assertObjectNotHasProperty(
            'handler',
            new ConfirmAction(),
            'ConfirmAction SHOULD NOT have `handler` property.'
        );

        // `HttpActionHandler` is not a valid subtype of `Action` in Schema.org.
        $confirmAction = new ConfirmAction();
        $this->expectException(
            \Error::class,
            'ConfirmAction.handler property and HttpActionHandler object type do not exist in Schema.org.'
        );
        $confirmAction->handler = new \StoreCore\Types\HttpActionHandler();
    }
}
