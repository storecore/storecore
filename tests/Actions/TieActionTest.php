<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\AchieveAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\TieAction::class)]
final class TieActionTest extends TestCase
{
    #[TestDox('TieAction class is concrete')]
    public function testTieActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(TieAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TieAction is an AchieveAction')]
    public function testTieActionIsAchieveAction(): void
    {
        $this->assertInstanceOf(Action::class, new TieAction());

        $this->assertInstanceOf(
            AchieveAction::class,
            new TieAction(),
            'TieAction class hierarcy Thing > Action > AchieveAction > TieAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('TieAction is NOT a WinAction and NOT a LoseAction')]
    public function testTieActionIsNotAWinActionAndNotALoseAction(): void
    {
        $tie = new TieAction();
        $this->assertNotInstanceOf(WinAction::class, $tie);
        $this->assertNotInstanceOf(LoseAction::class, $tie);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TieAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TieAction::VERSION);
        $this->assertIsString(TieAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TieAction::VERSION, '1.0.0', '>=')
        );
    }
}
