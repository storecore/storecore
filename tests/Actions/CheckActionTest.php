<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[CoversClass(\StoreCore\Actions\CheckAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\CheckAction::class)]
final class CheckActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CheckAction class is concrete')]
    public function testCheckActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CheckAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CheckAction is a FindAction')]
    public function testCheckActionIsFindAction(): void
    {
        $this->assertInstanceOf(Action::class, new CheckAction());
        $this->assertInstanceOf(
            FindAction::class,
            new CheckAction(),
            'CheckAction class hierarchy is Thing > Action > FindAction > CheckAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('CheckAction is JSON serializable')]
    public function testCheckActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new CheckAction());
    }

    #[Group('hmvc')]
    #[TestDox('CheckAction is stringable')]
    public function testCheckActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CheckAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CheckAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CheckAction::VERSION);
        $this->assertIsString(CheckAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CheckAction::VERSION, '1.0.0', '>=')
        );
    }
}
