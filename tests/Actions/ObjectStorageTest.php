<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Actions; 

use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Actions\ObjectStorage::class)]
#[CoversClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class ObjectStorageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ObjectStorage is countable')]
    public function testObjectStorageIsCountable(): void
    {
        $class = new ReflectionClass(ObjectStorage::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }

    #[TestDox('ObjectStorage implements SingletonInterface')]
    public function testObjectStorageImplementsSingletonInterface(): void
    {
        $this->assertTrue(interface_exists('StoreCore\\SingletonInterface'));

        $class = new ReflectionClass(ObjectStorage::class);
        $this->assertTrue($class->hasMethod('getInstance'));
        $this->assertTrue($class->implementsInterface(\StoreCore\SingletonInterface::class));

        $method = new ReflectionMethod(ObjectStorage::class, 'getInstance');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());

        $this->expectException(\Error::class);
        $failure = new ObjectStorage();
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ObjectStorage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ObjectStorage::VERSION);
        $this->assertIsString(ObjectStorage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ObjectStorage::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ObjectStorage::has exists')]
    public function testObjectStorageHasExists(): void
    {
        $class = new ReflectionClass(ObjectStorage::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testObjectStorageHasExists')]
    #[TestDox('ObjectStorage::has is public')]
    public function testObjectStorageHasIsPublic(): void
    {
        $method = new ReflectionMethod(ObjectStorage::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testObjectStorageHasExists')]
    #[TestDox('ObjectStorage::has has one REQUIRED parameter')]
    public function testObjectStorageHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ObjectStorage::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testObjectStorageHasExists')]
    #[Depends('testObjectStorageHasIsPublic')]
    #[Depends('testObjectStorageHasHasOneRequiredParameter')]
    #[TestDox('ObjectStorage::has returns (bool) false on invalid UUID string')]
    public function testObjectStorageHasReturnsBoolFalseOnInvalidUuidString(): void
    {
        $storage = ObjectStorage::getInstance();
        $this->assertIsBool($storage->has('42'));
        $this->assertFalse($storage->has('42'));

        $this->expectException(\TypeError::class);
        $failure = $storage->has(42);
    }

    #[Depends('testObjectStorageIsCountable')]
    #[TestDox('ObjectStorage::has returns false on empty storage')]
    public function testObjectStorageHasReturnsFalseOnEmptyStorage(): void
    {
        $storage = ObjectStorage::getInstance();
        $this->assertCount(0, $storage);
        $this->assertEquals(0, $storage->count());
        $this->assertEquals(0, count($storage));

        // UUID v1
        $this->assertFalse($storage->has('826fa26a-aeae-11ed-afa1-0242ac120002'));

        // UUID v4
        $this->assertFalse($storage->has('ff16ffe7-8687-48bd-b0f9-ee2bf5346e84'));
    }

    /**
     * @see https://schema.org/SearchAction#eg-0138
     */
    #[Depends('testObjectStorageIsCountable')]
    #[TestDox('ObjectStorage::has returns bool')]
    public function testObjectStorageHasReturnsBool(): void
    {
        $action = new SearchAction();
        $action->agent = new Person('John');
        $action->query = 'What is the answer to life the universe and everything?';
        $action->setIdentifier('9d6a75a2-a79f-4d92-9c0e-a071d2915811');

        $storage = ObjectStorage::getInstance();
        $this->assertFalse($storage->has('9d6a75a2-a79f-4d92-9c0e-a071d2915811'));
        $this->assertCount(0, $storage);

        $storage->attach($action);
        $this->assertTrue($storage->has('9d6a75a2-a79f-4d92-9c0e-a071d2915811'));
        $this->assertCount(1, $storage);

        $storage->detach($action);
        $this->assertFalse($storage->has('9d6a75a2-a79f-4d92-9c0e-a071d2915811'));
        $this->assertCount(0, $storage);
    }
}
