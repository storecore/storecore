<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\ScholarlyArticle;
use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\IgnoreAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\CMS\ScholarlyArticle::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class IgnoreActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('IgnoreAction class is concrete')]
    public function testIgnoreActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(IgnoreAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('IgnoreAction is an Action')]
    public function testIgnoreActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new IgnoreAction());
    }

    #[Depends('testIgnoreActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('IgnoreAction is an AssessAction')]
    public function testIgnoreActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new IgnoreAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(IgnoreAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(IgnoreAction::VERSION);
        $this->assertIsString(IgnoreAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(IgnoreAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/IgnoreAction#eg-0042
     *      Schema.org `IgnoreAction` Example 1
     */
    #[TestDox('John ignored Steve')]
    public function testJohnIgnoredSteve(): void
    {
        $action = new IgnoreAction();
        $action->agent = new Person('John');
        $action->object = new Person('Steve');

        $this->assertNotEmpty($action->agent);
        $this->assertIsObject($action->agent);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $action->agent);
        $this->assertSame('John', (string) $action->agent->name);

        $this->assertNotEmpty($action->object);
        $this->assertIsObject($action->object);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $action->object);
        $this->assertSame('Steve', (string) $action->object->name);

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "IgnoreAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "object": {
         *     "@type": "Person",
         *     "name": "Steve"
         *   }
         * }
         * ```
         */
        $json = (string) $action;
        $this->assertJson($json);
        $this->assertStringContainsString('@type":"IgnoreAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"object":{"@type":"Person","name":"Steve"}', $json);
    }

    /**
     * @see https://schema.org/IgnoreAction#eg-0043
     *      Schema.org `IgnoreAction` Example 2
     */
    #[TestDox('John and Steve ignored an article')]
    public function testJohnAndSteveIgnoredAnArticle(): void
    {
        $action = new IgnoreAction();
        $action->agent = new Person('John');
        $action->participant = new Person('Steve');
        $action->object = new ScholarlyArticle('Do we really need to know whether P = NP?');

        /*
         * Note that “John and Steve” are not two agents,
         * but a single `agent` with a `participant`.
         */
        $this->assertIsObject($action->agent);
        $this->assertInStanceOf(\StoreCore\CRM\Person::class, $action->agent);
        $this->assertIsObject($action->participant);
        $this->assertInStanceOf(\StoreCore\CRM\Person::class, $action->participant);
        $this->assertNotSame($action->agent, $action->participant);

        $this->assertNotEmpty($action->object);
        $this->assertIsObject($action->object);
        $this->assertInstanceOf(
            \StoreCore\CMS\Article::class,
            $action->object,
            'Agents SHOULD be able to ignore things such as articles.'
        );
    }
}
