<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\DiscoverAction::class)]
#[UsesClass(\StoreCore\Actions\FindAction::class)]
final class DiscoverActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DiscoverAction class is concrete')]
    public function testDiscoverActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DiscoverAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('DiscoverAction is an Action')]
    public function testDiscoverActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new DiscoverAction());
    }

    #[Depends('testDiscoverActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('DiscoverAction is a FindAction')]
    public function testDiscoverActionIsFindAction(): void
    {
        $this->assertInstanceOf(FindAction::class, new DiscoverAction());
    }


    #[Group('hmvc')]
    #[TestDox('DiscoverAction is JSON serializable')]
    public function testDiscoverActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new DiscoverAction());
    }

    #[Group('hmvc')]
    #[TestDox('DiscoverAction is stringable')]
    public function testDiscoverActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new DiscoverAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DiscoverAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DiscoverAction::VERSION);
        $this->assertIsString(DiscoverAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DiscoverAction::VERSION, '1.0.0', '>=')
        );
    }
}
