<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\CheckOutAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
final class CheckOutActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CheckOutAction class is concrete')]
    public function testCheckOutActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CheckOutAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('CheckOutAction is an Action')]
    public function testCheckOutActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new CheckOutAction());
    }

    #[Depends('testCheckOutActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('CheckOutAction is an InteractAction')]
    public function testCheckOutActionIsInteractAction(): void
    {
        $this->assertInstanceOf(InteractAction::class, new CheckOutAction());
    }

    #[Depends('testCheckOutActionIsInteractAction')]
    #[Group('hmvc')]
    #[TestDox('CheckOutAction is a CommunicateAction')]
    public function testCheckOutActionIsCommunicateAction(): void
    {
        $this->assertInstanceOf(CommunicateAction::class, new CheckOutAction());
    }


    #[Group('hmvc')]
    #[TestDox('CheckOutAction is JSON serializable')]
    public function testCheckOutActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new CheckOutAction());
    }

    #[Group('hmvc')]
    #[TestDox('CheckOutAction is stringable')]
    public function testCheckOutActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CheckOutAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CheckOutAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CheckOutAction::VERSION);
        $this->assertIsString(CheckOutAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CheckOutAction::VERSION, '0.1.0', '>=')
        );
    }
}
