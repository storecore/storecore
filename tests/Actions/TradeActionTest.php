<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\TradeAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class TradeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('TradeAction class is concrete')]
    public function testTradeActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(TradeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TradeAction is an Action')]
    public function testTradeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new TradeAction());
    }

    /**
     * @see https://schema.org/TradeAction#subtypes
     */
    #[Group('hmvc')]
    #[TestDox('TradeAction subtypes')]
    public function testTradeActionSubtypes(): void
    {
        $this->assertTrue(class_exists(BuyAction::class));
        $this->assertInstanceOf(TradeAction::class, new BuyAction());

        $this->assertTrue(class_exists(DonateAction::class));
        $this->assertInstanceOf(TradeAction::class, new DonateAction());

        $this->assertTrue(class_exists(OrderAction::class));
        $this->assertInstanceOf(TradeAction::class, new OrderAction());

        $this->assertTrue(class_exists(PayAction::class));
        $this->assertInstanceOf(TradeAction::class, new PayAction());

        $this->assertTrue(class_exists(PreOrderAction::class));
        $this->assertInstanceOf(TradeAction::class, new PreOrderAction());

        $this->assertTrue(class_exists(QuoteAction::class));
        $this->assertInstanceOf(TradeAction::class, new QuoteAction());

        $this->assertTrue(class_exists(RentAction::class));
        $this->assertInstanceOf(TradeAction::class, new RentAction());

        $this->assertTrue(class_exists(SellAction::class));
        $this->assertInstanceOf(TradeAction::class, new SellAction());

        $this->assertTrue(class_exists(TipAction::class));
        $this->assertInstanceOf(TradeAction::class, new TipAction());
    }


    #[Group('hmvc')]
    #[TestDox('TradeAction is JSON serializable')]
    public function testTradeActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new TradeAction());
    }

    #[Group('hmvc')]
    #[TestDox('TradeAction is stringable')]
    public function testTradeActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new TradeAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TradeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TradeAction::VERSION);
        $this->assertIsString(TradeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TradeAction::VERSION, '0.1.0', '>=')
        );
    }
}
