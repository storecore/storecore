<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Actions; 

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Actions\Error::class)]
#[CoversClass(\StoreCore\Actions\ErrorThing::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class ErrorThingTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ErrorThing class is concrete')]
    public function testErrorThingClassIsConcrete(): void
    {
        $class = new ReflectionClass(ErrorThing::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ErrorThing is a Schema.org Thing')]
    public function testErrorThingIsSchemaOrgThing(): void
    {
        $class = new ReflectionClass(ErrorThing::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Types\Thing::class));
    }

    #[Group('hmvc')]
    #[TestDox('ErrorThing is JSON serializable')]
    public function testErrorThingIsJsonSerializable(): void
    {
        $class = new ReflectionClass(ErrorThing::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ErrorThing::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ErrorThing::VERSION);
        $this->assertIsString(ErrorThing::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(ErrorThing::VERSION, '0.1.0', '>='));
    }


    #[TestDox('ErrorThing::__construct exists')]
    public function testErrorThingConstructorExists(): void
    {
        $class = new ReflectionClass(ErrorThing::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testErrorThingConstructorExists')]
    #[TestDox('ErrorThing::__construct is public constructor')]
    public function testErrorThingConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(ErrorThing::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testErrorThingConstructorExists')]
    #[TestDox('ErrorThing::__construct has one REQUIRED parameter')]
    public function testErrorThingConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ErrorThing::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testErrorThingConstructorExists')]
    #[Depends('testErrorThingConstructorIsPublicConstructor')]
    #[Depends('testErrorThingConstructorHasOneRequiredParameter')]
    #[TestDox('ErrorThing::__construct accepts Error object')]
    public function testErrorThingConstructorAcceptsErrorObject(): void
    {
        $error = new Error();
        $error->status = 404;

        $thing = new ErrorThing($error);
        $this->assertNotEmpty($thing->name);
        $this->assertIsString($thing->name);
        $this->assertSame('404 Not Found', $thing->name);

        $expectedJson = '
            {
              "@type": "Thing",
              "name": "404 Not Found"
            }
        ';
        $actualJson = json_encode($thing, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
