<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\LikeAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class LikeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('LikeAction class is concrete')]
    public function testLikeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(LikeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LikeAction is an Action')]
    public function testLikeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new LikeAction());
    }

    #[Group('hmvc')]
    #[TestDox('LikeAction is a ReactAction')]
    public function testLikeActionIsReactAction(): void
    {
        $this->assertInstanceOf(ReactAction::class, new LikeAction());
    }

    #[Group('hmvc')]
    #[TestDox('LikeAction is an AssessAction')]
    public function testLikeActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new LikeAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LikeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LikeAction::VERSION);
        $this->assertIsString(LikeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LikeAction::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
