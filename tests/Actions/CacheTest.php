<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Actions;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Actions\Cache::class)]
#[CoversClass(\StoreCore\Actions\ObjectStorage::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class CacheTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');
            $db->query('SELECT 1 FROM `sc_action_status_types`');
            $db->query('SELECT 1 FROM `sc_actions`');
            $db->query('SELECT 1 FROM `sc_action_objects`');
        } catch (\Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    #[TestDox('Cache class is concrete')]
    public function testCacheClassIsConcrete(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Cache class is a database model')]
    public function testCacheClassIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[TestDox('Cache class implements PSR-20 ClockInterface')]
    public function testCacheClassImplementsPsr20ClockInterface(): void
    {
        $this->assertTrue(interface_exists('Psr\\Clock\\ClockInterface'));

        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Cache::VERSION);
        $this->assertIsString(Cache::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Cache::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Cache::__construct exists')]
    public function testCacheConstructExists(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testCacheConstructExists')]
    #[TestDox('Cache::__construct is public constructor')]
    public function testCacheConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Cache::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testCacheConstructExists')]
    #[TestDox('Cache::__construct has one REQUIRED parameter')]
    public function testCacheConstructHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Cache::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());

        $registry = Registry::getInstance();
        $cache = new Cache($registry);
    }


    #[TestDox('Cache::has exists')]
    public function testCacheHasExists(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testCacheHasExists')]
    #[TestDox('Cache::has is public')]
    public function testCacheHasIsPublic(): void
    {
        $method = new ReflectionMethod(Cache::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCacheHasExists')]
    #[TestDox('Cache::has has one REQUIRED parameter')]
    public function testCacheHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Cache::class, 'has');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());

        $registry = Registry::getInstance();
        $cache = new Cache($registry);
    }

    #[Depends('testCacheHasExists')]
    #[Depends('testCacheHasIsPublic')]
    #[Depends('testCacheHasHasOneRequiredParameter')]
    #[TestDox('Cache::has returns (bool) false on invalid UUID')]
    public function testCacheHasReturnsBoolFalseOnInvalidUuid(): void
    {
        $registry = Registry::getInstance();
        $cache = new Cache($registry);
        $this->assertIsBool($cache->has('6587fddb'));
        $this->assertFalse($cache->has('c0f182c1'));

        $this->expectException(\TypeError::class);
        $failure = $cache->has(666);
    }

    #[Depends('testCacheHasExists')]
    #[Depends('testCacheHasIsPublic')]
    #[Depends('testCacheHasHasOneRequiredParameter')]
    #[TestDox('Cache::has returns (bool) false if UUID does not exist')]
    public function testCacheHasReturnsBoolFalseIfUuidDoesNotExist(): void
    {
        $registry = Registry::getInstance();
        $cache = new Cache($registry);
        $this->assertIsBool($cache->has('639d8679-6374-4b09-a5c2-2d8f5d2c53c9'));
        $this->assertFalse($cache->has('9944a2cd-0664-42fe-9b8b-687f9d27ebc7'));
    }


    #[TestDox('Cache::set exists')]
    public function testCacheSetExists(): void
    {
        $class = new ReflectionClass(Cache::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testCacheSetExists')]
    #[TestDox('Cache::set is public')]
    public function testCacheSetIsPublic(): void
    {
        $method = new ReflectionMethod(Cache::class, 'set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCacheSetExists')]
    #[TestDox('Cache::set has three parameters')]
    public function testCacheSetHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Cache::class, 'set');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testCacheSetHasThreeParameters')]
    #[TestDox('Cache::set has two required parameters')]
    public function testCacheSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Cache::class, 'set');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }
}
