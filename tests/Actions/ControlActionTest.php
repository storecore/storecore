<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ControlAction::class)]
final class ControlActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ControlAction class is concrete')]
    public function testControlActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ControlAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ControlAction is an Action')]
    public function testControlActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ControlAction());
    }

    /**
     * @see https://schema.org/ControlAction#subtypes
     *      More specific types of `ControlAction`
     */
    #[Group('hmvc')]
    #[TestDox('ControlAction subtypes')]
    public function testControlActionSubtypes(): void
    {
        $this->assertTrue(class_exists(ActivateAction::class));
        $this->assertInstanceOf(ControlAction::class, new ActivateAction());

        $this->assertTrue(class_exists(DeactivateAction::class));
        $this->assertInstanceOf(ControlAction::class, new DeactivateAction());

        $this->assertTrue(class_exists(ResumeAction::class));
        $this->assertInstanceOf(ControlAction::class, new ResumeAction());

        $this->assertTrue(class_exists(SuspendAction::class));
        $this->assertInstanceOf(ControlAction::class, new SuspendAction());
    }


    #[Group('hmvc')]
    #[TestDox('ControlAction is JSON serializable')]
    public function testControlActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ControlAction());
    }

    #[Group('hmvc')]
    #[TestDox('ControlAction is stringable')]
    public function testControlActionIsStringable(): void
    {
        $action = new ControlAction();
        $this->assertInstanceOf(\Stringable::class, $action);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ControlAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ControlAction::VERSION);
        $this->assertIsString(ControlAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ControlAction::VERSION, '1.0.0', '>=')
        );
    }
}
