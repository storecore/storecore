<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\EndorseAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ReactAction::class)]
final class EndorseActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('EndorseAction class is concrete')]
    public function testEndorseActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(EndorseAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('EndorseAction is an Action')]
    public function testEndorseActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new EndorseAction());
    }

    #[Depends('testEndorseActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('EndorseAction is an AssessAction')]
    public function testEndorseActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new EndorseAction());
    }

    #[Depends('testEndorseActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('EndorseAction is a ReactAction')]
    public function testEndorseActionIsReactAction(): void
    {
        $this->assertInstanceOf(
            ReactAction::class,
            new EndorseAction(),
            'EndorseAction class hierachy is Thing > Action > AssessAction > ReactAction > EndorseAction.'
        );
    }


    #[Group('hmvc')]
    #[TestDox('EndorseAction is JSON serializable')]
    public function testEndorseActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new EndorseAction());
    }

    #[Group('hmvc')]
    #[TestDox('EndorseAction is stringable')]
    public function testEndorseActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new EndorseAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(EndorseAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EndorseAction::VERSION);
        $this->assertIsString(EndorseAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(EndorseAction::VERSION, '1.0.0', '>=')
        );
    }
}
