<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\OrderAction::class)]
#[UsesClass(\StoreCore\Actions\TradeAction::class)]
final class OrderActionTest extends TestCase
{
    #[TestDox('OrderAction class is concrete')]
    public function testOrderActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderAction is a TradeAction')]
    public function testOrderActionIsTradeAction(): void
    {
        $this->assertInstanceOf(Action::class, new OrderAction());

        $this->assertInstanceOf(
            TradeAction::class,
            new OrderAction(),
            'OrderAction class hierarchy is Thing > Action > TradeAction > OrderAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('OrderAction is JSON serializable')]
    public function testOrderActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new OrderAction());
    }

    #[Group('hmvc')]
    #[TestDox('OrderAction is stringable')]
    public function testOrderActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new OrderAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderAction::VERSION);
        $this->assertIsString(OrderAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderAction::VERSION, '0.1.0', '>=')
        );
    }
}
