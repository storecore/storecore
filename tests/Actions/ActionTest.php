<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions; 

use StoreCore\CMS\EmailMessage;
use StoreCore\CRM\Person;
use StoreCore\PIM\Product;
use StoreCore\Types\EntryPoint;
use StoreCore\Types\Thing;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\Action::class)]
#[CoversClass(\StoreCore\Actions\ActionStatusType::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\CMS\EmailMessage::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\EntryPoint::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class ActionTest extends TestCase
{
    #[TestDox('Action class is concrete')]
    public function testActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(Action::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Action is Thing')]
    public function testActionIsThing(): void
    {
        $this->assertInstanceOf(Thing::class, new Action());
    }

    #[TestDox('Action is JSON serializable')]
    public function testActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Action());
    }

    #[TestDox('Action is stringable')]
    public function testActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Action());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Action::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Action::VERSION);
        $this->assertIsString(Action::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Action::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://schema.org/actionStatus
     */
    #[TestDox('Action.actionStatus exists')]
    public function testActionActionStatusExists(): void
    {
        $action = new Action();
        $this->assertObjectHasProperty('actionStatus', $action);
    }

    #[Depends('testActionActionStatusExists')]
    #[TestDox('Action.actionStatus is publicsts')]
    public function testActionActionStatusIsPublic(): void
    {
        $property = new ReflectionProperty(Action::class, 'actionStatus');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testActionActionStatusExists')]
    #[Depends('testActionActionStatusIsPublic')]
    #[TestDox('Action.actionStatus is null by default')]
    public function testActionActionStatusIsNullByDefault(): void
    {
        $action = new Action();
        $this->assertNull($action->actionStatus);
    }

    #[Depends('testActionActionStatusExists')]
    #[Depends('testActionActionStatusIsPublic')]
    #[TestDox('Action.actionStatus is stringable ActionStatusType enumeration member')]
    public function testActionActionStatusIsStringableActionStatusTypeEnumerationMember(): void
    {
        $search = new SearchAction();
        $this->assertInstanceOf(Action::class, $search);
        $this->assertNull($search->actionStatus);

        $search->actionStatus = ActionStatusType::PotentialActionStatus;
        $this->assertNotNull($search->actionStatus);
        $this->assertSame('PotentialActionStatus', $search->actionStatus->name);
        $this->assertSame('https://schema.org/PotentialActionStatus', $search->actionStatus->value);
    }


    /**
     * @see https://schema.org/object
     */
    #[TestDox('Action.object exists')]
    public function testActionObjectExists(): void
    {
        $this->assertObjectHasProperty('object', new Action());
    }

    #[Depends('testActionObjectExists')]
    #[TestDox('Action.object is public')]
    public function testActionObjectIsPublic(): void
    {
        $property = new ReflectionProperty(Action::class, 'object');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testActionObjectExists')]
    #[Depends('testActionObjectIsPublic')]
    #[TestDox('Action.object is null by default')]
    public function testActionObjectIsNullByDefault(): void
    {
        $action = new Action();
        $this->assertNull($action->object);
    }

    #[Depends('testActionObjectIsNullByDefault')]
    #[TestDox('Action.object can be a Person')]
    public function testActionObjectCanBeAPerson(): void
    {
        $action = new Action();
        $this->assertEmpty($action->object);

        $action->object = new Person('Steve');
        $this->assertNotEmpty($action->object);
        $this->assertIsObject($action->object);
        $this->assertInstanceOf(\StoreCore\CRM\Person::class, $action->object);

        $this->assertIsObject($action->object->name);
        $this->assertInstanceOf(\Stringable::class, $action->object->name);
        $this->assertEquals('Steve', (string) $action->object->name);
    }

    #[Depends('testActionObjectIsNullByDefault')]
    #[TestDox('Action.object can be a Product')]
    public function testActionObjectCanBeAProduct(): void
    {
        $action = new Action();
        $this->assertEmpty($action->object);

        $product = new Product();
        $product->name = 'iPod';

        $action->object = $product;
        $this->assertNotEmpty($action->object);
        $this->assertIsObject($action->object);
        $this->assertInstanceOf(\StoreCore\PIM\Product::class, $action->object);
        $this->assertEquals('iPod', $action->object->name);
    }


    #[Group('hmvc')]
    #[TestDox('Action.target exists')]
    public function testActionTargetExists(): void
    {
        $this->assertObjectHasProperty('target', new Action());
    }

    #[Depends('testActionTargetExists')]
    #[Group('hmvc')]
    #[TestDox('Action.target is protected')]
    public function testActionTargetIsProtected(): void
    {
        $property = new ReflectionProperty(Action::class, 'target');
        $this->assertFalse($property->isPublic());
        $this->assertTrue($property->isProtected());
    }

    #[Depends('testActionTargetExists')]
    #[Group('hmvc')]
    #[TestDox('Action.target is null by default')]
    public function testActionTargetIsNullByDefault(): void
    {
        $action = new Action();
        $this->assertNull($action->target);
    }

    /**
     * @see https://schema.org/Movie#eg-0187 Schema.org `Movie` example 2
     */
    #[Depends('testActionTargetIsNullByDefault')]
    #[Group('hmvc')]
    #[TestDox('Action.target EntryPoint MAY be set with URL string')]
    public function testActionTargetEntryPointMayBeSetWithUrlString(): void
    {
        $action = new Action();
        $action->target = 'https://example.com/player?id=123';
        $this->assertNotNull($action->target);
        $this->assertIsNotString($action->target);

        $this->assertIsObject($action->target);
        $this->assertInstanceOf(\StoreCore\Types\EntryPoint::class, $action->target);
        $this->assertObjectHasProperty('urlTemplate', $action->target);
        $this->assertSame('https://example.com/player?id=123', $action->target->urlTemplate);
    }


    /**
     * @see https://developers.google.com/gmail/markup/actions/declaring-actions#expiring_actions
     *      Expiring Actions - Declare Actions - Email Markup for Gmail - Google Developers
     */
    #[TestDox('Expiring actions have time windows with startTime and endTime')]
    public function testExpiringActionsHaveTimeWindowsWithStartTimeAndEndTime(): void
    {
        $action = new Action();
        $this->assertObjectHasProperty('startTime', $action);
        $this->assertObjectHasProperty('endTime', $action);

        $email = new EmailMessage();
        $email->potentialAction = new ConfirmAction('Save coupon');

        $this->assertNull($email->potentialAction->startTime);
        $this->assertNull($email->potentialAction->endTime);

        $email->potentialAction->startTime = '2015-06-01T12:00:00Z';
        $email->potentialAction->endTime = '2015-06-05T12:00:00Z';
        $this->assertNotNull($email->potentialAction->startTime);
        $this->assertNotNull($email->potentialAction->endTime);

        /*
         * @see https://validator.schema.org/
         *      Note that we use the `DateTime` format from the Schema.org validator.
         */
        $json = json_encode($email, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"endTime": "2015-06-05T12:00:00+00:00"', $json);
        $this->assertStringContainsString('"startTime": "2015-06-01T12:00:00+00:00"', $json);
    }
}
