<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ActivateAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ControlAction::class)]
final class ActivateActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ActivateAction class is concrete')]
    public function testActivateActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ActivateAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ActivateAction is an Action')]
    public function testActivateActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ActivateAction());
    }

    #[Depends('testActivateActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('ActivateAction is a ControlAction')]
    public function testActivateActionIsControlAction(): void
    {
        $action = new ActivateAction();
        $this->assertInstanceOf(ControlAction::class, $action);
    }


    #[Group('hmvc')]
    #[TestDox('ActivateAction is JSON serializable')]
    public function testActivateActionIsJsonSerializable(): void
    {
        $action = new ActivateAction();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('ActivateAction is stringable')]
    public function testActivateActionIsStringable(): void
    {
        $action = new ActivateAction();
        $this->assertInstanceOf(\Stringable::class, $action);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ActivateAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ActivateAction::VERSION);
        $this->assertIsString(ActivateAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ActivateAction::VERSION, '1.0.0', '>=')
        );
    }
}
