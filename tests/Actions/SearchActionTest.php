<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\WebSite;
use StoreCore\Types\EntryPoint;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;

#[CoversClass(\StoreCore\Actions\SearchAction::class)]
#[CoversClass(\StoreCore\Types\EntryPoint::class)]
#[UsesClass(\StoreCore\CMS\WebSite::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class SearchActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('SearchAction class is concrete')]
    public function testSearchActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(SearchAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('SearchAction is an Action')]
    public function testSearchActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new SearchAction());
    }

    #[Group('hmvc')]
    #[TestDox('SearchAction is JSON serializable')]
    public function testSearchActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new SearchAction());
    }


    #[TestDox('SearchAction has an EntryPoint target')]
    public function testSearchActionHasAnEntryPointTarget(): void
    {
        $entry_point = new EntryPoint();
        $entry_point->urlTemplate = 'https://query.example.com/search?q={search_term_string}';

        $potential_action = new SearchAction();
        $potential_action->target = $entry_point;

        $this->assertNotNull($potential_action->target);
        $this->assertInstanceOf(EntryPoint::class, $potential_action->target);
        $this->assertSame(
            'https://query.example.com/search?q={search_term_string}',
            $potential_action->target->urlTemplate,
            'SearchAction::$target SHOULD contain EntryPoint::$urlTemplate.'
        );
    }

    /**
     * @see https://developers.google.com/search/docs/advanced/structured-data/sitelinks-searchbox
     *
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "WebSite",
     *       "url": "https://www.example.com/",
     *       "potentialAction": {
     *       "@type": "SearchAction",
     *         "target": {
     *           "@type": "EntryPoint",
     *           "urlTemplate": "https://query.example.com/search?q={search_term_string}"
     *         },
     *         "query-input": "required name=search_term_string"
     *       }
     *     }
     *     ```
     */
    #[TestDox('WebSite.potentialAction is SearchAction')]
    public function testWebSitePotentialActionIsSearchAction(): void
    {
        $website = new WebSite();
        $website->url = 'https://www.example.com/';
        $website->potentialAction = new SearchAction();
        $website->potentialAction->target = new EntryPoint();
        $website->potentialAction->target->urlTemplate = 'https://query.example.com/search?q={search_term_string}';
        $website->potentialAction->queryInput = 'required name=search_term_string';

        $json = json_encode($website, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@type":"WebSite"', $json);
        $this->assertStringContainsString('"@type":"SearchAction"', $json);
        $this->assertStringContainsString('"@type":"EntryPoint"', $json);
        $this->assertStringContainsString('"urlTemplate":"https://query.example.com/search?q={search_term_string}"', $json);
        $this->assertStringContainsString('"query-input":"required name=search_term_string"', $json);

        $site = json_decode($json, false);

        $this->assertObjectHasProperty('url', $site);
        $this->assertSame('https://www.example.com/', $site->url);

        $this->assertObjectHasProperty('potentialAction', $site);
        $this->assertIsObject($site->potentialAction);

        $this->assertObjectHasProperty('@type', $site->potentialAction);

        $this->assertObjectHasProperty('target', $site->potentialAction);
        $this->assertIsObject($site->potentialAction->target);

        $this->assertObjectHasProperty('query-input', $site->potentialAction);
    }
}
