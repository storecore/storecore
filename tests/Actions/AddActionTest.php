<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\AddAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\UpdateAction::class)]
final class AddActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AddAction class is concrete')]
    public function testAddActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(AddAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('AddAction is an Action')]
    public function testAddActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new AddAction());
    }

    #[Depends('testAddActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('AddAction is an UpdateAction')]
    public function testAddActionIsUpdateAction(): void
    {
        $this->assertInstanceOf(UpdateAction::class, new AddAction());
    }


    #[Group('hmvc')]
    #[TestDox('AddAction is JSON serializable')]
    public function testAddActionIsJsonSerializable(): void
    {
        $action = new AddAction();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AddAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AddAction::VERSION);
        $this->assertIsString(AddAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AddAction::VERSION, '1.0.0', '>=')
        );
    }
}
