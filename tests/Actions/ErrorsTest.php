<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Actions; 

use StoreCore\Registry;
use StoreCore\Types\DateTime;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Actions\Errors::class)]
#[CoversClass(\StoreCore\FileSystem\Logger::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\Cache::class)]
#[UsesClass(\StoreCore\Actions\Error::class)]
#[UsesClass(\StoreCore\Actions\ErrorThing::class)]
#[UsesClass(\StoreCore\Actions\ObjectStorage::class)]
#[UsesClass(\StoreCore\Actions\Repository::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ErrorsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Errors is a database model')]
    public function testErrorsIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Errors::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Errors implements PSR-11 ContainerInterface')]
    public function testErrorsImplementsPsr11ContainerInterface(): void
    {
        $class = new ReflectionClass(Errors::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Errors::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Errors::VERSION);
        $this->assertIsString(Errors::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Errors::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Errors::get exists')]
    public function testErrorsGetExists(): void
    {
        $class = new \ReflectionClass(Errors::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testErrorsGetExists')]
    #[TestDox('Errors::get exists')]
    public function testErrorsGetIsPublic(): void
    {
        $method = new ReflectionMethod(Errors::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[Depends('testErrorsGetExists')]
    #[TestDox('Errors::get has one REQUIRED parameter')]
    public function testErrorsGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Errors::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Errors::has exists')]
    public function testErrorsHasExists(): void
    {
        $class = new \ReflectionClass(Errors::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testErrorsHasExists')]
    #[TestDox('Errors::has is public')]
    public function testErrorsHasIsPublic(): void
    {
        $method = new ReflectionMethod(Errors::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testErrorsHasExists')]
    #[TestDox('Errors::has has one REQUIRED parameter')]
    public function testErrorsHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Errors::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Errors::set exists')]
    public function testErrorsSetExists(): void
    {
        $class = new \ReflectionClass(Errors::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testErrorsSetExists')]
    #[TestDox('Errors::set is public')]
    public function testErrorsSetIsPublic(): void
    {
        $method = new ReflectionMethod(Errors::class, 'set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testErrorsSetExists')]
    #[TestDox('Errors::set has one REQUIRED parameter')]
    public function testErrorsSetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Errors::class, 'set');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    /**
     * @see https://www.rfc-editor.org/rfc/rfc7807#section-3
     */
    #[TestDox('Actions with errors example')]
    public function testActionsWithErrorsExample(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        // An `Action` and the `Action.error` have the same `UUID`.
        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();
        $this->assertIsNotString($uuid);
        $this->assertIsObject($uuid);

        $errors_repository = new Errors($registry);
        $this->assertFalse($errors_repository->has((string) $uuid));

        // Logic dictates that we need an `Action` before an `Action.error` can occur.
        $action = new PayAction();
        $action->startTime = new DateTime('now');

        // The `Action.actionStatus` MAY change on errors.
        $action->actionStatus = ActionStatusType::ActiveActionStatus;
        $this->assertSame(ActionStatusType::ActiveActionStatus, $action->actionStatus);

        $action->setIdentifier($uuid);
        $this->assertTrue($action->hasIdentifier());

        $actions_repository = new Repository($registry);
        $this->assertFalse($actions_repository->has((string) $uuid));

        $actions_repository->set($action);
        $this->assertTrue($actions_repository->has((string) $uuid));

        $error = new Error();
        $error->setIdentifier($uuid);
        $this->assertTrue($error->hasIdentifier());

        $error->status   = 402;
        $error->type     = 'https://example.com/probs/out-of-credit';
        $error->title    = 'You do not have enough credit.';
        $error->detail   = 'Your current balance is 30, but that costs 50.';
        $error->instance = 'https://example.com/account/12345/msgs/abc';

        $errors_repository->set($error);
        $this->assertTrue($errors_repository->has((string) $uuid));

        $action = null;
        unset($action);

        /*
         * @see https://schema.org/ActionStatusType
         *   If we now retrieve the `Action`, its `actionStatus` MUST be
         *   `ActionStatusType::FailedActionStatus` and its `error` property
         *   SHOULD be a `Thing` that references the stored `Error`.
         */
        $this->assertTrue($actions_repository->has((string) $uuid));

        $action = $actions_repository->get((string) $uuid);
        $this->assertInstanceOf(Action::class, $action);

        $this->assertTrue($action->hasIdentifier());
        $this->assertEquals(
            $uuid, $action->getIdentifier(),
            'UUIDs should now be equal but NOT reference the same value object.'
        );

        $this->assertNotEquals(ActionStatusType::ActiveActionStatus, $action->actionStatus);
        $this->assertEquals(
            ActionStatusType::FailedActionStatus, $action->actionStatus,
            'Action.actionStatus MUST now report that the Action has failed.'
        );

        $this->assertNotEmpty($action->error);
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, $action->error);

        // Clean up by deleting the action through the pass-through cache.
        $cache = new Cache($registry);
        $this->assertTrue($cache->delete((string) $uuid));
    }
}
