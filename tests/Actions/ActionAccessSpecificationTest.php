<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use StoreCore\CMS\MusicRecording;
use StoreCore\PIM\Offer;
use StoreCore\Types\{Intangible, Thing};

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ActionAccessSpecification::class)]
#[UsesClass(\StoreCore\CMS\MusicRecording::class)]
#[UsesClass(\StoreCore\PIM\Offer::class)]
#[UsesClass(\StoreCore\Types\Intangible::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
final class ActionAccessSpecificationTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ActionAccessSpecification class is concrete')]
    public function testActionAccessSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(ActionAccessSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ActionAccessSpecification is an Intangible Thing')]
    public function testActionAccessSpecificationIsIntangibleThing(): void
    {
        $specification = new ActionAccessSpecification();
        $this->assertInstanceOf(Intangible::class, $specification);
        $this->assertInstanceOf(Thing::class, $specification);
    }


    #[Group('hmvc')]
    #[TestDox('ActionAccessSpecification is JSON serializable')]
    public function testActionAccessSpecificationIsJsonSerializable(): void
    {
        $specification = new ActionAccessSpecification();
        $this->assertInstanceOf(\JsonSerializable::class, $specification);
    }

    #[Group('hmvc')]
    #[TestDox('ActionAccessSpecification is stringable')]
    public function testActionAccessSpecificationIsStringable(): void
    {
        $specification = new ActionAccessSpecification();
        $this->assertInstanceOf(\Stringable::class, $specification);
    }


    #[Group('hmvc')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ActionAccessSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ActionAccessSpecification::VERSION);
        $this->assertIsString(ActionAccessSpecification::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('hmvc')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ActionAccessSpecification::VERSION, '0.1.0', '>=')
        );
    }


    /**
     * @see https://schema.org/expectsAcceptanceOf
     *      Schema.org property `expectsAcceptanceOf`
     */
    #[TestDox('ActionAccessSpecification.expectsAcceptanceOf exists')]
    public function testActionAccessSpecificationExpectsAcceptanceOfExists(): void
    {
        $actionAccessSpecification = new ActionAccessSpecification();
        $this->assertObjectHasProperty('expectsAcceptanceOf', $actionAccessSpecification);
    }

    #[Depends('testActionAccessSpecificationExpectsAcceptanceOfExists')]
    #[TestDox('ActionAccessSpecification.expectsAcceptanceOf is public')]
    public function testConsumeActionExpectsAcceptanceOfIsPublic(): void
    {
        $property = new ReflectionProperty(ActionAccessSpecification::class, 'expectsAcceptanceOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testActionAccessSpecificationExpectsAcceptanceOfExists')]
    #[TestDox('ActionAccessSpecification.expectsAcceptanceOf is null by default')]
    public function testActionAccessSpecificationExpectsAcceptanceOfIsNullByDefault()
    {
        $specification = new ActionAccessSpecification();
        $this->assertNull($specification->expectsAcceptanceOf);
    }

    /**
     * @see https://schema.org/ActionAccessSpecification#eg-0253
     *      Schema.org `ActionAccessSpecification` Example 1
     */
    #[Depends('testActionAccessSpecificationExpectsAcceptanceOfIsNullByDefault')]
    #[TestDox('ActionAccessSpecification.expectsAcceptanceOf accepts Offer')]
    public function testActionAccessSpecificationExpectsAcceptanceOfAcceptsOffer(): void
    {
        $offer = new Offer();
        $offer->itemOffered = new MusicRecording();
        $offer->itemOffered->name = 'Song I want to stream that is only available for streaming after I buy it.';

        $requirement = new ActionAccessSpecification();
        $requirement->expectsAcceptanceOf = $offer;
        $this->assertNotNull($requirement->expectsAcceptanceOf);
        $this->assertSame($offer, $requirement->expectsAcceptanceOf);
    }
}
