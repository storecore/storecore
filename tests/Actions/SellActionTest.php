<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Book;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\SellAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\CMS\Book::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class SellActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('SellAction class is concrete')]
    public function testSellActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(SellAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('SellAction is an Action')]
    public function testSellActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new SellAction());
    }

    #[Depends('testSellActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('SellAction is a TradeAction')]
    public function testSellActionIsTradeAction(): void
    {
        $this->assertInstanceOf(TradeAction::class, new SellAction());
    }


    #[Group('hmvc')]
    #[TestDox('SellAction is JSON serializable')]
    public function testSellActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new SellAction());
    }

    #[Group('hmvc')]
    #[TestDox('SellAction is stringable')]
    public function testSellActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new SellAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SellAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SellAction::VERSION);
        $this->assertIsString(SellAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SellAction::VERSION, '0.1.0', '>=')
        );
    }


    /**
     * @see https://schema.org/SellAction#eg-0147
     *      Schema.org `SellAction` Example 1
     */
    #[TestDox('John sold a book on Amazon.com')]
    public function testJohnSoldABookOnAmazonCom(): void
    {
        $action = new SellAction();
        $action->agent = new Person('John');
        $action->object = new Book('Outliers');

        $this->assertSame('John', (string) $action->agent->name);
        $this->assertSame('Outliers', $action->object->name);

        // Amazon.com is not the `buyer` but a `participant`.
        $action->participant = new Organization('Amazon.com');
        $this->assertSame('Amazon.com', $action->participant->name);
        $this->assertNull($action->buyer);

        /**
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "SellAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "object": {
         *     "@type": "Book",
         *     "name": "Outliers"
         *   },
         *   "participant": {
         *     "@type": "Organization",
         *     "name": "Amazon.com"
         *   }
         * }
         * ```
         */
        $json = (string) $action;
        $this->assertNotEmpty($json);
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"SellAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"object":{"@type":"Book","name":"Outliers"', $json);
        $this->assertStringContainsString('"participant":{"@type":"Organization","name":"Amazon.com"}', $json);
    }
}
