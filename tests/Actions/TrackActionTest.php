<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\OML\{DeliveryMethod, DeliveryMethodTrait};

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\TrackAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\FindAction::class)]
#[UsesClass(\StoreCore\OML\DeliveryMethod::class)]
final class TrackActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('TrackAction class is concrete')]
    public function testTrackActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(TrackAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('TrackAction is an Action')]
    public function testTrackActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new TrackAction());
    }

    #[Depends('testTrackActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('TrackAction is a FindAction')]
    public function testTrackActionIsFindAction(): void
    {
        $this->assertInstanceOf(FindAction::class, new TrackAction());
    }


    #[Group('hmvc')]
    #[TestDox('TrackAction is JSON serializable')]
    public function testTrackActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new TrackAction());
    }

    #[Group('hmvc')]
    #[TestDox('TrackAction is stringable')]
    public function testTrackActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new TrackAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TrackAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TrackAction::VERSION);
        $this->assertIsString(TrackAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TrackAction::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('TrackAction.deliveryMethod exists')]
    public function testTrackActionDeliveryMethodExists(): void
    {
        $this->assertObjectHasProperty('deliveryMethod', new TrackAction());
    }

    #[Depends('testTrackActionDeliveryMethodExists')]
    #[TestDox('TrackAction.deliveryMethod is null by default')]
    public function testTrackActionDeliveryMethodIsNullByDefault(): void
    {
        $trackAction = new TrackAction();
        $this->assertNull($trackAction->deliveryMethod);
    }

    #[Depends('testTrackActionDeliveryMethodExists')]
    #[Depends('testTrackActionDeliveryMethodIsNullByDefault')]
    #[TestDox('TrackAction.deliveryMethod accepts DeliveryMethod')]
    public function testTrackActionDeliveryMethodAcceptsDeliveryMethod(): void
    {
        $trackAction = new TrackAction();
        $trackAction->deliveryMethod = new DeliveryMethod('UPS');

        $this->assertInstanceOf(\StoreCore\OML\DeliveryMethod::class, $trackAction->deliveryMethod);
        $this->assertEquals('UPS', $trackAction->deliveryMethod->name);

        $expectedJson = <<<'JSON'
        {
          "@context": "https://schema.org",
          "@type": "TrackAction",
          "deliveryMethod": "UPS"
        }
        JSON;

        $actualJson = json_encode($trackAction, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
