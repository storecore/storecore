<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions; 

use StoreCore\Types\URL;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\Error::class)]
#[CoversClass(\StoreCore\Types\URL::class)]
final class ErrorTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Error class is concrete')]
    public function testErrorClassIsConcrete(): void
    {
        $class = new ReflectionClass(Error::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Error implements IdentityInterface')]
    public function testErrorImplementsIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Error());
    }

    #[Group('hmvc')]
    #[TestDox('Error is JSON serializable')]
    public function testErrorIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Error());
    }

    #[Group('hmvc')]
    #[TestDox('Error is stringable')]
    public function testErrorIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Error());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Error::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Error::VERSION);
        $this->assertIsString(Error::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(Error::VERSION, '1.0.0-alpha.1', '>='));
    }


    #[TestDox('Error.instance exists')]
    public function testErrorInstanceExists(): void
    {
        $error = new Error();
        $this->assertObjectHasProperty('instance', $error);
    }

    #[Depends('testErrorInstanceExists')]
    #[TestDox('Error.instance is null by default')]
    public function testErrorInstanceIsNullByDefault(): void
    {
        $error = new Error();
        $this->assertNull($error->instance);
    }

    #[Depends('testErrorInstanceIsNullByDefault')]
    #[TestDox('Error.instance accepts URL as value object')]
    public function testErrorInstanceAcceptsUrlAsValueObject(): void
    {
        $error = new Error();
        $error->instance = new URL('https://panel.sendcloud.sc/api/v2/parcels/statuses');

        $this->assertNotNull($error->instance);
        $this->assertIsObject($error->instance);
        $this->assertInstanceOf(URL::class, $error->instance);
    }

    #[Depends('testErrorInstanceIsNullByDefault')]
    #[TestDox('Error.instance accepts URL as string')]
    public function testErrorInstanceAcceptsUrlAsString(): void
    {
        $error = new Error();
        $error->instance = 'https://panel.sendcloud.sc/api/v2/parcels/statuses';

        $this->assertNotNull($error->instance);
        $this->assertIsNotString($error->instance);
        $this->assertInstanceOf(\Stringable::class, $error->instance);
    }

    /**
     * @see https://www.rfc-editor.org/rfc/rfc7807#section-3.1
     *   Note that both `type` and `instance` accept relative URIs; this means
     *   that they MUST be resolved relative to the document’s base URI, as
     *   per RFC 3986, Section 5.
     */
    #[TestDox('Error.instance does not accept relative URLs')]
    public function testErrorInstanceDoesNotAcceptRelativeURLs(): void
    {
        $error = new Error();
        $error->type = 'https://example.com/probs/out-of-credit';

        $this->expectException(\ValueError::class);
        $error->instance = '/account/12345/msgs/abc';
    }


    #[TestDox('Error.status exists')]
    public function testErrorStatusExists(): void
    {
        $error = new Error();
        $this->assertObjectHasProperty('status', $error);
    }

    #[Depends('testErrorStatusExists')]
    #[TestDox('Error.status is non-empty integer')]
    public function testErrorStatusIsNonEmptyInteger(): void
    {
        $error = new Error();
        $this->assertNotEmpty($error->status);
        $this->assertIsInt($error->status);
    }

    #[Depends('testErrorStatusIsNonEmptyInteger')]
    #[TestDox('Error.status is (int) 501 by default')]
    public function testErrorStatusIsInt501ByDefault(): void
    {
        $error = new Error();
        $this->assertSame(501, $error->status);
    }

    #[Depends('testErrorIsJsonSerializable')]
    #[Depends('testErrorStatusIsInt501ByDefault')]
    #[TestDox('Error.status is 501 in JSON')]
    public function testErrorStatusIs501InJson(): void
    {
        $error = new Error();
        $expectedJson = '
            {
              "type": "about:blank",
              "status": 501
            }
        ';
        $actualJson = json_encode($error, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    /**
     * @see https://api.bol.com/retailer/public/redoc/v9/retailer.html#operation/get-competing-offers
     *
     * ```json
     * {
     *   "type": "https://api.bol.com/problems",
     *   "title": "Error validating request. Consult the bol.com API documentation for more information.",
     *   "status": "40X",
     *   "detail": "Bad request",
     *   "host": "Instance-001",
     *   "instance": "https://api.bol.com//retailer/resource",
     *   "violations": [
     *     {
     *       "name": "exampleValue",
     *       "reason": "Request contains invalid value(s): 'INVALID', allowed values: ALLOWED_VALUE1, ALLOWED_VALUE2."
     *     }
     *   ]
     * }
     * ```
     */
    #[TestDox('Example matching bol.com API v9 response')]
    public function testExampleMatchingBolComApiV9Response(): void
    {
        $error = new Error();

        $error->type = 'https://api.bol.com/problems';
        $this->assertInstanceOf(\Stringable::class, $error->type);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $error->type);

        /*
         * The bol.com API example has a long `title` and a very short `detail`.
         * According to RFC 7807 this SHOULD be the other way around.
         */
        $error->title = 'Bad Request';
        $error->detail = 'Error validating request. Consult the bol.com API documentation for more information.';

        /*
         * The `(string) "40X"` is not a valid HTTP or RFC 7807 `status`.
         * Because the error reports a `Bad Request`, this MUST be an `(int) 400`.
         */
        $error->status = 400;
        $this->assertSame(400, $error->status);

        $error->instance = 'https://api.bol.com//retailer/resource';
        $this->assertInstanceOf(\Stringable::class, $error->instance);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $error->instance);

        $expectedJson = <<<'JSON'
            {
              "type": "https://api.bol.com/problems",
              "title": "Bad Request",
              "status": 400,
              "detail": "Error validating request. Consult the bol.com API documentation for more information.",
              "instance": "https://api.bol.com//retailer/resource"
            }
        JSON;

        $actualJson = json_encode($error, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        $actualJson = (string) $error;
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
