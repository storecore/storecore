<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\ScholarlyArticle;
use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Actions\CreateAction::class)]
#[CoversClass(\StoreCore\Actions\WriteAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\CMS\ScholarlyArticle::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class WriteActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('WriteAction class is concrete')]
    public function testWriteActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(WriteAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('WriteAction is an Action')]
    public function testWriteActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new WriteAction());
    }

    #[Depends('testWriteActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('WriteAction is a CreateAction')]
    public function testWriteActionIsCreateAction(): void
    {
        $this->assertInstanceOf(CreateAction::class, new WriteAction());
    }


    #[Group('hmvc')]
    #[TestDox('WriteAction is JSON serializable')]
    public function testWriteActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new WriteAction());
    }

    #[Group('hmvc')]
    #[TestDox('WriteAction is stringable')]
    public function testWriteActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new WriteAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(WriteAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WriteAction::VERSION);
        $this->assertIsString(WriteAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(WriteAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/WriteAction#eg-0079
     *      Schema.org `WriteAction` Example 1
     */
    #[TestDox('John wrote an article on algorithms')]
    public function testJohnWroteAnArticleOnAlgorithms(): void
    {
        $action = new WriteAction();
        $action->agent = new Person('John');
        $action->result = new ScholarlyArticle('We found that P = NP!');

        $this->assertEquals('John', (string) $action->agent->name);
        $this->assertEquals('We found that P = NP!', $action->result->name);

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "WriteAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "result": {
         *     "@type": "ScholarlyArticle",
         *     "name": "We found that P = NP!"
         *   }
         * }
         * ```
         */
        $json = (string) $action;
        $this->assertNotEmpty($json);
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"WriteAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"result":{"@type":"ScholarlyArticle","name":"We found that P = NP!"', $json);
    }
}
