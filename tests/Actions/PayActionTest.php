<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\PayAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\TradeAction::class)]
final class PayActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('PayAction class is concrete')]
    public function testPayActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(PayAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('PayAction is an Action')]
    public function testPayActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new PayAction());
    }

    #[Depends('testPayActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('PayAction is a TradeAction')]
    public function testPayActionIsTradeAction(): void
    {
        $this->assertInstanceOf(TradeAction::class, new PayAction());
    }

    #[Depends('testPayActionIsTradeAction')]
    #[Group('hmvc')]
    #[TestDox('PayAction is NOT a BuyAction')]
    public function testPayActionIsNotBuyAction(): void
    {
        $this->assertNotInstanceOf(
            BuyAction::class,
            new PayAction(),
            'PayAction is NOT a BuyAction.'
        );
    }


    #[Group('hmvc')]
    #[TestDox('PayAction is JSON serializable')]
    public function testPayActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new PayAction());
    }

    #[Group('hmvc')]
    #[TestDox('PayAction is stringable')]
    public function testPayActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new PayAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PayAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PayAction::VERSION);
        $this->assertIsString(PayAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PayAction::VERSION, '0.1.0', '>=')
        );
    }
}
