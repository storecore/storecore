<?php declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\SuspendAction::class)]
final class SuspendActionTest extends TestCase
{
    #[TestDox('SuspendAction class is concrete')]
    public function testSuspendActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(SuspendAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('SuspendAction is a ControlAction')]
    public function testSuspendActionIsControlAction(): void
    {
        $this->assertInstanceOf(Action::class, new SuspendAction());

        $this->assertInstanceOf(
            ControlAction::class,
            new SuspendAction(),
            'SuspendAction class hierarchy is Thing > Action > ControlAction > SuspendAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('SuspendAction is JSON serializable')]
    public function testSuspendActionIsJsonSerializable(): void
    {
        $action = new SuspendAction();
        $this->assertInstanceOf(\JsonSerializable::class, $action);
    }

    #[Group('hmvc')]
    #[TestDox('SuspendAction is stringable')]
    public function testSuspendActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new SuspendAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SuspendAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SuspendAction::VERSION);
        $this->assertIsString(SuspendAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SuspendAction::VERSION, '1.0.0', '>=')
        );
    }
}
