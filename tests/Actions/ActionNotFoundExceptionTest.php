<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Actions\ActionNotFoundException::class)]
final class ActionNotFoundExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ActionNotFoundException class is concrete')]
    public function testActionNotFoundExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ActionNotFoundException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('ActionNotFoundException is a throwable runtime exception')]
    public function testActionNotFoundExceptionIsThrowableRuntimeException(): void
    {
        $class = new ReflectionClass(ActionNotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\RuntimeException::class));

        $this->expectException(\RuntimeException::class);
        throw new ActionNotFoundException();
    }


    #[Depends('testActionNotFoundExceptionIsThrowableRuntimeException')]
    #[TestDox('ActionNotFoundException is a NotFoundException')]
    public function testActionNotFoundExceptionIsNotFoundException(): void
    {
        $class = new ReflectionClass(ActionNotFoundException::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\NotFoundException::class));

        $this->expectException(\StoreCore\NotFoundException::class);
        throw new ActionNotFoundException();
    }

    #[Group('distro')]
    #[TestDox('Implemented PSR-11 NotFoundExceptionInterface exists')]
    public function testImplementedPSR11NotFoundExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists(\Psr\Container\NotFoundExceptionInterface::class));
    }

    #[Depends('testActionNotFoundExceptionIsThrowableRuntimeException')]
    #[Depends('testImplementedPSR11NotFoundExceptionInterfaceExists')]
    #[TestDox('ActionNotFoundException implements PSR-11 NotFoundExceptionInterface')]
    public function testActionNotFoundExceptionImplementsPsr11NotFoundExceptionInterface(): void
    {
        $class = new ReflectionClass(ActionNotFoundException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\NotFoundExceptionInterface::class));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        throw new ActionNotFoundException();
    }
}
