<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ReserveAction::class)]
#[UsesClass(\StoreCore\Actions\OrganizeAction::class)]
#[UsesClass(\StoreCore\Actions\PlanAction::class)]
final class ReserveActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ReserveAction class is concrete')]
    public function testReserveActionClassIsConcrete(): void
    {
        $class = new \ReflectionClass(ReserveAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ReserveAction is a PlanAction')]
    public function testReserveActionIsPlanAction(): void
    {
        $reserveAction = new ReserveAction();
        $this->assertInstanceOf(Action::class, $reserveAction);
        $this->assertInstanceOf(OrganizeAction::class, $reserveAction);
        $this->assertInstanceOf(
            PlanAction::class,
            $reserveAction,
            'ReserveAction class hierarchy is Thing > Action > OrganizeAction > PlanAction > ReserveAction.'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ReserveAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ReserveAction::VERSION);
        $this->assertIsString(ReserveAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ReserveAction::VERSION, '1.0.0', '>=')
        );
    }
}
