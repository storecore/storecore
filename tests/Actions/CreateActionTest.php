<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\WebPage;
use StoreCore\CRM\Person;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\CreateAction::class)]
final class CreateActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CreateAction class is concrete')]
    public function testCreateActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(CreateAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CreateAction is an Action')]
    public function testCreateActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new CreateAction());
    }

    /**
     * @see https://schema.org/CreateAction#subtypes
     *      More specific types of `CreateAction`
     */
    #[Group('hmvc')]
    #[TestDox('CreateAction subtypes')]
    public function testCreateActionSubtypes(): void
    {
        $this->assertTrue(class_exists(CookAction::class));
        $this->assertInstanceOf(CreateAction::class, new CookAction());

        $this->assertTrue(class_exists(DrawAction::class));
        $this->assertInstanceOf(CreateAction::class, new DrawAction());

        $this->assertTrue(class_exists(FilmAction::class));
        $this->assertInstanceOf(CreateAction::class, new FilmAction());

        $this->assertTrue(class_exists(PaintAction::class));
        $this->assertInstanceOf(CreateAction::class, new PaintAction());

        $this->assertTrue(class_exists(PhotographAction::class));
        $this->assertInstanceOf(CreateAction::class, new PhotographAction());

        $this->assertTrue(class_exists(WriteAction::class));
        $this->assertInstanceOf(CreateAction::class, new WriteAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CreateAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CreateAction::VERSION);
        $this->assertIsString(CreateAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CreateAction::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CreateAction has an agent that creates a result')]
    public function testCreateActionHasAnAgentThatCreatesAResult(): void
    {
        $createAction = new CreateAction();
        $this->assertObjectHasProperty('agent',  $createAction);
        $this->assertObjectHasProperty('result', $createAction);
    }


    /**
     * @see https://schema.org/CreateAction#eg-0073
     *      Schema.org `CreateAction` Example 1
     *
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "CreateAction",
     *       "agent": {
     *         "@type": "Person",
     *         "name": "John"
     *       },
     *       "result": {
     *         "@type": "WebPage",
     *         "name": "John's thought about the web"
     *       }
     *     }
     *     ```
     */
    #[TestDox('John created a web page')]
    public function testJohnCreatedWebPage(): void
    {
        $action = new CreateAction();
        $action->description = 'John created a web page.';

        // Note that we set `givenName` and not just `name`.
        $action->agent = new Person();
        $action->agent->givenName = 'John';

        $action->result = new WebPage();
        $action->result->name = 'John’s thought about the web';

        $json = (string) $action;
        $this->assertJson($json);

        // John …
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        // … created …
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"CreateAction"', $json);
        $this->assertStringContainsString('"@type":"CreateAction","description":"John created a web page."', $json);
        // … a web page.
        $this->assertStringContainsString('"result":{"@type":"WebPage","name":"John’s thought about the web"', $json);
    }
}
