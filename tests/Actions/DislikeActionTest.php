<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\DislikeAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class DislikeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DislikeAction class is concrete')]
    public function testDislikeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DislikeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('DislikeAction is an Action')]
    public function testDislikeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new DislikeAction());
    }

    #[Depends('testDislikeActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('DislikeAction is an AssessAction')]
    public function testDislikeActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new DislikeAction());
    }

    #[Depends('testDislikeActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('DislikeAction is a ReactAction')]
    public function testDislikeActionIsReactAction(): void
    {
        $this->assertInstanceOf(ReactAction::class, new DislikeAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DislikeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DislikeAction::VERSION);
        $this->assertIsString(DislikeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DislikeAction::VERSION, '1.0.0', '>=')
        );
    }
}
