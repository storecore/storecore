<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ChooseAction::class)]
final class ChooseActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ChooseAction class is concrete')]
    public function testChooseActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ChooseAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ChooseAction is an Action')]
    public function testChooseActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ChooseAction());
    }

    #[Depends('testChooseActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('ChooseAction is an AssessAction')]
    public function testChooseActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new ChooseAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ChooseAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ChooseAction::VERSION);
        $this->assertIsString(ChooseAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ChooseAction::VERSION, '0.1.0', '>=')
        );
    }
}
