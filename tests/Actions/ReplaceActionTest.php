<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Movie;
use StoreCore\CRM\Person;
use StoreCore\Types\ItemList;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ReplaceAction::class)]
#[UsesClass(\StoreCore\Actions\UpdateAction::class)]
final class ReplaceActionTest extends TestCase
{
    #[TestDox('ReplaceAction class is concrete')]
    public function testReplaceActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ReplaceAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ReplaceAction is an UpdateAction')]
    public function testReplaceActionIsUpdateAction(): void
    {
        $this->assertInstanceOf(Action::class, new ReplaceAction());
        $this->assertInstanceOf(UpdateAction::class, new ReplaceAction());
    }

    #[Group('hmvc')]
    #[TestDox('ReplaceAction is JSON serializable')]
    public function testReplaceActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ReplaceAction());
    }

    #[Group('hmvc')]
    #[TestDox('ReplaceAction is stringable')]
    public function testReplaceActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new ReplaceAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ReplaceAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ReplaceAction::VERSION);
        $this->assertIsString(ReplaceAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[TestDox('VERSION matches master branch')]
    #[Group('distro')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ReplaceAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/ReplaceAction#eg-0165
     *      Schema.org `ReplaceAction` Example 1
     */
    #[TestDox('John replaced “The Internship” with “The Wedding Crashers” from his movie queue.')]
    public function testJohnReplacedTheInternshipWithTheWeddingCrashersFromHisMovieQueue()
    {
        $action = new ReplaceAction();
        $action->agent = new Person('John');
        $action->replacee = new Movie('The Internship');
        $action->replacer = new Movie('The Wedding Crashers');
        $action->targetCollection = new ItemList();
        $action->targetCollection->name = 'List of my favorite movies';
        $action->targetCollection->url = 'http://netflix.com/john/favorite';

        $this->assertIsObject($action->agent->name);
        $this->assertSame('John', (string) $action->agent->name);

        $this->assertSame('The Internship',       $action->replacee->name);
        $this->assertSame('The Wedding Crashers', $action->replacer->name);

        // The `ReplaceAction` describes the replacement but does
        // not actually create a collection with the `replacer`.
        $this->assertInstanceOf(\Countable::class, $action->targetCollection);
        $this->assertSame(0, count($action->targetCollection));

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "ReplaceAction",
         *   "agent": {
         *     "@type": "Person",
         *     "name": "John"
         *   },
         *   "replacee": {
         *     "@type": "Movie",
         *     "name": "The Internship"
         *   },
         *   "replacer": {
         *     "@type": "Movie",
         *     "name": "The Wedding Crashers"
         *   },
         *   "targetCollection": {
         *     "@type": "ItemList",
         *     "name": "List of my favorite movies",
         *     "url": "http://netflix.com/john/favorite"
         *   }
         * }
         * ```
         */
        $json = (string) $action;
        $this->assertJson($json);
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"ReplaceAction"', $json);
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        $this->assertStringContainsString('"replacee":{"@type":"Movie","name":"The Internship"', $json);
        $this->assertStringContainsString('"replacer":{"@type":"Movie","name":"The Wedding Crashers"', $json);
    }
}
