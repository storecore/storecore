<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\TransferAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
final class TransferActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('TransferAction class is concrete')]
    public function testTransferActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(TransferAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TransferAction is an Action')]
    public function testTransferActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new TransferAction());
    }

    /**
     * @see https://schema.org/TransferAction#subtypes
     *      More specific types of `TransferAction`
     */
    #[Group('hmvc')]
    #[TestDox('TransferAction subtypes')]
    public function testTransferActionSubtypes(): void
    {
        $this->assertTrue(class_exists(BorrowAction::class));
        $this->assertInstanceOf(TransferAction::class, new BorrowAction());

        $this->assertTrue(class_exists(DownloadAction::class));
        $this->assertInstanceOf(TransferAction::class, new DownloadAction());

        $this->assertTrue(class_exists(GiveAction::class));
        $this->assertInstanceOf(TransferAction::class, new GiveAction());

        $this->assertTrue(class_exists(LendAction::class));
        $this->assertInstanceOf(TransferAction::class, new LendAction());

        $this->assertTrue(class_exists(MoneyTransfer::class));
        $this->assertInstanceOf(TransferAction::class, new MoneyTransfer());

        $this->assertTrue(class_exists(ReceiveAction::class));
        $this->assertInstanceOf(TransferAction::class, new ReceiveAction());

        $this->assertTrue(class_exists(ReturnAction::class));
        $this->assertInstanceOf(TransferAction::class, new ReturnAction());

        $this->assertTrue(class_exists(SendAction::class));
        $this->assertInstanceOf(TransferAction::class, new SendAction());

        $this->assertTrue(class_exists(TakeAction::class));
        $this->assertInstanceOf(TransferAction::class, new TakeAction());
    }


    #[Group('hmvc')]
    #[TestDox('TransferAction is JSON serializable')]
    public function testTransferActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new TransferAction());
    }

    #[Group('hmvc')]
    #[TestDox('TransferAction is stringable')]
    public function testTransferActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new TransferAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TransferAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TransferAction::VERSION);
        $this->assertIsString(TransferAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TransferAction::VERSION, '0.1.0', '>=')
        );
    }
}
