<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\AssessAction::class)]
#[UsesClass(\StoreCore\Actions\ReactAction::class)]
#[UsesClass(\StoreCore\Actions\DisagreeAction::class)]
final class DisagreeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DisagreeAction class is concrete')]
    public function testDisagreeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DisagreeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('DisagreeAction is an Action')]
    public function testDisagreeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new DisagreeAction());
    }

    #[Depends('testDisagreeActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('DisagreeAction is an AssessAction')]
    public function testDisagreeActionIsAssessAction(): void
    {
        $this->assertInstanceOf(AssessAction::class, new DisagreeAction());
    }

    #[Depends('testDisagreeActionIsAssessAction')]
    #[Group('hmvc')]
    #[TestDox('DisagreeAction is a ReactAction')]
    public function testDisagreeActionIsAReactAction(): void
    {
        $this->assertInstanceOf(ReactAction::class, new DisagreeAction());
    }


    #[Group('hmvc')]
    #[TestDox('DisagreeAction is JSON serializable')]
    public function testDisagreeActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new DisagreeAction());
    }

    #[Group('hmvc')]
    #[TestDox('DisagreeAction is stringable')]
    public function testDisagreeActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new DisagreeAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DisagreeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DisagreeAction::VERSION);
        $this->assertIsString(DisagreeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DisagreeAction::VERSION, '1.0.0', '>=')
        );
    }
}
