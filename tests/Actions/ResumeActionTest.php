<?php

declare(strict_types=1);

namespace StoreCore\Actions; 

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ResumeAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\ControlAction::class)]
final class ResumeActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ResumeAction class is concrete')]
    public function testResumeActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ResumeAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ResumeAction is an Action')]
    public function testResumeActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ResumeAction());
    }

    #[Depends('testResumeActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('ResumeAction is a ControlAction')]
    public function testResumeActionIsControlAction(): void
    {
        $this->assertInstanceOf(
            ControlAction::class,
            new ResumeAction(),
            'ResumeAction class hierarchy is Thing > Action > ControlAction > ResumeAction.'
        );
    }


    #[Group('hmvc')]
    #[TestDox('ResumeAction is JSON serializable')]
    public function testResumeActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ResumeAction());
    }

    #[Group('hmvc')]
    #[TestDox('ResumeAction is stringable')]
    public function testResumeActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new ResumeAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ResumeAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ResumeAction::VERSION);
        $this->assertIsString(ResumeAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ResumeAction::VERSION, '1.0.0', '>=')
        );
    }
}
