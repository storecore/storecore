<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Review;
use StoreCore\CMS\ScholarlyArticle;
use StoreCore\CRM\Person;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Actions\ReviewAction::class)]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\CMS\Review::class)]
#[UsesClass(\StoreCore\CMS\ScholarlyArticle::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class ReviewActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ReviewAction class is concrete')]
    public function testReviewActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(ReviewAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('ReviewAction is an Action')]
    public function testReviewActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new ReviewAction());
    }

    #[Depends('testReviewActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('ReviewAction is an AssessAction')]
    public function testReviewActionIsAssessAction()
    {
        $this->assertInstanceOf(AssessAction::class, new ReviewAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ReviewAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ReviewAction::VERSION);
        $this->assertIsString(ReviewAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ReviewAction::VERSION, '1.0.0', '>=')
        );
    }


    /**
     * @see https://schema.org/resultReview
     *      Schema.org `resultReview` property of a `ReviewAction`
     */
    #[TestDox('ReviewAction.resultReview exists')]
    public function testReviewActionResultReviewExists(): void
    {
        $reviewAction = new ReviewAction();
        $this->assertObjectHasProperty('resultReview', $reviewAction);
    }

    #[Depends('testReviewActionResultReviewExists')]
    #[TestDox('ReviewAction.resultReview is public')]
    public function testReviewActionResultReviewIsPublic(): void
    {
        $property = new ReflectionProperty(ReviewAction::class, 'resultReview');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testReviewActionResultReviewExists')]
    #[TestDox('ReviewAction.resultReview is null by default')]
    public function testReviewActionResultReviewIsNullByDefault(): void
    {
        $reviewAction = new ReviewAction();
        $this->assertNull($reviewAction->resultReview);
    }


    /**
     * @see https://schema.org/ReviewAction#eg-0052
     *      Schema.org `ReviewAction` Example 1
     */
    #[TestDox('John and Steve reviewed an article')]
    public function testJohnAndSteveReviewedAnArticle(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "ReviewAction",
              "agent": {
                "@type": "Person",
                "name": "John"
              },
              "resultReview": {
                "@type": "Review",
                "reviewBody": "It is pretty awesome!"
              },
              "object": {
                "@type": "ScholarlyArticle",
                "name": "We found out that P = NP!"
              },
              "participant": {
                "@type": "Person",
                "name": "Steve"
              }
            }
        JSON;

        $action = new ReviewAction();

        // John …
        $action->agent = new Person('John');
        // … and Steve …
        $action->participant = new Person('Steve');
        // … reviewed …
        $action->resultReview = new Review();
        $action->resultReview->reviewBody = 'It is pretty awesome!';
        // … an article.
        $action->object = new ScholarlyArticle();
        $action->object->name = 'We found out that P = NP!';

        $actualJson = (string) $action;
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
