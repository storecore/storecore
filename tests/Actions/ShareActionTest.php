<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\CommunicateAction::class)]
#[UsesClass(\StoreCore\Actions\InteractAction::class)]
#[UsesClass(\StoreCore\Actions\ShareAction::class)]
final class ShareActionTest extends TestCase
{
    #[TestDox('ShareAction class is concrete')]
    public function testShareActionClassIsConcrete()
    {
        $class = new \ReflectionClass(ShareAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ShareAction is a CommunicateAction')]
    public function testShareActionIsCommunicateAction(): void
    {
        $this->assertInstanceOf(Action::class, new ShareAction());
        $this->assertInstanceOf(InteractAction::class, new ShareAction());

        $this->assertInstanceOf(
            CommunicateAction::class,
            new ShareAction(),
            'ShareAction class hierarchy is Thing > Action > InteractAction > CommunicateAction > ShareAction.'
        );
    }

    #[Group('hmvc')]
    #[TestDox('ShareAction is JSON serializable')]
    public function testShareActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ShareAction());
    }

    #[Group('hmvc')]
    #[TestDox('ShareAction is stringable')]
    public function testShareActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new ShareAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ShareAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ShareAction::VERSION);
        $this->assertIsString(ShareAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ShareAction::VERSION, '0.1.0', '>=')
        );
    }
}
