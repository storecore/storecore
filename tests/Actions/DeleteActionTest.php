<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Movie;
use StoreCore\CRM\Person;
use StoreCore\Types\ItemList;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\Actions\Action::class)]
#[UsesClass(\StoreCore\Actions\UpdateAction::class)]
#[UsesClass(\StoreCore\Actions\DeleteAction::class)]
#[UsesClass(\StoreCore\CMS\Movie::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Types\ItemList::class)]
final class DeleteActionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DeleteAction class is concrete')]
    public function testDeleteActionClassIsConcrete(): void
    {
        $class = new ReflectionClass(DeleteAction::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('DeleteAction is an Action')]
    public function testDeleteActionIsAction(): void
    {
        $this->assertInstanceOf(Action::class, new DeleteAction());
    }

    #[Depends('testDeleteActionIsAction')]
    #[Group('hmvc')]
    #[TestDox('DeleteAction is an UpdateAction')]
    public function testDeleteActionIsUpdateAction(): void
    {
        $this->assertInstanceOf(UpdateAction::class, new DeleteAction());
    }


    #[Group('hmvc')]
    #[TestDox('DeleteAction is JSON serializable')]
    public function testDeleteActionIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new DeleteAction());
    }

    #[Group('hmvc')]
    #[TestDox('DeleteAction is stringable')]
    public function testDeleteActionIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new DeleteAction());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DeleteAction::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DeleteAction::VERSION);
        $this->assertIsString(DeleteAction::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DeleteAction::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://schema.org/DeleteAction#eg-0164
     *      Schema.org `DeleteAction` Example 1
     *
     * ```json
     * {
     *   "@context": "https://schema.org",
     *   "@type": "DeleteAction",
     *   "agent": {
     *     "@type": "Person",
     *     "name": "John"
     *   },
     *   "object": {
     *     "@type": "Movie",
     *     "name": "The Internship"
     *   },
     *   "targetCollection": {
     *     "@type": "ItemList",
     *     "name": "List of my favorite movies",
     *     "url": "http://netflix.com/john/favorite"
     *   }
     * }
     * ```
     */
    #[TestDox('John deleted The Internship from his movie queue')]
    public function testJohnDeletedTheInternshipFromHisMovieQueue(): void
    {
        $action = new DeleteAction();
        $action->agent = new Person('John');
        $action->object = new Movie('The Internship');
        $action->targetCollection = new ItemList('List of my favorite movies');
        $action->targetCollection->url = 'http://netflix.com/john/favorite';

        $json = (string) $action;

        // John …
        $this->assertStringContainsString('"agent":{"@type":"Person","name":"John"}', $json);
        // … deleted …
        $this->assertStringStartsWith('{"@context":"https://schema.org","@type":"DeleteAction"', $json);
        // … The Internship …
        $this->assertStringContainsString('"object":{"@type":"Movie","name":"The Internship"', $json);
        // … from his movie queue.
        $this->assertStringContainsString('"targetCollection":{"@type":"ItemList","name":"List of my favorite movies","url":"http://netflix.com/john/favorite"}', $json);
    }
}
