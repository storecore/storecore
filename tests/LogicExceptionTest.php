<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\LogicException::class)]
final class LogicExceptionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('LogicException class is concrete')]
    public function testLogicExceptionClassIsConcrete(): void
    {
        $class = new ReflectionClass(LogicException::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('LogicException is a throwable exception')]
    public function testLogicExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(LogicException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));

        $this->expectException(\Exception::class);
        throw new LogicException();
    }

    #[Depends('testLogicExceptionIsThrowableException')]
    #[Group('hmvc')]
    #[TestDox('LogicException is an SPL LogicException')]
    public function testLogicExceptionIsSplLogicException(): void
    {
        $class = new ReflectionClass(\StoreCore\LogicException::class);
        $this->assertTrue($class->isSubclassOf(\LogicException::class));

        $this->expectException(\LogicException::class);
        throw new \StoreCore\LogicException();
    }
}
