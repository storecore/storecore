<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Admin\AccessControl::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
final class AccessControlTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AccessControl class is concrete')]
    public function testAccessControlClassIsConcrete(): void
    {
        $class = new ReflectionClass(AccessControl::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AccessControl is a controller')]
    public function testAccessControlIsController(): void
    {
        $registry = Registry::getInstance();
        $controller = new AccessControl($registry);
        $this->assertInstanceOf(\StoreCore\AbstractController::class, $controller);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AccessControl::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AccessControl::VERSION);
        $this->assertIsString(AccessControl::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AccessControl::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('AccessControl::check() exists')]
    public function testAccessControlCheckExists(): void
    {
        $class = new ReflectionClass(AccessControl::class);
        $this->assertTrue($class->hasMethod('check'));
    }

    #[Depends('testAccessControlCheckExists')]
    #[TestDox('AccessControl::check() is public')]
    public function testAccessControlCheckIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControl::class, 'check');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAccessControlCheckExists')]
    #[TestDox('AccessControl::check() has no parameters')]
    public function testAccessControlCheckHasNoParameters(): void
    {
        $method = new ReflectionMethod(AccessControl::class, 'check');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
