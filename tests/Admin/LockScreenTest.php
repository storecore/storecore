<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Admin\LockScreen::class)]
final class LockScreenTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Lock screen class exists')]
    public function testLockScreenClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR .  'LockScreen.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR .  'LockScreen.php');

        $this->assertTrue(class_exists('\\StoreCore\\Admin\\LockScreen'));
        $this->assertTrue(class_exists(\StoreCore\Admin\LockScreen::class));
    }

    #[Group('distro')]
    #[TestDox('Lock screen template exists')]
    public function testLockScreenTemplateExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR .  'LockScreen.phtml');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR .  'LockScreen.phtml');
    }


    #[Group('hmvc')]
    #[TestDox('Lock screen class is a controller')]
    public function testLockScreenClassIsController(): void
    {
        $class = new ReflectionClass(LockScreen::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LockScreen::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LockScreen::VERSION);
        $this->assertIsString(LockScreen::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LockScreen::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('LockScreen::__construct exists')]
    public function testLockScreenConstructorExists(): void
    {
        $class = new ReflectionClass(LockScreen::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testLockScreenConstructorExists')]
    #[TestDox('LockScreen::__construct is public constructor')]
    public function testLockScreenConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(LockScreen::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testLockScreenConstructIsPublicConstructor')]
    #[TestDox('LockScreen::__construct has one REQUIRED parameter')]
    public function testLockScreenConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(LockScreen::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
