<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Admin\Installer::class)]
final class InstallerTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Installer class exists')]
    public function testInstallerClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Installer.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Installer.php');

        $this->assertTrue(class_exists('\\StoreCore\\Admin\\Installer'));
        $this->assertTrue(class_exists(\StoreCore\Admin\Installer::class));
    }


    #[Group('hmvc')]
    #[TestDox('Installer is a controller')]
    public function testInstallerIsController(): void
    {
        $class = new ReflectionClass(Installer::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }

    #[Group('hmvc')]
    #[TestDox('Installer class is concrete')]
    public function testInstallerClassIsConcrete(): void
    {
        $class = new ReflectionClass(Installer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Installer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Installer::VERSION);
        $this->assertIsString(Installer::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Installer::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('Installer::__construct() exists')]
    public function testInstallerConstructorExists(): void
    {
        $class = new ReflectionClass(Installer::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testInstallerConstructorExists')]
    #[TestDox('Installer::__construct() is public constructor')]
    public function testInstallerConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Installer::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testInstallerConstructorExists')]
    #[TestDox('Installer::__construct() has one REQUIRED parameter')]
    public function testInstallerConstructHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Installer::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Installer::__destruct() exists')]
    public function testInstallerDestructorExists(): void
    {
        $class = new ReflectionClass(Installer::class);
        $this->assertTrue($class->hasMethod('__destruct'));
    }

    #[Depends('testInstallerDestructorExists')]
    #[TestDox('Installer::__destruct() is public destructor')]
    public function testInstallerDestructIsPublicDestructor(): void
    {
        $method = new ReflectionMethod(Installer::class, '__destruct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isDestructor());
    }
}
