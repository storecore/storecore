<?php

declare(strict_types=1);

namespace StoreCore\Admin;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \ctype_graph;
use function \ctype_lower;
use function \ctype_print;
use function \ctype_upper;
use function \strlen;

#[CoversClass(\StoreCore\Admin\RandomPassword::class)]
#[Group('security')]
final class RandomPasswordTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Random password class file exists')]
    public function testRandomPasswordClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'RandomPassword.php'
        );
    }

    #[Depends('testRandomPasswordClassFileExists')]
    #[Group('distro')]
    #[TestDox('Random password class file is readable')]
    public function testRandomPasswordClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'RandomPassword.php'
        );
    }

    #[Group('hmvc')]
    #[TestDox('RandomPassword class is concrete')]
    public function testRandomPasswordClassIsConcrete(): void
    {
        $class = new ReflectionClass(RandomPassword::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[TestDox('RandomPassword is stringable')]
    public function testRandomPasswordIsStringable(): void
    {
        $class = new ReflectionClass(RandomPassword::class);
        $this->assertTrue($class->isSubclassOf(\Stringable::class));
        $this->assertTrue($class->hasMethod('__toString'));

        $password = new RandomPassword();
        $this->assertInstanceOf(\Stringable::class, $password);
        $this->assertNotEmpty((string) $password);
        $this->assertIsString((string) $password);
    }

    #[Depends('testRandomPasswordIsStringable')]
    #[TestDox('Random password is displayed only once')]
    public function testRandomPasswordIsDisplayedOnlyOnce(): void
    {
        $password = new RandomPassword();
        $this->assertNotEquals(
            (string) $password,
            (string) $password,
            'If the password was displayed as a string, it cannot be used again.'
        );
    }

    #[Depends('testRandomPasswordIsStringable')]
    #[TestDox('RandomPassword::__construct() determines password length')]
    public function testRandomPasswordConstructorDeterminesPasswordLength(): void
    {
        $class = new ReflectionClass(RandomPassword::class);
        $this->assertTrue($class->hasMethod('__construct'));

        $password = new RandomPassword(14);
        $this->assertEquals(14, strlen((string) $password));

        $password = new RandomPassword(20);
        $this->assertEquals(20, strlen((string) $password));
    }

    #[Depends('testRandomPasswordIsStringable')]
    #[Depends('testRandomPasswordConstructorDeterminesPasswordLength')]
    #[TestDox('Minimum password length is 12 characters')]
    public function testMinimumPasswordLengthIs12Characters(): void
    {
        $password = new RandomPassword();
        $this->assertEquals(12, strlen((string) $password));


        $password = new RandomPassword(7);
        $this->assertNotEquals(7, strlen((string) $password));
        $this->assertEquals(12, strlen((string) $password));

        $password = new RandomPassword(11);
        $this->assertNotEquals(11, strlen((string) $password));
        $this->assertEquals(12, strlen((string) $password));


        $password = new RandomPassword(13);
        $this->assertNotEquals(12, strlen((string) $password));
        $this->assertEquals(13, strlen((string) $password));
    }

    #[Depends('testRandomPasswordIsStringable')]
    #[TestDox('Random password contains uppercase and lowercase letters')]
    public function testRandomPasswordContainsUppercaseAndLowercaseLetters(): void
    {
        $password = new RandomPassword();
        $password = (string) $password;

        $this->assertTrue(ctype_graph($password));
        $this->assertTrue(ctype_print($password));
        $this->assertFalse(ctype_lower($password));
        $this->assertFalse(ctype_upper($password));
    }
}
