<?php

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Admin\SignIn::class)]
#[Group('security')]
final class SignInTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SignIn template file exists')]
    public function testSignInTemplateFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'SignIn.phtml'
        );
    }

    #[Depends('testSignInTemplateFileExists')]
    #[Group('distro')]
    #[TestDox('SignIn template file is readable')]
    public function testSignInTemplateFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'SignIn.phtml'
        );
    }


    #[Group('hmvc')]
    #[TestDox('SignIn class is a controller')]
    public function testSignInClassIsController(): void
    {
        $class = new ReflectionClass(SignIn::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }

    #[Group('hmvc')]
    #[TestDox('SignIn class is concrete')]
    public function testSignInClassIsConcrete(): void
    {
        $class = new ReflectionClass(SignIn::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SignIn::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SignIn::VERSION);
        $this->assertIsString(SignIn::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SignIn::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('SignIn::__construct exists')]
    public function testSignInConstructExists(): void
    {
        $class = new ReflectionClass(SignIn::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('SignIn::__construct is public constructor')]
    public function testSignInConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(SignIn::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('SignIn::__construct has one REQUIRED parameter')]
    public function testSignInConstructHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(SignIn::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('SignIn::getToken exists')]
    public function testSignInGetTokenExists(): void
    {
        $class = new ReflectionClass(SignIn::class);
        $this->assertTrue($class->hasMethod('getToken'));
    }

    #[Depends('testSignInGetTokenExists')]
    #[TestDox('SignIn::getToken is public')]
    public function testSignInGetTokenIsPublic(): void
    {
        $method = new ReflectionMethod(SignIn::class, 'getToken');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testSignInGetTokenExists')]
    #[TestDox('SignIn::getToken has no parameters')]
    public function testSignInGetTokenHasNoParameters(): void
    {
        $method = new ReflectionMethod(SignIn::class, 'getToken');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
