<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Admin\User::class)]
#[Group('security')]
final class UserTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('User class exists')]
    public function testUserClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'User.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'User.php');

        $this->assertTrue(class_exists('\\StoreCore\\Admin\\User'));
        $this->assertTrue(class_exists(User::class));
    }

    #[Group('hmvc')]
    #[TestDox('User class is concrete')]
    public function testUserClassIsConcrete(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Admin User is a StoreCore User')]
    public function testAdminUserIsStoreCoreUser(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\User::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(User::VERSION);
        $this->assertIsString(User::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(User::VERSION, '0.3.1', '>=')
        );
    }
}
