<?php

declare(strict_types=1);

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(Document::class)]
final class DocumentTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Admin\Document class file exists')]
    public function testAdminDocumentClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Document.php'
        );
    }

    #[Depends('testAdminDocumentClassFileExists')]
    #[Group('distro')]
    #[TestDox('Admin\Document class file is readable')]
    public function testAdminDocumentClassFileIsReadable(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Document.php'
        );
    }

    #[Group('hmvc')]
    #[TestDox('Document class is concrete')]
    public function testDocumentClassIsConcrete(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Document extends PHP DOMDocument')]
    public function testDocumentExtendsPhpDomdocument(): void
    {
        $this->assertInstanceOf(\DOMDocument::class, new Document());
    }

    #[TestDox('Document is stringable')]
    public function testDocumentIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new Document());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Document::VERSION);
        $this->assertIsString(Document::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Document::VERSION, '0.3.0', '>=')
        );
    }


    #[TestDox('Document::__toString() exists')]
    public function testDocumentToStringExists(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testDocumentToStringExists')]
    #[TestDox('Document::__toString() returns non-empty string')]
    public function testDocumentToStringReturnsNonEmptyString(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertNotEmpty($document);
        $this->assertIsString($document);
    }

    #[Depends('testDocumentToStringReturnsNonEmptyString')]
    #[TestDox('Document::__toString() returns HTML document')]
    public function testDocumentToStringReturnsHtmlDocument(): void
    {
        $document = new Document();
        $document = (string) $document;

        $this->assertNotEmpty($document);
        $this->assertStringStartsWith( '<!DOCTYPE html>', $document);
        $this->assertStringStartsWith( '<!DOCTYPE html><html>', $document);
        $this->assertStringStartsWith( '<!DOCTYPE html><html><head>', $document);
        $this->assertStringContainsString('</head>', $document);
        $this->assertStringContainsString('</body>', $document);
        $this->assertStringEndsWith('</html>', $document);
    }


    #[TestDox('Default document title is project name')]
    public function testDefaultDocumentTitleIsProjectName(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<title>StoreCore</title>', $document);
    }

    #[Group('seo')]
    #[TestDox('Robots MUST NOT index admin pages')]
    public function testRobotsMustNotIndexAdminPages(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<meta name="robots" content="noindex,nofollow">', $document);
    }

    #[TestDox('Admin document uses Roboto fonts from Google CDN')]
    public function testAdminDocumentUsesRobotoFontsFromGoogleCDN(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;900&amp;display=swap" rel="stylesheet">', $document);
    }

    #[TestDox('Admin document uses Material Icons from Google CDN')]
    public function testAdminDocumentUsesMaterialIconsFromGoogleCDN(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">', $document);
    }

    #[TestDox('Admin document uses minified Material admin CSS from assets')]
    public function testAdminDocumentUsesMinifiedMaterialAdminCssFromAssets(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<link href="/admin/admin.min.css" rel="stylesheet">', $document);
    }

    #[TestDox('Admin document contains MDC Typography body tag')]
    public function testAdminDocumentContainsMdcTypographyBodyTag(): void
    {
        $document = new Document();
        $document = (string) $document;
        $this->assertStringContainsString('<body class="mdc-typography"', $document);
        $this->assertStringContainsString('</head><body class="mdc-typography"', $document);
    }


    #[TestDox('Document::setTitle() exists')]
    public function testDocumentSetTitleExists(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertTrue($class->hasMethod('setTitle'));
    }

    #[Depends('testDocumentSetTitleExists')]
    #[TestDox('Document::setTitle() is public')]
    public function testDocumentSetTitleIsPublic(): void
    {
        $method = new ReflectionMethod(Document::class, 'setTitle');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testDocumentSetTitleExists')]
    #[TestDox('Document::setTitle() has one REQUIRED parameter')]
    public function testDocumentSetTitleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Document::class, 'setTitle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testDocumentSetTitleHasOneRequiredParameter')]
    #[TestDox('Document::setTitle() throws value error on empty string')]
    public function testDocumentSetTitleThrowsValueErrorOnEmptyString(): void
    {
        $title = (string) null;
        $this->assertEmpty($title);
        $this->assertIsString($title);

        $document = new Document();
        $this->expectException(\ValueError::class);
        $document->setTitle($title);
    }

    #[Depends('testDocumentSetTitleHasOneRequiredParameter')]
    #[TestDox('Document::setTitle() strips HTML tags from title')]
    public function testDocumentSetTitleStripsHtmlTagsFromTitle(): void
    {
        $title = '<h1><strong></strong></h1>';
        $this->assertNotEmpty($title);
        $this->assertIsString($title);

        $document = new Document();
        $this->expectException(\ValueError::class);
        $document->setTitle($title);
    }


    #[TestDox('Document::write() exists')]
    public function testDocumentWriteExists(): void
    {
        $class = new ReflectionClass(Document::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testDocumentWriteExists')]
    #[TestDox('Document::write() is public')]
    public function testDocumentWriteIsPublic(): void
    {
        $method = new ReflectionMethod(Document::class, 'write');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testDocumentWriteExists')]
    #[TestDox('Document::write() has one REQUIRED parameter')]
    public function testDocumentWriteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Document::class, 'write');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
