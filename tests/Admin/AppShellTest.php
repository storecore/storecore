<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use StoreCore\Registry;
use StoreCore\User;
use StoreCore\Session;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Admin\AppShell::class)]
#[UsesClass(\StoreCore\DependencyException::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\User::class)]
final class AppShellTest extends TestCase
{
    #[TestDox('App shell class is concrete')]
    public function testAppShellClassIsConcrete(): void
    {
        $class = new ReflectionClass(AppShell::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('App shell template file exists')]
    public function testAppShellTemplateFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'AppShell.phtml'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'AppShell.phtml'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AppShell::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AppShell::VERSION);
        $this->assertIsString(AppShell::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AppShell::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('App shell throws dependency logic exception on missing user')]
    public function testAppShellThrowsDependencyLogicExceptionOnMissingUser(): void
    {
        $registry = Registry::getInstance();
        $this->assertFalse($registry->has('User'));
        $this->expectException(\LogicException::class);
        $this->expectException(\StoreCore\DependencyException::class);
        $app_shell = new AppShell($registry);
    }

    #[Depends('testAppShellThrowsDependencyLogicExceptionOnMissingUser')]
    #[Group('hmvc')]
    #[TestDox('App shell throws dependency logic exception on missing session')]
    public function testAppShellThrowsDependencyLogicExceptionOnMissingSession(): void
    {
        $registry = Registry::getInstance();
        $user = new User();
        $registry->set('User', $user);
        $this->assertTrue($registry->has('User'));
        $this->assertFalse($registry->has('Session'));

        $this->expectException(\LogicException::class);
        $this->expectException(\StoreCore\DependencyException::class);
        $app_shell = new AppShell($registry);
    }
}
