<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Admin\ManifestController::class)]
final class ManifestControllerTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ManifestController class exists')]
    public function testManifestControllerClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'ManifestController.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'ManifestController.php');

        $this->assertTrue(class_exists('\\StoreCore\\Admin\\ManifestController'));
        $this->assertTrue(class_exists(ManifestController::class));
    }

    #[Group('hmvc')]
    #[TestDox('ManifestController is a controller')]
    public function testManifestControllerIsController(): void
    {
        $class = new ReflectionClass(ManifestController::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }

    #[Group('hmvc')]
    #[TestDox('ManifestController class is concrete')]
    public function testManifestControllerClassIsConcrete(): void
    {
        $class = new ReflectionClass(ManifestController::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ManifestController::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ManifestController::VERSION);
        $this->assertIsString(ManifestController::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ManifestController::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('ManifestController::__construct() exists')]
    public function testManifestControllerConstructorExists(): void
    {
        $class = new ReflectionClass(ManifestController::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testManifestControllerConstructorExists')]
    #[TestDox('ManifestController::__construct() is public constructor')]
    public function testManifestControllerConstructIsPublicConstructor()
    {
        $method = new ReflectionMethod(ManifestController::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testManifestControllerConstructorExists')]
    #[TestDox('ManifestController::__construct() has one REQUIRED parameter')]
    public function testManifestControllerConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(ManifestController::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
