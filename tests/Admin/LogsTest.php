<?php

declare(strict_types=1);

namespace StoreCore\Admin;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[Group('security')]
#[CoversClass(\StoreCore\Admin\Logs::class)]
final class LogsTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Logs class file exists')]
    public function testLogsClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Logs.php'
        );
    }

    #[Depends('testLogsClassFileExists')]
    #[Group('distro')]
    #[TestDox('Logs class file is readable')]
    public function testLogsClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Logs.php'
        );
    }


    #[Group('hmvc')]
    #[TestDox('Logs class is concrete')]
    public function testLogsClassIsConcrete(): void
    {
        $class = new ReflectionClass(Logs::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Logs class is an admin controller')]
    public function testLogsClassIsAdminController(): void
    {
        $class = new ReflectionClass(Logs::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Admin\Controller::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Logs::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Logs::VERSION);
        $this->assertIsString(Logs::VERSION);
    }

    #[Group('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Logs::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('Logs::download() exists')]
    public function testLogsDownloadExists(): void
    {
        $class = new ReflectionClass(Logs::class);
        $this->assertTrue($class->hasMethod('download'));
    }

    #[Depends('testLogsDownloadExists')]
    #[TestDox('Logs::download() exists')]
    public function testLogsDownloadIsPublic(): void
    {
        $method = new ReflectionMethod(Logs::class, 'download');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isFinal());
    }

    #[Depends('testLogsDownloadExists')]
    #[TestDox('Logs::download() has no parameters')]
    public function testLogsDownloadHasNoParameters(): void
    {
        $method = new ReflectionMethod(Logs::class, 'download');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
