<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018, 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Registry;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Admin\ManifestModel::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
final class ManifestModelTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ManifestModel extends AbstractModel')]
    public function testManifestModelClassExtendsAbstractModel(): void
    {
        $registry = Registry::getInstance();
        $model = new ManifestModel($registry);
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $model);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ManifestModel::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ManifestModel::VERSION);
        $this->assertIsString(ManifestModel::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ManifestModel::VERSION, '0.2.0', '>=')
        );
    }
}
