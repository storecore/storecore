<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Admin\Controller::class)]
#[CoversClass(\StoreCore\User::class)]
#[Group('security')]
#[UsesClass(\StoreCore\Registry::class)]
final class ControllerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Admin controller class is concrete')]
    public function testAdminControllerClassIsConcrete(): void
    {
        $class = new ReflectionClass(Controller::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Admin controller is a controller')]
    public function testAdminControllerIsController(): void
    {
        $class = new ReflectionClass(Controller::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Controller::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Controller::VERSION);
        $this->assertIsString(Controller::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Controller::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Controller::__construct() exists')]
    public function testControllerConstructorExists(): void
    {
        $class = new ReflectionClass(Controller::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Controller::__construct() is public constructor')]
    public function testControllerConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Controller::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Controller::__construct() has one REQUIRED parameter')]
    public function testControllerConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Controller::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Controller::__construct() throws StoreCore\DependencyException on invalid User')]
    public function testControllerConstructorThrowsStoreCoreDependencyExceptionOnInvalidUser(): void
    {
        $registry = Registry::getInstance();
        $registry->set('User', new \StoreCore\User());
        $this->assertInstanceOf(\StoreCore\User::class, $registry->get('User'));
        $this->assertNotInstanceOf(\StoreCore\Admin\User::class, $registry->get('User'));

        $this->expectException(\StoreCore\DependencyException::class);
        $failed_controller = new \StoreCore\Admin\Controller($registry);
    }
}
