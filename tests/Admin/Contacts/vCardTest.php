<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin\Contacts;

use \StoreCore\CRM\Person;
use \StoreCore\CRM\Organization;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Admin\Contacts\vCard::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
final class vCardTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('vCard class is concrete')]
    public function testVCardClassIsConcrete(): void
    {
        $class = new ReflectionClass(vCard::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('vCard is stringable')]
    public function testVCardIsStringable(): void
    {
        $class = new ReflectionClass(vCard::class);
        $this->assertTrue($class->implementsInterface(\Stringable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(vCard::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(vCard::VERSION);
        $this->assertIsString(vCard::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(vCard::VERSION, '0.1.1', '>=')
        );
    }


    #[TestDox('vCard::__construct() exists')]
    public function testVCardConstructorExists(): void
    {
        $class = new ReflectionClass(vCard::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testVCardConstructorExists')]
    #[TestDox('vCard::__construct() is public constructor')]
    public function testVCardConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(vCard::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testVCardConstructorExists')]
    #[TestDox('vCard::__construct has two parameters')]
    public function testVCardConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(vCard::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testVCardConstructorHasTwoParameters')]
    #[TestDox('vCard::__construct has one REQUIRED parameter')]
    public function testVCardConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(vCard::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('vCard::__toString() exists')]
    public function testVCardToStringExists(): void
    {
        $class = new ReflectionClass(vCard::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testVCardToStringExists')]
    #[TestDox('vCard::__toString() returns non-empty string')]
    public function testVCardToStringReturnsNonEmptyString(): void
    {
        $person = new Person;
        $person->givenName = 'John';
        $person->familyName = 'Doe';

        $vcard = new vCard($person);

        $this->assertNotEmpty((string) $vcard);
        $this->assertIsString((string) $vcard);

        $this->assertStringStartsWith('BEGIN:VCARD', (string) $vcard);
        $this->assertStringContainsString('VERSION:4.0', (string) $vcard);
        $this->assertStringEndsWith('END:VCARD', (string) $vcard);
    }

    #[Depends('testVCardToStringReturnsNonEmptyString')]
    #[TestDox('vCard::__toString() returns name (N)')]
    public function testVCardToStringReturnsName(): void
    {
        $person = new Person();
        $person->givenName = 'John';
        $person->familyName = 'Doe';

        $vcard = new vCard($person);
        $this->assertStringContainsString('N:Doe;John;;;', (string) $vcard);
    }

    #[Depends('testVCardToStringReturnsNonEmptyString')]
    #[TestDox('vCard::__toString() returns non-empty full name (FN)')]
    public function testVCardToStringReturnsNonEmptyFullName(): void
    {
        $person = new Person('John Doe');
        $vcard = new vCard($person);

        // vCard does not parse the display name …
        $this->assertStringNotContainsString('N:Doe;John;;;', (string) $vcard);
        // … but does return it.
        $this->assertStringContainsString('FN:John Doe', (string) $vcard);
    }
}
