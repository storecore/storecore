<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin\Settings;

use StoreCore\Registry;
use StoreCore\DependencyException;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(Database::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\User::class)]
#[UsesClass(\StoreCore\DependencyException::class)]
final class DatabaseTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Database template file exists')]
    public function testDatabaseTemplateFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'Database.phtml'
        );
    }

    #[Depends('testDatabaseTemplateFileExists')]
    #[Group('distro')]
    #[TestDox('Database template file is readable')]
    public function testDatabaseTemplateFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'Database.phtml'
        );
    }


    #[Group('hmvc')]
    #[Group('security')]
    #[TestDox('Database is an admin controller')]
    public function testDatabaseIsAdminController(): void
    {
        $class = new ReflectionClass(Database::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
        $this->assertTrue($class->isSubclassOf(\StoreCore\Admin\Controller::class));
    }

    #[Group('hmvc')]
    #[TestDox('Database class is concrete')]
    public function testDatabaseClassIsConcrete(): void
    {
        $class = new ReflectionClass(Database::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Database::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Database::VERSION);
        $this->assertIsString(Database::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Database::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Database::__construct() exists')]
    public function testDatabaseConstructorExists(): void
    {
        $class = new ReflectionClass(Database::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testDatabaseConstructorExists')]
    #[TestDox('Database::__construct() is public')]
    public function testDatabaseConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Database::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testDatabaseConstructorExists')]
    #[TestDox('Database::__construct() has one REQUIRED parameter')]
    public function testDatabaseConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Database::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testDatabaseConstructorExists')]
    #[TestDox('Database::__construct() requires registered admin User')]
    public function testDatabaseConstructorRequiresRegisteredAdminUser(): void
    {
        $registry = Registry::getInstance();
        $registry->set('User', new \StoreCore\User());

        $this->expectException(DependencyException::class);
        $failure = new Database($registry);
    }
}
