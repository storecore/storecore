<?php

declare(strict_types=1);

namespace StoreCore\Admin\Settings;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Admin\Settings\Configurator::class)]
final class ConfiguratorTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Configurator class file exists')]
    public function testConfiguratorClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'Configurator.php'
        );
    }

    #[Depends('testConfiguratorClassFileExists')]
    #[Group('distro')]
    #[TestDox('Configurator class file is readable')]
    public function testConfiguratorClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'Configurator.php'
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Configurator::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Configurator::VERSION);
        $this->assertIsString(Configurator::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Configurator::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Configurator::set() exists')]
    public function testConfiguratorSetExists(): void
    {
        $class = new ReflectionClass(Configurator::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testConfiguratorSetExists')]
    #[TestDox('Configurator::set() is public')]
    public function testConfiguratorSetIsPublic(): void
    {
        $method = new ReflectionMethod(Configurator::class, 'set');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testConfiguratorSetExists')]
    #[TestDox('Configurator::set() has two REQUIRED parameters')]
    public function testConfiguratorSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Configurator::class, 'set');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Configurator::write() exists')]
    public function testConfiguratorWriteExists()
    {
        $class = new ReflectionClass(Configurator::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testConfiguratorSetExists')]
    #[TestDox('Configurator::write() is public static')]
    public function testConfiguratorWriteIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Configurator::class, 'write');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testConfiguratorSetExists')]
    #[TestDox('Configurator::write() has two REQUIRED parameters')]
    public function testConfiguratorWriteHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Configurator::class, 'write');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }
}
