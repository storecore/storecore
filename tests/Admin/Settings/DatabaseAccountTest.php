<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin\Settings;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Admin\Settings\DatabaseAccount::class)]
final class DatabaseAccountTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DatabaseAccount class exists')]
    public function testDatabaseAccountClassExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'DatabaseAccount.php'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'DatabaseAccount.php'
        );

        $this->assertTrue(class_exists('\\StoreCore\\Admin\\Settings\\DatabaseAccount'));
        $this->assertTrue(class_exists(\StoreCore\Admin\Settings\DatabaseAccount::class));
    }


    #[Group('distro')]
    #[TestDox('DatabaseAccount template file exists')]
    public function testDatabaseAccountTemplateFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'DatabaseAccount.phtml'
        );

        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'Settings' . DIRECTORY_SEPARATOR . 'DatabaseAccount.phtml'
        );
    }


    #[Group('hmvc')]
    #[TestDox('DatabaseAccount is a controller')]
    public function testDatabaseAccountIsController(): void
    {
        $class = new ReflectionClass(DatabaseAccount::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }

    #[Group('hmvc')]
    #[TestDox('DatabaseAccount class is concrete')]
    public function testDatabaseAccountClassIsConcrete(): void
    {
        $class = new ReflectionClass(DatabaseAccount::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DatabaseAccount::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DatabaseAccount::VERSION);
        $this->assertIsString(DatabaseAccount::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DatabaseAccount::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('DatabaseAccount::__construct exists')]
    public function testDatabaseAccountConstructorExists(): void
    {
        $class = new ReflectionClass(DatabaseAccount::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testDatabaseAccountConstructorExists')]
    #[TestDox('DatabaseAccount::__construct is public constructor')]
    public function testDatabaseAccountConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(DatabaseAccount::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testDatabaseAccountConstructorExists')]
    #[TestDox('DatabaseAccount::__construct has one REQUIRED parameter')]
    public function testDatabaseAccountConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(DatabaseAccount::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
