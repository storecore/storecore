<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\AbstractModel::class)]
#[Group('hmvc')]
final class AbstractModelTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractModel class exists')]
    public function testAbstractModelClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractModel.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractModel.php');

        $this->assertTrue(class_exists('\\StoreCore\\AbstractModel'));
        $this->assertTrue(class_exists(AbstractModel::class));
    }

    #[TestDox('AbstractModel class is abstract')]
    public function testAbstractModelClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractModel::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }
}
