<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractCacheExceptionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractCacheException class exists')]
    public function testAbstractCacheExceptionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractCacheException.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractCacheException.php');

        $this->assertTrue(class_exists('\\StoreCore\\AbstractCacheException'));
        $this->assertTrue(class_exists(\StoreCore\AbstractCacheException::class));
    }

    #[TestDox('AbstractCacheException is abstract')]
    public function testAbstractCacheExceptionIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractCacheException::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('Implemented PSR-16 CacheException exists')]
    public function testImplementedPsr16CacheExceptionExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheException::class));
    }

    #[Depends('testImplementedPsr16CacheExceptionExists')]
    #[TestDox('AbstractCacheException is a throwable PSR-16 CacheException')]
    public function testAbstractCacheExceptionIsThrowablePSR16CacheException(): void
    {
        $class = new ReflectionClass(AbstractCacheException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Psr\SimpleCache\CacheException::class));
    }
}
