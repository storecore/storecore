<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2017, 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\Request::class)]
#[CoversClass(\StoreCore\Engine\Message::class)]
#[UsesClass(\StoreCore\Engine\ServerContainer::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
final class RequestTest extends TestCase
{
    /**
     * @var array $serverParams
     *   Temporary copy of the superglobal $_SERVER.
     */
    private array $serverParams;

    /**
     * Sets up a generic HTTP GET request.
     */
    protected function setUp(): void
    {
        $this->serverParams = $_SERVER;
        $_SERVER = [
            'HTTP_HOST'      => 'www.example.com',
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/foo-category/bar-product-name',
            'SERVER_PORT'    => '80',
        ];
    }

    /**
     * Restores the $_SERVER superglobal array.
     */
    protected function tearDown(): void
    {
        $_SERVER = $this->serverParams;
    }


    #[Group('distro')]
    #[TestDox('Request class file exists')]
    public function testRequestClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'Request.php');
    }

    #[Depends('testRequestClassFileExists')]
    #[Group('distro')]
    #[TestDox('Request class file is readable')]
    public function testRequestClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'Request.php'
        );
    }

    #[Group('distro')]
    #[TestDox('Request class is readable')]
    public function testRequestClassIsReadable(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Engine\\Request'));
        $this->assertTrue(class_exists(Request::class));
    }


    #[Group('hmvc')]
    #[TestDox('Request class is concrete')]
    public function testRequestClassIsConcrete(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Request is a Message')]
    public function testRequestIsAMessage(): void
    {
        $request = new Request();
        $this->assertInstanceOf(\StoreCore\Engine\Message::class, $request);
        $this->assertInstanceOf(Message::class, $request);
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 MessageInterface exists')]
    public function testImplementedPSR7MessageInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\MessageInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\MessageInterface::class));
    }

    #[Depends('testRequestClassIsConcrete')]
    #[Depends('testImplementedPSR7MessageInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Request implements PSR-7 MessageInterface')]
    public function testRequestImplementsPSR7MessageInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\MessageInterface::class, new Request());
    }


    #[Depends('testImplementedPSR7MessageInterfaceExists')]
    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 RequestInterface exists')]
    public function testImplementedPSR7RequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\RequestInterface::class));
    }

    #[Depends('testRequestClassIsConcrete')]
    #[Depends('testImplementedPSR7RequestInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Request implements PSR-7 RequestInterface')]
    public function testRequestImplementsPSR7RequestInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, new Request());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Request::VERSION);
        $this->assertIsString(Request::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Request::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Request::getMethod() exists')]
    public function testRequestGetMethodExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('getMethod'));
    }

    #[Depends('testRequestGetMethodExists')]
    #[TestDox('Request::getMethod() is public')]
    public function testRequestGetMethodIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'getMethod');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRequestGetMethodExists')]
    #[TestDox('Request::getMethod() has no parameters')]
    public function testRequestGetMethodHasNoParameters(): void
    {
        $method = new ReflectionMethod(Request::class, 'getMethod');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRequestGetMethodHasNoParameters')]
    #[TestDox('Request::getMethod() returns non-empty string')]
    public function testRequestGetMethodReturnsNonEmptyString(): void
    {
        $request = new Request();
        $this->assertNotEmpty($request->getMethod());
        $this->assertIsString($request->getMethod());
    }

    #[Depends('testRequestGetMethodReturnsNonEmptyString')]
    #[TestDox('Request::getMethod() returns returns uppercase `GET` by default')]
    public function testRequestGetMethodReturnsUppercaseGetByDefault(): void
    {
        $request = new Request();
        $this->assertTrue(ctype_upper($request->getMethod()));
        $this->assertEquals('GET', $request->getMethod());
    }


    #[TestDox('Request::getRequestTarget() exists')]
    public function testRequestGetRequestTargetExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('getRequestTarget'));
    }

    #[Depends('testRequestGetRequestTargetExists')]
    #[TestDox('Request::getRequestTarget() is public')]
    public function testRequestGetRequestTargetIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'getRequestTarget');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestGetRequestTargetExists')]
    #[TestDox('Request::getRequestTarget() has no parameters')]
    public function testRequestGetRequestTargetHasNoParameters(): void
    {
        $method = new ReflectionMethod(Request::class, 'getRequestTarget');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRequestGetRequestTargetExists')]
    #[Depends('testRequestGetRequestTargetIsPublic')]
    #[Depends('testRequestGetRequestTargetHasNoParameters')]
    #[TestDox('Request::getRequestTarget() returns non-empty string')]
    public function testRequestGetRequestTargetReturnsNonEmptyString(): void
    {
        $request = new Request();
        $this->assertNotEmpty($request->getRequestTarget());
        $this->assertIsString($request->getRequestTarget());
    }

    #[Depends('testRequestGetRequestTargetReturnsNonEmptyString')]
    #[TestDox('Request::getRequestTarget() returns `/` if no request target is provided')]
    public function testRequestGetRequestTargetReturnSlashIfNoRequestTargetIsProvided(): void
    {
        $_SERVER['REQUEST_URI'] = null;
        $request = new Request();
        $this->assertEquals('/', $request->getRequestTarget());
    }


    #[TestDox('Request::getUri() exists')]
    public function testRequestGetUriExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('getUri'));
    }

    #[Depends('testRequestGetUriExists')]
    #[TestDox('Request::getUri() is public')]
    public function testRequestGetUriIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'getUri');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestGetUriExists')]
    #[TestDox('Request::getUri() has no parameters')]
    public function testRequestGetUriHasNoParameters(): void
    {
        $method = new ReflectionMethod(Request::class, 'getUri');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRequestGetUriExists')]
    #[Depends('testRequestGetUriIsPublic')]
    #[Depends('testRequestGetUriHasNoParameters')]
    #[TestDox('Request::getUri() returns PSR-7 UriInterface')]
    public function testRequestGetUriReturnsPsr7UriInterface(): void
    {
        // URI in PHP equals two server variables:
        // $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
        $_SERVER['HTTP_HOST']   = 'www.example.com';
        $_SERVER['REQUEST_URI'] = '/foo-category/bar-product-name';
        $_SERVER['SERVER_PORT'] = '80';

        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\UriInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\UriInterface::class));

        $request = new Request();
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $request->getUri());
    }


    #[TestDox('Request::setMethod() exists')]
    public function testRequestSetMethodExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('setMethod'));
    }

    #[Depends('testRequestSetMethodExists')]
    #[TestDox('Request::setMethod() is public')]
    public function testRequestSetMethodIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'setMethod');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestSetMethodExists')]
    #[TestDox('Request::setMethod() has one REQUIRED parameter')]
    public function testRequestSetMethodHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'setMethod');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestSetMethodHasOneRequiredParameter')]
    #[TestDox('Request::setMethod() is case sensitive')]
    public function testRequestSetMethodIsCaseSensitive(): void
    {
        $request = new Request();

        $request->setMethod('get');
        $this->assertNotEquals('GET', $request->getMethod());

        $request->setMethod('Get');
        $this->assertNotEquals('GET', $request->getMethod());

        $request->setMethod('GET');
        $this->assertEquals('GET', $request->getMethod());
    }

    #[Depends('testRequestSetMethodHasOneRequiredParameter')]
    #[TestDox('Request::setMethod() sets Request::getMethod() return')]
    public function testRequestSetMethodSetsRequestGetMethodReturn(): void
    {
        $methods = array('GET', 'POST', 'PUT', 'PATCH', 'DELETE');
        foreach ($methods as $method) {
            $request = new Request();
            $request->setMethod($method);
            $this->assertEquals($method, $request->getMethod());
        }
    }

    #[Depends('testRequestSetMethodHasOneRequiredParameter')]
    #[TestDox('Request::setMethod() throws invalid argument exception if HTTP method does not exist')]
    public function testRequestSetMethodThrowsInvalidArgumentExceptionIfHttpMethodDoesNotExist(): void
    {
        $request = new Request();
        $this->expectException(\InvalidArgumentException::class);
        $request->setMethod('FOO');
    }


    #[TestDox('HTTP request method GET is supported')]
    public function testHttpRequestMethodGetIsSupported(): void
    {
        $request = new Request();
        $request->setMethod('GET');
        $this->assertEquals('GET', $request->getMethod());
    }

    #[TestDox('HTTP request supports REST API operations')]
    public function testHttpRequestSupportsRestApiOperations(): void
    {
        $request = new Request();

        $request->setMethod('GET');
        $this->assertEquals('GET', $request->getMethod());

        $request->setMethod('POST');
        $this->assertEquals('POST', $request->getMethod());

        $request->setMethod('PUT');
        $this->assertEquals('PUT', $request->getMethod());

        $request->setMethod('PATCH');
        $this->assertEquals('PATCH', $request->getMethod());

        $request->setMethod('DELETE');
        $this->assertEquals('DELETE', $request->getMethod());
    }

    #[TestDox('HTTP request supports HEAD requests')]
    public function testHttpRequestSupportsHeadRequests(): void
    {
        $request = new Request();
        $request->setMethod('HEAD');
        $this->assertEquals('HEAD', $request->getMethod());
    }


    #[TestDox('Request::setRequestTarget exists')]
    public function testRequestSetRequestTargetExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('setRequestTarget'));
    }

    #[Depends('testRequestSetRequestTargetExists')]
    #[TestDox('Request::setRequestTarget is public')]
    public function testRequestSetRequestTargetIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'setRequestTarget');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestSetRequestTargetExists')]
    #[TestDox('Request::setRequestTarget has one REQUIRED parameter')]
    public function testRequestSetRequestTargetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'setRequestTarget');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Request::setUri exists')]
    public function testRequestSetUriExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('setUri'));
    }

    #[Depends('testRequestSetUriExists')]
    #[TestDox('Request::setUri is public')]
    public function testRequestSetUriIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'setUri');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestSetUriExists')]
    #[TestDox('Request::setUri has one REQUIRED parameter')]
    public function testRequestSetUriHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'setUri');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestSetUriExists')]
    #[Depends('testRequestSetUriIsPublic')]
    #[Depends('testRequestSetUriHasOneRequiredParameter')]
    #[TestDox('Request::setUri sets UriInterface')]
    public function testRequestSetUriSetsUriInterface(): void
    {
        $url = 'https://shop.example.com/foo/bar/baz-qux';
        $factory = new UriFactory();
        $uri = $factory->createUri($url);

        $request = new Request();
        $request->setUri($uri);
        $this->assertSame($uri, $request->getUri());
    }


    #[Group('hmvc')]
    #[TestDox('Request::withMethod exists')]
    public function testRequestWithMethodExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('withMethod'));
    }

    #[Depends('testRequestWithMethodExists')]
    #[TestDox('Request::withMethod is public')]
    public function testRequestWithMethodIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'withMethod');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRequestWithMethodExists')]
    #[Group('hmvc')]
    #[TestDox('Request::withMethod has one REQUIRED parameter')]
    public function testRequestWithMethodHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'withMethod');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestWithMethodExists')]
    #[TestDox('Request::withMethod throws invalid argument exception if HTTP method does not exist')]
    public function RequestWithMethodThrowsInvalidArgumentExceptionIfHttpMethodDoesNotExist(): void
    {
        $request = new Request();
        $this->expectException(\InvalidArgumentException::class);
        $instance = $request->withMethod('FOO');
    }

    #[Depends('testRequestWithMethodExists')]
    #[Depends('testRequestWithMethodHasOneRequiredParameter')]
    #[Group('hmvc')]
    #[TestDox('Request::withMethod returns instance of StoreCore\Engine\Request')]
    public function testRequestWithMethodReturnsInstanceOfStoreCoreRequest(): void
    {
        $first_instance = new Request();
        $first_instance->setMethod('POST');
        $second_instance = $first_instance->withMethod('PATCH');

        $this->assertInstanceOf(Request::class, $second_instance);
        $this->assertSame('PATCH', $second_instance->getMethod());
        $this->assertNotSame($second_instance->getMethod(), $first_instance->getMethod());
    }


    #[TestDox('Request::withRequestTarget exists')]
    public function testRequestWithRequestTargetExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('withRequestTarget'));
    }

    #[Depends('testRequestWithRequestTargetExists')]
    #[TestDox('Request::withRequestTarget is public')]
    public function testRequestWithRequestTargetIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'withRequestTarget');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestWithRequestTargetExists')]
    #[TestDox('Request::withRequestTarget has one REQUIRED parameter')]
    public function testRequestWithRequestTargetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'withRequestTarget');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestWithRequestTargetExists')]
    #[Depends('testRequestWithRequestTargetIsPublic')]
    #[Depends('testRequestWithRequestTargetHasOneRequiredParameter')]
    #[TestDox('Request::withRequestTarget returns instance of Request Message')]
    public function testRequestWithRequestTargetReturnsInstanceOfRequestMessage(): void
    {
        $request = new Request();
        $this->assertInstanceOf(Message::class, $request->withRequestTarget('/shopping-cart/shipping-address'));
        $this->assertInstanceOf(Request::class, $request->withRequestTarget('/shopping-cart/shipping-address'));

        $clone = $request->withRequestTarget('/shopping-cart/shipping-address');
        $this->assertNotSame($request, $clone);

        $this->assertNotEquals('/shopping-cart/shipping-address', $request->getRequestTarget());
        $this->assertEquals('/shopping-cart/shipping-address', $clone->getRequestTarget());
    }


    #[TestDox('Request::withUri exists')]
    public function testRequestWithUriExists(): void
    {
        $class = new ReflectionClass(Request::class);
        $this->assertTrue($class->hasMethod('withUri'));
    }

    #[Depends('testRequestWithUriExists')]
    #[TestDox('Request::withUri is public')]
    public function testRequestWithUriIsPublic(): void
    {
        $method = new ReflectionMethod(Request::class, 'withUri');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestWithUriExists')]
    #[TestDox('Request::withUri has two parameters')]
    public function testRequestWithUriHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Request::class, 'withUri');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testRequestWithUriHasTwoParameters')]
    #[TestDox('Request::withUri has one REQUIRED parameter')]
    public function testRequestWithUriHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Request::class, 'withUri');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRequestWithUriHasOneRequiredParameter')]
    #[TestDox('Request::withUri returns instance of PSR-7 RequestInterface')]
    public function testRequestWithUriReturnsInstanceOfPsr7RequestInterface(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUri('https://shop.example.com/foo/bar');
        $request = new Request();
        $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, $request->withUri($uri));
    }

    #[Depends('testRequestWithUriHasOneRequiredParameter')]
    #[TestDox('Request::withUri returns instance of Request Message')]
    public function testRequestWithUriReturnsInstanceOfRequestMessage(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUri('https://shop.example.com/foo/bar');

        $request = new Request();
        $this->assertInstanceOf(Request::class, $request->withUri($uri));
        $this->assertInstanceOf(Message::class, $request->withUri($uri));
    }

    #[Depends('testRequestWithUriHasTwoParameters')]
    #[TestDox('Request::withUri $preserveHost test cases')]
    public function testRequestWithUriPreserveHostTestCases(): void
    {
        $factory = new UriFactory();

        $initial_uri  = $factory->createUri('https://www.example.nl/api/v1/stores');
        $new_uri      = $factory->createUri('https://api.example.com/api/v2/stores');
        $expected_uri = $factory->createUri('https://www.example.nl/api/v2/stores');

        $initial_request = new Request();
        $initial_request->setUri($initial_uri);

        // Replace host
        $delegated_request = $initial_request->withUri($new_uri);
        $this->assertNotSame($initial_request->getUri(), $delegated_request->getUri());
        $this->assertEquals($new_uri, $delegated_request->getUri());

        // Preserve host
        $delegated_request = $initial_request->withUri($new_uri, true);
        $this->assertNotSame($initial_request->getUri(), $delegated_request->getUri());
        $this->assertEquals($expected_uri, $delegated_request->getUri());
        $this->assertEquals((string) $expected_uri, (string) $delegated_request->getUri());
    }
}
