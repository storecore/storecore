<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Engine\Message::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Engine\TemporaryStream::class)]
final class MessageTest extends TestCase
{
    #[TestDox('Message class is concrete')]
    public function testMessageClassIsAbstract(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[TestDox('Implemented PSR-7 MessageInterface exists')]
    public function testImplementedPSR7MessageInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\MessageInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\MessageInterface::class));
    }

    #[Depends('testImplementedPSR7MessageInterfaceExists')]
    #[TestDox('Message implements PSR-7 MessageInterface')]
    public function testMessageImplementsPSR7MessageInterface(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Message\MessageInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Message::VERSION);
        $this->assertIsString(Message::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Message::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Message::addHeader exists')]
    public function testMessageAddHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('addHeader'));
    }

    #[Depends('testMessageAddHeaderExists')]
    #[TestDox('Message::addHeader is public')]
    public function testMessageAddHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'addHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageAddHeaderExists')]
    #[TestDox('Message::addHeader has two REQUIRED parameters')]
    public function testMessageAddHeaderHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'addHeader');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageAddHeaderExists')]
    #[Depends('testMessageAddHeaderIsPublic')]
    #[Depends('testMessageAddHeaderHasTwoRequiredParameters')]
    #[TestDox('Message::addHeader adds new HTTP header')]
    public function testMessageAddHeaderAddsNewHttpHeader(): void
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('Content-Type'));
        $this->assertEmpty( $message->getHeader('Content-Type'));

        $message->addHeader('Content-Type', 'text/html');
        $this->assertTrue($message->hasHeader('Content-Type'));
        $this->assertNotEmpty($message->getHeader('Content-Type'));
        $this->assertCount(1, $message->getHeader('Content-Type'));
    }

    #[Depends('testMessageAddHeaderAddsNewHttpHeader')]
    #[TestDox('Message::addHeader adds values to an existing HTTP header')]
    public function testMessageAddHeaderAddsValuesToAnExistingHttpHeader(): void
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('Allow'));

        $message->addHeader('Allow', 'GET');
        $this->assertTrue($message->hasHeader('Allow'));
        $this->assertCount(1, $message->getHeader('Allow'));

        $message->addHeader('Allow', 'HEAD');
        $this->assertTrue($message->hasHeader('Allow'));
        $this->assertCount(2, $message->getHeader('Allow'));

        $message->addHeader('allow', 'GET');
        $this->assertTrue($message->hasHeader('Allow'));
        $this->assertTrue($message->hasHeader('allow'));
        $this->assertCount(2, $message->getHeader('Allow'), 'Case-insensitive header name MUST NOT add extra header.');
        $this->assertCount(2, $message->getHeader('allow'), 'Case-insensitive header name MUST NOT add extra header.');
    }


    #[TestDox('Message::getBody exists')]
    public function testMessageGetBodyExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('getBody'));
    }

    #[Depends('testMessageGetBodyExists')]
    #[TestDox('Message::getBody is public')]
    public function testMessageGetBodyIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'getBody');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageGetBodyExists')]
    #[TestDox('Message::getBody has no parameters')]
    public function testMessageGetBodyHasNoParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'getBody');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMessageGetBodyExists')]
    #[Depends('testMessageGetBodyIsPublic')]
    #[Depends('testMessageGetBodyHasNoParameters')]
    #[TestDox('Message::getBody returns \Psr\Http\Message\StreamInterface')]
    public function testMessageGetBodyReturnsPsrHttpMessageStreamInterface(): void
    {
        $message = new Message();
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, $message->getBody());
    }


    #[TestDox('Message::getHeader exists')]
    public function testMessageGetHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('getHeader'));
    }

    #[Depends('testMessageGetHeaderExists')]
    #[TestDox('Message::getHeader is public')]
    public function testMessageGetHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageGetHeaderExists')]
    #[TestDox('Message::getHeader has one REQUIRED parameter')]
    public function testMessageGetHeaderHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeader');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageGetHeaderExists')]
    #[Depends('testMessageGetHeaderIsPublic')]
    #[Depends('testMessageGetHeaderHasOneRequiredParameter')]
    #[TestDox('Message::getHeader returns array')]
    public function testMessageGetHeaderReturnsArray(): void
    {
        $message = new Message();
        $this->assertIsArray($message->getHeader('Accept-Language'));
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/#headers-with-multiple-values
     *      “Headers with multiple values” in PSR-7
     */
    #[Depends('testMessageGetHeaderReturnsArray')]
    #[TestDox('Message::getHeader returns multiple header values as array elements')]
    public function testMessageGetHeaderReturnsMultipleHeaderValuesAsArrayElements(): void
    {
        $message = new Message();
        $message = $message
            ->withHeader('foo', 'bar')
            ->withAddedHeader('foo', 'baz');

        $this->assertIsArray($message->getHeader('foo'));
        $this->assertNotEmpty($message->getHeader('foo'));
        $this->assertCount(2, $message->getHeader('foo'));
        $this->assertTrue(in_array('bar', $message->getHeader('foo'), true));
        $this->assertTrue(in_array('baz', $message->getHeader('foo'), true));
    }


    #[TestDox('Message::getHeaderLine exists')]
    public function testMessageGetHeaderLineExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('getHeaderLine'));
    }

    #[Depends('testMessageGetHeaderLineExists')]
    #[TestDox('Message::getHeaderLine is public')]
    public function testMessageGetHeaderLineIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeaderLine');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageGetHeaderLineExists')]
    #[TestDox('Message::getHeaderLine has one REQUIRED parameter')]
    public function testMessageGetHeaderLineHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeaderLine');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageGetHeaderLineExists')]
    #[Depends('testMessageGetHeaderLineIsPublic')]
    #[Depends('testMessageGetHeaderLineHasOneRequiredParameter')]
    #[TestDox('Message::getHeaderLine returns string')]
    public function testMessageGetHeaderLineReturnsString(): void
    {
        $message = new Message();
        $this->assertIsString($message->getHeaderLine('Accept-Language'));
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/#case-insensitive-header-field-names
     *      “Case-insensitive header field names” in PSR-7
     */
    #[Depends('testMessageGetHeaderLineReturnsString')]
    #[TestDox('Message::getHeaderLine is case insensitive')]
    public function testMessageGetHeaderLineIsCaseInsensitive(): void
    {
        $message = new Message();
        $message = $message->withHeader('foo', 'bar');
        $this->assertSame('bar', $message->getHeaderLine('foo'));
        $this->assertSame('bar', $message->getHeaderLine('FOO'));

        $message = $message->withHeader('fOO', 'baz');
        $this->assertSame('baz', $message->getHeaderLine('foo'));
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/#headers-with-multiple-values
     *      “Headers with multiple values” in PSR-7
     */
    #[Depends('testMessageGetHeaderLineReturnsString')]
    #[TestDox('Message::getHeaderLine returns headers with multiple values as one string')]
    public function testMessageGetHeaderLineReturnsHeadersWithMultipleValuesAsOneString(): void
    {
        $message = new Message();
        $message = $message
            ->withHeader('foo', 'bar')
            ->withAddedHeader('foo', 'baz');

        $this->assertIsString($message->getHeaderLine('foo'));
        $this->assertSame('bar,baz', $message->getHeaderLine('foo'));
    }


    #[TestDox('Message::getHeaders exists')]
    public function testMessageGetHeadersExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('getHeaders'));
    }

    #[Depends('testMessageGetHeadersExists')]
    #[TestDox('Message::getHeaders is public')]
    public function testMessageGetHeadersIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeaders');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageGetHeadersExists')]
    #[TestDox('Message::getHeaders has no parameters')]
    public function testMessageGetHeadersHasNoParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'getHeaders');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMessageGetHeadersExists')]
    #[Depends('testMessageGetHeadersIsPublic')]
    #[Depends('testMessageGetHeadersHasNoParameters')]
    #[TestDox('Message::getHeaders returns array')]
    public function testMessageGetHeadersReturnsArray(): void
    {
        $message = new Message();
        $this->assertIsArray($message->getHeaders());
    }


    #[TestDox('Message::$protocolVersion exists')]
    public function testMessageProtocolVersionExists(): void
    {
        $message = new Message();
        $this->assertObjectHasProperty('protocolVersion', $message);
    }

    #[TestDox('Message::getProtocolVersion exists')]
    public function testMessageGetProtocolVersionExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('getProtocolVersion'));
    }

    #[Depends('testMessageGetProtocolVersionExists')]
    #[TestDox('Message::getProtocolVersion is public')]
    public function testMessageGetProtocolVersionIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'getProtocolVersion');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageGetProtocolVersionExists')]
    #[TestDox('Message::getProtocolVersion has no parameters')]
    public function testMessageGetProtocolVersionHasNoParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'getProtocolVersion');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testMessageGetProtocolVersionExists')]
    #[Depends('testMessageGetProtocolVersionIsPublic')]
    #[Depends('testMessageGetProtocolVersionHasNoParameters')]
    #[TestDox('Message::getProtocolVersion returns non-empty string')]
    public function testMessageGetProtocolVersionReturnsNonEmptyString(): void
    {
        $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
        $message = new Message();
        $this->assertNotEmpty($message->getProtocolVersion());
        $this->assertIsString($message->getProtocolVersion());
    }

    #[Depends('testMessageGetProtocolVersionReturnsNonEmptyString')]
    #[TestDox('Message::getProtocolVersion returns protocol version only')]
    public function testMessageGetProtocolVersionReturnsProtocolVersionOnly(): void
    {
        $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
        $message = new Message();
        $this->assertEquals('1.1', $message->getProtocolVersion());
    }

    #[TestDox('Message::setProtocolVersion exists')]
    public function testMessageSetProtocolVersionExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('setProtocolVersion'));
    }

    #[Depends('testMessageSetProtocolVersionExists')]
    #[TestDox('Message::setProtocolVersion is public')]
    public function testMessageSetProtocolVersionIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'setProtocolVersion');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageSetProtocolVersionExists')]
    #[TestDox('Message::setProtocolVersion has one REQUIRED parameter')]
    public function testMessageSetProtocolVersionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'setProtocolVersion');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfParameters());
    }


    #[TestDox('Message::hasHeader exists')]
    public function testMessageHasHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('hasHeader'));
    }

    #[Depends('testMessageHasHeaderExists')]
    #[TestDox('Message::hasHeader is public')]
    public function testMessageHasHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'hasHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageHasHeaderExists')]
    #[TestDox('Message::hasHeader has one REQUIRED parameter')]
    public function testMessageHasHeaderHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'hasHeader');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageHasHeaderExists')]
    #[Depends('testMessageHasHeaderIsPublic')]
    #[Depends('testMessageHasHeaderHasOneRequiredParameter')]
    #[TestDox('Message::hasHeader returns boolean')]
    public function testMessageHasHeaderReturnsBoolean(): void
    {
        $message = new Message();
        $this->assertIsBool($message->hasHeader('Accept-Language'));
    }


    #[TestDox('Message::setHeader exists')]
    public function testMessageSetHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('setHeader'));
    }

    #[Depends('testMessageSetHeaderExists')]
    #[TestDox('Message::setHeader is public')]
    public function testMessageSetHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'setHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageSetHeaderExists')]
    #[TestDox('Message::setHeader has two REQUIRED parameters')]
    public function testMessageSetHeaderHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'setHeader');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageSetHeaderExists')]
    #[Depends('testMessageSetHeaderIsPublic')]
    #[Depends('testMessageSetHeaderHasTwoRequiredParameters')]
    #[TestDox('Message::setHeader adds new HTTP header')]
    public function testMessageSetHeaderAddsNewHttpHeader(): void
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('Content-Type'));
        $this->assertCount(0, $message->getHeader('Content-Type'));

        $message->setHeader('Content-Type', 'text/html');
        $this->assertTrue($message->hasHeader('Content-Type'));
        $this->assertCount(1, $message->getHeader('Content-Type'));
    }

    #[Depends('testMessageSetHeaderAddsNewHttpHeader')]
    #[TestDox('Message::setHeader replaces existing HTTP header')]
    public function testMessageSetHeaderReplacesExistingHttpHeader(): void
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('Content-Type'));

        $message->setHeader('Content-Type', 'text/html');
        $this->assertTrue($message->hasHeader('Content-Type'));
        $this->assertCount(1, $message->getHeader('Content-Type'));
        $this->assertContains('text/html', $message->getHeader('Content-Type'));

        $message->setHeader('Content-Type', 'image/png');
        $this->assertTrue($message->hasHeader('Content-Type'));
        $this->assertCount(1, $message->getHeader('Content-Type'));
        $this->assertNotContains('text/html', $message->getHeader('Content-Type'));
        $this->assertContains('image/png', $message->getHeader('Content-Type'));
    }


    #[TestDox('Message::withAddedHeader exists')]
    public function testMessageWithAddedHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('withAddedHeader'));
    }

    #[Depends('testMessageWithAddedHeaderExists')]
    #[TestDox('Message::withAddedHeader is public')]
    public function testMessageWithAddedHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'withAddedHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageWithAddedHeaderExists')]
    #[TestDox('Message::withAddedHeader has two REQUIRED parameters')]
    public function testMessageWithAddedHeaderHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'withAddedHeader');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageWithAddedHeaderExists')]
    #[Depends('testMessageWithAddedHeaderIsPublic')]
    #[Depends('testMessageWithAddedHeaderHasTwoRequiredParameters')]
    #[TestDox('Message::withAddedHeader returns instance of Message')]
    public function testMessageWithAddedHeaderReturnsInstanceOfMessage(): void
    {
        $message = new Message();
        $this->assertInstanceOf(Message::class, $message->withAddedHeader('Accept-Encoding', 'gzip, deflate'));
        $this->assertNotSame($message->withAddedHeader('Accept-Encoding', 'gzip, deflate'), $message);
    }

    #[Depends('testMessageWithAddedHeaderReturnsInstanceOfMessage')]
    #[TestDox('Message::withAddedHeader adds header values')]
    public function testMessageWithAddedHeaderAddsHeaderValues(): void
    {
        $first_message = new Message();
        $first_message->setHeader('Accept-Encoding', 'deflate');
        $second_message = $first_message->withAddedHeader('Accept-Encoding', 'gzip');

        $this->assertCount(1, $first_message->getHeader('Accept-Encoding'));
        $this->assertCount(2, $second_message->getHeader('Accept-Encoding'));

        $this->assertEquals('deflate', $first_message->getHeaderLine('Accept-Encoding'));
        $this->assertEquals('deflate,gzip', $second_message->getHeaderLine('Accept-Encoding'));
    }


    #[TestDox('Message::withBody exists')]
    public function testMessageWithBodyExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('withBody'));
    }

    #[Depends('testMessageWithBodyExists')]
    #[TestDox('Message::withBody is public')]
    public function testMessageWithBodyIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'withBody');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageWithBodyExists')]
    #[TestDox('Message::withBody has one REQUIRED parameter')]
    public function testMessageWithBodyHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'withBody');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageWithBodyExists')]
    #[Depends('testMessageWithBodyIsPublic')]
    #[Depends('testMessageWithBodyHasOneRequiredParameter')]
    #[TestDox('Message::withBody returns instance of Message')]
    public function testMessageWithBodyReturnsInstanceOfMessage(): void
    {
        $message = new Message();
        $stream = new \StoreCore\Engine\TemporaryMemoryStream();
        $this->assertInstanceOf(Message::class, $message->withBody($stream));
    }


    #[TestDox('Message::withHeader exists')]
    public function testMessageWithHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('withHeader'));
    }

    #[Depends('testMessageWithHeaderExists')]
    #[TestDox('Message::withHeader is public')]
    public function testMessageWithHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'withHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageWithHeaderExists')]
    #[TestDox('Message::withHeader has two REQUIRED parameters')]
    public function testMessageWithHeaderHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Message::class, 'withHeader');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageWithHeaderExists')]
    #[Depends('testMessageWithHeaderIsPublic')]
    #[Depends('testMessageWithHeaderHasTwoRequiredParameters')]
    #[TestDox('Message::withHeader returns instance of Message')]
    public function testMessageWithHeaderReturnsInstanceOfMessage(): void
    {
        $message = new Message();
        $this->assertInstanceOf(Message::class, $message->withHeader('Accept-Encoding', 'gzip, deflate'));
        $this->assertNotSame($message->withHeader('Accept-Encoding', 'gzip, deflate'), $message);
    }

    #[Depends('testMessageWithHeaderReturnsInstanceOfMessage')]
    #[TestDox('Message::withHeader adds header')]
    public function testMessageWithHeaderAddsHeader(): void
    {
        $first_message  = new Message();
        $second_message = $first_message->withHeader('Accept-Encoding', 'gzip, deflate');

        $this->assertEmpty($first_message->getHeader('Accept-Encoding'));
        $this->assertNotEmpty($second_message->getHeader('Accept-Encoding'));

        $this->assertEquals('', $first_message->getHeaderLine('Accept-Encoding'));
        $this->assertEquals('gzip,deflate', $second_message->getHeaderLine('Accept-Encoding'));
    }


    #[TestDox('Message::withoutHeader exists')]
    public function testMessageWithoutHeaderExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('withoutHeader'));
    }

    #[Depends('testMessageWithoutHeaderExists')]
    #[TestDox('Message::withoutHeader is public')]
    public function testMessageWithoutHeaderIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'withoutHeader');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageWithoutHeaderExists')]
    #[TestDox('Message::withoutHeader has one REQUIRED parameter')]
    public function testMessageWithoutHeaderHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'withoutHeader');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageWithoutHeaderExists')]
    #[Depends('testMessageWithoutHeaderIsPublic')]
    #[Depends('testMessageWithoutHeaderHasOneRequiredParameter')]
    #[TestDox('Message::withoutHeader returns instance of Message')]
    public function testMessageWithoutHeaderReturnsInstanceOfMessage(): void
    {
        $message = new Message();
        $this->assertInstanceOf(Message::class, $message->withoutHeader('User-Agent'));
        $this->assertNotSame($message->withoutHeader('User-Agent'), $message);
    }


    #[TestDox('Message::withProtocolVersion exists')]
    public function testMessageWithProtocolVersionExists(): void
    {
        $class = new ReflectionClass(Message::class);
        $this->assertTrue($class->hasMethod('withProtocolVersion'));
    }

    #[Depends('testMessageWithProtocolVersionExists')]
    #[TestDox('Message::withProtocolVersion is public')]
    public function testMessageWithProtocolVersionIsPublic(): void
    {
        $method = new ReflectionMethod(Message::class, 'withProtocolVersion');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testMessageWithProtocolVersionExists')]
    #[TestDox('Message::withProtocolVersion has one REQUIRED parameter')]
    public function testMessageWithProtocolVersionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Message::class, 'withProtocolVersion');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testMessageWithProtocolVersionExists')]
    #[Depends('testMessageWithProtocolVersionIsPublic')]
    #[Depends('testMessageWithProtocolVersionHasOneRequiredParameter')]
    #[TestDox('Message::withProtocolVersion returns instance of Message')]
    public function testMessageWithProtocolVersionReturnsInstanceOfMessage(): void
    {
        $message = new Message();
        $this->assertInstanceOf(Message::class, $message->withProtocolVersion('1.0'));
    }
}
