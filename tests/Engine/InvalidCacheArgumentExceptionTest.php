<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversNothing]
final class InvalidCacheArgumentExceptionTest extends TestCase
{
    #[TestDox('InvalidCacheArgumentException is a throwable exception')]
    public function testInvalidCacheArgumentExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(InvalidCacheArgumentException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));

        $this->expectException(InvalidCacheArgumentException::class);
        throw new InvalidCacheArgumentException();
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-16 CacheException interface exists')]
    public function testImplementedPSR16CacheExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheException::class));
    }

    #[Depends('testImplementedPSR16CacheExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('InvalidCacheArgumentException is a PSR-16 CacheException')]
    public function testInvalidCacheArgumentExceptionIsPSR16CacheException(): void
    {
        $class = new ReflectionClass(InvalidCacheArgumentException::class);
        $this->assertTrue($class->implementsInterface(\Psr\SimpleCache\CacheException::class));

        $this->expectException(\Psr\SimpleCache\CacheException::class);
        throw new InvalidCacheArgumentException();
    }

    #[Depends('testImplementedPSR16CacheExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-16 InvalidArgumentException interface exists')]
    public function testImplementedPSR16InvalidArgumentExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheException'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheException::class));
    }

    #[Depends('testImplementedPSR16InvalidArgumentExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('InvalidCacheArgumentException is a PSR-16 InvalidArgumentException')]
    public function testInvalidCacheArgumentExceptionIsPSR16InvalidArgumentException(): void
    {
        $class = new ReflectionClass(InvalidCacheArgumentException::class);
        $this->assertTrue($class->implementsInterface(\Psr\SimpleCache\InvalidArgumentException::class));

        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        throw new InvalidCacheArgumentException();
    }
}
