<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class UniformResourceIdentifierTest extends TestCase
{
    private UriFactory $factory;

    public function setUp(): void
    {
        $this->factory = new UriFactory();
    }


    #[Group('distro')]
    #[TestDox('Uniform Resource Identifier model class file exists')]
    public function testUniformResourceIdentifierModelClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'UniformResourceIdentifier.php'
        );
    }

    #[Depends('testUniformResourceIdentifierModelClassFileExists')]
    #[Group('distro')]
    #[TestDox('Uniform Resource Identifier model class file is readable')]
    public function testUniformResourceIdentifierModelClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'UniformResourceIdentifier.php'
        );
    }

    #[Depends('testUniformResourceIdentifierModelClassFileExists')]
    #[Depends('testUniformResourceIdentifierModelClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('Uniform Resource Identifier model class exists')]
    public function testUniformResourceIdentifierModelClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\Engine\\UniformResourceIdentifier'));
        $this->assertTrue(class_exists(UniformResourceIdentifier::class));
    }


    #[Group('hmvc')]
    #[TestDox('Uniform Resource Identifier class is concrete')]
    public function testUniformResourceIdentifierClassIsConcrete(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Uniform Resource Identifier is stringable')]
    public function testUniformResourceIdentifierIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new UniformResourceIdentifier());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 UriInterface exists')]
    public function testImplementedPSR7UriInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\UriInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\UriInterface::class));
    }

    #[Depends('testImplementedPSR7UriInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('UniformResourceIdentifier implements PSR-7 UriInterface')]
    public function testUniformResourceIdentifierImplementsPSR7UriInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, new UniformResourceIdentifier());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UniformResourceIdentifier::VERSION);
        $this->assertIsString(UniformResourceIdentifier::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UniformResourceIdentifier::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('UniformResourceIdentifier::__toString() exists')]
    public function testUniformResourceIdentifierToStringExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testUniformResourceIdentifierToStringExists')]
    #[TestDox('UniformResourceIdentifier::__toString() is public')]
    public function testUniformResourceIdentifierToStringIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, '__toString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[TestDox('UniformResourceIdentifier::get() exists')]
    public function testUniformResourceIdentifierGetExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testUniformResourceIdentifierToStringExists')]
    #[TestDox('UniformResourceIdentifier::get() is public')]
    public function testUniformResourceIdentifierGetIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierToStringExists')]
    #[TestDox('UniformResourceIdentifier::get() has no parameters')]
    public function testUniformResourceIdentifierGetHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'get');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierToStringExists')]
    #[Depends('testUniformResourceIdentifierGetIsPublic')]
    #[Depends('testUniformResourceIdentifierGetHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::get() returns string')]
    public function testUniformResourceIdentifierGetReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->get());
    }


    #[TestDox('UniformResourceIdentifier::getAuthority() exists')]
    public function testUniformResourceIdentifierGetAuthorityExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getAuthority'));
    }

    #[Depends('testUniformResourceIdentifierGetAuthorityExists')]
    #[TestDox('UniformResourceIdentifier::getAuthority() is public')]
    public function testUniformResourceIdentifierGetAuthorityIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getAuthority');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetAuthorityExists')]
    #[TestDox('UniformResourceIdentifier::getAuthority() has no parameters')]
    public function testUniformResourceIdentifierGetAuthorityHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getAuthority');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetAuthorityExists')]
    #[Depends('testUniformResourceIdentifierGetAuthorityIsPublic')]
    #[Depends('testUniformResourceIdentifierGetAuthorityHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getAuthority() returns string')]
    public function testUniformResourceIdentifierGetAuthorityReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->getAuthority());
    }

    #[Depends('testUniformResourceIdentifierGetAuthorityReturnsString')]
    #[TestDox('UniformResourceIdentifier::getAuthority() returns empty string if no authority is present')]
    public function testUniformResourceIdentifierGetAuthorityReturnsEmptyStringIfNoAuthorityIsPresent()
    {
        $uri = new UniformResourceIdentifier();
        $this->assertEmpty($uri->getAuthority());
    }


    #[TestDox('UniformResourceIdentifier::getFragment() exists')]
    public function testUniformResourceIdentifierGetFragmentExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getFragment'));
    }

    #[Depends('testUniformResourceIdentifierGetFragmentExists')]
    #[TestDox('UniformResourceIdentifier::getFragment() is public')]
    public function testUniformResourceIdentifierGetFragmentIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getFragment');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetFragmentExists')]
    #[TestDox('UniformResourceIdentifier::getFragment() has no parameters')]
    public function testUniformResourceIdentifierGetFragmentHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getFragment');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetFragmentExists')]
    #[Depends('testUniformResourceIdentifierGetFragmentIsPublic')]
    #[Depends('testUniformResourceIdentifierGetFragmentHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getFragment() returns string')]
    public function testUniformResourceIdentifierGetFragmentReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->getFragment());
    }

    #[Depends('testUniformResourceIdentifierGetFragmentReturnsString')]
    #[TestDox('UniformResourceIdentifier::getFragment() returns empty string if no fragment is present')]
    public function testUniformResourceIdentifierGetFragmentReturnsEmptyStringIfNoFragmentIsPresent(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertEmpty($uri->getFragment());
    }


    #[TestDox('UniformResourceIdentifier::getHost() exists')]
    public function testUniformResourceIdentifierGetHostExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getHost'));
    }

    #[Depends('testUniformResourceIdentifierGetHostExists')]
    #[TestDox('UniformResourceIdentifier::getHost() is public')]
    public function testUniformResourceIdentifierGetHostIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getHost');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetHostExists')]
    #[TestDox('UniformResourceIdentifier::getHost() has no parameters')]
    public function testUniformResourceIdentifierGetHostHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getHost');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetHostExists')]
    #[Depends('testUniformResourceIdentifierGetHostIsPublic')]
    #[Depends('testUniformResourceIdentifierGetHostHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getHost() returns string')]
    public function testUniformResourceIdentifierGetHostReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->getHost());
    }

    #[Depends('testUniformResourceIdentifierGetHostReturnsString')]
    #[TestDox('UniformResourceIdentifier::getHost() returns empty string if no host is present')]
    public function testUniformResourceIdentifierGetHostReturnsEmptyStringIfNoHostIsPresent(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertEmpty($uri->getHost());
    }

    #[Depends('testUniformResourceIdentifierGetHostReturnsString')]
    #[TestDox('UniformResourceIdentifier::getHost() returns lowercase host name')]
    public function testUniformResourceIdentifierGetHostReturnsLowercaseHostName(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setHost('Example.com');
        $this->assertNotEmpty($uri->getHost());
        $this->assertIsString($uri->getHost());
        $this->assertEquals('example.com', $uri->getHost());

        $uri = $this->factory->createUri('https://Example.com');
        $this->assertNotEmpty($uri->getHost());
        $this->assertIsString($uri->getHost());
        $this->assertEquals('example.com', $uri->getHost());
    }

    #[Depends('testUniformResourceIdentifierGetHostReturnsLowercaseHostName')]
    #[TestDox('UniformResourceIdentifier::getHost() returns subdomain')]
    public function testUniformResourceIdentifierGetHostReturnsSubdomain(): void
    {
        $uri = $this->factory->createUri('https://example.com/');
        $this->assertEquals('example.com', $uri->getHost());

        $uri = $this->factory->createUri('https://www.example.com/');
        $this->assertEquals('www.example.com', $uri->getHost());

        $uri = $this->factory->createUri('https://api.example.com/');
        $this->assertStringStartsWith('api', $uri->getHost());
        $this->assertStringStartsWith('api.', $uri->getHost());
    }


    #[TestDox('UniformResourceIdentifier::getPath() exists')]
    public function testUniformResourceIdentifierGetPathExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getPath'));
    }

    #[Depends('testUniformResourceIdentifierGetPathExists')]
    #[TestDox('UniformResourceIdentifier::getPath() is public')]
    public function testUniformResourceIdentifierGetPathIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getPath');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetPathExists')]
    #[TestDox('UniformResourceIdentifier::getPath() has no parameters')]
    public function testUniformResourceIdentifierGetPathHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getPath');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetPathExists')]
    #[Depends('testUniformResourceIdentifierGetPathIsPublic')]
    #[Depends('testUniformResourceIdentifierGetPathHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getPath() returns string')]
    public function testUniformResourceIdentifierGetPathReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->getPath());
    }

    #[Depends('testUniformResourceIdentifierGetPathReturnsString')]
    #[TestDox('UniformResourceIdentifier::getPath() returns empty string if no path is present')]
    public function testUniformResourceIdentifierGetPathReturnsEmptyStringIfNoPathIsPresent(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertEmpty($uri->getPath());
        $this->assertIsString($uri->getPath());
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/
     *   Normally, the empty path "" and absolute path "/" are considered equal
     *   as defined in RFC 7230 Section 2.7.3.  But this method MUST NOT 
     *   automatically do this normalization because in contexts with a trimmed
     *   base path, e.g. the front controller, this difference becomes significant.
     *   It’s the task of the user to handle both "" and "/".
     */
    #[Depends('testUniformResourceIdentifierGetPathReturnsString')]
    #[TestDox('UniformResourceIdentifier::getPath() returns two types of domain URIs')]
    public function testUniformResourceIdentifierGetPathReturnsTwoTypesOfDomainUris(): void
    {
        $uri = $this->factory->createUri('https://www.example.com');
        $this->assertEquals('', $uri->getPath());

        $uri = $this->factory->createUri('https://www.example.com/');
        $this->assertEquals('/', $uri->getPath());
    }

    #[Depends('testUniformResourceIdentifierGetPathReturnsString')]
    #[TestDox('UniformResourceIdentifier::getPath() returns directories')]
    public function testUniformResourceIdentifierGetPathReturnsDirectories(): void
    {
        $uri = $this->factory->createUri('https://www.example.com/api');
        $this->assertEquals('/api', $uri->getPath());

        $uri = $this->factory->createUri('https://www.example.com/api/');
        $this->assertEquals('/api/', $uri->getPath());

        $uri = $this->factory->createUri('https://www.example.com/api/v1');
        $this->assertEquals('/api/v1', $uri->getPath());

        $uri = $this->factory->createUri('https://www.example.com/api/v1/');
        $this->assertEquals('/api/v1/', $uri->getPath());
    }

    #[Depends('testUniformResourceIdentifierGetPathReturnsString')]
    #[TestDox('UniformResourceIdentifier::getPath() returns filename and extension')]
    public function testUniformResourceIdentifierGetPathReturnsFileNamesAndExtensions(): void
    {
        $uri = $this->factory->createUri('https://www.example.com/favicon.ico');
        $this->assertStringEndsWith('favicon.ico', $uri->getPath());

        $filename = pathinfo($uri->getPath(), \PATHINFO_FILENAME);
        $this->assertEquals('favicon', $filename);

        $extension = pathinfo($uri->getPath(), \PATHINFO_EXTENSION);
        $this->assertEquals('ico', $extension);
    }


    #[TestDox('UniformResourceIdentifier::getPort() exists')]
    public function testUniformResourceIdentifierGetPortExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getPort'));
    }

    #[Depends('testUniformResourceIdentifierGetPortExists')]
    #[TestDox('UniformResourceIdentifier::getPort() is public')]
    public function testUniformResourceIdentifierGetPortIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getPort');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetPortExists')]
    #[TestDox('UniformResourceIdentifier::getPort() has no parameters')]
    public function testUniformResourceIdentifierGetPortHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getPort');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetPortExists')]
    #[Depends('testUniformResourceIdentifierGetPortIsPublic')]
    #[Depends('testUniformResourceIdentifierGetPortHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getPort() returns null by default')]
    public function testUniformResourceIdentifierGetPortReturnsNullByDefault(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertNull($uri->getPort());
    }


    #[TestDox('UniformResourceIdentifier::getQuery() exists')]
    public function testUniformResourceIdentifierGetQueryExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getQuery'));
    }

    #[Depends('testUniformResourceIdentifierGetQueryExists')]
    #[TestDox('UniformResourceIdentifier::getQuery() is public')]
    public function testUniformResourceIdentifierGetQueryIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getQuery');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetQueryExists')]
    #[TestDox('UniformResourceIdentifier::getQuery() has no parameters')]
    public function testUniformResourceIdentifierGetQueryHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getQuery');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('UniformResourceIdentifier::getScheme() exists')]
    public function testUniformResourceIdentifierGetSchemeExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getScheme'));
    }

    #[Depends('testUniformResourceIdentifierGetSchemeExists')]
    #[TestDox('UniformResourceIdentifier::getScheme() is public')]
    public function testUniformResourceIdentifierGetSchemeIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getScheme');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetSchemeExists')]
    #[TestDox('UniformResourceIdentifier::getScheme() has no parameters')]
    public function testUniformResourceIdentifierGetSchemeHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getScheme');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierGetSchemeExists')]
    #[Depends('testUniformResourceIdentifierGetSchemeIsPublic')]
    #[Depends('testUniformResourceIdentifierGetSchemeHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getScheme() returns string')]
    public function testUniformResourceIdentifierGetSchemeReturnsString(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertIsString($uri->getScheme());
    }

    #[Depends('testUniformResourceIdentifierGetSchemeHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::getScheme() returns empty string if no scheme is present')]
    public function testUniformResourceIdentifierGetSchemeReturnsEmptyStringIfNoSchemeIsPresent(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertEmpty($uri->getScheme());
    }


    #[TestDox('UniformResourceIdentifier::getUserInfo() exists')]
    public function testUniformResourceIdentifierGetUserInfoExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('getUserInfo'));
    }

    #[Depends('testUniformResourceIdentifierGetUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::getUserInfo() is public')]
    public function testUniformResourceIdentifierGetUserInfoIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getUserInfo');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierGetUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::getUserInfo() has no parameters')]
    public function testUniformResourceIdentifierGetUserInfoHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'getUserInfo');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('UniformResourceIdentifier::setAuthority() exists')]
    public function testUniformResourceIdentifierSetAuthorityExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setAuthority'));
    }

    #[Depends('testUniformResourceIdentifierSetAuthorityExists')]
    #[TestDox('UniformResourceIdentifier::setAuthority() is public')]
    public function testUniformResourceIdentifierSetAuthorityIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setAuthority');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetAuthorityExists')]
    #[TestDox('UniformResourceIdentifier::setAuthority() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetAuthorityHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setAuthority');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::setFragment() exists')]
    public function testUniformResourceIdentifierSetFragmentExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setFragment'));
    }

    #[Depends('testUniformResourceIdentifierSetFragmentExists')]
    #[TestDox('UniformResourceIdentifier::setFragment() is public')]
    public function testUniformResourceIdentifierSetFragmentIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setFragment');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetFragmentExists')]
    #[TestDox('UniformResourceIdentifier::setFragment() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetFragmentHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setFragment');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::setHost() exists')]
    public function testUniformResourceIdentifierSetHostExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setHost'));
    }

    #[Depends('testUniformResourceIdentifierSetHostExists')]
    #[TestDox('UniformResourceIdentifier::setHost() is public')]
    public function testUniformResourceIdentifierSetHostIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setHost');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetHostExists')]
    #[TestDox('UniformResourceIdentifier::setHost() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetHostHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setHost');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUniformResourceIdentifierSetHostHasOneRequiredParameter')]
    #[TestDox('UniformResourceIdentifier::setHost() is case insensitive')]
    public function testUniformResourceIdentifierSetHostIsCaseInsensitive(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setHost('Example.com');
        $this->assertEquals('example.com', $uri->gethost());
        $uri->setHost('Example.NET');
        $this->assertEquals('example.net', $uri->getHost());
    }


    #[TestDox('UniformResourceIdentifier::setPath() exists')]
    public function testUniformResourceIdentifierSetPathExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setPath'));
    }

    #[Depends('testUniformResourceIdentifierSetPathExists')]
    #[TestDox('UniformResourceIdentifier::setPath() is public')]
    public function testUniformResourceIdentifierSetPathIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setPath');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetPathExists')]
    #[TestDox('UniformResourceIdentifier::setPath() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetPathHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setPath');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUniformResourceIdentifierSetPathExists')]
    #[Depends('testUniformResourceIdentifierSetPathIsPublic')]
    #[Depends('testUniformResourceIdentifierSetPathHasOneRequiredParameter')]
    #[TestDox('UniformResourceIdentifier::setPath() throws InvalidArgumentException for wrong data type')]
    public function testUniformResourceIdentifierSetPathThrowsInvalidArgumentExceptionForWrongDataType(): void
    {
        $uri =  new UniformResourceIdentifier();
        $this->expectException(\InvalidArgumentException::class);
        $uri->setPath(true);
    }


    #[TestDox('UniformResourceIdentifier::setPort() exists')]
    public function testUniformResourceIdentifierSetPortExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setPort'));
    }

    #[Depends('testUniformResourceIdentifierSetPortExists')]
    #[TestDox('UniformResourceIdentifier::setPort() is public')]
    public function testUniformResourceIdentifierSetPortIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setPort');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetPortExists')]
    #[TestDox('UniformResourceIdentifier::setPort() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetPortHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setPort');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUniformResourceIdentifierSetPortExists')]
    #[TestDox('UniformResourceIdentifier::setPort() accepts integers')]
    public function testUniformResourceIdentifierSetPortAcceptsIntegers(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertNull($uri->getPort());
        $uri->setPort(80);
        $this->assertNotNull($uri->getPort());
        $this->assertIsInt($uri->getPort());
        $this->assertEquals(80, $uri->getPort());
    }

    #[Depends('testUniformResourceIdentifierSetPortAcceptsIntegers')]
    #[TestDox('UniformResourceIdentifier::setPort() accepts strings with digits')]
    public function testUniformResourceIdentifierSetPortAcceptsStringsWithDigits(): void
    {
        $uri = new UniformResourceIdentifier();
        $this->assertNull($uri->getPort());
        $uri->setPort('8080');
        $this->assertIsInt($uri->getPort());
        $this->assertEquals(8080, $uri->getPort());
    }

    #[Depends('testUniformResourceIdentifierSetPortAcceptsIntegers')]
    #[TestDox('UniformResourceIdentifier::setPort() accepts null')]
    public function testUniformResourceIdentifierSetPortAcceptsNull(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setPort(80);
        $this->assertEquals(80, $uri->getPort());
        $uri->setPort(null);
        $this->assertNull($uri->getPort());
    }

    #[Depends('testUniformResourceIdentifierSetPortAcceptsIntegers')]
    #[TestDox('UniformResourceIdentifier::setPort() accepts empty string as null')]
    public function testUniformResourceIdentifierSetPortAcceptsEmptyStringAsNull(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setPort(80);
        $this->assertEquals(80, $uri->getPort());
        $uri->setPort('');
        $this->assertNull($uri->getPort());
    }


    #[TestDox('UniformResourceIdentifier::setQuery() exists')]
    public function testUniformResourceIdentifierSetQueryExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setQuery'));
    }

    #[Depends('testUniformResourceIdentifierSetQueryExists')]
    #[TestDox('UniformResourceIdentifier::setQuery() is public')]
    public function testUniformResourceIdentifierSetQueryIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setQuery');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetQueryExists')]
    #[TestDox('UniformResourceIdentifier::setQuery() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetQueryHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setQuery');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::setScheme() exists')]
    public function testUniformResourceIdentifierSetSchemeExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setScheme'));
    }

    #[Depends('testUniformResourceIdentifierSetSchemeExists')]
    #[TestDox('UniformResourceIdentifier::setScheme() is public')]
    public function testUniformResourceIdentifierSetSchemeIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setScheme');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetSchemeExists')]
    #[TestDox('UniformResourceIdentifier::setScheme() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetSchemeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setScheme');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testUniformResourceIdentifierGetSchemeReturnsString')]
    #[Depends('testUniformResourceIdentifierSetSchemeExists')]
    #[Depends('testUniformResourceIdentifierSetSchemeIsPublic')]
    #[Depends('testUniformResourceIdentifierSetSchemeHasOneRequiredParameter')]
    #[TestDox('UniformResourceIdentifier::setScheme() accepts `http` scheme')]
    public function testUniformResourceIdentifierSetSchemeAcceptsHttpScheme(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setScheme('http');
        $this->assertEquals('http', $uri->getScheme());
    }

    #[Depends('testUniformResourceIdentifierGetSchemeReturnsString')]
    #[Depends('testUniformResourceIdentifierSetSchemeExists')]
    #[Depends('testUniformResourceIdentifierSetSchemeIsPublic')]
    #[Depends('testUniformResourceIdentifierSetSchemeHasOneRequiredParameter')]
    #[TestDox('UniformResourceIdentifier::setScheme() accepts `https` scheme')]
    public function testUniformResourceIdentifierSetSchemeAcceptsHttpsScheme(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setScheme('https');
        $this->assertEquals('https', $uri->getScheme());
    }

    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpScheme')]
    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpsScheme')]
    #[TestDox('UniformResourceIdentifier::getScheme() returns set scheme')]
    public function testUniformResourceIdentifierGetSchemeReturnsSetScheme(): void
    {
        $uri = new UniformResourceIdentifier();

        $uri->setScheme('http');
        $this->assertEquals('http', $uri->getScheme());

        $uri->setScheme('https');
        $this->assertEquals('https', $uri->getScheme());
    }

    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpScheme')]
    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpsScheme')]
    #[TestDox('UniformResourceIdentifier::setScheme() is case insensitive')]
    public function testUniformResourceIdentifierSetSchemeIsCaseInsensitive(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setScheme('HTTPS');
        $this->assertEquals('https', $uri->getScheme());
        $uri->setScheme('HTTPs');
        $this->assertEquals('https', $uri->getScheme());
    }


    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpScheme')]
    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpsScheme')]
    #[TestDox('UniformResourceIdentifier::getPort() returns null on default HTTP port')]
    public function testUniformResourceIdentifierGetPortReturnNullOnDefaultHttpPort(): void
    {
        $uri = new UniformResourceIdentifier();

        $uri->setPort(80);
        $this->assertNotNull($uri->getPort());
        $uri->setScheme('http');
        $this->assertNull($uri->getPort());

        $uri->setPort(8008);
        $this->assertNotNull($uri->getPort());

        $uri->setPort(8080);
        $this->assertNotNull($uri->getPort());
    }

    /**
     * @see https://stackoverflow.com/questions/32478277/is-there-any-standard-alternative-https-port
     */
    #[Depends('testUniformResourceIdentifierSetSchemeAcceptsHttpsScheme')]
    #[TestDox('UniformResourceIdentifier::getPort() returns null on default HTTPS port')]
    public function testUniformResourceIdentifierGetPortReturnNullOnDefaultHttpsPort(): void
    {
        $uri = new UniformResourceIdentifier();

        $uri->setPort(443);
        $this->assertNotNull($uri->getPort());
        $uri->setScheme('https');
        $this->assertNull($uri->getPort());

        $uri->setPort(8443);
        $this->assertNotNull($uri->getPort());
    }


    #[TestDox('UniformResourceIdentifier::setUserInfo() exists')]
    public function testUniformResourceIdentifierSetUserInfoExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('setUserInfo'));
    }

    #[Depends('testUniformResourceIdentifierSetUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::setUserInfo() is public')]
    public function testUniformResourceIdentifierSetUserInfoIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setUserInfo');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierSetUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::setUserInfo() has two parameters')]
    public function testUniformResourceIdentifierSetUserInfoHasTwoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setUserInfo');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierSetUserInfoHasTwoParameters')]
    #[TestDox('UniformResourceIdentifier::setUserInfo() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierSetUserInfoHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'setUserInfo');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('UniformResourceIdentifier::setAuthority() sets OPTIONAL user information')]
    public function testUniformResourceIdentifierSetAuthoritySetsOptionalUserInformation(): void
    {
        $uri = new UniformResourceIdentifier();

        $uri->setAuthority('alice@example.com');
        $this->assertEquals('alice', $uri->getUserInfo());

        $uri->setAuthority('bob:secret@example.com');
        $this->assertEquals('bob:secret', $uri->getUserInfo());

        $uri->setAuthority('bob:secret@example.com:8080');
        $this->assertEquals('bob:secret', $uri->getUserInfo());
    }


    #[TestDox('UniformResourceIdentifier::unsetPort() exists')]
    public function testUniformResourceIdentifierUnsetPortExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('unsetPort'));
    }

    #[Depends('testUniformResourceIdentifierUnsetPortExists')]
    #[TestDox('UniformResourceIdentifier::unsetPort() is public')]
    public function testUniformResourceIdentifierUnsetPortIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'unsetPort');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierUnsetPortExists')]
    #[TestDox('UniformResourceIdentifier::unsetPort() has no parameters')]
    public function testUniformResourceIdentifierUnsetPortHasNoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'unsetPort');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierUnsetPortExists')]
    #[Depends('testUniformResourceIdentifierUnsetPortIsPublic')]
    #[Depends('testUniformResourceIdentifierUnsetPortHasNoParameters')]
    #[TestDox('UniformResourceIdentifier::unsetPort() sets port to null')]
    public function testUniformResourceIdentifierUnsetPortSetsPortToNull(): void
    {
        $uri = new UniformResourceIdentifier();
        $uri->setPort(8080);
        $this->assertEquals(8080, $uri->getPort());
        $uri->unsetPort();
        $this->assertNull($uri->getPort());
    }


    #[TestDox('UniformResourceIdentifier::withFragment() exists')]
    public function testUniformResourceIdentifierWithFragmentExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withFragment'));
    }

    #[Depends('testUniformResourceIdentifierWithFragmentExists')]
    #[TestDox('UniformResourceIdentifier::withFragment() is public')]
    public function testUniformResourceIdentifierWithFragmentIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withFragment');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithFragmentExists')]
    #[TestDox('UniformResourceIdentifier::withFragment() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithFragmentHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withFragment');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withHost() exists')]
    public function testUniformResourceIdentifierWithHostExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withHost'));
    }

    #[Depends('testUniformResourceIdentifierWithHostExists')]
    #[TestDox('UniformResourceIdentifier::withHost() is public')]
    public function testUniformResourceIdentifierWithHostIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withHost');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithHostExists')]
    #[TestDox('UniformResourceIdentifier::withHost() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithHostHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withHost');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withPath() exists')]
    public function testUniformResourceIdentifierWithPathExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withPath'));
    }

    #[Depends('testUniformResourceIdentifierWithPathExists')]
    #[TestDox('UniformResourceIdentifier::withPath() is public')]
    public function testUniformResourceIdentifierWithPathIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withPath');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithPathExists')]
    #[TestDox('UniformResourceIdentifier::withPath() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithPathHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withPath');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withPort() exists')]
    public function testUniformResourceIdentifierWithPortExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withPort'));
    }

    #[Depends('testUniformResourceIdentifierWithPortExists')]
    #[TestDox('UniformResourceIdentifier::withPort() is public')]
    public function testUniformResourceIdentifierWithPortIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withPort');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithPortExists')]
    #[TestDox('UniformResourceIdentifier::withPort() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithPortHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withPort');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withQuery() exists')]
    public function testUniformResourceIdentifierWithQueryExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withQuery'));
    }

    #[Depends('testUniformResourceIdentifierWithQueryExists')]
    #[TestDox('UniformResourceIdentifier::withQuery() is public')]
    public function testUniformResourceIdentifierWithQueryIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withQuery');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithQueryExists')]
    #[TestDox('UniformResourceIdentifier::withQuery() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithQueryHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withQuery');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withScheme() exists')]
    public function testUniformResourceIdentifierWithSchemeExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withScheme'));
    }

    #[Depends('testUniformResourceIdentifierWithSchemeExists')]
    #[TestDox('UniformResourceIdentifier::withScheme() is public')]
    public function testUniformResourceIdentifierWithSchemeIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withScheme');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithSchemeExists')]
    #[TestDox('UniformResourceIdentifier::withScheme() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithSchemeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withScheme');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('UniformResourceIdentifier::withUserInfo() exists')]
    public function testUniformResourceIdentifierWithUserInfoExists(): void
    {
        $class = new ReflectionClass(UniformResourceIdentifier::class);
        $this->assertTrue($class->hasMethod('withUserInfo'));
    }

    #[Depends('testUniformResourceIdentifierWithUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::withUserInfo() is public')]
    public function testUniformResourceIdentifierWithUserInfoIsPublic(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withUserInfo');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUniformResourceIdentifierWithUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::withUserInfo() has two parameters')]
    public function testUniformResourceIdentifierWithUserInfoHasTwoParameters(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withUserInfo');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testUniformResourceIdentifierWithUserInfoExists')]
    #[TestDox('UniformResourceIdentifier::withUserInfo() has one REQUIRED parameter')]
    public function testUniformResourceIdentifierWithUserInfoHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UniformResourceIdentifier::class, 'withUserInfo');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
