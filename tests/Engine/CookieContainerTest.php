<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\BackupGlobals;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[BackupGlobals(true)]
#[CoversClass(\StoreCore\Engine\CookieContainer::class)]
final class CookieContainerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CookieContainer class is concrete')]
    public function testCookieContainerClassIsConcrete(): void
    {
        $class = new ReflectionClass(CookieContainer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('CookieContainer implements PSR-11 ContainerInterface')]
    public function testCookieContainerImplementsPSR11ContainerInterface(): void
    {
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, new CookieContainer());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CookieContainer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CookieContainer::VERSION);
        $this->assertIsString(CookieContainer::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CookieContainer::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('CookieContainer::__construct exists')]
    public function testCookieContainerConstructExists(): void
    {
        $class = new ReflectionClass(CookieContainer::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testCookieContainerConstructExists')]
    #[TestDox('CookieContainer::__construct is public constructor')]
    public function testCookieContainerConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testCookieContainerConstructExists')]
    #[TestDox('CookieContainer::__construct has one OPTIONAL parameter')]
    public function testCookieContainerConstructHasOneOptionalParameters(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CookieContainer::get exists')]
    public function testCookieContainerGetExists(): void
    {
        $class = new ReflectionClass(CookieContainer::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testCookieContainerGetExists')]
    #[TestDox('CookieContainer::get is public')]
    public function testCookieContainerGetIsPublic(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCookieContainerGetExists')]
    #[TestDox('CookieContainer::get has one REQUIRED parameter')]
    public function testCookieContainerGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCookieContainerGetExists')]
    #[Depends('testCookieContainerGetIsPublic')]
    #[Depends('testCookieContainerGetHasOneRequiredParameter')]
    #[TestDox('CookieContainer::get throws Psr\Container\NotFoundExceptionInterface when cookie does not exist')]
    public function testCookieContainerGetThrowsPsrContainerNotFoundExceptionInterfaceWhenCookieDoesNotExist()
    {
        $container = new CookieContainer();
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $value = $container->get('KeyThatDoesNotExist');
    }

    #[Depends('testCookieContainerGetExists')]
    #[Depends('testCookieContainerGetIsPublic')]
    #[Depends('testCookieContainerGetHasOneRequiredParameter')]
    #[TestDox('CookieContainer::get returns cookie parameters')]
    public function testCookieContainerGetReturnsCookieParameters(): void
    {
        $cookie_params = ['Language' => 'nl-NL'];
        $container = new CookieContainer($cookie_params);
        $this->assertEquals('nl-NL', $container->get('Language'));
    }

    #[Depends('testCookieContainerGetReturnsCookieParameters')]
    #[TestDox('CookieContainer::get returns superglobal $_COOKIE parameters')]
    public function testCookieContainerGetReturnsSuperglobalCookieParameters(): void
    {
        $_COOKIE['name'] = 'Hannes';
        $container = new CookieContainer();
        $this->assertTrue($container->has('name'));
        $this->assertSame('Hannes', $container->get('name'));

        $_COOKIE['name'] = 'Hannes';
        $container = new CookieContainer(['name' => 'Johannes']);
        $this->assertNotSame('Hannes', $container->get('name'));
        $this->assertSame('Johannes', $container->get('name'));
    }

    /**
     * @see https://stackoverflow.com/questions/11311893/is-the-name-of-a-cookie-case-sensitive
     *      Cookie names are case-sensitive.
     */
    #[Depends('testCookieContainerGetReturnsCookieParameters')]
    #[TestDox('CookieContainer::get is case sensitive')]
    public function testCookieContainerGetReturnIsCaseSensitive(): void
    {
        $cookie_params = [
            'foo' => '123',
            'Foo' => '456',
        ];
        $container = new CookieContainer($cookie_params);

        $this->assertEquals('123', $container->get('foo'));
        $this->assertEquals('456', $container->get('Foo'));
        $this->assertNotSame($container->get('foo'), $container->get('Foo'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('FOO');
    }


    #[TestDox('CookieContainer::has exists')]
    public function testCookieContainerHasExists(): void
    {
        $class = new ReflectionClass(CookieContainer::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testCookieContainerHasExists')]
    #[TestDox('CookieContainer::has is public')]
    public function testCookieContainerHasIsPublic(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testCookieContainerHasExists')]
    #[TestDox('CookieContainer::has has one REQUIRED parameter')]
    public function testCookieContainerHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CookieContainer::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testCookieContainerHasExists')]
    #[Depends('testCookieContainerHasIsPublic')]
    #[Depends('testCookieContainerHasHasOneRequiredParameter')]
    #[TestDox('CookieContainer::has returns boolean')]
    public function testCookieContainerHasReturnsBoolean(): void
    {
        $container = new CookieContainer();
        $this->assertIsBool($container->has('Foo'));
        $this->assertFalse($container->has('Foo'));

        $container = new CookieContainer(array('Foo' => 'Bar'));
        $this->assertIsBool($container->has('Foo'));
        $this->assertTrue($container->has('Foo'));
    }

    #[Depends('testCookieContainerHasReturnsBoolean')]
    #[TestDox('CookieContainer::has returns false by default')]
    public function testCookieContainerHasReturnsFalseByDefault(): void
    {
        $container = new CookieContainer();
        $this->assertFalse($container->has('NoFooNoGlory'));
    }

    #[Depends('testCookieContainerHasReturnsBoolean')]
    #[TestDox('CookieContainer::has is case sensitive')]
    public function testCookieContainerHasIsCaseSensitive(): void
    {
        $cookie_params = [
            'foo' => '123',
            'Foo' => '456',
        ];
        $container = new CookieContainer($cookie_params);

        $this->assertTrue($container->has('foo'));
        $this->assertTrue($container->has('Foo'));
        $this->assertFalse($container->has('FOO'));
    }
}
