<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\AbstractServerRequestHandler::class)]
#[Group('hmvc')]
final class AbstractServerRequestHandlerTest extends TestCase
{
    #[TestDox('AbstractServerRequestHandler class is abstract')]
    public function testAbstractServerRequestHandlerClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractServerRequestHandler::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-15 RequestHandlerInterface exists')]
    public function testImplementedPSR15RequestHandlerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Server\RequestHandlerInterface::class));
    }

    #[Depends('testImplementedPSR15RequestHandlerInterfaceExists')]
    #[TestDox('AbstractServerRequestHandler implements PSR-15 RequestHandlerInterface')]
    public function testAbstractServerRequestHandlerImplementsPSR15RequestHandlerInterface(): void
    {
        $class = new ReflectionClass(AbstractServerRequestHandler::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Server\RequestHandlerInterface::class));
    }


    #[TestDox('AbstractServerRequestHandler::handle exists')]
    public function testAbstractServerRequestHandlerHandleExists(): void
    {
        $class = new ReflectionClass(AbstractServerRequestHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testAbstractServerRequestHandlerHandleExists')]
    #[TestDox('AbstractServerRequestHandler::handle is final public')]
    public function testAbstractServerRequestHandlerHandleIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractServerRequestHandler::class, 'handle');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractServerRequestHandlerHandleExists')]
    #[TestDox('AbstractServerRequestHandler::handle has one REQUIRED parameters')]
    public function testAbstractServerRequestHandlerHandleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractServerRequestHandler::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractServerRequestHandler::process exists')]
    public function testAbstractServerRequestHandlerProcessExists(): void
    {
        $class = new ReflectionClass(AbstractServerRequestHandler::class);
        $this->assertTrue($class->hasMethod('process'));
    }

    #[Depends('testAbstractServerRequestHandlerHandleExists')]
    #[TestDox('AbstractServerRequestHandler::process is abstract')]
    public function testAbstractServerRequestHandlerProcesIsAbstract(): void
    {
        $method = new ReflectionMethod(AbstractServerRequestHandler::class, 'process');
        $this->assertTrue($method->isAbstract());
    }

    #[Depends('testAbstractServerRequestHandlerHandleExists')]
    #[TestDox('AbstractServerRequestHandler::process is not public')]
    public function testAbstractServerRequestHandlerProcesIsNotPublic(): void
    {
        $method = new ReflectionMethod(AbstractServerRequestHandler::class, 'process');
        $this->assertFalse($method->isPublic());
    }
}
