<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Engine\RequestFactory::class)]
#[CoversClass(\StoreCore\Engine\Request::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
class RequestFactoryTest extends TestCase
{
    #[TestDox('RequestFactory class is concrete')]
    public function testRequestFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(RequestFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-17 RequestFactoryInterface exists')]
    public function testImplementedPSR17RequestFactoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestFactoryInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\RequestFactoryInterface::class));
    }

    #[Depends('testRequestFactoryClassIsConcrete')]
    #[Depends('testImplementedPSR17RequestFactoryInterfaceExists')]
    #[TestDox('RequestFactory implements PSR-17 RequestFactoryInterface')]
    public function testRequestFactoryImplementsPSR17RequestFactoryInterface(): void
    {
        $class = new RequestFactory('GET', 'https://www.example.com/');
        $this->assertInstanceOf(\Psr\Http\Message\RequestFactoryInterface::class, $class);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RequestFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RequestFactory::VERSION);
        $this->assertIsString(RequestFactory::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RequestFactory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('RequestFactory::createRequest exists')]
    public function testRequestFactoryCreateRequestExists(): void
    {
        $class = new ReflectionClass(RequestFactory::class);
        $this->assertTrue($class->hasMethod('createRequest'));
    }

    #[Depends('testRequestFactoryCreateRequestExists')]
    #[TestDox('RequestFactory::createRequest is public')]
    public function testRequestFactoryCreateRequestIsPublic(): void
    {
        $method = new ReflectionMethod(RequestFactory::class, 'createRequest');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRequestFactoryCreateRequestExists')]
    #[TestDox('RequestFactory::createRequest has two REQUIRED parameters')]
    public function testRequestFactoryCreateRequestHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(RequestFactory::class, 'createRequest');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Returned PSR-7 RequestInterface exists')]
    public function testReturnedPSR7RequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestInterface'));
    }

    #[Depends('testReturnedPSR7RequestInterfaceExists')]
    #[TestDox('RequestFactory::createRequest returns PSR-7 RequestInterface')]
    public function testRequestFactoryCreateRequestReturnsPsr7RequestInterface(): void
    {
        $factory = new RequestFactory();
        $this->assertInstanceOf(
            \Psr\Http\Message\RequestInterface::class,
            $factory->createRequest('GET', 'https://www.example.com/')
        );
    }
}
