<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\ServerRequest::class)]
#[CoversClass(\StoreCore\Engine\Request::class)]
#[CoversClass(\StoreCore\Engine\Message::class)]
final class ServerRequestTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ServerRequest class file exists')]
    public function testServerRequestClassFileExists()
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'ServerRequest.php'
        );
    }

    #[Depends('testServerRequestClassFileExists')]
    #[Group('distro')]
    #[TestDox('ServerRequest class file is readable')]
    public function testServerRequestClassFileIsReadable()
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'ServerRequest.php'
        );
    }

    #[Depends('testServerRequestClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('ServerRequest class is readable')]
    public function testServerRequestClassIsReadable()
    {
        $this->assertTrue(class_exists('\\StoreCore\\Engine\\ServerRequest'));
        $this->assertTrue(class_exists(ServerRequest::class));
    }


    #[Group('hmvc')]
    #[TestDox('ServerRequest class is concrete')]
    public function testServerRequestClassIsConcrete(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Extended Request class file exists')]
    public function testExtendedRequestClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'Request.php'
        );
    }

    #[Depends('testExtendedRequestClassFileExists')]
    #[Group('hmvc')]
    #[TestDox('Extended Request class file is readable')]
    public function testExtendedRequestClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'Request.php'
        );
    }

    #[Depends('testExtendedRequestClassFileIsReadable')]
    #[Group('hmvc')]
    #[TestDox('ServerRequest is a Request')]
    public function testServerRequestIsARequest(): void
    {
        $this->assertInstanceOf(\StoreCore\Engine\Request::class, new ServerRequest());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 ServerRequestInterface exists')]
    public function testImplementedPSR7ServerRequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ServerRequestInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\ServerRequestInterface::class));
    }

    #[Depends('testServerRequestClassIsConcrete')]
    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServerRequest implements PSR-7 ServerRequestInterface')]
    public function testServerRequestImplementsPSR7ServerRequestInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, new ServerRequest());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 RequestInterface exists')]
    public function testImplementedPSR7RequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\RequestInterface::class));
    }

    #[Depends('testServerRequestClassIsConcrete')]
    #[Depends('testImplementedPSR7RequestInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServerRequest implements PSR-7 RequestInterface')]
    public function testServerRequestImplementsPSR7RequestInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, new ServerRequest());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ServerRequest::VERSION);
        $this->assertIsString(ServerRequest::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ServerRequest::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ServerRequest::get() exists')]
    public function testServerRequestGetExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testServerRequestGetExists')]
    #[TestDox('ServerRequest::get() is public')]
    public function testServerRequestGetIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetExists')]
    #[TestDox('ServerRequest::get() has one REQUIRED parameter')]
    public function testServerRequestGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestGetHasOneRequiredParameter')]
    #[TestDox('ServerRequest::get() returns null on invalid argument')]
    public function testServerRequestGetReturnsNullOnInvalidArgument(): void
    {
        $request = new ServerRequest();
        $this->assertNull($request->get(false));
    }

    #[Depends('testServerRequestGetHasOneRequiredParameter')]
    #[TestDox('ServerRequest::get() returns null on missing value')]
    public function testServerRequestGetReturnsNullOnMissingValue(): void
    {
        $request = new ServerRequest();
        $this->assertNull($request->get('Foo'));
    }


    #[TestDox('ServerRequest::getAttribute() exists')]
    public function testServerRequestGetAttributeExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getAttribute'));
    }

    #[Depends('testServerRequestGetAttributeExists')]
    #[TestDox('ServerRequest::getAttribute() is public')]
    public function testServerRequestGetAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetAttributeExists')]
    #[TestDox('ServerRequest::getAttribute() has two parameters')]
    public function testServerRequestGetAttributeHasTwoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getAttribute');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetAttributeHasTwoParameters')]
    #[TestDox('ServerRequest::getAttribute() has one REQUIRED parameter')]
    public function testServerRequestGetAttributeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getAttribute');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestGetAttributeHasTwoParameters')]
    #[TestDox('ServerRequest::getAttribute() returns second parameter if attribute does not exist')]
    public function testServerRequestGetAttributeReturnsSecondParameterIfAttributeDoesNotExist(): void
    {
        $server_request = new ServerRequest();
        $this->assertNull($server_request->getAttribute('foo'));
        $this->assertEquals('bar', $server_request->getAttribute('foo', 'bar'));
        $this->assertFalse($server_request->getAttribute('foo', false));
    }


    #[TestDox('ServerRequest::getAttributes() exists')]
    public function testServerRequestGetAttributesExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getAttributes'));
    }

    #[Depends('testServerRequestGetAttributesExists')]
    #[TestDox('ServerRequest::getAttributes() is public')]
    public function testServerRequestGetAttributesIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getAttributes');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetAttributesExists')]
    #[TestDox('ServerRequest::getAttributes() has no parameters')]
    public function testServerRequestGetAttributesHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getAttributes');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetAttributesExists')]
    #[Depends('testServerRequestGetAttributesIsPublic')]
    #[Depends('testServerRequestGetAttributesHasNoParameters')]
    #[TestDox('ServerRequest::getAttributes() returns empty array by default')]
    public function testServerRequestGetAttributesReturnsEmptyArrayByDefault(): void
    {
        $server_request = new ServerRequest();
        $this->assertEmpty($server_request->getAttributes());
        $this->assertIsArray($server_request->getAttributes());
    }


    #[TestDox('ServerRequest::getCookieParams() exists')]
    public function testServerRequestGetCookieParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getCookieParams'));
    }

    #[Depends('testServerRequestGetCookieParamsExists')]
    #[TestDox('ServerRequest::getCookieParams() is public')]
    public function testServerRequestGetCookieParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getCookieParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetCookieParamsExists')]
    #[TestDox('ServerRequest::getCookieParams() has no parameters')]
    public function testServerRequestGetCookieParamsHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getCookieParams');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetCookieParamsExists')]
    #[Depends('testServerRequestGetCookieParamsIsPublic')]
    #[Depends('testServerRequestGetCookieParamsHasNoParameters')]
    #[TestDox('ServerRequest::getCookieParams() returns array')]
    public function testServerRequestGetCookieParamsReturnsArray(): void
    {
        $server_request = new ServerRequest();
        $this->assertIsArray($server_request->getCookieParams());
    }


    #[TestDox('ServerRequest::getQueryParams() exists')]
    public function testServerRequestGetQueryParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getQueryParams'));
    }

    #[Depends('testServerRequestGetQueryParamsExists')]
    #[TestDox('ServerRequest::getQueryParams() is public')]
    public function testServerRequestGetQueryParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getQueryParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetQueryParamsExists')]
    #[TestDox('ServerRequest::getQueryParams() has no parameters')]
    public function testServerRequestGetQueryParamsHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getQueryParams');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetQueryParamsExists')]
    #[Depends('testServerRequestGetQueryParamsIsPublic')]
    #[Depends('testServerRequestGetQueryParamsHasNoParameters')]
    #[TestDox('ServerRequest::getQueryParams() returns array')]
    public function testServerRequestGetQueryParamsReturnsArray(): void
    {
        $server_request = new ServerRequest();
        $this->assertIsArray($server_request->getQueryParams());
    }

    #[Depends('testServerRequestGetQueryParamsReturnsArray')]
    #[TestDox('ServerRequest::getQueryParams() MAY return empty array')]
    public function testServerRequestGetQueryParamsMayReturnEmptyArray(): void
    {
        $server_request = new ServerRequest();
        $this->assertEmpty($server_request->getQueryParams());
    }


    #[TestDox('ServerRequest::getRemoteAddress() exists')]
    public function testServerRequestGetRemoteAddressExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getRemoteAddress'));
    }

    #[Depends('testServerRequestGetRemoteAddressExists')]
    #[TestDox('ServerRequest::getRemoteAddress() is public')]
    public function testServerRequestGetRemoteAddressIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getRemoteAddress');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetRemoteAddressExists')]
    #[TestDox('ServerRequest::getRemoteAddress() has no parameters')]
    public function testServerRequestGetRemoteAddressHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getRemoteAddress');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetRemoteAddressExists')]
    #[Depends('testServerRequestGetRemoteAddressIsPublic')]
    #[Depends('testServerRequestGetRemoteAddressHasNoParameters')]
    #[TestDox('ServerRequest::getRemoteAddress() returns string')]
    public function testServerRequestGetRemoteAddressReturnsString(): void
    {
        $request = new ServerRequest();
        $this->assertIsString($request->getRemoteAddress());
    }


    #[TestDox('ServerRequest::getServerParam() exists')]
    public function testServerRequestGetServerParamExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getServerParam'));
    }

    #[Depends('testServerRequestGetServerParamExists')]
    #[TestDox('ServerRequest::getServerParam() is public')]
    public function testServerRequestGetServerParamIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getServerParam');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetServerParamExists')]
    #[TestDox('ServerRequest::getServerParam() has one REQUIRED parameter')]
    public function testServerRequestGetServerParamHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getServerParam');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestGetServerParamExists')]
    #[Depends('testServerRequestGetServerParamIsPublic')]
    #[Depends('testServerRequestGetServerParamHasOneRequiredParameter')]
    #[TestDox('ServerRequest::getServerParam() returns a server parameter')]
    public function testServerRequestGetServerParamReturnsServerParameter(): void
    {
        $server_request = new ServerRequest();
        $this->assertSame('GET', $server_request->getServerParam('REQUEST_METHOD'));
    }


    #[TestDox('ServerRequest::getServerParams() exists')]
    public function testServerRequestGetServerParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getServerParams'));
    }

    #[Depends('testServerRequestGetServerParamsExists')]
    #[TestDox('ServerRequest::getServerParams() is public')]
    public function testServerRequestGetServerParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getServerParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetServerParamsExists')]
    #[TestDox('ServerRequest::getServerParams() has no parameters')]
    public function testServerRequestGetServerParamsHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getServerParams');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetServerParamsExists')]
    #[Depends('testServerRequestGetServerParamsIsPublic')]
    #[Depends('testServerRequestGetServerParamsHasNoParameters')]
    #[TestDox('ServerRequest::getServerParams() has no parameters')]
    public function testServerRequestGetServerParamsReturnsNonEmptyArrayByDefault(): void
    {
        $server_request = new ServerRequest();
        $this->assertNotEmpty($server_request->getServerParams());
        $this->assertIsArray($server_request->getServerParams());
    }


    #[TestDox('ServerRequest::getUploadedFiles() exists')]
    public function testServerRequestGetUploadedFilesExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('getUploadedFiles'));
    }

    #[Depends('testServerRequestGetUploadedFilesExists')]
    #[TestDox('ServerRequest::getUploadedFiles() is public')]
    public function testServerRequestGetUploadedFilesIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getUploadedFiles');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestGetUploadedFilesExists')]
    #[TestDox('ServerRequest::getUploadedFiles() has no parameters')]
    public function testServerRequestGetUploadedFilesHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'getUploadedFiles');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestGetUploadedFilesExists')]
    #[TestDox('ServerRequest::getUploadedFiles() returns empty array by default')]
    public function testServerRequestGetUploadedFilesReturnsEmptyArrayByDefault(): void
    {
        $server_request = new ServerRequest();
        $this->assertEmpty($server_request->getUploadedFiles());
        $this->assertIsArray($server_request->getUploadedFiles());
    }


    #[TestDox('ServerRequest::setAttribute() exists')]
    public function testServerRequestSetAttributeExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setAttribute'));
    }

    #[Depends('testServerRequestSetAttributeExists')]
    #[TestDox('ServerRequest::setAttribute() is public')]
    public function testServerRequestSetAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetAttributeExists')]
    #[TestDox('ServerRequest::setAttribute() has two REQUIRED parameters')]
    public function testServerRequestSetAttributeHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setAttribute');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('testServerRequestSetCookieParamsExists')]
    public function testServerRequestSetCookieParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setCookieParams'));
    }

    #[Depends('testServerRequestSetCookieParamsExists')]
    #[TestDox('ServerRequest::setCookieParams() is public')]
    public function testServerRequestSetCookieParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setCookieParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetCookieParamsExists')]
    #[TestDox('ServerRequest::setCookieParams() has one REQUIRED parameter')]
    public function testServerRequestSetCookieParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setCookieParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('ServerRequest::setParsedBody() exists')]
    public function testServerRequestSetParsedBodyExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setParsedBody'));
    }

    #[Depends('testServerRequestSetParsedBodyExists')]
    #[TestDox('ServerRequest::setParsedBody() is public')]
    public function testServerRequestSetParsedBodyIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setParsedBody');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetParsedBodyExists')]
    #[TestDox('ServerRequest::setParsedBody() has one REQUIRED parameter')]
    public function testServerRequestSetParsedBodyHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setParsedBody');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestSetParsedBodyExists')]
    #[Depends('testServerRequestSetParsedBodyIsPublic')]
    #[Depends('testServerRequestSetParsedBodyHasOneRequiredParameter')]
    #[TestDox('ServerRequest::setParsedBody() sets server request body')]

    public function testServerRequestSetParsedBodyReturnsPsr7ServerRequestInterface(): void
    {
        // HTTP POST request
        $_POST = [
            'name'  => 'Alice Bob',
            'email' => 'alice@example.com',
        ];

        $request = new ServerRequest();
        $this->assertNull($request->getParsedBody());

        $request->setParsedBody($_POST);
        $this->assertEquals($_POST, $request->getParsedBody());
    }


    #[TestDox('ServerRequest::setQueryParams() exists')]
    public function testServerRequestSetQueryParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setQueryParams'));
    }

    #[Depends('testServerRequestSetQueryParamsExists')]
    #[TestDox('ServerRequest::setQueryParams() exists')]
    public function testServerRequestSetQueryParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setQueryParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetQueryParamsExists')]
    #[TestDox('ServerRequest::setQueryParams() has one REQUIRED parameter')]
    public function testServerRequestSetQueryParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setQueryParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('ServerRequest::setServerParams() exists')]
    public function testServerRequestSetServerParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setServerParams'));
    }

    #[Depends('testServerRequestSetServerParamsExists')]
    #[TestDox('ServerRequest::setServerParams() is public')]
    public function testServerRequestSetServerParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setServerParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetServerParamsExists')]
    #[TestDox('ServerRequest::setServerParams() has one REQUIRED parameter')]
    public function testServerRequestSetServerParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setServerParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('ServerRequest::setUploadedFiles() exists')]
    public function testServerRequestSetUploadedFilesExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('setUploadedFiles'));
    }

    #[Depends('testServerRequestSetUploadedFilesExists')]
    #[TestDox('ServerRequest::setUploadedFiles() is public')]
    public function testServerRequestSetUploadedFilesIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setUploadedFiles');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestSetUploadedFilesExists')]
    #[TestDox('ServerRequest::setUploadedFiles() has one REQUIRED parameter')]
    public function testServerRequestSetUploadedFilesHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'setUploadedFiles');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('ServerRequest::unsetAttribute() exists')]
    public function testServerRequestUnsetAttributeExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('unsetAttribute'));
    }

    #[Depends('testServerRequestUnsetAttributeExists')]
    #[TestDox('ServerRequest::unsetAttribute() is public')]
    public function testServerRequestUnsetAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'unsetAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestUnsetAttributeExists')]
    #[TestDox('ServerRequest::unsetAttribute() has one REQUIRED parameter')]
    public function testServerRequestUnsetAttributeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'unsetAttribute');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestUnsetAttributeExists')]
    #[Depends('testServerRequestUnsetAttributeIsPublic')]
    #[Depends('testServerRequestUnsetAttributeHasOneRequiredParameter')]
    #[TestDox('ServerRequest::unsetAttribute() has one REQUIRED parameter')]
    public function testServerRequestUnsetAttributeReturnsTrueOnSuccess(): void
    {
        $server_request = new ServerRequest();
        $server_request->setAttribute('foo', 'bar');
        $this->assertTrue($server_request->unsetAttribute('foo'));
    }

    #[Depends('testServerRequestUnsetAttributeReturnsTrueOnSuccess')]
    #[TestDox('ServerRequest::unsetAttribute() returns false if attribute does not exist')]
    public function testServerRequestUnsetAttributeReturnsFalseIfAttributeDoesNotExist(): void
    {
        $server_request = new ServerRequest();
        $this->assertFalse($server_request->unsetAttribute('foo'));
    }


    #[TestDox('ServerRequest::withAttribute() exists')]
    public function testServerRequestWithAttributeExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withAttribute'));
    }

    #[Depends('testServerRequestWithAttributeExists')]
    #[TestDox('ServerRequest::withAttribute() is public')]
    public function testServerRequestWithAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithAttributeExists')]
    #[TestDox('ServerRequest::withAttribute() has two REQUIRED parameters')]
    public function testServerRequestWithAttributeHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withAttribute');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithAttributeExists')]
    #[Depends('testServerRequestWithAttributeIsPublic')]
    #[Depends('testServerRequestWithAttributeHasTwoRequiredParameters')]
    #[TestDox('ServerRequest::withAttribute() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithAttributeReturnsPsr7ServerRequestInterface(): void
    {
        $request = new ServerRequest();
        $new_request_instance = $request->withAttribute('foo', 'bar');
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withoutAttribute() exists')]
    public function testServerRequestWithoutAttributeExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withoutAttribute'));
    }

    #[Depends('testServerRequestWithoutAttributeExists')]
    #[TestDox('ServerRequest::withoutAttribute() is public')]
    public function testServerRequestWithoutAttributeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withoutAttribute');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithoutAttributeExists')]
    #[TestDox('ServerRequest::withoutAttribute() has one REQUIRED parameter')]
    public function testServerRequestWithoutAttributeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withoutAttribute');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithoutAttributeExists')]
    #[Depends('testServerRequestWithoutAttributeIsPublic')]
    #[Depends('testServerRequestWithoutAttributeHasOneRequiredParameter')]
    #[TestDox('ServerRequest::withoutAttribute() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithoutAttributeReturnsPsr7ServerRequestInterface(): void
    {
        $request = new ServerRequest();
        $request->setAttribute('foo', 'bar');

        $new_request_instance = $request->withoutAttribute('foo', 'bar');
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withCookieParams() exists')]
    public function testServerRequestWithCookieParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withCookieParams'));
    }

    #[Depends('testServerRequestWithCookieParamsExists')]
    #[TestDox('ServerRequest::withCookieParams() is public')]
    public function testServerRequestWithCookieParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withCookieParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithCookieParamsExists')]
    #[TestDox('ServerRequest::withCookieParams() has one REQUIRED parameter')]
    public function testServerRequestWithCookieParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withCookieParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithCookieParamsExists')]
    #[Depends('testServerRequestWithCookieParamsIsPublic')]
    #[Depends('testServerRequestWithCookieParamsHasOneRequiredParameter')]
    #[TestDox('ServerRequest::withCookieParams() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithCookieParamsReturnsPsr7ServerRequestInterface(): void
    {
        $request = new ServerRequest();
        $params = array('PHPSESSID' => 'engjq09365i8jukojlbpvk36t0');
        $new_request_instance = $request->withCookieParams($params);
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withParsedBody() exists')]
    public function testServerRequestWithParsedBodyExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withParsedBody'));
    }

    #[Depends('testServerRequestWithParsedBodyExists')]
    #[TestDox('ServerRequest::withParsedBody() is public')]
    public function testServerRequestWithParsedBodyIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withParsedBody');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithParsedBodyExists')]
    #[TestDox('ServerRequest::withParsedBody() has one REQUIRED parameter')]
    public function testServerRequestWithParsedBodyHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withParsedBody');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithParsedBodyExists')]
    #[Depends('testServerRequestWithParsedBodyIsPublic')]
    #[Depends('testServerRequestWithParsedBodyHasOneRequiredParameter')]
    #[TestDox('ServerRequest::withParsedBody() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithParsedBodyReturnsPsr7ServerRequestInterface(): void
    {
        // HTTP POST request
        $_POST = [
            'name'  => 'Alice Bob',
            'email' => 'alice@example.com',
        ];

        $request = new ServerRequest();
        $new_request_instance = $request->withParsedBody($_POST);
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withQueryParams() exists')]
    public function testServerRequestWithQueryParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withQueryParams'));
    }

    #[Depends('testServerRequestWithQueryParamsExists')]
    #[TestDox('ServerRequest::withQueryParams() is public')]
    public function testServerRequestWithQueryParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withQueryParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithQueryParamsExists')]
    #[TestDox('ServerRequest::withQueryParams() has one REQUIRED parameter')]
    public function testServerRequestWithQueryParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withQueryParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithQueryParamsExists')]
    #[Depends('testServerRequestWithQueryParamsIsPublic')]
    #[Depends('testServerRequestWithQueryParamsHasOneRequiredParameter')]
    #[TestDox('ServerRequest::withQueryParams() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithQueryParamsReturnsPsr7ServerRequestInterface(): void
    {
        // URL with `?q=foo+bar&hl=en` query string
        $query = [
            'q'  => 'foo bar',
            'hl' => 'en',
        ];

        $request = new ServerRequest();
        $new_request_instance = $request->withQueryParams($query);
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withServerParams() exists')]
    public function testServerRequestWithServerParamsExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withServerParams'));
    }

    #[Depends('testServerRequestWithServerParamsExists')]
    #[TestDox('ServerRequest::withServerParams() is public')]
    public function testServerRequestWithServerParamsIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withServerParams');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithServerParamsExists')]
    #[TestDox('ServerRequest::withServerParams() has one REQUIRED parameter')]
    public function testServerRequestWithServerParamsHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withServerParams');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testImplementedPSR7ServerRequestInterfaceExists')]
    #[Depends('testServerRequestWithServerParamsExists')]
    #[Depends('testServerRequestWithServerParamsIsPublic')]
    #[Depends('testServerRequestWithServerParamsHasOneRequiredParameter')]
    #[TestDox('ServerRequest::withServerParams() returns PSR-7 ServerRequestInterface')]
    public function testServerRequestWithServerParamsReturnsPsr7ServerRequestInterface(): void
    {
        // Basic HTTP request
        $params = [
            'SERVER_ADDR'     => '::1',
            'SERVER_PORT'     => '80',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
        ];

        $request = new ServerRequest();
        $new_request_instance = $request->withServerParams($params);
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $new_request_instance);
    }


    #[TestDox('ServerRequest::withUploadedFiles() exists')]
    public function testServerRequestWithUploadedFilesExists(): void
    {
        $class = new ReflectionClass(ServerRequest::class);
        $this->assertTrue($class->hasMethod('withUploadedFiles'));
    }

    #[Depends('testServerRequestWithUploadedFilesExists')]
    #[TestDox('ServerRequest::withUploadedFiles() is public')]
    public function testServerRequestWithUploadedFilesIsPublic(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withUploadedFiles');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerRequestWithUploadedFilesExists')]
    #[TestDox('ServerRequest::withUploadedFiles() has one REQUIRED parameter')]
    public function testServerRequestWithUploadedFilesHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerRequest::class, 'withUploadedFiles');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
