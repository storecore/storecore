<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Engine\AccessControlHandler::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\RequestFactory::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class AccessControlHandlerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AccessControlHandler class is concrete')]
    public function testAccessControlHandlerClassIsConcrete(): void
    {
        $class = new ReflectionClass(AccessControlHandler::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('AccessControlHandler is a controller')]
    public function testAccessControlHandlerIsController(): void
    {
        $class = new ReflectionClass(AccessControlHandler::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-15 RequestHandlerInterface exists')]
    public function testImplementedPSR15RequestHandlerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Server\RequestHandlerInterface::class));
    }

    #[Depends('testImplementedPSR15RequestHandlerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('AccessControlHandler implements PSR-15 RequestHandlerInterface')]
    public function testAccessControlHandlerImplementsPsr15RequestHandlerInterface(): void
    {
        $class = new ReflectionClass(AccessControlHandler::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Server\RequestHandlerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AccessControlHandler::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AccessControlHandler::VERSION);
        $this->assertIsString(AccessControlHandler::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AccessControlHandler::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AccessControlHandler::handle exists')]
    public function testAccessControlHandlerHandleExists(): void
    {
        $class = new ReflectionClass(\StoreCore\Engine\AccessControlHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testAccessControlHandlerHandleExists')]
    #[TestDox('AccessControlHandler::handle is public')]
    public function testAccessControlHandlerHandleIsPublic(): void
    {
        $method = new ReflectionMethod(AccessControlHandler::class, 'handle');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAccessControlHandlerHandleExists')]
    #[TestDox('AccessControlHandler::handle has one REQUIRED parameter')]
    public function testAccessControlHandlerHandleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AccessControlHandler::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAccessControlHandlerHandleHasOneRequiredParameter')]
    #[TestDox('AccessControlHandler::handle requires server request')]
    public function testAccessControlHandlerHandleRequiresServerRequest(): void
    {
        $factory = new \StoreCore\Engine\RequestFactory();
        $request = $factory->createRequest('GET', 'https://www.example.com/');
        $this->assertNotInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $request);

        $registry = Registry::getInstance();
        $handler = new \StoreCore\Engine\AccessControlHandler($registry);
        $this->expectException(\TypeError::class);
        $failure = $handler->handle($request);
    }

    #[Depends('testAccessControlHandlerHandleExists')]
    #[Depends('testAccessControlHandlerHandleIsPublic')]
    #[Depends('testAccessControlHandlerHandleHasOneRequiredParameter')]
    #[TestDox('AccessControlHandler::handle returns response')]
    public function testAccessControlHandlerHandleReturnsResponse(): void
    {
        $factory = new \StoreCore\Engine\ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/');
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $request);

        $registry = Registry::getInstance();
        $handler = new \StoreCore\Engine\AccessControlHandler($registry);
        $response = $handler->handle($request);

        $this->assertIsObject($response);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);

        $this->assertNotEmpty($response->getStatusCode());
        $this->assertIsInt($response->getStatusCode());

        $this->assertTrue(
               $response->getStatusCode() === 100
            || $response->getStatusCode() === 403
            || $response->getStatusCode() === 502
        );

        if (STORECORE_CMS_BLOCKLIST === false) {
            $this->assertTrue($response->getStatusCode() === 100);
        }

        if (STORECORE_CMS_BLOCKLIST === 0) {
            $this->assertEquals(100, $response->getStatusCode());
        }
    }
}
