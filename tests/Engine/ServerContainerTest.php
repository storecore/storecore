<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(ServerContainer::class)]
final class ServerContainerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ServerContainer class is concrete')]
    public function testServerContainerClassIsConcrete(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServerContainer implements PSR-11 ContainerInterface')]
    public function testServerContainerImplementsPSR11ContainerInterface(): void
    {
        $class = new ServerContainer();
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $class);
    }


    #[TestDox('ServerContainer is JSON serializable')]
    public function testServerContainerIsJsonSerializable(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));

        $class = new ServerContainer();
        $this->assertInstanceOf(\JsonSerializable::class, $class);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ServerContainer::VERSION);
        $this->assertIsString(ServerContainer::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ServerContainer::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('ServerContainer::__construct exists')]
    public function testServerContainerConstructExists(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('ServerContainer::__construct is public constructor')]
    public function testServerContainerConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('ServerContainer::__construct has one OPTIONAL parameter')]
    public function testServerContainerConstructHasOneOptionalParameters(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('ServerContainer::get exists')]
    public function testServerContainerGetExists(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('ServerContainer::get is public')]
    public function testServerContainerGetIsPublic(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('ServerContainer::get has one REQUIRED parameter')]
    public function testServerContainerGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerContainerGetHasOneRequiredParameter')]
    #[TestDox('ServerContainer::get throws Psr\Container\NotFoundExceptionInterface when key does not exist')]
    public function testServerContainerGetThrowsPsrContainerNotFoundExceptionInterfaceWhenKeyDoesNotExist(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));

        $container = new ServerContainer();
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('KeyThatDoesNotExist');
    }

    #[Depends('testServerContainerGetExists')]
    #[Depends('testServerContainerGetIsPublic')]
    #[Depends('testServerContainerGetHasOneRequiredParameter')]
    #[TestDox('ServerContainer::get returns server parameters')]
    public function testServerContainerGetReturnsServerParameters(): void
    {
        $server_params = [
            'REQUEST_TIME' => 1644829662,
            'REQUEST_TIME_FLOAT' => 1644829662.410466,
        ];
        $container = new ServerContainer($server_params);

        $this->assertIsInt($container->get('REQUEST_TIME'));
        $this->assertSame(1644829662, $container->get('REQUEST_TIME'));

        $this->assertIsFloat($container->get('REQUEST_TIME_FLOAT'));
        $this->assertSame(1644829662.410466, $container->get('REQUEST_TIME_FLOAT'));
    }

    #[Depends('testServerContainerGetHasOneRequiredParameter')]
    #[TestDox('ServerContainer::get is case sensitive')]
    public function testServerContainerGetReturnIsCaseSensitive(): void
    {
        $container = new ServerContainer(
            [
                'HTTP_ACCEPT_LANGUAGE' => 'en',
                'http_accept_language' => 'nl-NL,nl;q=0.9',
            ]
        );

        $this->assertSame('en', $container->get('HTTP_ACCEPT_LANGUAGE'));
        $this->assertSame('nl-NL,nl;q=0.9', $container->get('http_accept_language'));
        $this->assertNotSame($container->get('http_accept_language'), $container->get('HTTP_ACCEPT_LANGUAGE'));

        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $container->get('Http_Accept_Language');
    }


    #[TestDox('ServerContainer::has exists')]
    public function testServerContainerHasExists(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testServerContainerHasExists')]
    #[TestDox('ServerContainer::has is public')]
    public function testServerContainerHasIsPublic(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'has');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testServerContainerHasExists')]
    #[TestDox('ServerContainer::has has one REQUIRED parameter')]
    public function testServerContainerHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerContainerHasExists')]
    #[Depends('testServerContainerHasIsPublic')]
    #[Depends('testServerContainerHasHasOneRequiredParameter')]
    #[TestDox('ServerContainer::has returns boolean')]
    public function testServerContainerHasReturnsBoolean(): void
    {
        $container = new ServerContainer();
        $this->assertIsBool($container->has('Foo'));
        $this->assertFalse($container->has('Foo'));

        $container = new ServerContainer(array('Foo' => 'Bar'));
        $this->assertIsBool($container->has('Foo'));
        $this->assertTrue($container->has('Foo'));
    }

    #[Depends('testServerContainerHasReturnsBoolean')]
    #[TestDox('ServerContainer::has returns false by default')]
    public function testServerContainerHasReturnsFalseByDefault(): void
    {
        $container = new ServerContainer();
        $this->assertFalse($container->has('NoFooNoGlory'));
    }

    #[Depends('testServerContainerHasReturnsBoolean')]
    #[TestDox('ServerContainer::has is case sensitive')]
    public function testServerContainerHasIsCaseSensitive(): void
    {
        $server_params = [
            'foo' => '123',
            'Foo' => '456',
        ];
        $container = new ServerContainer($server_params);

        $this->assertTrue($container->has('foo'));
        $this->assertTrue($container->has('Foo'));
        $this->assertFalse($container->has('FOO'));
    }


    #[TestDox('ServerContainer::jsonSerialize exists')]
    public function testServerContainerJsonSerializeExists(): void
    {
        $class = new ReflectionClass(ServerContainer::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[Depends('testServerContainerJsonSerializeExists')]
    #[TestDox('ServerContainer::jsonSerialize is public')]
    public function testServerContainerJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'jsonSerialize');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testServerContainerJsonSerializeExists')]
    #[TestDox('ServerContainer::jsonSerialize has no parameters')]
    public function testServerContainerJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(ServerContainer::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testServerContainerJsonSerializeExists')]
    #[Depends('testServerContainerJsonSerializeIsPublic')]
    #[Depends('testServerContainerJsonSerializeHasNoParameters')]
    #[TestDox('ServerContainer::jsonSerialize returns array')]
    public function testServerContainerJsonSerializeReturnsArray(): void
    {
        $container = new ServerContainer();
        $this->assertIsArray($container->jsonSerialize());
    }

    #[Depends('testServerContainerJsonSerializeReturnsArray')]
    #[TestDox('ServerContainer::jsonSerialize returns non-empty array')]
    public function testServerContainerJsonSerializeReturnsNonEmptyArray(): void
    {
        $container = new ServerContainer();
        $this->assertNotEmpty($container->jsonSerialize());
    }


    #[Depends('testServerContainerImplementsPSR11ContainerInterface')]
    #[TestDox('ServerContainer::get("REQUEST_METHOD") is not empty')]
    public function testServerContainerGetRequestMethodIsNotEmpty(): void
    {
        $server = new ServerContainer();
        $this->assertTrue($server->has('REQUEST_METHOD'));
        $this->assertNotEmpty($server->get('REQUEST_METHOD'));
    }

    #[Depends('testServerContainerGetRequestMethodIsNotEmpty')]
    #[TestDox('ServerContainer::get("REQUEST_METHOD") is (string) GET by default')]
    public function testServerContainerGetRequestMethodIsStringGetByDefault(): void
    {
        $server = new ServerContainer();
        $this->assertIsString($server->get('REQUEST_METHOD'));
        $this->assertEquals('GET', $server->get('REQUEST_METHOD'));
    }
}
