<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\RequestStream::class)]
final class RequestStreamTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('RequestStream class is concrete')]
    public function testRequestStreamClassIsConcrete(): void
    {
        $class = new ReflectionClass(RequestStream::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testRequestStreamClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('RequestStream extends AbstractStream')]
    public function testRequestStreamExtendsAbstractStream(): void
    {
        $stream = new RequestStream();
        $this->assertInstanceOf(\StoreCore\Engine\AbstractStream::class, $stream);
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testImplementedPSR7StreamInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\StreamInterface::class));
    }

    #[Depends('testImplementedPSR7StreamInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testRequestStreamImplementsPSR7StreamInterface(): void
    {
        $object = new RequestStream();
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, $object);
    }


    #[TestDox('RequestStream::$writable exists')]
    public function testRequestStreamWritableExists(): void
    {
        $stream = new RequestStream();
        $this->assertObjectHasProperty('writable', $stream);
    }

    #[TestDox('RequestStream::isWritable exists')]
    public function testRequestStreamIsWritableExists(): void
    {
        $class = new ReflectionClass(RequestStream::class);
        $this->assertTrue($class->hasMethod('isWritable'));
    }

    #[Depends('testRequestStreamIsWritableExists')]
    #[TestDox('RequestStream::isWritable is final public')]
    public function testRequestStreamIsWritableIsFinalPublic(): void
    {
        $method = new ReflectionMethod(RequestStream::class, 'isWritable');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRequestStreamIsWritableExists')]
    #[TestDox('RequestStream::isWritable has no parameters')]
    public function testRequestStreamIsWritableHasNoParameters(): void
    {
        $method = new ReflectionMethod(RequestStream::class, 'isWritable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRequestStreamIsWritableExists')]
    #[Depends('testRequestStreamIsWritableIsFinalPublic')]
    #[Depends('testRequestStreamIsWritableHasNoParameters')]
    #[TestDox('RequestStream::isWritable returns bool')]
    public function testRequestStreamIsWritableReturnsBool(): void
    {
        $requestStream = new RequestStream();
        $this->assertIsBool($requestStream->isWritable());
    }

    #[Depends('testRequestStreamIsWritableReturnsBool')]
    #[TestDox('RequestStream::isWritable returns false')]
    public function testRequestStreamIsWritableReturnsFalse(): void
    {
        $requestStream = new RequestStream();
        $this->assertFalse($requestStream->isWritable());
    }
}
