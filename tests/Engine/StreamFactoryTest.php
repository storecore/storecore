<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\StreamFactory::class)]
#[CoversClass(\StoreCore\Engine\TemporaryStream::class)]
#[CoversClass(\StoreCore\FileSystem\FileStream::class)]
#[Group('hmvc')]
final class StreamFactoryTest extends TestCase
{
    #[TestDox('StreamFactory is concrete')]
    public function testStreamFactoryIsConcrete(): void
    {
        $class = new ReflectionClass(StreamFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-17 StreamFactoryInterface exists')]
    public function testImplementedPsr17StreamFactoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamFactoryInterface'));
    }

    #[Depends('testStreamFactoryIsConcrete')]
    #[Depends('testImplementedPsr17StreamFactoryInterfaceExists')]
    #[TestDox('Implemented PSR-17 StreamFactoryInterface exists')]
    public function testStreamFactoryImplementsPSR17StreamFactoryInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\StreamFactoryInterface::class, new StreamFactory());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(StreamFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(StreamFactory::VERSION);
        $this->assertIsString(StreamFactory::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(StreamFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('StreamFactory::createStream exists')]
    public function testStreamFactoryCreateStreamExists(): void
    {
        $class = new ReflectionClass(StreamFactory::class);
        $this->assertTrue($class->hasMethod('createStream'));
    }

    #[TestDox('StreamFactory::createStream is public')]
    public function testStreamFactoryCreateStreamIsPublic(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStream');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StreamFactory::createStream has one OPTIONAL parameter')]
    public function testStreamFactoryCreateStreamHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStream');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testStreamFactoryCreateStreamExists')]
    #[Depends('testStreamFactoryCreateStreamIsPublic')]
    #[Depends('testStreamFactoryCreateStreamHasOneOptionalParameter')]
    #[TestDox('StreamFactory::createStream returns PSR-7 StreamInterface')]
    public function testStreamFactoryCreateStreamReturnsPsr7StreamInterface(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));

        $factory = new StreamFactory();
        $stream = $factory->createStream();
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, $stream);
    }


    #[TestDox('StreamFactory::createStreamFromFile exists')]
    public function testStreamFactoryCreateStreamFromFileExists(): void
    {
        $class = new ReflectionClass(StreamFactory::class);
        $this->assertTrue($class->hasMethod('createStreamFromFile'));
    }

    #[TestDox('StreamFactory::createStreamFromFile is public')]
    public function testStreamFactoryCreateStreamFromFileIsPublic(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStreamFromFile');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StreamFactory::createStreamFromFile has two parameters')]
    public function testStreamFactoryCreateStreamFromFileHasTwoParameters(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStreamFromFile');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testStreamFactoryCreateStreamFromFileHasTwoParameters')]
    #[TestDox('StreamFactory::createStreamFromFile has one REQUIRED parameter')]
    public function testStreamFactoryCreateStreamFromFileHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStreamFromFile');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testStreamFactoryCreateStreamFromFileHasOneRequiredParameter')]
    #[TestDox('StreamFactory::createStreamFromFile returns file as file stream')]
    public function testStreamFactoryCreateStreamFromFileReturnsFileAsFileStream(): void
    {
        $this->assertTrue(\is_file(__FILE__));
        $this->assertTrue(\is_readable(__FILE__));

        $factory = new StreamFactory();
        $stream = $factory->createStreamFromFile(__FILE__);
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, $stream);
        $this->assertInstanceOf(\StoreCore\FileSystem\FileStream::class, $stream);

        $stream = null;

        // Illegal read/write mode
        $this->expectException(\RuntimeException::class);
        $stream = $factory->createStreamFromFile(__FILE__, 'whoops');
    }

    #[Depends('testStreamFactoryCreateStreamFromFileReturnsFileAsFileStream')]
    #[TestDox('StreamFactory::createStreamFromFile throws runtime exception if file does not exist')]
    public function testStreamFactoryCreateStreamFromFileThrowsRuntimeExceptionIfFileDoesNotExist(): void
    {
        $factory = new StreamFactory();
        $this->expectException(\RuntimeException::class);
        $stream = $factory->createStreamFromFile('README.txt');
    }

    #[Depends('testStreamFactoryCreateStreamFromFileReturnsFileAsFileStream')]
    #[TestDox('StreamFactory::createStreamFromFile throws runtime exception on directory')]
    public function testStreamFactoryCreateStreamFromFileThrowsRuntimeExceptionOnDirectory(): void
    {
        $this->assertTrue(\is_dir(__DIR__));
        $this->assertTrue(\is_readable(__DIR__));

        $factory = new StreamFactory();
        $this->expectException(\RuntimeException::class);
        $stream = $factory->createStreamFromFile(__DIR__);
    }


    #[TestDox('StreamFactory::createStreamFromResource exists')]
    public function testStreamFactoryCreateStreamFromResourceExists(): void
    {
        $class = new ReflectionClass(StreamFactory::class);
        $this->assertTrue($class->hasMethod('createStreamFromResource'));
    }

    #[TestDox('StreamFactory::createStreamFromResource is public')]
    public function testStreamFactoryCreateStreamFromResourceIsPublic(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStreamFromResource');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('StreamFactory::createStreamFromResource has one REQUIRED parameter')]
    public function testStreamFactoryCreateStreamFromResourceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreamFactory::class, 'createStreamFromResource');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
