<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(Response::class)]
#[CoversClass(ResponseFactory::class)]
#[Group('hmvc')]
#[UsesClass(ResponseCodes::class)]
#[UsesClass(\StoreCore\Registry::class)]
final class ResponseFactoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ResponseFactory class is concrete')]
    public function testResponseFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(ResponseFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-17 ResponseFactoryInterface exists')]
    public function testImplementedPSR17ResponseFactoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ResponseFactoryInterface'));
    }

    #[Depends('testResponseFactoryClassIsConcrete')]
    #[Depends('testImplementedPSR17ResponseFactoryInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-17 ResponseFactoryInterface exists')]
    public function testResponseFactoryImplementsPSR17ResponseFactoryInterface(): void
    {
        $class = new \StoreCore\Engine\ResponseFactory();
        $this->assertInstanceOf(\Psr\Http\Message\ResponseFactoryInterface::class, $class);
    }



    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ResponseFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ResponseFactory::VERSION);
        $this->assertIsString(ResponseFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(ResponseFactory::VERSION, '1.0.0', '>='));
    }


    #[TestDox('ResponseFactory::createResponse exists')]
    public function testResponseFactoryCreateResponseExists(): void
    {
        $class = new ReflectionClass(ResponseFactory::class);
        $this->assertTrue($class->hasMethod('createResponse'));
    }

    #[Depends('testResponseFactoryCreateResponseExists')]
    #[TestDox('ResponseFactory::createResponse is public')]
    public function testResponseFactoryCreateResponseIsPublic(): void
    {
        $method = new ReflectionMethod(ResponseFactory::class, 'createResponse');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testResponseFactoryCreateResponseExists')]
    #[TestDox('ResponseFactory::createResponse has two OPTIONAL parameters')]
    public function testResponseFactoryCreateResponseHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(ResponseFactory::class, 'createResponse');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testResponseFactoryCreateResponseHasTwoOptionalParameters')]
    #[TestDox('ResponseFactory::createResponse returns PSR-7 ResponseInterface')]
    public function testResponseFactoryCreateResponseReturnsPsr7ResponseInterface(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createResponse();
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }

    #[Depends('testResponseFactoryCreateResponseHasTwoOptionalParameters')]
    #[TestDox('ResponseFactory::createResponse returns StoreCore response')]
    public function testResponseFactoryCreateResponseReturnsStoreCoreResponse(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createResponse();
        $this->assertInstanceOf(\StoreCore\Engine\Response::class, $response);
        $this->assertInstanceOf(Response::class, $response);
    }

    #[Depends('testResponseFactoryCreateResponseHasTwoOptionalParameters')]
    #[Depends('testResponseFactoryCreateResponseReturnsPsr7ResponseInterface')]
    #[TestDox('ResponseFactory::createResponse accepts integer as HTTP status code')]
    public function testResponseFactoryCreateResponseAcceptsIntegerAsHttpStatusCode(): void
    {
        $factory = new ResponseFactory();

        $response = $factory->createResponse(400);
        $this->assertSame(400, $response->getStatusCode());

        $response = $factory->createResponse(404);
        $this->assertSame(404, $response->getStatusCode());

        $response = $factory->createResponse(500);
        $this->assertSame(500, $response->getStatusCode());
    }

    #[Depends('testResponseFactoryCreateResponseReturnsPsr7ResponseInterface')]
    #[TestDox('ResponseFactory::createResponse throws \ValueError on invalid status code')]
    public function testResponseFactoryCreateResponseThrowsValueErrorOnInvalidStatusCode(): void
    {
        $factory = new ResponseFactory();
        $this->expectException(\ValueError::class);
        $failure = $factory->createResponse(42);
    }

    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/100
     */
    #[Depends('testResponseFactoryCreateResponseReturnsPsr7ResponseInterface')]
    #[TestDox('ResponseFactory supports HTTP 100 Continue')]
    public function testResponseFactorySupportsHttp100Continue(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createResponse(100);
        $this->assertSame(100, $response->getStatusCode());
        $this->assertSame('Continue', $response->getReasonPhrase());
    }

    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/418
     */
    #[Depends('testResponseFactoryCreateResponseReturnsPsr7ResponseInterface')]
    #[TestDox('ResponseFactory supports HTTP 418 I\'m a teapot')]
    public function testResponseFactorySupportsHttp418Teapot(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createResponse(418);
        $this->assertSame(418, $response->getStatusCode());
        $this->assertSame("I'm a teapot", $response->getReasonPhrase());
    }


    #[TestDox('ResponseFactory::createRedirect exists')]
    public function testResponseFactoryCreateRedirectExists(): void
    {
        $class = new ReflectionClass(ResponseFactory::class);
        $this->assertTrue($class->hasMethod('createRedirect'));
    }

    #[TestDox('ResponseFactory::createRedirect is public')]
    public function testResponseFactoryCreateRedirectIsPublic(): void
    {
        $method = new ReflectionMethod(ResponseFactory::class, 'createRedirect');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('ResponseFactory::createRedirect has two parameters')]
    public function testResponseFactoryCreateRedirectHasTwoParameters(): void
    {
        $method = new ReflectionMethod(ResponseFactory::class, 'createRedirect');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testResponseFactoryCreateRedirectHasTwoParameters')]
    #[TestDox('ResponseFactory::createRedirect has one REQUIRED parameter')]
    public function testResponseFactoryCreateRedirectHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ResponseFactory::class, 'createRedirect');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('ResponseFactory::createRedirect returns PSR-7 MessageInterface')]
    public function testResponseFactoryCreateedirectReturnsPsr7MessageInterface(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createRedirect('https://www.example.com/');
        $this->assertInstanceOf(\Psr\Http\Message\MessageInterface::class, $response);
    }

    #[TestDox('ResponseFactory::createRedirect returns PSR-7 ResponseInterface')]
    public function testResponseFactoryCreateedirectReturnsPsr7ResponseInterface(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createRedirect('https://www.example.com/');
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
    }

    #[TestDox('ResponseFactory::createRedirect returns HTTP redirection message')]
    public function testResponseFactoryCreateRedirectReturnsHttpRedirectionMessage(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createRedirect('https://www.example.com/');

        $this->assertNotEmpty($response->getStatusCode());
        $this->assertIsInt($response->getStatusCode());
        $this->assertTrue($response->getStatusCode() >= 300);
        $this->assertTrue($response->getStatusCode() <= 399);
    }

    #[TestDox('ResponseFactory::createRedirect throws OutOfRangeException on invalid HTTP redirection status code')]
    public function testResponseFactoryCreateRedirectThrowsOutOfRangeExceptionOnInvalidHttpRedirectionStatusCode(): void
    {
        $factory = new ResponseFactory();
        $this->expectException(\OutOfRangeException::class);
        $response = $factory->createRedirect('https://www.example.com/', 404);
    }

    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/303
     */
    #[TestDox('ResponseFactory::createRedirect returns 303 See Other response by default')]
    public function testResponseFactoryCreateRedirectReturns303SeeOtherResponseByDefault(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createRedirect('https://www.example.com/');

        $this->assertSame(303, $response->getStatusCode());
        $this->assertSame('See Other', $response->getReasonPhrase());
    }

    #[TestDox('ResponseFactory::createRedirect returns HTTP Location header with URL')]
    public function testResponseFactoryCreateRedirectReturnsHttpLocationHeaderWithUrl(): void
    {
        $factory = new ResponseFactory();
        $response = $factory->createRedirect('https://www.example.com/');
        $this->assertTrue($response->hasHeader('Location'));
        $this->assertSame('https://www.example.com/', $response->getHeaderLine('Location'));
    }
}
