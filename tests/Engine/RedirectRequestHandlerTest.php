<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use StoreCore\Registry;
use StoreCore\Database\RedirectCache;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Engine\RedirectRequestHandler::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\RedirectCache::class)]
#[UsesClass(\StoreCore\Database\RedirectMapper::class)]
#[UsesClass(\StoreCore\Engine\Redirect::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
#[UsesClass(\StoreCore\Types\CacheKey::class)]
final class RedirectRequestHandlerTest extends TestCase
{
    protected function tearDown(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->exec(
                "DELETE
                   FROM `sc_redirects`
                  WHERE `redirect_from_url` = '//localhost/help'
            ");
        }
    }


    #[TestDox('RedirectRequestHandler class is concrete')]
    public function testRedirectRequestHandlerClassIsConcrete(): void
    {
        $class = new ReflectionClass(RedirectRequestHandler::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('RedirectRequestHandler is a server request handler')]
    public function testRedirectRequestHandlerIsAServerRequestHandler(): void
    {
        $this->assertInstanceOf(\StoreCore\Engine\AbstractServerRequestHandler::class, new RedirectRequestHandler());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RedirectRequestHandler::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RedirectRequestHandler::VERSION);
        $this->assertIsString(RedirectRequestHandler::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RedirectRequestHandler::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('RedirectRequestHandler::createRedirectResponse() exists')]
    public function testRedirectRequestHandlerCreateRedirectResponseExists(): void
    {
        $class = new ReflectionClass(RedirectRequestHandler::class);
        $this->assertTrue($class->hasMethod('createRedirectResponse'));
    }

    #[Depends('testRedirectRequestHandlerCreateRedirectResponseExists')]
    #[TestDox('RedirectRequestHandler::createRedirectResponse() is public')]
    public function testRedirectRequestHandlerCreateRedirectResponseIsPublic(): void
    {
        $method = new ReflectionMethod(RedirectRequestHandler::class, 'createRedirectResponse');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[TestDox('RedirectRequestHandler::handle exists')]
    public function testRedirectRequestHandlerHandleExists(): void
    {
        $class = new ReflectionClass(RedirectRequestHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testRedirectRequestHandlerHandleExists')]
    #[TestDox('RedirectRequestHandler::handle() is public')]
    public function testRedirectRequestHandlerHandleIsPublic(): void
    {
        $method = new ReflectionMethod(RedirectRequestHandler::class, 'handle');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }


    #[Group('seo')]
    #[TestDox('RedirectRequestHandler::handle() handles cached redirect')]
    public function testRedirectRequestHandlerHandleHandlesCachedRedirect(): void
    {
        // Create a cacheable redirect
        $this->assertTrue(class_exists(Redirect::class));
        $redirect = new Redirect('http://localhost/help', 'https://www.storecore.io/knowledge-base/');
        $redirect->expiresAfter(600);  // 10 minutes

        // Get the redirect cache key
        $this->assertTrue(class_exists(RedirectCache::class));
        $key = RedirectCache::createKey($redirect);
        $this->assertTrue(class_exists(\StoreCore\Types\CacheKey::class));
        $this->assertTrue(\StoreCore\Types\CacheKey::validate($key));

        // Create a server request for the cacheable redirect
        $this->assertTrue(class_exists(ServerRequestFactory::class));
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'http://localhost/help');
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $request);

        // Redirect is not found: HTTP response is “100 Continue”
        $redirector = new RedirectRequestHandler();
        $response = $redirector->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
        $this->assertSame(100, $response->getStatusCode());
        $this->assertSame('Continue', $response->getReasonPhrase());

        // From this point on we need database access
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->query('SELECT 1 FROM `sc_redirects`');
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }

        $cache = new RedirectCache($registry);
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $cache);
        $this->assertInstanceOf(\Psr\SimpleCache\CacheInterface::class, $cache);

        // Redirect does not exist in the redirect cache
        $this->assertFalse($cache->has($key));

        // Redirect is cached
        $this->assertTrue($cache->set($key, $redirect, 600));
        $this->assertTrue($cache->has($key));

        // Request can now be redirected
        if (!defined('STORECORE_CMS_REDIRECTS')) {
            $response = $redirector->handle($request);
            $this->assertSame(307, $response->getStatusCode());
        }

        // Redirect is deleted
        $this->assertTrue($cache->delete($key));
        $this->assertFalse($cache->has($key));

        // And we are back to a “100 Continue” response
        $response = $redirector->handle($request);
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
        $this->assertSame(100, $response->getStatusCode());
    }
}
