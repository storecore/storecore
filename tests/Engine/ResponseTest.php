<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[Group('hmvc')]
#[CoversClass(Response::class)]
final class ResponseTest extends TestCase
{
    #[TestDox('Response class is concrete')]
    public function testResponseClassIsConcrete(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('Response is an HTTP Message')]
    public function testResponseIsHttpMessage(): void
    {
        $response = new Response();
        $this->assertInstanceOf(\StoreCore\Engine\Message::class, $response);
    }


    /**
     * @see https://www.php-fig.org/psr/psr-7/
     *      PSR-7: HTTP message interfaces
     */
    #[TestDox('Implemented PSR-7 MessageInterface exists')]
    public function testImplementedPSR7MessageInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\MessageInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\MessageInterface::class));
    }

    #[Depends('testImplementedPSR7MessageInterfaceExists')]
    #[TestDox('Response implements PSR-7 MessageInterface')]
    public function testResponseImplementsPsr7MessageInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\MessageInterface::class, new Response());
    }

    #[TestDox('Implemented PSR-7 ResponseInterface exists')]
    public function testImplementedPSR7ResponseInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ResponseInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\ResponseInterface::class));
    }

    #[Depends('testImplementedPSR7MessageInterfaceExists')]
    #[Depends('testImplementedPSR7ResponseInterfaceExists')]
    #[TestDox('Response implements PSR-7 ResponseInterface')]
    public function testResponseImplementsPSR7ResponseInterface()
    {
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, new Response());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Response::VERSION);
        $this->assertIsString(Response::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Response::VERSION, '0.4.0', '>=')
        );
    }


    #[TestDox('Response::getReasonPhrase exists')]
    public function testResponseGetReasonPhraseExists(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasMethod('getReasonPhrase'));
    }

    #[Depends('testResponseGetReasonPhraseExists')]
    #[TestDox('Response::getReasonPhrase is public')]
    public function testResponseGetReasonPhraseIsPublic(): void
    {
        $method = new ReflectionMethod(Response::class, 'getReasonPhrase');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testResponseGetReasonPhraseExists')]
    #[TestDox('Response::getReasonPhrase has no parameters')]
    public function testResponseGetReasonPhraseHasNoParameters(): void
    {
        $method = new ReflectionMethod(Response::class, 'getReasonPhrase');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testResponseGetReasonPhraseExists')]
    #[Depends('testResponseGetReasonPhraseIsPublic')]
    #[Depends('testResponseGetReasonPhraseHasNoParameters')]
    #[TestDox('Response::getReasonPhrase returns non-empty string')]
    public function testResponseGetReasonPhraseReturnsNonEmptyString(): void
    {
        $response = new Response();
        $this->assertNotEmpty($response->getReasonPhrase());
        $this->assertIsString($response->getReasonPhrase());
    }

    #[Depends('testResponseGetReasonPhraseReturnsNonEmptyString')]
    #[TestDox('Response::getReasonPhrase returns (string) `OK` by default')]
    public function testResponseGetReasonPhraseReturnsStringOKByDefault(): void
    {
        $response = new Response();
        $this->assertEquals('OK', $response->getReasonPhrase());
    }


    #[TestDox('Response::getStatusCode exists')]
    public function testResponseGetStatusCodeExists(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasMethod('getStatusCode'));
    }

    #[Depends('testResponseGetStatusCodeExists')]
    #[TestDox('Response::getStatusCode is public')]
    public function testResponseGetStatusCodeIsPublic(): void
    {
        $method = new ReflectionMethod(Response::class, 'getStatusCode');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testResponseGetStatusCodeExists')]
    #[TestDox('Response::getStatusCode has no parameters')]
    public function testResponseGetStatusCodeHasNoParameters(): void
    {
        $method = new ReflectionMethod(Response::class, 'getStatusCode');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testResponseGetStatusCodeExists')]
    #[Depends('testResponseGetStatusCodeIsPublic')]
    #[Depends('testResponseGetStatusCodeHasNoParameters')]
    #[TestDox('Response::getStatusCode returns integer')]
    public function testResponseGetStatusCodeReturnsInteger(): void
    {
        $response = new Response();
        $this->assertIsInt($response->getStatusCode());
    }

    #[Depends('testResponseGetStatusCodeReturnsInteger')]
    #[TestDox('Response::getStatusCode returns 200 by default')]
    public function testResponseGetStatusCodeReturns200ByDefault(): void
    {
        $response = new Response();
        $this->assertEquals(200, $response->getStatusCode());
    }


    #[TestDox('Response::setStatusCode exists')]
    public function testResponseSetStatusCodeExists(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasMethod('setStatusCode'));
    }

    #[Depends('testResponseSetStatusCodeExists')]
    #[TestDox('Response::setStatusCode is public')]
    public function testResponseSetStatusCodeIsPublic(): void
    {
        $method = new ReflectionMethod(Response::class, 'setStatusCode');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testResponseSetStatusCodeExists')]
    #[TestDox('Response::setStatusCode has one REQUIRED parameter')]
    public function testResponseSetStatusCodeHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(Response::class, 'setStatusCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testResponseGetStatusCodeReturns200ByDefault')]
    #[Depends('testResponseSetStatusCodeHasOneRequiredParameter')]
    #[TestDox('Response::setStatusCode accepts integers')]
    public function testResponseSetStatusCodeAcceptsIntegers(): void
    {
        $response = new Response();

        $response->setStatusCode(503);
        $this->assertSame(503, $response->getStatusCode());

        $response->setStatusCode(408);
        $this->assertNotSame(503, $response->getStatusCode());
        $this->assertSame(408, $response->getStatusCode());
    }


    #[TestDox('Response::setReasonPhrase exists')]
    public function testResponseSetReasonPhraseExists(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasMethod('setReasonPhrase'));
    }

    #[Depends('testResponseSetReasonPhraseExists')]
    #[TestDox('Response::setReasonPhrase is public')]
    public function testResponseSetReasonPhraseIsPublic(): void
    {
        $method = new ReflectionMethod(Response::class, 'setReasonPhrase');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testResponseSetReasonPhraseExists')]
    #[TestDox('Response::setReasonPhrase has one REQUIRED parameter')]
    public function testResponseSetReasonPhraseHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Response::class, 'setReasonPhrase');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Response::withStatus exists')]
    public function testResponseWithStatusExists(): void
    {
        $class = new ReflectionClass(Response::class);
        $this->assertTrue($class->hasMethod('withStatus'));
    }

    #[Depends('testResponseWithStatusExists')]
    #[TestDox('Response::withStatus is public')]
    public function testResponseWithStatusIsPublic(): void
    {
        $method = new ReflectionMethod(Response::class, 'withStatus');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testResponseWithStatusExists')]
    #[TestDox('Response::withStatus has two parameters')]
    public function testResponseWithStatusHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Response::class, 'withStatus');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testResponseWithStatusHasTwoParameters')]
    #[TestDox('Response::withStatus has one REQUIRED parameter')]
    public function testResponseWithStatusHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Response::class, 'withStatus');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
