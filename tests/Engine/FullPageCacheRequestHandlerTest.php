<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Engine\FullPageCacheRequestHandler::class)]
#[CoversClass(\StoreCore\Engine\AbstractServerRequestHandler::class)]
#[CoversClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[CoversClass(\StoreCore\FileSystem\FullPageCache::class)]
#[CoversClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Engine\FullPageCacheRequestHandler::class)]
#[UsesClass(\StoreCore\FileSystem\CacheDirectory::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class FullPageCacheRequestHandlerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('FullPageCacheRequestHandler class is concrete')]
    public function testFullPageCacheRequestHandlerClassIsConcrete(): void
    {
        $class = new ReflectionClass(FullPageCacheRequestHandler::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testFullPageCacheRequestHandlerClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('FullPageCacheRequestHandler is a server request handler')]
    public function testFullPageCacheRequestHandlerIsAServerRequestHandler(): void
    {
        $this->assertInstanceOf(
            \StoreCore\Engine\AbstractServerRequestHandler::class,
            new FullPageCacheRequestHandler()
        );
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-15 RequestHandlerInterface exists')]
    public function testImplementedPSR15RequestHandlerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
    }

    #[Depends('testFullPageCacheRequestHandlerClassIsConcrete')]
    #[Depends('testImplementedPSR15RequestHandlerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('FullPageCacheRequestHandler implements PSR-15 RequestHandlerInterface')]
    public function testFullPageCacheRequestHandlerImplementsPSR15RequestHandlerInterface(): void
    {
        $this->assertInstanceOf(
            \Psr\Http\Server\RequestHandlerInterface::class,
            new FullPageCacheRequestHandler()
        );
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FullPageCacheRequestHandler::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FullPageCacheRequestHandler::VERSION);
        $this->assertIsString(FullPageCacheRequestHandler::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FullPageCacheRequestHandler::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('FullPageCacheRequestHandler::handle exists')]
    public function testFullPageCacheRequestHandlerHandleExists(): void
    {
        $class = new ReflectionClass(FullPageCacheRequestHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testFullPageCacheRequestHandlerHandleExists')]
    #[TestDox('FullPageCacheRequestHandler::handle is final public')]
    public function testFullPageCacheRequestHandlerHandleIsFinalPublic(): void
    {
        $method = new ReflectionMethod(FullPageCacheRequestHandler::class, 'handle');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFullPageCacheRequestHandlerHandleExists')]
    #[TestDox('FullPageCacheRequestHandler::handle has one REQUIRED parameter')]
    public function testFullPageCacheRequestHandlerMatchHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FullPageCacheRequestHandler::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('FullPageCacheRequestHandler::process exists')]
    public function testFullPageCacheRequestHandlerProcessExists(): void
    {
        $class = new ReflectionClass(FullPageCacheRequestHandler::class);
        $this->assertTrue($class->hasMethod('process'));
    }

    #[Depends('testFullPageCacheRequestHandlerProcessExists')]
    #[TestDox('FullPageCacheRequestHandler::process is protected')]
    public function testFullPageCacheRequestHandlerMatchIsProtected(): void
    {
        $method = new ReflectionMethod(FullPageCacheRequestHandler::class, 'process');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertFalse($method->isPublic());
        $this->assertTrue($method->isProtected());
        $this->assertFalse($method->isStatic());
    }


    #[Depends('testFullPageCacheRequestHandlerHandleExists')]
    #[Depends('testFullPageCacheRequestHandlerMatchHasOneRequiredParameter')]
    #[TestDox('FullPageCacheRequestHandler::handle returns response with status code')]
    public function testFullPageCacheRequestHandlerHandleReturnsResponseWithStatusCode(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://example.com/foo/bar');

        $cache = new FullPageCacheRequestHandler();
        $this->assertNotNull($cache->handle($request));
        $this->assertIsObject($cache->handle($request));

        $interface = new ReflectionClass(\Psr\Http\Message\ResponseInterface::class);
        $this->assertTrue($interface->isInterface());
        $this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $cache->handle($request));

        $this->assertIsInt($cache->handle($request)->getStatusCode());
        $this->assertTrue($cache->handle($request)->getStatusCode() >= 100);
    }

    #[Depends('testFullPageCacheRequestHandlerHandleReturnsResponseWithStatusCode')]
    #[TestDox('FullPageCacheRequestHandler::handle `GET` root does not create “404 Not Found”')]
    public function testFullPageCacheRequestHandlerHandleGetRootDoesNotCreate404NotFound(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/');

        $cache = new FullPageCacheRequestHandler();
        $this->assertNotNull($cache->handle($request));

        $response = $cache->handle($request);
        $this->assertNotSame(404, $response->getStatusCode());
    }
}
