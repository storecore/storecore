<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class TemporaryMemoryStreamTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('TemporaryMemoryStream class exists')]
    public function testTemporaryMemoryStreamClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'TemporaryMemoryStream.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'TemporaryMemoryStream.php');

        $this->assertTrue(class_exists('\\StoreCore\\Engine\\TemporaryMemoryStream'));
        $this->assertTrue(class_exists(\StoreCore\Engine\TemporaryMemoryStream::class));
    }

    #[Group('hmvc')]
    #[TestDox('TemporaryMemoryStream class is concrete')]
    public function testTemporaryMemoryStreamClassIsConcrete(): void
    {
        $class = new ReflectionClass(TemporaryMemoryStream::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testTemporaryMemoryStreamClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('TemporaryMemoryStream is a TemporaryStream')]
    public function testTemporaryMemoryStreamIsTemporaryStream(): void
    {
        $this->assertInstanceOf(\StoreCore\Engine\TemporaryStream::class, new TemporaryMemoryStream());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testImplementedPSR7StreamInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\StreamInterface::class));
    }

    #[Depends('testImplementedPSR7StreamInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('TemporaryMemoryStream implements PSR-7 StreamInterface')]
    public function testTemporaryMemoryStreamImplementsPSR7StreamInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, new TemporaryMemoryStream());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TemporaryMemoryStream::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TemporaryMemoryStream::VERSION);
        $this->assertIsString(TemporaryMemoryStream::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TemporaryMemoryStream::VERSION, '1.0.0', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('FILENAME constant is defined')]
    public function testFilenameConstantIsDefined(): void
    {
        $class = new ReflectionClass(TemporaryMemoryStream::class);
        $this->assertTrue($class->hasConstant('FILENAME'));
    }

    #[Depends('testFilenameConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('FILENAME constant equals (string) "php://memory"')]
    public function testFilenameConstantEqualsStringPhpMemory(): void
    {
        $this->assertIsString(TemporaryMemoryStream::FILENAME);
        $this->assertEquals('php://memory', TemporaryMemoryStream::FILENAME);
    }


    #[Group('hmvc')]
    #[TestDox('MODE constant is defined')]
    public function testModeConstantIsDefined()
    {
        $class = new ReflectionClass(TemporaryMemoryStream::class);
        $this->assertTrue($class->hasConstant('MODE'));
    }

    #[Depends('testModeConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('MODE constant equals (string) "r+" for read-write')]
    public function testFilenameConstantEqualsRPlusForReadWrite()
    {
        $this->assertEquals('r+', TemporaryMemoryStream::MODE);
    }
}
