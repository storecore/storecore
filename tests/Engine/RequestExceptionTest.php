<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \Exception;
use \Throwable;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\Request::class)]
#[CoversClass(\StoreCore\Engine\RequestException::class)]
#[UsesClass(\StoreCore\Engine\RequestFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class RequestExceptionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('RequestException is throwable exception')]
    public function testClientExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(RequestException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));

        try {
            throw new RequestException();
        } catch (Throwable $t) {
            $this->assertInstanceOf(\StoreCore\Engine\RequestException::class, $t);
        }
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-18 RequestExceptionInterface exists')]
    public function testImplementedPsr18RequestExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Client\\RequestExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Client\RequestExceptionInterface::class));
    }

    #[Depends('testClientExceptionIsThrowableException')]
    #[Depends('testImplementedPsr18RequestExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('RequestException implements PSR-18 RequestExceptionInterface')]
    public function testRequestExceptionImplementsPsr18RequestExceptionInterface(): void
    {
        $class = new ReflectionClass(RequestException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Client\RequestExceptionInterface::class));

        try {
            throw new RequestException();
        } catch (Throwable $t) {
            $this->assertInstanceOf(\Psr\Http\Client\RequestExceptionInterface::class, $t);
        }
    }


    #[TestDox('RequestException::getRequest exists')]
    public function testRequestExceptionGetRequestExists(): void
    {
        $class = new ReflectionClass(\StoreCore\Engine\RequestException::class);
        $this->assertTrue($class->hasMethod('getRequest'));
    }

    #[Depends('testRequestExceptionGetRequestExists')]
    #[TestDox('RequestException::getRequest is public')]
    public function testRequestExceptionGetRequestIsPublic()
    {
        $method = new ReflectionMethod(\StoreCore\Engine\RequestException::class, 'getRequest');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testRequestExceptionGetRequestExists')]
    #[TestDox('RequestException::getRequest has no parameters')]
    public function testRequestExceptionGetRequestHasNoParameters(): void
    {
        $method = new ReflectionMethod(RequestException::class, 'getRequest');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Group('distro')]
    #[TestDox('Returned PSR-7 RequestInterface exists')]
    public function testReturnedPSR7RequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\RequestInterface::class));
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/#32-psrhttpmessagerequestinterface
     */
    #[Depends('testRequestExceptionGetRequestExists')]
    #[Depends('testRequestExceptionGetRequestIsPublic')]
    #[Depends('testRequestExceptionGetRequestHasNoParameters')]
    #[Depends('testReturnedPSR7RequestInterfaceExists')]
    #[TestDox('RequestException::getRequest returns PSR-7 RequestInterface')]
    public function testRequestExceptionGetRequestReturnsPsr7RequestInterface(): void
    {
        $factory = new RequestFactory();
        $request = $factory->createRequest('GET', 'https://www.example.com/favicon.ico');
        $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, $request);

        try {
            throw new RequestException(
                'Something is wrong',
                42,
                null,
                $request
            );
        } catch (Exception $e) {
            $this->assertInstanceOf(\Psr\Http\Client\RequestExceptionInterface::class, $e);
            $this->assertIsObject($e->getRequest());
            $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, $e->getRequest());
            $this->assertSame($request, $e->getRequest());
        }
    }
}
