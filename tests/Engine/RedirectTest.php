<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\Engine\Redirect::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
final class RedirectTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Redirect class is concrete')]
    public function testRedirectClassIsConcrete(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Redirect::VERSION);
        $this->assertIsString(Redirect::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Redirect::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Redirect::__construct() exists')]
    public function testRedirectConstructorExists(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testRedirectConstructorExists')]
    #[TestDox('Redirect::__construct() is public constructor')]
    public function testRedirectConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Redirect::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testRedirectConstructorExists')]
    #[TestDox('Redirect::__construct() has three parameters')]
    public function testRedirectConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Redirect::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testRedirectConstructorHasThreeParameters')]
    #[TestDox('Redirect::__construct() has one REQUIRED parameter')]
    public function testRedirectConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Redirect::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRedirectConstructorHasOneRequiredParameter')]
    #[TestDox('Redirect::__construct() sets “from” origin URL')]
    public function testRedirectConstructSetsFromOriginURL(): void
    {
        $redirect = new Redirect('http://www.example.com/webshop/');
        $this->assertIsArray($redirect->get());
        $this->assertArrayHasKey('//www.example.com/webshop/', $redirect->get());
    }


    #[TestDox('Redirect.expiresAt exists')]
    public function testRedirectExpiresAtExists(): void
    {
        $redirect = new Redirect('https://www.example.nl/webshop/');
        $this->assertObjectHasProperty('expiresAt', $redirect);
    }

    #[TestDox('Redirect.expiresAt is null by default')]
    public function testRedirectExpiresAtisNullByDefault(): void
    {
        $redirect = new Redirect('https://www.example.nl/webshop/');
        $this->assertNull($redirect->expiresAt);

        $redirect = new Redirect('https://www.example.nl/webshop/', 'https://example.com/');
        $this->assertNull($redirect->expiresAt);
    }

    #[Depends('testRedirectExpiresAtisNullByDefault')]
    #[Depends('testRedirectConstructorHasThreeParameters')]
    #[TestDox('Redirect::__construct() sets Redirect.expiresAt date and time')]
    public function testRedirectConstructorSetsRedirectExpiresAtDateAndTime(): void
    {
        $redirect = new Redirect(
            'https://www.example.nl/webshop/',
            'https://example.com/',
            new \DateTimeImmutable('2025-12-31 23:59:59')
        );

        $this->assertNotNull($redirect->expiresAt);
        $this->assertInstanceOf(\DateTimeInterface::class, $redirect->expiresAt);
    }

    #[Depends('testRedirectConstructorHasThreeParameters')]
    #[Depends('testRedirectConstructorSetsRedirectExpiresAtDateAndTime')]
    #[TestDox('Redirect::__construct() sets Redirect.expiresAfter interval')]
    public function testRedirectConstructorSetsRedirectExpiresAfterInterval(): void
    {
        $redirect = new Redirect(
            'https://www.example.nl/webshop/',
            'https://example.com/',
            new \DateInterval('P1Y')
        );

        $this->assertNotNull($redirect->expiresAt);
        $this->assertInstanceOf(\DateTimeInterface::class, $redirect->expiresAt);
    }


    #[TestDox('Redirect::expiresAfter() exists')]
    public function testRedirectExpiresAfterExists(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasMethod('expiresAfter'));
    }

    #[TestDox('Redirect::expiresAfter() is public')]
    public function testRedirectExpiresAfterIsPublic(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'expiresAfter');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Redirect::expiresAfter() has one REQUIRED parameter')]
    public function testRedirectExpiresAfterHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'expiresAfter');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRedirectExpiresAfterHasOneRequiredParameter')]
    #[TestDox('Redirect::expiresAfter() accepts integer and sets Redirect.expiresAt')]
    public function testRedirectExpiresAfterAcceptsIntegerAndSetsRedirectExpiresAt(): void
    {
        $redirect = new Redirect('http://www.example.org/museum-shop/', 'https://shop.example.musuem');
        $this->assertNull($redirect->expiresAt);

        $redirect->expiresAfter(3600);
        $this->assertNotNull($redirect->expiresAt);
        $this->assertIsNotInt($redirect->expiresAt);
        $this->assertInstanceOf(\DateTimeInterface::class, $redirect->expiresAt);
    }

    #[Depends('testRedirectExpiresAfterHasOneRequiredParameter')]
    #[TestDox('Redirect::expiresAfter() accepts DateInterval and sets Redirect.expiresAt')]
    public function testRedirectExpiresAfterAcceptsDateIntervalAndSetsRedirectExpiresAt(): void
    {
        $redirect = new Redirect('http://www.example.org/museum-shop/', 'https://shop.example.musuem');
        $redirect->expiresAfter(\DateInterval::createFromDateString('2 years'));
        $this->assertInstanceOf(\DateTimeInterface::class, $redirect->expiresAt);
    }

    #[Depends('testRedirectConstructorSetsRedirectExpiresAfterInterval')]
    #[TestDox('Redirect::expiresAfter() accepts null to unset Redirect.expiresAt')]
    public function testRedirectExpiresAfterAcceptsNullToUnsetRedirectExpiresAt(): void
    {
        $redirect = new Redirect('http://www.example.org/museum-shop/', 'https://shop.example.musuem', new \DateInterval('P1Y'));
        $this->assertNotNull($redirect->expiresAt);
        $redirect->expiresAfter(null);
        $this->assertNull($redirect->expiresAt);
    }


    #[TestDox('Redirect::get() exists')]
    public function testRedirectGetExists(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('Redirect::get() is public')]
    public function testRedirectGetIsPublic(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Redirect::get() has no parameters')]
    public function testRedirectGetHasNoParameters(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'get');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    /**
     * @see https://ahrefs.com/blog/trailing-slash/
     *      “What Is A Trailing Slash & When Does It Matter”, by Patrick Stox
     */
    #[Depends('testRedirectGetExists')]
    #[Depends('testRedirectGetIsPublic')]
    #[Depends('testRedirectGetHasNoParameters')]
    #[TestDox('Redirect::get() returns array with one element')]
    public function testRedirectGetReturnsArrayWithOneElement(): void
    {
        $redirect = new Redirect('http://shop.example.com/', 'https://www.example.com/shop/');
        $this->assertNotEmpty($redirect->get());
        $this->assertIsArray($redirect->get());
        $this->assertCount(1, $redirect->get());

        $this->assertArrayHasKey('//shop.example.com/', $redirect->get());
        $this->assertSame('https://www.example.com/shop', $redirect->get()['//shop.example.com/']);
    }


    #[TestDox('Redirect::getLocation() exists')]
    public function testRedirectGetLocationExists(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasMethod('getLocation'));
    }

    #[Depends('testRedirectGetLocationExists')]
    #[TestDox('Redirect::getLocation() is public')]
    public function testRedirectGetLocationIsPublic(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'getLocation');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRedirectGetLocationExists')]
    #[TestDox('Redirect::getLocation() has no parameters')]
    public function testRedirectGetLocationHasNoParameters(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'getLocation');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testRedirectGetLocationExists')]
    #[Depends('testRedirectGetLocationIsPublic')]
    #[Depends('testRedirectGetLocationHasNoParameters')]
    #[TestDox('Redirect::getLocation() returns non-empty URL string')]
    public function testRedirectGetLocationReturnsNonEmptyUrlString(): void
    {
        $redirect = new Redirect('http://shop.example.com/');
        $this->assertNotEmpty($redirect->getLocation());
        $this->assertIsString($redirect->getLocation());
        $this->assertEquals('/', $redirect->getLocation());

        $redirect = new Redirect('http://example.org/tickets', 'https://www.example.org/tickets');
        $this->assertNotEmpty($redirect->getLocation());
        $this->assertIsString($redirect->getLocation());
        $this->assertEquals('https://www.example.org/tickets', $redirect->getLocation());
    }

    #[Depends('testRedirectGetLocationReturnsNonEmptyUrlString')]
    #[TestDox('Redirect::getLocation() does not return trailing slash for web pages')]
    public function testRedirectGetLocationDoesNotReturnTrailingSlashForWebPages(): void
    {
        $redirect = new Redirect('http://www.example.com/webshop/', 'https://example.com/shop/');
        $this->assertStringEndsNotWith('/', $redirect->getLocation());
        $this->assertSame('https://example.com/shop', $redirect->getLocation());
    }

    
    #[TestDox('Redirect::set() exists')]
    public function testRedirectSetExists(): void
    {
        $class = new ReflectionClass(Redirect::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testRedirectSetExists')]
    #[TestDox('Redirect::set() is public')]
    public function testRedirectSetIsPublic(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testRedirectSetExists')]
    #[TestDox('Redirect::set() has one REQUIRED parameter')]
    public function testRedirectSetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Redirect::class, 'set');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testRedirectSetExists')]
    #[Depends('testRedirectSetIsPublic')]
    #[Depends('testRedirectSetHasOneRequiredParameter')]
    #[TestDox('Redirect::set() accepts array with one key-value pair')]
    public function testRedirectSetAcceptsArrayWithOneKeyValuePair(): void
    {
        $redirect = new Redirect('https://www.example.com/shop/');
        $this->assertEquals('/', $redirect->getLocation());

        $redirect->set(['https://shop.example.org/' => 'https://example.shop/']);
        $this->assertEquals('https://example.shop', $redirect->getLocation());

        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\InvalidArgumentException'));
        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $redirect->set('https://shop.example.org/');
    }

    #[Depends('testRedirectSetAcceptsArrayWithOneKeyValuePair')]
    #[TestDox('Redirect::set() does not accept empty “from” origin URL')]
    public function testRedirectSetDoesNotAcceptEmptyFromOriginURL(): void
    {
        $redirect = new Redirect('https://www.example.com/shop/');
        $this->assertEquals('/', $redirect->getLocation());

        $this->expectException(\Psr\SimpleCache\InvalidArgumentException::class);
        $redirect->set(['' => 'https://example.shop/']);
    }

    #[Depends('testRedirectSetAcceptsArrayWithOneKeyValuePair')]
    #[TestDox('Redirect::set() does not accept empty “to” destination URL')]
    public function testRedirectSetDoesNotAcceptEmptyToDestinationOriginURL(): void
    {
        $redirect = new Redirect('https://www.example.com/shop/');
        $this->assertEquals('/', $redirect->getLocation());

        $this->expectException(\ValueError::class);
        $redirect->set(['https://www.example.com/shop/' => '']);
    }
}
