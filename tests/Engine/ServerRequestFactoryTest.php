<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\Request::class)]
#[CoversClass(\StoreCore\Engine\ServerRequest::class)]
#[CoversClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class ServerRequestFactoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ServerRequestFactory class is concrete')]
    public function testServerRequestFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(ServerRequestFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-17 ServerRequestFactoryInterface exists')]
    public function testImplementedPSR17ServerRequestFactoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ServerRequestFactoryInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\ServerRequestFactoryInterface::class));
    }

    #[Depends('testServerRequestFactoryClassIsConcrete')]
    #[Depends('testImplementedPSR17ServerRequestFactoryInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServerRequestFactory implements PSR-17 ServerRequestFactoryInterface')]
    public function testServerRequestFactoryImplementsPSR17ServerRequestFactoryInterface(): void
    {
        $factory = new ServerRequestFactory('GET', 'https://www.example.com/');
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestFactoryInterface::class, $factory);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ServerRequestFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ServerRequestFactory::VERSION);
        $this->assertIsString(ServerRequestFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ServerRequestFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ServerRequestFactory::createServerRequest exists')]
    public function testServerRequestFactoryCreateServerRequestExists(): void
    {
        $class = new ReflectionClass(ServerRequestFactory::class);
        $this->assertTrue($class->hasMethod('createServerRequest'));
    }

    #[TestDox('ServerRequestFactory::createServerRequest is public')]
    public function testServerRequestFactoryCreateServerRequestIsPublic(): void
    {
        $method = new ReflectionMethod(\StoreCore\Engine\ServerRequestFactory::class, 'createServerRequest');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('ServerRequestFactory::createServerRequest has three parameters')]
    public function testServerRequestFactoryCreateServerRequestHasThreeParameters(): void
    {
        $method = new ReflectionMethod(ServerRequestFactory::class, 'createServerRequest');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testServerRequestFactoryCreateServerRequestHasThreeParameters')]
    #[TestDox('ServerRequestFactory::createServerRequest has two required parameters')]
    public function testServerRequestFactoryCreateServerRequestHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(ServerRequestFactory::class, 'createServerRequest');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServerRequestFactoryCreateServerRequestHasTwoRequiredParameters')]
    #[TestDox('ServerRequestFactory::createServerRequest returns PSR-7 RequestInterface')]
    public function testServerRequestFactoryCreateServerRequestReturnsPsr7RequestInterface(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/');
        
        $this->assertIsObject($request);
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\ServerRequestInterface'));
        $this->assertInstanceOf(\Psr\Http\Message\ServerRequestInterface::class, $request);

        $this->assertEquals('GET', $request->getMethod());

        $this->assertEquals('https', $request->getUri()->getScheme());
        $this->assertEquals('www.example.com', $request->getUri()->getHost());
        $this->assertEquals('https://www.example.com/', (string) $request->getUri());
    }
}
