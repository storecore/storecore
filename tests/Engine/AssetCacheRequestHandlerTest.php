<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\AssetCacheRequestHandler::class)]
#[CoversClass(\StoreCore\Types\CacheKey::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\MediaLibrary::class)]
#[UsesClass(\StoreCore\Engine\Message::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\Response::class)]
#[UsesClass(\StoreCore\Engine\ResponseCodes::class)]
#[UsesClass(\StoreCore\Engine\ResponseFactory::class)]
#[UsesClass(\StoreCore\Engine\ServerRequest::class)]
#[UsesClass(\StoreCore\Engine\ServerRequestFactory::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class AssetCacheRequestHandlerTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('AssetCacheRequestHandler class is concrete')]
    public function testAssetCacheRequestHandlerClassIsConcrete(): void
    {
        $class = new ReflectionClass(AssetCacheRequestHandler::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-15 RequestHandlerInterface exists')]
    public function testImplementedPsr15RequestHandlerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Server\\RequestHandlerInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Server\RequestHandlerInterface::class));
    }

    #[Depends('testImplementedPsr15RequestHandlerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('AssetCacheRequestHandler implements PSR-15 RequestHandlerInterface')]
    public function testAssetCacheRequestHandlerImplementsPsr15RequestHandlerInterface()
    {
        $class = new AssetCacheRequestHandler();
        $this->assertInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class, $class);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AssetCacheRequestHandler::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AssetCacheRequestHandler::VERSION);
        $this->assertIsString(AssetCacheRequestHandler::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AssetCacheRequestHandler::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('distro')]
    #[TestDox('CONTENT_TYPES constant is defined')]
    public function testContentTypesConstantIsDefined(): void
    {
        $class = new ReflectionClass(AssetCacheRequestHandler::class);
        $this->assertTrue($class->hasConstant('CONTENT_TYPES'));
    }

    #[Depends('testContentTypesConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('CONTENT_TYPES constant is non-empty array')]
    public function testContentTypesConstantIsNonEmptyArray(): void
    {
        $this->assertNotEmpty(AssetCacheRequestHandler::CONTENT_TYPES);
        $this->assertIsArray(AssetCacheRequestHandler::CONTENT_TYPES);
    }


    #[TestDox('AssetCacheRequestHandler::handle exists')]
    public function testAssetCacheRequestHandlerHandleExists(): void
    {
        $class = new ReflectionClass(AssetCacheRequestHandler::class);
        $this->assertTrue($class->hasMethod('handle'));
    }

    #[Depends('testAssetCacheRequestHandlerHandleExists')]
    #[TestDox('AssetCacheRequestHandler::handle is final public')]
    public function testAssetCacheRequestHandlerHandleIsPublic(): void
    {
        $method = new ReflectionMethod(AssetCacheRequestHandler::class, 'handle');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAssetCacheRequestHandlerHandleExists')]
    #[TestDox('AssetCacheRequestHandler::handle has one REQUIRED parameter')]
    public function testAssetCacheRequestHandlerHandleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AssetCacheRequestHandler::class, 'handle');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAssetCacheRequestHandlerHandleExists')]
    #[Depends('testAssetCacheRequestHandlerHandleHasOneRequiredParameter')]
    #[TestDox('AssetCacheRequestHandler::handle returns “404 Not Found” response if image does not exist')]
    public function testAssetCacheRequestHandlerHandleReturns404NotFoundResponseIfImageDoesNotExist(): void
    {
        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest('GET', 'https://www.example.com/brands/foo/foo-logo.webp');
        $handler = new AssetCacheRequestHandler();

        $this->assertNotNull($handler->handle($request));
        $this->assertIsObject($handler->handle($request));
        $this->assertInstanceOf(\StoreCore\Engine\Response::class, $handler->handle($request));
        $this->assertEquals(404, $handler->handle($request)->getStatusCode());
        $this->assertEquals('Not Found', $handler->handle($request)->getReasonPhrase());
    }
}
