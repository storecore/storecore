<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\BackupGlobals;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[BackupGlobals(true)]
#[CoversClass(UriFactory::class)]
#[CoversClass(UniformResourceIdentifier::class)]
#[UsesClass(ServerContainer::class)]
final class UriFactoryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('UriFactory class exists')]
    public function testUriFactoryClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'UriFactory.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Engine' . DIRECTORY_SEPARATOR . 'UriFactory.php');

        $this->assertTrue(class_exists('\\StoreCore\\Engine\\UriFactory'));
        $this->assertTrue(class_exists(UriFactory::class));
    }

    #[Group('hmvc')]
    #[TestDox('UriFactory class is concrete')]
    public function testUriFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(UriFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('UriFactory implements PSR-17 UriFactoryInterface')]
    public function testImplementedPSR17UriFactoryInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\UriFactoryInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\UriFactoryInterface::class));
    }

    #[Depends('testUriFactoryClassIsConcrete')]
    #[Depends('testImplementedPSR17UriFactoryInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('UriFactory implements PSR-17 UriFactoryInterface')]
    public function testUriFactoryImplementsPSR17UriFactoryInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\UriFactoryInterface::class, new UriFactory());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UriFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UriFactory::VERSION);
        $this->assertIsString(UriFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UriFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('UriFactory::createUri exists')]
    public function testUriFactoryCreateUriExists(): void
    {
        $class = new ReflectionClass(UriFactory::class);
        $this->assertTrue($class->hasMethod('createUri'));
    }

    #[Depends('testUriFactoryCreateUriExists')]
    #[TestDox('UriFactory::createUri is public')]
    public function testUriFactoryCreateUriIsPublic(): void
    {
        $method = new ReflectionMethod(UriFactory::class, 'createUri');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testUriFactoryCreateUriExists')]
    #[TestDox('UriFactory::createUri has one parameter')]
    public function testUriFactoryCreateUriHasOneParameter(): void
    {
        $method = new ReflectionMethod(UriFactory::class, 'createUri');
        $this->assertEquals(1, $method->getNumberOfParameters());
    }

    #[Depends('testUriFactoryCreateUriHasOneParameter')]
    #[TestDox('UriFactory::createUri throws InvalidArgumentException on empty string')]
    public function testUriFactoryCreateUriThrowsInvalidArgumentExceptionOnEmptyString()
    {
        $factory = new UriFactory();
        $this->expectException(\InvalidArgumentException::class);
        $uri = $factory->createUri('');
    }

    #[Group('hmvc')]
    #[TestDox('UriFactory::createUri returns instance of PSR-7 UriInterface')]
    public function testUriFactoryCreateUriReturnsInstanceOfPsr7UriInterface(): void
    {
        $common_uris = [
            'https://www.example.com',
            'https://www.example.com/',
            'https://www.example.com/foo',
            'https://www.example.com/foo/',
            'https://www.example.com/foo/bar',
            'https://www.example.com/foo-bar/baz-qux',
        ];

        foreach ($common_uris as $uri) {
            $factory = new UriFactory();
            $uri = $factory->createUri($uri);
            $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $uri);
        }
    }

    #[TestDox('UriFactory::createUri throws InvalidArgumentException if URI cannot be parsed')]
    public function testUriFactoryCreateUriThrowsInvalidArgumentExceptionIfUriCannotBeParsed(): void
    {
        $factory = new UriFactory();
        $this->expectException(\InvalidArgumentException::class);
        $uri = $factory->createUri('');
    }


    #[Group('hmvc')]
    #[TestDox('UriFactory::getCurrentUri exists')]
    public function testUriFactoryGetCurrentUriExists(): void
    {
        $class = new ReflectionClass(UriFactory::class);
        $this->assertTrue($class->hasMethod('getCurrentUri'));
    }

    #[Depends('testUriFactoryGetCurrentUriExists')]
    #[Group('hmvc')]
    #[TestDox('UriFactory::getCurrentUri is public static')]
    public function testUriFactoryGetCurrentUriIsPublicStatic(): void
    {
        $method = new ReflectionMethod(UriFactory::class, 'getCurrentUri');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testUriFactoryGetCurrentUriExists')]
    #[Group('hmvc')]
    #[TestDox('UriFactory::getCurrentUri has no parameters')]
    public function testUriFactoryGetCurrentUriHasNoParameters(): void
    {
        $method = new ReflectionMethod(UriFactory::class, 'getCurrentUri');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testUriFactoryGetCurrentUriExists')]
    #[Depends('testUriFactoryGetCurrentUriIsPublicStatic')]
    #[Depends('testUriFactoryGetCurrentUriHasNoParameters')]
    #[Group('hmvc')]
    #[TestDox('UriFactory::getCurrentUri returns instance of PSR-7 UriInterface')]
    public function testUriFactoryGetCurrentUriReturnsInstanceOfPSR7UriInterface(): void
    {
        $_SERVER['HTTP_HOST']   = 'www.example.com';
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['SERVER_PORT'] = '80';

        $uri = UriFactory::getCurrentUri();
        $this->assertInstanceOf(\Psr\Http\Message\UriInterface::class, $uri);
        $this->assertEquals('www.example.com', $uri->getHost());
        $this->assertEquals('/', $uri->getPath());
        $this->assertNull($uri->getPort());
    }
}
