<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\AbstractStream::class)]
#[Group('hmvc')]
final class AbstractStreamTest extends TestCase
{
    #[TestDox('AbstractStream class is abstract')]
    public function testAbstractStreamClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testImplementedPSR7StreamInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\StreamInterface::class));
    }

    #[Depends('testImplementedPSR7StreamInterfaceExists')]
    #[TestDox('AbstractStream implements PSR-7 StreamInterface')]
    public function testAbstractStreamImplementsPSR7StreamInterface(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Message\StreamInterface::class));
    }


    #[TestDox('Audience class is stringable')]
    public function testAbstractStreamIsStringable(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->implementsInterface(\Stringable::class));
    }


    #[TestDox('AbstractStream::__destruct exists')]
    public function testAbstractStreamDestructorExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('__destruct'));
    }

    #[Depends('testAbstractStreamDestructorExists')]
    #[TestDox('AbstractStream::__destruct public destructor')]
    public function testAbstractStreamDestructIsPublicDestructor(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, '__destruct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isDestructor());
    }


    #[TestDox('AbstractStream::__toString exists')]
    public function testAbstractStreamToStringExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }


    #[TestDox('AbstractStream::close exists')]
    public function testAbstractStreamCloseExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('close'));
    }

    #[Depends('testAbstractStreamCloseExists')]
    #[TestDox('AbstractStream::close is public')]
    public function testAbstractStreamCloseIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'close');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractStreamCloseExists')]
    #[TestDox('AbstractStream::close has no parameters')]
    public function testAbstractStreamCloseHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'close');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::detach exists')]
    public function testAbstractStreamDetachExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('detach'));
    }

    #[Depends('testAbstractStreamDetachExists')]
    #[TestDox('AbstractStream::detach is final public')]
    public function testAbstractStreamDetachIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'detach');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamDetachExists')]
    #[TestDox('AbstractStream::detach has no parameters')]
    public function testAbstractStreamDetachHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'detach');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::eof exists')]
    public function testAbstractStreamEofExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('eof'));
    }

    #[Depends('testAbstractStreamEofExists')]
    #[TestDox('AbstractStream::eof is final public')]
    public function testAbstractStreamEofIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'eof');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamEofExists')]
    #[TestDox('AbstractStream::eof has no parameters')]
    public function testAbstractStreamEofHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'eof');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::getContents exists')]
    public function testAbstractStreamGetContentsExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('getContents'));
    }

    #[Depends('testAbstractStreamGetContentsExists')]
    #[TestDox('AbstractStream::getContents is public')]
    public function testAbstractStreamGetContentsIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getContents');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamGetContentsExists')]
    #[TestDox('AbstractStream::getContents has no parameters')]
    public function testAbstractStreamGetContentsHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getContents');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::getMetadata exists')]
    public function testAbstractStreamGetMetadataExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('getMetadata'));
    }

    #[Depends('testAbstractStreamGetMetadataExists')]
    #[TestDox('AbstractStream::getMetadata is public')]
    public function testAbstractStreamGetMetadataIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getMetadata');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamGetMetadataExists')]
    #[TestDox('AbstractStream::getMetadata has one OPTIONAL parameter')]
    public function testAbstractStreamGetMetadataHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getMetadata');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractStream::getSize exists')]
    public function testAbstractStreamGetSizeExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('getSize'));
    }

    #[Depends('testAbstractStreamGetSizeExists')]
    #[TestDox('AbstractStream::getSize is public')]
    public function testAbstractStreamGetSizeIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getSize');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamGetSizeExists')]
    #[TestDox('AbstractStream::getSize has no parameters')]
    public function testAbstractStreamGetSizeHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'getSize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::$readable exists')]
    public function testAbstractStreamReadableExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasProperty('readable'));
    }

    #[Depends('testAbstractStreamReadableExists')]
    #[TestDox('AbstractStream::$readable is protected')]
    public function testAbstractStreamReadableIsProtected(): void
    {
        $property = new ReflectionProperty(AbstractStream::class, 'readable');
        $this->assertTrue($property->isProtected());
        $this->assertFalse($property->isPublic());
    }

    #[TestDox('AbstractStream::isReadable exists')]
    public function testAbstractStreamIsReadableExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('isReadable'));
    }

    #[Depends('testAbstractStreamIsReadableExists')]
    #[TestDox('AbstractStream::isReadable is final public')]
    public function testAbstractStreamIsReadableIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isReadable');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamIsReadableExists')]
    #[TestDox('AbstractStream::isReadable has no parameters')]
    public function testAbstractStreamIsReadableHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isReadable');
        $this->assertEquals(0, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::isSeekable exists')]
    public function testAbstractStreamIsSeekableExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('isSeekable'));
    }

    #[Depends('testAbstractStreamIsSeekableExists')]
    #[TestDox('AbstractStream::isSeekable is final public')]
    public function testAbstractStreamIsSeekableIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isSeekable');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamIsSeekableExists')]
    #[TestDox('AbstractStream::isSeekable has no parameters')]
    public function testAbstractStreamIsSeekableHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isSeekable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::$writable exists')]
    public function testAbstractStreamWritableExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasProperty('writable'));
    }

    #[Depends('testAbstractStreamWritableExists')]
    #[TestDox('AbstractStream::$writable exists')]
    public function testAbstractStreamWritableIsProtected(): void
    {
        $property = new ReflectionProperty(AbstractStream::class, 'writable');
        $this->assertTrue($property->isProtected());
    }

    #[TestDox('AbstractStream::isWritable exists')]
    public function testAbstractStreamIsWritableExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('isWritable'));
    }

    #[Depends('testAbstractStreamIsWritableExists')]
    #[TestDox('AbstractStream::isWritable is final public')]
    public function testAbstractStreamIsWritableIsFinal(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isWritable');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamIsWritableExists')]
    #[TestDox('AbstractStream::isWritable has no parameters')]
    public function testAbstractStreamIsWritableHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'isWritable');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::read exists')]
    public function testAbstractStreamReadExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('read'));
    }

    #[Depends('testAbstractStreamReadExists')]
    #[TestDox('AbstractStream::read is public')]
    public function testAbstractStreamReadIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'read');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamReadExists')]
    #[TestDox('AbstractStream::read has one REQUIRED parameter')]
    public function testAbstractStreamReadHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'read');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractStream::rewind exists')]
    public function testAbstractStreamRewindExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('rewind'));
    }

    #[Depends('testAbstractStreamRewindExists')]
    #[TestDox('AbstractStream::rewind is final public')]
    public function testAbstractStreamRewindIsFinalPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'rewind');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamRewindExists')]
    #[TestDox('AbstractStream::rewind has no parameters')]
    public function testAbstractStreamRewindHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'rewind');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::seek exists')]
    public function testAbstractStreamSeekExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('seek'));
    }

    #[Depends('testAbstractStreamSeekExists')]
    #[TestDox('AbstractStream::seek is public')]
    public function testAbstractStreamSeekIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'seek');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamSeekExists')]
    #[TestDox('AbstractStream::seek has two parameters')]
    public function testAbstractStreamSeekHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'seek');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractStreamSeekHasTwoParameters')]
    #[TestDox('AbstractStream::seek has one REQUIRED parameter')]
    public function testAbstractStreamSeekHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'seek');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractStream::tell exists')]
    public function testAbstractStreamTellExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('tell'));
    }

    #[Depends('testAbstractStreamTellExists')]
    #[TestDox('AbstractStream::tell is public')]
    public function testAbstractStreamTellIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'tell');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractStreamTellExists')]
    #[TestDox('AbstractStream::tell has no parameters')]
    public function testAbstractStreamTellHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'tell');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('AbstractStream::write exists')]
    public function testAbstractStreamWriteExists(): void
    {
        $class = new ReflectionClass(AbstractStream::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testAbstractStreamWriteExists')]
    #[TestDox('AbstractStream::write is abstract')]
    public function testAbstractStreamWriteIsAbstract(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'write');
        $this->assertTrue($method->isAbstract());
    }

    #[Depends('testAbstractStreamWriteExists')]
    #[TestDox('AbstractStream::write is public')]
    public function testAbstractStreamWriteIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'write');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractStreamWriteExists')]
    #[TestDox('AbstractStream::write has one REQUIRED parameter')]
    public function testAbstractStreamWriteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractStream::class, 'write');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
