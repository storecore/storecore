<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversClass(\StoreCore\Engine\NetworkException::class)]
#[CoversClass(\StoreCore\Engine\RequestFactory::class)]
#[CoversClass(\StoreCore\Engine\Request::class)]
#[CoversClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[CoversClass(\StoreCore\Engine\UriFactory::class)]
final class NetworkExceptionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('NetworkException is a throwable exception')]
    public function testNetworkExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(NetworkException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));
    }

    #[Depends('testNetworkExceptionIsThrowableException')]
    #[Group('hmvc')]
    #[TestDox('NetworkException is a runtime exception')]
    public function testNetworkExceptionIsRuntimeException(): void
    {
        $this->expectException(\RuntimeException::class);
        throw new NetworkException();
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-18 NetworkExceptionInterface exists')]
    public function testImplementedPSR18NetworkExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Client\\NetworkExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Client\NetworkExceptionInterface::class));
    }

    #[Depends('testNetworkExceptionIsThrowableException')]
    #[Depends('testImplementedPSR18NetworkExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('NetworkException implements PSR-18 NetworkExceptionInterface')]
    public function testNetworkExceptionImplementsPSR18NetworkExceptionInterface(): void
    {
        $class = new ReflectionClass(NetworkException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Client\NetworkExceptionInterface::class));

        try {
            throw new NetworkException();
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Psr\Http\Client\NetworkExceptionInterface::class, $e);
        }
    }


    #[TestDox('NetworkException::getRequest exists')]
    public function testNetworkExceptionGetRequestExists(): void
    {
        $class = new ReflectionClass(NetworkException::class);
        $this->assertTrue($class->hasMethod('getRequest'));
    }

    #[Depends('testNetworkExceptionGetRequestExists')]
    #[TestDox('NetworkException::getRequest is public')]
    public function testNetworkExceptionGetRequestIsPublic(): void
    {
        $method = new ReflectionMethod(NetworkException::class, 'getRequest');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testNetworkExceptionGetRequestExists')]
    #[TestDox('NetworkException::getRequest has no parameters')]
    public function testNetworkExceptionGetRequestHasNoParameters(): void
    {
        $method = new ReflectionMethod(NetworkException::class, 'getRequest');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Group('distro')]
    #[TestDox('Returned PSR-7 RequestInterface exists')]
    public function testReturnedPSR7RequestInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\RequestInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\RequestInterface::class));
    }

    /**
     * @see https://www.php-fig.org/psr/psr-7/#32-psrhttpmessagerequestinterface
     */
    #[Depends('testNetworkExceptionGetRequestExists')]
    #[Depends('testNetworkExceptionGetRequestIsPublic')]
    #[Depends('testNetworkExceptionGetRequestHasNoParameters')]
    #[Depends('testReturnedPSR7RequestInterfaceExists')]
    #[TestDox('NetworkException::getRequest returns PSR-7 RequestInterface')]
    public function testNetworkExceptionGetRequestReturnsPSR7RequestInterface(): void
    {
        $factory = new RequestFactory();
        $request = $factory->createRequest('GET', 'https://www.example.org/logo.gif');
        $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, $request);

        try {
            throw new NetworkException(
                'Image file server is down',
                502,
                null,
                $request
            );
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Psr\Http\Client\NetworkExceptionInterface::class, $e);
            $this->assertIsObject($e->getRequest());
            $this->assertInstanceOf(\Psr\Http\Message\RequestInterface::class, $e->getRequest());
            $this->assertSame($request, $e->getRequest());
        }
    }
}
