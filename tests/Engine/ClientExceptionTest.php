<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Engine;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;

#[CoversNothing]
final class ClientExceptionTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ClientException is a throwable exception')]
    public function testClientExceptionIsThrowableException(): void
    {
        $class = new ReflectionClass(ClientException::class);
        $this->assertTrue($class->implementsInterface(\Throwable::class));
        $this->assertTrue($class->isSubclassOf(\Exception::class));

        $this->expectException(\StoreCore\Engine\ClientException::class);
        throw new ClientException();
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-18 ClientExceptionInterface exists')]
    public function testImplementedPSR18ClientExceptionInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Client\\ClientExceptionInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Client\ClientExceptionInterface::class));
    }

    #[Depends('testImplementedPSR18ClientExceptionInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-18 ClientExceptionInterface exists')]
    public function testClientExceptionImplementsPsr18ClientExceptionInterface()
    {
        $class = new ReflectionClass(ClientException::class);
        $this->assertTrue($class->implementsInterface(\Psr\Http\Client\ClientExceptionInterface::class));

        try {
            throw new ClientException();
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Psr\Http\Client\ClientExceptionInterface::class, $e);
        }
    }
}
