<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\ResponseCodes::class)]
final class ResponseCodesTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ResponseCodes class is final readonly')]
    public function testResponseCodesClassIsFinalReadonly(): void
    {
        $class = new ReflectionClass(ResponseCodes::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ResponseCodes::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ResponseCodes::VERSION);
        $this->assertIsString(ResponseCodes::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ResponseCodes::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('ResponseCodes::getReasonPhrase exists')]
    public function testResponseCodesGetReasonPhraseExists(): void
    {
        $class = new ReflectionClass(ResponseCodes::class);
        $this->assertTrue($class->hasMethod('getReasonPhrase'));
    }

    #[Depends('testResponseCodesGetReasonPhraseExists')]
    #[TestDox('ResponseCodes::getReasonPhrase is final public static')]
    public function testResponseCodesGetReasonPhraseIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(ResponseCodes::class, 'getReasonPhrase');
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testResponseCodesGetReasonPhraseExists')]
    #[TestDox('ResponseCodes::getReasonPhrase has one REQUIRED parameter')]
    public function testResponseCodesGetReasonPhraseHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ResponseCodes::class, 'getReasonPhrase');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testResponseCodesGetReasonPhraseExists')]
    #[Depends('testResponseCodesGetReasonPhraseIsFinalPublicStatic')]
    #[Depends('testResponseCodesGetReasonPhraseHasOneRequiredParameter')]
    #[TestDox('ResponseCodes::getReasonPhrase returns non-empty string')]
    public function testResponseCodesGetReasonPhraseReturnsNonEmptyString(): void
    {
        $reason_phrase = ResponseCodes::getReasonPhrase(404);
        $this->assertNotEmpty($reason_phrase);
        $this->assertIsString($reason_phrase);
        $this->assertEquals('Not Found', $reason_phrase);
    }

    #[Depends('testResponseCodesGetReasonPhraseHasOneRequiredParameter')]
    #[TestDox('ResponseCodes::getReasonPhrase accepts integer and numeric string')]
    public function testResponseCodesGetReasonPhraseAcceptsIntegerAndNumericString(): void
    {
        $this->assertEquals('Not Found', ResponseCodes::getReasonPhrase(404));
        $this->assertEquals('Not Found', ResponseCodes::getReasonPhrase('404'));
    }

    #[Depends('testResponseCodesGetReasonPhraseHasOneRequiredParameter')]
    #[Depends('testResponseCodesGetReasonPhraseReturnsNonEmptyString')]
    #[TestDox('ResponseCodes::getReasonPhrase supports common client errors')]
    public function testResponseCodesGetReasonPhraseSupportsCommonClientErrors(): void
    {
        $this->assertEquals('Not Found', ResponseCodes::getReasonPhrase(404));
        $this->assertEquals('Method Not Allowed', ResponseCodes::getReasonPhrase(405));
        $this->assertEquals('Gone', ResponseCodes::getReasonPhrase(410));
    }

    #[Depends('testResponseCodesGetReasonPhraseHasOneRequiredParameter')]
    #[Depends('testResponseCodesGetReasonPhraseReturnsNonEmptyString')]
    #[TestDox('ResponseCodes::getReasonPhrase supports common server errors')]
    public function testResponseCodesGetReasonPhraseSupportsCommonServerErrors(): void
    {
        $this->assertEquals('Internal Server Error', ResponseCodes::getReasonPhrase(500));
        $this->assertEquals('Not Implemented', ResponseCodes::getReasonPhrase(501));
        $this->assertEquals('Service Unavailable', ResponseCodes::getReasonPhrase(503));
    }

    #[Depends('testResponseCodesGetReasonPhraseHasOneRequiredParameter')]
    #[TestDox('ResponseCodes::getReasonPhrase throws value error on invalid HTTP status code')]
    public function testResponseCodesGetReasonPhraseThrowsValueErrorOnInvalidHttpStatusCode()
    {
        $this->expectException(\ValueError::class);
        $failure = ResponseCodes::getReasonPhrase(42);
    }
}
