<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Engine\TemporaryStream::class)]
#[CoversClass(\StoreCore\Engine\AbstractStream::class)]
final class TemporaryStreamTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('TemporaryStream class is concrete')]
    public function testTemporaryStreamClassIsConcrete(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TemporaryStream extends AbstractStream')]
    public function testTemporaryStreamExtendsAbstractStream(): void
    {
        $this->assertInstanceOf(\StoreCore\Engine\AbstractStream::class, new TemporaryStream());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-7 StreamInterface exists')]
    public function testImplementedPSR7StreamInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Http\\Message\\StreamInterface'));
        $this->assertTrue(interface_exists(\Psr\Http\Message\StreamInterface::class));
    }

    #[Depends('testImplementedPSR7StreamInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('TemporaryStream implements PSR-7 StreamInterface')]
    public function testTemporaryStreamImplementsPSR7StreamInterface(): void
    {
        $this->assertInstanceOf(\Psr\Http\Message\StreamInterface::class, new TemporaryStream());
    }


    #[Group('hmvc')]
    #[TestDox('FILENAME constant is defined')]
    public function testFilenameConstantIsDefined(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertTrue($class->hasConstant('FILENAME'));
    }

    #[Depends('testFilenameConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('FILENAME constant equals (string) "php://temp"')]
    public function testFilenameConstantEqualsPhpTemp(): void
    {
        $this->assertIsString(TemporaryStream::FILENAME);
        $this->assertEquals('php://temp', TemporaryStream::FILENAME);
    }

    #[Group('hmvc')]
    #[TestDox('MODE constant is defined')]
    public function testModeConstantIsDefined(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertTrue($class->hasConstant('MODE'));
    }

    #[Depends('testModeConstantIsDefined')]
    #[Group('hmvc')]
    #[TestDox('MODE constant equals (string) "r+" for read-write')]
    public function testFilenameConstantEqualsRPlusForReadWrite(): void
    {
        $this->assertEquals('r+', TemporaryStream::MODE);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TemporaryStream::VERSION);
        $this->assertIsString(TemporaryStream::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TemporaryStream::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('TemporaryStream::__construct exists')]
    public function testTemporaryStreamConstructExists(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testTemporaryStreamConstructExists')]
    #[TestDox('TemporaryStream::__construct is public constructor')]
    public function testTemporaryStreamConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(TemporaryStream::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testTemporaryStreamConstructExists')]
    #[TestDox('TemporaryStream::__construct has one OPTIONAL parameter')]
    public function testTemporaryStreamConstructHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(TemporaryStream::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('TemporaryStream::__construct creates readable stream')]
    public function testTemporaryStreamConstructCreatesReadableStream(): void
    {
        $stream = new \StoreCore\Engine\TemporaryStream();
        $this->assertTrue($stream->isReadable());
    }

    #[TestDox('TemporaryStream::__construct creates writable stream')]
    public function testTemporaryStreamConstructCreatesWritableStream(): void
    {
        $stream = new \StoreCore\Engine\TemporaryStream();
        $this->assertTrue($stream->isWritable());
    }


    #[TestDox('TemporaryStream::write exists')]
    public function testTemporaryStreamWriteExists(): void
    {
        $class = new ReflectionClass(TemporaryStream::class);
        $this->assertTrue($class->hasMethod('write'));
    }

    #[Depends('testTemporaryStreamWriteExists')]
    #[TestDox('TemporaryStream::write is public')]
    public function testTemporaryStreamWriteIsPublic(): void
    {
        $method = new ReflectionMethod(TemporaryStream::class, 'write');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testTemporaryStreamWriteExists')]
    #[TestDox('TemporaryStream::write has one REQUIRED parameter')]
    public function testTemporaryStreamWriteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(TemporaryStream::class, 'write');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testTemporaryStreamWriteExists')]
    #[Depends('testTemporaryStreamWriteIsPublic')]
    #[Depends('testTemporaryStreamWriteHasOneRequiredParameter')]
    #[TestDox('TemporaryStream::write returns bytes written as integer')]
    public function testTemporaryStreamWriteReturnsBytesWrittenAsInteger(): void
    {
        $stream = new TemporaryStream();
        $bytes_written = $stream->write('Hello world');
        $this->assertIsInt($bytes_written);
        $this->assertEquals(11, $bytes_written);
        $this->assertEquals($stream->getSize(), $bytes_written);
    }
}
