<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class PricingStrategyInterfaceTest extends TestCase
{
    #[TestDox('PricingStrategyInterface interface exists')]
    public function testPricingStrategyInterfaceInterfaceExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PricingStrategyInterface.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PricingStrategyInterface.php');

        $this->assertTrue(interface_exists('\\StoreCore\\PricingStrategyInterface'));
        $this->assertTrue(interface_exists(PricingStrategyInterface::class));
    }


    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PricingStrategyInterface::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PricingStrategyInterface::VERSION);
        $this->assertIsString(PricingStrategyInterface::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PricingStrategyInterface::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
