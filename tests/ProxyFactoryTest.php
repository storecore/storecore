<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\Order;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;
use StoreCore\OML\OrderProxy;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\ProxyFactory::class)]
#[CoversClass(\StoreCore\DependencyException::class)]
#[CoversClass(\StoreCore\CRM\OrganizationProxy::class)]
#[CoversClass(\StoreCore\OML\OrderProxy::class)]
#[UsesClass(\StoreCore\Order::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ProxyFactoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ProxyFactory class is concrete')]
    public function testProxyFactoryIsConcrete(): void
    {
        $class = new ReflectionClass(ProxyFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ProxyFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ProxyFactory::VERSION);
        $this->assertIsString(ProxyFactory::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ProxyFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface exists')]
    public function testProxyFactoryCreateFromIdentityInterfaceExists(): void
    {
        $class = new ReflectionClass(ProxyFactory::class);
        $this->assertTrue($class->hasMethod('createFromIdentityInterface'));
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface is public static')]
    public function testProxyFactoryCreateFromIdentityInterfaceIsPublicStatic(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromIdentityInterface');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface has one REQUIRED parameter')]
    public function testProxyFactoryCreateFromIdentityInterfaceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromIdentityInterface');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface accepts IdentityInterface and returns ProxyInterface')]
    public function testProxyFactoryCreateFromIdentityInterfaceAcceptsIdentityInterfaceAndReturnsProxyInterface()
    {
        $customer = new Person('Jane Doe');
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $customer);
        $customer->setIdentifier('7b4ea906-7dbf-4c1b-92e1-6b8e4bbea901');

        $customer_proxy = ProxyFactory::createFromIdentityInterface($customer);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $customer_proxy);

        $this->assertNotEmpty($customer_proxy->identifier);
        $this->assertIsString($customer_proxy->identifier);
        $this->assertSame((string) $customer->getIdentifier(), $customer_proxy->identifier);
        $this->assertSame((string) $customer->getIdentifier(), (string) $customer_proxy);
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface throws dependency exception on missing UUID')]
    public function testProxyFactoryCreateFromIdentityInterfaceThrowsDependencyExceptionOnMissingUUID()
    {
        $customer = new Person('Jane Doe');
        $this->assertFalse($customer->hasIdentifier());

        $this->expectException(\StoreCore\DependencyException::class);
        $failure = ProxyFactory::createFromIdentityInterface($customer);
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromIdentityInterface returns OrganizationProxy on Organization')]
    public function testProxyFactoryCreateFromIdentityInterfaceReturnsOrganizationProxyOnOrganization()
    {
        $organization = new Organization('Wonka Industries');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $organization->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromIdentityInterface($organization);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
        $this->assertInstanceOf(\StoreCore\Proxy::class, $proxy);
        $this->assertInstanceOf(\StoreCore\CRM\OrganizationProxy::class, $proxy);
    }


    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrder exists')]
    public function testProxyFactoryCreateFromOrderExists(): void
    {
        $class = new ReflectionClass(ProxyFactory::class);
        $this->assertTrue($class->hasMethod('createFromOrder'));
    }

    #[Depends('testProxyFactoryCreateFromOrderExists')]
    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrder is public static')]
    public function testProxyFactoryCreateFromOrderIsPublicStatic(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromOrder');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testProxyFactoryCreateFromOrderExists')]
    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrder has one REQUIRED parameter')]
    public function testProxyFactoryCreateFromOrderHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromOrder');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testProxyFactoryCreateFromOrderExists')]
    #[Depends('testProxyFactoryCreateFromOrderIsPublicStatic')]
    #[Depends('testProxyFactoryCreateFromOrderHasOneRequiredParameter')]
    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrder accepts Order and returns OrderProxy')]
    public function testProxyFactoryCreateFromOrderAcceptsOrderAndReturnspOrderProxy(): void
    {
        $this->assertTrue(class_exists(Order::class));
        $this->assertTrue(class_exists(OrderProxy::class));

        $registry = Registry::getInstance();
        $order = new Order($registry);
        $this->assertFalse($order->hasIdentifier());
        $order->setIdentifier('f6792416-40a5-4460-9cb9-ca1169c083bc');
        $this->assertTrue($order->hasIdentifier());

        $proxy = ProxyFactory::createFromOrder($order);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
        $this->assertTrue($proxy->hasIdentifier());
        $this->assertEquals($order->getIdentifier(), $proxy->getIdentifier());
        $this->assertEquals((string) $order->getIdentifier(), (string) $proxy->getIdentifier());
    }


    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrganization exists')]
    public function testProxyFactoryCreateFromOrganizationExists(): void
    {
        $class = new ReflectionClass(ProxyFactory::class);
        $this->assertTrue($class->hasMethod('createFromOrganization'));
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrganization is public static')]
    public function testProxyFactoryCreateFromOrganizationIsPublicStatic(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromOrganization');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrganization has one REQUIRED parameter')]
    public function testProxyFactoryCreateFromOrganizationHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ProxyFactory::class, 'createFromOrganization');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Group('hmvc')]
    #[TestDox('ProxyFactory::createFromOrganization accepts Organization and returns OrganizationProxy')]
    public function testProxyFactoryCreateFromOrganizationAcceptsOrganizationAndReturnsOrganizationProxy(): void
    {
        $this->assertTrue(class_exists(Organization::class));

        $organization = new Organization();
        $organization->setIdentifier('714b67ae-82c7-41d6-a0ca-41cb0be2486b');
        $organization->name = 'Acme Corporation';
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
        $this->assertInstanceOf(\StoreCore\CRM\OrganizationProxy::class, $proxy);

        $this->assertTrue($proxy->hasIdentifier());
        $this->assertEquals($organization->getIdentifier(), $proxy->getIdentifier());

        $this->assertEquals('714b67ae-82c7-41d6-a0ca-41cb0be2486b', $proxy->identifier);
        $this->assertEquals('Organization', $proxy->type);
        $this->assertEquals('Acme Corporation', $proxy->name);
    }

    #[Group('distro')]
    #[TestDox('ProxyFactory::createFromOrganization includes optional Organization.name')]
    public function testProxyFactoryCreateFromOrganizationIncludesOptionalOrganizationName()
    {
        $organization = new Organization();
        $organization->setIdentifier('abe9e4da-f9fd-11ed-be56-0242ac120002');

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertEmpty($proxy->name);

        $organization = new Organization();
        $organization->setIdentifier('ac8b6ad2-63b1-4f73-9912-00b44fe02fb3');
        $organization->name = 'John Wiley & Sons';
        $organization->legalName = 'John Wiley & Sons, Inc.';
        $organization->alternateName = 'Wiley';

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertNotEmpty($proxy->name);
        $this->assertIsString($proxy->name);
        $this->assertEquals('John Wiley & Sons', $proxy->name);
    }
}
