<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversClass(\StoreCore\AbstractContainer::class)]
#[Group('hmvc')]
final class AbstractContainerTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractContainer class exists')]
    public function testAbstractContainerClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractContainer.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractContainer.php');

        $this->assertTrue(class_exists('\\StoreCore\\AbstractContainer'));
        $this->assertTrue(class_exists(AbstractContainer::class));
    }

    #[TestDox('AbstractContainer is abstract')]
    public function testAbstractContainerIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractContainer::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[TestDox('AbstractContainer implements PSR-11 ContainerInterface')]
    public function testAbstractContainerImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(AbstractContainer::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[TestDox('AbstractContainer::get() exists')]
    public function testAbstractContainerGetExists(): void
    {
        $class = new ReflectionClass(AbstractContainer::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testAbstractContainerGetExists')]
    #[TestDox('AbstractContainer::get() is public')]
    public function testAbstractContainerGetIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractContainer::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractContainerGetExists')]
    #[TestDox('AbstractContainer::get() has one REQUIRED parameter')]
    public function testAbstractContainerGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractContainer::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractContainer::has() exists')]
    public function testAbstractContainerHasExists(): void
    {
        $class = new ReflectionClass(AbstractContainer::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testAbstractContainerHasExists')]
    #[TestDox('AbstractContainer::has() is public')]
    public function testAbstractContainerHasIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractContainer::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractContainerHasExists')]
    #[TestDox('AbstractContainer::has() has one REQUIRED parameter')]
    public function testAbstractContainerHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractContainer::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
