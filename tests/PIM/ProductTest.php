<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\CMS\Review;
use StoreCore\CRM\{Organization, Person};
use StoreCore\Types\AdultOrientedEnumeration;
use StoreCore\Types\DateTime;
use StoreCore\Types\{Rating, AggregateRating};
use StoreCore\Types\{UUIDFactory, UUID};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\Product::class)]
#[CoversClass(\StoreCore\PIM\AbstractProductOrService::class)]
#[CoversClass(\StoreCore\PIM\Brand::class)]
#[UsesClass(\StoreCore\CMS\AbstractCreativeWork::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\AggregateRating::class)]
#[UsesClass(\StoreCore\Types\Rating::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ProductTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Product class exists')]
    public function testProductClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Product.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Product.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\Product'));
        $this->assertTrue(class_exists(Product::class));
    }

    #[Group('hmvc')]
    #[TestDox('Product class is concrete')]
    public function testProductClassIsConcrete(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Product extends AbstractProductOrService')]
    public function testProductExtendsAbstractProductOrService(): void
    {
        $this->assertInstanceOf(AbstractProductOrService::class, new Product());
    }

    #[Group('hmvc')]
    #[TestDox('Product extends AbstractProduct')]
    public function testProductExtendsAbstractProduct(): void
    {
        $this->assertInstanceOf(AbstractProduct::class, new Product());
    }

    #[Group('hmvc')]
    #[TestDox('Product implements StoreCore IdentityInterface')]
    public function testProductImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Product());
    }

    #[Group('hmvc')]
    #[TestDox('Product is JSON serializable')]
    public function testProductIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Product());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Product::VERSION);
        $this->assertIsString(Product::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Product::VERSION, '0.4.0', '>=')
        );
    }


    #[TestDox('Product::__construct() exists')]
    public function testProductConstructorExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Product::__construct() is public constructor')]
    public function testProductConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Product::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Product::__construct() has one optional parameter')]
    public function testProductConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Product::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Product::__construct() sets UUID or name')]
    public function testProductConstructorSetsUuidOrName(): void
    {
        // Product.name as string
        $product = new Product('Google Chromecast');
        $this->assertFalse($product->hasIdentifier());
        $this->assertSame('Google Chromecast', $product->name);

        // Product.identifier as UUID string
        $product = new Product('bca82a6a-5d1e-47bd-86f8-247ce18b246c');
        $this->assertTrue($product->hasIdentifier());
        $this->assertSame('bca82a6a-5d1e-47bd-86f8-247ce18b246c', (string) $product->getIdentifier());

        // Product.identifier as UUID object
        $uuid = UUIDFactory::pseudoRandomUUID();
        $this->assertInstanceOf(UUID::class, $uuid);
        $product = new Product($uuid);
        $this->assertTrue($product->hasIdentifier());
        $this->assertSame($uuid, $product->getIdentifier());
    }


    #[TestDox('Product.availability exists')]
    public function testProductAvailabilityExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('availability', $product);
    }

    #[TestDox('Product.availability is public')]
    public function testProductAvailabilityIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'availability');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Product.availability is public')]
    public function testProductAvailabilityIsNotEmpty(): void
    {
        $product = new Product();
        $this->assertNotEmpty($product->availability);
    }

    #[TestDox('Product.availability is StoreCore\PIM\ItemAvailability object')]
    public function testProductAvailabilityIsStoreCoreTypesItemAvailabilityObject(): void
    {
        $product = new Product();
        $this->assertInstanceOf(\StoreCore\PIM\ItemAvailability::class, $product->availability);
    }

    #[Depends('testProductAvailabilityIsStoreCoreTypesItemAvailabilityObject')]
    #[TestDox('Product.availability is https://schema.org/InStock by default')]
    public function testProductAvailabilityIsSchemaOrgInStockByDefault(): void
    {
        $product = new Product();
        $this->assertEquals('https://schema.org/InStock', $product->availability->value);
    }


    #[TestDox('Product.brand exists')]
    public function testProductBrandExists(): void
    {
        $this->assertObjectHasProperty('brand', new Product());
    }

    #[TestDox('Product.brand is null by default')]
    public function testProductBrandIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->brand);
    }

    #[TestDox('Product.brand accepts StoreCore\PIM\Brand')]
    public function testProductBrandAcceptsStoreCorePimBrand(): void
    {
        $product = new Product();
        $product->brand = new Brand('Nike');

        $this->assertNotNull($product->brand);
        $this->assertInstanceOf(Brand::class, $product->brand);
        $this->assertSame('Nike', $product->brand->name);
    }

    #[TestDox('Product.brand accepts StoreCore\CRM\Organization')]
    public function testProductBrandAcceptsStoreCoreCrmOrganization(): void
    {
        $product = new Product();
        $product->brand = new Organization('Nike');

        $this->assertNotNull($product->brand);
        $this->assertInstanceOf(Organization::class, $product->brand);
        $this->assertSame('Nike', $product->brand->name);
    }

    #[Depends('testProductBrandAcceptsStoreCorePimBrand')]
    #[Depends('testProductBrandAcceptsStoreCoreCrmOrganization')]
    #[TestDox('Brand is preferred over Organization')]
    public function testBrandIsPreferredOverOrganization(): void
    {
        /*
         * `Organization` with a `Brand`:
         *
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "Organization",
         *   "name": "Adidas AG",
         *   "brand": {
         *     "@type": "Brand",
         *     "name": "ADIDAS"
         *   }
         * }
         */
        $organization = new Organization('Adidas AG');
        $organization->brand = new Brand('ADIDAS');
        $this->assertInstanceOf(Brand::class, $organization->brand);
        $this->assertSame('ADIDAS', $organization->brand->name);

        // Product.brand is a Brand:
        $product = new Product();
        $product->brand = $organization;
        $this->assertInstanceOf(Brand::class, $product->brand);

        /*
         * `Organization` with more than one `Brand`:
         * 
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "Organization",
         *   "name": "Nike, Inc.",
         *   "brand": [
         *     {
         *       "@type": "Brand",
         *       "name": "Nike"
         *     },
         *     {
         *       "@type": "Brand",
         *       "name": "Jordan"
         *     },
         *     {
         *       "@type": "Brand",
         *       "name": "Converse"
         *     }
         *   ]
         * }
         * ```
         */
        $organization = new Organization('Nike, Inc.');
        $brands = [
            new Brand('Nike'),
            new Brand('Jordan'),
            new Brand('Converse'),
        ];
        $organization->brand = $brands;
        $this->assertNotInstanceOf(Brand::class, $organization->brand);
        $this->assertIsArray($organization->brand);

        // Product.brand is an Organization:
        $product = new Product();
        $product->brand = $organization;
        $this->assertNotInstanceOf(Brand::class, $product->brand);
        $this->assertInstanceOf(Organization::class, $product->brand);
    }


    #[TestDox('Product.description exists')]
    public function testProductDescriptionExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('description', $product);
    }

    #[TestDox('Product.description is public')]
    public function testProductDescriptionIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'description');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Product.description is null by default')]
    public function testProductDescriptionIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->description);
    }

    /**
     * @see https://schema.org/Product#eg-0010 Schema.org Product Example 1
     */
    #[TestDox('Product.description accepts string')]
    public function testProductDescriptionAcceptsString(): void
    {
        $product = new Product();
        $this->assertEmpty($product->description);

        $product->description = '0.7 cubic feet countertop microwave. Has six preset cooking categories and convenience features like Add-A-Minute and Child Lock.';
        $this->assertNotEmpty($product->description);
        $this->assertIsString($product->description);
    }


    #[TestDox('Product.hasAdultConsideration exists')]
    public function testProductHasAdultConsiderationExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('hasAdultConsideration', $product);
    }

    #[TestDox('Product.hasAdultConsideration is public')]
    public function testProductHasAdultConsiderationIsPublic()
    {
        $property = new ReflectionProperty(Product::class, 'hasAdultConsideration');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Product.hasAdultConsideration is null by default')]
    public function testProductHasAdultConsiderationIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->hasAdultConsideration);
    }

    #[TestDox('Product.hasAdultConsideration is null by default')]
    public function testProductHasAdultConsiderationAcceptsStoreCoreTypesAdultOrientedEnumerationMembers(): void
    {
        $product = new Product();
        $product->hasAdultConsideration = AdultOrientedEnumeration::UnclassifiedAdultConsideration;
        $this->assertNotNull($product->hasAdultConsideration);
        $this->assertSame('UnclassifiedAdultConsideration', $product->hasAdultConsideration->name);
        $this->assertSame('https://schema.org/UnclassifiedAdultConsideration', $product->hasAdultConsideration->value);
    }


    #[TestDox('AbstractProductOrService.dateCreated and AbstractProductOrService.dateModified are readable')]
    public function testAbstractProductOrServiceDateCreatedAndAbstractProductOrServiceDateModifiedAreReadable(): void
    {
        $product = new Product();

        $this->assertNotEmpty($product->dateCreated);
        $this->assertNotEmpty($product->dateModified);

        $this->assertInstanceOf(\DateTimeInterface::class, $product->dateCreated);
        $this->assertSame($product->dateCreated, $product->dateModified);
    }

    #[TestDox('AbstractProductOrService::getDateModified() method exists')]
    public function testAbstractProductOrServiceGetDateModifiedExists(): void
    {
        $class = new ReflectionClass(AbstractProductOrService::class);
        $this->assertTrue($class->hasMethod('getDateModified'));

        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('getDateModified'));
    }

    #[TestDox('AbstractProductOrService::getDateModified() is public')]
    public function testAbstractProductOrServiceGetDateModifiedIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'getDateModified');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('AbstractProductOrService::getDateModified() has no parameters')]
    public function testAbstractProductOrServiceGetDateModifiedHasNoParameters(): void
    {
        $method = new ReflectionMethod(Product::class, 'getDateModified');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('AbstractProductOrService::getDateModified() returns DateTime object by default')]
    public function testAbstractProductOrServiceGetDateModifiedReturnsDateTimeObjectByDefault()
    {
        $product = new Product();
        $this->assertTrue($product->getDateModified() instanceof \DateTime);
        $this->assertSame($product->dateModified, $product->getDateModified());
    }

    #[TestDox('AbstractProductOrService::setDateModified() exists')]
    public function testAbstractProductOrServiceSetDateModifiedExists()
    {
        $class = new ReflectionClass(AbstractProductOrService::class);
        $this->assertTrue($class->hasMethod('setDateModified'));

        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('setDateModified'));
    }

    #[TestDox('AbstractProductOrService::setDateModified() is public')]
    public function testAbstractProductOrServiceSetDateModifiedIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'setDateModified');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('AbstractProductOrService::setDateModified() has one optional parameter')]
    public function testAbstractProductOrServiceSetDateModifiedHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Product::class, 'setDateModified');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('AbstractProductOrService::setDateModified() accepts DateTime object')]
    public function testAbstractProductOrServiceSetDateModifiedAcceptsDateTimeObject(): void
    {
        $product = new Product();
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $product->setDateModified($now);
        $this->assertEquals($now, $product->getDateModified());
    }

    #[TestDox('AbstractProductOrService::setDateModified() accepts date/time string')]
    public function testAbstractProductOrServiceSetDateModifiedAcceptsDateTimeString(): void
    {
        $string = '2018-04-01 22:21:20';
        $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $string);

        $product_string = new Product();
        $product_string->setDateModified($string);

        $product_datetime = new Product();
        $product_datetime->setDateModified($datetime);

        $this->assertNotEquals($product_string, $product_datetime);
        $this->assertEquals($product_string->getDateModified(), $product_datetime->getDateModified());
    }


    #[TestDox('Product.itemCondition exists')]
    public function testProductItemConditionExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('itemCondition', $product);
    }

    #[TestDox('Product.itemCondition is public')]
    public function testProductItemConditionIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'itemCondition');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Product.itemCondition is not empty')]
    public function testProductItemConditionIsNotEmpty(): void
    {
        $product = new Product();
        $this->assertNotEmpty($product->itemCondition);
    }

    #[TestDox('Product.itemCondition is StoreCore\PIM\OfferItemCondition object')]
    public function testProductItemConditionIsStoreCorePIMOfferItemConditionObject(): void
    {
        $product = new Product();
        $this->assertInstanceOf(\StoreCore\PIM\OfferItemCondition::class, $product->itemCondition);
        $this->assertInstanceOf(OfferItemCondition::class, $product->itemCondition);
    }

    #[Depends('testProductItemConditionIsStoreCorePIMOfferItemConditionObject')]
    #[TestDox('Product.itemCondition is https://schema.org/NewCondition by default')]
    public function testProductItemConditionIsSchemaOrgNewConditionByDefault(): void
    {
        $product = new Product();
        $this->assertEquals('https://schema.org/NewCondition', $product->itemCondition->value);
    }


    #[Group('seo')]
    #[TestDox('Product.keywords exists')]
    public function testProductKeywordsExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('keywords', $product);
    }

    #[Group('seo')]
    #[TestDox('Product.keywords is not public')]
    public function testProductKeywordsIsNotPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'keywords');
        $this->assertTrue($property->isPrivate());
        $this->assertFalse($property->isProtected());
        $this->assertFalse($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Group('seo')]
    #[TestDox('Product.keywords is null by default')]
    public function testProductKeywordsIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->keywords);
    }

    #[Group('seo')]
    #[TestDox('Product.keywords accepts string')]
    public function testProductKeywordsAcceptsString(): void
    {
        $product = new Product();
        $product->keywords = 'foo, bar, baz, qux';
        $this->assertNotNull($product->keywords);
        $this->assertNotEmpty($product->keywords);
        $this->assertIsString($product->keywords);
        $this->assertEquals('foo, bar, baz, qux', $product->keywords);

        // Comma and space
        $product->keywords = 'Foo,Bar,Baz';
        $this->assertEquals('Foo, Bar, Baz', $product->keywords);

        // Only single spaces
        $product->keywords = 'Foo  Bar,  Baz  Qux';
        $this->assertEquals('Foo Bar, Baz Qux', $product->keywords);
    }

    #[Group('seo')]
    #[TestDox('Product.keywords accepts array')]
    public function testProductKeywordsAcceptsArray(): void
    {
        $product = new Product();
        $product->keywords = array(
            'Foo',
            'Bar',
            'Baz',
        );

        $this->assertNotEmpty($product->keywords);
        $this->assertIsNotArray($product->keywords);
        $this->assertIsString($product->keywords);
        $this->assertEquals('Foo, Bar, Baz', $product->keywords);
    }

    #[Group('seo')]
    #[TestDox('Product.keywords contains no duplicates')]
    public function testProductKeywordsContainsNoDuplicates(): void
    {
        $product = new Product();
        $product->keywords = 'foo, bar, baz, foo, qux';
        $this->assertEquals('foo, bar, baz, qux', $product->keywords);

        $product->keywords = array(
            'Foo',
            'Bar',
            'Foo',
            'Baz',
        );
        $this->assertEquals('Foo, Bar, Baz', $product->keywords);
    }


    #[TestDox('Product.mpn exists')]
    public function testProductMpnExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('mpn', $product);
    }

    #[TestDox('Product.mpn is public')]
    public function testProductMpnIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'mpn');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Product.mpn is null by default')]
    public function testProductMpnIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->brand);
    }

    /**
     * @see https://en.wikipedia.org/wiki/Part_number
     */
    #[TestDox('Product.mpn is null by default')]
    public function testProductMpnAcceptsStrings(): void
    {
        $product = new Product();
        $product->name = 'Hardware, screw, machine, 4-40, 3/4″ long, pan head, Phillips';
        $product->mpn = 'HSC0424PP';
        $this->assertNotNull($product->mpn);
        $this->assertIsString($product->mpn);
        $this->assertEquals('HSC0424PP', $product->mpn);
    }

    #[TestDox('Product.mpn accepts Part Number prefixes')]
    public function testProductMpnAcceptsPartNumberPrefixes(): void
    {
        $product = new Product();
        $product->name = 'Hardware, screw, machine, 4-40, 3/4″ long, pan head, Phillips';

        $product->mpn = 'PN HSC0424PP';
        $this->assertEquals('PN HSC0424PP', $product->mpn);

        $product->mpn = 'P/N HSC0424PP';
        $this->assertEquals('P/N HSC0424PP', $product->mpn);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Product",
              "itemCondition": "https://schema.org/NewCondition",
              "mpn": "P/N HSC0424PP",
              "name": "Hardware, screw, machine, 4-40, 3/4″ long, pan head, Phillips"
            }
        ';
        $actualJson = json_encode($product, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    /**
     * @see https://en.wikipedia.org/wiki/Part_number#User_part_numbers_versus_manufacturing_part_numbers_(MPN)
     *      User part numbers versus manufacturing part numbers (MPN)
     */
    #[TestDox('Product.mpn accepts multiple part numbers')]
    public function testProductMpnAcceptsMultiplePartNumbers()
    {
        $product = new Product();
        $product->name = 'Hardware, screw, machine, 4-40, 3/4″ long, Phillips';
        $this->assertEmpty($product->mpn);

        /*
         * Manufacturer A uses part number "4-40-3/4"-pan-phil",
         * Manufacturer B uses part number "100-440-0.750-3434-A".
         * Manufacturer C uses part number "TSR-1002".
         */
        $product->mpn = array(
            '4-40-3/4"-pan-phil',
            '100-440-0.750-3434-A',
            'TSR-1002',
        );

        $this->assertNotEmpty($product->mpn);
        $this->assertIsArray($product->mpn);
        $this->assertCount(3, $product->mpn);
    }


    #[TestDox('Product.name exists')]
    public function testProductNameExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('name', $product);
    }

    #[TestDox('Product.name is public')]
    public function testProductNameIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'name');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Product.name is null by default')]
    public function testProductNameIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->name);
    }

    /**
     * @see https://schema.org/Product#eg-0010 Schema.org Product Example 1
     */
    #[Group('seo')]
    #[TestDox('Product.name can be set to string')]
    public function testProductNameCanBeSetToString(): void
    {
        $product = new Product();
        $this->assertEmpty($product->name);

        $product->name = 'Kenmore White 17″ Microwave';
        $this->assertNotEmpty($product->name);
        $this->assertIsString($product->name);
    }


    #[TestDox('Product::setIdentifier() exists')]
    public function testProductSetIdentifierExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('setIdentifier'));
    }

    #[TestDox('Product::setIdentifier() is public')]
    public function testProductSetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'setIdentifier');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Product::setIdentifier() has one REQUIRED parameter')]
    public function testProductSetIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Product::class, 'setIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Product::setIdentifier() throws TypeError exception on integer')]
    public function testProductSetIdentifierThrowsTypeErrorExceptionOnInteger(): void
    {
        $isbn = 9789043017121;
        $this->assertIsInt($isbn);
        $product = new Product();
        $this->expectException(\TypeError::class);
        $product->setIdentifier($isbn);
    }

    #[TestDox('Product::setIdentifier() throws ValueError exception on invalid string')]
    public function testProductSetIdentifierThrowsValueErrorExceptionOnInvalidString(): void
    {
        $product = new Product();
        $this->expectException(\ValueError::class);
        $product->setIdentifier('123-456-789');
    }

    #[TestDox('Product::setIdentifier() accepts UUID v1 and v4 strings')]
    public function testProductSetIdentifierAcceptsUUIDStrings(): void
    {
        $product = new Product();
        $uuids = [
            '00000000-0000-0000-0000-000000000000',
            '9edb2384-234f-11ec-9621-0242ac130002',
            'c8ba99d2-4eca-4e13-8e2e-1bc9bf3838f8',
            'b9bf6ba6-234f-11ec-9621-0242ac130002',
            'aca78ef7-fd71-4f63-bc8d-502963fcf6fa',
            'e3158a62-234f-11ec-9621-0242ac130002',
            'c446909b-1d13-44e1-994f-15a69ebd8531',
            'fc8b8ae6-234f-11ec-9621-0242ac130002',
            '956c66bb-12ea-4e67-83f9-cb497a8a66a0',
            '0f846e7e-2350-11ec-9621-0242ac130002',
            '82e543bd-9f67-48d6-9f81-e4c5311a382b',
            'ffffffff-ffff-ffff-ffff-ffffffffffff',
        ];
        foreach ($uuids as $uuid) {
            $product->setIdentifier($uuid);
            $this->assertInstanceOf(UUID::class, $product->getIdentifier());
            $this->assertEquals($uuid, (string) $product->getIdentifier());
        }
    }

    #[TestDox('Product::getIdentifier() exists')]
    public function testProductGetIdentifierExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[TestDox('Product::getIdentifier() is public')]
    public function testProductGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'getIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Product::getIdentifier() has no parameters')]
    public function testProductGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Product::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Product::getIdentifier() returns null by default')]
    public function testProductGetIdentifierReturnsNullByDefault(): void
    {
        $product = new Product();
        $this->assertFalse($product->hasIdentifier());
        $this->assertNull($product->getIdentifier());
    }

    #[TestDox('Product::getIdentifier() returns set UUID object')]
    public function testProductGetIdentifierReturnsSetUUIDObject(): void
    {
        $uuid_v4_string = '2092e1dd-5c3d-4996-a4c2-7c45d81cea50';
        $uuid_object = new UUID($uuid_v4_string);
        $product = new Product();
        $product->setIdentifier($uuid_object);

        $this->assertIsObject($product->getIdentifier());
        $this->assertInstanceOf(UUID::class, $product->getIdentifier());
        $this->assertSame($uuid_object, $product->getIdentifier());
        $this->assertSame($uuid_v4_string, (string) $product->getIdentifier());
    }


    #[TestDox('Product.releaseDate exists')]
    public function testProductReleaseDateExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('releaseDate', $product);
    }

    #[TestDox('Product.releaseDate is null by default')]
    public function testProductReleaseDateIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->releaseDate);
    }

    #[TestDox('Product::setReleaseDate() exists')]
    public function testProductSetReleaseDateExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('setReleaseDate'));
    }

    #[TestDox('Product::setReleaseDate() is public')]
    public function testProductSetReleaseDateIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'setReleaseDate');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Product::setReleaseDate() has one optional parameter')]
    public function testProductSetReleaseDateHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Product::class, 'setReleaseDate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Product::setReleaseDate() has accepts string')]
    public function testProductSetReleaseDateAcceptsString(): void
    {
        $product = new Product();
        $product->name = 'Google Pixel 7 Pro';
        $product->setReleaseDate('October 6, 2022');

        $this->assertNotEmpty($product->releaseDate);
        $this->assertIsObject($product->releaseDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->releaseDate);
        $this->assertEquals('2022-10-06', $product->releaseDate->format('Y-m-d'));
    }

    #[Depends('testProductSetReleaseDateAcceptsString')]
    #[TestDox('Product::setReleaseDate() accepts an ISO date as string')]
    public function testProductSetReleaseDateAcceptsISODateAsString(): void
    {
        $product = new Product();

        $today = gmdate('Y-m-d');
        $this->assertIsString($today);

        $product->setReleaseDate($today);
        $this->assertNotNull($product->releaseDate);
        $this->assertIsObject($product->releaseDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->releaseDate);
        $this->assertEquals($today, $product->releaseDate->format('Y-m-d'));
    }

    #[TestDox('Product::setReleaseDate() without parameter sets current date and time')]
    public function testProductSetReleaseDateWithoutParameterSetsCurrentDateAndTime(): void
    {
        $product = new Product();
        $product->setReleaseDate();
        $this->assertNotNull($product->releaseDate);
        $this->assertIsObject($product->releaseDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->releaseDate);
        $this->assertEquals(gmdate('Y-m-d'), $product->releaseDate->format('Y-m-d'));
    }

    #[Group('seo')]
    #[TestDox('Product::setReleaseDate() sets Schema.org JSON-LD `releaseDate` property')]
    public function testProductSetReleaseDateSetsSchemaOrgJSONLDreleaseDateProperty(): void
    {
        $product = new Product();
        $product->name = 'Google Pixel 7 Pro';
        $json = json_encode($product);
        $this->assertStringNotContainsString('releaseDate', $json);

        $product->setReleaseDate('October 6, 2022');
        $json = json_encode($product);
        $this->assertStringContainsString('releaseDate', $json);
        $this->assertStringContainsString('"releaseDate":"2022-10-06"', $json);

        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Product",
            "itemCondition": "https://schema.org/NewCondition",
            "name": "Google Pixel 7 Pro",
            "releaseDate": "2022-10-06"
          }
        ';
        $actualJson = json_encode($product, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Product::setSalesDiscontinuationDate() exists')]
    public function testProductSetSalesDiscontinuationDateExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('setSalesDiscontinuationDate'));
    }

    #[TestDox('Product::setSalesDiscontinuationDate() is public')]
    public function testProductSetSalesDiscontinuationDateIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'setSalesDiscontinuationDate');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Product::setSalesDiscontinuationDate() has one optional parameter')]
    public function testProductSetSalesDiscontinuationDateHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Product::class, 'setSalesDiscontinuationDate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Product::setSalesDiscontinuationDate() sets current date and time')]
    public function testProductSetSalesDiscontinuationDateSetsCurrentDateAndTime(): void
    {
        $product = new Product();
        $this->assertNull($product->salesDiscontinuationDate);

        $product->setSalesDiscontinuationDate();
        $this->assertNotNull($product->salesDiscontinuationDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->salesDiscontinuationDate);
        $this->assertEquals(date('Y-m-d H:i'), $product->salesDiscontinuationDate->format('Y-m-d H:i'));
    }


    #[TestDox('Product.sku exists')]
    public function testProductSkuExists(): void
    {
        $product = new Product();
        $this->assertObjectHasProperty('sku', $product);
    }

    #[TestDox('Product.sku is public')]
    public function testProductSkuIsPublic(): void
    {
        $property = new ReflectionProperty(Product::class, 'sku');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Product.sku is null by default')]
    public function testProductSkuIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->sku);
    }

    /**
     * @see https://schema.org/OrderItem#eg-0376 Schema.org OrderItem example 1
     */
    #[TestDox('Product.sku accepts string')]
    public function testProductSkuAcceptsString(): void
    {
        $product = new Product();
        $product->sku = 'abc123';
        $this->assertNotNull($product->sku);
        $this->assertIsString($product->sku);
        $this->assertSame('abc123', $product->sku);

        $product->sku = 'def456';
        $this->assertSame('def456', $product->sku);
    }


    #[TestDox('Product::setSupportDiscontinuationDate() exists')]
    public function testProductSetSupportDiscontinuationDateExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertTrue($class->hasMethod('setSupportDiscontinuationDate'));
    }

    #[TestDox('Product::setSupportDiscontinuationDate() is public')]
    public function testProductSetSupportDiscontinuationDateIsPublic(): void
    {
        $method = new ReflectionMethod(Product::class, 'setSupportDiscontinuationDate');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Product::setSupportDiscontinuationDate() has one REQUIRED parameter')]
    public function testProductSetSupportDiscontinuationDateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Product::class, 'setSupportDiscontinuationDate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Product::getSupportDiscontinuationDate() no longer exists')]
    public function testProductGetSupportDiscontinuationDateNoLongerExists(): void
    {
        $class = new ReflectionClass(Product::class);
        $this->assertFalse($class->hasMethod('getSupportDiscontinuationDate'));
    }

    #[TestDox('Product.supportDiscontinuationDate is null by default')]
    public function testProductSupportDiscontinuationDateIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->supportDiscontinuationDate);
    }

    #[TestDox('Product.supportDiscontinuationDate accepts \DateTimeInterface')]
    public function testProductSupportDiscontinuationDateAcceptsDateTimeInterface(): void
    {
        $product = new Product();
        $product->supportDiscontinuationDate = new \DateTimeImmutable('today');
        $this->assertNotNull($product->supportDiscontinuationDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $product->supportDiscontinuationDate);
        $this->assertEquals(date('Y-m-d'), $product->supportDiscontinuationDate->format('Y-m-d'));
    }

    #[TestDox('Product::setSalesDiscontinuationDate() also sets missing Product.supportDiscontinuationDate')]
    public function testProductSetSalesDiscontinuationDateAlsoSetsMissingProductSupportDiscontinuationDate(): void
    {
        $product = new Product();
        $this->assertNull($product->salesDiscontinuationDate);
        $this->assertNull($product->supportDiscontinuationDate);

        $product->salesDiscontinuationDate = '2023-12-31 23:59';
        $this->assertNotNull($product->salesDiscontinuationDate);
        $this->assertNotNull($product->supportDiscontinuationDate);
        $this->assertNotSame($product->salesDiscontinuationDate, $product->supportDiscontinuationDate);

        $this->assertEquals('2023-12-31 23:59', $product->salesDiscontinuationDate->format('Y-m-d H:i'));
        $this->assertEquals('2025-12-31 23:59', $product->supportDiscontinuationDate->format('Y-m-d H:i'));
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product#product-review-page-example
     */
    #[Group('seo')]
    #[TestDox('Google Product review page acceptance test')]
    public function testGoogleProductReviewPageAcceptanceTest(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Product",
            "name": "Executive Anvil",
            "description": "Sleeker than ACME’s Classic Anvil, the Executive Anvil is perfect for the business traveler looking for something to drop from a height.",
            "itemCondition": "https://schema.org/NewCondition",
            "review": {
              "@type": "Review",
              "reviewRating": {
                "@type": "Rating",
                "bestRating": 5,
                "ratingValue": 4,
                "worstRating": 1
              },
              "author": {
                "@type": "Person",
                "name": "Fred Benson"
              }
            },
            "aggregateRating": {
              "@type": "AggregateRating",
              "bestRating": 5,
              "ratingValue": 4.4,
              "reviewCount": 89,
              "worstRating": 1
            }
          }
        ';

        $product = new Product();
        $product->name = 'Executive Anvil';
        $product->description = 'Sleeker than ACME’s Classic Anvil, the Executive Anvil is perfect for the business traveler looking for something to drop from a height.';

        $this->assertTrue(class_exists(Review::class));
        $this->assertTrue(class_exists(Rating::class));
        $review = new Review();
        $review->reviewRating = new Rating(4);
        $review->author = 'Fred Benson';
        $product->review = $review;

        $this->assertTrue(class_exists(AggregateRating::class));
        $product->aggregateRating = new AggregateRating(4.4, null, 89);

        $actualJson = json_encode($product, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
