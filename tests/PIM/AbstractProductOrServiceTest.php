<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\AbstractProductOrService::class)]
#[Group('hmvc')]
final class AbstractProductOrServiceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractProductOrService class exists')]
    public function testAbstractProductOrServiceClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'AbstractProductOrService.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'AbstractProductOrService.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\AbstractProductOrService'));
        $this->assertTrue(class_exists(AbstractProductOrService::class));
    }

    #[TestDox('AbstractProductOrService class is abstract')]
    public function testAbstractProductOrServiceClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractProductOrService::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }
}
