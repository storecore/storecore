<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;
use StoreCore\CRM\Person;
use StoreCore\Types\ServiceType;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\Service::class)]
#[CoversClass(\StoreCore\PIM\AbstractProductOrService::class)]
#[CoversClass(\StoreCore\PIM\AbstractService::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\Person::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ServiceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Service class exists')]
    public function testServiceClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Service.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Service.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\Service'));
        $this->assertTrue(class_exists(Service::class));
    }

    #[Group('hmvc')]
    #[TestDox('Service class is concrete')]
    public function testServiceClassIsConcrete(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Service extends AbstractProductOrService')]
    public function testServiceExtendsAbstractProductOrService(): void
    {
        $this->assertInstanceOf(AbstractProductOrService::class, new Service());
    }

    #[Group('hmvc')]
    #[TestDox('Service extends AbstractService')]
    public function testServiceExtendsAbstractService(): void
    {
        $service = new Service();
        $this->assertInstanceOf(AbstractService::class, $service);
        $this->assertNotInstanceOf(AbstractProduct::class, $service);
    }

    #[Group('hmvc')]
    #[TestDox('Service implements StoreCore IdentityInterface')]
    public function testServiceImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Service());
    }

    #[Group('seo')]
    #[TestDox('Service is JSON serializable')]
    public function testServiceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Service());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Service::VERSION);
        $this->assertIsString(Service::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Service::VERSION, '0.4.0', '>=')
        );
    }


    #[TestDox('Service.description exists')]
    public function testServiceDescriptionExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasProperty('description'));
    }

    #[TestDox('Service.description is null by default')]
    public function testServiceDescriptionIsNullByDefault(): void
    {
        $service = new Service();
        $this->assertEmpty($service->description);
        $this->assertNull($service->description);
    }

    #[TestDox('Service.description accepts string')]
    public function testServiceDescriptionAcceptsString(): void
    {
        $service = new Service();
        $service->description = 'furnace installation';

        $this->assertNotEmpty($service->description);
        $this->assertIsString($service->description);
        $this->assertSame('furnace installation', $service->description);
    }


    #[TestDox('Service.identifier exists')]
    public function testServiceIdentifierExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasProperty('identifier'));
    }

    #[TestDox('Service::getIdentifier() exists')]
    public function testServiceGetIdentifierExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[TestDox('Service::getIdentifier() is public')]
    public function testServiceGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Service::class, 'getIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Service::getIdentifier() has no parameters')]
    public function testServiceGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Service::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Service::getIdentifier() returns null by default')]
    public function testServiceGetIdentifierReturnsNullByDefault(): void
    {
        $service = new Service();
        $this->assertNull($service->getIdentifier());
    }

    #[TestDox('Service::hasIdentifier() exists')]
    public function testServiceHasIdentifierExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasMethod('hasIdentifier'));
    }

    #[TestDox('Service::hasIdentifier() is public')]
    public function testServiceHasIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Service::class, 'hasIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Service::hasIdentifier() has no parameters')]
    public function testServiceHasIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Service::class, 'hasIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Service::hasIdentifier() returns boolean')]
    public function testServiceHasIdentifierReturnsBoolean(): void
    {
        $service = new Service();
        $this->assertIsBool($service->hasIdentifier());
        $this->assertFalse($service->hasIdentifier());

        $service->setIdentifier('b9934e42-77f8-45f6-b3d0-e2b864cd84c3');
        $this->assertTrue($service->hasIdentifier());
    }


    #[TestDox('Service.provider exists')]
    public function testServiceProviderExists(): void
    {
        $class = new ReflectionClass(Service::class);
        $this->assertTrue($class->hasProperty('provider'));
    }

    #[TestDox('Service.provider is null by default')]
    public function testServiceProviderIsNullByDefault(): void
    {
        $service = new Service();
        $this->assertEmpty($service->provider);
        $this->assertNull($service->provider);
    }

    /**
     * Sets some `@type` of `Organization`, for example a `LocalBusiness`,
     * as the `provider` of the `Service`:
     *
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "Service",
     *       "provider": {
     *         "@type": "LocalBusiness",
     *         "name": "ACME Home Cleaning"
     *       }
     *     }
     *     ```
     *
     * @see https://schema.org/Service#eg-0396 Schema.org type `Service` example 2
     */
    #[Group('seo')]
    #[TestDox('Service.provider accepts Organization')]
    public function testServiceProviderAcceptsOrganization()
    {
        $organization = new Organization();
        $organization->type = OrganizationType::LocalBusiness;
        $organization->name = 'ACME Home Cleaning';

        $service = new Service();
        $service->provider = $organization;

        $this->assertNotNull($service->provider);
        $this->assertIsObject($service->provider);
        $this->assertInstanceOf(\JsonSerializable::class, $service->provider);
        $this->assertInstanceOf(Organization::class, $service->provider);
        $this->assertSame('ACME Home Cleaning', $service->provider->name);
    }

    /**
     * The `provider` of a `Service` MAY be a `Person`:
     *
     *     ```json
     *     {
     *       "@context": "https://schema.org",
     *       "@type": "TaxiService",
     *       "provider": {
     *         "@type": "Person",
     *         "name": "Travis Bickle"
     *       }
     *     }
     *     ```
     */
    #[Group('seo')]
    #[TestDox('Service.provider accepts Person')]
    public function testServiceProviderAcceptsPerson(): void
    {
        $taxi = new Service();
        $taxi->type = ServiceType::TaxiService;
        $this->assertNull($taxi->provider);

        $driver = new Person();
        $driver->setName('Travis Bickle');
        $taxi->provider = $driver;

        $this->assertNotNull($taxi->provider);
        $this->assertIsObject($taxi->provider);
        $this->assertInstanceOf(\JsonSerializable::class, $taxi->provider);
        $this->assertInstanceOf(Person::class, $taxi->provider);
        $this->assertSame('Travis Bickle', (string) $taxi->provider->name);
    }


    #[Group('hmvc')]
    #[TestDox('Service.type exists')]
    public function testServiceTypeExists(): void
    {
        $service = new Service();
        $this->assertObjectHasProperty('type', $service);
    }

    #[Group('hmvc')]
    #[TestDox('Service.type is non-empty string')]
    public function testServiceTypeIsNonEmptyString(): void
    {
        $service = new Service();
        $this->assertNotEmpty($service->type);
        $this->assertIsObject($service->type);

        $this->assertTrue(class_exists('\\StoreCore\\Types\\ServiceType'));
        $this->assertInstanceOf(\StoreCore\Types\ServiceType::class, $service->type);
    }

    #[Group('hmvc')]
    #[TestDox('Service.type is non-empty string')]
    public function testServiceTypeIsSchemaOrgServiceTypeServiceByDefault(): void
    {
        $service = new Service();
        $this->assertSame('Service', $service->type->name);
        $this->assertSame('https://schema.org/Service', $service->type->value);
    }

    #[Group('seo')]
    #[TestDox('JSON encoded Service contains @type Service')]
    public function testJsonEncodedServiceContainsTypeService(): void
    {
        $service = new Service();
        $json = json_encode($service, \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"@type":"Service"', $json);
    }


    /**
     * @see https://schema.org/Service#eg-0375
     */
    #[Group('seo')]
    #[TestDox('Schema.org type `Service` example 1')]
    public function testSchemaOrgTypeServiceExample1(): void
    {
        $orderItem = new Service();
        $orderItem->description = 'furnace installation';
        $this->assertSame('furnace installation', $orderItem->description);

        $json = json_encode($orderItem, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@context":"https://schema.org"', $json);
        $this->assertStringContainsString('"@type":"Service"', $json);
        $this->assertStringContainsString('"description":"furnace installation"', $json);
    }

    /**
     * @see https://schema.org/Service#eg-0396
     */
    #[Group('seo')]
    #[TestDox('Schema.org type `Service` example 2')]
    public function testSchemaOrgTypeServiceExample2()
    {
        $itemOffered = new Service();
        $itemOffered->name = 'Apartment light cleaning';
        $this->assertSame('Apartment light cleaning', $itemOffered->name);

        $json = json_encode($itemOffered, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@context":"https://schema.org"', $json);
        $this->assertStringContainsString('"@type":"Service"', $json);
        $this->assertStringContainsString('"name":"Apartment light cleaning"', $json);
    }
}
