<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\Currency;
use StoreCore\Database\Currencies;
use StoreCore\Types\CommonCode;
use StoreCore\Types\QuantitativeValue;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\UnitPriceSpecification::class)]
#[CoversClass(\StoreCore\PIM\PriceSpecification::class)]
#[CoversClass(\StoreCore\Types\CommonCode::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Database\Currencies::class)]
#[UsesClass(\StoreCore\Types\QuantitativeValue::class)]
final class UnitPriceSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('UnitPriceSpecification class exists')]
    public function testUnitPriceSpecificationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'UnitPriceSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'UnitPriceSpecification.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\UnitPriceSpecification'));
        $this->assertTrue(class_exists(UnitPriceSpecification::class));
    }

    #[Group('hmvc')]
    #[TestDox('UnitPriceSpecification class is concrete')]
    public function testUnitPriceSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(UnitPriceSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('UnitPriceSpecification is a PriceSpecification')]
    public function testUnitPriceSpecificatioIsPriceSpecification(): void
    {
        $price = new UnitPriceSpecification();
        $this->assertInstanceOf(PriceSpecification::class, $price);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(UnitPriceSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(UnitPriceSpecification::VERSION);
        $this->assertIsString(UnitPriceSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(UnitPriceSpecification::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('UnitPriceSpecification.billingIncrement exists')]
    public function testUnitPriceSpecificationBillingIncrementExists(): void
    {
        $unitPriceSpecification = new UnitPriceSpecification();
        $this->assertObjectHasProperty('billingIncrement', $unitPriceSpecification);
    }

    #[TestDox('UnitPriceSpecification.billingIncrement is public')]
    public function testUnitPriceSpecificationBillingIncrementIsPublic(): void
    {
        $property = new ReflectionProperty(UnitPriceSpecification::class, 'billingIncrement');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('UnitPriceSpecification.billingIncrement is null by default')]
    public function testUnitPriceSpecificationBillingIncrementIsNullByDefault(): void
    {
        $billingIncrement = new UnitPriceSpecification();
        $this->assertNull($billingIncrement->billingIncrement);
    }


    #[TestDox('UnitPriceSpecification::setCommonCode() exists')]
    public function testUnitPriceSpecificationSetCommonCodeExists(): void
    {
        $class = new ReflectionClass(UnitPriceSpecification::class);
        $this->assertTrue($class->hasMethod('setCommonCode'));
    }

    #[TestDox('UnitPriceSpecification::setCommonCode() is public')]
    public function tesUnitPriceSpecificationSetCommonCodeIsPublic(): void
    {
        $method = new ReflectionMethod(UnitPriceSpecification::class, 'setCommonCode');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('UnitPriceSpecification::setCommonCode() has one REQUIRED parameters')]
    public function testUnitPriceSpecificationSetCommonCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(UnitPriceSpecification::class, 'setCommonCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('UnitPriceSpecification::setCommonCode() sets UnitPriceSpecification.unitCode')]
    public function testUnitPriceSpecificationSetCommonCodeSetsUnitPriceSpecificationUnitCode(): void
    {
        $litre = new CommonCode('LTR', 'l', 'litre');
        $price_per_litre = new UnitPriceSpecification();
        $price_per_litre->setCommonCode($litre);

        $this->assertNotNull($price_per_litre->unitCode);
        $this->assertIsString($price_per_litre->unitCode);
        $this->assertSame('LTR', $price_per_litre->unitCode);
    }

    #[TestDox('UnitPriceSpecification::setCommonCode() sets uppercase UN/CEFACT unitCode')]
    public function testUnitPriceSpecificationSetCommonCodeSetsUppercaseUnCefactUnitCode(): void
    {
        $litre = new CommonCode('ltr', 'l', 'litre');
        $price_per_litre = new UnitPriceSpecification();
        $price_per_litre->setCommonCode($litre);

        $this->assertNotEquals('ltr', $price_per_litre->unitCode);
        $this->assertEquals('LTR', $price_per_litre->unitCode);
    }

    #[TestDox('UnitPriceSpecification::setCommonCode() sets UnitPriceSpecification.unitText')]
    public function testUnitPriceSpecificationSetCommonCodeSetsUnitPriceSpecificationUnitText(): void
    {
        $price = new UnitPriceSpecification();
        $this->assertEmpty($price->unitText);

        $litre = new CommonCode('LTR', 'ℓ', 'litre');
        $price->setCommonCode($litre);
        $this->assertNotEmpty($price->unitText);
        $this->assertIsString($price->unitText);
        $this->assertEquals('ℓ', $price->unitText);
    }


    /**
     * @see https://schema.org/UnitPriceSpecification#eg-0350
     */
    #[Group('seo')]
    #[TestDox('@testdox Schema.org acceptance tests')]
    public function testSchemaOrgAcceptanceTests()
    {
        // Price of 25 USD per year:
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "UnitPriceSpecification",
              "price": 25,
              "priceCurrency": "USD",
              "referenceQuantity": {
                "@type": "QuantitativeValue",
                "value": 1,
                "unitCode": "ANN"
              }
            }
        ';

        $priceSpecification = new UnitPriceSpecification();
        $priceSpecification->price = 25;
        $priceSpecification->priceCurrency = Currencies::createFromString('USD');

        // Remove the VAT specification:
        $priceSpecification->valueAddedTaxIncluded = null;

        // Add the reference quantity:
        $priceSpecification->referenceQuantity = new QuantitativeValue(1, 'ANN');

        $actualJson = json_encode($priceSpecification, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
