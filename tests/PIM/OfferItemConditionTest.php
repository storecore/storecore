<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\OfferItemCondition::class)]
final class OfferItemConditionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OfferItemCondition class exists')]
    public function testOfferItemConditionClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferItemCondition.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferItemCondition.php');
        $this->assertTrue(enum_exists('\\StoreCore\\PIM\\OfferItemCondition'));
        $this->assertTrue(enum_exists(OfferItemCondition::class));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionEnum(OfferItemCondition::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OfferItemCondition::VERSION);
        $this->assertIsString(OfferItemCondition::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org v27.0')]
    public function testVersionMatchesSchemaOrgVersion27(): void
    {
        $this->assertTrue(
            version_compare(OfferItemCondition::VERSION, '27.0', '>=')
        );
    }


    #[TestDox('OfferItemCondition enumeration has four (4) cases')]
    public function testOfferItemConditionEnumerationHasFourCases(): void
    {
        $this->assertCount(4, OfferItemCondition::cases());
    }


    #[TestDox('OfferItemCondition::fromInteger exists')]
    public function testOfferItemConditionFromIntegerExists(): void
    {
        $enum = new ReflectionEnum(OfferItemCondition::class);
        $this->assertTrue($enum->hasMethod('fromInteger'));
    }

    #[Depends('testOfferItemConditionFromIntegerExists')]
    #[TestDox('OfferItemCondition::fromInteger is public static')]
    public function testOfferItemConditionFromIntegerIsPublicStatic(): void
    {
        $method = new ReflectionMethod(OfferItemCondition::class, 'fromInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testOfferItemConditionFromIntegerIsPublicStatic')]
    #[TestDox('OfferItemCondition::fromInteger has one REQUIRED parameter')]
    public function testOfferItemConditionFromIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OfferItemCondition::class, 'fromInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOfferItemConditionFromIntegerIsPublicStatic')]
    #[TestDox('OfferItemCondition::fromInteger throws value error exception on invalid integer')]
    public function testOfferItemConditionFromIntegerThrowsValueErrorExceptionOnInvalidInteger(): void
    {
        $this->expectException(\ValueError::class);
        $failure = OfferItemCondition::fromInteger(4);
    }


    #[TestDox('OfferItemCondition::NewCondition exists')]
    public function testOfferItemConditionNewConditionExists(): void
    {
        $this->assertEquals('https://schema.org/NewCondition', OfferItemCondition::NewCondition->value);
        $this->assertEquals(OfferItemCondition::NewCondition, OfferItemCondition::fromInteger(0));
    }
}
