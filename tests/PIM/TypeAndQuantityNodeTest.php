<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\PIM\TypeAndQuantityNode::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class TypeAndQuantityNodeTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('TypeAndQuantityNode class exists')]
    public function testTypeAndQuantityNodeClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'TypeAndQuantityNode.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'TypeAndQuantityNode.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\TypeAndQuantityNode'));
        $this->assertTrue(class_exists(TypeAndQuantityNode::class));
    }

    #[Group('hmvc')]
    #[TestDox('TypeAndQuantityNode class is concrete')]
    public function testTypeAndQuantityNodeClassIsConcrete(): void
    {
        $class = new ReflectionClass(TypeAndQuantityNode::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TypeAndQuantityNode class is read-only')]
    public function testTypeAndQuantityNodeClassIsReadOnly(): void
    {
        $class = new ReflectionClass(TypeAndQuantityNode::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[TestDox('TypeAndQuantityNode is countable')]
    public function testTypeAndQuantityNodeIsCountable(): void
    {
        // 42 units of a countable product:
        $product = new Product();
        $node = new TypeAndQuantityNode($product, 42);
        $this->assertInstanceOf(\Countable::class, $node);
        $this->assertNotEmpty(count($node));
        $this->assertIsInt(count($node));
        $this->assertEquals(42, count($node));

        // 2 kilograms (KGM) of an uncountable product:
        $product = new Product();
        $node = new TypeAndQuantityNode($product, 2, 'KGM');
        $this->assertNotEquals(2, count($node));
        $this->assertEquals(1, count($node));

        // 3 labour hours (LH) for a service:
        $service = new Service();
        $node = new TypeAndQuantityNode($service, 3, 'LH');
        $this->assertNotEquals(3, count($node));
        $this->assertEquals(1, count($node));
    }

    #[TestDox('TypeAndQuantityNode is JSON serializable')]
    public function testTypeAndQuantityNodeIsJsonSerializable(): void
    {
        $product = new Product();
        $node = new TypeAndQuantityNode($product);
        $this->assertInstanceOf(\JsonSerializable::class, $node);

        $this->assertNotEmpty($node->jsonSerialize());
        $this->assertIsArray($node->jsonSerialize());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TypeAndQuantityNode::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TypeAndQuantityNode::VERSION);
        $this->assertIsString(TypeAndQuantityNode::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TypeAndQuantityNode::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('TypeAndQuantityNode::__construct() exists')]
    public function testTypeAndQuantityNodeConstructorExists(): void
    {
        $class = new ReflectionClass(TypeAndQuantityNode::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('TypeAndQuantityNode::__construct() is public constructor')]
    public function testTypeAndQuantityNodeConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(TypeAndQuantityNode::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('TypeAndQuantityNode::__construct() has three parameters')]
    public function testTypeAndQuantityNodeConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(TypeAndQuantityNode::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[TestDox('TypeAndQuantityNode::__construct() has one REQUIRED parameter')]
    public function testTypeAndQuantityNodeConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(TypeAndQuantityNode::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('TypeAndQuantityNode.typeOfGood property')]
    public function testTypeAndQuantityNodeTypeOfGoodProperty(): void
    {
        $product = new Product();
        $product->setIdentifier('d6959554-2786-4116-84bb-e83e266ab0d7');

        $node = new TypeAndQuantityNode($product);
        $this->assertObjectHasProperty('typeOfGood', $node);
        $this->assertIsObject($node->typeOfGood);
        $this->assertSame($product, $node->typeOfGood);
        $this->assertEquals('d6959554-2786-4116-84bb-e83e266ab0d7', (string) $node->typeOfGood->getIdentifier());
    }

    #[TestDox('TypeAndQuantityNode.amountOfThisGood property')]
    public function testTypeAndQuantityNodeAmountOfThisGoodProperty(): void
    {
        $product = new Product();

        $node = new TypeAndQuantityNode($product);
        $this->assertObjectHasProperty('amountOfThisGood', $node);
        $this->assertIsInt($node->amountOfThisGood);
        $this->assertEquals(1, $node->amountOfThisGood);

        $node = new TypeAndQuantityNode($product, 6);
        $this->assertNotEquals(1, $node->amountOfThisGood);
        $this->assertEquals(6, $node->amountOfThisGood);
    }


    #[TestDox('TypeAndQuantityNode.unitCode exists')]
    public function testTypeAndQuantityNodeUnitCodeExists(): void
    {
        $product = new Product();
        $typeAndQuantityNode = new TypeAndQuantityNode($product);
        $this->assertObjectHasProperty('unitCode', $typeAndQuantityNode);
    }

    #[TestDox('TypeAndQuantityNode.unitCode is (string) `C62` by default')]
    public function testTypeAndQuantityNodeUnitCodeIsStringC62ByDefault(): void
    {
        $product = new Product();
        $node = new TypeAndQuantityNode($product);

        $this->assertNotEmpty($node->unitCode);
        $this->assertIsString($node->unitCode);
        $this->assertEquals('C62', $node->unitCode);
    }

    #[TestDox('TypeAndQuantityNode.unitCode accepts UN/CEFACT Common Codes')]
    public function testTypeAndQuantityNodeUnitCodeAcceptsUnCefactCommonCodes(): void
    {
        $product = new Product();
        $product->name = 'Heineken Premium pilsener';
        $product->gtin13 = '8712000058852';

        $node = new TypeAndQuantityNode($product, 0.33, 'LTR');
        $this->assertEquals('LTR', $node->unitCode);

        $node = new TypeAndQuantityNode($product, 33, 'CLT');
        $this->assertEquals('CLT', $node->unitCode);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "TypeAndQuantityNode",
              "amountOfThisGood": 33,
              "unitCode": "CLT",
              "typeOfGood": {
                "@context": "https://schema.org",
                "@type": "Product",
                "gtin13": "8712000058852",
                "itemCondition": "https://schema.org/NewCondition",
                "name": "Heineken Premium pilsener"
              }
            }
        ';
        $actualJson = json_encode($node, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
