<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\CMS\ImageObject;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\PIM\Brand::class)]
#[UsesClass(\StoreCore\CMS\ImageObject::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class BrandTest extends TestCase
{
    #[TestDox('Brand class is concrete')]
    public function testBrandClassIsConcrete(): void
    {
        $class = new ReflectionClass(Brand::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Brand implements IdentityInterface')]
    public function testBrandImplementsIdentityInterface(): void
    {
        $class = new ReflectionClass(Brand::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\IdentityInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('Brand is JSON serializable')]
    public function testBrandIsJsonSerializable(): void
    {
        $class = new ReflectionClass(Brand::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Brand::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Brand::VERSION);
        $this->assertIsString(Brand::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Brand::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Brand.aggregateRating exists')]
    public function testBrandAggregateRatingExists(): void
    {
        $brand = new Brand();
        $this->assertObjectHasProperty('aggregateRating', $brand);
    }

    #[TestDox('Brand.aggregateRating is public')]
    public function testBrandAggregateRatingIsPublic(): void
    {
        $property = new ReflectionProperty(Brand::class, 'aggregateRating');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Brand.aggregateRating is null by default')]
    public function testBrandAggregateRatingIsNullByDefault(): void
    {
        $brand = new Brand();
        $this->assertNull($brand->aggregateRating);
    }


    #[TestDox('Brand.logo exists')]
    public function testBrandLogoExists(): void
    {
        $brand = new Brand();
        $this->assertObjectHasProperty('logo', $brand);
    }

    #[TestDox('Brand.logo is public')]
    public function testBrandLogoIsPublic(): void
    {
        $property = new ReflectionProperty(Brand::class, 'logo');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Brand.logo is null by default')]
    public function testBrandLogoIsNullByDefault(): void
    {
        $brand = new Brand();
        $this->assertNull($brand->logo);
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Brand.logo accepts logo URL')]
    public function testBrandLogoAcceptsLogoURL(): void
    {
        $brand = new Brand();
        $this->assertNull($brand->logo);

        $url = new URL('https://www.example.com/images/logo.png');
        $brand->logo = $url;
        $this->assertNotNull($brand->logo);
        $this->assertIsObject($brand->logo);
        $this->assertInstanceOf(URL::class, $brand->logo);
        $this->assertSame($url, $brand->logo);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Brand",
              "logo": "https://www.example.com/images/logo.png"
            }
        JSON;
        $actualJson = json_encode($brand, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('Brand.logo accepts ImageObject')]
    public function testBrandLogoAcceptsImageObject()
    {
        $brand = new Brand();
        $this->assertNull($brand->logo);

        $logo = new ImageObject();
        $logo->contentUrl = new URL('https://www.example.com/images/logo.png');

        $brand->logo = $logo;
        $this->assertNotNull($brand->logo);
        $this->assertIsObject($brand->logo);
        $this->assertInstanceOf(ImageObject::class, $brand->logo);
        $this->assertSame($logo, $brand->logo);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Brand",
              "logo": {
                "@type": "ImageObject",
                "contentUrl": "https://www.example.com/images/logo.png"
              }
            }
        JSON;
        $actualJson = json_encode($brand, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Brand.name exists')]
    public function testBrandNameExists()
    {
        $brand = new Brand();
        $this->assertObjectHasProperty('name', $brand);
    }

    #[TestDox('Brand.name is public')]
    public function testBrandNameIsPublic()
    {
        $property = new ReflectionProperty(Brand::class, 'name');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Brand.name is null by default')]
    public function testBrandNameIsNullByDefault()
    {
        $brand = new Brand();
        $this->assertNull($brand->name);

        $property = new ReflectionProperty(Brand::class, 'name');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertNull($property->getDefaultValue());
    }

    /**
     * @see https://twitter.com/nike/status/1002623183512629248
     */
    #[TestDox('Brand.name accepts string')]
    public function testBrandNameAcceptsString()
    {
        $brand = new Brand();
        $this->assertNull($brand->name);

        $brand->name = 'Nike';
        $this->assertNotEmpty($brand->name);
        $this->assertIsString($brand->name);
        $this->assertEquals('Nike', $brand->name);
    }

    #[TestDox('Brand::__construct sets Brand.name')]
    public function testBrandConstructorSetsBrandName()
    {
        $brand = new Brand('Google Drive™');
        $this->assertNotEmpty($brand->name);
        $this->assertIsString($brand->name);
        $this->assertEquals('Google Drive™', $brand->name);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Brand",
              "name": "Google Drive™"
            }
        ';
        $actualJson = json_encode($brand, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Brand.alternateName exists')]
    public function testBrandAlternateNameExists()
    {
        $brand = new Brand();
        $this->assertObjectHasProperty('alternateName', $brand);
    }

    #[TestDox('Brand.alternateName accepts string')]
    public function testBrandSetNameSetsAlternateNameProperty(): void
    {
        $brand = new Brand();
        $brand->name = 'Nike';
        $brand->alternateName = '耐克';

        $this->assertNotEmpty($brand->alternateName);
        $this->assertIsString($brand->alternateName);
        $this->assertEquals('耐克', $brand->alternateName);

        $brand = new Brand();
        $brand->name = '耐克';
        $brand->alternateName = 'Nike';

        $this->assertNotEmpty($brand->alternateName);
        $this->assertIsString($brand->alternateName);
        $this->assertEquals('Nike', $brand->alternateName);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Brand",
              "name": "耐克",
              "alternateName": "Nike"
            }
        ';
        $actualJson = json_encode($brand, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    /**
     * @see https://schema.org//brand
     *      Schema.org `brand` property
     */
    #[Test]
    #[TestDox('Schema.org entity types with a brand property')]
    public function testSchemaOrgEntityTypesWithBrandProperty(): void
    {
        $class = new ReflectionClass(\StoreCore\CRM\Organization::class);
        $this->assertTrue($class->hasProperty('brand'));

        $class = new ReflectionClass(\StoreCore\CRM\Person::class);
        $this->assertFalse(
            $class->hasProperty('brand'),
            'StoreCore does not support “business person” as separate entity.'
        );

        $class = new ReflectionClass(\StoreCore\PIM\Product::class);
        $this->assertTrue($class->hasProperty('brand'));

        $class = new ReflectionClass(\StoreCore\PIM\Service::class);
        $this->assertTrue($class->hasProperty('brand'));
    }
}
