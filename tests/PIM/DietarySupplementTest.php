<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\PIM\DietarySupplement::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class DietarySupplementTest extends TestCase
{
    #[TestDox('DietarySupplement class is concrete')]
    public function testDietarySupplementClassIsConcrete()
    {
        $class = new ReflectionClass(DietarySupplement::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('DietarySupplement is a Product')]
    public function testDietarySupplementIsProduct()
    {
        $dietarySupplement = new DietarySupplement();
        $this->assertInstanceOf(Product::class, $dietarySupplement);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(DietarySupplement::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(DietarySupplement::VERSION);
        $this->assertIsString(DietarySupplement::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(DietarySupplement::VERSION, '0.1.0', '>=')
        );
    }
}
