<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\GlobalProductClassificationBrick::class)]
final class GlobalProductClassificationBrickTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('GlobalProductClassificationBrick class exists')]
    public function testGlobalProductClassificationBrickClasExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'GlobalProductClassificationBrick.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'GlobalProductClassificationBrick.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\GlobalProductClassificationBrick'));
        $this->assertTrue(class_exists(GlobalProductClassificationBrick::class));
    }


    #[Group('hmvc')]
    #[TestDox('GlobalProductClassificationBrick class is concrete')]
    public function testGlobalProductClassificationBrickClassIsConcrete()
    {
        $class = new ReflectionClass(GlobalProductClassificationBrick::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('GlobalProductClassificationBrick class is read-only')]
    public function testGlobalProductClassificationBrickClassIsReadOnly()
    {
        $class = new ReflectionClass(GlobalProductClassificationBrick::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(GlobalProductClassificationBrick::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(GlobalProductClassificationBrick::VERSION);
        $this->assertIsString(GlobalProductClassificationBrick::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(GlobalProductClassificationBrick::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('GlobalProductClassificationBrick::__construct exists')]
    public function testGlobalProductClassificationBrickConstructorExists()
    {
        $class = new ReflectionClass(GlobalProductClassificationBrick::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('GlobalProductClassificationBrick::__construct is public constructor')]
    public function testGlobalProductClassificationBrickConstructIsPublicConstructor()
    {
        $method = new ReflectionMethod(GlobalProductClassificationBrick::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('GlobalProductClassificationBrick::__construct has two parameters')]
    public function testGlobalProductClassificationBrickConstructorHasTwoParameters()
    {
        $method = new ReflectionMethod(GlobalProductClassificationBrick::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());

        $category = new GlobalProductClassificationBrick(
            '10002087',
            'Camping Stoves/Grills/Ovens',
        );

        $this->assertObjectHasProperty('identifier', $category);
        $this->assertEquals('10002087', $category->identifier);

        $this->assertObjectHasProperty('name', $category);
        $this->assertEquals('Camping Stoves/Grills/Ovens', $category->name);
    }

    #[TestDox('GlobalProductClassificationBrick::__construct has one REQUIRED parameter')]
    public function testGlobalProductClassificationBrickConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(GlobalProductClassificationBrick::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());

        $category = new GlobalProductClassificationBrick('10002087');
        $this->assertEquals('10002087', $category->identifier);
        $this->assertEmpty($category->name);
    }

    #[TestDox('GlobalProductClassificationBrick::__construct accepts integer but sets string')]
    public function testGlobalProductClassificationBrickConstructorAcceptsIntegerButSetsString()
    {
        $method = new ReflectionMethod(GlobalProductClassificationBrick::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());

        $category = new GlobalProductClassificationBrick(
            10002087,
            'Camping Stoves/Grills/Ovens',
        );
        $this->assertIsNotInt($category->identifier);
        $this->assertIsString($category->identifier);
    }
}
