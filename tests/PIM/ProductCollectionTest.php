<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\ProductCollection::class)]
#[CoversClass(\StoreCore\PIM\TypeAndQuantityNode::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ProductCollectionTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ProductCollection class exists')]
    public function testProductCollectionClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductCollection.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductCollection.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductCollection'));
        $this->assertTrue(class_exists(ProductCollection::class));
    }

    #[Group('hmvc')]
    #[TestDox('ProductCollection class is concrete')]
    public function testProductCollectionClassIsConcrete()
    {
        $class = new ReflectionClass(ProductCollection::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(ProductCollection::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(ProductCollection::VERSION);
        $this->assertIsString(ProductCollection::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(ProductCollection::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('ProductCollection.collectionSize exists')]
    public function testProductCollectionCollectionSizeExists()
    {
        $productCollection = new ProductCollection();
        $this->assertObjectHasProperty('collectionSize', $productCollection);
    }

    #[TestDox('ProductCollection is countable')]
    public function testProductCollectionIsCountable()
    {
        $collection = new ProductCollection();
        $this->assertInstanceOf(\Countable::class, $collection);
        $this->assertIsInt(count($collection));
        $this->assertEquals(0, $collection->count());
        $this->assertEquals(0, count($collection));
        $this->assertCount(0, $collection);

        $foo = new Product('Foo');
        $foo->setIdentifier('47fe9b75-78c5-4bcc-a726-7114d5915307');
        $foos = new TypeAndQuantityNode($foo, 3);

        $collection->attach($foos);
        $this->assertCount(3, $collection);
        $this->assertEquals(3, $collection->collectionSize);

        $bar = new Product('Bar');
        $bar->setIdentifier('74cca66d-bdab-4dbb-a414-9093df7f6462');
        $bars = new TypeAndQuantityNode($bar, 4);

        $collection->attach($bars);
        $this->assertCount(7, $collection);
        $this->assertEquals(7, $collection->collectionSize);
    }
}
