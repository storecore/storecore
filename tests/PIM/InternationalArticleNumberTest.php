<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\InternationalArticleNumber::class)]
final class InternationalArticleNumberTest extends TestCase
{
    protected ReflectionClass $reflectionClass;

    protected function setUp(): void
    {
        $this->reflectionClass = new ReflectionClass(InternationalArticleNumber::class);
    }


    #[Group('distro')]
    #[TestDox('InternationalArticleNumber class exists')]
    public function testInternationalArticleNumberClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'InternationalArticleNumber.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'InternationalArticleNumber.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\InternationalArticleNumber'));
        $this->assertTrue(class_exists(InternationalArticleNumber::class));
    }


    #[TestDox('InternationalArticleNumber extends Varchar')]
    public function testInternationalArticleNumberExtendsVarchar(): void
    {
        $ean = new InternationalArticleNumber('9789043017121');
        $this->assertInstanceOf(\StoreCore\Types\Varchar::class, $ean);
    }

    #[Group('hmvc')]
    #[TestDox('InternationalArticleNumber is stringable')]
    public function testInternationalArticleNumberIsStringable(): void
    {
        $ean = new InternationalArticleNumber('9789043017121');
        $this->assertInstanceOf(\Stringable::class, $ean);
    }

    #[Group('hmvc')]
    #[TestDox('InternationalArticleNumber implements TypeInterface')]
    public function testInternationalArticleNumberImplementsTypeInterface(): void
    {
        $ean = new InternationalArticleNumber('9789043017121');
        $this->assertInstanceOf(\StoreCore\Types\TypeInterface::class, $ean);
    }

    #[Group('hmvc')]
    #[TestDox('InternationalArticleNumber implements ValidateInterface')]
    public function testInternationalArticleNumberImplementsValidateInterface(): void
    {
        $ean = new InternationalArticleNumber('9789043017121');
        $this->assertInstanceOf(\StoreCore\Types\ValidateInterface::class, $ean);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $this->assertTrue($this->reflectionClass->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(InternationalArticleNumber::VERSION);
        $this->assertIsString(InternationalArticleNumber::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(InternationalArticleNumber::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('InternationalArticleNumber::__construct() removes hyphens, spaces, and dots')]
    public function testInternationalArticleNumberConstructorRemovesHyphensSpacesAndDots(): void
    {
        $ean = new InternationalArticleNumber('4-056489-100553');
        $this->assertSame('4056489100553', (string) $ean);

        $ean = new InternationalArticleNumber('4 056489 100553');
        $this->assertSame('4056489100553', (string) $ean);

        $ean = new InternationalArticleNumber('4.056489.100553');
        $this->assertSame('4056489100553', (string) $ean);
    }

    #[TestDox('InternationalArticleNumber::__construct() adds valid check digits')]
    public function testInternationalArticleNumberConstructorAddsValidCheckDigits(): void
    {
        // Article numbers and valid check digits
        $article_numbers = [
            '019019806709' => 8,
            '072439315613' => 6,
            '275006487359' => 8,
            '871210051638' => 2,
            '871210057043' => 8,
            '871480000115' => 1,
            '880608827983' => 1,
        ];
        foreach ($article_numbers as $article_number => $check_digit) {
            $ean = new InternationalArticleNumber($article_number);
            $this->assertSame($article_number . $check_digit, (string) $ean);
        }
    }

    #[TestDox('InternationalArticleNumber::__construct() generates random internal numbers on zero')]
    public function testInternationalArticleNumberConstructorGeneratesRandomInternalNumbersOnZero(): void
    {
        $ean = new InternationalArticleNumber(0, false);
        $ean = (string) $ean;
        $this->assertTrue(strlen($ean) === 13);
        $this->assertTrue(substr($ean, 0, 1) == '2');

        $ean = new InternationalArticleNumber('0', false);
        $ean = (string) $ean;
        $this->assertTrue(strlen($ean) === 13);
        $this->assertTrue(substr($ean, 0, 1) == '2');
    }


    #[TestDox('InternationalArticleNumber::__toString() exists')]
    public function testInternationalArticleNumberToStringExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('__toString'));
    }

    #[TestDox('InternationalArticleNumber::__toString() is public')]
    public function testInternationalArticleNumberToStringIsPublic(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('InternationalArticleNumber::__toString() returns non-empty string')]
    public function testInternationalArticleNumberToStringReturnsNonEmptyString(): void
    {
        $ean = new InternationalArticleNumber('0190198067098');
        $ean = (string) $ean;
        $this->assertNotEmpty($ean);
        $this->assertIsString($ean);
    }

    #[Depends('testInternationalArticleNumberToStringReturnsNonEmptyString')]
    #[TestDox('InternationalArticleNumber::__toString() returns numeric string')]
    public function testInternationalArticleNumberToStringReturnsNumericString(): void
    {
        $ean = new InternationalArticleNumber('0190198067098');
        $ean = (string) $ean;
        $this->assertTrue(is_numeric($ean));
        $this->assertTrue(ctype_digit($ean));
    }

    #[Depends('testInternationalArticleNumberToStringReturnsNonEmptyString')]
    #[Depends('testInternationalArticleNumberToStringReturnsNumericString')]
    #[TestDox('InternationalArticleNumber::__toString() returns 13 characters')]
    public function testInternationalArticleNumberToStringReturns13Characters(): void
    {
        $ean = new InternationalArticleNumber('0190198067098');
        $ean = (string) $ean;
        $this->assertEquals(13, strlen($ean));
    }


    #[TestDox('InternationalArticleNumber::calculateCheckDigit() exists')]
    public function testInternationalArticleNumberCalculateCheckDigitExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('calculateCheckDigit'));
    }

    #[Depends('testInternationalArticleNumberCalculateCheckDigitExists')]
    #[TestDox('InternationalArticleNumber::calculateCheckDigit() is public static')]
    public function testInternationalArticleNumberCalculateCheckDigitIsPublicStatic(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'calculateCheckDigit');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testInternationalArticleNumberCalculateCheckDigitExists')]
    #[TestDox('InternationalArticleNumber::calculateCheckDigit() has one REQUIRED parameter')]
    public function testInternationalArticleNumberCalculateCheckDigitHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'calculateCheckDigit');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testInternationalArticleNumberCalculateCheckDigitHasOneRequiredParameter')]
    #[TestDox('InternationalArticleNumber::calculateCheckDigit() returns integer')]
    public function testInternationalArticleNumberCalculateCheckDigitReturnsInteger(): void
    {
        $incomplete_ean_numbers = [
            '019019806709' => 8,
            '072439315613' => 6,
            '275006487359' => 8,
            '871210051638' => 2,
            '871210057043' => 8,
            '871480000115' => 1,
            '880608827983' => 1,
        ];
        foreach ($incomplete_ean_numbers as $number => $checksum) {
            $this->assertEquals(
                (int) $checksum,
                InternationalArticleNumber::calculateCheckDigit($number)
            );
        }
    }


    #[TestDox('InternationalArticleNumber::getNextNumber() exists')]
    public function testInternationalArticleNumberGetNextNumberExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('getNextNumber'));
    }

    #[TestDox('InternationalArticleNumber::getNextNumber() is public')]
    public function testInternationalArticleNumberGetNextNumberIsPublic(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'getNextNumber');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('InternationalArticleNumber::getNextNumber() has no parameters')]
    public function testInternationalArticleNumberGetNextNumberHasNoParameters(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'getNextNumber');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('InternationalArticleNumber::getNextNumber() returns new InternationalArticleNumber')]
    public function testInternationalArticleNumberGetNextNumberReturnsNewInternationalArticleNumber(): void
    {
        $ean = new InternationalArticleNumber('4006381333931');
        $this->assertIsObject($ean->getNextNumber());
        $this->assertInstanceOf(InternationalArticleNumber::class, $ean->getNextNumber());
        $this->assertNotSame($ean->getNextNumber(), $ean);
    }

    #[TestDox('InternationalArticleNumber::getNextNumber() returns current number + 1')]
    public function testInternationalArticleNumberGetNextNumberReturnsCurrentNumberPlusOne(): void
    {
        $current_number = '4006381333931';
        $current_ean = new InternationalArticleNumber($current_number);
        $next_ean = $current_ean->getNextNumber();
        $next_number = (string) $next_ean;
        $this->assertEquals($next_number, '4006381333948');
    }

    #[TestDox('InternationalArticleNumber::getNextNumber() throws \RangeException on 99999')]
    public function testInternationalArticleNumberGetNextNumberThrowsRangeExceptionOn99999(): void
    {
        $ean = new InternationalArticleNumber('123400099999', false);
        $this->expectException(\RangeException::class);
        $next_number = $ean->getNextNumber();
    }


    #[TestDox('InternationalArticleNumber::getRandomNumber() exists')]
    public function testInternationalArticleNumberGetRandomNumberExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('getRandomNumber'));
    }

    #[TestDox('InternationalArticleNumber::getRandomNumber() is public static')]
    public function testInternationalArticleNumberGetRandomNumberIsPublicStatic(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'getRandomNumber');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testInternationalArticleNumberGetRandomNumberIsPublicStatic')]
    #[TestDox('InternationalArticleNumber::getRandomNumber() returns InternationalArticleNumber object')]
    public function testInternationalArticleNumberGetRandomNumberReturnsInternationalArticleNumberObject(): void
    {
        $ean = InternationalArticleNumber::getRandomNumber();
        $this->assertIsObject($ean);
        $this->assertInstanceOf(\StoreCore\PIM\InternationalArticleNumber::class, $ean);
    }


    #[TestDox('InternationalArticleNumber::validate() exists')]
    public function testInternationalArticleNumberValidateExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('validate'));
    }

    #[TestDox('InternationalArticleNumber::validate() is public static')]
    public function testInternationalArticleNumberValidateIsPublicStatic(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'validate');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('InternationalArticleNumber::validate() has one REQUIRED parameter')]
    public function testInternationalArticleNumberValidateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(InternationalArticleNumber::class, 'validate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('InternationalArticleNumber::validate() returns true on valid numbers')]
    public function testInternationalArticleNumberValidateReturnsTrueOnValidNumbers(): void
    {
        $valid_ean_numbers = [
            '0190198067098',
            '0724393156136',
            '2750064873598',
            '8712100516382',
            '8712100570438',
            '8714800001151',
            '8806088279831',
        ];
        foreach ($valid_ean_numbers as $ean) {
            $this->assertTrue(InternationalArticleNumber::validate($ean));
        }
    }
}
