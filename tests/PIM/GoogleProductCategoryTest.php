<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\GoogleProductCategory::class)]
final class GoogleProductCategoryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('GoogleProductCategory class exists')]
    public function testGoogleProductCategoryClasExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'GoogleProductCategory.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'GoogleProductCategory.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\GoogleProductCategory'));
        $this->assertTrue(class_exists(GoogleProductCategory::class));
    }

    #[Group('hmvc')]
    #[TestDox('GoogleProductCategory class is concrete')]
    public function testGoogleProductCategoryClassIsConcrete()
    {
        $class = new ReflectionClass(GoogleProductCategory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('GoogleProductCategory class is read-only')]
    public function testGoogleProductCategoryClassIsReadOnly()
    {
        $class = new ReflectionClass(GoogleProductCategory::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(GoogleProductCategory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(GoogleProductCategory::VERSION);
        $this->assertIsString(GoogleProductCategory::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(GoogleProductCategory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('GoogleProductCategory::__construct() exists')]
    public function testGoogleProductCategoryConstructorExists()
    {
        $class = new ReflectionClass(GoogleProductCategory::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('GoogleProductCategory::__construct() is public constructor')]
    public function testGoogleProductCategoryConstructIsPublicConstructor()
    {
        $method = new ReflectionMethod(GoogleProductCategory::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('GoogleProductCategory::__construct() has three parameters')]
    public function testGoogleProductCategoryConstructorHasThreeParameters()
    {
        $method = new ReflectionMethod(GoogleProductCategory::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());

        $category = new GoogleProductCategory(
            2353,
            'en-US',
            'Electronics > Communications > Telephony > Mobile Phone Accessories > Mobile Phone Cases'
        );

        $this->assertObjectHasProperty('identifier', $category);
        $this->assertEquals(2353, $category->identifier);

        $this->assertObjectHasProperty('language', $category);
        $this->assertEquals('en-US', $category->language);

        $this->assertObjectHasProperty('name', $category);
        $this->assertEquals('Electronics > Communications > Telephony > Mobile Phone Accessories > Mobile Phone Cases', $category->name);
    }

    #[TestDox('GoogleProductCategory::__construct() has one REQUIRED parameter')]
    public function testGoogleProductCategoryConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(GoogleProductCategory::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
