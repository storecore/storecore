<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\AbstractService::class)]
#[Group('hmvc')]
final class AbstractServiceTest extends TestCase
{
    #[TestDox('AbstractService class is abstract')]
    public function testAbstractServiceClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractService::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractService extends AbstractProductOrService')]
    public function testAbstractServiceExtendsAbstractProductOrService(): void
    {
        $service = new class extends AbstractService {};
        $this->assertInstanceOf(AbstractProductOrService::class, $service);
    }

    #[TestDox('AbstractService implements StoreCore IdentityInterface')]
    public function testAbstractServiceImplementsStoreCoreIdentityInterface(): void
    {
        $service = new class extends AbstractService {};
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $service);
    }
}
