<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \json_encode;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\PIM\EnergyConsumptionDetails::class)]
#[CoversClass(\StoreCore\PIM\AbstractProduct::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
final class EnergyConsumptionDetailsTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('EnergyConsumptionDetails class is abstract')]
    public function testProductClassIsConcrete(): void
    {
        $class = new ReflectionClass(EnergyConsumptionDetails::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('EnergyConsumptionDetails is a Thing')]
    public function testEnergyConsumptionDetailsIsThing(): void
    {
        $this->assertInstanceOf(\StoreCore\Types\Thing::class, new EnergyConsumptionDetails());
    }

    #[Group('hmvc')]
    #[TestDox('EnergyConsumptionDetails is JSON serializable')]
    public function testEnergyConsumptionDetailsIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new EnergyConsumptionDetails());
    }


    #[Group('hmvc')]
    #[TestDox('Product.hasEnergyConsumptionDetails exists')]
    public function testProductHasEnergyConsumptionDetailsExists(): void
    {
        $this->assertObjectHasProperty('hasEnergyConsumptionDetails', new Product());
    }

    #[TestDox('Product.hasEnergyConsumptionDetails is null by default')]
    public function testProductHasEnergyConsumptionDetailsIsNullByDefault(): void
    {
        $product = new Product();
        $this->assertNull($product->hasEnergyConsumptionDetails);
    }

    #[Group('hmvc')]
    #[TestDox('Product.hasEnergyConsumptionDetails accepts EnergyConsumptionDetails')]
    public function testProductHasEnergyConsumptionDetailsAcceptsEnergyConsumptionDetails(): void
    {
        // Product with a B rating on a scale from D to A+++
        $product = new Product();
        $product->hasEnergyConsumptionDetails = new EnergyConsumptionDetails();
        $product->hasEnergyConsumptionDetails->energyEfficiencyScaleMin = EUEnergyEfficiencyEnumeration::EUEnergyEfficiencyCategoryD;
        $product->hasEnergyConsumptionDetails->energyEfficiencyScaleMax = EUEnergyEfficiencyEnumeration::EUEnergyEfficiencyCategoryA3Plus;
        $product->hasEnergyConsumptionDetails->hasEnergyEfficiencyCategory = EUEnergyEfficiencyEnumeration::EUEnergyEfficiencyCategoryB;

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Product",
              "hasEnergyConsumptionDetails": {
                "@type": "EnergyConsumptionDetails",
                "energyEfficiencyScaleMax": "https://schema.org/EUEnergyEfficiencyCategoryA3Plus",
                "energyEfficiencyScaleMin": "https://schema.org/EUEnergyEfficiencyCategoryD",
                "hasEnergyEfficiencyCategory": "https://schema.org/EUEnergyEfficiencyCategoryB"
              },
              "itemCondition": "https://schema.org/NewCondition"
            }
        ';

        $actualJson = json_encode($product, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
