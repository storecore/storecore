<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\Currency;
use StoreCore\CRM\Organization;
use StoreCore\Types\BusinessFunction;
use StoreCore\Types\Date;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \enum_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\Offer::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class OfferTest extends TestCase
{
    #[TestDox('Offer class is concrete')]
    public function testOfferClassIsConcrete(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Offer is Intangible')]
    public function testOfferIsIntangible(): void
    {
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, new Offer());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Offer::VERSION);
        $this->assertIsString(Offer::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Offer::VERSION, '0.4.0', '>=')
        );
    }


    #[TestDox('Offer.businessFunction exists')]
    public function testOfferBusinessFunctionExists(): void
    {
        $offer = new Offer();
        $this->assertObjectHasProperty('businessFunction', $offer);
    }

    #[TestDox('Offer.businessFunction is not null')]
    public function testOfferBusinessFunctionIsNotNull(): void
    {
        $property = new ReflectionProperty(Offer::class, 'businessFunction');
        $this->assertTrue($property->isPublic());

        $offer = new Offer();
        $this->assertNotNull($offer->businessFunction);
    }

    #[TestDox('BusinessFunction enumeration exists')]
    public function testBusinessFunctionEnumerationExists(): void
    {
        $this->assertTrue(enum_exists('\\StoreCore\\Types\\BusinessFunction'));
        $this->assertTrue(enum_exists(BusinessFunction::class));
    }

    #[Depends('testBusinessFunctionEnumerationExists')]
    #[TestDox('Offer.businessFunction is BusinessFunction enumeration member')]
    public function testOfferBusinessFunctionIsBusinessFunctionEnumerationMember(): void
    {
        $offer = new Offer();
        $this->assertInStanceOf(BusinessFunction::class, $offer->businessFunction);
    }

    #[Depends('testOfferBusinessFunctionIsBusinessFunctionEnumerationMember')]
    #[TestDox('Offer.businessFunction is GoodRelations Sell URL by default')]
    public function testOfferBusinessFunctionIsGoodRelationsSellUrlByDefault(): void
    {
        $offer = new Offer();
        $this->assertEquals('http://purl.org/goodrelations/v1#Sell', $offer->businessFunction->value);
    }


    #[TestDox('Offer::__construct exists')]
    public function testOfferConstructorExists(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Offer::__construct is public constructor')]
    public function testOfferConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Offer::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Offer::__construct has two OPTIONAL parameters')]
    public function testOfferConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(Offer::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Offer::__construct sets price and priceCurrency properties')]
    public function testOfferConstructorSetsPriceAndPriceCurrencyProperties(): void
    {
        $offer = new Offer(19.95, 'EUR');
        $this->assertEquals(19.95, $offer->price);
        $this->assertSame('EUR', $offer->priceCurrency);

        $offer = new Offer(23, 'USD');
        $this->assertEquals(23, $offer->price);
        $this->assertSame('USD', $offer->priceCurrency);
    }

    #[TestDox('Offer.priceCurrency is EUR by default')]
    public function testOfferPriceCurrencyIsEURByDefault(): void
    {
        $offer = new Offer(19.95);
        $this->assertNotNull($offer->priceCurrency);
        $this->assertIsString($offer->priceCurrency);
        $this->assertSame('EUR', $offer->priceCurrency);
    }


    #[TestDox('Offer::setAvailability() exists')]
    public function testOfferSetAvailabilityExists(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasMethod('setAvailability'));
    }

    #[TestDox('Offer::setAvailability() is public')]
    public function testOfferSetAvailabilityIsPublic(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setAvailability');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Offer::setAvailability() has one REQUIRED parameter')]
    public function testOfferSetAvailabilityHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setAvailability');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Offer.availability is https://schema.org/InStock by default')]
    public function testOfferAvailabilityIsHttpsSchemaOrgInStockByDefault(): void
    {
        $offer = new Offer();
        $this->assertSame(ItemAvailability::InStock, $offer->availability);
        $this->assertSame('https://schema.org/InStock', $offer->availability->value);
    }

    #[TestDox('Offer::setAvailability() sets Offer.availability')]
    public function testDefaultOfferSetAvailabilitySetsOfferAvailability(): void
    {
        $offer = new Offer();
        $this->assertNotSame('https://schema.org/OutOfStock', $offer->availability->value);

        // Set property as string:
        $offer->setAvailability('https://schema.org/OutOfStock');
        $this->assertSame(ItemAvailability::OutOfStock, $offer->availability);
        $this->assertSame('https://schema.org/OutOfStock', $offer->availability->value);

        // Set property as enumeration member:
        $offer->setAvailability(ItemAvailability::SoldOut);
        $this->assertSame(ItemAvailability::SoldOut, $offer->availability);
        $this->assertSame('https://schema.org/SoldOut', $offer->availability->value);
    }

    #[TestDox('Offer::setAvailability() sets Offer.availability')]
    public function testOfferSetAvailabilityAcceptsStoreCoreTypesItemAvailability(): void
    {
        $offer = new Offer();
        $this->assertNotEquals('https://schema.org/OutOfStock', $offer->availability->value);

        $offer->setAvailability(ItemAvailability::OutOfStock);
        $this->assertEquals('https://schema.org/OutOfStock', $offer->availability->value);
    }


    #[TestDox('Offer::setItemCondition() exists')]
    public function testOfferSetItemConditionExists(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasMethod('setItemCondition'));
    }

    #[TestDox('Offer::setItemCondition() is public')]
    public function testOfferSetItemConditionIsPublic(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setItemCondition');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Offer::setItemCondition() has one REQUIRED parameter')]
    public function testOfferSetItemConditionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setItemCondition');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Offer.itemCondition is https://schema.org/NewCondition by default')]
    public function testOfferItemConditionIsHttpsSchemaOrgNewConditionByDefault(): void
    {
        $offer = new Offer();
        $this->assertSame('https://schema.org/NewCondition', $offer->itemCondition->value);
    }

    #[TestDox('Offer::setItemCondition() sets itemCondition property')]
    public function testDefaultOfferSetItemConditionSetsItemConditionProperty(): void
    {
        $offer = new Offer();
        $this->assertNotSame('https://schema.org/RefurbishedCondition', $offer->itemCondition->value);

        $offer->setItemCondition('https://schema.org/RefurbishedCondition');
        $this->assertSame('https://schema.org/RefurbishedCondition', $offer->itemCondition->value);
    }


    #[TestDox('Offer::setPrice() exists')]
    public function testOfferSetPriceExists(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasMethod('setPrice'));
    }

    #[TestDox('Offer::setPrice() is public')]
    public function testOfferSetPriceIsPublic(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setPrice');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Offer::setPrice() has one REQUIRED parameter')]
    public function testOfferSetPriceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setPrice');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Offer::setPrice() sets price property')]
    public function testOfferSetPriceSetsPriceProperty(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->price);
        $offer->setPrice(39.99);
        $this->assertNotNull($offer->price);
    }

    #[TestDox('Offer::setPrice() sets price as float')]
    public function testOfferSetPriceSetsPriceAsFloat(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->price);
        $offer->setPrice(39.99);
        $this->assertIsFloat($offer->price);
        $this->assertEquals(39.99, $offer->price);
    }

    #[TestDox('Offer::setPrice() sets price as int')]
    public function testOfferSetPriceSetsPriceAsInt(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->price);
        $offer->setPrice(239);
        $this->assertIsInt($offer->price);
        $this->assertEquals(239, $offer->price);
    }

    #[TestDox('Offer::setPrice() accepts price as numeric string')]
    public function testOfferSetPriceAcceptsPriceAsNumericString(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->price);

        $offer->setPrice('39.99');
        $this->assertIsFloat($offer->price);
        $this->assertEquals(39.99, $offer->price);

        $offer->setPrice('239');
        $this->assertIsInt($offer->price);
        $this->assertEquals(239, $offer->price);
    }


    #[TestDox('Offer::setPriceCurrency() exists')]
    public function testOfferSetPriceCurrencyExists(): void
    {
        $class = new ReflectionClass(Offer::class);
        $this->assertTrue($class->hasMethod('setPriceCurrency'));
    }

    #[TestDox('Offer::setPriceCurrency() is public')]
    public function testOfferSetPriceCurrencyIsPublic(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setPriceCurrency');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Offer::setPriceCurrency() has one REQUIRED parameter')]
    public function testOfferSetPriceCurrencyHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Offer::class, 'setPriceCurrency');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Offer::setPriceCurrency() sets Offer.priceCurrency as string')]
    public function testOfferSetPriceCurrencySetsOfferPriceCurrencyAsString(): void
    {
        $offer = new Offer();
        $this->assertEquals('EUR', $offer->priceCurrency);

        $offer->setPriceCurrency('USD');
        $this->assertEquals('USD', $offer->priceCurrency);

        $this->expectException(\ValueError::class);
        $offer->setPriceCurrency('SFr.');
    }

    #[Depends('testOfferSetPriceCurrencySetsOfferPriceCurrencyAsString')]
    #[TestDox('Offer::setPriceCurrency() accepts Offer.priceCurrency as Currency value object')]
    public function testOfferSetPriceCurrencyAcceptsOfferPriceCurrencyAsCurrencyValueObject(): void
    {
        $dollar = new Currency(840, 'USD', 2, '$');
        $offer = new Offer();
        $offer->setPriceCurrency($dollar);

        $this->assertNotEquals('EUR', $offer->priceCurrency);
        $this->assertEquals('USD', $offer->priceCurrency);
    }


    #[TestDox('Offer.priceValidUntil exists')]
    public function testOfferPriceValidUntilExists(): void
    {
        $offer = new Offer();
        $this->assertObjectHasProperty('priceValidUntil', $offer);
    }

    #[TestDox('Offer.priceValidUntil is null by default')]
    public function testOfferPriceValidUntilIsNullByDefault(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->priceValidUntil);
    }

    #[TestDox('Offer.priceValidUntil is a Date value object')]
    public function testOfferPriceValidUntilIsADateValueObject(): void
    {
        $offer = new Offer();
        $date = Date::createFromFormat('Y-m-d', '2022-12-31', new \DateTimeZone('UTC'));
        $offer->priceValidUntil = $date;

        $this->assertNotNull($offer->priceValidUntil);
        $this->assertIsObject($offer->priceValidUntil);
        $this->assertSame($date, $offer->priceValidUntil);
    }


    #[TestDox('Offer.seller exists')]
    public function testOfferSellerExists(): void
    {
        $this->assertObjectHasProperty('seller', new Offer());
    }

    #[Depends('testOfferSellerExists')]
    #[TestDox('Offer.seller is null by default')]
    public function testOfferSellerIsNullByDefault(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->seller);
    }

    #[Depends('testOfferSellerIsNullByDefault')]
    #[TestDox('Offer.seller accepts Organization')]
    public function testOfferSellerAcceptsOrganization(): void
    {
        $seller = new Organization('Acme Corporation');
        $offer = new Offer();
        $offer->seller = $seller;

        $this->assertNotNull($offer->seller);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $offer->seller);
        $this->assertEquals('Acme Corporation', $offer->seller->name);
    }

    #[Depends('testOfferSellerIsNullByDefault')]
    #[TestDox('Offer.seller supersedes merchant and vendor')]
    public function testOfferSellerSupersedesMerchantAndVendor(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->merchant);
        $this->assertNull($offer->vendor);

        $offer->seller = new Organization('Acme Corporation');
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $offer->seller);
        $this->assertEquals('Acme Corporation', $offer->seller->name);

        $this->assertSame($offer->merchant, $offer->seller);
        $this->assertSame($offer->vendor, $offer->seller);
    }


    #[TestDox('Offer.url exists')]
    public function testOfferUrlExists(): void
    {
        $this->assertObjectHasProperty('url', new Offer());
    }

    #[TestDox('Offer.url is null by default')]
    public function testOfferUrlIsNullByDefault(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->url);
    }

    /**
     * @see https://schema.org/Offer#eg-0011 Schema.org Offer JSON-LD Example 2
     */
    #[TestDox('Offer.url is URL value object')]
    public function testOfferUrlIsURLValueObject(): void
    {
        $offer = new Offer();
        $offer->url = new URL('https://save-a-lot-monitors.com/dell-30.html');
        $this->assertNotNull($offer->url);
        $this->assertIsObject($offer->url);
        $this->assertInstanceOf(URL::class, $offer->url);

        $offer = new Offer();
        $offer->url = new URL('http://jondoe-gadgets.com/dell-30.html');
        $this->assertNotNull($offer->url);
        $this->assertIsObject($offer->url);
        $this->assertInstanceOf(URL::class, $offer->url);
    }


    /**
     * @see https://schema.org/Offer#eg-0010 Schema.org Offer JSON-LD Example 1
     */
    #[TestDox('Offer is JSON serializable Example 1')]
    public function testOfferIsJsonSerializableExample1(): void
    {
        $offer = new Offer();
        $offer->price = '55.00';
        $offer->priceCurrency = 'USD';
        $json = json_encode($offer, \JSON_UNESCAPED_SLASHES);

        $this->assertNotEmpty($json);
        $this->assertIsString($json);
        $this->assertStringContainsString('"@type":"Offer"', $json);
        $this->assertStringContainsString('"availability":"https://schema.org/InStock"', $json);
        $this->assertStringContainsString('"price":55', $json);
        $this->assertStringContainsString('"priceCurrency":"USD"', $json);
    }

    /**
     * @see https://schema.org/Offer#eg-0012 Schema.org Offer JSON-LD Example 3
     */
    #[TestDox('Offer is JSON serializable Example 3')]
    public function testOfferIsJsonSerializableExample3(): void
    {
        $offer = new Offer(13.00, 'USD');
        $offer->url = new URL('http://www.ticketfly.com/purchase/309433');
        $json = json_encode($offer, \JSON_UNESCAPED_SLASHES);

        $this->assertNotEmpty($json);
        $this->assertIsString($json);
        $this->assertStringContainsString('"@type":"Offer"', $json);
        $this->assertStringContainsString('"price":13', $json);
        $this->assertStringContainsString('"priceCurrency":"USD"', $json);
        $this->assertStringContainsString('"url":"http://www.ticketfly.com/purchase/309433"', $json);
    }

    /**
     * @see https://schema.org/Offer#eg-0173 Schema.org Offer JSON-LD Example 6
     */
    #[TestDox('Offer is JSON serializable Example 6')]
    public function testOfferIsJsonSerializableExample6(): void
    {
        $offer = new Offer(13.00, 'USD');
        $offer->availability = ItemAvailability::SoldOut;
        $offer->url = new URL('http://www.ticketfly.com/purchase/309433');
        $json = json_encode($offer, \JSON_UNESCAPED_SLASHES);

        $this->assertNotEmpty($json);
        $this->assertIsString($json);
        $this->assertStringContainsString('"@type":"Offer"', $json);
        $this->assertStringContainsString('"availability":"https://schema.org/SoldOut"', $json);
        $this->assertStringContainsString('"price":13', $json);
        $this->assertStringContainsString('"priceCurrency":"USD"', $json);
        $this->assertStringContainsString('"url":"http://www.ticketfly.com/purchase/309433"', $json);
    }
}
