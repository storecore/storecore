<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\InternationalStandardBookNumber::class)]
#[CoversClass(\StoreCore\PIM\InternationalArticleNumber::class)]
final class InternationalStandardBookNumberTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('InternationalStandardBookNumber class exists')]
    public function testInternationalStandardBookNumberClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'InternationalStandardBookNumber.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'InternationalStandardBookNumber.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\InternationalStandardBookNumber'));
        $this->assertTrue(class_exists(InternationalStandardBookNumber::class));
    }

    #[Group('hmvc')]
    #[TestDox('InternationalStandardBookNumber is an International Article Number')]
    public function testIsbnIsInternationalArticleNumber(): void
    {
        $isbn = new InternationalStandardBookNumber('9781449373320');
        $this->assertInstanceOf(\StoreCore\PIM\InternationalArticleNumber::class, $isbn);
    }

    #[Group('hmvc')]
    #[TestDox('InternationalStandardBookNumber is stringable')]
    public function testIsbnIsStringable(): void
    {
        $isbn = 9781449373320;
        $this->assertIsInt($isbn);

        $isbn = new InternationalStandardBookNumber($isbn);
        $this->assertInstanceOf(\Stringable::class, $isbn);

        $this->assertNotEmpty((string) $isbn);
        $this->assertIsNotInt((string) $isbn);
        $this->assertIsString((string) $isbn);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(InternationalStandardBookNumber::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(InternationalStandardBookNumber::VERSION);
        $this->assertIsString(InternationalStandardBookNumber::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(InternationalStandardBookNumber::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('InternationalStandardBookNumber::__construct() accepts ISBN written as float')]
    public function testInternationalStandardBookNumberConstructAcceptsIsbnWittenAsFloat(): void
    {
        $isbn = 9781788.162067;
        $this->assertIsFloat($isbn);
        $this->assertTrue(InternationalStandardBookNumber::validate($isbn));

        $this->expectException(\TypeError::class);
        $isbn = new InternationalStandardBookNumber(false);
    }

    #[Group('hmvc')]
    #[TestDox('InternationalStandardBookNumber::__construct() converts ISBN-10 to ISBN-13 and EAN-13')]
    public function testIsbnConstructorConvertsIsbn10ToIsbn13AndEan13(): void
    {
        // Converts InternationalStandardBookNumber-10 as integer to InternationalStandardBookNumber-13 as string.
        $isbn = new InternationalStandardBookNumber(1449373321);
        $this->assertNotEmpty((string) $isbn);
        $this->assertIsString((string) $isbn);
        $this->assertTrue(strlen((string) $isbn) === 13);
        $this->assertSame('9781449373320', (string) $isbn);

        // Maps ISBN-10 to EAN-13.
        $isbn2ean = [
            '0578973839' => '9780578973838',
            '1098106474' => '9781098106478',
            '1491958707' => '9781491958704',
            '1492088781' => '9781492088783',
            '1617292230' => '9781617292231',
        ];
        foreach ($isbn2ean as $isbn => $ean) {
            $isbn = new InternationalStandardBookNumber($isbn);
            $ean = new InternationalArticleNumber($ean);
            $this->assertEquals((string) $ean, (string) $isbn);
        }
    }

    /**
     * An ISBN is an EAN, but an EAN is not always an ISBN,
     * so a valid EAN MAY be an invalid ISBN.
     */
    #[Group('hmvc')]
    #[TestDox('A valid EAN MAY be an invalid ISBN')]
    public function testValidEanMayBeInvalidIsbn(): void
    {
        $ean = 8712100516382;
        $this->assertTrue(InternationalArticleNumber::validate($ean));

        $ean = new InternationalArticleNumber($ean, true);
        $this->assertIsObject($ean);
        $this->assertEquals('8712100516382', (string) $ean);

        $this->expectException(\ValueError::class);
        $failure = new InternationalStandardBookNumber((string) $ean);
    }
}
