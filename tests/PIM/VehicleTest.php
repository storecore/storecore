<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\Vehicle::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class VehicleTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Vehicle class exists')]
    public function testVehicleClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Vehicle.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'Vehicle.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\Vehicle'));
        $this->assertTrue(class_exists(Vehicle::class));
    }

    #[Group('hmvc')]
    #[TestDox('Vehicle class is concrete')]
    public function testVehicleClassIsConcrete(): void
    {
        $class = new ReflectionClass(Vehicle::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Vehicle is a Product')]
    public function testVehicleIsProduct(): void
    {
        $this->assertInstanceOf(Product::class, new Vehicle());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Vehicle::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Vehicle::VERSION);
        $this->assertIsString(Vehicle::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Vehicle::VERSION, '0.1.0', '>=')
        );
    }
}
