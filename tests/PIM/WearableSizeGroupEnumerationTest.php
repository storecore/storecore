<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionEnum, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\WearableSizeGroupEnumeration::class)]
final class WearableSizeGroupEnumerationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('WearableSizeGroupEnumeration enumeration exists')]
    public function testWearableSizeGroupEnumerationEnumerationExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'WearableSizeGroupEnumeration.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'WearableSizeGroupEnumeration.php');
        $this->assertTrue(enum_exists('\\StoreCore\\PIM\\WearableSizeGroupEnumeration'));
        $this->assertTrue(enum_exists(WearableSizeGroupEnumeration::class));
    }

    #[TestDox('WearableSizeGroupEnumeration is an enumeration')]
    public function testWearableSizeGroupEnumerationIsEnumeration(): void
    {
        $class = new ReflectionClass(WearableSizeGroupEnumeration::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testWearableSizeGroupEnumerationIsEnumeration')]
    #[TestDox('WearableSizeGroupEnumeration is a backed enumeration')]
    public function testWearableSizeGroupEnumerationIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(WearableSizeGroupEnumeration::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $enum = new ReflectionEnum(WearableSizeGroupEnumeration::class);
        $this->assertTrue($enum->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(WearableSizeGroupEnumeration::VERSION);
        $this->assertIsString(WearableSizeGroupEnumeration::VERSION);
    }

    /**
     * @see https://schema.org/docs/releases.html
     */
    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches latest Schema.org release')]
    public function testVersionMatchesLatestSchemaOrgRelease(): void
    {
        $this->assertTrue(
            version_compare(WearableSizeGroupEnumeration::VERSION, '23.0', '>=')
        );
    }


    #[Depends('testWearableSizeGroupEnumerationIsEnumeration')]
    #[TestDox('WearableSizeGroupEnumeration has 17 enumeration members')]
    public function testWearableSizeGroupEnumerationHas17EnumerationMember(): void
    {
        $this->assertEquals(17, count(WearableSizeGroupEnumeration::cases()));
    }


    #[TestDox('WearableSizeGroupEnumeration::fromInteger() exists')]
    public function testWearableSizeGroupEnumerationFromIntegerExists(): void
    {
        $enum = new ReflectionEnum(WearableSizeGroupEnumeration::class);
        $this->assertTrue($enum->hasMethod('fromInteger'));
    }

    #[Depends('testWearableSizeGroupEnumerationFromIntegerExists')]
    #[TestDox('WearableSizeGroupEnumeration::fromInteger() is final public static')]
    public function testWearableSizeGroupEnumerationFromIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(WearableSizeGroupEnumeration::class, 'fromInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testWearableSizeGroupEnumerationFromIntegerExists')]
    #[TestDox('WearableSizeGroupEnumeration::fromInteger() has one REQUIRED parameter')]
    public function testWearableSizeGroupEnumerationFromIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(WearableSizeGroupEnumeration::class, 'fromInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testWearableSizeGroupEnumerationFromIntegerIsFinalPublicStatic')]
    #[Depends('testWearableSizeGroupEnumerationFromIntegerHasOneRequiredParameter')]
    #[TestDox('WearableSizeGroupEnumeration::fromInteger() accepts integers from 0 through 16')]
    public function testWearableSizeGroupEnumerationFromIntegerAcceptsIntegersFrom0Through16(): void
    {
        for ($int = 0; $int <= 16; $int++) {
            $case = WearableSizeGroupEnumeration::fromInteger($int);
            $this->assertStringStartsWith('https://schema.org/WearableSizeGroup', $case->value);
        }

        $this->expectException(\ValueError::class);
        $failure = WearableSizeGroupEnumeration::fromInteger($int + 1);
    }

    #[Depends('testWearableSizeGroupEnumerationFromIntegerAcceptsIntegersFrom0Through16')]
    #[TestDox('WearableSizeGroupEnumeration::fromInteger() accepts (int) 0 for Regular')]
    public function testWearableSizeGroupEnumerationFromIntegerAcceptsInt0ForRegular(): void
    {
        $case = WearableSizeGroupEnumeration::fromInteger(0);
        $this->assertEquals('Regular', $case->name);
        $this->assertEquals('https://schema.org/WearableSizeGroupRegular', $case->value);
    }


    #[TestDox('WearableSizeGroupEnumeration::toInteger() exists')]
    public function testWearableSizeGroupEnumerationToIntegerExists(): void
    {
        $enum = new ReflectionEnum(WearableSizeGroupEnumeration::class);
        $this->assertTrue($enum->hasMethod('toInteger'));
    }

    #[Depends('testWearableSizeGroupEnumerationToIntegerExists')]
    #[TestDox('WearableSizeGroupEnumeration::toInteger() is final public static')]
    public function testWearableSizeGroupEnumerationToIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(WearableSizeGroupEnumeration::class, 'toInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testWearableSizeGroupEnumerationToIntegerExists')]
    #[TestDox('WearableSizeGroupEnumeration::toInteger() has one REQUIRED parameter')]
    public function testWearableSizeGroupEnumerationToIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(WearableSizeGroupEnumeration::class, 'toInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testWearableSizeGroupEnumerationToIntegerIsFinalPublicStatic')]
    #[Depends('testWearableSizeGroupEnumerationToIntegerHasOneRequiredParameter')]
    #[TestDox('WearableSizeGroupEnumeration::toInteger() maps all WearableSizeGroupEnumeration cases to integers')]
    public function testWearableSizeGroupEnumerationToIntegerMapsAllWearableSizeGroupEnumerationCasesToIntegers(): void
    {
        for ($int = 0; $int <= 16; $int++) {
            $case = WearableSizeGroupEnumeration::fromInteger($int);
            $this->assertStringEndsWith($case->name, $case->value);
            $this->assertEquals($int, WearableSizeGroupEnumeration::toInteger($case));
        }
    }
}
