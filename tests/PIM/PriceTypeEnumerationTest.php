<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class PriceTypeEnumerationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PriceTypeEnumeration enumeration exists')]
    public function testPriceTypeEnumerationClassFileIsReadable()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PriceTypeEnumeration.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PriceTypeEnumeration.php');
        $this->assertTrue(enum_exists(\StoreCore\PIM\PriceTypeEnumeration::class));
        $this->assertTrue(enum_exists(PriceTypeEnumeration::class));
    }

    #[Group('hmvc')]
    #[TestDox('PriceTypeEnumeration is an enumeration')]
    public function testUnitPriceSpecificatioIsEnumeration(): void
    {
        $class = new ReflectionClass(PriceTypeEnumeration::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testUnitPriceSpecificatioIsEnumeration')]
    #[Group('hmvc')]
    #[TestDox('PriceTypeEnumeration is a backed enumeration')]
    public function testUnitPriceSpecificatioIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(PriceTypeEnumeration::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PriceTypeEnumeration::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PriceTypeEnumeration::VERSION);
        $this->assertIsString(PriceTypeEnumeration::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PriceTypeEnumeration::VERSION, '0.2.0', '>=')
        );
    }


    /**
     * @see https://schema.org/PriceTypeEnumeration#subtypes
     *      Schema.org enumeration type `PriceTypeEnumeration` enumeration members
     */
    #[TestDox('PriceTypeEnumeration has six (6) enumeration members')]
    public function testPriceTypeEnumerationHasSixEnumerationMembers(): void
    {
        $this->assertCount(6, PriceTypeEnumeration::cases());

        $this->assertEquals('https://schema.org/InvoicePrice', PriceTypeEnumeration::InvoicePrice->value);
        $this->assertEquals('https://schema.org/ListPrice', PriceTypeEnumeration::ListPrice->value);
        $this->assertEquals('https://schema.org/MinimumAdvertisedPrice', PriceTypeEnumeration::MinimumAdvertisedPrice->value);
        $this->assertEquals('https://schema.org/MSRP', PriceTypeEnumeration::MSRP->value);
        $this->assertEquals('https://schema.org/SalePrice', PriceTypeEnumeration::SalePrice->value);
        $this->assertEquals('https://schema.org/SRP', PriceTypeEnumeration::SRP->value);
    }
}
