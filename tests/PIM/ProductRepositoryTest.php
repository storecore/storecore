<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\ProductRepository::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\IdentityMap::class)]
#[UsesClass(\StoreCore\Database\ProductMapper::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ProductRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->beginTransaction();
        }
    }

    protected function tearDown(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->rollback();
        }
    }


    #[Group('distro')]
    #[TestDox('ProductRepository class exists')]
    public function testProductRepositoryClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductRepository.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductRepository.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductRepository'));
        $this->assertTrue(class_exists(ProductRepository::class));
    }

    #[Group('hmvc')]
    #[TestDox('ProductRepository class is concrete')]
    public function testProductRepositoryClassIsConcrete()
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ProductRepository is a database model')]
    public function testProductRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ProductRepository implements PSR-11 ContainerInterface')]
    public function testProductRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('ProductRepository is Countable')]
    public function testProductRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(ProductRepository::VERSION);
        $this->assertIsString(ProductRepository::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(ProductRepository::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('ProductRepository::count() exists')]
    public function testProductRepositoryCountExists()
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('ProductRepository::count() is public')]
    public function testProductRepositoryCountIsPublic()
    {
        $method = new ReflectionMethod(ProductRepository::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('ProductRepository::count() has no parameters')]
    public function testProductRepositoryCountHasNoParameters()
    {
        $method = new ReflectionMethod(ProductRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('ProductRepository::count() returns zero or positive integer')]
    public function testProductRepositoryCountReturnsZeroOrPositiveInteger()
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $repository = new ProductRepository($registry);
            $this->assertIsInt($repository->count());
            $this->assertTrue($repository->count() >= 0);
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[TestDox('ProductRepository::has() exists')]
    public function testProductRepositoryHasExists()
    {
        $class = new ReflectionClass(ProductRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('ProductRepository::has() is public')]
    public function testProductRepositoryHasIsPublic()
    {
        $method = new ReflectionMethod(ProductRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('ProductRepository::has() has one REQUIRED parameter')]
    public function testProductRepositoryHasHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(ProductRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('ProductRepository::has() always returns false on `(string) 0`')]
    public function testProductRepositoryHasAlwaysReturnsFalseOnStringZero()
    {
        $registry = Registry::getInstance();
        $repository = new ProductRepository($registry);
        $this->assertIsBool($repository->has('0'));
        $this->assertFalse($repository->has('0'));

        $this->expectException(\TypeError::class);
        $failure = $repository->has(0);
    }
}
