<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\SizeSystem::class)]
final class SizeSystemTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SizeSystem class exists')]
    public function testSizeSystemClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SizeSystem.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SizeSystem.php');
        $this->assertTrue(enum_exists('\\StoreCore\\PIM\\SizeSystem'));
        $this->assertTrue(enum_exists(SizeSystem::class));
    }

    #[Group('hmvc')]
    #[TestDox('SizeSystem is an enumeration')]
    public function testSizeSystemIsEnumeration(): void
    {
        $class = new ReflectionClass(SizeSystem::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testSizeSystemIsEnumeration')]
    #[Group('hmvc')]
    #[TestDox('SizeSystem is a backed enumeration')]
    public function testSizeSystemIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(SizeSystem::class);
        $this->assertTrue($enum->isBacked());
    }
}
