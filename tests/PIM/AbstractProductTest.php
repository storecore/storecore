<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\AbstractProduct::class)]
#[Group('hmvc')]
final class AbstractProductTest extends TestCase
{
    #[TestDox('AbstractProduct class is abstract')]
    public function testAbstractProductClassIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractProduct::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractProduct extends AbstractProductOrService')]
    public function testAbstractProductExtendsAbstractProductOrService(): void
    {
        $class = new ReflectionClass(AbstractProduct::class);
        $this->assertTrue($class->isSubclassOf(AbstractProductOrService::class));
    }

    #[TestDox('AbstractProduct implements StoreCore IdentityInterface')]
    public function testAbstractProductImplementsStoreCoreIdentityInterface(): void
    {
        $product = new class extends AbstractProduct {};
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $product);

        $class = new ReflectionClass(AbstractProduct::class);
        $this->assertTrue($class->hasMethod('hasIdentifier'));
        $this->assertTrue($class->hasMethod('getIdentifier'));
        $this->assertTrue($class->hasMethod('setIdentifier'));
        $this->assertTrue($class->implementsInterface(\StoreCore\IdentityInterface::class));
    }
}
