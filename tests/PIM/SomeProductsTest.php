<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\SomeProducts::class)]
final class SomeProductsTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SomeProducts class exists')]
    public function testSomeProductsClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SomeProducts.php'        );
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SomeProducts.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\SomeProducts'));
        $this->assertTrue(class_exists(SomeProducts::class));
    }
}
