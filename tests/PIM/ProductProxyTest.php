<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\IndentityInterface;
use StoreCore\{ProxyInterface, ProxyFactory, Proxy};
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\PIM\ProductProxy::class)]
#[CoversClass(\StoreCore\Proxy::class)]
#[CoversClass(\StoreCore\ProxyFactory::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ProductProxyTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ProductProxy class exists')]
    public function testProductProxyClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductProxy.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductProxy.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductProxy'));
        $this->assertTrue(class_exists(ProductProxy::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(ProductProxy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(ProductProxy::VERSION);
        $this->assertIsString(ProductProxy::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(ProductProxy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('ProductProxy implements IdentityInterface')]
    public function testProductProxyImplementsIdentityInterface()
    {
        $product = new Product('Dell UltraSharp 30″ LCD Monitor');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $product->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromProduct($product);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
        $this->assertTrue($proxy->hasIdentifier());
        $this->assertNotEmpty($proxy->getIdentifier());
    }

    #[Group('hmvc')]
    #[TestDox('Implemented ProxyInterface exists')]
    public function testImplementedProxyInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\ProxyInterface'));
        $this->assertTrue(interface_exists(\StoreCore\ProxyInterface::class));
        $this->assertTrue(interface_exists(ProxyInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('ProductProxy implements ProxyInterface')]
    public function testProductProxyImplementsProxyInterface()
    {
        $product = new Product('Dell UltraSharp 30″ LCD Monitor');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $product->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromProduct($product);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('ProductProxy is JSON serializable')]
    public function testProductProxyIsJsonSerializable()
    {
        $this->assertTrue(interface_exists(\JsonSerializable::class));

        $product = new Product('Dell UltraSharp 30″ LCD Monitor');
        $product->setIdentifier('359a28e2-7cbd-4f58-95ce-e77e03015507');

        $proxy = ProxyFactory::createFromProduct($product);
        $this->assertInstanceOf(\JsonSerializable::class, $proxy);

        $expectedJson = '
            {
              "@id": "/api/v1/products/359a28e2-7cbd-4f58-95ce-e77e03015507",
              "@context": "https://schema.org",
              "@type": "Product",
              "identifier": "359a28e2-7cbd-4f58-95ce-e77e03015507",
              "name": "Dell UltraSharp 30″ LCD Monitor"
            }
        ';
        $actualJson = json_encode($proxy, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('ProductProxy::__construct() exists')]
    public function testProductProxyConstructorExists()
    {
        $class = new ReflectionClass(ProductProxy::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('ProductProxy::__construct() is public constructor')]
    public function testProductProxyConstructIsPublicConstructor()
    {
        $method = new ReflectionMethod(ProductProxy::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('ProductProxy::__construct() has three parameters')]
    public function testProductProxyConstructorHasThreeParameters()
    {
        $method = new ReflectionMethod(ProductProxy::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[TestDox('ProductProxy::__construct() has one REQUIRED parameter')]
    public function testProductProxyConstructorHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(ProductProxy::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('ProductProxy::__construct() sets three read-only properties')]
    public function testProductProxyConstructorSetsThreeReadOnlyProperties()
    {
        $proxy = new ProductProxy(
            'c6d9e536-5543-49c4-97c7-6ab8b87b66ba',
            'ProductModel',
            'ACME Colorvision 123'
        );

        $this->assertSame('c6d9e536-5543-49c4-97c7-6ab8b87b66ba', $proxy->identifier);
        $this->assertSame('ProductModel', $proxy->type);
        $this->assertSame('ACME Colorvision 123', $proxy->name);
    }

    #[TestDox('ProductProxy.type is not Thing but Product')]
    public function testProductProxyTypeIsNotThingButProduct()
    {
        $proxy = new Proxy('430d4252-09e0-11ee-be56-0242ac120002');
        $this->assertEquals('Thing', $proxy->type);

        $proxy = new ProductProxy('d06584d4-6280-4e64-8a1e-9c56dff12edb');
        $this->assertNotEquals('Thing', $proxy->type);
        $this->assertEquals('Product', $proxy->type);
    }

    #[TestDox('ProductProxy::__construct accepts type as string')]
    public function testProductProxyConstructorAcceptsTypeAsString()
    {
        $proxy = new ProductProxy(
            'd8233d13-5fa0-4c59-b813-3914f30b82bf',
        );
        $this->assertSame('Product', $proxy->type);

        $proxy = new ProductProxy(
            'd8233d13-5fa0-4c59-b813-3914f30b82bf',
            'IndividualProduct'
        );
        $this->assertNotSame('Product', $proxy->type);
        $this->assertSame('IndividualProduct', $proxy->type);
    }
}
