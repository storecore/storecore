<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\Money;
use StoreCore\Database\Currencies;
use StoreCore\Types\QuantitativeValue;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\PriceSpecification::class)]
#[CoversClass(\StoreCore\Database\Currencies::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Money::class)]
#[UsesClass(\StoreCore\Types\QuantitativeValue::class)]
final class PriceSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PriceSpecification class exists')]
    public function testPriceSpecificationClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PriceSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PriceSpecification.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\PriceSpecification'));
        $this->assertTrue(class_exists(PriceSpecification::class));
    }

    #[Group('hmvc')]
    #[TestDox('PriceSpecification class is concrete')]
    public function testPriceSpecificationClassIsConcrete()
    {
        $class = new ReflectionClass(PriceSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PriceSpecification is JSON serializable')]
    public function testPriceSpecificationIsJsonSerializable()
    {
        $this->assertInstanceOf(\JsonSerializable::class, new PriceSpecification());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(PriceSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(PriceSpecification::VERSION);
        $this->assertIsString(PriceSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(PriceSpecification::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('PriceSpecification.eligibleQuantity exists')]
    public function testPriceSpecificationEligibleQuantityExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('eligibleQuantity', $priceSpecification);
    }

    #[TestDox('PriceSpecification.eligibleQuantity is public')]
    public function testPriceSpecificationEligibleQuantityIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'eligibleQuantity');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.eligibleQuantity is null by default')]
    public function testPriceSpecificationEligibleQuantityIsNullByDefault()
    {
        $price = new PriceSpecification();
        $this->assertNull($price->eligibleQuantity);
    }

    #[TestDox('PriceSpecification.eligibleQuantity accepts QuantitativeValue')]
    public function testPriceSpecificationEligibleQuantityAcceptsQuantitativeValue()
    {
        $eligible_quantity = new QuantitativeValue();
        $eligible_quantity->value = 2;
        $price = new PriceSpecification();
        $price->eligibleQuantity = $eligible_quantity;

        $this->assertNotNull($price->eligibleQuantity);
        $this->assertIsObject($price->eligibleQuantity);
        $this->assertInstanceOf(QuantitativeValue::class, $price->eligibleQuantity);
        $this->assertSame(2, $price->eligibleQuantity->value);
    }


    #[TestDox('PriceSpecification.eligibleTransactionVolume exists')]
    public function testPriceSpecificationEligibleTransactionVolumeExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('eligibleTransactionVolume', $priceSpecification);
    }

    #[TestDox('PriceSpecification.eligibleTransactionVolume is public')]
    public function testPriceSpecificationEligibleTransactionVolumeIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'eligibleTransactionVolume');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.eligibleTransactionVolume is null by default')]
    public function testPriceSpecificationEligibleTransactionVolumeIsNullByDefault()
    {
        $price = new PriceSpecification();
        $this->assertNull($price->eligibleTransactionVolume);
    }


    #[TestDox('PriceSpecification.maxPrice exists')]
    public function testPriceSpecificationMaxPriceExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('maxPrice', $priceSpecification);
    }

    #[TestDox('PriceSpecification.maxPrice is public')]
    public function testPriceSpecificationMaxPriceIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'maxPrice');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.maxPrice is null by default')]
    public function testPriceSpecificationMaxPriceIsNullByDefault()
    {
        $price = new PriceSpecification();
        $this->assertNull($price->maxPrice);
    }


    #[TestDox('PriceSpecification.minPrice exists')]
    public function testPriceSpecificationMinPriceExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('minPrice', $priceSpecification);
    }

    #[TestDox('PriceSpecification.minPrice is public')]
    public function testPriceSpecificationMinPriceIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'minPrice');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.minPrice is null by default')]
    public function testPriceSpecificationMinPriceIsNullByDefault()
    {
        $price = new PriceSpecification();
        $this->assertNull($price->minPrice);
    }


    #[TestDox('PriceSpecification.price exists')]
    public function testPriceSpecificationPriceExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('price', $priceSpecification);
    }

    #[TestDox('PriceSpecification.price is public')]
    public function testPriceSpecificationPriceIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'price');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('PriceSpecification.price is null by default')]
    public function testPriceSpecificationPriceIsNullByDefault()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertNull($priceSpecification->price);
    }


    #[TestDox('PriceSpecification.priceCurrency exists')]
    public function testPriceSpecificationPriceCurrencyExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('priceCurrency', $priceSpecification);
    }

    #[TestDox('PriceSpecification.priceCurrency is public')]
    public function testPriceSpecificationPriceCurrencyIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'priceCurrency');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.priceCurrency is null by default')]
    public function testPriceSpecificationPriceCurrencyIsNullByDefault()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertNull($priceSpecification->priceCurrency);
    }


    #[TestDox('PriceSpecification.validFrom exists')]
    public function testPriceSpecificationValidFromExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('validFrom', $priceSpecification);
    }

    #[TestDox('PriceSpecification.validFrom is public')]
    public function testPriceSpecificationValidFromIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'validFrom');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.validFrom is null by default')]
    public function testPriceSpecificationValidFromIsNullByDefault()
    {
        $validFrom = new PriceSpecification();
        $this->assertNull($validFrom->validFrom);
    }


    #[TestDox('PriceSpecification.validThrough exists')]
    public function testPriceSpecificationValidThroughExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('validThrough', $priceSpecification);
    }

    #[TestDox('PriceSpecification.validThrough is public')]
    public function testPriceSpecificationValidThroughIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'validThrough');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.validThrough is null by default')]
    public function testPriceSpecificationValidThroughIsNullByDefault()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertNull($priceSpecification->validThrough);
    }


    #[TestDox('PriceSpecification.valueAddedTaxIncluded exists')]
    public function testPriceSpecificationValueAddedTaxIncludedExists()
    {
        $priceSpecification = new PriceSpecification();
        $this->assertObjectHasProperty('valueAddedTaxIncluded', $priceSpecification);
    }

    #[TestDox('PriceSpecification.valueAddedTaxIncluded is public')]
    public function testPriceSpecificationValueAddedTaxIncludedIsPublic()
    {
        $property = new ReflectionProperty(PriceSpecification::class, 'valueAddedTaxIncluded');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PriceSpecification.valueAddedTaxIncluded is true by default')]
    public function testPriceSpecificationValueAddedTaxIncludedIsTrueByDefault()
    {
        $price = new PriceSpecification();
        $this->assertIsBool($price->valueAddedTaxIncluded);
        $this->assertTrue($price->valueAddedTaxIncluded);
    }

    #[TestDox('PriceSpecification.valueAddedTaxIncluded is nullable')]
    public function testPriceSpecificationValueAddedTaxIncludedIsNullable()
    {
        $price = new PriceSpecification();
        $this->assertNotNull($price->valueAddedTaxIncluded);
        $price->valueAddedTaxIncluded = null;
        $this->assertNull($price->valueAddedTaxIncluded);
    }


    #[TestDox('PriceSpecification::setPrice() exists')]
    public function testPriceSpecificationSetPriceExists()
    {
        $class = new ReflectionClass(PriceSpecification::class);
        $this->assertTrue($class->hasMethod('setPrice'));
    }

    #[TestDox('PriceSpecification::setPrice() is public')]
    public function tesPriceSpecificationSetPriceIsPublic()
    {
        $method = new ReflectionMethod(PriceSpecification::class, 'setPrice');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('PriceSpecification::setPrice() has one REQUIRED parameter')]
    public function testPriceSpecificationSetPriceHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(UnitPriceSpecification::class, 'setPrice');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('PriceSpecification::setPrice() accepts Money')]
    public function testPriceSpecificationSetPriceAcceptsMoney()
    {
        $amount = new Money(297, 'EUR');
        $this->assertInstanceOf(\StoreCore\Money::class, $amount);

        $priceSpecification = new PriceSpecification();
        $priceSpecification->setPrice($amount);

        $this->assertIsFloat($priceSpecification->price);
        $this->assertEquals(297.00, $priceSpecification->price);

        $this->assertIsObject($priceSpecification->priceCurrency);
        $this->assertEquals('EUR', $priceSpecification->priceCurrency);
    }


    /**
     * @see https://schema.org/PriceSpecification#eg-0375 Schema.org example 2
     */
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest()
    {
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "PriceSpecification",
              "price": 0.00,
              "priceCurrency": "USD"
            }
        ';

        $totalPaymentDue = new PriceSpecification();
        $totalPaymentDue->price = 0.00;
        $totalPaymentDue->priceCurrency = Currencies::createFromString('USD');
        $totalPaymentDue->valueAddedTaxIncluded = null;

        $actualJson = json_encode($totalPaymentDue, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
