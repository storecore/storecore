<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\PIM\CompoundPriceSpecification::class)]
#[CoversClass(\StoreCore\PIM\PriceSpecification::class)]
final class CompoundPriceSpecificationTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CompoundPriceSpecification class is concrete')]
    public function testCompoundPriceSpecificationClassIsConcrete()
    {
        $class = new ReflectionClass(CompoundPriceSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CompoundPriceSpecification is a PriceSpecification')]
    public function testCompoundPriceSpecificationIsPriceSpecification()
    {
        $price = new CompoundPriceSpecification();
        $this->assertInstanceOf(PriceSpecification::class, $price);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(CompoundPriceSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(CompoundPriceSpecification::VERSION);
        $this->assertIsString(CompoundPriceSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(CompoundPriceSpecification::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('CompoundPriceSpecification::attach() exists')]
    public function testCompoundPriceSpecificationAttachExists()
    {
        $class = new ReflectionClass(CompoundPriceSpecification::class);
        $this->assertTrue($class->hasMethod('attach'));
    }

    #[TestDox('CompoundPriceSpecification::attach() is public')]
    public function testCompoundPriceSpecificationAttachIsPublic()
    {
        $method = new ReflectionMethod(CompoundPriceSpecification::class, 'attach');
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('CompoundPriceSpecification::attach() has one REQUIRED parameter')]
    public function testCompoundPriceSpecificationAttachHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(CompoundPriceSpecification::class, 'attach');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
