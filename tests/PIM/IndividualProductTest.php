<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\IndividualProduct::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class IndividualProductTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('IndividualProduct class is concrete')]
    public function testIndividualProductClassIsConcrete(): void
    {
        $class = new ReflectionClass(IndividualProduct::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('IndividualProduct implements IdentityInterface')]
    public function testIndividualProductImplementsIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));

        $product = new IndividualProduct();
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $product);
    }

    #[Group('hmvc')]
    #[TestDox('IndividualProduct is JSON serializable')]
    public function testIndividualProductIsJsonSerializable(): void
    {
        $product = new IndividualProduct();
        $this->assertInstanceOf(\JsonSerializable::class, $product);
    }

    #[Group('hmvc')]
    #[TestDox('IndividualProduct is a Product model')]
    public function testIndividualProductIsProductModel(): void
    {
        $this->assertTrue(class_exists(AbstractProductOrService::class));
        $this->assertTrue(class_exists(AbstractProduct::class));
        $this->assertTrue(class_exists(Product::class));

        // Class hierarchy: AbstractProductOrService > AbstractProduct > Product > IndividualProduct
        $product = new IndividualProduct();
        $this->assertInstanceOf(AbstractProductOrService::class, $product);
        $this->assertInstanceOf(AbstractProduct::class, $product);
        $this->assertInstanceOf(Product::class, $product);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(IndividualProduct::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(IndividualProduct::VERSION);
        $this->assertIsString(IndividualProduct::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(IndividualProduct::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('IndividualProduct.serialNumber exists')]
    public function testIndividualProductSerialNumberExists(): void
    {
        $product = new IndividualProduct();
        $this->assertObjectHasProperty('serialNumber', $product);
    }

    #[TestDox('IndividualProduct.serialNumber is public')]
    public function testIndividualProductSerialNumberIsPublic(): void
    {
        $property = new ReflectionProperty(IndividualProduct::class, 'serialNumber');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('IndividualProduct.serialNumber is empty string by default')]
    public function testIndividualProductSerialNumberIsEmptyStringByDefault(): void
    {
        $product = new IndividualProduct();
        $this->assertEmpty($product->serialNumber);
        $this->assertIsString($product->serialNumber);
    }

    #[TestDox('IndividualProduct clone does not have the same serial number')]
    public function testIndividualProductCloneDoesNotHaveTheSameSerialNumber(): void
    {
        $product = new IndividualProduct();
        $product->serialNumber = (string) random_int(1, 2147483647);

        $clone = clone $product;
        $this->assertNotSame($clone->serialNumber, $product->serialNumber);

        $this->assertNotEmpty($product->serialNumber);
        $this->assertEmpty($clone->serialNumber);
    }
}
