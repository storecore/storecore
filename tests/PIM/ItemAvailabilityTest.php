<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\ItemAvailability::class)]
final class ItemAvailabilityTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ItemAvailability enumeration exists')]
    public function testItemAvailabilityClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ItemAvailability.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ItemAvailability.php');
        $this->assertTrue(enum_exists('\\StoreCore\\PIM\\ItemAvailability'));
        $this->assertTrue(enum_exists(ItemAvailability::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $enum = new ReflectionEnum(ItemAvailability::class);
        $this->assertTrue($enum->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ItemAvailability::VERSION);
        $this->assertIsString(ItemAvailability::VERSION);
    }

    /**
     * @see https://schema.org/docs/releases.html Schema.org releases
     */
    #[Depends('testVersionConstantIsNonEmptyString')]
    #[TestDox('VERSION matches Schema.org v23.0')]
    public function testVersionMatchesSchemaOrgVersion23(): void
    {
        $this->assertGreaterThanOrEqual('23.0', ItemAvailability::VERSION);
    }


    /**
     * @see https://schema.org/ItemAvailability#subtypes
     *      Schema.org `ItemAvailability` enumeration members
     */
    #[TestDox('ItemAvailability enumeration has 10 members')]
    public function testItemAvailabilityEnumerationHas10Members(): void
    {
        $this->assertCount(10, ItemAvailability::cases());
    }


    #[TestDox('ItemAvailability::fromInteger() exists')]
    public function testItemAvailabilityFromIntegerExists(): void
    {
        $enum = new ReflectionEnum(ItemAvailability::class);
        $this->assertTrue($enum->hasMethod('fromInteger'));
    }

    #[TestDox('ItemAvailability::fromInteger() is final public static')]
    public function testItemAvailabilityFromIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(ItemAvailability::class, 'fromInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('ItemAvailability::fromInteger() has one REQUIRED parameter')]
    public function testItemAvailabilityFromIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ItemAvailability::class, 'fromInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('ItemAvailability::fromInteger() accepts integers from (int) 1')]
    public function testItemAvailabilityFromIntegerAcceptsIntegersFromInt1(): void
    {
        $this->assertSame(ItemAvailability::BackOrder, ItemAvailability::fromInteger(1));

        $this->expectException(\ValueError::class);
        $failure = ItemAvailability::fromInteger(0);
    }

    #[TestDox('ItemAvailability::fromInteger() accepts integers up to and including (int) 10')]
    public function testItemAvailabilityFromIntegerAcceptsIntegersUpToAndIncludingInt10(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $this->assertIsObject(ItemAvailability::fromInteger($i));
        }

        $this->expectException(\ValueError::class);
        $failure = ItemAvailability::fromInteger(11);
    }


    #[TestDox('ItemAvailability::toInteger() exists')]
    public function testItemAvailabilityToIntegerExists(): void
    {
        $enum = new ReflectionEnum(ItemAvailability::class);
        $this->assertTrue($enum->hasMethod('fromInteger'));
    }

    #[Depends('testItemAvailabilityToIntegerExists')]
    #[TestDox('ItemAvailability::toInteger() is final public static')]
    public function testItemAvailabilityToIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(ItemAvailability::class, 'toInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('ItemAvailability::toInteger() has one REQUIRED parameter')]
    public function testItemAvailabilityToIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ItemAvailability::class, 'toInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testItemAvailabilityToIntegerIsFinalPublicStatic')]
    #[Depends('testItemAvailabilityToIntegerHasOneRequiredParameter')]
    #[TestDox('ItemAvailability::toInteger() accepts ItemAvailability and returns integer')]
    public function testItemAvailabilityToIntegerAcceptsItemAvailabilityAndReturnsInteger(): void
    {
        $item_availability = ItemAvailability::OutOfStock;
        $this->assertIsInt(ItemAvailability::toInteger($item_availability));
        $this->assertGreaterThanOrEqual(1, ItemAvailability::toInteger($item_availability));
    }

    #[Depends('testItemAvailabilityToIntegerAcceptsItemAvailabilityAndReturnsInteger')]
    #[TestDox('ItemAvailability::toInteger() converts all ItemAvailability members to integers')]
    public function test(): void
    {
        foreach (ItemAvailability::cases() as $member) {
            $this->assertIsInt(ItemAvailability::toInteger($member));
        }
    }


    #[TestDox('ItemAvailability::BackOrder exists')]
    public function testItemAvailabilityBackOrderExists(): void
    {
        $this->assertSame('https://schema.org/BackOrder', ItemAvailability::BackOrder->value);
        $this->assertSame(ItemAvailability::BackOrder, ItemAvailability::fromInteger(1));
    }

    #[TestDox('ItemAvailability::InStock exists')]
    public function testItemAvailabilityInStockExists(): void
    {
        $this->assertSame('https://schema.org/InStock', ItemAvailability::InStock->value);
        $this->assertSame(ItemAvailability::InStock, ItemAvailability::fromInteger(3));
    }

    #[TestDox('ItemAvailability::OutOfStock exists')]
    public function testItemAvailabilityOutOfStockExists(): void
    {
        $this->assertSame('https://schema.org/OutOfStock', ItemAvailability::OutOfStock->value);
        $this->assertSame(ItemAvailability::OutOfStock, ItemAvailability::fromInteger(7));
    }
}
