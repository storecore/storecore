<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\IndentityInterface;
use StoreCore\{ProxyInterface, ProxyFactory, Proxy};

use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\ServiceProxy::class)]
#[CoversClass(\StoreCore\Proxy::class)]
#[CoversClass(\StoreCore\ProxyFactory::class)]
#[UsesClass(\StoreCore\PIM\Service::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class ServiceProxyTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ServiceProxy class exists')]
    public function testServiceProxyClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ServiceProxy.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ServiceProxy.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ServiceProxy'));
        $this->assertTrue(class_exists(ServiceProxy::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ServiceProxy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ServiceProxy::VERSION);
        $this->assertIsString(ServiceProxy::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ServiceProxy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('Implemented StoreCore IdentityInterface exists')]
    public function testImplementedStoreCoreIdentityInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertTrue(interface_exists(\StoreCore\IdentityInterface::class));
    }

    #[Depends('testImplementedStoreCoreIdentityInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServiceProxy implements StoreCore IdentityInterface')]
    public function testServiceProxyImplementsStoreCoreIdentityInterface(): void
    {
        $service = new Service();
        $uuid = UUIDFactory::pseudoRandomUUID();
        $service->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromService($service);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('Implemented StoreCore ProxyInterface exists')]
    public function testImplementedStoreCoreProxyInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\ProxyInterface'));
        $this->assertTrue(interface_exists(\StoreCore\ProxyInterface::class));
    }

    #[Depends('testImplementedStoreCoreProxyInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('ServiceProxy implements StoreCore ProxyInterface')]
    public function testServiceProxyImplementsStoreCoreProxyInterface(): void
    {
        $service = new Service();
        $uuid = UUIDFactory::pseudoRandomUUID();
        $service->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromService($service);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('ServiceProxy is JSON serializable')]
    public function testServiceProxyIsJsonSerializable(): void
    {
        $service = new Service();
        $service->setIdentifier('b5e34740-51c8-449d-8bea-edf9f59e3c62');

        $proxy = ProxyFactory::createFromService($service);
        $this->assertInstanceOf(\JsonSerializable::class, $proxy);

        $expectedJson = '
            {
              "@id": "/api/v1/services/b5e34740-51c8-449d-8bea-edf9f59e3c62",
              "@context": "https://schema.org",
              "@type": "Service",
              "identifier": "b5e34740-51c8-449d-8bea-edf9f59e3c62"
            }
        ';
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('ServiceProxy::__construct exists')]
    public function testServiceProxyConstructorExists(): void
    {
        $class = new ReflectionClass(ServiceProxy::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('ServiceProxy::__construct is public constructor')]
    public function testServiceProxyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(ServiceProxy::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('ServiceProxy::__construct has three parameters')]
    public function testServiceProxyConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(ServiceProxy::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testServiceProxyConstructorHasThreeParameters')]
    #[TestDox('ServiceProxy::__construct has one REQUIRED parameter')]
    public function testServiceProxyConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(ServiceProxy::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testServiceProxyConstructorHasThreeParameters')]
    #[TestDox('ServiceProxy::__construct sets three read-only properties')]
    public function testServiceProxyConstructorSetsThreeReadOnlyProperties(): void
    {
        $proxy = new ServiceProxy(
            '167e7b5c-2892-4ece-b2df-91ac3645eeeb',
            'FoodService',
            'Breakfast'
        );

        $this->assertEquals('167e7b5c-2892-4ece-b2df-91ac3645eeeb', $proxy->identifier);
        $this->assertEquals('FoodService', $proxy->type);
        $this->assertEquals('Breakfast', $proxy->name);
    }

    #[Group('hmvc')]
    #[TestDox('ServiceProxy.type is not Thing but Service')]
    public function testServiceProxyTypeIsNotThingButService(): void
    {
        $proxy = new Proxy('430d4252-09e0-11ee-be56-0242ac120002');
        $this->assertEquals('Thing', $proxy->type);

        $proxy = new ServiceProxy('d06584d4-6280-4e64-8a1e-9c56dff12edb');
        $this->assertNotEquals('Thing', $proxy->type);
        $this->assertEquals('Service', $proxy->type);
    }

    #[TestDox('ServiceProxy::__construct accepts type as string')]
    public function testServiceProxyConstructorAcceptsTypeAsString(): void
    {
        $proxy = new ServiceProxy(
            'd8233d13-5fa0-4c59-b813-3914f30b82bf',
        );
        $this->assertEquals('Service', $proxy->type);

        $proxy = new ServiceProxy(
            'd8233d13-5fa0-4c59-b813-3914f30b82bf',
            'PaymentService'
        );
        $this->assertNotEquals('Service', $proxy->type);
        $this->assertEquals('PaymentService', $proxy->type);
    }
}
