<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\Drug::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class DrugTest extends TestCase
{
    #[TestDox('Drug class is concrete')]
    public function testDrugClassIsConcrete(): void
    {
        $class = new ReflectionClass(Drug::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Drug is a Product')]
    public function testDrugIsProduct(): void
    {
        $this->assertInstanceOf(Product::class, new Drug());
    }
}
