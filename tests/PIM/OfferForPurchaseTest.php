<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\OfferForPurchase::class)]
final class OfferForPurchaseTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OfferForPurchase class exists')]
    public function testOfferForPurchaseClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferForPurchase.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferForPurchase.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\OfferForPurchase'));
        $this->assertTrue(class_exists(OfferForPurchase::class));
    }

    #[TestDox('OfferForPurchase class is concrete')]
    public function testOfferForPurchaseClassIsConcrete(): void
    {
        $class = new ReflectionClass(OfferForPurchase::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OfferForPurchase is an Intangible Offer')]
    public function testOfferForPurchaseIsIntangibleOffer(): void
    {
        $offer = new OfferForPurchase();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $offer);
        $this->assertInstanceOf(\StoreCore\PIM\Offer::class, $offer);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OfferForPurchase::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OfferForPurchase::VERSION);
        $this->assertIsString(OfferForPurchase::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OfferForPurchase::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('OfferForPurchase.businessFunction is always Sell')]
    public function testOfferForPurchaseBusinessFunctionIsAlwaysSell()
    {
        $offer = new OfferForPurchase();
        $this->assertObjectHasProperty('businessFunction', $offer);

        $this->assertEquals('Sell', $offer->businessFunction->name);
        $this->assertEquals('http://purl.org/goodrelations/v1#Sell', $offer->businessFunction->value);
    }
}
