<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\ProductGroup::class)]
final class ProductGroupTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ProductGroup class exists')]
    public function testProductGroupClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductGroup.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductGroup.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductGroup'));
        $this->assertTrue(class_exists(ProductGroup::class));
    }
}
