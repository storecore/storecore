<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class EUEnergyEfficiencyEnumerationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('EUEnergyEfficiencyEnumeration exists')]
    public function testEUEnergyEfficiencyEnumerationExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'EUEnergyEfficiencyEnumeration.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'EUEnergyEfficiencyEnumeration.php');
        $this->assertTrue(enum_exists('\\StoreCore\\PIM\\EUEnergyEfficiencyEnumeration'));
        $this->assertTrue(enum_exists(EUEnergyEfficiencyEnumeration::class));
    }


    #[TestDox('EUEnergyEfficiencyEnumeration is an enumeration')]
    public function testEUEnergyEfficiencyEnumerationIsEnumeration(): void
    {
        $class = new ReflectionClass(EUEnergyEfficiencyEnumeration::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testEUEnergyEfficiencyEnumerationIsEnumeration')]
    #[TestDox('EUEnergyEfficiencyEnumeration is a backed enumeration')]
    public function testEUEnergyEfficiencyEnumerationIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(EUEnergyEfficiencyEnumeration::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionEnum(EUEnergyEfficiencyEnumeration::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(EUEnergyEfficiencyEnumeration::VERSION);
        $this->assertIsString(EUEnergyEfficiencyEnumeration::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org v23.0')]
    public function testVersionMatchesSchemaOrgVersion23(): void
    {
        $this->assertTrue(
            version_compare(EUEnergyEfficiencyEnumeration::VERSION, '23.0', '>=')
        );
    }


    #[Depends('testEUEnergyEfficiencyEnumerationIsBackedEnumeration')]
    #[TestDox('EUEnergyEfficiencyEnumeration has ten (10) enumeration members')]
    public function testEUEnergyEfficiencyEnumerationHasTenEnumerationMembers(): void
    {
        $this->assertCount(10, EUEnergyEfficiencyEnumeration::cases());
    }

    #[Depends('testEUEnergyEfficiencyEnumerationIsBackedEnumeration')]
    #[TestDox('All EUEnergyEfficiencyEnumeration enumeration members are Schema.org URIs')]
    public function testAllEUEnergyEfficiencyEnumerationEnumerationMembersAreSchemaOrgURIs(): void
    {
        foreach (EUEnergyEfficiencyEnumeration::cases() as $case) {
            $this->assertStringStartsWith('https://schema.org/', $case->value);
            $this->assertStringEndsWith($case->name, $case->value);
        }
    }
}
