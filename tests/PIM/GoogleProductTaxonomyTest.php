<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\GoogleProductTaxonomy::class)]
#[UsesClass(\StoreCore\Registry::class)]
final class GoogleProductTaxonomyTest extends TestCase
{
    protected function setUp(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $statement = $registry->get('Database')->query('SELECT COUNT(*) FROM `sc_product_taxonomy`');
            if ($statement->fetchColumn(0) === 0) {
                $this->markTestSkipped('StoreCore database table `sc_product_taxonomy` is empty.');
            }
        } catch (\PDOException $e) {
            $this->markTestIncomplete($e->getMessage());
        }

        $registry->get('Database')->beginTransaction();
    }

    protected function tearDown(): void
    {
        $registry = Registry::getInstance();
        if ($registry->has('Database')) {
            $registry->get('Database')->rollback();
        }
    }

    #[Group('hmvc')]
    #[TestDox('GoogleProductTaxonomy class is concrete')]
    public function testGoogleProductTaxonomyClassIsConcrete()
    {
        $class = new ReflectionClass(GoogleProductTaxonomy::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('GoogleProductTaxonomy is a database model')]
    public function testGoogleProductTaxonomyIsDatabaseModel()
    {
        $stub = $this->createStub(GoogleProductTaxonomy::class);
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $stub);
        $this->assertInstanceOf(\StoreCore\Database\AbstractModel::class, $stub);
    }

    #[Group('hmvc')]
    #[TestDox('GoogleProductTaxonomy implements PSR-11 ContainerInterface')]
    public function testGoogleProductTaxonomyImplementsPsr11ContainerInterface()
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));

        $stub = $this->createStub(GoogleProductTaxonomy::class);
        $this->assertInstanceOf(\Psr\Container\ContainerInterface::class, $stub);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(GoogleProductTaxonomy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(GoogleProductTaxonomy::VERSION);
        $this->assertIsString(GoogleProductTaxonomy::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(GoogleProductTaxonomy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('GoogleProductTaxonomy::get() exists')]
    public function testGoogleProductTaxonomyGetExists(): void
    {
        $class = new ReflectionClass(GoogleProductTaxonomy::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('GoogleProductTaxonomy::get() is public')]
    public function testGoogleProductTaxonomyGetIsPublic(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('GoogleProductTaxonomy::get() has one REQUIRED parameter')]
    public function testGoogleProductTaxonomyGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('GoogleProductTaxonomy::get() returns GoogleProductCategory on valid key')]
    public function testGoogleProductTaxonomyGetReturnsGoogleProductCategoryOnValidKey(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    
        $repository = new GoogleProductTaxonomy($registry);
        $category = $repository->get('3287');
        $this->assertIsObject($category);
        $this->assertInstanceOf(GoogleProductCategory::class, $category);

        $this->assertIsInt($category->identifier);
        $this->assertEquals(3287, $category->identifier);
        $this->assertEquals('Toys & Games > Toys > Building Toys > Interlocking Blocks', $category->name);
    }


    #[TestDox('GoogleProductTaxonomy::find() exists')]
    public function testGoogleProductTaxonomyFindExists(): void
    {
        $class = new ReflectionClass(GoogleProductTaxonomy::class);
        $this->assertTrue($class->hasMethod('find'));
    }

    #[Depends('testGoogleProductTaxonomyFindExists')]
    #[TestDox('GoogleProductTaxonomy::find() is public')]
    public function testGoogleProductTaxonomyFindIsPublic(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'find');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testGoogleProductTaxonomyFindExists')]
    #[TestDox('GoogleProductTaxonomy::find() has one REQUIRED parameter')]
    public function testGoogleProductTaxonomyFindHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testGoogleProductTaxonomyFindExists')]
    #[Depends('testGoogleProductTaxonomyFindIsPublic')]
    #[Depends('testGoogleProductTaxonomyFindHasOneRequiredParameter')]
    #[TestDox('GoogleProductTaxonomy::find() returns empty array on empty needle')]
    public function testGoogleProductTaxonomyFindReturnsEmptyArrayOnEmptyNeedle(): void
    {
        $needle = '';
        $this->assertEmpty($needle);
        $this->assertIsString($needle);

        $registry = Registry::getInstance();
        $taxonomy = new GoogleProductTaxonomy($registry);
        $this->assertEmpty($taxonomy->find($needle));
        $this->assertIsArray($taxonomy->find($needle));
    }

    #[Depends('testGoogleProductTaxonomyFindHasOneRequiredParameter')]
    #[TestDox('GoogleProductTaxonomy::find() returns key-value array with up to 10 categories')]
    public function testGoogleProductTaxonomyFindReturnsKeyValueArrayWithUpTo10Categories(): void
    {
        $registry = Registry::getInstance();
        $taxonomy = new GoogleProductTaxonomy($registry);
        $taxonomy->setLanguage('en-US');

        $result = $taxonomy->find('toys');
        $this->assertNotEmpty($result);
        $this->assertIsArray($result);
        $this->assertCount(10, $result);
    }


    #[TestDox('GoogleProductTaxonomy::has() exists')]
    public function testGoogleProductTaxonomyHasExists(): void
    {
        $class = new ReflectionClass(GoogleProductTaxonomy::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('GoogleProductTaxonomy::has() is public')]
    public function testGoogleProductTaxonomyHasIsPublic(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('GoogleProductTaxonomy::has() has one REQUIRED parameter')]
    public function testGoogleProductTaxonomyHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(GoogleProductTaxonomy::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('GoogleProductTaxonomy::has() returns bool')]
    public function testGoogleProductTaxonomyHasReturnsBool(): void
    {
        $registry = Registry::getInstance();

        $repository = new GoogleProductTaxonomy($registry);
        $this->assertTrue($repository->has('3287'));

        $illegal_id = (string) \PHP_INT_MAX;
        $this->assertFalse($repository->has($illegal_id));

        $illegal_id = GoogleProductCategory::INT_MAX + 1;
        $this->assertIsInt($illegal_id);
        $illegal_id = (string) $illegal_id;
        $this->assertFalse($repository->has($illegal_id));
    }

    #[TestDox('GoogleProductTaxonomy::has() always returns false on `(string) 0`')]
    public function testGoogleProductTaxonomyHasAlwaysReturnsFalseOnStringZero(): void
    {
        $registry = Registry::getInstance();
        $repository = new GoogleProductTaxonomy($registry);
        $this->assertIsBool($repository->has('0'));
        $this->assertFalse($repository->has('0'));

        $this->expectException(\TypeError::class);
        $failure = $repository->has(0);
    }
}
