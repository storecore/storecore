<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\Types\PaymentMethod;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\PaymentChargeSpecification::class)]
#[UsesClass(\StoreCore\Types\PaymentMethod::class)]
final class PaymentChargeSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PaymentChargeSpecification class exists')]
    public function testPaymentChargeSpecificationClassFileIsReadable()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PaymentChargeSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'PaymentChargeSpecification.php');
        $this->assertTrue(class_exists(\StoreCore\PIM\PaymentChargeSpecification::class));
        $this->assertTrue(class_exists(PaymentChargeSpecification::class));
    }

    #[TestDox('PaymentChargeSpecification class is concrete')]
    public function testPaymentChargeSpecificationClassIsConcrete()
    {
        $class = new ReflectionClass(PaymentChargeSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PaymentChargeSpecification is a PriceSpecification')]
    public function testPaymentChargeSpecificationIsAPriceSpecification()
    {
        $charge = new PaymentChargeSpecification();
        $this->assertInstanceOf(PriceSpecification::class, $charge);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new ReflectionClass(PaymentChargeSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(PaymentChargeSpecification::VERSION);
        $this->assertIsString(PaymentChargeSpecification::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(PaymentChargeSpecification::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('PaymentChargeSpecification.appliesToPaymentMethod exists')]
    public function testPaymentChargeSpecificationAppliesToPaymentMethodExists(): void
    {
        $class = new ReflectionClass(PaymentChargeSpecification::class);
        $this->assertTrue($class->hasProperty('appliesToPaymentMethod'));
    }

    #[TestDox('PaymentChargeSpecification.appliesToPaymentMethod is public')]
    public function testPaymentChargeSpecificationAppliesToPaymentMethodIsPublic(): void
    {
        $property = new \ReflectionProperty(PaymentChargeSpecification::class, 'appliesToPaymentMethod');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('PaymentChargeSpecification.appliesToPaymentMethod is null by default')]
    public function testPaymentChargeSpecificationAppliesToPaymentMethodIsNullByDefault(): void
    {
        $payment_charge = new PaymentChargeSpecification();
        $this->assertNull($payment_charge->appliesToPaymentMethod);
    }

    #[Group('hmvc')]
    #[TestDox('PaymentChargeSpecification.appliesToPaymentMethod accepts PaymentMethod')]
    public function testPaymentChargeSpecificationAppliesToPaymentMethodAcceptsPaymentMethod(): void
    {
        $payment_charge = new PaymentChargeSpecification();
        $paypal = new PaymentMethod('http://purl.org/goodrelations/v1#PayPal');
        $payment_charge->appliesToPaymentMethod = $paypal;
        $this->assertNotNull($payment_charge->appliesToPaymentMethod);
    }
}
