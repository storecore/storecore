<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PIM;

use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PIM\ProductModel::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ProductModelTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ProductModel class exists')]
    public function testProductModelClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductModel.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductModel.php');

        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductModel'));
        $this->assertTrue(class_exists(ProductModel::class));
    }

    #[TestDox('ProductModel class is concrete')]
    public function testProductModelClassIsConcrete(): void
    {
        $class = new ReflectionClass(ProductModel::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ProductModel is a Product')]
    public function testProductModelIsProduct(): void
    {
        $productModel = new ProductModel();
        $this->assertInstanceOf(AbstractProductOrService::class, $productModel);
        $this->assertInstanceOf(Product::class, $productModel);
    }

    #[Group('hmvc')]
    #[TestDox('ProductModel is JSON serializable')]
    public function testProductModelIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ProductModel());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ProductModel::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ProductModel::VERSION);
        $this->assertIsString(ProductModel::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ProductModel::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('ProductModel.isVariantOf exists')]
    public function testProductModelIsVariantOfExists()
    {
        $productModel = new ProductModel();
        $this->assertObjectHasProperty('isVariantOf', $productModel);
    }

    #[TestDox('ProductModel.isVariantOf is public')]
    public function testProductModelIsVariantOfIsPublic()
    {
        $property = new ReflectionProperty(ProductModel::class, 'isVariantOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('ProductModel.isVariantOf is null by default')]
    public function testProductModelIsVariantOfIsNullByDefault()
    {
        $productModel = new ProductModel();
        $this->assertNull($productModel->isVariantOf);
    }

    #[TestDox('ProductModel.isVariantOf accepts ProductGroup or ProductModel')]
    public function testProductModelIsVariantOfAcceptsProductGroupOrProductModel()
    {
        $productModel = new ProductModel();

        $productModel->isVariantOf = new ProductGroup();
        $this->assertNotNull($productModel->isVariantOf);
        $this->assertInstanceOf(ProductGroup::class, $productModel->isVariantOf);

        $productModel->isVariantOf = new ProductModel();
        $this->assertNotInstanceOf(ProductGroup::class, $productModel->isVariantOf);
        $this->assertInstanceOf(ProductModel::class, $productModel->isVariantOf);
    }


    #[TestDox('ProductModel.predecessorOf exists')]
    public function testProductModelPredecessorOfExists()
    {
        $productModel = new ProductModel();
        $this->assertObjectHasProperty('predecessorOf', $productModel);
    }

    #[TestDox('ProductModel.predecessorOf is public')]
    public function testProductModelPredecessorOfIsPublic()
    {
        $property = new ReflectionProperty(ProductModel::class, 'predecessorOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('ProductModel.predecessorOf is null by default')]
    public function testProductModelPredecessorOfIsNullByDefault()
    {
        $productModel = new ProductModel();
        $this->assertNull($productModel->predecessorOf);
    }

    #[TestDox('ProductModel.predecessorOf accepts ProductModel')]
    public function testProductModelPredecessorOfAcceptsProductModel()
    {
        $productModel = new ProductModel('Foo');
        $productModel->predecessorOf = new ProductModel('Bar');
        $this->assertNotNull($productModel->predecessorOf);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $productModel->predecessorOf);
        $this->assertInstanceOf(\StoreCore\PIM\ProductModel::class, $productModel->predecessorOf);
    }


    #[TestDox('ProductModel.successorOf exists')]
    public function testProductModelSuccessorOfExists()
    {
        $productModel = new ProductModel();
        $this->assertObjectHasProperty('successorOf', $productModel);
    }

    #[TestDox('ProductModel.successorOf is public')]
    public function testProductModelSuccessorOfIsPublic()
    {
        $property = new ReflectionProperty(ProductModel::class, 'successorOf');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('ProductModel.successorOf is null by default')]
    public function testProductModelSuccessorOfIsNullByDefault()
    {
        $productModel = new ProductModel();
        $this->assertNull($productModel->successorOf);
    }

    #[TestDox('ProductModel.successorOf accepts ProductModel')]
    public function testProductModelSuccessorOfAcceptsProductModel()
    {
        $productModel = new ProductModel('Bar');
        $productModel->successorOf = new ProductModel('Foo');
        $this->assertNotNull($productModel->successorOf);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $productModel->successorOf);
        $this->assertInstanceOf(\StoreCore\PIM\ProductModel::class, $productModel->successorOf);
    }


    #[TestDox('ProductModel relations example')]
    public function testProductModelRelationsExample()
    {
        $foo = new ProductModel('Foo');
        $bar = new ProductModel('Bar');
        $bar->successorOf = $foo;
        $foo->predecessorOf = $bar;

        // Foo is the predecessor of Bar …
        $this->assertEquals('Bar', $foo->predecessorOf->name);
        // … and Bar is the successor of Foo:
        $this->assertEquals('Foo', $bar->successorOf->name);
    }


    #[TestDox('ProductModel::jsonSerialize() returns @type ProductModel')]
    public function testProductModeljsonSerializeReturnsTypeProductModel()
    {
        $productModel = new ProductModel();
        $this->assertNotEmpty($productModel->jsonSerialize());
        $this->assertIsArray($productModel->jsonSerialize());
        $this->assertArrayHasKey('@type', $productModel->jsonSerialize());

        $this->assertNotEquals('Product', $productModel->jsonSerialize()['@type']);
        $this->assertEquals('ProductModel', $productModel->jsonSerialize()['@type']);
    }


    /**
     * @see https://schema.org/ProductModel#eg-0031
     *      ProductModel example 1 in JSON-LD
     */
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest()
    {
        // `"@id": "#model"` in the Schema.org example was replaced with a URL.
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@id": "https://www.example.com/tv/acme-colorvision-123",
              "@type": "ProductModel",
              "additionalType": "http://www.productontology.org/id/Television_set",
              "description": "The ACME Colorvision 123 is the leading-edge color TV from our company.",
              "gtin13": "1234567890123",
              "itemCondition": "https://schema.org/NewCondition",
              "name": "ACME Colorvision 123"
            }
        ';

        $product = new ProductModel('ACME Colorvision 123');
        $this->assertEquals('ACME Colorvision 123', $product->name);

        // Properties of `ProductModel`
        $this->assertObjectHasProperty('isVariantOf', $product);
        $this->assertObjectHasProperty('predecessorOf', $product);
        $this->assertObjectHasProperty('successorOf', $product);

        $product->id = new URL('https://www.example.com/tv/acme-colorvision-123');
        $product->additionalType = new URL('http://www.productontology.org/id/Television_set');
        $product->description = 'The ACME Colorvision 123 is the leading-edge color TV from our company.';
        $product->gtin13 = '1234567890123';

        $actualJson = json_encode($product, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
