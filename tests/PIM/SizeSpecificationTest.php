<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\SizeSpecification::class)]
final class SizeSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('SizeSpecification class exists')]
    public function testSizeSpecificationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SizeSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'SizeSpecification.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\SizeSpecification'));
        $this->assertTrue(class_exists(SizeSpecification::class));
    }

    #[Group('hmvc')]
    #[TestDox('SizeSpecification class is concrete')]
    public function testSizeSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(SizeSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('SizeSpecification is a QuantitativeValue')]
    public function testSizeSpecificationIsQuantitativeValue(): void
    {
        $this->assertInstanceOf(\StoreCore\Types\QuantitativeValue:: class, new SizeSpecification());
    }

    #[Group('seo')]
    #[TestDox('SizeSpecification is JSON serializable')]
    public function testSizeSpecificationIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new SizeSpecification());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(SizeSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(SizeSpecification::VERSION);
        $this->assertIsString(SizeSpecification::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(SizeSpecification::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('SizeSpecification.sizeGroup exists')]
    public function testSizeSpecificationSizeGroupExists(): void
    {
        $this->assertObjectHasProperty('sizeGroup', new SizeSpecification());
    }

    #[Depends('testSizeSpecificationSizeGroupExists')]
    #[TestDox('SizeSpecification.sizeGroup is null by default')]
    public function testSizeSpecificationSizeGroupIsNullByDefault(): void
    {
        $size = new SizeSpecification();
        $this->assertNull($size->sizeGroup);
    }


    #[Group('seo')]
    #[TestDox('Schema.org acceptance test')]
    public function testSchemaOrgAcceptanceTest(): void
    {
        $size = new SizeSpecification();
        $size->sizeGroup = array(
            WearableSizeGroupEnumeration::Mens,
            WearableSizeGroupEnumeration::Big,
            WearableSizeGroupEnumeration::Tall,
        );

        $this->assertNotEmpty($size->sizeGroup);
        $this->assertCount(3, $size->sizeGroup);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "SizeSpecification",
              "sizeGroup": [
                "https://schema.org/WearableSizeGroupMens",
                "https://schema.org/WearableSizeGroupBig",
                "https://schema.org/WearableSizeGroupTall"
              ]
            }
        ';
        $actualJson = json_encode($size, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
