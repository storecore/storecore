<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(AggregateOffer::class)]
final class AggregateOfferTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AggregateOffer class file exists')]
    public function testAggregateOfferClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'AggregateOffer.php'
        );
    }

    #[Group('distro')]
    #[TestDox('AggregateOffer class file is readable')]
    public function testAggregateOfferClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'AggregateOffer.php'
        );
    }

    #[Group('hmvc')]
    #[TestDox('AggregateOffer class is concrete')]
    public function testAggregateOfferClassIsConcrete(): void
    {
        $class = new ReflectionClass(AggregateOffer::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AggregateOffer is an Intangible Offer')]
    public function testAggregateOfferIsIntangibleOffer(): void
    {
        $aggregate_offer = new AggregateOffer();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $aggregate_offer);
        $this->assertInstanceOf(\StoreCore\PIM\Offer::class, $aggregate_offer);
    }

    #[Group('seo')]
    #[TestDox('AggregateOffer is JSON serializable')]
    public function testAggregateOfferIsJsonSerializable(): void
    {
        $aggregate_offer = new AggregateOffer();
        $this->assertInstanceOf(\JsonSerializable::class, $aggregate_offer);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AggregateOffer::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AggregateOffer::VERSION);
        $this->assertIsString(AggregateOffer::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AggregateOffer::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('AggregateOffer.highPrice exists')]
    public function testAggregateOfferHighPriceExists(): void
    {
        $offer = new AggregateOffer();
        $this->assertObjectHasProperty('highPrice', $offer);
    }

    #[TestDox('AggregateOffer.highPrice is null by default')]
    public function testAggregateOfferHighPriceIsNullByDefault(): void
    {
        $offer = new AggregateOffer();
        $this->assertNull($offer->highPrice);
    }


    #[TestDox('AggregateOffer.lowPrice exists')]
    public function testAggregateOfferLowPriceExists(): void
    {
        $offer = new AggregateOffer();
        $this->assertObjectHasProperty('lowPrice', $offer);
    }

    #[TestDox('AggregateOffer.lowPrice is null by default')]
    public function testAggregateOfferLowPriceIsNullByDefault(): void
    {
        $offer = new AggregateOffer();
        $this->assertNull($offer->lowPrice);
    }


    #[TestDox('AggregateOffer.offerCount exists')]
    public function testAggregateOfferOfferCountExists(): void
    {
        $offer = new AggregateOffer();
        $this->assertObjectHasProperty('offerCount', $offer);
    }

    #[TestDox('AggregateOffer.offerCount is integer')]
    public function testAggregateOfferOfferCountIsInteger(): void
    {
        $offer = new AggregateOffer();
        $this->assertIsInt($offer->offerCount);
    }

    #[TestDox('AggregateOffer.offerCount is 0 by default')]
    public function testAggregateOfferOfferCountIs0ByDefault(): void
    {
        $offer = new AggregateOffer();
        $this->assertEquals(0, $offer->offerCount);
    }


    #[TestDox('AggregateOffer::count() exists')]
    public function testAggregateOfferCountExists(): void
    {
        $class = new ReflectionClass(AggregateOffer::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('AggregateOffer::count() is public')]
    public function testAggregateOfferCountIsPublic(): void
    {
        $method = new ReflectionMethod(AggregateOffer::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('AggregateOffer::count() has no parameters')]
    public function testAggregateOfferCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(AggregateOffer::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('AggregateOffer::count() returns integer')]
    public function testAggregateOfferCountReturnsInteger(): void
    {
        $offer = new AggregateOffer();
        $this->assertIsInt($offer->count());
        $this->assertEquals(0, $offer->count());
    }

    #[TestDox('AggregateOffer is countable')]
    public function testAggregateOfferIsCountable(): void
    {
        $aggregate_offer = new AggregateOffer();
        $this->assertInstanceOf(\Countable::class, $aggregate_offer);

        $this->assertIsInt(count($aggregate_offer));
        $this->assertEquals(0, count($aggregate_offer));
        $this->assertCount(0, $aggregate_offer);
    }


    #[TestDox('AggregateOffer.offers exists')]
    public function testAggregateOfferOffersExists(): void
    {
        $offer = new AggregateOffer();
        $this->assertObjectHasProperty('offers', $offer);
    }

    #[TestDox('AggregateOffer.offers is empty by default')]
    public function testAggregateOfferOffersIsEmptyByDefault()
    {
        $offer = new AggregateOffer();
        $this->assertEmpty($offer->offers);
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product
     *   The `offerCount` MAY be set to a distinct `Offer` count even
     *   if no concrete offers are included in the `AggregateOffer`:
     * 
     *     ```json
     *     {
     *       "@type": "AggregateOffer",
     *       "offerCount": "5",
     *       "lowPrice": "119.99",
     *       "highPrice": "199.99",
     *       "priceCurrency": "USD"
     *     }
     *     ```
     */
    #[TestDox('AggregateOffer.offerCount may be set if AggregateOffer.offers is empty')]
    public function testAggregateOfferOfferCountMayBeSetIfAggregateOfferOffersIsEmpty(): void
    {
        $offer = new AggregateOffer();
        $offer->offerCount = 5;

        $this->assertEmpty($offer->offers);
        $this->assertEquals(5, $offer->count());
        $this->assertEquals(5, $offer->offerCount);

        $json = json_encode($offer, JSON_PRETTY_PRINT);
        $this->assertStringContainsString('"offerCount": 5', $json);
    }


    #[TestDox('Empty AggregateOffer.offers is not included in JSON-LD')]
    public function testEmptyAggregateOfferOffersIsNotIncludedInJsonLd(): void
    {
        $offer = new AggregateOffer();
        $json = json_encode($offer, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@type": "AggregateOffer"', $json);
        $this->assertStringNotContainsString('"offers"', $json);
    }


    #[TestDox('AggregateOffer.priceCurrency exists')]
    public function testAggregateOfferPriceCurrencyExists(): void
    {
        $offer = new AggregateOffer();
        $this->assertObjectHasProperty('priceCurrency', $offer);
    }

    #[TestDox('AggregateOffer.priceCurrency is (string) `EUR` by default')]
    public function testAggregateOfferPriceCurrencyIsStringEURByDefault(): void
    {
        $offer = new AggregateOffer();
        $this->assertNotEmpty($offer->priceCurrency);
        $this->assertIsString($offer->priceCurrency);
        $this->assertEquals('EUR', $offer->priceCurrency);

        $offer->priceCurrency = 'CHF';
        $this->assertNotSame('EUR', $offer->priceCurrency);
        $this->assertEquals('CHF', $offer->priceCurrency);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product#shopping-aggregator-page-example
     */
    #[TestDox('Google Product.offers shopping aggregator page example')]
    public function testGoogleProductOffersShoppingAggregatorPageExample(): void
    {
        /*
         * ```json
         * {
         *   "@type": "AggregateOffer",
         *   "offerCount": "5",
         *   "lowPrice": "119.99",
         *   "highPrice": "199.99",
         *   "priceCurrency": "USD"
         * }
         * ```
         */
        $aggregate_offer = new AggregateOffer();
        $aggregate_offer->offerCount = 5;
        $aggregate_offer->lowPrice = 119.99;
        $aggregate_offer->highPrice = 199.99;
        $aggregate_offer->priceCurrency = 'USD';
 
        $json = json_encode($aggregate_offer, JSON_PRETTY_PRINT);
        $this->assertStringContainsString('"@type": "AggregateOffer"', $json);
        $this->assertStringContainsString('"offerCount": 5', $json);
        $this->assertStringContainsString('"lowPrice": 119.99', $json);
        $this->assertStringContainsString('"highPrice": 199.99', $json);
        $this->assertStringContainsString('"priceCurrency": "USD"', $json);
    }
}
