<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\ProductOntologyClassIdentifier::class)]
final class ProductOntologyClassIdentifierTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ProductOntologyClassIdentifier class exists')]
    public function testProductOntologyClassIdentifierClassExists()
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductOntologyClassIdentifier.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'ProductOntologyClassIdentifier.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\ProductOntologyClassIdentifier'));
        $this->assertTrue(class_exists(ProductOntologyClassIdentifier::class));
    }

    #[TestDox('ProductOntologyClassIdentifier class is concrete')]
    public function testProductOntologyClassIdentifierClassIsConcrete()
    {
        $class = new ReflectionClass(ProductOntologyClassIdentifier::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ProductOntologyClassIdentifier is JSON serializable')]
    public function testProductOntologyClassIdentifierIsJsonSerializable()
    {
        $category = new ProductOntologyClassIdentifier('http://www.productontology.org/id/Hammer');
        $this->assertInstanceOf(\JsonSerializable::class, $category);
    }

    #[Group('hmvc')]
    #[TestDox('ProductOntologyClassIdentifier is stringable')]
    public function testProductOntologyClassIdentifierIsStringable()
    {
        $category = new ProductOntologyClassIdentifier('http://www.productontology.org/id/Hammer');
        $this->assertInstanceOf(\Stringable::class, $category);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined()
    {
        $class = new \ReflectionClass(ProductOntologyClassIdentifier::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString()
    {
        $this->assertNotEmpty(ProductOntologyClassIdentifier::VERSION);
        $this->assertIsString(ProductOntologyClassIdentifier::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch()
    {
        $this->assertTrue(
            version_compare(ProductOntologyClassIdentifier::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('ProductOntologyClassIdentifier::__construct() exists')]
    public function testProductOntologyClassIdentifierConstructorExists()
    {
        $class = new \ReflectionClass(ProductOntologyClassIdentifier::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('ProductOntologyClassIdentifier::__construct() is public')]
    public function testProductOntologyClassIdentifierConstructorIsPublic()
    {
        $method = new \ReflectionMethod(ProductOntologyClassIdentifier::class, '__construct');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('ProductOntologyClassIdentifier::__construct() has one REQUIRED parameter')]
    public function testProductOntologyClassIdentifierConstructorHasOneRequiredParameter()
    {
        $method = new \ReflectionMethod(ProductOntologyClassIdentifier::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('ProductOntologyClassIdentifier::__construct() accepts Wikipedia URL as string')]
    public function testProductOntologyClassIdentifierConstructorAcceptsWikipediaUrlAsString()
    {
        $url = new ProductOntologyClassIdentifier('https://en.wikipedia.org/wiki/Hammer');
        $this->assertSame('http://www.productontology.org/id/Hammer', (string) $url);
    }
}
