<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PIM\OfferForLease::class)]
final class OfferForLeaseTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OfferForLease class exists')]
    public function testOfferForLeaseClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferForLease.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'PIM' . DIRECTORY_SEPARATOR . 'OfferForLease.php');
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\OfferForLease'));
        $this->assertTrue(class_exists(OfferForLease::class));
    }

    #[TestDox('OfferForLease class is concrete')]
    public function testOfferForLeaseClassIsConcrete(): void
    {
        $class = new ReflectionClass(OfferForLease::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OfferForLease is an Intangible Offer')]
    public function testOfferForLeaseIsIntangibleOffer(): void
    {
        $offer = new OfferForLease();
        $this->assertInstanceOf(\StoreCore\Types\Intangible::class, $offer);
        $this->assertInstanceOf(\StoreCore\PIM\Offer::class, $offer);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OfferForLease::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OfferForLease::VERSION);
        $this->assertIsString(OfferForLease::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OfferForLease::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('OfferForLease.businessFunction is always LeaseOut')]
    public function testOfferForLeaseBusinessFunctionIsAlwaysLeaseOut(): void
    {
        $offer = new OfferForLease();
        $this->assertObjectHasProperty('businessFunction', $offer);

        $this->assertEquals('LeaseOut', $offer->businessFunction->name);
        $this->assertEquals('http://purl.org/goodrelations/v1#LeaseOut', $offer->businessFunction->value);
    }
}
