<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('hmvc')]
final class VisitableInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VisitableInterface interface file exists')]
    public function testVisitableInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'VisitableInterface.php');
    }

    #[Depends('testVisitableInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('VisitableInterface interface file is readable')]
    public function testVisitableInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'VisitableInterface.php');
    }

    #[TestDox('VisitableInterface is an interface')]
    public function testVisitableInterfaceIsAnInterface(): void
    {
        $interface = new ReflectionClass(VisitableInterface::class);
        $this->assertTrue($interface->isInterface());
    }
}
