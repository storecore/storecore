<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\PreOrder::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class PreOrderTest extends TestCase
{
    private PreOrder $preOrder;

    public function setUp(): void
    {
        $registry = Registry::getInstance();
        $this->preOrder = new PreOrder($registry);
    }


    #[Group('hmvc')]
    #[TestDox('PreOrder class is concrete')]
    public function testPreOrderClassIsConcrete(): void
    {
        $class = new ReflectionClass(PreOrder::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('PreOrder is a model')]
    public function testPreOrderIsModel(): void
    {
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $this->preOrder);
    }

    #[Depends('testPreOrderIsModel')]
    #[Group('hmvc')]
    #[TestDox('PreOrder is not a database model')]
    public function testPreOrderIsNotDatabaseModel(): void
    {
        $this->assertNotInstanceOf(\StoreCore\Database\AbstractModel::class, $this->preOrder);
    }

    #[Group('hmvc')]
    #[TestDox('PreOrder is an Order model')]
    public function testPreOrderIsOrderModel(): void
    {
        $this->assertInstanceOf(\StoreCore\Order::class, $this->preOrder);
    }


    #[TestDox('PreOrder is countable')]
    public function testPreOrderIsCountable(): void
    {
        $this->assertInstanceOf(\Countable::class, $this->preOrder);
    }

    #[Depends('testPreOrderIsCountable')]
    #[TestDox('PreOrder::count is (int) 0 by default')]
    public function testdoxPreOrderCountIsInt0ByDefault(): void
    {
        $registry = Registry::getInstance();
        $order = new PreOrder($registry);
        $this->assertEquals(0, $order->count());
        $this->assertEquals(0, count($order));
    }


    #[Group('hmvc')]
    #[TestDox('PreOrder implements StoreCore IdentityInterface')]
    public function testPreOrderImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $this->preOrder);
    }

    #[Group('hmvc')]
    #[TestDox('PreOrder implements StoreCore VisitableInterface')]
    public function testPreOrderImplementsStoreCoreVisitableInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\VisitableInterface::class, $this->preOrder);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PreOrder::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PreOrder::VERSION);
        $this->assertIsString(PreOrder::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PreOrder::VERSION, '0.1.0', '>=')
        );
    }
}
