<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \interface_exists;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractSubjectTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractSubject class exists')]
    public function testAbstractSubjectClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractSubject.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'AbstractSubject.php');

        $this->assertTrue(class_exists('\\StoreCore\\AbstractSubject'));
        $this->assertTrue(class_exists(AbstractSubject::class));
    }


    #[TestDox('AbstractSubject is abstract')]
    public function testAbstractSubjectIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractSubject::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('Implemented SplSubject interface exists')]
    public function testImplementedSplSubjectInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\SplSubject'));
        $this->assertTrue(interface_exists(\SplSubject::class));
    }

    #[Depends('testImplementedSplSubjectInterfaceExists')]
    #[TestDox('AbstractSubject implements SplSubjectInterface')]
    public function testAbstractSubjectImplementsSplSubjectInterface(): void
    {
        $subject = new class extends AbstractSubject {};
        $this->assertInstanceOf(\SplSubject::class, $subject);
    }
}
