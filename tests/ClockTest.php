<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[Group('hmvc')]
#[CoversClass(\StoreCore\Clock::class)]
#[CoversClass(\StoreCore\Registry::class)]
final class ClockTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Clock class is concrete')]
    public function testClockClassIsConcrete(): void
    {
        $class = new ReflectionClass(Clock::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Clock class is final read-only')]
    public function testClockClassIsFinalReadOnly(): void
    {
        $class = new ReflectionClass(Clock::class);
        $this->assertTrue($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-20 ClockInterface exists')]
    public function testImplementedPSR20ClockInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));
        $this->assertTrue(interface_exists(\Psr\Clock\ClockInterface::class));
    }

    #[Depends('testClockClassIsConcrete')]
    #[Depends('testImplementedPSR20ClockInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Clock class implements PSR-20 ClockInterface')]
    public function testClockClassImplementsPSR20ClockInterface(): void
    {
        $this->assertInstanceOf(\Psr\Clock\ClockInterface::class, new Clock());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Clock::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Clock::VERSION);
        $this->assertIsString(Clock::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Clock::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Clock::now exists')]
    public function testClockNowExists(): void
    {
        $class = new ReflectionClass(Clock::class);
        $this->assertTrue($class->hasMethod('now'));
    }

    #[Depends('testClockNowExists')]
    #[TestDox('Clock::now is public')]
    public function testClockNowIsPublic(): void
    {
        $method = new ReflectionMethod(Clock::class, 'now');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testClockNowExists')]
    #[TestDox('Clock::now has no parameters')]
    public function testClockNowHasNoParameters(): void
    {
        $method = new ReflectionMethod(Clock::class, 'now');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testClockNowExists')]
    #[Depends('testClockNowIsPublic')]
    #[Depends('testClockNowHasNoParameters')]
    #[TestDox('Clock::now returns \DateTimeImmutable object')]
    public function testClockNowReturnsDateTimeImmutableObject(): void
    {
        $clock = new Clock();
        $this->assertIsObject($clock->now());
        $this->assertInstanceOf(\DateTimeInterface::class, $clock->now());
        $this->assertInstanceOf(\DateTimeImmutable::class, $clock->now());
    }

    #[Depends('testClockNowReturnsDateTimeImmutableObject')]
    #[TestDox('Clock::now returns server request time')]
    public function testClockNowReturnsServerRequestTime(): void
    {
        $this->assertArrayHasKey('REQUEST_TIME_FLOAT', $_SERVER);

        $server_request_time = \DateTime::createFromFormat('U.u', (string) $_SERVER['REQUEST_TIME_FLOAT']);
        $clock = new Clock();

        $this->assertEquals(
            $server_request_time->format('Y-m-d H:i:s.u'),
            $clock->now()->format('Y-m-d H:i:s.u'),
            'Clock time in microsecnds MUST be identical to server request time.'
        );
    }

    #[Depends('testClockNowReturnsServerRequestTime')]
    #[TestDox('Clock::now returns current date and time on missing server request time')]
    public function testClockNowReturnsCurrentDateAndTimeOnMissingServerRequestTime(): void
    {
        $server = $_SERVER;
        $_SERVER = array();

        $clock = new Clock();
        $now = $clock->now();
        $this->assertInstanceOf(\DateTimeInterface::class, $now);
        $this->assertInstanceOf(\DateTimeImmutable::class, $now);

        $_SERVER = $server;
    }


    #[Depends('testClockNowReturnsDateTimeImmutableObject')]
    #[TestDox('Clock is registered globally')]
    public function testClockIsRegisteredGlobally(): void
    {
        $registry = Registry::getInstance();
        $this->assertTrue($registry->has('Clock'));
        $this->assertInstanceOf(\DateTimeImmutable::class, $registry->get('Clock')->now());
    }

    /**
     * @see https://www.php-fig.org/psr/psr-20/#13-usage
     */
    #[Depends('testClockNowReturnsDateTimeImmutableObject')]
    #[Depends('testClockIsRegisteredGlobally')]
    #[TestDox('Get the current timestamp')]
    public function testGetTheCurrentTimestamp(): void
    {
        $clock = new Clock();
        $timestamp = $clock->now()->getTimestamp();
        $this->assertIsInt($timestamp);

        $registry = Registry::getInstance();
        $timestamp = $registry->get('Clock')->now()->getTimestamp();
        $this->assertIsInt($timestamp);

        $this->assertSame(
            $clock->now()->getTimestamp(),
            $registry->get('Clock')->now()->getTimestamp()
        );
    }
}
