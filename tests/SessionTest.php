<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2017, 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\OML\OrderProxy;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\BackupGlobals;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \isset;
use function \version_compare;

#[BackupGlobals(true)]
#[CoversClass(\StoreCore\Session::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Engine\Request::class)]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\OML\OrderProxy::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class SessionTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        if (!isset($_SESSION)) {
            $_SESSION = array();
        }
    }

    #[Group('hmvc')]
    #[TestDox('Session class is concrete')]
    public function testSessionClassIsConcrete(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testSessionClassIsConcrete')]
    #[Group('hmvc')]
    #[TestDox('Session implements SplSubject interface')]
    public function testSessionImplementsSplSubjectInterface(): void
    {
        $this->assertInstanceOf(\SplSubject::class, new Session());
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-16 CacheInterface exists')]
    public function testImplementedPSR16CacheInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\SimpleCache\\CacheInterface'));
        $this->assertTrue(interface_exists(\Psr\SimpleCache\CacheInterface::class));
    }

    #[Depends('testSessionClassIsConcrete')]
    #[Depends('testImplementedPSR16CacheInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Session implements PSR-16 CacheInterface')]
    public function testSessionImplementsPsr16CacheInterface(): void
    {
        $this->assertInstanceOf(\Psr\SimpleCache\CacheInterface::class, new Session());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Session::VERSION);
        $this->assertIsString(Session::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Session::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Session::__construct exists')]
    public function testSessionConstructorExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testSessionConstructorExists')]
    #[TestDox('Session::__construct is public constructor')]
    public function testSessionContructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Session::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testSessionConstructorExists')]
    #[TestDox('Session::__construct has one OPTIONAL parameter')]
    public function testSessionContructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Session::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testSessionContructorHasOneOptionalParameter')]
    #[TestDox('Session::__construct throws type error if parameter is not an integer')]
    public function testSessionConstructThrowsTypeErrorIfParameterIsNotAnInteger(): void
    {
        $session = new Session(300);
        $this->assertInstanceOf(\StoreCore\Session::class, $session);

        $this->expectException(\TypeError::class);
        $failure = new Session('300');
    }

    #[Depends('testSessionContructorHasOneOptionalParameter')]
    #[Depends('testSessionConstructThrowsTypeErrorIfParameterIsNotAnInteger')]
    #[TestDox('Session::__construct throws value error if parameter less than 60 (sixty) seconds')]
    public function testSessionConstructThrowsTypeErrorIfParameterIsLessThanSixtySeconds(): void
    {
        $this->expectException(\ValueError::class);
        $failure = new Session(59);
    }


    #[TestDox('Session::clear exists')]
    public function testSessionClearExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('clear'));
    }

    #[Depends('testSessionClearExists')]
    #[TestDox('Session::clear is public')]
    public function testSessionClearIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'clear');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionClearExists')]
    #[TestDox('Session::clear has no parameters')]
    public function testSessionClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testSessionClearExists')]
    #[TestDox('Session::clear returns (bool) true on success')]
    public function testSessionClearReturnsBoolTrueOnSuccess(): void
    {
        $session = new Session();
        $this->assertTrue($session->clear());
    }


    #[TestDox('Session::delete exists')]
    public function testSessionDeleteExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('delete'));
    }

    #[Depends('testSessionDeleteExists')]
    #[TestDox('Session::delete is public')]
    public function testSessionDeleteIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'delete');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionDeleteExists')]
    #[TestDox('Session::delete has one REQUIRED parameter')]
    public function testSessionDeleteHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'delete');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Session::deleteMultiple exists')]
    public function testSessionDeleteMultipleExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('deleteMultiple'));
    }

    #[Depends('testSessionDeleteMultipleExists')]
    #[TestDox('Session::deleteMultiple is public')]
    public function testSessionDeleteMultipleIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'deleteMultiple');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionDeleteMultipleExists')]
    #[TestDox('Session::deleteMultiple has one REQUIRED parameter')]
    public function testSessionDeleteMultipleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'deleteMultiple');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Session::destroy exists')]
    public function testSessionDestroyExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('destroy'));
    }

    #[Depends('testSessionDestroyExists')]
    #[TestDox('Session::destroy is public')]
    public function testSessionDestroyIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'destroy');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionDestroyExists')]
    #[TestDox('Session::destroy has no parameters')]
    public function testSessionDestroyHasNoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'destroy');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('Session::get exists')]
    public function testSessionGetExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testSessionGetExists')]
    #[TestDox('Session::get is public')]
    public function testSessionGetIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'get');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionGetExists')]
    #[TestDox('Session::get has two parameters')]
    public function testSessionGetHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'get');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testSessionGetHasTwoParameters')]
    #[TestDox('Session::get has one REQUIRED parameter')]
    public function testSessionGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'get');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Session::getMultiple exists')]
    public function testSessionGetMultipleExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('getMultiple'));
    }

    #[Depends('testSessionGetMultipleExists')]
    #[TestDox('Session::getMultiple is public')]
    public function testSessionGetMultipleIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'getMultiple');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionGetMultipleExists')]
    #[TestDox('Session::getMultiple has two parameters')]
    public function testSessionGetMultipleHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'getMultiple');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testSessionGetMultipleHasTwoParameters')]
    #[TestDox('Session::getMultiple has one REQUIRED parameter')]
    public function testSessionGetMultipleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'getMultiple');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Session::has exists')]
    public function testSessionHasExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testSessionHasExists')]
    #[TestDox('Session::has is public')]
    public function testSessionHasIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'has');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionHasExists')]
    #[TestDox('Session::has has one REQUIRED parameter')]
    public function testSessionHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Session::regenerate exists')]
    public function testSessionRegenerateExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('regenerate'));
    }

    #[Depends('testSessionRegenerateExists')]
    #[TestDox('Session::regenerate is public')]
    public function testSessionRegenerateIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'regenerate');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionRegenerateExists')]
    #[TestDox('Session::regenerate has no parameters')]
    public function testSessionRegenerateHasNoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'regenerate');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Session::regenerate generates new session ID')]
    public function testSessionRegenerateGeneratesNewSessionID(): void
    {
        $session = new Session();
        $session_id = session_id();
        $this->assertNotFalse($session_id);

        $this->assertTrue($session->regenerate());
        $this->assertNotEquals(
            $session_id, session_id(),
            'The session MUST now have a new session ID.'
        );
    }


    #[TestDox('Session::set exists')]
    public function testSessionSetExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testSessionSetExists')]
    #[TestDox('Session::set is public')]
    public function testSessionSetIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'set');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionSetExists')]
    #[TestDox('Session::set has three parameters')]
    public function testSessionSetHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'set');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testSessionSetHasThreeParameters')]
    #[TestDox('Session::set has two REQUIRED parameters')]
    public function testSessionSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'set');
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testSessionSetExists')]
    #[Depends('testSessionSetIsPublic')]
    #[Depends('testSessionSetHasTwoRequiredParameters')]
    #[TestDox('Session::set sets session variable')]
    public function testSessionSetSetsSessionVariable(): void
    {
        $session = new Session();
        $this->assertFalse($session->has('Order'));

        $order = new OrderProxy(
            '64e082a7-cf3b-4449-9a1e-74fc0e143c84',
            'Order',
            '12345',
            '678-901234-567890',
        );

        $this->assertTrue($session->set('Order', $order));
        $this->assertTrue($session->has('Order'));

        unset($order);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $session->get('Order'));
        $this->assertTrue($session->get('Order')->hasIdentifier());
        $this->assertEquals('64e082a7-cf3b-4449-9a1e-74fc0e143c84', (string) $session->get('Order')->getIdentifier());

        $this->assertTrue($session->delete('Order'));
        $this->assertFalse($session->has('Order'));
    }


    #[TestDox('Session::setMultiple exists')]
    public function testSessionSetMultipleExists(): void
    {
        $class = new ReflectionClass(Session::class);
        $this->assertTrue($class->hasMethod('setMultiple'));
    }

    #[Depends('testSessionSetMultipleExists')]
    #[TestDox('Session::setMultiple is public')]
    public function testSessionSetMultipleIsPublic(): void
    {
        $method = new ReflectionMethod(Session::class, 'setMultiple');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testSessionSetMultipleExists')]
    #[TestDox('Session::setMultiple has two parameters')]
    public function testSessionSetMultipleHasTwoParameters(): void
    {
        $method = new ReflectionMethod(Session::class, 'setMultiple');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testSessionSetMultipleExists')]
    #[TestDox('Session::setMultiple has one REQUIRED parameter')]
    public function testSessionSetMultipleHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Session::class, 'setMultiple');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testSessionSetMultipleExists')]
    #[Depends('testSessionSetMultipleIsPublic')]
    #[Depends('testSessionSetMultipleHasOneRequiredParameter')]
    #[TestDox('Session::setMultiple sets multiple session variables')]
    public function testSessionSetMultipleSetsMultipleSessionVariables(): void
    {
        $params = array(
            'Currency' => new Currency(978, 'EUR', 2, '€'),
            'DateTimeZone' => new \DateTimeZone('Europe/Berlin'),
            'Language' => 'de-DE',
        );

        $session = new Session();
        $this->assertTrue($session->setMultiple($params));

        $this->assertTrue($session->has('Currency'));
        $this->assertTrue($session->has('DateTimeZone'));
        $this->assertTrue($session->has('Language'));
    }
}
