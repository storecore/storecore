<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\TestCase;

use function \json_encode;

use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\I18N\Language::class)]
#[UsesClass(\StoreCore\Types\LanguageCode::class)]
final class LanguageTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Language class is concrete')]
    public function testLanguageClassIsConcrete(): void
    {
        $class = new ReflectionClass(Language::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testLanguageClassIsConcrete')]
    #[TestDox('Language class is read-only')]
    public function testLanguageClassIsReadOnly(): void
    {
        $class = new ReflectionClass(Language::class);
        $this->assertTrue($class->isReadOnly());
    }

    #[TestDox('Language value object is stringable')]
    public function testLanguageValueObjectIsStringable(): void
    {
        $language = new Language('en', 'English');
        $this->assertInstanceOf(\Stringable::class, $language);

        $this->assertNotEmpty((string) $language);
        $this->assertIsString((string) $language);
    }

    #[TestDox('Language value object is JSON serializable')]
    public function testLanguageValueObjectIsJsonSerializable(): void
    {
        $language = new Language('es', 'Español', 'Spanish');
        $this->assertInstanceOf(\JsonSerializable::class, $language);

        $this->assertNotEmpty($language->jsonSerialize());
        $this->assertIsArray($language->jsonSerialize());

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "Language",
         *   "identifier": "es",
         *   "name": "Español",
         *   "alternateName": "Spanish"
         * }
         * ```
         */
        $json = json_encode($language, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('{"@context":"https://schema.org","@type":"Language","identifier":"es","name":"Español","alternateName":"Spanish"}', $json);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Language::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Language::VERSION);
        $this->assertIsString(Language::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Language::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Language::__construct exists')]
    public function testLanguageConstructorExists(): void
    {
        $class = new ReflectionClass(Language::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testLanguageConstructorExists')]
    #[TestDox('Language::__construct is public constructor')]
    public function testLanguageConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Language::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testLanguageConstructorExists')]
    #[TestDox('Language::__construct has three parameters')]
    public function testLanguageConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(Language::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testLanguageConstructorHasThreeParameters')]
    #[TestDox('Language::__construct has one REQUIRED parameter')]
    public function testLanguageConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Language::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLanguageConstructorHasOneRequiredParameter')]
    #[TestDox('Language::__construct accepts Language.identifier as string')]
    public function testLanguageConstructorAcceptsLanguageIdentifierAsString(): void
    {
        $language = new Language('en-US');
        $this->assertSame('en-US', $language->identifier);
    }

    #[Depends('testLanguageConstructorHasOneRequiredParameter')]
    #[TestDox('Language::__construct accepts LanguageCode object as Language.identifier')]
    public function testLanguageConstructorAcceptsLanguageCodeObjectAsLanguageIdentifier(): void
    {
        $code = new LanguageCode('en-US');
        $language = new Language($code);
        $this->assertSame('en-US', $language->identifier);
    }

    #[Depends('testLanguageConstructorHasThreeParameters')]
    #[TestDox('Language::__construct sets Language.name and Language.alternateName')]
    public function testLanguageConstructorSetsLanguageNameAndLanguageAlternateName(): void
    {
        $language = new Language('eng', 'English');
        $this->assertSame('English', $language->name);
        $this->assertNull($language->alternateName);

        $language = new Language('es', 'Español', 'Spanish');
        $this->assertSame('Español', $language->name);
        $this->assertSame('Spanish', $language->alternateName);
    }
}
