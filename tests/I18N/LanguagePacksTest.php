<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \count;
use function \version_compare;

#[CoversClass(\StoreCore\I18N\LanguagePacks::class)]
#[CoversClass(\StoreCore\I18N\AcceptLanguage::class)]
#[CoversClass(\StoreCore\Types\LanguageCode::class)]
final class LanguagePacksTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LanguagePacks::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LanguagePacks::VERSION);
        $this->assertIsString(LanguagePacks::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LanguagePacks::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('LanguagePacks::load exists')]
    public function testLanguagePacksLoadExists(): void
    {
        $class = new ReflectionClass(LanguagePacks::class);
        $this->assertTrue($class->hasMethod('load'));
    }

    #[TestDox('LanguagePacks::load is public static')]
    public function testLanguagePacksLoadIsPublicStatic(): void
    {
        $method = new ReflectionMethod(LanguagePacks::class, 'load');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('LanguagePacks::load has one OPTIONAL parameter')]
    public function testLanguagePacksLoadHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(LanguagePacks::class, 'load');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testLanguagePacksLoadHasOneOptionalParameter')]
    #[TestDox('LanguagePacks::load has one OPTIONAL parameter')]
    public function testLanguagePacksLoadDoesNotAcceptLanguageCodeAsString(): void
    {
        $this->expectException(\TypeError::class);
        LanguagePacks::load('en-US');
    }


    #[TestDox('LanguagePacks::getSupportedLanguages exists')]
    public function testLanguagePacksGetSupportedLanguagesExists(): void
    {
        $class = new ReflectionClass(LanguagePacks::class);
        $this->assertTrue($class->hasMethod('getSupportedLanguages'));
    }

    #[TestDox('LanguagePacks::getSupportedLanguages is public static')]
    public function testLanguagePacksGetSupportedLanguagesIsPublicStatic(): void
    {
        $method = new ReflectionMethod(LanguagePacks::class, 'getSupportedLanguages');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('LanguagePacks::getSupportedLanguages has no parameters')]
    public function testLanguagePacksGetSupportedLanguagesHasNoParameters(): void
    {
        $method = new ReflectionMethod(LanguagePacks::class, 'getSupportedLanguages');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('LanguagePacks::getSupportedLanguages returns non-empty array')]
    public function testLanguagePacksGetSupportedLanguagesReturnsNonEmptyArray(): void
    {
        $this->assertNotEmpty(LanguagePacks::getSupportedLanguages());
        $this->assertIsArray(LanguagePacks::getSupportedLanguages());
    }

    #[Depends('testLanguagePacksGetSupportedLanguagesReturnsNonEmptyArray')]
    #[TestDox('LanguagePacks::getSupportedLanguages returns at least one language')]
    public function testLanguagePacksGetSupportedLanguagesReturnsAtLeastOneLanguage(): void
    {
        $this->assertTrue(count(LanguagePacks::getSupportedLanguages()) >= 1);
    }

    #[Depends('testLanguagePacksGetSupportedLanguagesReturnsAtLeastOneLanguage')]
    #[TestDox('LanguagePacks::getSupportedLanguages() returns `en-GB` for British English')]
    public function testLanguagePacksGetSupportedLanguagesReturnsEnGbForBritishEnglish(): void
    {
        $this->assertArrayHasKey('en-GB', LanguagePacks::getSupportedLanguages());
    }

    #[Depends('testLanguagePacksGetSupportedLanguagesReturnsNonEmptyArray')]
    #[TestDox('LanguagePacks::getSupportedLanguages returns six supported lanuages')]
    public function testLanguagePacksGetSupportedLanguagesReturnsSixSupportedLanguage(): void
    {
        $this->assertEquals(6, count(LanguagePacks::getSupportedLanguages()));

        $languages = [
            'de-DE',
            'en-GB',
            'en-US',
            'fr-FR',
            'nl-BE',
            'nl-NL',
        ];

        foreach ($languages as $language) {
            $this->assertArrayHasKey($language, LanguagePacks::getSupportedLanguages());
        }
    }
}
