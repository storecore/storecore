<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\I18N;

use \ReflectionClass, \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\I18N\Languages::class)]
final class LanguagesTest extends TestCase
{
    #[TestDox('Languages is an enumeration')]
    public function testLanguagesIsEnumeration(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testLanguagesIsEnumeration')]
    #[TestDox('Languages is a backed enumeration')]
    public function testLanguagesIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(Languages::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Languages::VERSION);
        $this->assertIsString(Languages::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Languages::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('AUSTRIA constant is defined')]
    public function testAustriaConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('AUSTRIA'));
    }

    #[Depends('testAustriaConstantIsDefined')]
    #[TestDox("AUSTRIA constant is non-empty array")]
    public function testAustriaConstantIsNonEmptyArray(): void
    {
        $this->assertNotEmpty(Languages::AUSTRIA);
        $this->assertIsArray(Languages::AUSTRIA);
    }

    #[Depends('testAustriaConstantIsNonEmptyArray')]
    #[TestDox("AUSTRIA contains four languages")]
    public function testAustriaContainsFourLanguages(): void
    {
        $this->assertEquals(4, count(Languages::AUSTRIA));
    }


    #[TestDox('DEFAULT_LANGUAGE constant is defined')]
    public function testDefaultLanguageConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('DEFAULT_LANGUAGE'));
    }

    #[Depends('testDefaultLanguageConstantIsDefined')]
    #[TestDox("DEFAULT_LANGUAGE is (string) 'en-GB' for British English")]
    public function testDefaultLanguageConstantIsStringEnGbForBritishEnglish(): void
    {
        $this->assertIsString(Languages::DEFAULT_LANGUAGE);
        $this->assertEquals('en-GB', Languages::DEFAULT_LANGUAGE);
    }


    #[TestDox('EFIGS constant is defined')]
    public function testEfigsConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('EFIGS'));
    }

    #[Depends('testAustriaConstantIsDefined')]
    #[TestDox("EFIGS constant is non-empty array")]
    public function testEfigsConstantIsNonEmptyArray(): void
    {
        $this->assertNotEmpty(Languages::EFIGS);
        $this->assertIsArray(Languages::EFIGS);
    }

    #[Depends('testAustriaConstantIsNonEmptyArray')]
    #[TestDox("EFIGS contains at least five languages")]
    public function testEfigsContainsAtLeastFiveLanguages(): void
    {
        $this->assertGreaterThanOrEqual(5, count(Languages::EFIGS));
    }


    #[TestDox('EUROPEAN_UNION constant is defined')]
    public function testEuropeanUnionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('EUROPEAN_UNION'));
    }

    #[Depends('testEuropeanUnionConstantIsDefined')]
    #[TestDox("EUROPEAN_UNION constant is non-empty array")]
    public function testEuropeanUnionConstantIsNonEmptyArray(): void
    {
        $this->assertNotEmpty(Languages::EUROPEAN_UNION);
        $this->assertIsArray(Languages::EUROPEAN_UNION);
    }

    #[Depends('testEuropeanUnionConstantIsNonEmptyArray')]
    #[TestDox("EUROPEAN_UNION contains 24 languages")]
    public function testEuropeanUnionContainsTwentyFourLanguages(): void
    {
        $this->assertCount(24, Languages::EUROPEAN_UNION);
    }

    #[Depends('testEuropeanUnionConstantIsNonEmptyArray')]
    #[TestDox("EU is alias of EUROPEAN_UNION")]
    public function testEuIsAliasOfEuropeanUnion(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('EU'));

        $this->assertEquals(Languages::EUROPEAN_UNION, Languages::EU);
    }

    
    #[TestDox('SWITZERLAND constant is defined')]
    public function testSwitzerlandConstantIsDefined(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('SWITZERLAND'));
    }

    #[Depends('testSwitzerlandConstantIsDefined')]
    #[TestDox("SWITZERLAND constant is non-empty array")]
    public function testSwitzerlandConstantIsNonEmptyArray(): void
    {
        $this->assertNotEmpty(Languages::SWITZERLAND);
        $this->assertIsArray(Languages::SWITZERLAND);
    }

    #[Depends('testSwitzerlandConstantIsNonEmptyArray')]
    #[TestDox("SWITZERLAND contains 3 languages")]
    public function testEuropeanUnionContainsThreeLanguages(): void
    {
        $this->assertCount(3, Languages::SWITZERLAND);
    }

    #[Depends('testSwitzerlandConstantIsNonEmptyArray')]
    #[TestDox("CH is alias of SWITZERLAND")]
    public function testChIsAliasOfSwitzerland(): void
    {
        $class = new ReflectionClass(Languages::class);
        $this->assertTrue($class->hasConstant('CH'));

        $this->assertEquals(Languages::SWITZERLAND, Languages::CH);
    }
}
