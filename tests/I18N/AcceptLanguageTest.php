<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\BackupGlobals;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \empty;
use function \isset;
use function \version_compare;

#[BackupGlobals(true)]
#[CoversClass(\StoreCore\I18N\AcceptLanguage::class)]
#[CoversClass(\StoreCore\Types\LanguageCode::class)]
final class AcceptLanguageTest extends TestCase
{
    private mixed $serverHttpAcceptLanguage = false;

    public function setUp(): void
    {
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && !empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $this->serverHttpAcceptLanguage = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $this->serverHttpAcceptLanguage = false;
        }
    }

    public function tearDown(): void
    {
        if ($this->serverHttpAcceptLanguage !== false) {
            $_SERVER['HTTP_ACCEPT_LANGUAGE'] = $this->serverHttpAcceptLanguage;
        }
    }

    #[Group('hmvc')]
    #[TestDox('AcceptLanguage class is concrete')]
    public function testAcceptLanguageClassIsConcrete(): void
    {
        $class = new ReflectionClass(AcceptLanguage::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(AcceptLanguage::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AcceptLanguage::VERSION);
        $this->assertIsString(AcceptLanguage::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AcceptLanguage::VERSION, '0.2.2', '>=')
        );
    }


    #[TestDox('AcceptLanguage::$defaultLanguage exists')]
    public function testAcceptLanguageDefaultLanguageExists(): void
    {
        $acceptLanguage = new AcceptLanguage();
        $this->assertObjectHasProperty('defaultLanguage', $acceptLanguage);
    }


    #[TestDox('AcceptLanguage::negotiate exists')]
    public function testAcceptLanguageNegotiateExists(): void
    {
        $class = new ReflectionClass(AcceptLanguage::class);
        $this->assertTrue($class->hasMethod('negotiate'));
    }

    #[Depends('testAcceptLanguageNegotiateExists')]
    #[TestDox('AcceptLanguage::negotiate is public')]
    public function testAcceptLanguageNegotiateIsPublic(): void
    {
        $method = new ReflectionMethod(AcceptLanguage::class, 'negotiate');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAcceptLanguageNegotiateExists')]
    #[TestDox('AccAcceptLanguage::negotiate has two parameters')]
    public function testAcceptLanguageNegotiateHasTwoParameters(): void
    {
        $method = new ReflectionMethod(AcceptLanguage::class, 'negotiate');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testAcceptLanguageNegotiateHasTwoParameters')]
    #[TestDox('AcceptLanguage::negotiate has one REQUIRED parameter')]
    public function testAcceptLanguageNegotiateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AcceptLanguage::class, 'negotiate');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAcceptLanguageNegotiateHasOneRequiredParameter')]
    #[TestDox('AcceptLanguage::negotiate returns preferred language')]
    public function testAcceptLanguageNegotiateReturnsPreferredLanguage(): void
    {
        $supported_languages = array(
            'en-GB' => true,
            'en-US' => true,
            'de-DE' => true,
            'fr-FR' => true,
            'nl-BE' => true,
            'nl-NL' => true,
        );

        $headers_to_languages = array(
            'nl-NL,nl;q=0.9' => 'nl-NL',
            'nl' => 'nl-NL',
            'nl,en-US;q=0.7,en;q=0.3' => 'nl-NL',
            'nl-NL,nl;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6' => 'nl-NL',

            // @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.4
            'da, en-gb;q=0.8, en;q=0.7' => 'en-GB',

            // @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language
            'fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5' => 'fr-FR',
            'de' => 'de-DE',
            'de-CH' => 'de-DE',
            'en-US,en;q=0.5' => 'en-US',
        );

        $language = new \StoreCore\I18N\AcceptLanguage();
        foreach ($headers_to_languages as $header => $expected) {
            $_SERVER['HTTP_ACCEPT_LANGUAGE'] = $header;
            $this->assertEquals(
                new \StoreCore\Types\LanguageCode($expected),
                $language->negotiate($supported_languages),
                'HTTP Accept-Language header "' . $header . '" SHOULD be mapped to language code "' . $expected . '".'
            );
        }
    }

    /**
     * If `en-GB` is included in the supported languages but it is not
     * the first language mentioned, then `negotiate()` should still
     * return `en-GB` on the invalid Accept-Language `en-UK`.
     */
    #[Depends('testAcceptLanguageNegotiateReturnsPreferredLanguage')]
    #[TestDox('AcceptLanguage::negotiate maps `en-UK` to `en-GB`')]
    public function testAcceptLanguageNegotiateMapsEnUkToEnGb(): void
    {
        $supported_languages = array(
            'en-US' => true,
            'en-GB' => true,
        );

        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'en-UK';
        $language = new \StoreCore\I18N\AcceptLanguage();
        $this->assertInstanceOf(\StoreCore\Types\LanguageCode::class, $language->negotiate($supported_languages));
        $this->assertInstanceOf(\Stringable::class, $language->negotiate($supported_languages));
        $this->assertEquals('en-GB', (string) $language->negotiate($supported_languages));
    }
}
