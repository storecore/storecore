<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\CRM\Organization;
use StoreCore\Database\Metadata;
use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\Store::class)]
#[UsesClass(\StoreCore\Currency::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class StoreTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Store model class is concrete')]
    public function testStoreModelClassIsConcrete(): void
    {
        $class = new ReflectionClass(Store::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Store model is a StoreCore model')]
    public function testStoreModelIsStoreCoreModel(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $store);
    }

    #[Group('hmvc')]
    #[TestDox('Store model implements StoreCore IdentityInterface')]
    public function testStoreModelImplementsStoreCoreIdentityInterface(): void
    {
        $interface = new ReflectionClass(\StoreCore\IdentityInterface::class);
        $this->assertTrue($interface->isInterface());

        $store = new Store(Registry::getInstance());
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $store);
    }

    #[Group('seo')]
    #[TestDox('Store is JsonSerializable')]
    public function testStoreIsJsonSerializable(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertInstanceOf(\JsonSerializable::class, $store);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Store::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Store::VERSION);
        $this->assertIsString(Store::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION constant matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Store::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Store.dateTimeZone exists')]
    public function testStoreDateTimeZoneExists(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertObjectHasProperty('dateTimeZone', $store);
    }

    #[TestDox('Store.dateTimeZone is public')]
    public function testStoreDateTimeZoneIsPublic(): void
    {
        $property = new ReflectionProperty(Store::class, 'dateTimeZone');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Store.dateTimeZone is DateTimeZone object')]
    public function testStoreDateTimeZoneIsDateTimeZoneObject(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertIsObject($store->dateTimeZone);
        $this->assertInstanceOf(\DateTimeZone::class, $store->dateTimeZone);
    }

    #[Depends('testStoreDateTimeZoneIsDateTimeZoneObject')]
    #[TestDox('Store.dateTimeZone is `UTC` DateTimeZone by default')]
    public function testStoreDateTimeZoneIsUTCDateTimeZoneByDefault(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertEquals('UTC', $store->dateTimeZone->getName());
    }

    /**
     * @see https://stackoverflow.com/questions/11761082/php-cest-as-timezone
     */
    #[Depends('testStoreDateTimeZoneIsDateTimeZoneObject')]
    #[TestDox('Store.dateTimeZone accepts common timezone identifiers')]
    public function testStoreDateTimeZoneAcceptsCommonTimezoneIdentifiers(): void
    {
        $timezone_identifiers = [
            'America/Los_Angeles',
            'America/New_York',

            'Europe/Amsterdam',
            'Europe/Brussels',
            'Europe/Berlin',
            'Europe/London',
            'Europe/Paris',

            'UTC',
        ];

        $store = new Store(Registry::getInstance());
        foreach ($timezone_identifiers as $timezone_identifier) {
            $store->dateTimeZone = new \DateTimeZone($timezone_identifier);
            $this->assertEquals($timezone_identifier, $store->dateTimeZone->getName());
        }
    }


    #[TestDox('Store.identifier exists')]
    public function testStoreIdentifierExists(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertObjectHasProperty('identifier', $store);
    }

    #[TestDox('Store::getIdentifier exists')]
    public function testStoreGetIdentifierExists(): void
    {
        $class = new ReflectionClass(Store::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[TestDox('Store::getIdentifier is public')]
    public function testStoreGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Store::class, 'getIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Store::getIdentifier has no parameters')]
    public function testStoreGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Store::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Store::getIdentifier returns null by default')]
    public function testStoreGetIdentifierReturnsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertNull($store->getIdentifier());
    }


    #[TestDox('Store.metadata exists')]
    public function testStoreMetadataExists(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertObjectHasProperty('metadata', $store);
    }

    #[TestDox('Store.metadata is public')]
    public function testStoreMetadataIsPublic(): void
    {
        $property = new ReflectionProperty(Store::class, 'metadata');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Store.metadata is null by default')]
    public function testStoreMetadataIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertNull($store->metadata);
    }

    #[TestDox('Store.metadata accepts StoreCore database metadata')]
    public function testStoreMetadataAcceptsStoreCoreDatabaseMetadata(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $store->metadata = new Metadata();

        $this->assertNotNull($store->metadata);
        $this->assertInstanceOf(\StoreCore\Database\Metadata::class, $store->metadata);
    }


    #[TestDox('Store.name exists')]
    public function testStoreNameExists(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertObjectHasProperty('name', $store);
    }

    #[TestDox('Store.name is public')]
    public function testStoreNameIsPublic(): void
    {
        $property = new ReflectionProperty(Store::class, 'name');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Store.name is null by default')]
    public function testStoreNameIsNullByDefault(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertNull($store->name);

        $property = new ReflectionProperty(Store::class, 'name');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertEquals(null, $property->getDefaultValue());
    }

    #[Depends('testStoreIsJsonSerializable')]
    #[TestDox('Store.name is set as string')]
    public function testStoreNameIsSetAsString(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $store->name = 'Foo Shop';
        $this->assertNotNull($store->name);
        $this->assertNotEmpty($store->name);
        $this->assertIsString($store->name);
        $this->assertEquals('Foo Shop', $store->name);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "OnlineStore",
              "name": "Foo Shop"
            }
        JSON;
        $this->assertJson($expectedJson);

        $actualJson = json_encode($store, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Depends('testStoreIsJsonSerializable')]
    #[TestDox('Missing OnlineStore.name is set to Organization.name')]
    public function testMissingOnlineStoreNameIsSetToOrganizationName(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertEmpty($store->name);

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "OnlineStore"
            }
        JSON;

        $actualJson = json_encode($store, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        $organization = new Organization();
        $organization->name = 'Enterprise Example Organization';
        $store->organization = $organization;

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "OnlineStore",
              "name": "Enterprise Example Organization"
            }
        JSON;

        $actualJson = json_encode($store, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    #[TestDox('Store::getCurrencies exists')]
    public function testStoreGetCurrenciesExists(): void
    {
        $class = new ReflectionClass(Store::class);
        $this->assertTrue($class->hasMethod('getCurrencies'));
    }

    #[TestDox('Store::getCurrencies is public')]
    public function testStoreGetCurrenciesIsPublic(): void
    {
        $method = new ReflectionMethod(Store::class, 'getCurrencies');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Store::getCurrencies has no parameters')]
    public function testStoreGetCurrenciesHasNoParameters(): void
    {
        $method = new ReflectionMethod(Store::class, 'getCurrencies');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Store::getCurrencies returns non-empty array')]
    public function testStoreGetCurrenciesReturnsNonEmptyArray(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);

        $this->assertNotEmpty($store->getCurrencies());
        $this->assertIsArray($store->getCurrencies());
    }

    #[TestDox('Store::getCurrencies array contains euro currency')]
    public function testStoreGetCurrenciesArrayContainsEuroCurrency(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);

        $this->assertArrayHasKey(978, $store->getCurrencies());
        $this->assertInstanceOf(\StoreCore\Currency::class, $store->getCurrencies()[978]);
    }


    #[TestDox('Store::isDefault exists')]
    public function testStoreIsDefaultExists(): void
    {
        $class = new ReflectionClass(Store::class);
        $this->assertTrue($class->hasMethod('isDefault'));
    }

    #[Depends('testStoreIsDefaultExists')]
    #[TestDox('Store::isDefault is public')]
    public function testStoreIsDefaultIsPublic(): void
    {
        $method = new ReflectionMethod(Store::class, 'isDefault');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('Store::isDefault has one OPTIONAL parameter')]
    public function testStoreIsDefaultHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Store::class, 'isDefault');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Store::isDefault returns (bool) false by default')]
    public function testStoreIsDefaultReturnsBoolFalseByDefault(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertIsBool($store->isDefault());
        $this->assertFalse($store->isDefault());
    }

    #[TestDox('Store::isDefault(true) sets default store')]
    public function testStoreIsDefaultTrueSetsDefaultStore(): void
    {
        $store = new Store(Registry::getInstance());
        $this->assertFalse($store->isDefault());

        $store->isDefault(true);
        $this->assertIsBool($store->isDefault());
        $this->assertTrue($store->isDefault());

        $store->isDefault(false);
        $this->assertFalse($store->isDefault());
    }


    #[TestDox('Store.organization exists')]
    public function testStoreOrganizationExists(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertObjectHasProperty('organization', $store);
    }

    #[Depends('testStoreOrganizationExists')]
    #[TestDox('Store.organization is public')]
    public function testStoreOrganizationIsPublic(): void
    {
        $property = new ReflectionProperty(Store::class, 'organization');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testStoreOrganizationExists')]
    #[Depends('testStoreOrganizationIsPublic')]
    #[TestDox('Store.organization is null by default')]
    public function testStoreOrganizationIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $this->assertEmpty($store->organization);
        $this->assertNull($store->organization);
    }

    #[Depends('testStoreOrganizationIsNullByDefault')]
    #[TestDox('Store.organization accepts an Organization')]
    public function testStoreOrganizationAcceptsOrganization(): void
    {
        $registry = Registry::getInstance();
        $store = new Store($registry);
        $store->organization = new Organization('Example Organization');

        $this->assertNotEmpty($store->organization);
        $this->assertNotNull($store->organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $store->organization);
        $this->assertEquals('Example Organization', $store->organization->name);
    }
}
