<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\View::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\DependencyException::class)]
final class ViewTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('View class exists')]
    public function testViewClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'View.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'View.php');

        $this->assertTrue(class_exists('\\StoreCore\\View'));
        $this->assertTrue(class_exists(View::class));
    }

    #[TestDox('View class is concrete')]
    public function testViewClassIsAConcrete(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('View class is \Stringable')]
    public function testViewClassIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new View());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(View::VERSION);
        $this->assertIsString(View::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(View::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('View::$template exists')]
    public function testViewTemplateExists(): void
    {
        $view = new View();
        $this->assertObjectHasProperty('template', $view);
    }

    #[Depends('testViewTemplateExists')]
    #[TestDox('View::$template is public')]
    public function testViewTemplateIsPublic(): void
    {
        $property = new ReflectionProperty(View::class, 'template');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isProtected());
    }

    #[Depends('testViewTemplateExists')]
    #[TestDox('View::$template is empty by default')]
    public function testViewTemplateIsEmptyByDefault(): void
    {
        $view = new View();
        $this->assertEmpty(
            $view->template,
            'A `View` does require a `template`, so this property MAY be empty.'
        );
    }

    // @see https://www.php.net/releases/8.4/en.php
    #[Depends('testViewTemplateIsPublic')]
    #[TestDox('View::setTemplate no longer exists')]
    public function testViewSetTemplateNoLongerExists(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertFalse(
            $class->hasMethod('setTemplate'),
            'The View::setTemplate setter has been replaced with a property hook.'
        );
    }

    #[Depends('testViewTemplateExists')]
    #[Depends('testViewTemplateIsPublic')]
    #[TestDox('View::$template throws TypeError exception if parameter is not a string')]
    public function testViewTemplateThrowsTypeErrorExceptionIfParameterIsNotString(): void
    {
        $view = new View();
        $this->expectException(\TypeError::class);
        $view->template = 42;
    }

    #[Depends('testViewTemplateThrowsTypeErrorExceptionIfParameterIsNotString')]
    #[TestDox('View::$template throws ValueError exception if string parameter does not end with `.phtml`')]
    public function testViewTemplateThrowsValueErrorExceptionIfStringParameterDoesNotEndWithPhtml(): void
    {
        $view = new View();
        $this->expectException(\ValueError::class);
        $view->template = 'BlackFridaySale.js';
    }

    #[Depends('testViewTemplateThrowsTypeErrorExceptionIfParameterIsNotString')]
    #[TestDox('View::$template throws StoreCore DependencyException if `.phtml` template does not exist')]
    public function testViewTemplateThrowsStoreCoreDependencyExceptionIfPhtmlTemplateDoesNotExist(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'LockScreen.phtml');
        $view = new View();
        $view->template = STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'LockScreen.phtml';
        $this->assertEquals(STORECORE_FILESYSTEM_SRC_DIR . 'Admin' . DIRECTORY_SEPARATOR . 'LockScreen.phtml', $view->template);

        $this->assertFileDoesNotExist('BlackFridaySale.phtml');
        $this->expectException(\StoreCore\DependencyException::class);
        $view = new View();
        $view->template = 'BlackFridaySale.phtml';
    }

    #[Depends('testViewTemplateIsEmptyByDefault')]
    #[Depends('testViewTemplateThrowsTypeErrorExceptionIfParameterIsNotString')]
    #[TestDox('View::$template accepts null or empty string')]
    public function testViewTemplateAcceptsNullOrEmptyString(): void
    {
        $view = new View();
        $view->template = null;
        $this->assertEmpty($view->template);

        $view->template = ' ';
        $this->assertEmpty($view->template);
    }


    #[TestDox('View::__construct exists')]
    public function testViewConstructorExists(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('View::__construct is public constructor')]
    public function testInstallerConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(View::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('View::__construct has two OPTIONAL parameters')]
    public function testViewConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(View::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }



    #[TestDox('View::mapTemplate exists')]
    public function testViewMapTemplateExists(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertTrue($class->hasMethod('mapTemplate'));
    }

    #[Depends('testViewMapTemplateExists')]
    #[TestDox('View::mapTemplate is public')]
    public function testViewMapTemplateIsPublic(): void
    {
        $method = new ReflectionMethod(View::class, 'mapTemplate');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testViewMapTemplateExists')]
    #[TestDox('View::mapTemplate has no parameters')]
    public function testViewMapTemplateHasNoParameters(): void
    {
        $method = new ReflectionMethod(View::class, 'mapTemplate');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('View.data exists')]
    public function testViewDataExists(): void
    {
        $view = new View();
        $this->assertObjectHasProperty('data', $view);
    }

    #[Depends('testViewDataExists')]
    #[TestDox('View.data is protected')]
    public function testViewDataIsProtected(): void
    {
        $property = new ReflectionProperty(View::class, 'data');
        $this->assertFalse($property->isPublic());
        $this->assertTrue($property->isProtected());
    }

    #[Depends('testViewDataExists')]
    #[TestDox('View.data is array')]
    public function testViewDataIsArray(): void
    {
        $property = new ReflectionProperty(View::class, 'data');
        $reflection_type = $property->getType();
        $this->assertSame('array', $reflection_type->getName());
    }

    #[TestDox('View::setValues exists')]
    public function testViewSetValuesExists(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertTrue($class->hasMethod('setValues'));
    }

    #[Depends('testViewSetValuesExists')]
    #[TestDox('View::setValues is public')]
    public function testViewSetValuesIsPublic(): void
    {
        $method = new ReflectionMethod(View::class, 'setValues');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testViewSetValuesExists')]
    #[TestDox('View::setValues has one REQUIRED parameter')]
    public function testViewSetValuesHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(View::class, 'setValues');
        $this->assertEquals(1, $method->getNumberOfParameters());
    }

    #[Depends('testViewSetValuesExists')]
    #[Depends('testViewSetValuesIsPublic')]
    #[Depends('testViewSetValuesHasOneRequiredParameter')]
    #[TestDox('View::setValues accepts key-value array')]
    public function testViewSetValuesAcceptsKeyValueArray(): void
    {
        $view = new View();
        $view->setValues(['title' => 'Example-pet-store.com']);

        $property = new ReflectionProperty($view, 'data');
        $this->assertNotEmpty($property->getValue($view));
        $this->assertIsArray($property->getValue($view));
        $this->assertEquals(['title' => 'Example-pet-store.com'], $property->getValue($view));

        // Add a single key-value pair directly:
        $view->mailto = 'info@example-pet-store.com';
        $this->assertNotEquals(
            ['title' => 'Example-pet-store.com'],
            $property->getValue($view)
        );
        $this->assertEquals(
            [
                'title' => 'Example-pet-store.com',
                'mailto' => 'info@example-pet-store.com',
            ],
            $property->getValue($view)
        );
    }


    #[TestDox('View::render exists')]
    public function testViewRenderExists(): void
    {
        $class = new ReflectionClass(View::class);
        $this->assertTrue($class->hasMethod('render'));
    }

    #[TestDox('View::render is public')]
    public function testViewRenderIsPublic(): void
    {
        $method = new ReflectionMethod(View::class, 'render');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('View::render has no parameters')]
    public function testViewRenderHasNoParameters(): void
    {
        $method = new ReflectionMethod(View::class, 'render');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }
}
