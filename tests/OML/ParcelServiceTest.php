<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('hmvc')]
#[UsesClass(\StoreCore\OML\DeliveryMethod::class)]
final class ParcelServiceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('ParcelService class exists')]
    public function testParcelServiceClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'ParcelService.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'ParcelService.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\ParcelService'));
        $this->assertTrue(class_exists(\StoreCore\OML\ParcelService::class));
    }

    #[TestDox('ParcelService class is concrete')]
    public function testParcelServiceClassIsConcrete(): void
    {
        $class = new ReflectionClass(ParcelService::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[TestDox('ParcelService is a DeliveryMethod')]
    public function testParcelServiceIsDeliveryMethod(): void
    {
        $this->assertInstanceOf(\StoreCore\OML\DeliveryMethod::class, new ParcelService());
    }

    #[TestDox('ParcelService is JSON serializable')]
    public function testParcelServiceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new ParcelService());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ParcelService::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ParcelService::VERSION);
        $this->assertIsString(ParcelService::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ParcelService::VERSION, '1.0.0', '>=')
        );
    }
}
