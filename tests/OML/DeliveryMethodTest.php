<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\DeliveryMethod::class)]
final class DeliveryMethodTest extends TestCase
{
    #[TestDox('DeliveryMethod class is concrete')]
    public function testDeliveryMethodClassIsConcrete(): void
    {
        $class = new ReflectionClass(DeliveryMethod::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('DeliveryMethod is JSON serializable')]
    public function testDeliveryMethodIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new DeliveryMethod());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DeliveryMethod::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DeliveryMethod::VERSION);
        $this->assertIsString(DeliveryMethod::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DeliveryMethod::VERSION, '1.0.0', '>=')
        );
    }
}
