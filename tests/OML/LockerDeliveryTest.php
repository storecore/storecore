<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class LockerDeliveryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('LockerDelivery class is concrete')]
    public function testLockerDeliveryClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'LockerDelivery.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'LockerDelivery.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\LockerDelivery'));
        $this->assertTrue(class_exists(LockerDelivery::class));
    }

    #[TestDox('LockerDelivery class is concrete')]
    public function testLockerDeliveryClassIsConcrete(): void
    {
        $class = new ReflectionClass(LockerDelivery::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LockerDelivery is a DeliveryMethod')]
    public function testLockerDeliveryIsDeliveryMethod(): void
    {
        $this->assertInstanceOf(\StoreCore\OML\DeliveryMethod::class, new LockerDelivery());
    }

    #[Group('hmvc')]
    #[TestDox('LockerDelivery is JSON serializable')]
    public function testLockerDeliveryImplementsJsonSerializableInterface(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new LockerDelivery());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LockerDelivery::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LockerDelivery::VERSION);
        $this->assertIsString(LockerDelivery::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LockerDelivery::VERSION, '1.0.0', '>=')
        );
    }
}
