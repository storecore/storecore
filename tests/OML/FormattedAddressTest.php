<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Geo\Country;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\FormattedAddress::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class FormattedAddressTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('FormattedAddress class exists')]
    public function testFormattedAddressClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'FormattedAddress.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'FormattedAddress.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\FormattedAddress'));
        $this->assertTrue(class_exists(FormattedAddress::class));
    }


    #[Group('hmvc')]
    #[TestDox('FormattedAddress class is concrete')]
    public function testFormattedAddressClassIsConcrete(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FormattedAddress::VERSION);
        $this->assertIsString(FormattedAddress::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FormattedAddress::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('FormattedAddress.addressLocality exists')]
    public function testFormattedAddressAddressLocalityExists(): void
    {
        $this->assertObjectHasProperty('addressLocality', new FormattedAddress());
    }

    #[TestDox('FormattedAddress::getAddressLocality exists')]
    public function testFormattedAddressGetAddressLocalityExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('getAddressLocality'));
    }

    #[Depends('testFormattedAddressGetAddressLocalityExists')]
    #[TestDox('FormattedAddress::getAddressLocality is public')]
    public function testFormattedAddressGetAddressLocalityIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getAddressLocality');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressGetAddressLocalityExists')]
    #[TestDox('FormattedAddress::getAddressLocality has no parameters')]
    public function testFormattedAddressGetAddressLocalityHasNoParameters(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getAddressLocality');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testFormattedAddressGetAddressLocalityExists')]
    #[Depends('testFormattedAddressGetAddressLocalityIsPublic')]
    #[Depends('testFormattedAddressGetAddressLocalityHasNoParameters')]
    #[TestDox('FormattedAddress::getAddressLocality returns null by default')]
    public function testFormattedAddressGetAddressLocalityReturnsNullByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertNull($address->getAddressLocality());
    }

    #[TestDox('FormattedAddress::setAddressLocality exists')]
    public function testFormattedAddressSetAddressLocalityExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setAddressLocality'));
    }

    #[Depends('testFormattedAddressSetAddressLocalityExists')]
    #[TestDox('FormattedAddress::setAddressLocality is public')]
    public function testFormattedAddressSetAddressLocalityIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setAddressLocality');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressSetAddressLocalityExists')]
    #[TestDox('FormattedAddress::setAddressLocality has one REQUIRED parameter')]
    public function testFormattedAddressSetAddressLocalityHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setAddressLocality');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressSetAddressLocalityExists')]
    #[Depends('testFormattedAddressSetAddressLocalityIsPublic')]
    #[Depends('testFormattedAddressSetAddressLocalityHasOneRequiredParameter')]
    #[TestDox('FormattedAddress::setAddressLocality sets locality name as string')]
    public function testFormattedAddressSetAddressLocalitySetsLocalityNameAsString(): void
    {
        $address = new FormattedAddress();
        $this->assertEmpty($address->getAddressLocality());

        $address->setAddressLocality('Seattle');
        $this->assertNotEmpty($address->getAddressLocality());
        $this->assertIsString($address->getAddressLocality());
        $this->assertSame('Seattle', $address->getAddressLocality());
    }


    #[Group('hmvc')]
    #[TestDox('FormattedAddress.countryName exists')]
    public function testFormattedAddressCountryNameExists(): void
    {
        $this->assertObjectHasProperty('countryName', new FormattedAddress());
    }

    #[TestDox('FormattedAddress::getCountryName exists')]
    public function testFormattedAddressGetCountryNameExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('getCountryName'));
    }

    #[Depends('testFormattedAddressGetCountryNameExists')]
    #[TestDox('FormattedAddress::getCountryName is public')]
    public function testFormattedAddressGetCountryNameIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getCountryName');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressGetCountryNameExists')]
    #[TestDox('FormattedAddress::getCountryName has no parameters')]
    public function testFormattedAddressGetCountryNameHasNoParameters(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getCountryName');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testFormattedAddressGetCountryNameExists')]
    #[Depends('testFormattedAddressGetCountryNameIsPublic')]
    #[Depends('testFormattedAddressGetCountryNameHasNoParameters')]
    #[TestDox('FormattedAddress::getCountryName returns null by default')]
    public function testFormattedAddressGetCountryNameReturnsNullByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertNull($address->getCountryName());
    }

    #[TestDox('FormattedAddress::setCountry exists')]
    public function testFormattedAddressSetCountryExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setCountry'));
    }

    #[Depends('testFormattedAddressSetCountryExists')]
    #[TestDox('FormattedAddress::setCountry is public')]
    public function testFormattedAddressSetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setCountry');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressSetCountryExists')]
    #[TestDox('FormattedAddress::setCountry has one REQUIRED parameter')]
    public function testFormattedAddressSetCountryHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setCountry');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('FormattedAddress::setCountryName exists')]
    public function testFormattedAddressSetCountryNameExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setCountryName'));
    }

    #[Depends('testFormattedAddressSetCountryNameExists')]
    #[TestDox('FormattedAddress::setCountryName is public')]
    public function testFormattedAddressSetCountryNameIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setCountryName');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressSetCountryNameExists')]
    #[TestDox('FormattedAddress::setCountryName has one REQUIRED parameter')]
    public function testFormattedAddressSetCountryNameHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setCountryName');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressSetCountryNameExists')]
    #[Depends('testFormattedAddressSetCountryNameIsPublic')]
    #[Depends('testFormattedAddressSetCountryNameHasOneRequiredParameter')]
    #[TestDox('FormattedAddress::getCountryName returns non-empty string if the country name is set')]
    public function testFormattedAddressGetCountryNameReturnsNonEmptyStringIfTheCountryNameIsSet(): void
    {
        $address = new FormattedAddress();
        $this->assertEmpty($address->getCountryName());

        $address->setCountryName('Germany');
        $this->assertNotEmpty($address->getCountryName());
        $this->assertIsString($address->getCountryName());
    }

    #[Depends('testFormattedAddressGetCountryNameReturnsNonEmptyStringIfTheCountryNameIsSet')]
    #[TestDox('FormattedAddress::getCountryName returns country name in uppercase')]
    public function testFormattedAddressGetCountryNameReturnsCountryNameInUppercase(): void
    {
        $address = new FormattedAddress();
        $address->setCountryName('Germany');
        $this->assertNotEquals('Germany', $address->getCountryName());
        $this->assertEquals('GERMANY', $address->getCountryName());
    }

    #[Depends('testFormattedAddressGetCountryNameReturnsCountryNameInUppercase')]
    #[TestDox('FormattedAddress::getCountryName returns country name without accents')]
    public function testFormattedAddressGetCountryNameReturnsCountryNameWithoutAccents(): void
    {
        $address = new FormattedAddress();

        $address->setCountryName('Côte d’Ivoire');
        $this->assertEquals("COTE D'IVOIRE", $address->getCountryName());
        $this->assertNotEquals('CÔTE D’IVOIRE', $address->getCountryName());

        $address->setCountryName('Curaçao');
        $this->assertEquals('CURACAO', $address->getCountryName());
        $this->assertNotEquals('CURAÇAO', $address->getCountryName());

        $address->setCountryName('São Tomé and Principe');
        $this->assertEquals('SAO TOME AND PRINCIPE', $address->getCountryName());
        $this->assertNotEquals('SÃO TOMÉ AND PRINCIPE', $address->getCountryName());
    }

    #[Depends('testFormattedAddressGetCountryNameReturnsCountryNameInUppercase')]
    #[TestDox('FormattedAddress::setCountry sets country name')]
    public function testFormattedAddressSetCountrySetsCountryName(): void
    {
        $address = new FormattedAddress();
        $this->assertNotEquals('GERMANY', $address->getCountryName());

        $country = new Country('DE', 'Germany');
        $address->setCountry($country);
        $this->assertEquals('GERMANY', $address->getCountryName());

        $country = new Country('ST', 'São Tomé and Principe');
        $address->setCountry($country);
        $this->assertEquals('SAO TOME AND PRINCIPE', $address->getCountryName());
    }


    #[TestDox('FormattedAddress::endOfLine exists')]
    public function testFormattedAddressEndOfLineExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('endOfLine'));
    }

    #[Depends('testFormattedAddressEndOfLineExists')]
    #[TestDox('FormattedAddress::endOfLine is public')]
    public function testFormattedAddresssEndOfLineIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'endOfLine');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressEndOfLineExists')]
    #[TestDox('FormattedAddress::endOfLine has one OPTIONAL parameter')]
    public function testFormattedAddressEndOfLineHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'endOfLine');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressEndOfLineHasOneOptionalParameter')]
    #[TestDox('FormattedAddress::endOfLine returns non-empty string')]
    public function testFormattedAddressEndOfLineReturnsNonEmptyString(): void
    {
        $address = new FormattedAddress();
        $this->assertNotEmpty($address->endOfLine());
        $this->assertIsString($address->endOfLine());
    }

    #[Depends('testFormattedAddressEndOfLineReturnsNonEmptyString')]
    #[TestDox('FormattedAddress::endOfLine returns PHP_EOL constant by default')]
    public function testFormattedAddressEndOfLineReturnsPhpEolConstantByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertSame(\PHP_EOL, $address->endOfLine());
    }

    #[Depends('testFormattedAddressEndOfLineHasOneOptionalParameter')]
    #[TestDox('FormattedAddress::endOfLine sets end of line characters')]
    public function testFormattedAddressEndOfLineSetsEndOfLineCharacters(): void
    {
        $address = new FormattedAddress();

        $address->endOfLine("\r\n");
        $this->assertSame("\r\n", $address->endOfLine());

        $this->assertSame("\n", $address->endOfLine("\n"));
        $this->assertSame("\n", $address->endOfLine());
    }

    #[Depends('testFormattedAddressEndOfLineHasOneOptionalParameter')]
    #[Depends('testFormattedAddressEndOfLineReturnsNonEmptyString')]
    #[TestDox('FormattedAddress::endOfLine accepts HTML tags')]
    public function testFormattedAddressEndOfLineAcceptsHtmlTags(): void
    {
        $address = new FormattedAddress();

        $address->endOfLine('<br>');
        $this->assertSame('<br>', $address->endOfLine());

        $address->endOfLine('<br />');
        $this->assertSame('<br />', $address->endOfLine());
    }


    #[TestDox('FormattedAddress::getName exists')]
    public function testFormattedAddressGetNameExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('getName'));
    }

    #[Depends('testFormattedAddressGetNameExists')]
    #[TestDox('FormattedAddress::getName is public')]
    public function testFormattedAddressGetNameIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getName');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressGetNameExists')]
    #[TestDox('FormattedAddress::getName has no parameters')]
    public function testFormattedAddressGetNameHasNoParameters(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getName');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testFormattedAddressGetNameExists')]
    #[Depends('testFormattedAddressGetNameIsPublic')]
    #[Depends('testFormattedAddressGetNameHasNoParameters')]
    #[TestDox('FormattedAddress::getName returns null by default')]
    public function testFormattedAddressGetNameReturnsNullByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertNull($address->getName());
    }

    #[TestDox('FormattedAddress::setName exists')]
    public function testFormattedAddressSetNameExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setName'));
    }

    #[Depends('testFormattedAddressSetNameExists')]
    #[TestDox('FormattedAddress::setName is public')]
    public function testFormattedAddressSetNameIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setName');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressSetNameExists')]
    #[TestDox('FormattedAddress::setName has one REQUIRED parameter')]
    public function testFormattedAddressSetNameHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setName');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressGetNameReturnsNullByDefault')]
    #[Depends('testFormattedAddressSetNameExists')]
    #[Depends('testFormattedAddressSetNameIsPublic')]
    #[Depends('testFormattedAddressSetNameHasOneRequiredParameter')]
    #[TestDox('FormattedAddress::setName sets addressee name')]
    public function testFormattedAddressSetNameSetsAddresseeName(): void
    {
        $address = new FormattedAddress();
        $this->assertEmpty($address->getName());

        $address->setName('Jane Roe');
        $this->assertNotEmpty($address->getName());
        $this->assertIsString($address->getName());
        $this->assertEquals('Jane Roe', $address->getName());
    }


    #[TestDox('FormattedAddress.postalCode exists')]
    public function testFormattedAddressPostalCodeExists(): void
    {
        $this->assertObjectHasProperty('postalCode', new FormattedAddress());
    }

    #[TestDox('FormattedAddress::getPostalCode exists')]
    public function testFormattedAddressGetPostalCodeExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('getPostalCode'));
    }

    #[Depends('testFormattedAddressGetPostalCodeExists')]
    #[TestDox('FormattedAddress::getPostalCode is public')]
    public function testFormattedAddressGetPostalCodeIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getPostalCode');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressGetPostalCodeExists')]
    #[TestDox('FormattedAddress::getPostalCode has no parameters')]
    public function testFormattedAddressGetPostalCodeHasNoParameters(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getPostalCode');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testFormattedAddressGetPostalCodeExists')]
    #[Depends('testFormattedAddressGetPostalCodeIsPublic')]
    #[Depends('testFormattedAddressGetPostalCodeHasNoParameters')]
    #[TestDox('FormattedAddress::getPostalCode returns null by default')]
    public function testFormattedAddressGetPostalCodeReturnsNullByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertNull($address->getPostalCode());
    }

    #[TestDox('FormattedAddress::setPostalCode exists')]
    public function testFormattedAddressSetPostalCodeExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setPostalCode'));
    }

    #[Depends('testFormattedAddressSetPostalCodeExists')]
    #[TestDox('FormattedAddress::setPostalCode is public')]
    public function testFormattedAddressSetPostalCodeIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setPostalCode');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testFormattedAddressSetPostalCodeExists')]
    #[TestDox('FormattedAddress::setPostalCode has one REQUIRED parameter')]
    public function testFormattedAddressSetPostalCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setPostalCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressGetPostalCodeReturnsNullByDefault')]
    #[Depends('testFormattedAddressSetPostalCodeExists')]
    #[Depends('testFormattedAddressSetPostalCodeIsPublic')]
    #[Depends('testFormattedAddressSetPostalCodeHasOneRequiredParameter')]
    #[TestDox('FormattedAddress::setPostalCode sets postal code as string')]
    public function testFormattedAddressSetPostalCodeSetsPostalCodeAsString(): void
    {
        $address = new FormattedAddress();
        $this->assertEmpty($address->getPostalCode());

        $address->setPostalCode('94043');
        $this->assertNotEmpty($address->getPostalCode());
        $this->assertIsString($address->getPostalCode());
        $this->assertSame('94043', $address->getPostalCode());
    }

    #[Depends('testFormattedAddressSetPostalCodeSetsPostalCodeAsString')]
    #[TestDox('FormattedAddress::setPostalCode accepts postal code as integer')]
    public function testFormattedAddressSetPostalCodeAcceptsPostalCodeAsInteger(): void
    {
        $postal_code = 94043;
        $address = new FormattedAddress();
        $address->setPostalCode($postal_code);

        $this->assertIsNotString($postal_code);
        $this->assertIsInt($postal_code);
        $this->assertIsString($address->getPostalCode());

        $this->assertEquals($postal_code, $address->getPostalCode());
        $this->assertNotSame($postal_code, $address->getPostalCode());
        $this->assertSame((string)$postal_code, $address->getPostalCode());
    }


    #[TestDox('FormattedAddress::getStreetAddress exists')]
    public function testFormattedAddressGetStreetAddressExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('getStreetAddress'));
    }

    #[Depends('testFormattedAddressGetStreetAddressExists')]
    #[TestDox('FormattedAddress::getStreetAddress is public')]
    public function testFormattedAddressGetStreetAddressIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getStreetAddress');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testFormattedAddressGetStreetAddressExists')]
    #[TestDox('FormattedAddress::getStreetAddress has no parameters')]
    public function testFormattedAddressGetStreetAddressHasNoParameters(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'getStreetAddress');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testFormattedAddressGetStreetAddressExists')]
    #[Depends('testFormattedAddressGetStreetAddressIsPublic')]
    #[Depends('testFormattedAddressGetStreetAddressHasNoParameters')]
    #[TestDox('FormattedAddress::getStreetAddress returns null by default')]
    public function testFormattedAddressGetStreetAddressReturnsNullByDefault(): void
    {
        $address = new FormattedAddress();
        $this->assertNull($address->getStreetAddress());
    }

    #[TestDox('FormattedAddress::setStreetAddress exists')]
    public function testFormattedAddressSetStreetAddressExists(): void
    {
        $class = new ReflectionClass(FormattedAddress::class);
        $this->assertTrue($class->hasMethod('setStreetAddress'));
    }

    #[Depends('testFormattedAddressSetStreetAddressExists')]
    #[TestDox('FormattedAddress::setStreetAddress is public')]
    public function testFormattedAddressSetStreetAddressIsPublic(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setStreetAddress');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testFormattedAddressSetStreetAddressExists')]
    #[TestDox('FormattedAddress::setStreetAddress has one REQUIRED parameter')]
    public function testFormattedAddressSetStreetAddressHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(FormattedAddress::class, 'setStreetAddress');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testFormattedAddressGetStreetAddressReturnsNullByDefault')]
    #[Depends('testFormattedAddressSetStreetAddressExists')]
    #[Depends('testFormattedAddressSetStreetAddressIsPublic')]
    #[Depends('testFormattedAddressSetStreetAddressHasOneRequiredParameter')]
    #[TestDox('FormattedAddress::setStreetAddress sets street address as string')]
    public function testFormattedAddressSetStreetAddressSetsStreetAddressAsString(): void
    {
        $address = new FormattedAddress();
        $this->assertEmpty($address->getStreetAddress());

        $address->setStreetAddress('1600 Amphitheatre Pkwy.');
        $this->assertNotEmpty($address->getStreetAddress());
        $this->assertIsString($address->getStreetAddress());
        $this->assertEquals('1600 Amphitheatre Pkwy.', $address->getStreetAddress());
    }
}
