<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;
use StoreCore\Order;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\OrderFactory::class)]
#[CoversClass(\StoreCore\Order::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Database\OrderMapper::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrderFactoryTest extends TestCase
{
    public function setUp(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');
            $db->query('SELECT 1 FROM `sc_order_status`');
            $db->query('SELECT 1 FROM `sc_orders`');
            $db->exec('DELETE FROM `sc_orders` WHERE `order_number` = 456789 LIMIT 1');
            $db->exec("DELETE FROM `sc_orders` WHERE `order_number` = '405-8590108-7803546' LIMIT 1");
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[Group('hmvc')]
    #[TestDox('OrderFactory class is concrete')]
    public function testOrderFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderFactory is a model')]
    public function testOrderFactoryIsModel(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderFactory::VERSION);
        $this->assertIsString(OrderFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderFactory::createCart exists')]
    public function testOrderFactoryCreateCartExists(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertTrue($class->hasMethod('createCart'));
    }

    #[Depends('testOrderFactoryCreateCartExists')]
    #[TestDox('OrderFactory::createCart is public')]
    public function testOrderFactoryCreateCartIsPublic(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createCart');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderFactoryCreateCartExists')]
    #[TestDox('OrderFactory::createCart has one OPTIONAL parameter')]
    public function testOrderFactoryCreateCartHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createCart');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('OrderFactory::createOrder exists')]
    public function testOrderFactoryCreateOrderExists(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertTrue($class->hasMethod('createOrder'));
    }

    #[Depends('testOrderFactoryCreateOrderExists')]
    #[TestDox('OrderFactory::createOrder is public')]
    public function testOrderFactoryCreateOrderIsPublic(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createOrder');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderFactoryCreateOrderExists')]
    #[TestDox('OrderFactory::createOrder has two OPTIONAL parameters')]
    public function testOrderFactoryCreateOrderHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createOrder');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderFactoryCreateOrderExists')]
    #[Depends('testOrderFactoryCreateOrderIsPublic')]
    #[TestDox('OrderFactory::createOrder returns Order object')]
    public function testOrderFactoryCreateOrderReturnsOrderObject(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $result = $factory->createOrder();

        $this->assertIsObject($result);
        $this->assertInstanceOf(Order::class, $result);
    }

    #[Depends('testOrderFactoryCreateOrderReturnsOrderObject')]
    #[TestDox('OrderFactory::createOrder returns Order with UUID')]
    public function testOrderFactoryCreateOrderReturnsOrderWithUUID(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $result = $factory->createOrder();

        $this->assertIsObject($result);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $result);
        $this->assertTrue($result->hasIdentifier());
    }

    #[Depends('testOrderFactoryCreateOrderHasTwoOptionalParameters')]
    #[Depends('testOrderFactoryCreateOrderReturnsOrderObject')]
    #[TestDox('OrderFactory::createOrder accepts OPTIONAL order number')]
    public function testOrderFactoryCreateOrderAcceptsOptionalOrderNumber(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $order = $factory->createOrder();
        $this->assertEmpty($order->orderNumber);

        // Order.orderNumber as integer
        $order = $factory->createOrder(null, 456789);
        $this->assertNotEmpty($order->orderNumber);
        $this->assertIsInt($order->orderNumber);
        $this->assertEquals(456789, $order->orderNumber);

        // Order.orderNumber as string
        $order = $factory->createOrder(null, '405-8590108-7803546');
        $this->assertNotEmpty($order->orderNumber);
        $this->assertIsString($order->orderNumber);
        $this->assertEquals('405-8590108-7803546', $order->orderNumber);
    }


    #[TestDox('OrderFactory::createWishList exists')]
    public function testOrderFactoryCreateWishListExists(): void
    {
        $class = new ReflectionClass(OrderFactory::class);
        $this->assertTrue($class->hasMethod('createWishList'));
    }

    #[Depends('testOrderFactoryCreateWishListExists')]
    #[TestDox('OrderFactory::createWishList is public')]
    public function testOrderFactoryCreateWishListIsPublic(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createWishList');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderFactoryCreateWishListExists')]
    #[TestDox('OrderFactory::createWishList has no parameters')]
    public function testOrderFactoryCreateWishListHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderFactory::class, 'createWishList');
        $this->assertEquals(0, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }
}
