<?php

declare(strict_types=1);

namespace StoreCore\OML;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class ProviderInterfaceTest extends TestCase
{
    #[TestDox('ProviderInterface interface file exists')]
    public function testProviderInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'ProviderInterface.php'
        );
    }

    #[Depends('testProviderInterfaceInterfaceFileExists')]
    #[TestDox('ProviderInterface interface file exists')]
    public function testProviderInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'ProviderInterface.php'
        );
    }

    #[Depends('testProviderInterfaceInterfaceFileExists')]
    #[Depends('testProviderInterfaceInterfaceFileIsReadable')]
    #[TestDox('ProviderInterface interface exists')]
    public function testProviderInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\OML\\ProviderInterface'));
        $this->assertTrue(interface_exists(ProviderInterface::class));
    }
}
