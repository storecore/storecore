<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Order;
use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\PIM\Product;
use StoreCore\Types\UUID;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \json_encode;
use function \version_compare;

use const \JSON_PRETTY_PRINT;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\OML\AbstractParcelDelivery::class)]
#[CoversClass(\StoreCore\OML\ParcelDelivery::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Order::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Proxy::class)]
#[UsesClass(\StoreCore\ProxyFactory::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\OrganizationProxy::class)]
#[UsesClass(\StoreCore\CRM\OrganizationRepository::class)]
#[UsesClass(\StoreCore\CRM\Parties::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Database\PersonRepository::class)]
#[UsesClass(\StoreCore\OML\Provider::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class ParcelDeliveryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('ParcelDelivery is concrete')]
    public function testParcelDeliveryIsConcrete(): void
    {
        $class = new ReflectionClass(ParcelDelivery::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ParcelDelivery is JSON serializable')]
    public function testParcelDeliveryIsJsonSerializable(): void
    {
        $class = new ReflectionClass(ParcelDelivery::class);
        $this->assertTrue($class->implementsInterface(\JsonSerializable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ParcelDelivery::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ParcelDelivery::VERSION);
        $this->assertIsString(ParcelDelivery::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ParcelDelivery::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('ParcelDelivery.expectedArrivalFrom exists')]
    public function testParcelDeliveryExpectedArrivalFromExists(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertObjectHasProperty('expectedArrivalFrom', $parcelDelivery);
    }

    #[Depends('testParcelDeliveryExpectedArrivalFromExists')]
    #[TestDox('ParcelDelivery.expectedArrivalFrom is null by default')]
    public function testParcelDeliveryExpectedArrivalFromIsNullByDefault()
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertNull($parcelDelivery->expectedArrivalFrom);
    }


    #[TestDox('ParcelDelivery.expectedArrivalUntil exists')]
    public function testParcelDeliveryExpectedArrivalUntilExists(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertObjectHasProperty('expectedArrivalUntil', $parcelDelivery);
    }

    #[Depends('testParcelDeliveryExpectedArrivalUntilExists')]
    #[TestDox('ParcelDelivery.expectedArrivalUntil is null by default')]
    public function testParcelDeliveryExpectedArrivalUntilIsNullByDefault(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertNull($parcelDelivery->expectedArrivalUntil);
    }


    #[TestDox('ParcelDelivery.provider exists')]
    public function testParcelDeliveryProviderExists(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertObjectHasProperty('provider', $parcelDelivery);
    }

    #[Depends('testParcelDeliveryProviderExists')]
    #[TestDox('ParcelDelivery.provider is null by default')]
    public function testParcelDeliveryProviderIsNullByDefault(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertNull($parcelDelivery->provider);
    }

    /**
     * @see https://developers.google.com/gmail/markup/reference/parcel-delivery#json-ld
     */
    #[TestDox('ParcelDelivery.provider accepts Provider Organization')]
    public function testParcelDeliveryProviderAcceptsProviderOrganization(): void
    {
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "ParcelDelivery",
              "provider": {
                "@type": "Organization",
                "name": "FedEx",
                "url": "https://fedex.com/"
              }
            }
        ';

        $parcelDelivery = new ParcelDelivery();
        $parcelDelivery->provider = new Provider('FedEx', 'https://fedex.com/');

        $this->assertNotNull($parcelDelivery->provider);
        $this->assertIsObject($parcelDelivery->provider);
        $this->assertInstanceOf(\JsonSerializable::class, $parcelDelivery->provider);

        $actualJson = json_encode($parcelDelivery, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertNotFalse($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    /**
     * @see https://schema.org/carrier
     *   In Schema.org `carrier` is an out-dated term indicating the `provider`
     *   for parcel delivery (and flights).
     */
    #[Depends('testParcelDeliveryProviderAcceptsProviderOrganization')]
    #[TestDox('ParcelDelivery.provider supersedes ParcelDelivery.carrier')]
    public function testParcelDeliveryProviderSupersedesParcelDeliveryCarrier(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $parcelDelivery->carrier = new Provider('FedEx', 'https://fedex.com/');
        $this->assertSame('FedEx', $parcelDelivery->provider->getName());
        $this->assertSame('https://fedex.com/', $parcelDelivery->provider->getUrl());
    }


    #[TestDox('ParcelDelivery.trackingNumber exists')]
    public function testParcelDeliveryTrackingNumberExists(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertObjectHasProperty('trackingNumber', $parcelDelivery);
    }

    #[Depends('testParcelDeliveryTrackingNumberExists')]
    #[TestDox('ParcelDelivery.trackingNumber is empty by default')]
    public function testParcelDeliveryTrackingNumberIsEmptyByDefault(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $this->assertNotNull($parcelDelivery->trackingNumber);
        $this->assertEmpty($parcelDelivery->trackingNumber);
    }

    #[Depends('testParcelDeliveryTrackingNumberIsEmptyByDefault')]
    #[TestDox('ParcelDelivery.trackingNumber accepts string')]
    public function testParcelDeliveryTrackingNumberAcceptsString(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $parcelDelivery->trackingNumber = '3453291231';
        $this->assertNotNull($parcelDelivery->trackingNumber);
        $this->assertIsString($parcelDelivery->trackingNumber);
        $this->assertEquals('3453291231', $parcelDelivery->trackingNumber);
    }

    #[Depends('testParcelDeliveryTrackingNumberIsEmptyByDefault')]
    #[TestDox('ParcelDelivery.trackingNumber accepts integer but returns string')]
    public function testParcelDeliveryTrackingNumberAcceptsIntegerButReturnsString(): void
    {
        $parcelDelivery = new ParcelDelivery();
        $parcelDelivery->trackingNumber = (int) 3453291231;
        $this->assertNotNull($parcelDelivery->trackingNumber);
        $this->assertIsNotInt($parcelDelivery->trackingNumber);
        $this->assertIsString($parcelDelivery->trackingNumber);
        $this->assertSame('3453291231', $parcelDelivery->trackingNumber);
    }


    /**
     * @see https://developers.google.com/gmail/markup/reference/types/ParcelDelivery
     */
    #[TestDox('Google Workspace Gmail required properties acceptance test')]
    public function testGoogleWorkspaceGmailRequiredPropertiesAcceptanceTest(): void
    {
        $parcelDelivery = new ParcelDelivery();

        // ParcelDelivery.carrier
        $parcelDelivery->carrier = new Provider('FedEx');
        $this->assertIsObject($parcelDelivery->carrier);
        $this->assertInstanceOf(\JsonSerializable::class, $parcelDelivery->carrier);

        // ParcelDelivery.carrier.name
        $this->assertNotEmpty($parcelDelivery->carrier->name);
        $this->assertIsString($parcelDelivery->carrier->name);
        $this->assertSame('FedEx', $parcelDelivery->carrier->name);
    }


    /**
     * @see https://developers.google.com/gmail/markup/reference/parcel-delivery#basic_parcel_delivery
     *      Basic Parcel Delivery, Use cases
     */
    #[TestDox('Google Workspace minimal acceptance test')]
    public function testGoogleWorkspaceMinimalAcceptanceTest(): void
    {
        /* ```json
         * {
         *   "@context": "http://schema.org",
         *   "@type": "ParcelDelivery",
         *   "deliveryAddress": {
         *     "@type": "PostalAddress",
         *     "name": "Pickup Corner",
         *     "streetAddress": "24 Willie Mays Plaza",
         *     "addressLocality": "San Francisco",
         *     "addressRegion": "CA",
         *     "addressCountry": "US",
         *     "postalCode": "94107"
         *   },
         *   "expectedArrivalUntil": "2027-03-12T12:00:00-08:00",
         *   "provider": {
         *     "@type": "Organization",
         *     "name": "FedEx"
         *   },
         *   "itemShipped": {
         *     "@type": "Product",
         *     "name": "Google Chromecast"
         *   },
         *   "partOfOrder": {
         *     "@type": "Order",
         *     "orderNumber": "176057",
         *     "merchant": {
         *       "@type": "Organization",
         *       "name": "Bob Dole"
         *     }
         *   }
         * }
         * ```
         */

        $parcelDelivery = new ParcelDelivery();

        $parcelDelivery->provider = new Provider('FedEx');
        $this->assertEquals('FedEx', $parcelDelivery->provider->name);

        $parcelDelivery->itemShipped = new Product('Google Chromecast');
        $this->assertEquals('Google Chromecast', $parcelDelivery->itemShipped->name);

        $registry = Registry::getInstance();
        $order = new Order($registry);
        $order->orderNumber = '176057';

        $merchant = new Organization();
        $merchant->identifier = new UUID('41bd467a-48e0-4a4d-b344-0eea5b4024b7');
        $merchant->name = 'Bob Dole';
        $order->merchant = $merchant;

        // `seller` supersedes `mechant` and `vendor`
        $this->assertTrue($order->merchant->hasIdentifier());
        $this->assertEquals($order->seller->getIdentifier(), $order->merchant->getIdentifier());

        $parcelDelivery->partOfOrder = $order;

        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "ParcelDelivery",
              "itemShipped": {
                "@type": "Product",
                "itemCondition": "https://schema.org/NewCondition",
                "name": "Google Chromecast"
              },
              "partOfOrder": {
                "@type": "Order",
                "orderNumber": "176057",
                "seller": {
                  "@id": "/api/v1/organizations/41bd467a-48e0-4a4d-b344-0eea5b4024b7",
                  "@type": "Organization",
                  "identifier": "41bd467a-48e0-4a4d-b344-0eea5b4024b7",
                  "name": "Bob Dole"
                }
              },
              "provider": {
                "@type": "Organization",
                "name": "FedEx"
              }
            }
        JSON;
        $this->assertJson($expectedJson);

        $actualJson = json_encode($parcelDelivery, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
