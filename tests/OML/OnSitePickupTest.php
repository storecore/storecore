<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
final class OnSitePickupTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OnSitePickup class exists')]
    public function testOnSitePickupClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'OnSitePickup.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'OnSitePickup.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\OnSitePickup'));
        $this->assertTrue(class_exists(OnSitePickup::class));
    }

    #[Group('hmvc')]
    #[TestDox('OnSitePickup class is concrete')]
    public function testOnSitePickupClassIsConcrete(): void
    {
        $class = new ReflectionClass(OnSitePickup::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OnSitePickup is a DeliveryMethod')]
    public function testOnSitePickupIsDeliveryMethod(): void
    {
        $this->assertInstanceOf(\StoreCore\OML\DeliveryMethod::class, new OnSitePickup());
    }

    #[Group('hmvc')]
    #[TestDox('OnSitePickup is JSON serializable')]
    public function testOnSitePickupIsJsonSerializable(): void
    {
        $object = new OnSitePickup();
        $this->assertInstanceOf(\JsonSerializable::class, $object);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OnSitePickup::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OnSitePickup::VERSION);
        $this->assertIsString(OnSitePickup::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OnSitePickup::VERSION, '1.0.0', '>=')
        );
    }
}
