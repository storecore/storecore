<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionEnum, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\OrderStatus::class)]
final class OrderStatusTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('OrderStatus class exists')]
    public function testOrderStatusClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'OrderStatus.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'OrderStatus.php');

        $this->assertTrue(enum_exists('\\StoreCore\\OML\\OrderStatus'));
        $this->assertTrue(enum_exists(OrderStatus::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionEnum(OrderStatus::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderStatus::VERSION);
        $this->assertIsString(OrderStatus::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches Schema.org v26.0')]
    public function testVersionMatchesSchemaOrgVersion26(): void
    {
        $this->assertTrue(
            version_compare(OrderStatus::VERSION, '26.0', '>=')
        );
    }


    #[TestDox('OrderStatus enumeration has eight members')]
    public function testOrderStatusEnumerationHasEightMembers(): void
    {
        $this->assertCount(8, OrderStatus::cases());
    }

    #[Group('seo')]
    #[TestDox('OrderStatus enumeration values are Schema.org URLs')]
    public function testOrderStatusEnumerationValuesAreSchemaOrgUrls(): void
    {
        foreach (OrderStatus::cases() as $case) {
            $this->assertNotEmpty($case->value);
            $this->assertIsString($case->value);
            $this->assertSame(filter_var($case->value, \FILTER_VALIDATE_URL), $case->value);
            $this->assertStringStartsWith('https://schema.org/', $case->value);
        }
    }

    #[TestDox('OrderStatus::OrderCancelled exists')]
    public function testOrderStatusOrderCancelledExists(): void
    {
        $this->assertEquals('https://schema.org/OrderCancelled', OrderStatus::OrderCancelled->value);
    }

    #[TestDox('OrderStatus::OrderProblem exists')]
    public function testOrderStatusOrderProblemExists(): void
    {
        $this->assertEquals('https://schema.org/OrderProblem', OrderStatus::OrderProblem->value);
    }


    // OrderStatus::fromInteger

    #[TestDox('OrderStatus::fromInteger() exists')]
    public function testOrderStatusFromIntegerExists(): void
    {
        $class = new ReflectionEnum(OrderStatus::class);
        $this->assertTrue($class->hasMethod('fromInteger'));
    }

    #[Depends('testOrderStatusFromIntegerExists')]
    #[TestDox('OrderStatus::fromInteger() is final public static')]
    public function testOrderStatusFromIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(OrderStatus::class, 'fromInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testOrderStatusFromIntegerExists')]
    #[TestDox('OrderStatus::fromInteger() has one REQUIRED parameter')]
    public function testOrderStatusFromIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderStatus::class, 'fromInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderStatusFromIntegerExists')]
    #[Depends('testOrderStatusFromIntegerIsFinalPublicStatic')]
    #[Depends('testOrderStatusFromIntegerHasOneRequiredParameter')]
    #[TestDox('OrderStatus::fromInteger() accepts integer and returns OrderStatus')]
    public function testOrderStatusFromIntegerAcceptsIntegerAndReturnsOrderStatus(): void
    {
        $int = random_int(1, 8);
        $order_status = OrderStatus::fromInteger($int);
        $this->assertInstanceOf(OrderStatus::class, $order_status);
    }

    #[Depends('testOrderStatusFromIntegerAcceptsIntegerAndReturnsOrderStatus')]
    #[TestDox('OrderStatus::fromInteger() accepts integers from (int) 1')]
    public function testOrderStatusFromIntegerAcceptsIntegersFromInt1(): void
    {
        $order_status = OrderStatus::fromInteger(1);
        $this->assertInstanceOf(OrderStatus::class, $order_status);

        $this->expectException(\ValueError::class);
        $failure = OrderStatus::fromInteger(0);
    }

    #[Depends('testOrderStatusFromIntegerAcceptsIntegerAndReturnsOrderStatus')]
    #[TestDox('OrderStatus::fromInteger() accepts integers up to and including (int) 8')]
    public function testOrderStatusFromIntegerAcceptsIntegersUpToAndIncludingInt8(): void
    {
        $order_status = OrderStatus::fromInteger(8);
        $this->assertInstanceOf(OrderStatus::class, $order_status);

        $this->expectException(\ValueError::class);
        $failure = OrderStatus::fromInteger(9);
    }


    // OrderStatus::toInteger

    #[TestDox('OrderStatus::toInteger() exists')]
    public function testOrderStatusToIntegerExists(): void
    {
        $class = new ReflectionEnum(OrderStatus::class);
        $this->assertTrue($class->hasMethod('toInteger'));
    }

    #[Depends('testOrderStatusToIntegerExists')]
    #[TestDox('OrderStatus::toInteger() is final public static')]
    public function testOrderStatusToIntegerIsFinalPublicStatic(): void
    {
        $method = new ReflectionMethod(OrderStatus::class, 'toInteger');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testOrderStatusToIntegerExists')]
    #[TestDox('OrderStatus::toInteger() has one REQUIRED parameter')]
    public function testOrderStatusToIntegerHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderStatus::class, 'toInteger');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderStatusToIntegerExists')]
    #[Depends('testOrderStatusToIntegerIsFinalPublicStatic')]
    #[Depends('testOrderStatusToIntegerHasOneRequiredParameter')]
    #[TestDox('OrderStatus::toInteger() accepts OrderStatus and returns integer')]
    public function testOrderStatusToIntegerAcceptsOrderStatusAndReturnsInteger(): void
    {
        $order_status = OrderStatus::OrderCancelled;
        $this->assertInstanceOf(\StoreCore\OML\OrderStatus::class, $order_status);

        $order_status_identifier = OrderStatus::toInteger($order_status);
        $this->assertIsInt($order_status_identifier);
        $this->assertGreaterThan(0, $order_status_identifier);
    }
}
