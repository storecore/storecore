<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\CartID::class)]
#[CoversClass(\StoreCore\OML\CartRepository::class)]
#[CoversClass(\StoreCore\OML\OrderRepository::class)]
#[UsesClass(\StoreCore\Registry::class)]
final class CartRepositoryTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CartRepository is a database model')]
    public function testCartRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('hmvc')]
    #[TestDox('CartRepository is an OrderRepository')]
    public function testCartRepositoryIsOrderRepository(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\OML\OrderRepository::class));
    }

    #[Group('hmvc')]
    #[TestDox('CartRepository implements PSR-11 ContainerInterface')]
    public function testCartRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CartRepository::VERSION);
        $this->assertIsString(CartRepository::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CartRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CartRepository::get exists')]
    public function testCartRepositoryGetExists(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('CartRepository::get is public')]
    public function testCartRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(CartRepository::class, 'get');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartRepository::get has one REQUIRED parameter')]
    public function testCartRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CartRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CartRepository::has exists')]
    public function testCartRepositoryHasExists(): void
    {
        $class = new ReflectionClass(CartRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('CartRepository::has is public')]
    public function testCartRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(CartRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('CartRepository::has has one REQUIRED parameter')]
    public function testCartRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CartRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('CartRepository::get throws PSR-11 NotFoundExceptionInterface on invalid ID')]
    public function testCartRepositoryGetThrowsPSR11NotFoundExceptionInterfaceOnInvalidID(): void
    {
        $registry = Registry::getInstance();
        $repository = new CartRepository($registry);
        $this->assertFalse($repository->has('42'));

        $this->assertTrue(interface_exists('\\Psr\\Container\\NotFoundExceptionInterface'));
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $failure = $repository->get('42');
    }
}
