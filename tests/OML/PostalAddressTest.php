<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;
use StoreCore\Geo\State;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\PostalAddress::class)]
#[CoversClass(\StoreCore\OML\StreetAddress::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\CountryRepository::class)]
#[UsesClass(\StoreCore\Geo\State::class)]
#[UsesClass(\StoreCore\Geo\StateRepository::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class PostalAddressTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PostalAddress class exists')]
    public function testPostalAddressClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalAddress.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalAddress.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\PostalAddress'));
        $this->assertTrue(class_exists(PostalAddress::class));
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress class is concrete')]
    public function testPostalAddressClassIsConcrete(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress extends StoreCore\Geo\PostalAddress')]
    public function testPostalAddressExtendsStoreCoreGeoPostalAddress(): void
    {
        $this->assertInstanceOf(\StoreCore\Geo\PostalAddress::class, new PostalAddress());
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress is JSON serializable')]
    public function testPostalAddressIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new PostalAddress());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PostalAddress::VERSION);
        $this->assertIsString(PostalAddress::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PostalAddress::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('PostalAddress::getAddressRegion exists')]
    public function testPostalAddressGetAddressRegionExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('getAddressRegion'));
    }

    #[TestDox('PostalAddress::getAddressRegion is public')]
    public function testPostalAddressGetAddressRegionIsPublic()
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getAddressRegion');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::getAddressRegion has no parameters')]
    public function testPostalAddressGetAddressRegionHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getAddressRegion');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('PostalAddress::setAddressRegion() exists')]
    public function testPostalAddressSetAddressRegionExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('setAddressRegion'));
    }

    #[TestDox('PostalAddress::setAddressRegion() is public')]
    public function testPostalAddressSetAddressRegionIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setAddressRegion');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::setAddressRegion() has one REQUIRED parameter')]
    public function testPostalAddressSetAddressRegionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setAddressRegion');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress::setAddressRegion() accepts State value object')]
    public function testPostalAddressSetAddressRegionAcceptsStateValueObject(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $postalAddress = new PostalAddress();
        $postalAddress->setCountry('US');
        $this->assertFalse($postalAddress->has('addressRegion'));

        $state = new State('US-MT', 'Montana');
        $postalAddress->setAddressRegion($state);
        $this->assertTrue($postalAddress->has('addressRegion'));
    }


    #[TestDox('PostalAddress.streetAddress exists')]
    public function testPostalAddressStreetAddressExists(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertObjectHasProperty('streetAddress', $postalAddress);
    }

    #[TestDox('PostalAddress.streetAddress is null by default')]
    public function testPostalAddressStreetAddressIsNullByDefault(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertNull($postalAddress->streetAddress);
    }

    #[TestDox('PostalAddress::getStreetAddress() exists')]
    public function testPostalAddressGetStreetAddressExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('getStreetAddress'));
    }

    #[TestDox('PostalAddress::getStreetAddress() is public')]
    public function testPostalAddressGetStreetAddressIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getStreetAddress');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::getStreetAddress() has no parameters')]
    public function testPostalAddressGetStreetAddressHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'getStreetAddress');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('PostalAddress::getStreetAddress() returns null by default')]
    public function testPostalAddressGetStreetAddressReturnsNullByDefault(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertNull($postalAddress->getStreetAddress());
    }


    #[TestDox('PostalAddress::setStreetAddress() exists')]
    public function testPostalAddressSetStreetAddressExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('setStreetAddress'));
    }

    #[TestDox('PostalAddress::setStreetAddress() is public')]
    public function testPostalAddressSetStreetAddressIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setStreetAddress');
        $this->assertTrue($method->isPublic());
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress::setStreetAddress() has one REQUIRED parameter')]
    public function testPostalAddressSetStreetAddressHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'setStreetAddress');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Group('hmvc')]
    #[TestDox('PostalAddress::setStreetAddress() accepts string')]
    public function testPostalAddressSetStreetAddressAcceptsString(): void
    {
        $postalAddress = new PostalAddress();
        $postalAddress->setStreetAddress('38 avenue de l’Opera');
        $this->assertEquals('38 avenue de l’Opera', $postalAddress->streetAddress);
        $this->assertEquals('38 avenue de l’Opera', $postalAddress->getStreetAddress());
    }

    #[Depends('testPostalAddressSetStreetAddressHasOneRequiredParameter')]
    #[Depends('testPostalAddressSetStreetAddressAcceptsString')]
    #[Group('hmvc')]
    #[TestDox('PostalAddress::setStreetAddress() accepts StreetAddress object')]
    public function testPostalAddressSetStreetAddressAcceptsStreetAddressObject(): void
    {
        $street_address = new StreetAddress();
        $street_address->setStreetName('S. Broadway');
        $street_address->setHouseNumber(7);

        $postalAddress = new PostalAddress();
        $postalAddress->setStreetAddress($street_address);
        $this->assertEquals('7 S. Broadway', $postalAddress->streetAddress);
        $this->assertEquals('7 S. Broadway', $postalAddress->getStreetAddress());
    }


    #[TestDox('PostalAddress::jsonSerialize() exists')]
    public function testPostalAddressJsonSerializeExists(): void
    {
        $class = new ReflectionClass(PostalAddress::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[TestDox('PostalAddress::jsonSerialize() is public')]
    public function testPostalAddressJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'jsonSerialize');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('PostalAddress::jsonSerialize() has no parameters')]
    public function testPostalAddressJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalAddress::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('PostalAddress::jsonSerialize() returns non-empty array')]
    public function testPostalAddressJsonSerializeReturnsNonEmptyArray(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertNotFalse($postalAddress->jsonSerialize());
        $this->assertIsArray($postalAddress->jsonSerialize());
        $this->assertNotEmpty($postalAddress->jsonSerialize());
    }

    #[TestDox('PostalAddress JSON contains @context https://schema.org')]
    public function testJsonEncodedPostalAddressIsNonEmptyString(): void
    {
        $postalAddress = new PostalAddress();
        $json = json_encode($postalAddress, \JSON_UNESCAPED_SLASHES);
        $this->assertIsString($json);
        $this->assertNotEmpty($json);
    }

    #[Depends('testPostalAddressJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('PostalAddress JSON contains @context https://schema.org')]
    public function testPostalAddressJsonContainsContextHttpsSchemaOrg(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertArrayHasKey('@context', $postalAddress->jsonSerialize());
        $this->assertEquals('https://schema.org', $postalAddress->jsonSerialize()['@context']);
    }

    #[Depends('testPostalAddressJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('PostalAddress JSON contains Schema.org @type PostalAddress')]
    public function testPostalAddressJsonContainsSchemaOrgTypePostalAddress(): void
    {
        $postalAddress = new PostalAddress();
        $this->assertArrayHasKey('@type', $postalAddress->jsonSerialize());
        $this->assertEquals('PostalAddress', $postalAddress->jsonSerialize()['@type']);
    }

    /**
     * @see https://developers.google.com/gmail/markup/reference/parcel-delivery#basic_parcel_delivery
     */
    #[TestDox('Google Workspace acceptance test')]
    public function testGoogleWorkspaceAcceptanceTest(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $deliveryAddress = new PostalAddress(
            [
                'name'            => 'Pickup Corner',
                'streetAddress'   => '24 Willie Mays Plaza',
                'addressLocality' => 'San Francisco',
                'addressRegion'   => 'CA',
                'addressCountry'  => 'US',
                'postalCode'      => '94107',
            ]
        );

        $this->assertTrue($deliveryAddress->has('name'));
        $this->assertEquals('Pickup Corner', $deliveryAddress->get('name'));

        $this->assertTrue($deliveryAddress->has('streetAddress'));
        $this->assertEquals('24 Willie Mays Plaza', $deliveryAddress->get('streetAddress'));

        $this->assertTrue($deliveryAddress->has('addressLocality'));
        $this->assertEquals('San Francisco', $deliveryAddress->get('addressLocality'));

        $this->assertTrue($deliveryAddress->has('addressRegion'));
        $this->assertEquals('CA', $deliveryAddress->get('addressRegion'));

        $this->assertTrue($deliveryAddress->has('addressCountry'));
        $this->assertIsObject($deliveryAddress->get('addressCountry'));
        $this->assertInstanceOf(\Stringable::class, $deliveryAddress->get('addressCountry'));
        $this->assertEquals('US', (string) $deliveryAddress->get('addressCountry'));

        $this->assertTrue($deliveryAddress->has('postalCode'));
        $this->assertEquals('94107', $deliveryAddress->get('postalCode'));

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "PostalAddress",
              "name": "Pickup Corner",
              "streetAddress": "24 Willie Mays Plaza",
              "addressLocality": "San Francisco",
              "addressRegion": "CA",
              "addressCountry": "US",
              "postalCode": "94107"
            }
        ';
        $actualJson = json_encode($deliveryAddress, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

        // Convert object to JSON …
        $deliveryAddress = json_encode($deliveryAddress, \JSON_UNESCAPED_SLASHES);
        $this->assertNotEmpty($deliveryAddress);
        $this->assertIsString($deliveryAddress);

        // … and then convert it back to an object.
        $deliveryAddress = json_decode($deliveryAddress, false);
        $this->assertIsObject($deliveryAddress);
        $this->assertEquals('Pickup Corner', $deliveryAddress->name);
        $this->assertEquals('24 Willie Mays Plaza', $deliveryAddress->streetAddress);
        $this->assertEquals('San Francisco', $deliveryAddress->addressLocality);
        $this->assertEquals('CA', $deliveryAddress->addressRegion);
        $this->assertEquals('US', $deliveryAddress->addressCountry);
        $this->assertEquals('94107', $deliveryAddress->postalCode);
    }
}
