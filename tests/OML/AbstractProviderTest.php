<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Engine\UriFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\OML\AbstractProvider::class)]
#[CoversClass(\StoreCore\OML\Provider::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\Engine\UniformResourceIdentifier::class)]
#[UsesClass(\StoreCore\Engine\UriFactory::class)]
final class AbstractProviderTest extends TestCase
{
    #[TestDox('AbstractProvider class exists')]
    public function testAbstractProviderClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractProvider.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractProvider.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\AbstractProvider'));
        $this->assertTrue(class_exists(\StoreCore\OML\AbstractProvider::class));
    }

    #[TestDox('AbstractProvider is abstract')]
    public function testAbstractProviderIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractProvider::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractProvider implements ProviderInterface')]
    public function testAbstractProviderImplementsProviderInterface(): void
    {
        $provider = new class extends AbstractProvider {};
        $this->assertInstanceOf(\StoreCore\OML\ProviderInterface::class, $provider);
    }


    #[TestDox('AbstractProvider.name exists')]
    public function testAbstractProviderNameExists(): void
    {
        $provider = new class extends AbstractProvider {};
        $this->assertObjectHasProperty('name', $provider);
    }

    #[TestDox('AbstractProvider::getName exists')]
    public function testAbstractProviderGetNameExists(): void
    {
        $class = new ReflectionClass(AbstractProvider::class);
        $this->assertTrue($class->hasMethod('getName'));
    }

    #[Depends('testAbstractProviderGetNameExists')]
    #[TestDox('AbstractProvider::getName is public')]
    public function testAbstractProviderGetNameIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'getName');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractProviderGetNameExists')]
    #[TestDox('AbstractProvider::getName has no parameters')]
    public function testAbstractProviderGetNameHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'getName');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('AbstractProvider::setName exists')]
    public function testAbstractProviderEvenSetNameExists(): void
    {
        $class = new ReflectionClass(AbstractProvider::class);
        $this->assertTrue($class->hasMethod('setName'));
    }

    #[Depends('testAbstractProviderEvenSetNameExists')]
    #[TestDox('AbstractProvider::setName is public')]
    public function testAbstractProviderSetNameIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'setName');
        $this->assertTrue($method->isPublic());
    }

    #[Depends('testAbstractProviderEvenSetNameExists')]
    #[TestDox('AbstractProvider::setName has one REQUIRED parameter')]
    public function testAbstractProviderSetNameHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'setName');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractProviderEvenSetNameExists')]
    #[Depends('testAbstractProviderSetNameIsPublic')]
    #[Depends('testAbstractProviderSetNameHasOneRequiredParameter')]
    #[TestDox('AbstractProvider::setName has one REQUIRED parameter')]
    public function testAbstractProviderSetNameSetsNameAsString(): void
    {
        $provider = new class extends AbstractProvider {};
        $provider->setName('FedEx');
        $this->assertNotEmpty($provider->getName());
        $this->assertIsString($provider->getName());
        $this->assertEquals('FedEx', $provider->getName());
    }


    #[TestDox('AbstractProvider::getUrl exists')]
    public function testAbstractProviderGetUrlExists(): void
    {
        $class = new ReflectionClass(AbstractProvider::class);
        $this->assertTrue($class->hasMethod('getUrl'));
    }

    #[Depends('testAbstractProviderGetUrlExists')]
    #[TestDox('AbstractProvider::getUrl is public')]
    public function testAbstractProviderGetUrlIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'getUrl');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractProviderGetUrlExists')]
    #[TestDox('AbstractProvider::getUrl has no parameters')]
    public function testAbstractProviderGetUrlHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'getUrl');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractProviderGetUrlExists')]
    #[Depends('testAbstractProviderGetUrlIsPublic')]
    #[Depends('testAbstractProviderGetUrlHasNoParameters')]
    #[TestDox('AbstractProvider::getUrl returns null by default')]
    public function testAbstractProviderGetUrlReturnsNullByDefault(): void
    {
        $provider = new class extends AbstractProvider {};
        $this->assertNull($provider->getUrl());
    }

    #[TestDox('AbstractProvider::setURL exists')]
    public function testAbstractProviderSetUrlExists(): void
    {
        $class = new ReflectionClass(AbstractProvider::class);
        $this->assertTrue($class->hasMethod('setURL'));
    }

    #[Depends('testAbstractProviderSetUrlExists')]
    #[TestDox('AbstractProvider::setURL is public')]
    public function testAbstractProviderSetUrlIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'setURL');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractProviderSetUrlExists')]
    #[TestDox('AbstractProvider::setURL has one REQUIRED parameter')]
    public function testAbstractProviderSetUrlHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractProvider::class, 'setURL');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractProviderSetUrlHasOneRequiredParameter')]
    #[TestDox('AbstractProvider::setURL accepts URL as PSR-7 UriInterface')]
    public function testAbstractProviderSetUrlAcceptsUrlAsPsr7UriInterface(): void
    {
        $url_string  = 'https://www.ups.com/us/en/Home.page';
        $uri_factory = new UriFactory();
        $uri_object  = $uri_factory->createUri($url_string);
        $this->assertIsObject($uri_object);

        $provider = new Provider();
        $provider->setURL($uri_object);
        $this->assertNotEmpty($provider->getUrl());
        $this->assertIsString($provider->getUrl());
        $this->assertSame($url_string, $provider->getUrl());
    }
}
