<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Geo\Country;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\PostalCodeFactory::class)]
#[CoversClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\OML\PostalCode::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class PostalCodeFactoryTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PostalCodeFactory class exists')]
    public function testPostalCodeFactoryClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalCodeFactory.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalCodeFactory.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\PostalCodeFactory'));
        $this->assertTrue(class_exists(PostalCodeFactory::class));
    }


    #[Group('hmvc')]
    #[TestDox('PostalCodeFactory class is concrete')]
    public function testPostalCodeFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(PostalCodeFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PostalCodeFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PostalCodeFactory::VERSION);
        $this->assertIsString(PostalCodeFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PostalCodeFactory::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('PostalCodeFactory::__construct exists')]
    public function testPostalCodeFactoryConstructorExists(): void
    {
        $class = new ReflectionClass(PostalCodeFactory::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testPostalCodeFactoryConstructorExists')]
    #[TestDox('PostalCodeFactory::__construct is public')]
    public function testPostalCodeFactoryConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(PostalCodeFactory::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testPostalCodeFactoryConstructorExists')]
    #[TestDox('PostalCodeFactory::__construct has one OPTIONAL parameter')]
    public function testPostalCodeFactoryConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(PostalCodeFactory::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('PostalCodeFactory.country exists')]
    public function testPostalCodeFactoryCountryExists(): void
    {
        $class = new ReflectionClass(PostalCodeFactory::class);
        $this->assertTrue($class->hasProperty('country'));
    }

    #[TestDox('PostalCodeFactory::setCountry exists')]
    public function testPostalCodeFactorySetCountryExists(): void
    {
        $class = new ReflectionClass(PostalCodeFactory::class);
        $this->assertTrue($class->hasMethod('setCountry'));
    }

    #[Depends('testPostalCodeFactorySetCountryExists')]
    #[TestDox('PostalCodeFactory::setCountry is public')]
    public function testPostalCodeFactorySetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(PostalCodeFactory::class, 'setCountry');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testPostalCodeFactorySetCountryExists')]
    #[TestDox('PostalCodeFactory::setCountry has one REQUIRED parameter')]
    public function testPostalCodeFactorySetCountryHasOneRequiredParameter()
    {
        $method = new ReflectionMethod(PostalCodeFactory::class, 'setCountry');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('PostalCode for the Netherlands contains a space')]
    public function testPostalCodeForTheNetherlandsContainsSpace(): void
    {
        $country = new Country('NL', 'Netherlands', 'Nederland');
        $postal_code_factory = new PostalCodeFactory($country);
        $postal_code = $postal_code_factory->createPostalCode('5626hg');

        $this->assertNotEquals('5626hg', (string) $postal_code);
        $this->assertNotEquals('5626HG', (string) $postal_code);
        $this->assertEquals('5626 HG', (string) $postal_code);
    }
}
