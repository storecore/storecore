<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\PostalCode::class)]
final class PostalCodeTest extends TestCase
{
    #[TestDox('PostalCode class is concrete')]
    public function testPostalCodeClassIsConcrete(): void
    {
        $class = new ReflectionClass(PostalCode::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testPostalCodeClassIsConcrete')]
    #[TestDox('PostalCode class is read-only')]
    public function testPostalCodeClassIsReadOnly()
    {
        $class = new ReflectionClass(PostalCode::class);
        $this->assertTrue($class->isReadOnly());
    }


    /**
     * @see https://schema.org/postalCode
     *      Schema.org `postalCode` property of a `PostalAddress`
     */
    #[TestDox('PostalCode is stringable')]
    public function testPostalCodeIsStringable(): void
    {
        $postalCode = new PostalCode('1082 MD');
        $this->assertInstanceOf(\Stringable::class, $postalCode);
    }

    #[Depends('testPostalCodeIsStringable')]
    #[TestDox('PostalCode is JSON serializable')]
    public function testPostalCodeIsJsonSerializable(): void
    {
        $postalCode = new PostalCode('1082 MD');
        $this->assertInstanceOf(\JsonSerializable::class, $postalCode);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PostalCode::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PostalCode::VERSION);
        $this->assertIsString(PostalCode::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PostalCode::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('PostalCode::__construct exists')]
    public function testPostalCodeConstructorExists(): void
    {
        $class = new ReflectionClass(PostalCode::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testPostalCodeConstructorExists')]
    #[TestDox('PostalCode::__construct is public constructor')]
    public function testPostalCodeConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(PostalCode::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testPostalCodeConstructorExists')]
    #[TestDox('PostalCode::__construct has one REQUIRED parameter')]
    public function testPostalCodeConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(PostalCode::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testPostalCodeConstructorExists')]
    #[Depends('testPostalCodeConstructIsPublicConstructor')]
    #[Depends('testPostalCodeConstructorHasOneRequiredParameter')]
    #[TestDox('PostalCode::__construct accepts string')]
    public function testPostalCodeConstructorAcceptsString(): void
    {
        $postalCode = new PostalCode('98052');
        $this->assertEquals('98052', $postalCode->__toString());
    }

    #[Depends('testPostalCodeConstructorAcceptsString')]
    #[TestDox('PostalCode::__construct accepts integer as string')]
    public function testPostalCodeConstructorAcceptsIntegerAsString(): void
    {
        $postalCode = new PostalCode(98052);
        $this->assertEquals('98052', $postalCode->__toString());
    }


    #[TestDox('PostalCode::__toString exists')]
    public function testPostalCodeToStringExists(): void
    {
        $class = new ReflectionClass(PostalCode::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[Depends('testPostalCodeToStringExists')]
    #[TestDox('PostalCode::__toString is public')]
    public function testPostalCodeToStringIsPublic(): void
    {
        $method = new ReflectionMethod(PostalCode::class, '__toString');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testPostalCodeToStringExists')]
    #[TestDox('PostalCode::__toString has no parameters')]
    public function testPostalCodeToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(PostalCode::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testPostalCodeConstructorExists')]
    #[Depends('testPostalCodeConstructorHasOneRequiredParameter')]
    #[Depends('testPostalCodeToStringExists')]
    #[Depends('testPostalCodeToStringHasNoParameters')]
    #[TestDox('PostalCode::__toString returns string')]
    public function testPostalCodeToStringReturnsString(): void
    {
        $postalCode = new PostalCode('5626 HG');
        $this->assertNotEmpty((string) $postalCode);
        $this->assertIsString((string) $postalCode);
        $this->assertEquals('5626 HG', (string) $postalCode);
    }

    #[Depends('testPostalCodeToStringReturnsString')]
    #[TestDox('PostalCode is an uppercase string')]
    public function testPostalCodeIsUpperCaseString(): void
    {
        $postalCode = new PostalCode('5626hg');
        $this->assertNotSame('5626hg', (string) $postalCode);
        $this->assertSame('5626HG', (string) $postalCode);
    }

    #[Depends('testPostalCodeToStringReturnsString')]
    #[TestDox('PostalCode string MAY be empty')]
    public function testPostalCodeStringMayBeEmpty(): void
    {
        $postalCode = new PostalCode('');
        $this->assertEmpty($postalCode->__toString());
        $this->assertEmpty((string) $postalCode);
    }
}
