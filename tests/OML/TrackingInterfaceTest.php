<?php

declare(strict_types=1);

namespace StoreCore\OML;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class TrackingInterfaceTest extends TestCase
{
    #[TestDox('TrackingInterface interface file exists')]
    public function testTrackingInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'TrackingInterface.php'
        );
    }

    #[Depends('testTrackingInterfaceInterfaceFileExists')]
    #[TestDox('TrackingInterface interface file is readable')]
    public function testTrackingInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'TrackingInterface.php'
        );
    }

    #[Depends('testTrackingInterfaceInterfaceFileExists')]
    #[Depends('testTrackingInterfaceInterfaceFileIsReadable')]
    #[TestDox('TrackingInterface interface exists')]
    public function testTrackingInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\OML\\TrackingInterface'));
        $this->assertTrue(interface_exists(TrackingInterface::class));
    }
}
