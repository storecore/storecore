<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Geo\Country;
use StoreCore\Geo\PostalAddress;
use StoreCore\Types\CountryCode;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\AddressIndex::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\CountryRepository::class)]
#[UsesClass(\StoreCore\Geo\PostalAddress::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class AddressIndexTest extends TestCase
{
    protected ReflectionClass $reflectionClass;

    protected function setUp(): void
    {
        $this->reflectionClass = new ReflectionClass(AddressIndex::class);
    }

    #[Group('distro')]
    #[TestDox('AddressIndex class exists')]
    public function testAddressIndexClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AddressIndex.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AddressIndex.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\AddressIndex'));
        $this->assertTrue(class_exists(AddressIndex::class));
    }


    #[Group('hmvc')]
    #[TestDox('AddressIndex class is concrete')]
    public function testAddressIndexClassIsConcrete(): void
    {
        $this->assertFalse($this->reflectionClass->isAbstract());
        $this->assertTrue($this->reflectionClass->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('AddressIndex is stringable')]
    public function testAddressIndexIsStringable(): void
    {
        $class = new ReflectionClass(AddressIndex::class);
        $this->assertTrue($class->implementsInterface(\Stringable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $this->assertTrue($this->reflectionClass->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(AddressIndex::VERSION);
        $this->assertIsString(AddressIndex::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(AddressIndex::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('AddressIndex::setCountry() exists')]
    public function testAddressIndexSetCountryExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('setCountry'));
    }

    #[TestDox('AddressIndex::setCountry() is public')]
    public function testAddressIndexSetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, 'setCountry');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('AddressIndex::setCountry() has one REQUIRED parameter')]
    public function testAddressIndexSetCountryHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, 'setCountry');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('AddressIndex::setCountry() sets CountryCode')]
    public function testAddressIndexSetCountrySetsCountryCode(): void
    {
        $index = new AddressIndex();
        $this->assertNull($index->countryCode);

        $country = new CountryCode('BE');
        $index->setCountry($country);
        $this->assertNotNull($index->countryCode);
        $this->assertSame($country, $index->countryCode);
        $this->assertEquals('BE', (string) $index->countryCode);
    }

    #[Depends('testAddressIndexSetCountrySetsCountryCode')]
    #[TestDox('AddressIndex::setCountry() accepts Country')]
    public function testAddressIndexSetCountryAcceptsCountry(): void
    {
        $index = new AddressIndex();
        $country = new Country('DE', 'Germany', 'Deutschland');
        $index->setCountry($country);

        $this->assertNotInstanceOf(Country::class, $index->countryCode);
        $this->assertInstanceOf(CountryCode::class, $index->countryCode);
        $this->assertEquals('DE', (string) $index->countryCode);
    }


    #[TestDox('AddressIndex::__toString() exists')]
    public function testAddressIndexToStringExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('__toString'));
    }

    #[TestDox('AddressIndex::__toString() is public')]
    public function testAddressIndexToStringIsPublic(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('AddressIndex::__toString() has no parameters')]
    public function testAddressIndexToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('AddressIndex::__toString() returns non-empty string')]
    public function testAddressIndexToStringReturnsNonEmptyString(): void
    {
        $index = new AddressIndex();
        $index->postalCode = '5626 HG';
        $index->houseNumber = '7';

        $this->assertNotEmpty((string) $index);
        $this->assertIsString((string) $index);
    }

    #[Depends('testAddressIndexToStringReturnsNonEmptyString')]
    #[TestDox('AddressIndex::__toString() returns uppercase postal code without spaces')]
    public function testAddressIndexToStringReturnsUppercasePostalCodeWithoutSpaces(): void
    {
        $index = new AddressIndex();
        $index->postalCode = '5611 EM';
        $index->houseNumber = '10';
        $this->assertEquals('5611EM10', (string) $index);

        $index = new AddressIndex();
        $index->postalCode = '5611 em';
        $index->houseNumber = '10';
        $this->assertEquals('5611EM10', (string) $index);

        $index = new AddressIndex();
        $index->postalCode = '5611em';
        $index->houseNumber = '10';
        $this->assertEquals('5611EM10', (string) $index);
    }

    #[Depends('testAddressIndexToStringReturnsUppercasePostalCodeWithoutSpaces')]
    #[TestDox('AddressIndex::__toString() string starts with optional country code')]
    public function testAddressIndexToStringStringStartsWithOptionalCountryCode(): void
    {
        $country = new Country('NL', 'Netherlands');

        $index = new AddressIndex();
        $index->setCountry($country);
        $this->assertStringStartsWith('NL', (string) $index);

        $index = new AddressIndex();
        $index->postalCode = '5611 EM';
        $index->houseNumber = '10';
        $index->setCountry($country);
        $this->assertEquals('NL5611EM10', (string) $index);
    }


    #[TestDox('AddressIndex::createFromPostalAddress() exists')]
    public function testAddressIndexCreateFromPostalAddressExists(): void
    {
        $this->assertTrue($this->reflectionClass->hasMethod('createFromPostalAddress'));
    }

    #[TestDox('AddressIndex::createFromPostalAddress() is public static')]
    public function testAddressIndexCreateFromPostalAddressIsPublicStatic(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, 'createFromPostalAddress');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('AddressIndex::createFromPostalAddress() has one REQUIRED parameter')]
    public function testAddressIndexCreateFromPostalAddressHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AddressIndex::class, 'createFromPostalAddress');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('AddressIndex::createFromPostalAddress() creates an address index for an address')]
    public function testAddressIndexCreateFromPostalAddressCreatesAnAddressIndexForAnAddress(): void
    {
        $api_params = array(
            'status'      => '1',
            'streetName'  => 'Prinses Beatrixlaan',
            'houseNumber' => '23',
            'postalCode'  => '2595AK',
            'city'        => '’S-GRAVENHAGE',
            'areaCode'    => '070',
        );

        $address = new PostalAddress($api_params);
        $index = AddressIndex::createFromPostalAddress($address);
        $this->assertEquals('2595AK23', (string) $index);


        $api_request = '
            {
                "PostalCode": "1507TN",
                "HouseNumber": "6",
                "HouseNumberAddition": "",
                "Selection": ["All"]
            }
        ';

        $api_params = json_decode($api_request, true);
        $this->assertIsArray($api_params);
        $this->assertNotEmpty($api_params);

        $address = new PostalAddress($api_params);
        $index = AddressIndex::createFromPostalAddress($address);
        $this->assertEquals('1507TN6', (string) $index);
    }

    /**
     * @see https://developer.postnl.nl/api-overview/addresses/adrescheck-nederland/documentation-v4/
     */
    #[TestDox('PostNL “Adrescheck Nederland” integration test')]
    public function testPostNLAdrescheckNederlandIntegrationTest(): void
    {
        $params = array(
            'countryIso2' => 'NL',
            'postalCode' => '1234AB',
            'houseNumber' => 3,
            'houseNumberAddition' => 'A',
        );
        $address = new PostalAddress($params);
        $index = AddressIndex::createFromPostalAddress($address);
        $this->assertEquals('NL1234AB3XA', (string) $index);
    }
}
