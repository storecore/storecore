<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\OML\DeliveryMethod::class)]
#[UsesClass(\StoreCore\PIM\PriceSpecification::class)]
final class DeliveryChargeSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DeliveryChargeSpecification class exists')]
    public function testDeliveryChargeSpecificationClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'DeliveryChargeSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'DeliveryChargeSpecification.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\DeliveryChargeSpecification'));
        $this->assertTrue(class_exists(DeliveryChargeSpecification::class));
    }

    #[TestDox('DeliveryChargeSpecification class is concrete')]
    public function testDeliveryChargeSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(DeliveryChargeSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('DeliveryChargeSpecification is a PriceSpecification')]
    public function testDeliveryChargeSpecificationIsPriceSpecification()
    {
        $this->assertTrue(class_exists('\\StoreCore\\PIM\\PriceSpecification'));
        $this->assertTrue(class_exists(\StoreCore\PIM\PriceSpecification::class));

        $charge = new DeliveryChargeSpecification();
        $this->assertInstanceOf(\StoreCore\PIM\PriceSpecification::class, $charge);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DeliveryChargeSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DeliveryChargeSpecification::VERSION);
        $this->assertIsString(DeliveryChargeSpecification::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DeliveryChargeSpecification::VERSION, '0.2.0', '>=')
        );
    }


    #[TestDox('DeliveryChargeSpecification.appliesToDeliveryMethod exists')]
    public function testDeliveryChargeSpecificationAppliesToDeliveryMethodExists(): void
    {
        $class = new ReflectionClass(DeliveryChargeSpecification::class);
        $this->assertTrue($class->hasProperty('appliesToDeliveryMethod'));
    }

    #[Depends('testDeliveryChargeSpecificationAppliesToDeliveryMethodExists')]
    #[TestDox('DeliveryChargeSpecification.appliesToDeliveryMethod is public')]
    public function testhargeSpecificationAppliesToDeliveryMethodIsPublic(): void
    {
        $property = new ReflectionProperty(DeliveryChargeSpecification::class, 'appliesToDeliveryMethod');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testDeliveryChargeSpecificationAppliesToDeliveryMethodExists')]
    #[Depends('testhargeSpecificationAppliesToDeliveryMethodIsPublic')]
    #[TestDox('DeliveryChargeSpecification.appliesToDeliveryMethod is null by default')]
    public function testDeliveryChargeSpecificationAppliesToDeliveryMethodIsNullByDefault(): void
    {
        $delivery_charge_specification = new DeliveryChargeSpecification();
        $this->assertNull($delivery_charge_specification->appliesToDeliveryMethod);
    }

    #[Depends('testDeliveryChargeSpecificationAppliesToDeliveryMethodIsNullByDefault')]
    #[TestDox('DeliveryChargeSpecification.appliesToDeliveryMethod accepts DeliveryMethod')]
    public function testDeliveryChargeSpecificationAppliesToDeliveryMethodAcceptsDeliveryMethod(): void
    {
        $same_day_delivery = new DeliveryChargeSpecification();
        $this->assertNull($same_day_delivery->appliesToDeliveryMethod);

        $bicycle_courier = new DeliveryMethod('Monkey Business');
        $same_day_delivery->appliesToDeliveryMethod = $bicycle_courier;
        $this->assertNotNull($same_day_delivery->appliesToDeliveryMethod);
        $this->assertIsObject($same_day_delivery->appliesToDeliveryMethod);
        $this->assertInstanceOf(\StoreCore\OML\DeliveryMethod::class, $same_day_delivery->appliesToDeliveryMethod);
        $this->assertEquals('Monkey Business', $same_day_delivery->appliesToDeliveryMethod->name);
    }


    #[TestDox('DeliveryChargeSpecification.eligibleRegion exists')]
    public function testDeliveryChargeSpecificationEligibleRegionExists(): void
    {
        $class = new ReflectionClass(DeliveryChargeSpecification::class);
        $this->assertTrue($class->hasProperty('eligibleRegion'));
    }

    #[Depends('testDeliveryChargeSpecificationEligibleRegionExists')]
    #[TestDox('DeliveryChargeSpecification.eligibleRegion is public')]
    public function testhargeSpecificationEligibleRegionIsPublic(): void
    {
        $property = new ReflectionProperty(DeliveryChargeSpecification::class, 'eligibleRegion');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testDeliveryChargeSpecificationEligibleRegionExists')]
    #[Depends('testhargeSpecificationEligibleRegionIsPublic')]
    #[TestDox('DeliveryChargeSpecification.eligibleRegion is null by default')]
    public function testDeliveryChargeSpecificationEligibleRegionIsNullByDefault()
    {
        $delivery_charge_specification = new DeliveryChargeSpecification();
        $this->assertNull($delivery_charge_specification->eligibleRegion);
    }
}
