<?php

declare(strict_types=1);

namespace StoreCore\OML;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\DeliveryEvent::class)]
#[UsesClass(\StoreCore\OML\AbstractDeliveryEvent::class)]
final class DeliveryEventTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('DeliveryEvent class is concrete')]
    public function testDeliveryEventClassIsConcrete(): void
    {
        $class = new ReflectionClass(DeliveryEvent::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('DeliveryEvent extends AbstractDeliveryEvent')]
    public function testDeliveryEventExtendsAbstractDeliveryEvent(): void
    {
        $object = new DeliveryEvent();
        $this->assertInstanceOf(\StoreCore\OML\AbstractDeliveryEvent::class, $object);
    }

    #[Group('hmvc')]
    #[TestDox('DeliveryEvent implements DeliveryEventInterface')]
    public function testDeliveryEventImplementsDeliveryEventInterface(): void
    {
        $object = new DeliveryEvent();
        $this->assertInstanceOf(\StoreCore\OML\DeliveryEventInterface::class, $object);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(DeliveryEvent::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(DeliveryEvent::VERSION);
        $this->assertIsString(DeliveryEvent::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(DeliveryEvent::VERSION, '1.0.0', '>=')
        );
    }
}
