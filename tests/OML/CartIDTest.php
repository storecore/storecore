<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\Types\UUID;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(CartID::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class CartIDTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('CartID class is concrete')]
    public function testCoreCartIdClassIsConcrete(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('CartID is stringable')]
    public function testCartIdIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new CartID());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(CartID::VERSION);
        $this->assertIsString(CartID::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(CartID::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('CartID::__construct exists')]
    public function testCartIdConstructorExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('CartID::__construct is public constructor')]
    public function testCartIdConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(CartID::class, '__construct');
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('CartID::__construct has two OPTIONAL parameters')]
    public function testCartIdConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CartID::__construct sets UUID and token')]
    public function testCartIdConstructorSetsUuidAndToken(): void
    {
        $uuid = new UUID('7ca4c876-e3c1-11eb-b235-8cdcd48d7d2d');
        $token = 'ZubdpvbFAFJUTz6WqWa8lfumzsfF2ZrZZ1H3n1huNPIlCjhh8TsyT0tUsRSFr6Dq1NC2FtprtfBY5z4DQYb1GaWIIhE6sqz2hhCuB0atMUqGu1z0pBSoKLTPyRbkjnwEt0J51Pay3ZpiljyezkcOevr9JvyNVDiWvvWfB1s6n3HZivoP8tKDvmO0sF8vvzr7';

        $cart_id = new CartID($uuid, $token);
        $this->assertSame($uuid,  $cart_id->getUUID());
        $this->assertSame($token, $cart_id->getToken());

        $encoded_string = $cart_id->encode();
        unset($cart_id);

        $restored_cart_id = CartID::createFromString($encoded_string);
        $this->assertEquals($uuid,  $restored_cart_id->getUUID());
        $this->assertEquals($token, $restored_cart_id->getToken());
    }


    #[TestDox('CartID::createFromString exists')]
    public function testCartIdDecodeExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('createFromString'));
    }

    #[TestDox('CartID::createFromString is public static')]
    public function testCartIdDecodeIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'createFromString');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[TestDox('CartID::createFromString has one REQUIRED parameter')]
    public function testCartIdDecodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CartID::class, 'createFromString');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CartID::createFromString returns false on errors')]
    public function testCartIdDecodeReturnsFalseOnErrors(): void
    {
        $nothing_to_decode = (string) null;
        $this->assertFalse(CartID::createFromString($nothing_to_decode));

        $cannot_decode_integer = 12345;
        $this->assertFalse(CartID::createFromString($cannot_decode_integer));
    }


    #[TestDox('CartID::encode exists')]
    public function testCartIdEncodeExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('encode'));
    }

    #[TestDox('CartID::encode is public')]
    public function testCartIdEncodeIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'encode');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::encode has no parameters')]
    public function testCartIdEncodeHasNoParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, 'encode');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('CartID::encode returns non-empty string')]
    function testPublicEncodeMethodReturnsNonEmptyString(): void
    {
        $cart_id = new CartID();
        $this->assertNotEmpty($cart_id->encode());
        $this->assertIsString($cart_id->encode());
    }


    #[TestDox('CartID::__toString exists')]
    public function testCartIdToStringExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('CartID::__toString is public')]
    public function testCartIdToStringIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::__toString has no parameters')]
    public function testCartIdToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('CartID::__toString returns non-empty string')]
    function testCartIdToStringReturnsNonEmptyString(): void
    {
        $cart_id = new CartID();
        $this->assertNotEmpty((string)$cart_id);
        $this->assertIsString((string)$cart_id);

        $cart_id = new CartID();
        $this->assertNotEmpty($cart_id->__toString());
        $this->assertIsString($cart_id->__toString());
    }

    #[TestDox('CartID::__toString type cast equals encode() return value')]
    function testCartIdToStringTypeCastEqualsEncodeReturnValue(): void
    {
        $cart_id = new CartID();
        $encoded_cart_id = $cart_id->encode();
        $this->assertEquals($encoded_cart_id, (string) $cart_id);
    }


    #[TestDox('CartID::getToken exists')]
    public function testCartIdGetTokenExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('getToken'));
    }

    #[TestDox('CartID::__toString is public')]
    public function testCartIdGetTokenIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'getToken');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::__toString has no parameters')]
    public function testCartIdGetTokenHasNoParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, 'getToken');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('CartID::__toString returns non-empty string')]
    function testCartIdGetTokenReturnsNonEmptyString(): void
    {
        $cart_id = new CartID();
        $this->assertNotEmpty($cart_id->getToken());
        $this->assertIsString($cart_id->getToken());
    }

    #[TestDox('CartID::__toString returns 192 characters')]
    function testCartIdGetTokenReturns192Characters(): void
    {
        $cart_id = new CartID();
        $token = $cart_id->getToken();
        $this->assertEquals(192, strlen($token));
    }


    #[TestDox('CartID::getUUID exists')]
    public function testCartIdGetUuidExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('getUUID'));
    }

    #[TestDox('CartID::getUUID is public')]
    public function testCartIdGetUuidIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'getUUID');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::getUUID has no parameters')]
    public function testCartIdGetUuidHasNoParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, 'getUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('CartID::getUUID returns stringable UUID value object')]
    function testCartIdGetUuidReturnsNonEmptyString(): void
    {
        $cart_id = new CartID();
        $this->assertNotEmpty($cart_id->getUUID());
        $this->assertIsObject($cart_id->getUUID());
        $this->assertInstanceOf(\Stringable::class, $cart_id->getUUID());
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $cart_id->getUUID());
    }

    #[TestDox('CartID::getUUID returns initialized UUID')]
    public function testCartIdGetUuidReturnsInitializedUuid(): void
    {
        $uuid = new UUID('d0c528ed-e3c6-11eb-b235-8cdcd48d7d2d');
        $cart_id = new CartID($uuid);
        $this->assertSame($uuid, $cart_id->getUUID());
    }

    #[Depends('testCartIdGetUuidReturnsNonEmptyString')]
    #[TestDox('CartID::getUUID returns set UUID')]
    public function testCartIdGetUuidReturnsSetUuid(): void
    {
        $cart_id = new CartID();
        $uuid = new UUID('d0c528ed-e3c6-11eb-b235-8cdcd48d7d2d');
        $this->assertNotSame($uuid, $cart_id->getUUID());
        $cart_id->setUUID($uuid);
        $this->assertSame($uuid, $cart_id->getUUID());
    }


    #[TestDox('CartID::resetToken exists')]
    public function testCartIdResetTokenExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('resetToken'));
    }

    #[TestDox('CartID::resetToken is public')]
    public function testCartIdResetTokenIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'resetToken');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::resetToken has no parameters')]
    public function testCartIdResetTokenHasNoParameters(): void
    {
        $method = new ReflectionMethod(CartID::class, 'resetToken');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('CartID::resetToken changes token')]
    public function testCartIdResetTokenChangesToken(): void
    {
        $cart_id = new CartID();
        $token = $cart_id->getToken();
        $this->assertSame($token, $cart_id->getToken());
        $cart_id->resetToken();
        $this->assertNotSame($token, $cart_id->getToken());
    }


    #[TestDox('CartID::setToken exists')]
    public function testCartIdSetTokenExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('setToken'));
    }

    #[TestDox('CartID::setToken is public')]
    public function testCartIdSetTokenIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'setToken');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::setToken has one REQUIRED parameter')]
    public function testCartIdSetTokenHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CartID::class, 'setToken');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CartID::setToken sets token')]
    public function testCartIdSetTokenSetsToken(): void
    {
        $cart_id = new CartID();
        $token = 'PUO9OZ4FRIZaRtzShMNBtMSBWt5EaPM0T1YNwa6fWGPpZIpfOtzep2vkzMKHGdf6BWMQIL4I1zDYIhHEC7Eiyvk3RNQhbPDYy693pn5zONgVkL3OjwYAAEMobt5Baq7yzLdjsTftL3H3kzOViBP4QPZYJEWSYC2vOtbQDvydlGeZYjvmrIreb8tkDB7OQPji';
        $this->assertNotSame($token, $cart_id->getToken());
        $cart_id->setToken($token);
        $this->assertSame($token, $cart_id->getToken());
    }

    #[TestDox('CartID::setToken throws ValueError exception if string length is not 192 characters')]
    public function testCartIDSetTokenThrowsValueErrorExceptionIfStringLengthIsNot192Characters(): void
    {
        $cart_id = new CartID();
        $this->expectException(\ValueError::class);
        $cart_id->setToken('qwerty');
    }


    #[TestDox('CartID::setUUID() exists')]
    public function testCartIdSetUuidExists(): void
    {
        $class = new ReflectionClass(CartID::class);
        $this->assertTrue($class->hasMethod('setUUID'));
    }

    #[TestDox('CartID::setUUID() is public')]
    public function testCartIdSetUuidIsPublic(): void
    {
        $method = new ReflectionMethod(CartID::class, 'setUUID');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('CartID::setUUID() has one REQUIRED parameter')]
    public function testCartIdSetUuidHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(CartID::class, 'setUUID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('CartID::setUUID() sets UUID as value object')]
    public function testCartIdSetUUIDSetsUUIDAsValueObject(): void
    {
        $cart_id = new CartID();
        $uuid = new UUID('31c04e63-e3c8-11eb-b235-8cdcd48d7d2d');
        $this->assertNotSame($uuid, $cart_id->getUUID());
        $cart_id->setUUID($uuid);
        $this->assertSame($uuid, $cart_id->getUUID());
    }

    #[TestDox('CartID::setUUID() accepts UUID as string')]
    public function testCartIdSetUUIDAcceptsUUIDAsString(): void
    {
        $cart_id = new CartID();
        $cart_id->setUUID('31c04e63-e3c8-11eb-b235-8cdcd48d7d2d');
        $this->assertEquals('31c04e63-e3c8-11eb-b235-8cdcd48d7d2d', (string) $cart_id->getUUID());

        $invalid_uuid = '31c0-e3c8-11eb-b235-8cdc';
        $this->expectException(\ValueError::class);
        $cart_id->setUUID($invalid_uuid);
    }
}
