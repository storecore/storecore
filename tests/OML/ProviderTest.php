<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\CRM\Organization;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \json_encode;
use function \version_compare;

use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

#[CoversClass(\StoreCore\OML\Provider::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class ProviderTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Provider class exists')]
    public function testProviderClassFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'Provider.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'Provider.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\Provider'));
        $this->assertTrue(class_exists(Provider::class));
    }

    #[TestDox('Provider class is concrete')]
    public function testProviderClassIsConcrete(): void
    {
        $class = new ReflectionClass(Provider::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Provider extends AbstractProvider')]
    public function testProviderExtendsAbstractProvider(): void
    {
        $this->assertInstanceOf(AbstractProvider::class, new Provider());
    }

    #[Group('hmvc')]
    #[TestDox('Provider implements ProviderInterface')]
    public function testProviderImplementsProviderInterface()
    {
        $this->assertInstanceOf(ProviderInterface::class, new Provider());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Provider::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Provider::VERSION);
        $this->assertIsString(Provider::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Provider::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Provider::__construct exists')]
    public function testProviderConstructorExists(): void
    {
        $class = new ReflectionClass(Provider::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testProviderConstructorExists')]
    #[TestDox('Provider::__construct is public constructor')]
    public function testProviderConstructorIsPublic(): void
    {
        $method = new ReflectionMethod(Provider::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testProviderConstructorExists')]
    #[TestDox('Provider::__construct has two OPTIONAL parameters')]
    public function testProviderConstructorHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(Provider::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testProviderConstructorExists')]
    #[Depends('testProviderConstructorIsPublic')]
    #[Depends('testProviderConstructorHasTwoOptionalParameters')]
    #[TestDox('Provider::__construct sets provider name')]
    public function testProviderConstructorSetsProviderName(): void
    {
        $provider = new Provider('DHL');
        $this->assertEquals('DHL', $provider->getName());

        $provider = new Provider('UPS');
        $this->assertEquals('UPS', $provider->getName());
    }

    #[Depends('testProviderConstructorHasTwoOptionalParameters')]
    #[TestDox('Provider::__construct sets provider URL')]
    public function testProviderConstructorSetsProviderUrl(): void
    {
        $provider = new Provider('DHL', 'https://www.dhl.com/');
        $this->assertEquals('https://www.dhl.com/', $provider->getUrl());

        $provider = new Provider('UPS', 'https://www.ups.com/us/en/global.page');
        $this->assertEquals('https://www.ups.com/us/en/global.page', $provider->getUrl());
    }


    #[TestDox('Provider::createFromOrganization exists')]
    public function testProviderCreateFromOrganizationExists(): void
    {
        $class = new ReflectionClass(Provider::class);
        $this->assertTrue($class->hasMethod('createFromOrganization'));
    }

    #[Depends('testProviderCreateFromOrganizationExists')]
    #[TestDox('Provider::createFromOrganization is public static')]
    public function testProviderCreateFromOrganizationIsPublicStatic(): void
    {
        $method = new ReflectionMethod(Provider::class, 'createFromOrganization');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isStatic());
    }

    #[Depends('testProviderCreateFromOrganizationExists')]
    #[TestDox('Provider::createFromOrganization has one REQUIRED parameter')]
    public function testProviderCreateFromOrganizationHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Provider::class, 'createFromOrganization');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testProviderCreateFromOrganizationExists')]
    #[Depends('testProviderCreateFromOrganizationIsPublicStatic')]
    #[Depends('testProviderCreateFromOrganizationHasOneRequiredParameter')]
    #[TestDox('Provider::createFromOrganization accepts Organization and returns Provider')]
    public function testProviderCreateFromOrganizationAcceptsOrganizationAndReturnsProvider(): void
    {
        $organization = new Organization('UPS');
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);
        $this->assertSame('UPS', $organization->name);

        $provider = Provider::createFromOrganization($organization);
        $this->assertInstanceOf(\StoreCore\OML\Provider::class, $provider);
        $this->assertInstanceOf(\StoreCore\OML\ProviderInterface::class, $provider);
        $this->assertSame('UPS', $provider->getName());
        $this->assertEmpty($provider->getUrl());

        $organization = new Organization();
        $organization->name = 'DHL';
        $organization->url = 'https://www.dhl.com/nl-nl/home.html';

        $provider = Provider::createFromOrganization($organization);
        $this->assertInstanceOf(Provider::class, $provider);
        $this->assertInstanceOf(ProviderInterface::class, $provider);
        $this->assertSame('DHL', $provider->getName());
        $this->assertSame('https://www.dhl.com/nl-nl/home.html', $provider->getUrl());
    }

    /**
     * @see https://developers.google.com/gmail/markup/reference/parcel-delivery#json-ld
     */
    #[Depends('testProviderCreateFromOrganizationAcceptsOrganizationAndReturnsProvider')]
    #[TestDox('Provider::createFromOrganization accepts JSON string and returns Provider')]
    public function testProviderCreateFromOrganizationAcceptsJsonStringAndReturnsProvider(): void
    {
        $carrier = '
          {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "FedEx"
          }
        ';

        $provider = Provider::createFromOrganization($carrier);
        $this->assertInstanceOf(\StoreCore\OML\Provider::class, $provider);
        $this->assertInstanceOf(\StoreCore\OML\ProviderInterface::class, $provider);
        $this->assertEquals('FedEx', $provider->getName());

        $this->assertInstanceOf(\JsonSerializable::class, $provider);
        $provider = json_encode($provider, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($carrier, $provider);
    }
}
