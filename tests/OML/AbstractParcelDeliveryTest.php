<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversNothing]
#[Group('hmvc')]
final class AbstractParcelDeliveryTest extends TestCase
{
    protected ReflectionClass $reflectionClass;
    protected AbstractParcelDelivery $parcelDelivery;

    protected function setUp(): void
    {
        $this->reflectionClass = new ReflectionClass(AbstractParcelDelivery::class);
        $this->parcelDelivery = new class extends AbstractParcelDelivery {};
    }

    #[Group('distro')]
    #[TestDox('AbstractParcelDelivery class exists')]
    public function testAbstractParcelDeliveryClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractParcelDelivery.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractParcelDelivery.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\AbstractParcelDelivery'));
        $this->assertTrue(class_exists(AbstractParcelDelivery::class));
    }

    #[TestDox('AbstractParcelDelivery is abstract')]
    public function testAbstractParcelDeliveryIsAbstract(): void
    {
        $this->assertTrue($this->reflectionClass->isAbstract());
        $this->assertFalse($this->reflectionClass->isInstantiable());
    }

    #[TestDox('AbstractParcelDelivery is JSON serializable')]
    public function testAbstractParcelDeliveryIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, $this->parcelDelivery);
    }


    #[TestDox('AbstractParcelDelivery.expectedArrivalFrom exists')]
    public function testAbstractParcelDeliveryExpectedArrivalFromExists(): void
    {
        $this->assertObjectHasProperty('expectedArrivalFrom', $this->parcelDelivery);
    }

    #[TestDox('AbstractParcelDelivery.expectedArrivalUntil exists')]
    public function testAbstractParcelDeliveryExpectedArrivalUntilExists(): void
    {
        $this->assertObjectHasProperty('expectedArrivalUntil', $this->parcelDelivery);
    }

    #[TestDox('AbstractParcelDelivery.itemShipped exists')]
    public function testAbstractParcelDeliveryItemShippedExists(): void
    {
        $this->assertObjectHasProperty('itemShipped', $this->parcelDelivery);
    }

    #[TestDox('AbstractParcelDelivery.provider exists')]
    public function testAbstractParcelDeliveryProviderExists(): void
    {
        $this->assertObjectHasProperty('provider', $this->parcelDelivery);
    }

    #[TestDox('AbstractParcelDelivery.trackingNumber exists')]
    public function testAbstractParcelDeliveryTrackingNumberExists(): void
    {
        $this->assertObjectHasProperty('trackingNumber', $this->parcelDelivery);
    }

    #[TestDox('AbstractParcelDelivery.trackingUrl exists')]
    public function testAbstractParcelDeliveryTrackingUrlExists(): void
    {
        $this->assertObjectHasProperty('trackingUrl', $this->parcelDelivery);
    }
}
