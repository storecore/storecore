<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;
use StoreCore\Order;
use StoreCore\Database\StoreMapper;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\OrderRepository::class)]
#[CoversClass(\StoreCore\OML\OrderFactory::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\StoreMapper::class)]
final class OrderRepositoryTest extends TestCase
{
    private \PDO|null $Database = null;

    private Registry $Registry;

    protected function setUp(): void
    {
        $this->Registry = Registry::getInstance();
        if ($this->Registry->has('Database')) {
            $this->Database = $this->Registry->get('Database');
        } else {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $this->Database->query('SELECT 1 FROM `sc_orders`');
            $this->Database->query('SELECT 1 FROM `sc_stores`');

            if ($this->Database !== null) {
                $this->Database->exec(
                    "DELETE
                       FROM `sc_orders`
                      WHERE `order_uuid` = UNHEX('00000000000000000000000000000000')
                         OR `customer_uuid` = UNHEX('00000000000000000000000000000000')
                         OR `broker_uuid` = UNHEX('00000000000000000000000000000000')
                         OR `seller_uuid` = UNHEX('00000000000000000000000000000000')
                ");
            }
        } catch (\PDOException $e) {
            $this->Database = null;
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[Group('hmvc')]
    #[TestDox('OrderRepository class is concrete')]
    public function testOrderRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderRepository is a database model')]
    public function testOrderRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('OrderRepository implements PSR-11 ContainerInterface')]
    public function testOrderRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testOrderRepositoryImplementsPSR11ContainerInterface')]
    #[Group('hmvc')]
    #[TestDox('OrderRepository implements StoreCore RepositoryInterface')]
    public function testOrderRepositoryImplementsStoreCoreRepositoryInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\RepositoryInterface'));
        $this->assertTrue(interface_exists(\StoreCore\RepositoryInterface::class));

        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\RepositoryInterface::class));
    }


    #[Group('hmvc')]
    #[TestDox('Implemented PSR-20 ClockInterface exists')]
    public function testImplementedPSRClockInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Clock\\ClockInterface'));
        $this->assertTrue(interface_exists(\Psr\Clock\ClockInterface::class));
    }

    #[Depends('testImplementedPSRClockInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('OrderRepository implements PSR-20 ClockInterface')]
    public function testOrderRepositoryImplementsPSR20ClockInterface(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Clock\ClockInterface::class));
    }


    #[TestDox('OrderRepository is Countable')]
    public function testOrderRepositoryIsCountable(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderRepository::VERSION);
        $this->assertIsString(OrderRepository::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderRepository::count exists')]
    public function testOrderRepositoryCountExists(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('OrderRepository::count is public')]
    public function testOrderRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'count');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('OrderRepository::count has no parameters')]
    public function testOrderRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('OrderRepository::count returns positive integer')]
    public function testOrderRepositoryCountReturnsPositiveInteger(): void
    {
        $repository = new OrderRepository($this->Registry);
        $this->assertIsInt($repository->count());
        $this->assertGreaterThanOrEqual(0, $repository->count());
    }


    #[TestDox('OrderRepository::get exists')]
    public function testOrderRepositoryGetExists(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[TestDox('OrderRepository::get is public')]
    public function testOrderRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'get');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('OrderRepository::get has one REQUIRED parameter')]
    public function testOrderRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('OrderRepository::has exists')]
    public function testOrderRepositoryHasExists(): void
    {
        $class = new ReflectionClass(OrderRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[TestDox('OrderRepository::has is public')]
    public function testOrderRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OrderRepository::has has one REQUIRED parameter')]
    public function testOrderRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }
}
