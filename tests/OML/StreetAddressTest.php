<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Geo\Country;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\StreetAddress::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class StreetAddressTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('StreetAddress class exists')]
    public function testStreetAddressClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'StreetAddress.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'StreetAddress.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\StreetAddress'));
        $this->assertTrue(class_exists(StreetAddress::class));
    }

    #[Group('hmvc')]
    #[TestDox('StreetAddress class is concrete')]
    public function testStreetAddressClassIsConcrete(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('StreetAddress is Stringable')]
    public function testStreetAddressIsStringable(): void
    {
        $street_address = new StreetAddress();
        $this->assertInstanceOf(\Stringable::class, $street_address);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(StreetAddress::VERSION);
        $this->assertIsString(StreetAddress::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(StreetAddress::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('StreetAddress::__construct() exists')]
    public function testStreetAddressConstructorExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('StreetAddress::__construct() has three OPTIONAL parameters')]
    public function testStreetAddressConstructorHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('StreetAddress::__get() exists')]
    public function testStreetAddressGetExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('__get'));
    }

    #[TestDox('StreetAddress::__get() is public')]
    public function testStreetAddressGetIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__get');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::__get() has one REQUIRED parameter')]
    public function testStreetAddressGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('StreetAddress::__set() exists')]
    public function testStreetAddressSetExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('__set'));
    }

    #[TestDox('StreetAddress::__set() is public')]
    public function testStreetAddressSetIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__set');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::__set() has two REQUIRED parameters')]
    public function testStreetAddressSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__set');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('StreetAddress::__toString() exists')]
    public function testStreetAddressToStringExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('__toString'));
    }

    #[TestDox('StreetAddress::__toString() is public')]
    public function testStreetAddressToStringIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__toString');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::__toString() has no parameters')]
    public function testStreetAddressToStringHasNoParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, '__toString');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('StreetAddress::__toString() returns empty string if no properties are set')]
    public function testStreetAddressReturnsEmptyStringIfNoPropertiesAreSet(): void
    {
        $street_address = new StreetAddress();
        $this->assertIsString((string) $street_address);
        $this->assertEmpty((string) $street_address);
    }

    #[TestDox('StreetAddress::__toString() returns house number and street name')]
    public function testStreetAddressReturnsHouseNumberAndStreetName(): void
    {
        $street_address = new StreetAddress('Amphitheatre Parkway', '1600');
        $this->assertNotEmpty((string) $street_address);
        $this->assertSame('1600 Amphitheatre Parkway', (string) $street_address);

        $street_address = new StreetAddress('Rue de Londres', 8);
        $this->assertNotEmpty((string) $street_address);
        $this->assertSame('8 Rue de Londres', (string) $street_address);
    }


    #[TestDox('StreetAddress::getCountry() exists')]
    public function testStreetAddressGetCountryExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('getCountry'));
    }

    #[TestDox('StreetAddress::getCountry() is public')]
    public function testStreetAddressGetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getCountry');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::getCountry() has no parameters')]
    public function testStreetAddressGetCountryHasNoParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getCountry');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('StreetAddress::getCountry() returns null by default')]
    public function testStreetAddressGetCountryReturnsNullByDefault(): void
    {
        $streetAddress = new StreetAddress();
        $this->assertNull($streetAddress->getCountry());
    }


    #[TestDox('StreetAddress::getHouseNumber() exists')]
    public function testStreetAddressGetHouseNumberExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('getHouseNumber'));
    }

    #[TestDox('StreetAddress::getHouseNumber() is public')]
    public function testStreetAddressGetHouseNumberIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getHouseNumber');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::getHouseNumber() has no parameters')]
    public function testStreetAddressGetHouseNumberHasNoParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getHouseNumber');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('StreetAddress::getHouseNumber() returns null by default')]
    public function testStreetAddressGetHouseNumberReturnsNullByDefault(): void
    {
        $streetAddress = new StreetAddress();
        $this->assertNull($streetAddress->getHouseNumber());
    }


    #[TestDox('StreetAddress::getHouseNumberAddition() exists')]
    public function testStreetAddressGetHouseNumberAdditionExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('getHouseNumberAddition'));
    }

    #[TestDox('StreetAddress::getHouseNumberAddition() is public')]
    public function testStreetAddressGetHouseNumberAdditionIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getHouseNumberAddition');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::getHouseNumberAddition() has no parameters')]
    public function testStreetAddressGetHouseNumberAdditionHasNoParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getHouseNumberAddition');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('StreetAddress::getHouseNumberAddition() returns null by default')]
    public function testStreetAddressGetHouseNumberAdditionReturnsNullByDefault(): void
    {
        $streetAddress = new StreetAddress();
        $this->assertNull($streetAddress->getHouseNumberAddition());
    }


    #[TestDox('StreetAddress::getStreetName() exists')]
    public function testStreetAddressGetStreetNameExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('getStreetName'));
    }

    #[TestDox('StreetAddress::getStreetName() is public')]
    public function testStreetAddressGetStreetNameIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getStreetName');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::getStreetName() has no parameters')]
    public function testStreetAddressGetStreetNameHasNoParameters(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'getStreetName');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('StreetAddress::getStreetName() returns null by default')]
    public function testStreetAddressGetStreetNameReturnsNullByDefault(): void
    {
        $streetAddress = new StreetAddress();
        $this->assertNull($streetAddress->getStreetName());
    }


    #[TestDox('StreetAddress::setCountry() exists')]
    public function testStreetAddressSetCountryExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('setCountry'));
    }

    #[TestDox('StreetAddress::setCountry() is public')]
    public function testStreetAddressSetCountryIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setCountry');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::setCountry() has one REQUIRED parameter')]
    public function testStreetAddressSetCountryHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setCountry');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('StreetAddress::setCountry() sets Country object as country property')]
    public function testStreetAddressSetCountrySetsCountryObjectAsCountryProperty(): void
    {
        $street_address = new StreetAddress();
        $this->assertNull($street_address->country);

        $country = new Country('DE', 'Germany');
        $street_address->setCountry($country);
        $this->assertNotNull($street_address->country);
        $this->assertIsObject($street_address->country);
        $this->assertInstanceOf(\StoreCore\Geo\Country::class, $street_address->country);
        $this->assertSame($country, $street_address->country);
    }

    #[TestDox('StreetAddress::getCountry() returns set Country value object')]
    public function testStreetAddressGetCountryReturnsSetCountryValueObject(): void
    {
        $country = new Country('DE', 'Germany');

        $street_address = new StreetAddress();
        $this->assertNotSame($country, $street_address->getCountry());
        $this->assertEmpty($street_address->getCountry());

        $street_address->setCountry($country);
        $this->assertNotEmpty($street_address->getCountry());
        $this->assertIsObject($street_address->getCountry());
        $this->assertInstanceOf(\StoreCore\Geo\Country::class, $street_address->getCountry());
        $this->assertSame($country, $street_address->getCountry());
    }


    #[TestDox('StreetAddress::setHouseNumber() exists')]
    public function testStreetAddressSetHouseNumberExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('setHouseNumber'));
    }

    #[TestDox('StreetAddress::setHouseNumber() is public')]
    public function testStreetAddressSetHouseNumberIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setHouseNumber');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::setHouseNumber() has one REQUIRED parameter')]
    public function testStreetAddressSetHouseNumberHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setHouseNumber');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('StreetAddress::setHouseNumberAddition() exists')]
    public function testStreetAddressSetHouseNumberAdditionExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('setHouseNumberAddition'));
    }

    #[TestDox('StreetAddress::setHouseNumberAddition() is public')]
    public function testStreetAddressSetHouseNumberAdditionIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setHouseNumberAddition');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::setHouseNumberAddition() has one REQUIRED parameter')]
    public function testStreetAddressSetHouseNumberAdditionHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setHouseNumberAddition');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('StreetAddress::setStreetName() exists')]
    public function testStreetAddressSetStreetNameExists(): void
    {
        $class = new ReflectionClass(StreetAddress::class);
        $this->assertTrue($class->hasMethod('setStreetName'));
    }

    #[TestDox('StreetAddress::setStreetName() is public')]
    public function testStreetAddressSetStreetNameIsPublic(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setStreetName');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('StreetAddress::setStreetName() has one REQUIRED parameter')]
    public function testStreetAddressSetStreetNameHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(StreetAddress::class, 'setStreetName');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('StreetAddress::setStreetName() sets streetName property')]
    public function testStreetAddressSetStreetNameSetsStreetNameProperty(): void
    {
        $streetAddress = new StreetAddress();
        $this->assertNull($streetAddress->streetName);

        $streetAddress->setStreetName('Amphitheatre Parkway');
        $this->assertNotNull($streetAddress->streetName);
        $this->assertNotEmpty($streetAddress->streetName);
        $this->assertIsString($streetAddress->streetName);
        $this->assertSame('Amphitheatre Parkway', $streetAddress->streetName);
    }

    #[TestDox('StreetAddress::getStreetName() returns set street name')]
    public function testStreetAddressGetStreetNameReturnsSetStreetName(): void
    {
        $streetAddress = new StreetAddress();
        $streetAddress->setStreetName('Amphitheatre Parkway');
        $this->assertSame('Amphitheatre Parkway', $streetAddress->getStreetName());
    }
}
