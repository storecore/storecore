<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\OML\AbstractDeliveryEvent::class)]
#[Group('hmvc')]
#[UsesClass(\StoreCore\OML\DeliveryMethod::class)]
final class AbstractDeliveryEventTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractDeliveryEvent class exists')]
    public function testAbstractDeliveryEventClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractDeliveryEvent.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'AbstractDeliveryEvent.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\AbstractDeliveryEvent'));
        $this->assertTrue(class_exists(AbstractDeliveryEvent::class));
    }

    #[TestDox('AbstractDeliveryEvent is abstract')]
    public function testAbstractDeliveryEventIsAbstract(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isInstantiable());
    }

    #[TestDox('AbstractDeliveryEvent implements DeliveryEventInterface')]
    public function testAbstractDeliveryEventImplementsDeliveryEventInterface(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->implementsInterface(DeliveryEventInterface::class));
    }


    #[TestDox('AbstractDeliveryEvent::__get() exists')]
    public function testAbstractDeliveryEventMagicGetExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('__get'));
    }

    #[Depends('testAbstractDeliveryEventMagicGetExists')]
    #[TestDox('AbstractDeliveryEvent::__get() is public')]
    public function testAbstractDeliveryEventMagicGetIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, '__get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventMagicGetExists')]
    #[TestDox('AbstractDeliveryEvent::__get() has one REQUIRED parameter')]
    public function testAbstractDeliveryEventMagicGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, '__get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractDeliveryEventMagicGetExists')]
    #[Depends('testAbstractDeliveryEventMagicGetIsPublic')]
    #[Depends('testAbstractDeliveryEventMagicGetHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::__get() returns null if property does not exist')]
    public function testAbstractDeliveryEventMagicGetReturnsNullIfPropertyDoesNotExist(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->__get('fooBar'));
        $this->assertNull($deliveryEvent->fooBar);
    }


    #[TestDox('AbstractDeliveryEvent::__set() exists')]
    public function testAbstractDeliveryEventMagicSetExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('__set'));
    }

    #[Depends('testAbstractDeliveryEventMagicSetExists')]
    #[TestDox('AbstractDeliveryEvent::__set() is public')]
    public function testAbstractDeliveryEventMagicSetIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, '__set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventMagicSetExists')]
    #[TestDox('AbstractDeliveryEvent::__set() has two REQUIRED parameters')]
    public function testAbstractDeliveryEventMagicSetHasTwoRequiredParameters(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, '__set');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('AbstractDeliveryEvent.accessCode exists')]
    public function testAbstractDeliveryEventAccessCodeExists(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertObjectHasProperty('accessCode', $deliveryEvent);
    }

    #[TestDox('AbstractDeliveryEvent::getAccessCode() exists')]
    public function testAbstractDeliveryEventGetAccessCodeExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('getAccessCode'));
    }

    #[Depends('testAbstractDeliveryEventGetAccessCodeExists')]
    #[TestDox('AbstractDeliveryEvent::getAccessCode() is public')]
    public function testAbstractDeliveryGetAccessCodeIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAccessCode');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventGetAccessCodeExists')]
    #[TestDox('AbstractDeliveryEvent::getAccessCode() has no parameters')]
    public function testAbstractDeliveryEventGetAccessCodeHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAccessCode');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDeliveryEventGetAccessCodeExists')]
    #[Depends('testAbstractDeliveryGetAccessCodeIsPublic')]
    #[Depends('testAbstractDeliveryEventGetAccessCodeHasNoParameters')]
    #[TestDox('AbstractDeliveryEvent::getAccessCode() returns null by default')]
    public function testAbstractDeliveryEventGetAccessCodeReturnsNullByDefault(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->getAccessCode());
    }

    #[TestDox('AbstractDeliveryEvent::setAccessCode() exists')]
    public function testAbstractDeliveryEvenSetAccessCodeExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('setAccessCode'));
    }

    #[Depends('testAbstractDeliveryEvenSetAccessCodeExists')]
    #[TestDox('AbstractDeliveryEvent::setAccessCode() is public')]
    public function testAbstractDeliverySetAccessCodeIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAccessCode');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEvenSetAccessCodeExists')]
    #[TestDox('AbstractDeliveryEvent::setAccessCode() has one REQUIRED parameter')]
    public function testAbstractDeliveryEventSetAccessCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAccessCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractDeliveryEvenSetAccessCodeExists')]
    #[Depends('testAbstractDeliverySetAccessCodeIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAccessCodeHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::setAccessCode() sets string')]
    public function testAbstractDeliveryEventSetAccessCodeSetsString(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAccessCode('Sesame');
        $this->assertEquals('Sesame', $deliveryEvent->getAccessCode());
        $this->assertEquals('Sesame', $deliveryEvent->accessCode);
    }

    #[Depends('testAbstractDeliveryEventSetAccessCodeHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent.accessCode accepts integer as string')]
    public function testAbstractDeliveryEventAccessCodeAcceptsIntegerAsString(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertEmpty($deliveryEvent->accessCode);

        $deliveryEvent->accessCode = (int) 1234;
        $this->assertIsNotInt($deliveryEvent->accessCode);
        $this->assertIsString($deliveryEvent->accessCode);
        $this->assertEquals('1234', $deliveryEvent->accessCode);
    }


    #[TestDox('AbstractDeliveryEvent.availableFrom exists')]
    public function testAbstractDeliveryEventAvailableFromExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasProperty('availableFrom'));
    }

    #[TestDox('AbstractDeliveryEvent::getAvailableFrom() exists')]
    public function testAbstractDeliveryEventGetAvailableFromExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('getAvailableFrom'));
    }

    #[Depends('testAbstractDeliveryEventGetAvailableFromExists')]
    #[TestDox('AbstractDeliveryEvent::getAvailableFrom() is public')]
    public function testAbstractDeliveryGetAvailableFromIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAvailableFrom');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventGetAvailableFromExists')]
    #[TestDox('AbstractDeliveryEvent::getAvailableFrom() has no parameters')]
    public function testAbstractDeliveryEventGetAvailableFromHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAvailableFrom');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDeliveryEventGetAvailableFromExists')]
    #[Depends('testAbstractDeliveryGetAvailableFromIsPublic')]
    #[Depends('testAbstractDeliveryEventGetAvailableFromHasNoParameters')]
    #[TestDox('AbstractDeliveryEvent::getAvailableFrom() returns null by default')]
    public function testAbstractDeliveryEventGetAvailableFromReturnsNullByDefault(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->getAvailableFrom());
    }

    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() exists')]
    public function testAbstractDeliveryEventSetAvailableFromExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('setAvailableFrom'));
    }

    #[Depends('testAbstractDeliveryEventSetAvailableFromExists')]
    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() is public')]
    public function testAbstractDeliverySetAvailableFromIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAvailableFrom');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableFromExists')]
    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() has one OPTIONAL parameter')]
    public function testAbstractDeliveryEventSetAvailableFromHasOneOptionalParameter()
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAvailableFrom');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableFromExists')]
    #[Depends('testAbstractDeliverySetAvailableFromIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAvailableFromHasOneOptionalParameter')]
    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() sets date as ISO string')]
    public function testAbstractDeliveryEventSetAvailableFromSetsDateAsIsoString()
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAvailableFrom('2027-03-10');
        $this->assertSame('2027-03-10', $deliveryEvent->getAvailableFrom());
    }

    /**
     * @see https://en.wikipedia.org/wiki/ISO_8601
     */
    #[Depends('testAbstractDeliveryEventSetAvailableFromExists')]
    #[Depends('testAbstractDeliverySetAvailableFromIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAvailableFromHasOneOptionalParameter')]
    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() sets date with time as ISO string')]
    public function testAbstractDeliveryEventSetAvailableFromSetsDateWithTimeAsIsoString(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};

        $deliveryEvent->setAvailableFrom('2027-03-10T12:00:00+00:00');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableFrom());

        $deliveryEvent->setAvailableFrom('2027-03-10T12:00:00Z');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableFrom());

        $deliveryEvent->setAvailableFrom('20270310T120000Z');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableFrom());
    }

    /**
     * @see https://dev.mysql.com/doc/refman/8.0/en/date-and-time-types.html
     */
    #[Depends('testAbstractDeliveryEventSetAvailableFromSetsDateWithTimeAsIsoString')]
    #[TestDox('AbstractDeliveryEvent::setAvailableFrom() sets date with time in MySQL format')]
    public function testAbstractDeliveryEventSetAvailableFromSetsDatetimeInMysqlFormat(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAvailableFrom('2027-03-10 12:00:00');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableFrom());
    }


    #[TestDox('AbstractDeliveryEvent.availableThrough exists')]
    public function testAbstractDeliveryEventAvailableThroughExists(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertObjectHasProperty('availableThrough', $deliveryEvent);
    }

    #[TestDox('AbstractDeliveryEvent::getAvailableThrough() exists')]
    public function testAbstractDeliveryEventGetAvailableThroughExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('getAvailableThrough'));
    }

    #[Depends('testAbstractDeliveryEventGetAvailableThroughExists')]
    #[TestDox('AbstractDeliveryEvent::getAvailableThrough() is public')]
    public function testAbstractDeliveryGetAvailableThroughIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAvailableThrough');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventGetAvailableThroughExists')]
    #[TestDox('AbstractDeliveryEvent::getAvailableThrough() has no parameters')]
    public function testAbstractDeliveryEventGetAvailableThroughHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getAvailableThrough');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDeliveryEventGetAvailableThroughExists')]
    #[Depends('testAbstractDeliveryGetAvailableThroughIsPublic')]
    #[Depends('testAbstractDeliveryEventGetAvailableThroughHasNoParameters')]
    #[TestDox('AbstractDeliveryEvent::getAvailableThrough() returns null by default')]
    public function testAbstractDeliveryEventGetAvailableThroughReturnsNullByDefault(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->getAvailableThrough());
    }


    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() exists')]
    public function testAbstractDeliveryEventSetAvailableThroughExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('setAvailableThrough'));
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughExists')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() is public')]
    public function testAbstractDeliverySetAvailableThroughIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAvailableThrough');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughExists')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() has one REQUIRED parameter')]
    public function testAbstractDeliveryEventSetAvailableThroughHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setAvailableThrough');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughExists')]
    #[Depends('testAbstractDeliverySetAvailableThroughIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAvailableThroughHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() sets date as ISO string')]
    public function testAbstractDeliveryEventSetAvailableThroughSetsDateAsIsoString(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAvailableThrough('2027-03-10');
        $this->assertSame('2027-03-10', $deliveryEvent->getAvailableThrough());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughExists')]
    #[Depends('testAbstractDeliverySetAvailableThroughIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAvailableThroughHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() sets date with time as ISO string')]
    public function testAbstractDeliveryEventSetAvailableThroughSetsDateWithTimeAsIsoString(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};

        $deliveryEvent->setAvailableThrough('2027-03-10T12:00:00+00:00');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableThrough());

        $deliveryEvent->setAvailableThrough('2027-03-10T12:00:00Z');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableThrough());

        $deliveryEvent->setAvailableThrough('20270310T120000Z');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableThrough());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughExists')]
    #[Depends('testAbstractDeliverySetAvailableThroughIsPublic')]
    #[Depends('testAbstractDeliveryEventSetAvailableThroughHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() sets date with time in MySQL format')]
    public function testAbstractDeliveryEventSetAvailableThroughSetsDateWithTimeInMysqlFormat(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAvailableThrough('2027-03-10 12:00:00');
        $this->assertSame('2027-03-10T12:00:00+00:00', $deliveryEvent->getAvailableThrough());
    }

    #[Depends('testAbstractDeliveryEventSetAvailableThroughSetsDateAsIsoString')]
    #[TestDox('AbstractDeliveryEvent::setAvailableThrough() adds date interval as object')]
    public function testAbstractDeliveryEventSetAvailableThroughAddsDateIntervalAsObject(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $deliveryEvent->setAvailableFrom('2027-03-10');
        $fourteen_days = new \DateInterval('P14D');
        $deliveryEvent->setAvailableThrough($fourteen_days);
        $this->assertSame('2027-03-24', $deliveryEvent->getAvailableThrough());
    }


    #[TestDox('AbstractDeliveryEvent::getDeliveryMethod() exists')]
    public function testAbstractDeliveryEventGetDeliveryMethodExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('getDeliveryMethod'));
    }

    #[Depends('testAbstractDeliveryEventGetDeliveryMethodExists')]
    #[TestDox('AbstractDeliveryEvent::getDeliveryMethod() is public')]
    public function testAbstractDeliveryGetDeliveryMethodIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getDeliveryMethod');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventGetDeliveryMethodExists')]
    #[TestDox('AbstractDeliveryEvent::getDeliveryMethod() has no parameters')]
    public function testAbstractDeliveryEventGetDeliveryMethodHasNoParameters(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'getDeliveryMethod');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testAbstractDeliveryEventGetDeliveryMethodExists')]
    #[Depends('testAbstractDeliveryGetDeliveryMethodIsPublic')]
    #[Depends('testAbstractDeliveryEventGetDeliveryMethodHasNoParameters')]
    #[TestDox('AbstractDeliveryEvent::getDeliveryMethod() returns null by default')]
    public function testAbstractDeliveryEventGetDeliveryMethodReturnsNullByDefault(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->getDeliveryMethod());
    }


    #[TestDox('AbstractDeliveryEvent::setDeliveryMethod() exists')]
    public function testAbstractDeliveryEventSetDeliveryMethodExists(): void
    {
        $class = new ReflectionClass(AbstractDeliveryEvent::class);
        $this->assertTrue($class->hasMethod('setDeliveryMethod'));
    }

    #[Depends('testAbstractDeliveryEventSetDeliveryMethodExists')]
    #[TestDox('AbstractDeliveryEvent::setDeliveryMethod() is public')]
    public function testAbstractDeliverySetDeliveryMethodIsPublic(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setDeliveryMethod');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testAbstractDeliveryEventSetDeliveryMethodExists')]
    #[TestDox('AbstractDeliveryEvent::setDeliveryMethod() has one REQUIRED parameter')]
    public function testAbstractDeliveryEventSetDeliveryMethodHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(AbstractDeliveryEvent::class, 'setDeliveryMethod');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testAbstractDeliveryEventSetDeliveryMethodExists')]
    #[Depends('testAbstractDeliverySetDeliveryMethodIsPublic')]
    #[Depends('testAbstractDeliveryEventSetDeliveryMethodHasOneRequiredParameter')]
    #[TestDox('AbstractDeliveryEvent::setDeliveryMethod() sets DeliveryMethod objects')]
    public function testAbstractDeliveryEventSetDeliveryMethodSetsDeliveryMethodObjects(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->getDeliveryMethod());

        $click_and_collect = new \StoreCore\OML\DeliveryMethod();
        $deliveryEvent->setDeliveryMethod($click_and_collect);
        $this->assertIsObject($deliveryEvent->getDeliveryMethod());
        $this->assertEquals($click_and_collect, $deliveryEvent->getDeliveryMethod());

        $buy_online_pickup_in_store = new \StoreCore\OML\OnSitePickup();
        $deliveryEvent->setDeliveryMethod($buy_online_pickup_in_store);
        $this->assertNotEquals($click_and_collect, $deliveryEvent->getDeliveryMethod());
        $this->assertEquals($buy_online_pickup_in_store, $deliveryEvent->getDeliveryMethod());
    }

    #[Depends('testAbstractDeliveryEventSetDeliveryMethodSetsDeliveryMethodObjects')]
    #[TestDox('AbstractDeliveryEvent::setDeliveryMethod() sets DeliveryEvent.deliveryMethod')]
    public function testAbstractDeliveryEventSetDeliveryMethodSetsDeliveryEventDeliveryMethod(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $this->assertNull($deliveryEvent->deliveryMethod);

        $curbside_pickup = new \StoreCore\OML\OnSitePickup();
        $deliveryEvent->setDeliveryMethod($curbside_pickup);
        $this->assertNotNull($deliveryEvent->deliveryMethod);
        $this->assertEquals($curbside_pickup, $deliveryEvent->deliveryMethod);
    }

    #[Depends('testAbstractDeliveryEventSetDeliveryMethodSetsDeliveryMethodObjects')]
    #[TestDox('Schema.org DeliveryEvent.hasDeliveryMethod is alias of DeliveryEvent.deliveryMethod')]
    public function testSchemaOrgDeliveryEventHasDeliveryMethodIsAliasOfDeliveryEventDeliveryMethod(): void
    {
        $deliveryEvent = new class extends AbstractDeliveryEvent {};
        $pickup_in_store = new \StoreCore\OML\OnSitePickup();
        $deliveryEvent->setDeliveryMethod($pickup_in_store);

        $this->assertEquals($deliveryEvent->hasDeliveryMethod, $deliveryEvent->deliveryMethod);
        $this->assertEquals($pickup_in_store, $deliveryEvent->deliveryMethod);
        $this->assertEquals($pickup_in_store, $deliveryEvent->hasDeliveryMethod);
    }
}
