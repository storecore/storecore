<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\OML\BackOrder::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\OML\OrderStatus::class)]
final class BackOrderTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('BackOrder class is concrete')]
    public function testBackOrderClassIsConcrete(): void
    {
        $class = new ReflectionClass(BackOrder::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('BackOrder is a model')]
    public function testBackOrderIsModel(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $this->assertInstanceOf(\StoreCore\AbstractModel::class, $backOrder);
        $this->assertNotInstanceOf(\StoreCore\Database\AbstractModel::class, $backOrder);
    }

    #[Group('hmvc')]
    #[TestDox('BackOrder is an Order model')]
    public function testBackOrderIsOrderModel(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $this->assertInstanceOf(\StoreCore\Order::class, $backOrder);
    }


    #[TestDox('BackOrder is countable')]
    public function testBackOrderIsCountable(): void
    {
        $class = new ReflectionClass(BackOrder::class);
        $this->assertTrue($class->hasMethod('count'));
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }

    #[Depends('testBackOrderIsCountable')]
    #[TestDox('BackOrder::count() returns (int) 0 by default')]
    public function testdoxBackOrderCountReturnsInt0ByDefault(): void
    {
        $registry = Registry::getInstance();
        $order = new BackOrder($registry);
        $this->assertEquals(0, $order->count());
        $this->assertEquals(0, count($order));
        $this->assertCount(0, $order);
    }


    #[Group('hmvc')]
    #[TestDox('BackOrder implements StoreCore IdentityInterface')]
    public function testBackOrderImplementsStoreCoreIdentityInterface(): void
    {
        $class = new ReflectionClass(BackOrder::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\IdentityInterface::class));
    }

    #[Group('hmvc')]
    #[TestDox('BackOrder implements StoreCore VisitableInterface')]
    public function testBackOrderImplementsStoreCoreVisitableInterface(): void
    {
        $class = new ReflectionClass(BackOrder::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\VisitableInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BackOrder::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BackOrder::VERSION);
        $this->assertIsString(BackOrder::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BackOrder::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('hmvc')]
    #[TestDox('BackOrder.orderStatus exists')]
    public function testBackOrderOrderStatusExists(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $this->assertObjectHasProperty('orderStatus', $backOrder);
    }

    #[Depends('testBackOrderOrderStatusExists')]
    #[TestDox('BackOrder.orderStatus is not null')]
    public function testBackOrderOrderStatusIsNotNull(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $this->assertNotNull(
            $backOrder->orderStatus,
            'Because a `BackOrder` is an `Order` with a special state, the `OrderStatus` SHOULD NOT be unknown by default.'
        );
    }

    #[Depends('testBackOrderOrderStatusIsNotNull')]
    #[TestDox('BackOrder.orderStatus is OrderProcessing by default')]
    public function testBackOrderOrderStatusIsOrderProcessingByDefault(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $this->assertEquals('OrderProcessing', $backOrder->orderStatus->name);
        $this->assertEquals('https://schema.org/OrderProcessing', $backOrder->orderStatus->value);
    }

    #[Depends('testBackOrderOrderStatusIsOrderProcessingByDefault')]
    #[TestDox('BackOrder.orderStatus can be changed')]
    public function testBackOrderOrderStatusCanBeChanged(): void
    {
        $registry = Registry::getInstance();
        $backOrder = new BackOrder($registry);
        $backOrder->orderStatus = OrderStatus::OrderCancelled;
        $this->assertNotEquals('https://schema.org/OrderProcessing', $backOrder->orderStatus->value);
        $this->assertEquals('https://schema.org/OrderCancelled', $backOrder->orderStatus->value);
    }
}
