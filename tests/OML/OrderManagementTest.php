<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Order::class)]
#[CoversClass(\StoreCore\OML\OrderManagement::class)]
#[CoversClass(\StoreCore\OML\OrderRepository::class)]
#[CoversClass(\StoreCore\Database\OrderMapper::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Database\OrderNumbers::class)]
#[UsesClass(\StoreCore\OML\OrderFactory::class)]
#[UsesClass(\StoreCore\OML\OrderStatus::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrderManagementTest extends TestCase
{
    public function setUp(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $registry->get('Database')->exec(
                "DELETE FROM `sc_orders` WHERE `order_number` = '026-1520163-6049104'"
            );
        } catch (\PDOException $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    #[Group('hmvc')]
    #[TestDox('OrderManagement class is concrete')]
    public function testOrderManagementClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderManagement::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrderManagement is a controller')]
    public function testOrderManagementIsController(): void
    {
        $class = new ReflectionClass(OrderManagement::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\AbstractController::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderManagement::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderManagement::VERSION);
        $this->assertIsString(OrderManagement::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderManagement::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderManagement::confirm exists')]
    public function testOrderManagementConfirmExists(): void
    {
        $class = new ReflectionClass(OrderManagement::class);
        $this->assertTrue($class->hasMethod('confirm'));
    }

    #[TestDox('OrderManagement::confirm is public')]
    public function testOrderManagementConfirmIsPublic(): void
    {
        $method = new ReflectionMethod(OrderManagement::class, 'confirm');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OrderManagement::confirm has one REQUIRED parameter')]
    public function testOrderManagementConfirmHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderManagement::class, 'confirm');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('OrderManagement::confirm sets order numbers and order date')]
    public function testOrderManagementConfirmSetsOrderNumbersAndOrderDate(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $order = $factory->createOrder();
        $this->assertIsObject($order);
        $this->assertInstanceOf(\StoreCore\Order::class, $order);

        $this->assertNull($order->confirmationNumber);
        $this->assertNull($order->orderDate);
        $this->assertNull($order->orderNumber);

        $this->assertTrue($order->hasIdentifier());
        $uuid = (string) $order->getIdentifier();

        $repository = new OrderRepository($registry);
        $this->assertTrue(
            $repository->has($uuid),
            'Order with UUID ' . $uuid . ' MUST now be stored.'
        );

        /*
         * @todo
         *   This MAY need some more work: it is possible to confirm an order
         *   (and thereby add an order number and set an order date) if the
         *   order contains no known products or services.
         */
        $orderManagement = new OrderManagement($registry);
        $this->assertTrue($orderManagement->confirm($order));

        $this->assertNotNull($order->confirmationNumber);
        $this->assertNotNull($order->orderDate);
        $this->assertNotNull($order->orderNumber);

        $this->assertIsInt($order->orderNumber);
        $this->assertGreaterThanOrEqual(5000, $order->orderNumber);
        $this->assertLessThanOrEqual(2147483647, $order->orderNumber);

        $this->assertInstanceOf(\DateTimeInterface::class, $order->orderDate);
        $this->assertInstanceOf(\DateTimeImmutable::class, $order->orderDate);
        $this->assertEquals($repository->now(), $order->orderDate);
    }

    #[Depends('testOrderManagementConfirmSetsOrderNumbersAndOrderDate')]
    #[TestDox('OrderManagement::confirm does not overwrite existing order number')]
    public function testOrderManagementConfirmDoesNotOverwriteExistingOrderNumber(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $order = $factory->createOrder();
        $this->assertNull($order->orderNumber);
        $this->assertNull($order->orderDate);

        $order->orderNumber = '026-1520163-6049104';
        $this->assertNotEmpty($order->orderNumber);

        $orderManagement = new OrderManagement($registry);
        $this->assertTrue($orderManagement->confirm($order));

        $repository = new OrderRepository($registry);
        $this->assertTrue($repository->has('026-1520163-6049104'));

        $this->assertNotEmpty($order->orderNumber);
        $this->assertIsString($order->orderNumber);
        $this->assertEquals('026-1520163-6049104', $order->orderNumber);

        $this->assertNotNull($order->orderDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $order->orderDate);
        $this->assertInstanceOf(\DateTimeImmutable::class, $order->orderDate);
        $this->assertEquals($repository->now(), $order->orderDate);
    }

    #[Depends('testOrderManagementConfirmSetsOrderNumbersAndOrderDate')]
    #[TestDox('OrderManagement::confirm does not confirm orders with an order problem')]
    public function testOrderManagementConfirmDoesNotConfirmOrdersWithOrderProblem(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrderFactory($registry);
        $order = $factory->createOrder();
        $this->assertEmpty($order->orderStatus);

        $order->orderStatus = OrderStatus::OrderProblem;
        $this->assertNotEmpty($order->orderStatus);

        $orderManagement = new OrderManagement($registry);
        $this->assertFalse($orderManagement->confirm($order));

        $this->assertEmpty($order->confirmationNumber);
        $this->assertEmpty($order->orderDate);
        $this->assertEmpty($order->orderNumber);
    }
}
