<?php

declare(strict_types=1);

namespace StoreCore\OML;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('hmvc')]
final class DeliveryEventInterfaceTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('DeliveryEventInterface interface file is readable')]
    public function testDeliveryEventInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'DeliveryEventInterface.php'
        );
    }

    #[Depends('testDeliveryEventInterfaceInterfaceFileExists')]
    #[Group('distro')]
    #[TestDox('DeliveryEventInterface interface file is readable')]
    public function testDeliveryEventInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'DeliveryEventInterface.php'
        );
    }

    #[Depends('testDeliveryEventInterfaceInterfaceFileExists')]
    #[Depends('testDeliveryEventInterfaceInterfaceFileIsReadable')]
    #[TestDox('DeliveryEventInterface interface exists')]
    public function testDeliveryEventInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\OML\\DeliveryEventInterface'));
        $this->assertTrue(interface_exists(DeliveryEventInterface::class));
    }
}
