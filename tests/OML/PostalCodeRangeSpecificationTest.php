<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\OML;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\PostalCodeRangeSpecification::class)]
#[CoversClass(\StoreCore\OML\PostalCode::class)]
final class PostalCodeRangeSpecificationTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('PostalCodeRangeSpecification class is readable')]
    public function testPostalCodeRangeSpecificationClassIsReadable(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalCodeRangeSpecification.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'OML' . DIRECTORY_SEPARATOR . 'PostalCodeRangeSpecification.php');

        $this->assertTrue(class_exists('\\StoreCore\\OML\\PostalCodeRangeSpecification'));
        $this->assertTrue(class_exists(PostalCodeRangeSpecification::class));
    }

    #[Group('hmvc')]
    #[TestDox('PostalCodeRangeSpecification class is concrete')]
    public function testPostalCodeRangeSpecificationClassIsConcrete(): void
    {
        $class = new ReflectionClass(PostalCodeRangeSpecification::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Depends('testPostalCodeRangeSpecificationClassIsConcrete')]
    #[TestDox('PostalCodeRangeSpecification class is read-only')]
    public function testPostalCodeRangeSpecificationClassIsReadOnly(): void
    {
        $class = new ReflectionClass(PostalCodeRangeSpecification::class);
        $this->assertTrue($class->isReadOnly());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PostalCodeRangeSpecification::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PostalCodeRangeSpecification::VERSION);
        $this->assertIsString(PostalCodeRangeSpecification::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PostalCodeRangeSpecification::VERSION, '1.0.0-beta.1', '>=')
        );
    }


    #[TestDox('PostalCodeRangeSpecification::__construct exists')]
    public function testPostalCodeRangeSpecificationConstructorExists()
    {
        $class = new ReflectionClass(PostalCodeRangeSpecification::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testPostalCodeRangeSpecificationConstructorExists')]
    #[TestDox('PostalCodeRangeSpecification::__construct is public constructor')]
    public function testPostalCodeRangeSpecificationConstructIsPublicConstructor()
    {
        $method = new ReflectionMethod(PostalCodeRangeSpecification::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testPostalCodeRangeSpecificationConstructorExists')]
    #[TestDox('PostalCodeRangeSpecification::__construct has two REQUIRED parameters')]
    public function testPostalCodeRangeSpecificationConstructorHasTwoRequiredParameters()
    {
        $method = new ReflectionMethod(PostalCodeRangeSpecification::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(2, $method->getNumberOfRequiredParameters());
    }

    /**
     * @see https://nl.wikipedia.org/wiki/Lijst_van_postcodes_8000-8999_in_Nederland
     */
    #[Depends('testPostalCodeRangeSpecificationConstructorHasTwoRequiredParameters')]
    #[TestDox('PostalCodeRangeSpecification::__construct sets postalCodeBegin and postalCodeEnd')]
    public function testPostalCodeRangeSpecificationConstructorSetsPostalCodeBeginAndPostalCodeEnd()
    {
        $island = new PostalCodeRangeSpecification('8881 AA', '8897 ZZ');

        $this->assertObjectHasProperty('postalCodeBegin', $island);
        $this->assertInstanceOf(\Stringable::class, $island->postalCodeBegin);
        $this->assertInstanceOf(\StoreCore\OML\PostalCode::class, $island->postalCodeBegin);
        $this->assertEquals('8881 AA', (string) $island->postalCodeBegin);

        $this->assertObjectHasProperty('postalCodeEnd', $island);
        $this->assertInstanceOf(\Stringable::class, $island->postalCodeEnd);
        $this->assertInstanceOf(\StoreCore\OML\PostalCode::class, $island->postalCodeEnd);
        $this->assertEquals('8897 ZZ', (string) $island->postalCodeEnd);

        $this->assertTrue((string) $island->postalCodeBegin < (string) $island->postalCodeEnd);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "PostalCodeRangeSpecification",
              "postalCodeBegin": "8881 AA",
              "postalCodeEnd": "8897 ZZ"
            }
        ';
        $actualJson = json_encode($island, \JSON_UNESCAPED_SLASHES | \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
