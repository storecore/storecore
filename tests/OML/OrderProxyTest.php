<?php

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\Types\UUID;


use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\OML\OrderProxy::class)]
#[CoversClass(\StoreCore\Proxy::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class OrderProxyTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OrderProxy class is concrete')]
    public function testOrderProxyClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderProxy::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('hmvc')]
    #[TestDox('OrderProxy is a Proxy')]
    public function testOrderProxyIsProxy(): void
    {
        $proxy = new OrderProxy('00000000-0000-0000-0000-000000000000');
        $this->assertInstanceOf(\StoreCore\OML\OrderProxy::class, $proxy);
        $this->assertInstanceOf(\StoreCore\Proxy::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('OrderProxy implements StoreCore IdentityInterface')]
    public function testOrderProxyImplementsStoreCoreIdentityInterface(): void
    {
        $proxy = new OrderProxy('00000000-0000-0000-0000-000000000000');
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
    }

    #[Depends('testOrderProxyImplementsStoreCoreIdentityInterface')]
    #[Group('hmvc')]
    #[TestDox('OrderProxy implements StoreCore ProxyInterface')]
    public function testOrderProxyImplementsStoreCoreProxyInterface(): void
    {
        $proxy = new OrderProxy('00000000-0000-0000-0000-000000000000');
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('OrderProxy is JSON serializable')]
    public function testOrderProxyIsJsonSerializable(): void
    {
        $proxy = new OrderProxy('00000000-0000-0000-0000-000000000000');
        $this->assertInstanceOf(\JsonSerializable::class, $proxy);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderProxy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderProxy::VERSION);
        $this->assertIsString(OrderProxy::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderProxy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrderProxy::__construct exists')]
    public function testOrderProxyConstructExists(): void
    {
        $class = new ReflectionClass(OrderProxy::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('OrderProxy::__construct is public constructor')]
    public function testOrderProxyConstructIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(OrderProxy::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('OrderProxy::__construct has four parameters')]
    public function testOrderProxyConstructorHasFourParameters(): void
    {
        $method = new ReflectionMethod(OrderProxy::class, '__construct');
        $this->assertNotEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(4, $method->getNumberOfParameters());
    }

    #[Depends('testOrderProxyConstructorHasFourParameters')]
    #[TestDox('OrderProxy::__construct has one REQUIRED parameter')]
    public function testOrderProxyConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderProxy::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderProxyConstructorHasOneRequiredParameter')]
    #[Depends('testOrderProxyIsJsonSerializable')]
    #[TestDox('OrderProxy::__construct sets Order.identifier as UUID')]
    public function testOrderProxyConstructorSetsOrderIdentifierAsUUID(): void
    {
        $uuid = new UUID('f4ddfb73-6ea4-4e8f-862d-a0807a267f11');
        $proxy = new OrderProxy($uuid);
        $this->assertTrue($proxy->hasIdentifier());
        $this->assertEquals($uuid, $proxy->getIdentifier());
        $this->assertEquals('f4ddfb73-6ea4-4e8f-862d-a0807a267f11', $proxy->getIdentifier()->__toString());

        $json = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($json);
        $this->assertStringContainsString('"@id": "/api/v1/orders/f4ddfb73-6ea4-4e8f-862d-a0807a267f11"', $json);
        $this->assertStringContainsString('"identifier": "f4ddfb73-6ea4-4e8f-862d-a0807a267f11"', $json);
    }

    #[Depends('testOrderProxyConstructorHasOneRequiredParameter')]
    #[Depends('testOrderProxyIsJsonSerializable')]
    #[TestDox('OrderProxy::__construct sets Order.orderNumber and Order.confirmationNumber')]
    public function testOrderProxyConstructorSetsOrderOrderNumberAndOrderConfirmationNumber(): void
    {
        $proxy = new OrderProxy(
            '64e082a7-cf3b-4449-9a1e-74fc0e143c84',
            'Order',
            '12345',
            '678-901234-567890',
        );
        $this->assertEquals('12345', $proxy->orderNumber);
        $this->assertEquals('678-901234-567890', $proxy->confirmationNumber);

        $expectedJson = trim('
            {
              "@id": "/api/v1/orders/64e082a7-cf3b-4449-9a1e-74fc0e143c84",
              "@context": "https://schema.org",
              "@type": "Order",
              "confirmationNumber": "678-901234-567890",
              "identifier": "64e082a7-cf3b-4449-9a1e-74fc0e143c84",
              "orderNumber": "12345"
            }
        ');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
