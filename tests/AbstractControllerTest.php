<?php

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\AbstractController::class)]
#[Group('hmvc')]
final class AbstractControllerTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('AbstractController class file exists')]
    public function testAbstractControllerClassFileExists(): void
    {
        $this->assertFileExists(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractController.php'
        );
    }

    #[Depends('testAbstractControllerClassFileExists')]
    #[Group('distro')]
    #[TestDox('AbstractController class file is readable')]
    public function testAbstractControllerClassFileIsReadable(): void
    {
        $this->assertFileIsReadable(
            STORECORE_FILESYSTEM_SRC_DIR . 'AbstractController.php'
        );
    }

    #[Depends('testAbstractControllerClassFileExists')]
    #[Depends('testAbstractControllerClassFileIsReadable')]
    #[Group('distro')]
    #[TestDox('AbstractController class exists')]
    public function testAbstractControllerClassExists(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\AbstractController'));
        $this->assertTrue(class_exists(AbstractController::class));
    }


    #[TestDox('AbstractController class is abstract')]
    public function testProductClassIsConcrete(): void
    {
        $class = new ReflectionClass(AbstractController::class);
        $this->assertTrue($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertFalse($class->isInstantiable());
    }
}
