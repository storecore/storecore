<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\PIM\Product;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\OrderItem::class)]
#[CoversClass(\StoreCore\OrderItems::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrderItemsTest extends TestCase
{
    #[TestDox('OrderItems class is concrete')]
    public function testOrderItemsClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderItems::VERSION);
        $this->assertIsString(OrderItems::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrderItems::VERSION, '0.1.0', '>=')
        );
    }


    #[TestDox('OrderItems::count exists')]
    public function testOrderItemsCountExists(): void
    {
        $class = new ReflectionClass(OrderItems::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[TestDox('OrderItems::count is public')]
    public function testOrderItemsCountIsPublic(): void
    {
        $method = new ReflectionMethod(OrderItems::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('OrderItems::count has one OPTIONAL parameter')]
    public function testOrderItemsCountHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(OrderItems::class, 'count');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('OrderItems::count returns integer')]
    public function testOrderItemsCountReturnsInteger(): void
    {
        $orderItems = new OrderItems();
        $this->assertIsInt($orderItems->count());
    }

    #[Depends('testOrderItemsCountReturnsInteger')]
    #[TestDox('OrderItems::count returns (int) 0 by default')]
    public function testOrderItemsCountReturnsInt0ByDefault(): void
    {
        $orderItems = new OrderItems();
        $this->assertEmpty($orderItems);
        $this->assertIsInt($orderItems->count());
        $this->assertEquals(0, $orderItems->count());
    }

    #[Depends('testOrderItemsCountExists')]
    #[Depends('testOrderItemsCountIsPublic')]
    #[Depends('testOrderItemsCountReturnsInteger')]
    #[TestDox('OrderItems is countable')]
    public function testOrderItemsIsCountable(): void
    {
        $orderItems = new OrderItems();
        $this->assertInstanceOf(\Countable::class, $orderItems);
        $this->assertIsInt(count($orderItems));
        $this->assertEquals(0, count($orderItems));
    }

    #[TestDox('OrderItems::count counts normal or recursive')]
    public function testOrderItemsCountCountsNormalOrRecursive(): void
    {
        $ean = '87148169';
        $uuid = UUIDFactory::createFromString($ean);
        $product = new Product();
        $product->setIdentifier($uuid);

        // Fetch 6 products.
        $single_order_item = new OrderItem($product, 6);
        $this->assertSame(6, $single_order_item->count());

        // Create an empty collection of order items.
        $order_items_collection = new OrderItems();
        $this->assertEmpty($order_items_collection);
        $this->assertEquals(0, $order_items_collection->count());

        // Add the 6 products to the collection.
        $order_items_collection->attach($single_order_item);
        $this->assertNotEmpty($order_items_collection);

        // We now MUST have 1 order line and 6 ordered items.
        $this->assertEquals(1, $order_items_collection->count());
        $this->assertEquals(1, $order_items_collection->count(\COUNT_NORMAL));
        $this->assertEquals(6, $order_items_collection->count(\COUNT_RECURSIVE));
    }
}
