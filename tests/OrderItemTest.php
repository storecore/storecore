<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\OML\OrderStatus;
use StoreCore\OML\ParcelDelivery;
use StoreCore\PIM\{IndividualProduct, Product, Service};
use StoreCore\Types\{Date, DateTime};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\OrderItem::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\OML\ParcelDelivery::class)]
#[UsesClass(\StoreCore\PIM\Product::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
#[UsesClass(\StoreCore\Types\DateTime::class)]
final class OrderItemTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OrderItem class is concrete')]
    public function testOrderItemClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrderItem::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrderItem::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrderItem::VERSION);
        $this->assertIsString(OrderItem::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(version_compare(OrderItem::VERSION, '0.1.1', '>='));
    }


    /**
     * @see https://schema.org/orderDelivery
     *      Schema.org `orderDelivery` property of an `Order` or `OrderItem`
     */
    #[TestDox('OrderItem.orderDelivery exists')]
    public function testOrderItemOrderDeliveryExists(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertObjectHasProperty('orderDelivery', $orderItem);
    }

    #[Depends('testOrderItemOrderDeliveryExists')]
    #[TestDox('OrderItem.orderDelivery is public')]
    public function testOrderItemOrderDeliveryIsPublic(): void
    {
        $property = new ReflectionProperty(OrderItem::class, 'orderDelivery');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrderItemOrderDeliveryExists')]
    #[Depends('testOrderItemOrderDeliveryIsPublic')]
    #[TestDox('OrderItem.orderDelivery is null by default')]
    public function testOrderItemOrderDeliveryIsNullByDefault(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertNull($orderItem->orderDelivery);

        $property = new ReflectionProperty(OrderItem::class, 'orderDelivery');
        $this->assertTrue($property->isDefault());
    }


    /**
     * @see https://schema.org/orderItemNumber
     *      Schema.org `orderItemNumber` property of an `OrderItem`
     */
    #[TestDox('OrderItem.orderItemNumber exists')]
    public function testOrderItemOrderItemNumberExists(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertObjectHasProperty('orderItemNumber', $orderItem);
    }

    #[Depends('testOrderItemOrderItemNumberExists')]
    #[TestDox('OrderItem.orderItemNumber is public read-only')]
    public function testOrderItemOrderItemNumberIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(OrderItem::class, 'orderItemNumber');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }


    /**
     * @see https://schema.org/orderItemStatus
     */
    #[TestDox('OrderItem.orderItemStatus exists')]
    public function testOrderItemOrderItemStatusExists(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertObjectHasProperty('orderItemStatus', $orderItem);
    }

    #[Depends('testOrderItemOrderedItemExists')]
    #[TestDox('OrderItem.orderItemStatus is public')]
    public function testOrderItemorderItemStatusIsPublic(): void
    {
        $property = new ReflectionProperty(OrderItem::class, 'orderItemStatus');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrderItemOrderedItemExists')]
    #[TestDox('OrderItem.orderItemStatus is null and empty by default')]
    public function testOrderItemorderItemStatusIsNullAndEmptyByDefault(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertNull($orderItem->orderItemStatus);
        $this->assertEmpty($orderItem->orderItemStatus);

        $property = new ReflectionProperty(OrderItem::class, 'orderItemStatus');
        $this->assertTrue($property->isDefault());
    }

    #[Depends('testOrderItemOrderedItemExists')]
    #[Depends('testOrderItemorderItemStatusIsPublic')]
    #[Depends('testOrderItemorderItemStatusIsNullAndEmptyByDefault')]
    #[TestDox('OrderItem.orderItemStatus accepts OrderStatus')]
    public function testOrderItemOrderItemStatusAcceptsOrderStatus(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);

        $this->assertTrue(enum_exists(OrderStatus::class));
        $orderItem->orderItemStatus = OrderStatus::OrderDelivered;

        $this->assertNotNull($orderItem->orderItemStatus);
        $this->assertInstanceOf(\StoreCore\OML\OrderStatus::class, $orderItem->orderItemStatus);
        $this->assertIsString($orderItem->orderItemStatus->value);
        $this->assertEquals('https://schema.org/OrderDelivered', $orderItem->orderItemStatus->value);
    }


    /**
     * @see https://schema.org/orderedItem
     */
    #[TestDox('OrderItem.orderedItem exists')]
    public function testOrderItemOrderedItemExists(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertObjectHasProperty('orderedItem', $orderItem);
    }

    #[Depends('testOrderItemOrderedItemExists')]
    #[TestDox('OrderItem.orderedItem is public read-only')]
    public function testOrderItemOrderedItemIsPublicReadOnly(): void
    {
        $property = new ReflectionProperty(OrderItem::class, 'orderedItem');
        $this->assertTrue($property->isPublic());
        $this->assertTrue($property->isReadOnly());
    }


    #[TestDox('OrderItem::__construct exists')]
    public function testOrderItemConstructorExists(): void
    {
        $class = new ReflectionClass(OrderItem::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testOrderItemConstructorExists')]
    #[TestDox('OrderItem::__construct is public constructor')]
    public function testOrderItemConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(OrderItem::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testOrderItemConstructorExists')]
    #[TestDox('OrderItem::__construct has two parameters')]
    public function testOrderItemConstructorHasTwoParameters(): void
    {
        $method = new ReflectionMethod(OrderItem::class, '__construct');
        $this->assertEquals(2, $method->getNumberOfParameters());
    }

    #[Depends('testOrderItemConstructorHasTwoParameters')]
    #[TestDox('OrderItem::__construct has one REQUIRED parameter')]
    public function testOrderItemConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrderItem::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrderItemConstructorHasTwoParameters')]
    #[TestDox('OrderItem::__construct accepts OrderItem but sets Product or Service')]
    public function testOrderItemConstructorAcceptsOrderItemButSetsProductOrService(): void
    {
        $apple = new Product();
        $apple->name = 'Red Delicious';

        // Given that some fruit consists of six apples …
        $fruit = new OrderItem($apple, 6);
        $this->assertInstanceOf(\StoreCore\PIM\Product::class, $fruit->orderedItem);
        $this->assertEquals(6, count($fruit));

        // … when we order twice the amount of fruit …
        $basket = new OrderItem($fruit, 2);

        // … then we get twelve apples.
        $this->assertInstanceOf(\StoreCore\PIM\Product::class, $basket->orderedItem);
        $this->assertNotInstanceOf(\StoreCore\OrderItem::class, $basket->orderedItem);
        $this->assertEquals(12, count($basket));
        $this->assertEquals('Red Delicious', $basket->orderedItem->name);
    }


    /**
     * @see https://schema.org/orderQuantity
     */
    #[TestDox('OrderItem.orderQuantity exists')]
    public function testOrderItemOrderQuantityExists(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertObjectHasProperty('orderQuantity', $orderItem);
    }

    #[Depends('testOrderItemOrderQuantityExists')]
    #[TestDox('OrderItem.orderQuantity is readable and writeable')]
    public function testOrderItemOrderQuantityIsReadableAndWriteable(): void
    {
        $property = new ReflectionProperty(OrderItem::class, 'orderQuantity');
        $this->assertFalse($property->isPublic());
        $this->assertFalse($property->isReadOnly());

        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertNotEmpty($orderItem->orderQuantity);
        $this->assertNotNull($orderItem->orderQuantity);

        $orderItem->orderQuantity = 42;
        $this->assertEquals(42, $orderItem->orderQuantity);
    }

    #[Depends('testOrderItemOrderQuantityIsReadableAndWriteable')]
    #[TestDox('OrderItem.orderQuantity is (int) 1 by default')]
    public function testOrderItemOrderQuantityIsInt1ByDefault(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertIsInt($orderItem->orderQuantity);
        $this->assertEquals(1, $orderItem->orderQuantity);
    }

    /**
     * @see https://mariadb.com/kb/en/smallint/
     * @see https://dev.mysql.com/doc/refman/8.2/en/integer-types.html
     */
    #[Depends('testOrderItemOrderQuantityIsReadableAndWriteable')]
    #[TestDox('OrderItem.orderQuantity is not greater than SMALLINT UNSIGNED maximum')]
    public function testOrderItemOrderQuantityIsNotGreaterThanSmallintUnsignedMaximum(): void
    {
        $product = new Product();

        $orderItem = new OrderItem($product, 65535);
        $this->assertEquals(65535, $orderItem->orderQuantity);

        $this->expectException(\DomainException::class);
        $orderItem = new OrderItem($product, 65536);
    }

    #[Depends('testOrderItemOrderQuantityIsInt1ByDefault')]
    #[TestDox('OrderItem.orderQuantity is set by constructor')]
    public function testOrderItemOrderQuantityIsSetByConstructor(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product, 42);
        $this->assertNotEquals(1, $orderItem->orderQuantity);
        $this->assertEquals(42, $orderItem->orderQuantity);
    }

    #[Depends('testOrderItemOrderQuantityIsSetByConstructor')]
    #[TestDox('OrderItem.orderQuantity is always 1 on IndividualProduct')]
    public function testOrderItemOrderQuantityIsAlways1OnIndividualProduct(): void
    {
        $individualProduct = new IndividualProduct();
        $individualProduct->serialNumber = 'CONC91000937';

        $orderItem = new OrderItem($individualProduct, 42);
        $this->assertNotEquals(42, $orderItem->orderQuantity);
        $this->assertEquals(1, $orderItem->orderQuantity);
    }

    #[Depends('testOrderItemOrderQuantityIsSetByConstructor')]
    #[TestDox('OrderItem.orderQuantity can not be negative')]
    public function testOrderItem0rderQuantityCanNotBeNegative(): void
    {
        $product = new Product();
        $returnedItems = new OrderItem($product, -3);
        $this->assertNotEquals(-3, $returnedItems->orderQuantity);
        $this->assertEquals(3, $returnedItems->orderQuantity);
    }

    /**
     * If you want to remove an `OrderItem` from an `Order`, then you
     * SHOULD NOT set the `orderQuantity` to `0`, but remove the `OrderItem`
     * object from the `Order` entirely.
     */
    #[Depends('testOrderItemOrderQuantityIsSetByConstructor')]
    #[TestDox('OrderItem.orderQuantity can not be nil (0)')]
    public function testOrderItem0rderQuantityCanNotBeNil(): void
    {
        $product = new Product();
        $this->expectException(\DomainException::class);
        $removedOrderItem = new OrderItem($product, 0);
    }


    #[TestDox('OrderItem::count exists')]
    public function testOrderItemCountExists(): void
    {
        $class = new ReflectionClass(OrderItem::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testOrderItemCountExists')]
    #[TestDox('OrderItem::count exists')]
    public function testOrderItemCountIsPublic(): void
    {
        $method = new ReflectionMethod(OrderItem::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrderItemCountExists')]
    #[TestDox('OrderItem::count has no parameters')]
    public function testOrderItemCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderItem::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderItemCountExists')]
    #[Depends('testOrderItemCountIsPublic')]
    #[Depends('testOrderItemCountHasNoParameters')]
    #[TestDox('OrderItem::count has no parameters')]
    public function testOrderItemCountReturnsInteger(): void
    {
        $product = new Product();
        $product->name = 'Widget';

        $orderItem = new OrderItem($product);
        $this->assertIsInt($orderItem->count());

        $orderItem = new OrderItem($product, 42);
        $this->assertIsInt($orderItem->count());
    }

    #[Depends('testOrderItemCountReturnsInteger')]
    #[TestDox('OrderItem::count returns integer greater than or equal to 1')]
    public function testOrderItemCountReturnsIntegerGreaterThanOrEqualTo1(): void
    {
        $product = new Product();
        $product->name = 'Widget';

        $orderItem = new OrderItem($product);
        $this->assertEquals(1, $orderItem->count());

        $orderItem = new OrderItem($product, 42);
        $this->assertEquals(42, $orderItem->count());

        $orderItem->orderQuantity = 12;
        $this->assertEquals(12, $orderItem->count());
    }

    #[Depends('testOrderItemCountExists')]
    #[Depends('testOrderItemCountIsPublic')]
    #[Depends('testOrderItemCountHasNoParameters')]
    #[Depends('testOrderItemCountReturnsInteger')]
    #[TestDox('OrderItem is countable')]
    public function testOrderItemIsCountable(): void
    {
        $product = new Product();
        $product->name = 'Widget';

        $orderItem = new OrderItem($product, 42);
        $this->assertInstanceOf(\Countable::class, $orderItem);
        $this->assertEquals(42, count($orderItem));
        $this->assertCount(42, $orderItem);
    }


    #[TestDox('OrderItem::jsonSerialize exists')]
    public function testOrderItemJsonSerializeExists(): void
    {
        $class = new ReflectionClass(OrderItem::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[Depends('testOrderItemJsonSerializeExists')]
    #[TestDox('OrderItem::jsonSerialize has no parameters')]
    public function testOrderItemJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrderItem::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrderItemJsonSerializeExists')]
    #[Depends('testOrderItemJsonSerializeHasNoParameters')]
    #[TestDox('OrderItem::jsonSerialize returns non-empty array')]
    public function testOrderItemJsonSerializeReturnsNonEmptyArray(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertNotEmpty($orderItem->jsonSerialize());
        $this->assertIsArray($orderItem->jsonSerialize());
    }

    #[Depends('testOrderItemJsonSerializeExists')]
    #[Depends('testOrderItemJsonSerializeHasNoParameters')]
    #[Depends('testOrderItemJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('OrderItem is JSON serializable')]
    public function testOrderItemIsJsonSerializable(): void
    {
        $product = new Product();
        $orderItem = new OrderItem($product);
        $this->assertInstanceOf(\JsonSerializable::class, $orderItem);
    }


    /**
     * @see https://schema.org/OrderItem#eg-0376
     *      Schema.org OrderItem Example 1
     */
    #[Depends('testOrderItemIsJsonSerializable')]
    #[TestDox('OrderItem example')]
    public function testOrderItemExample(): void
    {
        /*
         * First `OrderItem` in JSON-LD:
         *
         * ```json
         * {
         *   "@type": "OrderItem",
         *   "orderItemNumber": "abc123",
         *   "orderQuantity": 1,
         *   "orderedItem": {
         *     "@type": "Product",
         *     "name": "Widget"
         *   },
         *   "orderItemStatus": "https://schema.org/OrderDelivered",
         *   "orderDelivery": {
         *     "@type": "ParcelDelivery",
         *     "expectedArrivalFrom": "2015-03-10"
         *   }
         * }
         * ```
         */
        $product = new Product();
        $product->name = 'Widget';
        $product->sku = 'abc123';
        $this->assertEquals('Widget', $product->name);
        $this->assertEquals('abc123', $product->sku);

        $orderItem = new OrderItem($product);
        $this->assertEquals('abc123', $orderItem->orderItemNumber);
        $this->assertEquals(1, $orderItem->orderQuantity);
        $this->assertEquals($product, $orderItem->orderedItem);
        $this->assertInstanceOf(\JsonSerializable::class, $orderItem->orderedItem);

        $orderItem->orderItemStatus = OrderStatus::OrderDelivered;
        $this->assertEquals('https://schema.org/OrderDelivered', $orderItem->orderItemStatus->value);

        $delivery = new ParcelDelivery();
        $delivery->expectedArrivalFrom = new Date('2015-03-10');
        $orderItem->orderDelivery = $delivery;
        $this->assertEquals($delivery, $orderItem->orderDelivery);
        $this->assertInstanceOf(\JsonSerializable::class, $orderItem->orderDelivery);

        $json = json_encode($orderItem, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertNotFalse($json);
        $this->assertStringContainsString('"orderItemNumber": "abc123"', $json);
        $this->assertStringContainsString('"orderedItem": {', $json);
        $this->assertStringContainsString('"orderItemStatus": "https://schema.org/OrderDelivered"', $json);
        $this->assertStringContainsString('"orderDelivery": {', $json);
        $this->assertStringContainsString('"expectedArrivalFrom": "2015-03-10"', $json);

        $json = json_encode($orderItem, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString('{"@context":"https://schema.org","@type":"OrderItem","orderItemNumber":"abc123","orderQuantity":1,"orderedItem":{"@type":"Product","itemCondition":"https://schema.org/NewCondition","name":"Widget","sku":"abc123"},"orderItemStatus":"https://schema.org/OrderDelivered","orderDelivery":{"@type":"ParcelDelivery","expectedArrivalFrom":"2015-03-10"}}', $json);

        /*
         * Second `OrderItem` in JSON-LD:
         *
         * ```json
         * {
         *   "@type": "OrderItem",
         *   "orderItemNumber": "def456",
         *   "orderQuantity": 3,
         *   "orderedItem": {
         *     "@type": "Product",
         *     "name": "Widget accessories"
         *   },
         *   "orderItemStatus": "https://schema.org/OrderInTransit",
         *   "orderDelivery": {
         *     "@type": "ParcelDelivery",
         *     "expectedArrivalFrom": "2015-03-15",
         *     "expectedArrivalUntil": "2015-03-18"
         *   }
         * }
         * ```
         */
        $product = new Product();
        $product->name = 'Widget accessories';
        $product->sku = 'def456';
        $this->assertEquals('Widget accessories', $product->name);
        $this->assertEquals('def456', $product->sku);

        $orderItem = new OrderItem($product, 3);
        $this->assertEquals('def456', $orderItem->orderItemNumber);
        $this->assertEquals(3, $orderItem->orderQuantity);
        $this->assertEquals($product, $orderItem->orderedItem);
        $this->assertInstanceOf(\JsonSerializable::class, $orderItem->orderedItem);

        $orderItem->orderItemStatus = OrderStatus::OrderInTransit;
        $this->assertEquals('https://schema.org/OrderInTransit', $orderItem->orderItemStatus->value);

        $delivery = new ParcelDelivery();
        $delivery->expectedArrivalFrom = Date::createFromFormat('Y-m-d', '2015-03-10');
        $delivery->expectedArrivalUntil = Date::createFromFormat('Y-m-d', '2015-03-18');
        $orderItem->orderDelivery = $delivery;
        $this->assertEquals($delivery, $orderItem->orderDelivery);
        $this->assertInstanceOf(\JsonSerializable::class, $orderItem->orderDelivery);

        $json = json_encode($orderItem, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertNotFalse($json);
        $this->assertJsonStringEqualsJsonString('{"@context":"https://schema.org","@type":"OrderItem","orderItemNumber":"def456","orderQuantity":3,"orderedItem":{"@type":"Product","itemCondition":"https://schema.org/NewCondition","name":"Widget accessories","sku":"def456"},"orderItemStatus":"https://schema.org/OrderInTransit","orderDelivery":{"@type":"ParcelDelivery","expectedArrivalFrom":"2015-03-10","expectedArrivalUntil":"2015-03-18"}}', $json);
    }
}
