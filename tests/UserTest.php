<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use StoreCore\Types\EmailAddress;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Depends, Group};
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\User::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\EmailAddress::class)]
#[UsesClass(\StoreCore\Types\LanguageCode::class)]
#[Group('security')]
final class UserTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('User class is concrete')]
    public function testUserClassIsConcrete(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(User::VERSION);
        $this->assertIsString(User::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(User::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('User::__construct exists')]
    public function testUserConstructorExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('User::__construct is public constructor')]
    public function testUserConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(User::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('User::__construct has no parameters')]
    public function testUserConstructorHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, '__construct');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('User::authenticate exists')]
    public function testUserAuthenticateExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('authenticate'));
    }

    #[TestDox('User::authenticate is public')]
    public function testUserAuthenticateIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'authenticate');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('User::authenticate has one REQUIRED parameter')]
    public function testUserAuthenticateHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(User::class, 'authenticate');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('User::authenticate returns (bool) false by default')]
    public function testUserAuthenticateReturnsBoolFalseByDefault(): void
    {
        $user = new User();
        $password = 'TopSecret';
        $this->assertIsBool($user->authenticate($password));
        $this->assertFalse($user->authenticate($password));
    }


    #[TestDox('User::getDateTimeZone exists')]
    public function testUserGetDateTimeZoneExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getDateTimeZone'));
    }

    #[TestDox('User::getDateTimeZone is public')]
    public function testUserGetDateTimeZoneIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getDateTimeZone');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('User::getDateTimeZone has no parameters')]
    public function testUserGetDateTimeZoneHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getDateTimeZone');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getDateTimeZone returns DateTimeZone object by default')]
    public function testUserGetDateTimeZoneReturnsDateTimeZoneObjectByDefault(): void
    {
        $user = new User();
        $this->assertIsObject($user->getDateTimeZone());
        $this->assertInstanceOf(\DateTimeZone::class, $user->getDateTimeZone());
    }

    #[Depends('testUserGetDateTimeZoneReturnsDateTimeZoneObjectByDefault')]
    #[TestDox('User::getDateTimeZone returns `UTC` DateTimeZone by default')]
    public function testUserGetDateTimeZoneReturnsUTCDateTimeZoneByDefault(): void
    {
        $user = new User();
        $this->assertEquals('UTC', $user->getDateTimeZone()->getName());
    }


    #[TestDox('User::getEmailAddress exists')]
    public function testUserGetEmailAddressExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getEmailAddress'));
    }

    #[TestDox('User::getEmailAddress is public')]
    public function testUserGetEmailAddressIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getEmailAddress');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('User::getEmailAddress has no parameters')]
    public function testUserGetEmailAddressHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getEmailAddress');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getEmailAddress returns null by default')]
    public function testUserGetEmailAddressReturnsNullByDefault(): void
    {
        $user = new User();
        $this->assertNull($user->getEmailAddress());
    }

    #[TestDox('User::getEmailAddress returns stringable EmailAddress object')]
    public function testUserGetEmailAddressReturnsStringableEmailAddressObject(): void
    {
        $user = new User();
        $user->setEmailAddress('Jane.Roe@example.net');
        $this->assertNotNull($user->getEmailAddress());
        $this->assertIsObject($user->getEmailAddress());
        $this->assertInstanceOf(EmailAddress::class, $user->getEmailAddress());
        $this->assertInstanceOf(\Stringable::class, $user->getEmailAddress());
    }


    #[TestDox('User::getHashAlgorithm exists')]
    public function testUserGetHashAlgorithmExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getHashAlgorithm'));
    }

    #[TestDox('User::getHashAlgorithm is public')]
    public function testUserGetHashAlgorithmIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getHashAlgorithm');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('User::getHashAlgorithm has no parameters')]
    public function testUserGetHashAlgorithmHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getHashAlgorithm');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getHashAlgorithm returns null by default')]
    public function testUserGetHashAlgorithmReturnsNullByDefault(): void
    {
        $user = new User();
        $this->assertNull($user->getHashAlgorithm());
    }


    #[TestDox('User::getLanguageID exists')]
    public function testUserGetLanguageIdExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getLanguageID'));
    }

    #[TestDox('User::getLanguageID is public')]
    public function testUserGetLanguageIdIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getLanguageID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[TestDox('User::getLanguageID has no parameters')]
    public function testUserGetLanguageIdHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getLanguageID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getLanguageID returns non-empty string')]
    public function testUserGetLanguageIdReturnsNonEmptyString(): void
    {
        $user = new User();
        $this->assertNotEmpty($user->getLanguageID());
        $this->assertIsString($user->getLanguageID());
    }

    #[TestDox('User::getLanguageID returns system default language `en-GB` by default')]
    public function testUserGetLanguageIdReturnssystemDefaultLanguageBritishEnglishByDefault(): void
    {
        $user = new User();
        $this->assertEquals(\StoreCore\I18N\Languages::DEFAULT_LANGUAGE, $user->getLanguageID());
        $this->assertEquals('en-GB', $user->getLanguageID());
    }

    #[TestDox('User::setLanguageID exists')]
    public function testUserSetLanguageIdExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setLanguageID'));
    }

    #[TestDox('User::setLanguageID is public')]
    public function testUserSetLanguageIdIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setLanguageID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::setLanguageID has one required')]
    public function testUserSetLanguageIdHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(User::class, 'setLanguageID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('User::setLanguageID accepts language code as string')]
    public function testUserSetLanguageIdAcceptsLanguageCodeAsString(): void
    {
        $user = new User();
        $user->setLanguageID('nl-NL');
        $this->assertSame('nl-NL', $user->getLanguageID());

        $user->setLanguageID('pl-PL');
        $this->assertNotSame('nl-NL', $user->getLanguageID());
        $this->assertSame('pl-PL', $user->getLanguageID());
    }

    #[TestDox('User::setLanguageID throws value error exception on invalid language code string')]
    public function testUserSetLanguageIdThrowsValueErrorExceptionOnInvalidLanguageCodeString(): void
    {
        $user = new User();
        $this->expectException(\ValueError::class);
        $user->setLanguageID('English');
    }

    #[TestDox('User::setLanguageID accepts language code as value object')]
    public function testUserSetLanguageIdAcceptsLanguageCodeAsValueObject(): void
    {
        $user = new User();
        $dutch = new \StoreCore\Types\LanguageCode('nl-NL');
        $user->setLanguageID($dutch);
        $this->assertSame('nl-NL', $user->getLanguageID());

        $danish = new \StoreCore\Types\LanguageCode('da-DK');
        $user->setLanguageID($danish);
        $this->assertNotSame('nl-NL', $user->getLanguageID());
        $this->assertSame('da-DK', $user->getLanguageID());
    }


    #[TestDox('User::getPasswordHash exists')]
    public function testUserGetPasswordHashExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getPasswordHash'));
    }

    #[TestDox('User::getPasswordHash is public')]
    public function testUserGetPasswordHashIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getPasswordHash');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::getPasswordHash has no parameters')]
    public function testUserGetPasswordHashHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getPasswordHash');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('User::getPasswordSalt exists')]
    public function testUserGetPasswordSaltExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getPasswordSalt'));
    }

    #[TestDox('User::getPasswordSalt is public')]
    public function testUserGetPasswordSaltIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getPasswordSalt');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::getPasswordSalt has no parameters')]
    public function testUserGetPasswordSaltHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getPasswordSalt');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }


    #[TestDox('User::getPersonUUID exists')]
    public function testUserGetPersonUUIDExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getPersonUUID'));
    }

    #[TestDox('User::getPersonID no longer exists')]
    public function testUserGetPersonIDNoLongerExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertFalse(
            $class->hasMethod('getPersonID'),
            'User::getPersonID has been superseded by User::getPersonUUID'
        );
    }

    #[TestDox('User::getPersonUUID is public')]
    public function testUserGetPersonUUIDIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getPersonUUID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::getPersonUUID has no parameters')]
    public function testUserGetPersonUUIDHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getPersonUUID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getPersonUUID returns null by default')]
    public function testUserGetPersonUUIDReturnsNullByDefault(): void
    {
        $object = new User();
        $this->assertNull($object->getPersonUUID());
    }


    #[TestDox('User::getPIN exists')]
    public function testUserGetPinExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getPIN'));
    }

    #[TestDox('User::getPIN is public')]
    public function testUserGetPinIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getPIN');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::getPIN has no parameters')]
    public function testUserGetPinHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getPIN');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getPIN returns string `0000` by default')]
    public function testUserGetPinReturnsString0000ByDefault(): void
    {
        $object = new User();
        $this->assertIsString($object->getPIN());
        $this->assertEquals('0000', $object->getPIN());
    }


    #[TestDox('User::getUserGroupID exists')]
    public function testUserGetUserGroupIdExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('getUserGroupID'));
    }

    #[TestDox('User::getUserGroupID is public')]
    public function testUserGetUserGroupIdIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'getUserGroupID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::getUserGroupID has no parameters')]
    public function testUserGetUserGroupIdHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'getUserGroupID');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::getUserGroupID returns int 0 by default')]
    public function testUserGetUserGroupIdReturnsInt0ByDefault(): void
    {
        $object = new User();
        $this->assertIsInt($object->getUserGroupID());
        $this->assertEquals(0, $object->getUserGroupID());
    }


    #[TestDox('User::hasEmailAddress exists')]
    public function testUserHasEmailAddressExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('hasEmailAddress'));
    }

    #[TestDox('User::hasEmailAddress is public')]
    public function testUserHasEmailAddressIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'hasEmailAddress');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::hasEmailAddress has no parameters')]
    public function testUserHasEmailAddressHasNoParameters(): void
    {
        $method = new ReflectionMethod(User::class, 'hasEmailAddress');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('User::hasEmailAddress returns boolean')]
    public function testUserHasEmailAddressReturnsNullByDefault(): void
    {
        $user = new User();
        $this->assertFalse($user->hasEmailAddress());
        $user->setEmailAddress('Jane.Doe@example.org');
        $this->assertTrue($user->hasEmailAddress());
    }


    #[TestDox('User::setDateTimeZone exists')]
    public function testUserSetDateTimeZoneExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setDateTimeZone'));
    }

    #[TestDox('User::setDateTimeZone is public')]
    public function testPublicSetDateTimeZoneMethodIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setDateTimeZone');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::setDateTimeZone has one REQUIRED parameter')]
    public function testStreamFactoryCreateStreamFromResourceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(User::class, 'setDateTimeZone');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('User::setDateTimeZone accepts global `UTC` DateTimeZone')]
    public function testUserSetDateTimeZoneAcceptsGlobalUtcDateTimeZone(): void
    {
        $timezone_string = 'UTC';
        $timezone_object = new \DateTimeZone($timezone_string);
        $user = new User();
        $user->setDateTimeZone($timezone_object);
        $this->assertSame($timezone_object, $user->getDateTimeZone());
        $this->assertSame($timezone_string, $user->getDateTimeZone()->getName());
    }


    #[TestDox('User::setPasswordHash exists')]
    public function testUserSetPasswordHashExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setPasswordHash'));
    }

    #[TestDox('User::setPasswordHash is public')]
    public function testUserSetPasswordHashIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setPasswordHash');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }


    #[TestDox('User::setPasswordSalt exists')]
    public function testUserSetPasswordSaltExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setPasswordSalt'));
    }

    #[TestDox('User::setPasswordSalt is public')]
    public function testUserSetPasswordSaltIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setPasswordSalt');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }


    #[TestDox('User::setPersonUUID exists')]
    public function testUserSetPersonUUIDExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setPersonUUID'));
    }

    #[TestDox('User::setPersonID() no longer exists')]
    public function testUserSetPersonIDNoLongerExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertFalse($class->hasMethod('setPersonID'));
    }

    #[TestDox('User::setPersonUUID is public')]
    public function testUserSetPersonUUIDIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setPersonUUID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }


    #[TestDox('User::setPIN exists')]
    public function testUserSetPinExists(): void
    {
        $class = new \ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setPIN'));
    }

    #[TestDox('User::setPIN is public')]
    public function testUserSetPinIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setPIN');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::setPIN throws \UnexpectedValueException on letters')]
    public function testUserSetPinThrowsUnexpectedValueExceptionOnLetters(): void
    {
        $object = new User();
        $this->expectException(\UnexpectedValueException::class);
        $object->setPIN('ABCD');
    }


    #[TestDox('User::setUserGroupID exists')]
    public function testUserSetUserGroupIdExists(): void
    {
        $class = new ReflectionClass(User::class);
        $this->assertTrue($class->hasMethod('setUserGroupID'));
    }

    #[TestDox('User::setUserGroupID is public')]
    public function testUserSetUserGroupIdIsPublic(): void
    {
        $method = new ReflectionMethod(User::class, 'setUserGroupID');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isAbstract());
    }

    #[TestDox('User::setUserGroupID has one REQUIRED parameter')]
    public function testUserSetUserGroupIdHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(User::class, 'setUserGroupID');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('User::setUserGroupID requires integer')]
    public function testUserSetUserGroupIdRequiresInteger(): void
    {
        $user = new User();
        $this->expectException(\TypeError::class);
        $user->setUserGroupID(1.2);
    }

    #[Depends('testUserSetUserGroupIdRequiresInteger')]
    #[TestDox('User::setUserGroupID throws \DomainException on integer less than 0')]
    public function testUserSetUserGroupIdThrowsDomainExceptionOnIntegerLessThanZero(): void
    {
        $object = new User();
        $this->expectException(\DomainException::class);
        $object->setUserGroupID(-1);
    }

    #[Depends('testUserSetUserGroupIdRequiresInteger')]
    #[TestDox('User::setUserGroupID throws \DomainException on integer greater than 255')]
    public function testUserSetUserGroupIdThrowsDomainExceptionOnIntegerGreaterThan255(): void
    {
        $object = new User();
        $this->expectException(\DomainException::class);
        $object->setUserGroupID(256);
    }
}
