<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\CRM\OrganizationType;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\LocalBusiness::class)]
#[UsesClass(\StoreCore\CRM\OrganizationType::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class LocalBusinessTest extends TestCase
{
    #[TestDox('LocalBusiness class is concrete')]
    public function testLocalBusinessClassIsConcrete(): void
    {
        $class = new ReflectionClass(LocalBusiness::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('LocalBusiness is an Organization')]
    public function testLocalBusinessIsOrganization(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\Organization'));
        $this->assertTrue(class_exists(Organization::class));

        $this->assertInstanceOf(Organization::class, new LocalBusiness());
    }

    #[Group('hmvc')]
    #[TestDox('LocalBusiness implements StoreCore IdentityInterface')]
    public function testLocalBusinessImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new LocalBusiness());
    }

    #[Group('hmvc')]
    #[TestDox('LocalBusineLocalBusiness is JSON serializable')]
    public function testLocalBusinessIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new LocalBusiness());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(LocalBusiness::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(LocalBusiness::VERSION);
        $this->assertIsString(LocalBusiness::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(LocalBusiness::VERSION, '1.0.0-alpha.1', '>=')
        );
    }

    /**
     * @see https://schema.org/currenciesAccepted
     *      Schema.org `currenciesAccepted` property of a `LocalBusiness`
     */
    #[Group('seo')]
    #[TestDox('LocalBusiness.currenciesAccepted exists')]
    public function testLocalBusinessCurrenciesAcceptedExists(): void
    {
        $this->assertObjectHasProperty('currenciesAccepted', new LocalBusiness());
    }

    #[Depends('testLocalBusinessCurrenciesAcceptedExists')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.currenciesAccepted is null by default')]
    public function testLocalBusinessCurrenciesAcceptedIsNullByDefault(): void
    {
        $localBusiness = new LocalBusiness();
        $this->assertNull($localBusiness->priceRange);
    }

    /**
     * @see https://schema.org/currenciesAccepted#eg-0432
     *      Schema.org `currenciesAccepted` example 1
     */
    #[Depends('testLocalBusinessCurrenciesAcceptedIsNullByDefault')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.currenciesAccepted accepts string')]
    public function testLocalBusinessCurrenciesAcceptedAcceptsString(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": ["TouristAttraction", "AmusementPark"],
              "name": "Disneyland Paris",
              "description": "It's an amusement park in Marne-la-Vallée, near Paris, in France and is the most visited theme park in all of France and Europe.",
              "openingHours":["Mo-Fr 10:00-19:00", "Sa 10:00-22:00", "Su 10:00-21:00"],
              "isAccessibleForFree": false,
              "currenciesAccepted": "EUR",
              "paymentAccepted":"Cash, Credit Card",
              "url":"http://www.disneylandparis.it/"
            }
        JSON;

        $localBusiness = new LocalBusiness();
        $localBusiness->name = 'Disneyland Paris';

        $localBusiness->currenciesAccepted = 'EUR';
        $this->assertNotEmpty($localBusiness->currenciesAccepted);
        $this->assertIsString($localBusiness->currenciesAccepted);
        $this->assertEquals('EUR', $localBusiness->currenciesAccepted);

        $actualJson = json_encode($localBusiness, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertStringContainsString('"currenciesAccepted": "EUR"', $actualJson);
    }


    #[Group('seo')]
    #[TestDox('LocalBusiness.hasMap exists')]
    public function testLocalBusinessHasMapExists(): void
    {
        $this->assertObjectHasProperty('hasMap', new LocalBusiness());
    }

    /**
     * @see https://schema.org/hasMap#eg-0362
     *      Schema.org property `hasMap` example 1
     */
    #[Group('hmvc')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.hasMap accepts URL')]
    public function testLocalBusinessHasMapAcceptsURL(): void
    {
        $hotel = new LocalBusiness();
        $hotel->type = OrganizationType::Hotel;
        $hotel->name = 'ACME Hotel Innsbruck';
        $hotel->hasMap = new URL('https://www.google.com/maps?ie=UTF8&hq&ll=47.1234,11.1234&z=13');

        $this->assertNotNull($hotel->hasMap);
        $this->assertIsObject($hotel->hasMap);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Hotel",
              "name": "ACME Hotel Innsbruck",
              "hasMap": "https://www.google.com/maps?ie=UTF8&hq&ll=47.1234,11.1234&z=13"
            }
        ';
        $actualJson = json_encode($hotel, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    /**
     * @see https://schema.org/openingHours
     *      Schema.org `openingHours` property of a `LocalBusiness` or `CivicStructure`
     */
    #[Group('seo')]
    #[TestDox('LocalBusiness.openingHours exists')]
    public function testLocalBusinessOpeningHoursExists(): void
    {
        $this->assertObjectHasProperty('openingHours', new LocalBusiness());
    }

    #[Depends('testLocalBusinessOpeningHoursExists')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.openingHours is public')]
    public function testLocalBusinessOpeningHoursIsPublic(): void
    {
        $property = new ReflectionProperty(LocalBusiness::class, 'openingHours');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testLocalBusinessOpeningHoursExists')]
    #[Depends('testLocalBusinessOpeningHoursIsPublic')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.openingHours is null by default')]
    public function testLocalBusinessOpeningHoursIsNullByDefault(): void
    {
        $localBusiness = new LocalBusiness();
        $this->assertNull($localBusiness->openingHours);
    }

    /**
     * @see https://schema.org/openingHours#eg-0194
     *      Schema.org `openingHours` example 1
     */
    #[Depends('testLocalBusinessOpeningHoursExists')]
    #[Depends('testLocalBusinessOpeningHoursIsPublic')]
    #[Depends('testLocalBusinessOpeningHoursIsNullByDefault')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.openingHours accepts string')]
    public function testLocalBusinessOpeningHoursAcceptsString(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Pharmacy",
              "name": "Philippa’s Pharmacy",
              "description": "A superb collection of fine pharmaceuticals for your beauty and healthcare convenience, a department of Delia’s Drugstore.",
              "openingHours": "Mo,Tu,We,Th 09:00-12:00",
              "telephone": "+18005551234"
            }
        JSON;

        $organization = new LocalBusiness('Philippa’s Pharmacy');
        $organization->type = OrganizationType::Pharmacy;
        $organization->openingHours = 'Mo,Tu,We,Th 09:00-12:00';

        $this->assertNotEmpty($organization->openingHours);
        $this->assertIsString($organization->openingHours);
        $this->assertEquals('Mo,Tu,We,Th 09:00-12:00', $organization->openingHours);

        $actualJson = json_encode($organization, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertStringContainsString('"openingHours": "Mo,Tu,We,Th 09:00-12:00"', $actualJson);
    }

    /**
     * @see https://schema.org/openingHours#eg-0432
     *      Schema.org `openingHours` example 2
     *      Schema.org `openingHours` example 2
     */
    #[Depends('testLocalBusinessOpeningHoursIsNullByDefault')]
    #[Depends('testLocalBusinessOpeningHoursAcceptsString')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.openingHours accepts array')]
    public function testLocalBusinessOpeningHoursAcceptsArray(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": ["TouristAttraction", "AmusementPark"],
              "name": "Disneyland Paris",
              "description": "It’s an amusement park in Marne-la-Vallée, near Paris, in France and is the most visited theme park in all of France and Europe.",
              "openingHours": ["Mo-Fr 10:00-19:00", "Sa 10:00-22:00", "Su 10:00-21:00"],
              "isAccessibleForFree": false,
              "currenciesAccepted": "EUR",
              "paymentAccepted": "Cash, Credit Card",
              "url": "http://www.disneylandparis.it/"
            }
        JSON;

        $organization = new LocalBusiness('Disneyland Paris');
        $organization->type = OrganizationType::AmusementPark;
        $organization->openingHours = array(
            'Mo-Fr 10:00-19:00',
            'Sa 10:00-22:00',
            'Su 10:00-21:00',
        );

        $this->assertNotEmpty($organization->openingHours);
        $this->assertIsNotString($organization->openingHours);
        $this->assertIsArray($organization->openingHours);
        $this->assertCount(3, $organization->openingHours);

        $actualJson = json_encode($organization, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJson($actualJson);
        $this->assertStringContainsString('"openingHours":["Mo-Fr 10:00-19:00","Sa 10:00-22:00","Su 10:00-21:00"]', $actualJson);
    }


    /**
     * @see https://schema.org/paymentAccepted
     *      Schema.org `paymentAccepted` property of a `LocalBusiness` or `CivicStructure`
     */
    #[Group('seo')]
    #[TestDox('LocalBusiness.paymentAccepted exists')]
    public function testLocalBusinessPaymentAcceptedExists(): void
    {
        $this->assertObjectHasProperty('paymentAccepted', new LocalBusiness());
    }

    #[Depends('testLocalBusinessPaymentAcceptedExists')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.paymentAccepted is public')]
    public function testLocalBusinessPaymentAcceptedIsPublic(): void
    {
        $property = new ReflectionProperty(LocalBusiness::class, 'paymentAccepted');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testLocalBusinessPaymentAcceptedExists')]
    #[Depends('testLocalBusinessPaymentAcceptedIsPublic')]
    #[Group('seo')]
    #[TestDox('LocalBusiness.paymentAccepted is null by default')]
    public function testLocalBusinessPaymentAcceptedIsNullByDefault(): void
    {
        $localBusiness = new LocalBusiness();
        $this->assertNull($localBusiness->paymentAccepted);
    }


    /**
     * @see https://schema.org/priceRange
     *      Schema.org `priceRange` property of a `LocalBusiness`
     */
    #[Group('seo')]
    #[TestDox('LocalBusiness.priceRange exists')]
    public function testLocalBusinessPriceRangeExists(): void
    {
        $this->assertObjectHasProperty('priceRange', new LocalBusiness());
    }

    #[Group('seo')]
    #[TestDox('LocalBusiness.priceRange is null by default')]
    public function testLocalBusinessPriceRangeIsNullByDefault(): void
    {
        $localBusiness = new LocalBusiness();
        $this->assertNull($localBusiness->priceRange);
    }

    #[Group('seo')]
    #[TestDox('LocalBusiness.priceRange accepts string')]
    public function testLocalBusinessPriceRangeAcceptsString(): void
    {
        $localBusiness = new LocalBusiness();
        $localBusiness->priceRange = '$$$';

        $this->assertNotNull($localBusiness->priceRange);
        $this->assertNotEmpty($localBusiness->priceRange);
        $this->assertIsString($localBusiness->priceRange);
    }
}
