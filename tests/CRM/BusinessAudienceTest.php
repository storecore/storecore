<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\{Thing, Intangible};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\BusinessAudience::class)]
final class BusinessAudienceTest extends TestCase
{
    #[TestDox('BusinessAudience class is concrete')]
    public function testBusinessAudienceClassIsConcrete(): void
    {
        $class = new ReflectionClass(BusinessAudience::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('BusinessAudience is an Audience')]
    public function testBusinessAudienceIsAudience(): void
    {
        $this->assertInstanceOf(Audience::class, new BusinessAudience());
    }

    #[Group('hmvc')]
    #[TestDox('BusinessAudience is an Intangible Thing')]
    public function testBusinessAudienceIsIntangibleThing(): void
    {
        $audience = new BusinessAudience();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('BusinessAudience is JSON serializable')]
    public function testBusinessAudienceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new BusinessAudience());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(BusinessAudience::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(BusinessAudience::VERSION);
        $this->assertIsString(BusinessAudience::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(BusinessAudience::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('BusinessAudience.numberOfEmployees exists')]
    public function testBusinessAudienceNumberOfEmployeesExists(): void
    {
        $class = new ReflectionClass(BusinessAudience::class);
        $this->assertTrue($class->hasProperty('numberOfEmployees'));
    }

    #[Depends('testBusinessAudienceNumberOfEmployeesExists')]
    #[TestDox('BusinessAudience.numberOfEmployees is public')]
    public function testBusinessAudienceNumberOfEmployeesIsPublic(): void
    {
        $property = new ReflectionProperty(BusinessAudience::class, 'numberOfEmployees');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testBusinessAudienceNumberOfEmployeesExists')]
    #[Depends('testBusinessAudienceNumberOfEmployeesIsPublic')]
    #[TestDox('BusinessAudience.numberOfEmployees is null by default')]
    public function testBusinessAudienceNumberOfEmployeesIsNullByDefault(): void
    {
        $audience = new BusinessAudience();
        $this->assertNull($audience->numberOfEmployees);
    }


    #[TestDox('BusinessAudience.yearlyRevenue exists')]
    public function testBusinessAudienceYearlyRevenueExists(): void
    {
        $class = new ReflectionClass(BusinessAudience::class);
        $this->assertTrue($class->hasProperty('yearlyRevenue'));
    }

    #[Depends('testBusinessAudienceYearlyRevenueExists')]
    #[TestDox('BusinessAudience.yearlyRevenue is public')]
    public function testBusinessAudienceYearlyRevenueIsPublic(): void
    {
        $property = new ReflectionProperty(BusinessAudience::class, 'yearlyRevenue');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testBusinessAudienceYearlyRevenueExists')]
    #[Depends('testBusinessAudienceYearlyRevenueIsPublic')]
    #[TestDox('BusinessAudience.yearlyRevenue is public')]
    public function testBusinessAudienceYearlyRevenueIsNullByDefault(): void
    {
        $audience = new BusinessAudience();
        $this->assertNull($audience->yearlyRevenue);
    }


    #[TestDox('BusinessAudience.yearsInOperation exists')]
    public function testBusinessAudienceYearsInOperationExists(): void
    {
        $class = new ReflectionClass(BusinessAudience::class);
        $this->assertTrue($class->hasProperty('yearsInOperation'));
    }

    #[Depends('testBusinessAudienceYearsInOperationExists')]
    #[TestDox('BusinessAudience.yearsInOperation is public')]
    public function testBusinessAudienceYearsInOperationIsPublic(): void
    {
        $property = new ReflectionProperty(BusinessAudience::class, 'yearsInOperation');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testBusinessAudienceYearsInOperationExists')]
    #[Depends('testBusinessAudienceYearsInOperationIsPublic')]
    #[TestDox('BusinessAudience.yearsInOperation is null by default')]
    public function testBusinessAudienceYearsInOperationIsNullByDefault(): void
    {
        $audience = new BusinessAudience();
        $this->assertNull($audience->yearsInOperation);
    }
}
