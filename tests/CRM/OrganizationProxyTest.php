<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IndentityInterface;
use StoreCore\{ProxyInterface, ProxyFactory, Proxy};
use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;
use StoreCore\Types\UUIDFactory;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \enum_exists;
use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\Proxy::class)]
#[CoversClass(\StoreCore\CRM\Organization::class)]
#[CoversClass(\StoreCore\CRM\OrganizationProxy::class)]
#[CoversClass(\StoreCore\Database\Metadata::class)]
#[CoversClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\ProxyFactory::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrganizationProxyTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('OrganizationProxy class is concrete')]
    public function testOrganizationProxyClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrganizationProxy::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationProxy implements StoreCore IdentityInterface')]
    public function testOrganizationProxyImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertTrue(interface_exists(\StoreCore\IdentityInterface::class));

        $organization = new Organization('NewCo');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $organization->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $proxy);
    }

    #[Depends('testOrganizationProxyImplementsStoreCoreIdentityInterface')]
    #[Group('hmvc')]
    #[TestDox('OrganizationProxy implements StoreCore IdentityInterface')]
    public function testOrganizationProxyImplementsProxyInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\ProxyInterface'));
        $this->assertTrue(interface_exists(\StoreCore\ProxyInterface::class));

        $organization = new Organization('NewCo');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $organization->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $proxy);
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationProxy is JSON serializable')]
    public function testOrganizationProxyIsJsonSerializable(): void
    {
        $organization = new Organization('NewCo');
        $uuid = UUIDFactory::pseudoRandomUUID();
        $organization->setIdentifier($uuid);

        $proxy = ProxyFactory::createFromOrganization($organization);
        $this->assertInstanceOf(\JsonSerializable::class, $proxy);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrganizationProxy::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrganizationProxy::VERSION);
        $this->assertIsString(OrganizationProxy::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrganizationProxy::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Depends('testOrganizationProxyClassIsConcrete')]
    #[TestDox('OrganizationProxy::__construct() exists')]
    public function testOrganizationProxyConstructorExists(): void
    {
        $class = new ReflectionClass(OrganizationProxy::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testOrganizationProxyConstructorExists')]
    #[TestDox('OrganizationProxy::__construct is public constructor')]
    public function testOrganizationProxyConstructorIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationProxy::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testOrganizationProxyConstructorExists')]
    #[TestDox('OrganizationProxy::__construct has three parameters')]
    public function testOrganizationProxyConstructorHasThreeParameters(): void
    {
        $method = new ReflectionMethod(OrganizationProxy::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
    }

    #[Depends('testOrganizationProxyConstructorHasThreeParameters')]
    #[TestDox('OrganizationProxy::__construct has one REQUIRED parameter')]
    public function testOrganizationProxyConstructorHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationProxy::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationProxyConstructorHasThreeParameters')]
    #[Depends('testOrganizationProxyIsJsonSerializable')]
    #[TestDox('OrganizationProxy::__construct sets three read-only properties')]
    public function testOrganizationProxyConstructorSetsThreeReadOnlyProperties()
    {
        $proxy = new OrganizationProxy(
            'c6d9e536-5543-49c4-97c7-6ab8b87b66ba',
            'OnlineStore',
            'Ollivander’s Wand Shop'
        );

        $this->assertEquals('c6d9e536-5543-49c4-97c7-6ab8b87b66ba', $proxy->identifier);
        $this->assertEquals('OnlineStore', $proxy->type);
        $this->assertEquals('Ollivander’s Wand Shop', $proxy->name);

        $expectedJson = trim('
            {
              "@id": "/api/v1/organizations/c6d9e536-5543-49c4-97c7-6ab8b87b66ba",
              "@context": "https://schema.org",
              "@type": "OnlineStore",
              "name": "Ollivander’s Wand Shop",
              "identifier": "c6d9e536-5543-49c4-97c7-6ab8b87b66ba"
            }
        ');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Depends('testOrganizationProxyConstructorHasOneRequiredParameter')]
    #[TestDox('OrganizationProxy.type is not Thing')]
    public function testOrganizationProxyTypeIsNotThing()
    {
        $proxy = new Proxy('268f9140-2197-48b1-b7c5-ef37cafab65c');
        $this->assertEquals('Thing', $proxy->type);

        $proxy = new OrganizationProxy('268f9140-2197-48b1-b7c5-ef37cafab65c');
        $this->assertNotEquals('Thing', $proxy->type);
        $this->assertEquals('Organization', $proxy->type);
    }

    #[Depends('testOrganizationProxyConstructorHasThreeParameters')]
    #[Depends('testOrganizationProxyConstructorSetsThreeReadOnlyProperties')]
    #[Depends('testOrganizationProxyIsJsonSerializable')]
    #[TestDox('OrganizationProxy::__construct accepts OrganizationType')]
    public function testOrganizationProxyConstructorAcceptsOrganizationType()
    {
        $this->assertTrue(enum_exists(OrganizationType::class));

        $proxy = new OrganizationProxy(
            '3cf945ee-9a43-11ed-a8fc-0242ac120002',
            OrganizationType::Brewery,
            'Duff Beer'
        );

        $this->assertNotEquals('Organization', $proxy->type);
        $this->assertEquals('Brewery', $proxy->type);

        $expectedJson = trim('
            {
              "@id": "/api/v1/organizations/3cf945ee-9a43-11ed-a8fc-0242ac120002",
              "@context": "https://schema.org",
              "@type": "Brewery",
              "name": "Duff Beer",
              "identifier": "3cf945ee-9a43-11ed-a8fc-0242ac120002"
            }
        ');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Depends('testOrganizationProxyConstructorAcceptsOrganizationType')]
    #[Depends('testOrganizationProxyIsJsonSerializable')]
    #[TestDox('OrganizationProxy::__construct accepts OrganizationType as string')]
    public function testOrganizationProxyConstructorAcceptsOrganizationTypeAsString()
    {
        $proxy = new OrganizationProxy(
            '3cf945ee-9a43-11ed-a8fc-0242ac120002',
            'Brewery',
            'Duff Beer'
        );

        $this->assertEquals('Brewery', $proxy->type);

        $expectedJson = trim('
            {
              "@id": "/api/v1/organizations/3cf945ee-9a43-11ed-a8fc-0242ac120002",
              "@context": "https://schema.org",
              "@type": "Brewery",
              "name": "Duff Beer",
              "identifier": "3cf945ee-9a43-11ed-a8fc-0242ac120002"
            }
        ');
        $actualJson = json_encode($proxy, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    #[Depends('testOrganizationProxyConstructorAcceptsOrganizationType')]
    #[Depends('testOrganizationProxyConstructorAcceptsOrganizationTypeAsString')]
    #[TestDox('OrganizationProxy.type other than OrganizationType is invalid')]
    public function testOrganizationProxyTypeOtherThanOrganizationTypeIsInvalid()
    {
        $this->expectException(\ValueError::class);
        $failure = new OrganizationProxy(
            'bc9820aa-3a87-4976-98ba-eac150f7af4b',
            'Person',
            'John Doe'
        );
    }
}
