<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CRM;

use StoreCore\Registry;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\CRM\OrganizationRepository::class)]
#[CoversClass(\StoreCore\CRM\Parties::class)]
#[CoversClass(\StoreCore\Database\PersonRepository::class)]
#[UsesClass(\StoreCore\Database\PersonMapper::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
final class PartiesTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Parties class is concrete')]
    public function testPartiesClassIsConcrete(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Parties is a database model')]
    public function testPartiesIsDatabaseModel(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPsr11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPsr11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('Parties implements PSR-11 ContainerInterface')]
    public function testPartiesImplementsPsr11ContainerInterface(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Parties::VERSION);
        $this->assertIsString(Parties::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Parties::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('Parties::get() exists')]
    public function testPartiesGetExists(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testPartiesGetExists')]
    #[TestDox('Parties::get() is public')]
    public function testPartiesGetIsPublic(): void
    {
        $method = new ReflectionMethod(Parties::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testPartiesGetExists')]
    #[TestDox('Parties::get() has one REQUIRED parameter')]
    public function testPartiesGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Parties::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('Parties::has() exists')]
    public function testPartiesHasExists(): void
    {
        $class = new ReflectionClass(Parties::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testPartiesHasExists')]
    #[TestDox('Parties::has() exists')]
    public function testPartiesHasIsPublic(): void
    {
        $method = new ReflectionMethod(Parties::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testPartiesHasExists')]
    #[TestDox('Parties::has() has one REQUIRED parameter')]
    public function testPartiesHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Parties::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testPartiesHasHasOneRequiredParameter')]
    #[TestDox('Parties::has() requires string parameter')]
    public function testPartiesHasRequiresStringParameter(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $parties = new Parties($registry);
        $this->expectException(\TypeError::class);
        $failure = $parties->has(42);
    }

    #[Depends('testPartiesHasExists')]
    #[Depends('testPartiesHasIsPublic')]
    #[Depends('testPartiesHasHasOneRequiredParameter')]
    #[TestDox('Parties::has() returns boolean')]
    public function testPartiesHasReturnsBoolean(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $parties = new Parties($registry);
        $this->assertIsBool($parties->has('bf725640-2307-4473-81f8-3dd742ad46e7'));
        $this->assertFalse($parties->has('26f55bbf-36af-409a-a761-f30f60c908bb'));

        /* If `ContainerInterface::has()` returns `false`,
         * then `ContainerInterface::get()` SHOULD throw
         * a `Psr\Container\NotFoundExceptionInterface`.
         */
        if (!$parties->has('26f55bbf-36af-409a-a761-f30f60c908bb')) {
            $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
            $failure = $parties->get('26f55bbf-36af-409a-a761-f30f60c908bb');
        }
    }
}
