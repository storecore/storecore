<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Geo\AdministrativeArea;
use StoreCore\Types\{Intangible, Thing};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\Audience::class)]
#[CoversClass(\StoreCore\Geo\AdministrativeArea::class)]
final class AudienceTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Audience class is concrete')]
    public function testAudienceClassIsConcrete(): void
    {
        $class = new ReflectionClass(Audience::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Audience is an Intangible Thing')]
    public function testAudienceIsIntangibleThing(): void
    {
        $audience = new Audience();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('Audience is JSON serializable')]
    public function testAudienceisJsonSerializable(): void
    {
        $audience = new Audience();
        $this->assertInstanceOf(\JsonSerializable::class, $audience);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Audience::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Audience::VERSION);
        $this->assertIsString(Audience::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Audience::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('Audience.audienceType exists')]
    public function testAudienceAudienceTypeExists(): void
    {
        $class = new ReflectionClass(Audience::class);
        $this->assertTrue($class->hasProperty('audienceType'));
    }

    #[TestDox('Audience.audienceType is public')]
    public function testAudienceAudienceTypeIsPublic(): void
    {
        $property = new ReflectionProperty(Audience::class, 'audienceType');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Audience.audienceType is null by default')]
    public function testAudienceAudienceIsNullByDefault(): void
    {
        $audience = new Audience();
        $this->assertNull($audience->audienceType);
    }

    #[TestDox('Audience.audienceType accepts string')]
    public function testAudienceAudienceAcceptsAudience(): void
    {
        $audience = new Audience();
        $audience->audienceType = 'Veterans';
        $this->assertNotNull($audience->audienceType);
        $this->assertIsString($audience->audienceType);
        $this->assertSame('Veterans', $audience->audienceType);
    }


    #[TestDox('Audience.name exists')]
    public function testAudienceNameExists(): void
    {
        $class = new ReflectionClass(Audience::class);
        $this->assertTrue($class->hasProperty('name'));
    }

    #[TestDox('Audience.name is public')]
    public function testAudienceNameIsPublic(): void
    {
        $property = new ReflectionProperty(Audience::class, 'name');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Audience.name is null by default')]
    public function testAudienceNameIsNullByDefault(): void
    {
        $audience = new Audience();
        $this->assertNull($audience->name);
    }

    #[TestDox('Audience.name accepts string')]
    public function testAudienceNameAcceptsString(): void
    {
        $audience = new Audience();
        $audience->name = 'Car owners';
        $this->assertNotNull($audience->name);
        $this->assertIsString($audience->name);
        $this->assertEquals('Car owners', $audience->name);
    }

    #[TestDox('Audience::__construct() sets Audience.name')]
    public function testAudienceConstructorSetsAudienceName(): void
    {
        $audience = new Audience('Small businesses');
        $this->assertSame('Small businesses', $audience->name);
    }


    #[TestDox('Audience.geographicArea exists')]
    public function testAudienceGeographicAreaExists(): void
    {
        $class = new ReflectionClass(Audience::class);
        $this->assertTrue($class->hasProperty('geographicArea'));
    }

    #[TestDox('Audience.geographicArea is public')]
    public function testAudienceGeographicAreaIsPublic(): void
    {
        $property = new ReflectionProperty(Audience::class, 'geographicArea');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Audience.geographicArea is null by default')]
    public function testAudienceGeographicAreaIsNullByDefault(): void
    {
        $audience = new Audience();
        $this->assertNull($audience->geographicArea);
    }

    #[Depends('testAudienceisJsonSerializable')]
    #[Group('hmvc')]
    #[TestDox('Audience.geographicArea accepts StoreCore\Geo\AdministrativeArea')]
    public function testAudienceGeographicAreaAcceptsStoreCoreGeoAdministrativeArea(): void
    {
        $audience = new Audience();
        $audience->name = 'Car owners in New York';
        $audience->geographicArea = new AdministrativeArea('New York Metropolitan Area');

        $this->assertNotNull($audience->geographicArea);
        $this->assertInstanceOf(\StoreCore\Geo\AdministrativeArea::class, $audience->geographicArea);

        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Audience",
              "name": "Car owners in New York",
              "geographicArea": {
                "@type": "AdministrativeArea",
                "name": "New York Metropolitan Area"
              }
            }
        ';
        $actualJson = json_encode($audience, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
