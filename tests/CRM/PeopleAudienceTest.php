<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\{Intangible, Thing};

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\PeopleAudience::class)]
final class PeopleAudienceTest extends TestCase
{
    #[TestDox('PeopleAudience class is concrete')]
    public function testPeopleAudienceClassIsConcrete(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PeopleAudience is an Audience')]
    public function testPeopleAudienceIsAudience(): void
    {
        $this->assertInstanceOf(Audience::class, new PeopleAudience());
    }

    #[Group('hmvc')]
    #[TestDox('PeopleAudience is an Intangible Thing')]
    public function testPeopleAudienceIsIntangibleThing(): void
    {
        $audience = new PeopleAudience();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('PeopleAudience is JSON serializable')]
    public function testPeopleAudienceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new PeopleAudience());
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(PeopleAudience::VERSION);
        $this->assertIsString(PeopleAudience::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(PeopleAudience::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('PeopleAudience.requiredMinAge exists')]
    public function testPeopleAudienceRequiredMinAgeExists(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertTrue($class->hasProperty('requiredMinAge'));
    }

    #[Depends('testPeopleAudienceRequiredMinAgeExists')]
    #[TestDox('PeopleAudience.requiredMinAge is public')]
    public function testPeopleAudienceRequiredMinAgeIsPublic(): void
    {
        $property = new ReflectionProperty(PeopleAudience::class, 'requiredMinAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testPeopleAudienceRequiredMinAgeIsPublic')]
    #[TestDox('PeopleAudience.requiredMinAge is null by default')]
    public function testPeopleAudienceRequiredMinAgeIsNullByDefault(): void
    {
        $audience = new PeopleAudience();
        $this->assertNull($audience->requiredMinAge);
    }


    #[TestDox('PeopleAudience.requiredMaxAge exists')]
    public function testPeopleAudienceRequiredMaxAgeExists(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertTrue($class->hasProperty('requiredMaxAge'));
    }

    #[Depends('testPeopleAudienceRequiredMaxAgeExists')]
    #[TestDox('PeopleAudience.requiredMaxAge is public')]
    public function testPeopleAudienceRequiredMaxAgeIsPublic(): void
    {
        $property = new ReflectionProperty(PeopleAudience::class, 'requiredMaxAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testPeopleAudienceRequiredMaxAgeIsPublic')]
    #[TestDox('PeopleAudience.requiredMaxAge is null by default')]
    public function testPeopleAudienceRequiredMaxAgeIsNullByDefault(): void
    {
        $audience = new PeopleAudience();
        $this->assertNull($audience->requiredMaxAge);
    }


    #[TestDox('PeopleAudience.suggestedMinAge exists')]
    public function testPeopleAudienceSuggestedMinAgeExists(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertTrue($class->hasProperty('suggestedMinAge'));
    }

    #[Depends('testPeopleAudienceSuggestedMinAgeExists')]
    #[TestDox('PeopleAudience.suggestedMinAge is public')]
    public function testPeopleAudienceSuggestedMinAgeIsPublic(): void
    {
        $property = new ReflectionProperty(PeopleAudience::class, 'suggestedMinAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testPeopleAudienceSuggestedMinAgeIsPublic')]
    #[TestDox('PeopleAudience.suggestedMinAge is null by default')]
    public function testPeopleAudienceSuggestedMinAgeIsNullByDefault(): void
    {
        $audience = new PeopleAudience();
        $this->assertNull($audience->suggestedMinAge);
    }


    #[TestDox('PeopleAudience.suggestedMaxAge exists')]
    public function testPeopleAudienceSuggestedMaxAgeExists(): void
    {
        $class = new ReflectionClass(PeopleAudience::class);
        $this->assertTrue($class->hasProperty('suggestedMaxAge'));
    }

    #[Depends('testPeopleAudienceSuggestedMaxAgeExists')]
    #[TestDox('PeopleAudience.suggestedMaxAge is public')]
    public function testPeopleAudienceSuggestedMaxAgeIsPublic(): void
    {
        $property = new ReflectionProperty(PeopleAudience::class, 'suggestedMaxAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testPeopleAudienceSuggestedMaxAgeIsPublic')]
    #[TestDox('PeopleAudience.suggestedMaxAge is null by default')]
    public function testPeopleAudienceSuggestedMaxAgeIsNullByDefault(): void
    {
        $audience = new PeopleAudience();
        $this->assertNull($audience->suggestedMaxAge);
    }
}
