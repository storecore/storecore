<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2018, 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Registry;
use StoreCore\I18N\Language;
use StoreCore\Google\People\Name;
use StoreCore\Geo\Country;
use StoreCore\Geo\Place;
use StoreCore\Types\URL;
use StoreCore\Types\{UUID, UUIDFactory};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\CRM\Person::class)]
#[CoversClass(\StoreCore\Google\People\Name::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\I18N\Language::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class PersonTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Person implements StoreCore IdentityInterface')]
    public function testPersonImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\IdentityInterface'));
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Person());
    }

    #[Group('hmvc')]
    #[TestDox('Person is JSON serializable')]
    public function testPersonIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Person());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Person::VERSION);
        $this->assertIsString(Person::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Person::VERSION, '0.4.2', '>=')
        );
    }


    #[TestDox('Person.birthPlace exists')]
    public function testPersonBirthPlaceExists(): void
    {
        $person = new Person();
        $this->assertObjectHasProperty('birthPlace', $person);
    }

    #[TestDox('Person.birthPlace is null by default')]
    public function testPersonBirthPlaceIsNullByDefault(): void
    {
        $person = new Person();
        $this->assertNull($person->identifier);
    }

    #[TestDox('Person.birthPlace can be set to Place')]
    public function testPersonBirthPlaceCanBeSetToPlace(): void
    {
        $person = new Person();
        $person->setName('René Magritte');
        $person->birthPlace = new Place('Lessines');

        $this->assertNotNull($person->birthPlace);
        $this->assertIsObject($person->birthPlace);
        $this->assertSame('Lessines', $person->birthPlace->name);
    }

    #[TestDox('Person::setBirthPlace() exists')]
    public function testPersonSetBirthPlaceExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasMethod('setBirthPlace'));
    }

    #[TestDox('Person::setBirthPlace() is public')]
    public function testPersonSetBirthPlaceIsPublic(): void
    {
        $method = new ReflectionMethod(Person::class, 'setBirthPlace');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Person::setBirthPlace() has one REQUIRED parameter')]
    public function testPersonSetBirthPlaceHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Person::class, 'setBirthPlace');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Person::setBirthPlace() sets Person.birthPlace as Place')]
    public function testPersonSetBirthPlaceSetsPersonBirthPlaceAsPlace(): void
    {
        $person = new Person();
        $person->setName('René Magritte');
        $place = new Place('Lessines');
        $person->setBirthPlace($place);

        $this->assertNotNull($person->birthPlace);
        $this->assertIsObject($person->birthPlace);
        $this->assertInstanceOf(Place::class, $person->birthPlace);
        $this->assertSame($place, $person->birthPlace);
        $this->assertSame('Lessines', $person->birthPlace->name);
    }

    #[TestDox('Person::setBirthPlace() sets Person.birthPlace as string')]
    public function testPersonSetBirthPlaceSetsPersonBirthPlaceAsString(): void
    {
        $person = new Person();
        $person->setName('René Magritte');
        $person->setBirthPlace('Lessines');

        $this->assertNotNull($person->birthPlace);
        $this->assertIsObject($person->birthPlace);
        $this->assertInstanceOf(Place::class, $person->birthPlace);
        $this->assertSame('Lessines', $person->birthPlace->name);
    }


    #[TestDox('Person.dateCreated exists')]
    public function testPersonDateCreatedExists(): void
    {
        $person = new Person();
        $this->assertObjectHasProperty('dateCreated', $person);
    }

    #[TestDox('Person.dateCreated is public')]
    public function testPersonDateCreatedIsPublic(): void
    {
        $property = new ReflectionProperty(Person::class, 'dateCreated');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Person.dateCreated is not null')]
    public function testPersonDateCreatedIsNotNull(): void
    {
        $person = new Person();
        $this->assertNotNull($person->dateCreated);
    }

    #[TestDox('Person.dateCreated implements PHP DateTimeInterface')]
    public function testPersonDateCreatedImplementsPhpDateTimeInterface(): void
    {
        $person = new Person();
        $this->assertInstanceOf(\DateTimeInterface::class, $person->dateCreated);
    }

    #[TestDox('Person.dateCreated is current date and time by default')]
    public function testPersonDateCreatedIsCurrentDateAndTimeByDefault(): void
    {
        $this->assertSame(
            gmdate('Y-m-d H:i:s'),
            (new Person())->dateCreated->format('Y-m-d H:i:s')
        );
    }


    #[TestDox('Person.dateDeleted exists')]
    public function testPersonDateDeletedExists(): void
    {
        $person = new Person();
        $this->assertObjectHasProperty('dateDeleted', $person);
    }

    #[TestDox('Person.dateDeleted is public')]
    public function testPersonDateDeletedIsPublic(): void
    {
        $property = new ReflectionProperty(Person::class, 'dateDeleted');
        $this->assertTrue($property->isPublic());
    }

    #[TestDox('Person.dateDeleted is null by default')]
    public function testPersonDateDeletedIsNullByDefault(): void
    {
        $person = new Person();
        $this->assertNull($person->dateDeleted);
    }

    #[Group('hmvc')]
    #[TestDox('Person.dateDeleted accepts StoreCore\Types\DateTime')]
    public function testPersonDateDeletedAcceptsStoreCoreTypesDateTime(): void
    {
        $person = new Person();
        $now = new \StoreCore\Types\DateTime('now', new \DateTimeZone('UTC'));

        $person->dateDeleted = $now;
        $this->assertNotNull($person->dateDeleted);
        $this->assertInstanceOf(\DateTimeInterface::class, $person->dateDeleted);
        $this->assertSame($now, $person->dateDeleted);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $person->dateDeleted->format('Y-m-d H:i:s'));
    }


    #[TestDox('Person.identifier exists')]
    public function testPersonIdentifierExists(): void
    {
        $person = new Person();
        $this->assertObjectHasProperty('identifier', $person);
    }

    #[TestDox('Person.identifier is null by default')]
    public function testPersonIdentifierIsNullByDefault(): void
    {
        $person = new Person();
        $this->assertNull($person->identifier);
    }

    #[TestDox('Person::getIdentifier() exists')]
    public function testPersonGetIdentifierExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasMethod('getIdentifier'));
    }

    #[TestDox('Person::getPersonID() no longer exists')]
    public function testPersonGetPersonIDNoLongerExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertFalse(
            $class->hasMethod('getPersonID'),
            'Person::getPersonID has been superseded by Person::getIdentifier'
        );
    }

    #[TestDox('Person::getIdentifier() is public')]
    public function testPersonGetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Person::class, 'getIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Person::getIdentifier() has no parameters')]
    public function testPersonGetIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Person::class, 'getIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Person::getIdentifier() returns null by default')]
    public function testPersonGetIdentifierReturnsNullByDefault(): void
    {
        $person = new Person();
        $this->assertNull($person->getIdentifier());
    }

    #[TestDox('Person::hasIdentifier() exists')]
    public function testPersonHasIdentifierExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasMethod('hasIdentifier'));
    }

    #[TestDox('Person::hasIdentifier() is public')]
    public function testPersonHasIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Person::class, 'hasIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Person::hasIdentifier() has no parameters')]
    public function testPersonHasIdentifierHasNoParameters(): void
    {
        $method = new ReflectionMethod(Person::class, 'hasIdentifier');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[TestDox('Person::hasIdentifier() returns false if Person.identifier is not set')]
    public function testPersonHasIdentifierReturnsFalseIfPersonIdentifierIsNotSet(): void
    {
        $person = new Person();
        $this->assertIsBool($person->hasIdentifier());
        if ($person->identifier === null) {
            $this->assertFalse($person->hasIdentifier());
        } else {
            $this->assertTrue($person->hasIdentifier());
        }
    }

    #[TestDox('Person::setIdentifier() exists')]
    public function testPersonSetIdentifierExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasMethod('setIdentifier'));
    }

    #[TestDox('Person::setPersonID() no longer exists')]
    public function testPersonSetPersonIDNoLongerExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertFalse(
            $class->hasMethod('setPersonID'),
            'Person::setPersonID has been superseded by Person::setIdentifier'
        );
    }

    #[TestDox('Person::setIdentifier() is public')]
    public function testPersonSetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Person::class, 'setIdentifier');
        $this->assertTrue($method->isPublic());
    }

    #[TestDox('Person::setIdentifier() has one REQUIRED parameter')]
    public function testPersonSetIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Person::class, 'setIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[TestDox('Person::setIdentifier() sets Person.identifier as UUID value object')]
    public function testPersonSetIdentifierSetsPersonIdentifierAsUUIDValueObject(): void
    {
        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $uuid = $factory->createUUID();

        $person = new Person();
        $this->assertFalse($person->hasIdentifier());
        $person->setIdentifier($uuid);
        $this->assertTrue($person->hasIdentifier());
        $this->assertIsObject($person->getIdentifier());
        $this->assertInstanceOf(UUID::class, $person->getIdentifier());
        $this->assertSame($uuid, $person->getIdentifier());
        $this->assertSame($uuid, $person->identifier);
    }

    #[TestDox('Person::setIdentifier() accepts Person.identifier as UUID string')]
    public function testPersonSetIdentifierAcceptsPersonIdentifierAsUUIDString(): void
    {
        $person = new Person();
        $this->assertFalse($person->hasIdentifier());
        $person->setIdentifier('123e4567-e89b-12d3-a456-426614174000');
        $this->assertTrue($person->hasIdentifier());
        $this->assertIsObject($person->getIdentifier());
        $this->assertInstanceOf(UUID::class, $person->getIdentifier());
        $this->assertSame($person->identifier, $person->getIdentifier());
    }


    #[TestDox('Person.name can be set and read')]
    public function testPersonNameCanBeSetAndRead(): void
    {
        $person = new Person();
        $this->assertNull($person->name);

        $person->name = new Name('Jane Doe');
        $this->assertNotEmpty($person->name);
        $this->assertIsNotString($person->name);
        $this->assertIsObject($person->name);
        $this->assertSame('Jane Doe', $person->name->displayName);

        $this->assertInstanceOf(\Stringable::class, $person->name);
        $this->assertSame('Jane Doe', (string) $person->name);

        $json = json_encode($person, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@type":"Person"', $json);
        $this->assertStringContainsString('"name":"Jane Doe"', $json);
    }

    #[TestDox('Person.name can be set and read')]
    public function testPersonConstructorSetsPersonName(): void
    {
        $person = new Person();
        $this->assertEmpty($person->name);

        $person = new Person('Jane Doe');
        $this->assertNotEmpty($person->name);
        $this->assertIsObject($person->name);
        $this->assertSame('Jane Doe', $person->name->displayName);
    }

    #[TestDox('Person.name is nullable')]
    public function testPersonNameIsNullable(): void
    {
        $person = new Person('Jane Doe');
        $this->assertNotNull($person->name);

        $person->name = null;
        $this->assertNull($person->name);
    }

    #[TestDox('Person.name is nullable with empty string')]
    public function testPersonNameIsNullableWithEmptyString(): void
    {
        $person = new Person('Jane Doe');
        $this->assertNotEmpty($person->name);

        $person->name = '';
        $this->assertEmpty($person->name);
        $this->assertNull($person->name);
    }

    #[TestDox('Person.name consists of Person.givenName and Person.familyName')]
    public function testPersonNameConsistsOfPersonGivenNameAndPersonGamilyName(): void
    {
        $person = new Person();
        $this->assertNull($person->name);

        $person->givenName = 'Jane';
        $person->familyName = 'Doe';
        $this->assertSame('Jane', $person->givenName);
        $this->assertSame('Doe', $person->familyName);

        $this->assertNotNull($person->name);
        $this->assertNotEmpty($person->name);
        $this->assertIsObject($person->name);
        $this->assertSame('Jane Doe', $person->name->displayName);
        $this->assertSame('Jane', $person->name->givenName);
        $this->assertSame('Doe', $person->name->familyName);
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/product#product-review-page-example
     */
    #[TestDox('Person.name accepts string')]
    public function testPersonNameAcceptsString(): void
    {
        $person = new Person();
        $person->name = 'Fred Benson';

        $this->assertNotNull($person->name);
        $this->assertInstanceOf(\Stringable::class, $person->name);
        $this->assertSame('Fred Benson', (string) $person->name);
    }


    #[TestDox('Person.nationality exists')]
    public function testPersonNationalityExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasProperty('nationality'));
    }

    #[TestDox('Person.nationality is public')]
    public function testPersonNationalityIsPublic(): void
    {
        $property = new ReflectionProperty(Person::class, 'nationality');
        $this->assertTrue($property->isPublic());
    }

    #[Group('hmvc')]
    #[TestDox('Person.nationality is null by default')]
    public function testPersonNationalityIsNullByDefault(): void
    {
        $person = new Person('Jane Doe');
        $this->assertNull($person->nationality);
    }

    /**
     * @see https://schema.org/Person#eg-0454
     */
    #[Group('hmvc')]
    #[TestDox('Person.nationality is a Country')]
    public function testPersonNationalityIsACountry(): void
    {
        $creator = new Person('René Magritte');
        $creator->sameAs = new URL('https://www.wikidata.org/wiki/Q7836');
        $creator->nationality = new Country('BE', 'Belgium');

        $this->assertSame('René Magritte', (string) $creator->name);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $creator->sameAs);
        $this->assertSame('https://www.wikidata.org/wiki/Q7836', (string) $creator->sameAs);
        $this->assertInstanceOf(\StoreCore\Geo\Country::class, $creator->nationality);
        $this->assertSame('BE', $creator->nationality->identifier);
        $this->assertSame('BE', (string) $creator->nationality);

        /*
         * ```json
         * {
         *   "@context": "https://schema.org",
         *   "@type": "Person",
         *   "name": "René Magritte",
         *   "nationality": "BE",
         *   "sameAs": "https://www.wikidata.org/wiki/Q7836"
         * }
         * ```
         */
        $json = json_encode($creator, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertStringContainsString('"@type":"Person"', $json);
        $this->assertStringContainsString('"name":"René Magritte"', $json);
        $this->assertStringContainsString('"nationality":"BE"', $json);
        $this->assertStringContainsString('"sameAs":"https://www.wikidata.org/wiki/Q7836"', $json);
    }


    #[TestDox('Person.knowsLanguage exists')]
    public function testPersonKnowsLanguageExists(): void
    {
        $class = new ReflectionClass(Person::class);
        $this->assertTrue($class->hasProperty('knowsLanguage'));
    }

    #[TestDox('Person.knowsLanguage is public')]
    public function testPersonKnowsLanguageIsPublic(): void
    {
        $property = new ReflectionProperty(Person::class, 'knowsLanguage');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[TestDox('Person.knowsLanguage is null by default')]
    public function testPersonKnowsLanguageIsNullByDefault(): void
    {
        $person = new Person('Jane Doe');
        $this->assertNull($person->knowsLanguage);
    }

    #[TestDox('Person.knowsLanguage accepts stringable Language value object')]
    public function testPersonKnowsLanguageAcceptsStringableLanguageValueObject(): void
    {
        $person = new Person('Jane Doe');
        $person->knowsLanguage = new Language('de', 'Deutsch', 'German');

        $this->assertNotNull($person->knowsLanguage);
        $this->assertIsObject($person->knowsLanguage);
        $this->assertInstanceOf(\Stringable::class, $person->knowsLanguage);
        $this->assertInstanceOf(\StoreCore\I18N\Language::class, $person->knowsLanguage);

        $this->assertEquals('de', $person->knowsLanguage->identifier);
        $this->assertEquals('Deutsch', $person->knowsLanguage->name);
        $this->assertEquals('German', $person->knowsLanguage->alternateName);

        /*
         * Although this long form with a `Language` object is correct,
         * we prefer the short form consisting of only BCP 47 language IDs.
         *
         *     ```json
         *     {
         *       "@context": "https://schema.org",
         *       "@type": "Person",
         *       "name": "Jane Doe"
         *       "knowsLanguage": {
         *         "@type": "Language",
         *         "identifier": "de",
         *         "name": "Deutsch",
         *         "alternateName": "German"
         *       },
         *     }
         *     ```
         */
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "Jane Doe",
            "knowsLanguage": "de"
          }
        ';
        $actualJson = json_encode($person, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/article
     */
    #[TestDox('Person with name and URL')]
    public function testPersonWithNameAndUrl(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "John Doe",
            "url": "https://example.com/profile/johndoe123"
          }
        ';

        $author = new Person();
        $author->name = 'John Doe';
        $author->url = 'https://example.com/profile/johndoe123';

        $actualJson = json_encode($author, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }

    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/article#author-bp
     *      Author markup best practices, Article (`Article`, `NewsArticle`, `BlogPosting`) structured data
     */
    #[TestDox('Person with honorificPrefix and jobTitle')]
    public function testPersonWithHonorificPrefixAndJobTitle(): void
    {
        $expectedJson = '
          {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "Echidna Jones",
            "honorificPrefix": "Dr",
            "jobTitle": "Editor in Chief"
          }
        ';

        $author = new Person('Echidna Jones');
        $author->honorificPrefix = 'Dr';
        $author->jobTitle = 'Editor in Chief';

        $actualJson = json_encode($author, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    /**
     * @see https://developers.google.com/maps-booking/verticals/food-ordering/reference/fulfillment-schema#base-types
     *      Fulfillment actions schema, Google Maps Booking API
     */
    #[TestDox('Person supports Google Maps Booking API Contact base type')]
    public function testPersonSupportsGoogleMapsBookingApiContactBaseType(): void
    {
        $contact = new Person();
        $contact->displayName = 'Lovefood Ordering';
        $this->assertSame('Lovefood Ordering', $contact->displayName);

        $contact = new Person('Lovefood Ordering');
        $this->assertSame('Lovefood Ordering', $contact->displayName);

        $contact->email = 'ilovefood@example.com';
        $this->assertIsString($contact->email);
        $this->assertSame('ilovefood@example.com', $contact->email);

        $contact = new Person();
        $contact->firstName = 'Lovefood';
        $contact->lastName = 'Ordering';
        $this->assertSame('Lovefood', $contact->firstName);
        $this->assertSame('Ordering', $contact->lastName);
        $this->assertSame('Lovefood Ordering', $contact->displayName);

        $contact->phoneNumber = '+16501234567';
        $this->assertSame('+16501234567', $contact->phoneNumber);
    }
}
