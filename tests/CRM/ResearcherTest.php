<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\{Thing, Intangible};

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\Researcher::class)]
final class ResearcherTest extends TestCase
{
    #[TestDox('Researcher class is concrete')]
    public function testResearcherClassIsConcrete(): void
    {
        $class = new ReflectionClass(Researcher::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('Researcher is an Audience')]
    public function testResearcherIsAudience(): void
    {
        $this->assertInstanceOf(Audience::class, new Researcher());
    }

    #[Group('hmvc')]
    #[TestDox('Researcher is an Intangible Thing')]
    public function testResearcherIsAnIntangibleThing(): void
    {
        $audience = new Researcher();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('Researcher is JSON serializable')]
    public function testResearcherIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Researcher());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Researcher::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Researcher::VERSION);
        $this->assertIsString(Researcher::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertGreaterThanOrEqual('1.0.0', Researcher::VERSION);
    }
}
