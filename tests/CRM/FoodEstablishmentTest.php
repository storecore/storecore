<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\CRM\OrganizationType;
use StoreCore\OML\PostalAddress;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\CRM\FoodEstablishment::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\AbstractContainer::class)]
#[UsesClass(\StoreCore\AbstractModel::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\CountryRepository::class)]
#[UsesClass(\StoreCore\Geo\PostalAddress::class)]
#[UsesClass(\StoreCore\Geo\State::class)]
#[UsesClass(\StoreCore\Geo\StateRepository::class)]
#[UsesClass(\StoreCore\OML\PostalAddress::class)]
#[UsesClass(\StoreCore\OML\StreetAddress::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class FoodEstablishmentTest extends TestCase
{
    #[TestDox('FoodEstablishment class is concrete')]
    public function testFoodEstablishmentClassIsConcrete(): void
    {
        $class = new ReflectionClass(FoodEstablishment::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('FoodEstablishment is an Organization')]
    public function testFoodEstablishmentIsOrganization(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\Organization'));
        $this->assertTrue(class_exists(Organization::class));

        $restaurant = new FoodEstablishment();
        $this->assertInstanceOf(Organization::class, $restaurant);
    }

    #[Depends('testFoodEstablishmentIsOrganization')]
    #[Group('hmvc')]
    #[TestDox('FoodEstablishment is a LocalBusiness')]
    public function testFoodEstablishmentIsLocalBusiness(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\LocalBusiness'));
        $this->assertTrue(class_exists(LocalBusiness::class));

        $restaurant = new FoodEstablishment();
        $this->assertInstanceOf(LocalBusiness::class, $restaurant);
    }

    #[Group('hmvc')]
    #[TestDox('FoodEstablishment implements StoreCore IdentityInterface')]
    public function testFoodEstablishmentImplementsStoreCoreIdentityInterface(): void
    {
        $restaurant = new FoodEstablishment();
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $restaurant);
    }

    #[Group('hmvc')]
    #[TestDox('FoodEstablishment is JSON serializable')]
    public function testFoodEstablishmentIsJsonSerializable(): void
    {
        $restaurant = new FoodEstablishment();
        $this->assertInstanceOf(\JsonSerializable::class, $restaurant);
    }

    #[Group('hmvc')]
    #[TestDox('FoodEstablishment is stringable')]
    public function testFoodEstablishmentIsStringable(): void
    {
        $restaurant = new FoodEstablishment();
        $this->assertInstanceOf(\Stringable::class, $restaurant);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FoodEstablishment::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FoodEstablishment::VERSION);
        $this->assertIsString(FoodEstablishment::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FoodEstablishment::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    /**
     * @see https://schema.org/priceRange
     *      Schema.org property `priceRange` of a `LocalBusiness`
     */
    #[Group('seo')]
    #[TestDox('FoodEstablishment.priceRange exists')]
    public function testFoodEstablishmentPriceRangeExists(): void
    {
        $localBusiness = new FoodEstablishment();
        $this->assertObjectHasProperty('priceRange', $localBusiness);
    }

    #[Depends('testFoodEstablishmentPriceRangeExists')]
    #[Group('seo')]
    #[TestDox('FoodEstablishment.priceRange is null by default')]
    public function testFoodEstablishmentPriceRangeIsNullByDefault(): void
    {
        $property = new ReflectionProperty(FoodEstablishment::class, 'priceRange');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertNull($property->getDefaultValue());

        $localBusiness = new FoodEstablishment();
        $this->assertNull($localBusiness->priceRange);
    }

    #[Depends('testFoodEstablishmentPriceRangeExists')]
    #[Depends('testFoodEstablishmentPriceRangeIsNullByDefault')]
    #[Group('seo')]
    #[TestDox('FoodEstablishment.priceRange accepts string')]
    public function testFoodEstablishmentPriceRangeAcceptsString(): void
    {
        $localBusiness = new FoodEstablishment();
        $localBusiness->priceRange = '$$$';

        $this->assertNotNull($localBusiness->priceRange);
        $this->assertNotEmpty($localBusiness->priceRange);
        $this->assertIsString($localBusiness->priceRange);
        $this->assertEquals('$$$', $localBusiness->priceRange);
    }


    /**
     * @see https://schema.org/servesCuisine 
     *      Schema.org property `servesCuisine` of a `FoodEstablishment`
     */
    #[Group('seo')]
    #[TestDox('FoodEstablishment.servesCuisine exists')]
    public function testFoodEstablishmentServesCuisineExists(): void
    {
        $foodEstablishment = new FoodEstablishment();
        $this->assertObjectHasProperty('servesCuisine', $foodEstablishment);
    }

    #[Depends('testFoodEstablishmentServesCuisineExists')]
    #[Group('seo')]
    #[TestDox('FoodEstablishment.servesCuisine is null by default')]
    public function testFoodEstablishmentServesCuisineIsNullByDefault(): void
    {
        $property = new ReflectionProperty(FoodEstablishment::class, 'servesCuisine');
        $this->assertTrue($property->hasDefaultValue());
        $this->assertNull($property->getDefaultValue());

        $localBusiness = new FoodEstablishment();
        $this->assertNull($localBusiness->servesCuisine);
    }

    #[Depends('testFoodEstablishmentServesCuisineExists')]
    #[Depends('testFoodEstablishmentServesCuisineIsNullByDefault')]
    #[Group('seo')]
    #[TestDox('FoodEstablishment.servesCuisine accepts string')]
    public function testFoodEstablishmentServesCuisineAcceptsString(): void
    {
        $localBusiness = new FoodEstablishment();
        $localBusiness->servesCuisine = 'Italian';

        $this->assertNotNull($localBusiness->servesCuisine);
        $this->assertNotEmpty($localBusiness->servesCuisine);
        $this->assertIsString($localBusiness->servesCuisine);
        $this->assertEquals('Italian', $localBusiness->servesCuisine);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/local-business#carousel-example
     *      Restaurant carousel (limited access)
     */
    #[Group('seo')]
    #[Test]
    #[TestDox('Google restaurant carousel acceptance test')]
    public function GoogleRestaurantCarouselAcceptanceTest(): void
    {
        /*
            {
              "@context": "https://schema.org/",
              "@type": "Restaurant",
              "name": "Trattoria Luigi",
              "image": [
                "https://example.com/photos/1x1/photo.jpg",
                "https://example.com/photos/4x3/photo.jpg",
                "https://example.com/photos/16x9/photo.jpg"
              ],
              "priceRange": "$$$",
              "servesCuisine": "Italian",
              "address": {
                "@type": "PostalAddress",
                "streetAddress": "148 W 51st St",
                "addressLocality": "New York",
                "addressRegion": "NY",
                "postalCode": "10019",
                "addressCountry": "US"
              }
            }
         */
        $expectedJson = '
            {
              "@context": "https://schema.org",
              "@type": "Restaurant",
              "name": "Trattoria Luigi",
              "image": [
                "https://example.com/photos/1x1/photo.jpg",
                "https://example.com/photos/4x3/photo.jpg",
                "https://example.com/photos/16x9/photo.jpg"
              ],
              "priceRange": "$$$",
              "servesCuisine": "Italian",
              "address": {
                "@type": "PostalAddress",
                "streetAddress": "148 W 51st St",
                "addressLocality": "New York",
                "addressRegion": "NY",
                "postalCode": "10019",
                "addressCountry": "US"
              }
            }
        ';

        $restaurant = new FoodEstablishment();
        $restaurant->type = OrganizationType::Restaurant;
        $restaurant->name = 'Trattoria Luigi';

        $restaurant->image = array(
            'https://example.com/photos/1x1/photo.jpg',
            'https://example.com/photos/4x3/photo.jpg',
            'https://example.com/photos/16x9/photo.jpg'
        );

        $restaurant->priceRange = '$$$';
        $restaurant->servesCuisine = 'Italian';

        $restaurant->address = new PostalAddress();

        $restaurant->address->addressCountry = 'US';
        $this->assertIsNotString($restaurant->address->addressCountry);
        $this->assertIsObject($restaurant->address->addressCountry);
        $this->assertInstanceOf(\JsonSerializable::class, $restaurant->address->addressCountry);

        $restaurant->address->streetAddress = '148 W 51st St';
        $restaurant->address->addressLocality = 'New York';
        $restaurant->address->addressRegion = 'NY';
        $restaurant->address->postalCode = '10019';

        $actualJson = (string) $restaurant;
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
