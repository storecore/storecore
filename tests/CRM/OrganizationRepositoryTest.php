<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CRM;

use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\Types\Date;
use StoreCore\Types\UUID;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \interface_exists;
use function \version_compare;

#[CoversClass(\StoreCore\CRM\OrganizationFactory::class)]
#[CoversClass(\StoreCore\CRM\OrganizationRepository::class)]
#[CoversClass(\StoreCore\CRM\Organization::class)]
#[CoversClass(\StoreCore\Database\OrganizationMapper::class)]
#[CoversClass(\StoreCore\Database\OrganizationProperties::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrganizationRepositoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        try {
            $db = $registry->get('Database');
            $db->query('SELECT 1 FROM `sc_organizations`');

            $db->exec(
                "DELETE
                   FROM `sc_organizations`
                  WHERE `organization_uuid` = UNHEX('00000000000000000000000000000000')
                     OR `common_name` = 'Acme Corporation'"
            );
        } catch (\PDOException $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }


    #[Group('hmvc')]
    #[TestDox('OrganizationRepository class is concrete')]
    public function testOrganizationRepositoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationRepository is a database model')]
    public function testOrganizationRepositoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }

    #[Group('distro')]
    #[Group('hmvc')]
    #[TestDox('Implemented PSR-11 ContainerInterface exists')]
    public function testImplementedPSR11ContainerInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\Psr\\Container\\ContainerInterface'));
        $this->assertTrue(interface_exists(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testImplementedPSR11ContainerInterfaceExists')]
    #[Group('hmvc')]
    #[TestDox('OrganizationRepository implements PSR-11 ContainerInterface')]
    public function testOrganizationRepositoryImplementsPSR11ContainerInterface(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->implementsInterface(\Psr\Container\ContainerInterface::class));
    }

    #[Depends('testOrganizationRepositoryImplementsPSR11ContainerInterface')]
    #[Group('hmvc')]
    #[TestDox('OrganizationRepository implements RepositoryInterface')]
    public function testOrganizationRepositoryImplementsRepositoryInterface(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->implementsInterface(\StoreCore\RepositoryInterface::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrganizationRepository::VERSION);
        $this->assertIsString(OrganizationRepository::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrganizationRepository::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrganizationRepository::clear exists')]
    public function testOrganizationRepositoryClearExists(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testOrganizationRepositoryClearExists')]
    #[TestDox('OrganizationRepository::clear is public')]
    public function testOrganizationRepositoryClearIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'clear');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationRepositoryClearExists')]
    #[TestDox('OrganizationRepository::clear has no parameters')]
    public function testOrganizationRepositoryClearHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'clear');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrganizationRepositoryClearExists')]
    #[TestDox('OrganizationRepository::clear returns (bool) true on success')]
    public function testOrganizationRepositoryClearReturnsBoolTrueOnSuccess(): void
    {
        $registry = Registry::getInstance();
        $repository = new OrganizationRepository($registry);
        $this->assertTrue($repository->clear());
    }


    #[TestDox('OrganizationRepository::count exists')]
    public function testOrganizationRepositoryCountExists(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasMethod('count'));
    }

    #[Depends('testOrganizationRepositoryCountExists')]
    #[TestDox('OrganizationRepository::count is public')]
    public function testOrganizationRepositoryCountIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'count');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationRepositoryCountExists')]
    #[TestDox('OrganizationRepository::count has no parameters')]
    public function testOrganizationRepositoryCountHasNoParameters(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'count');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrganizationRepositoryCountExists')]
    #[Depends('testOrganizationRepositoryCountIsPublic')]
    #[Depends('testOrganizationRepositoryCountHasNoParameters')]
    #[TestDox('OrganizationRepository::count returns zero or positive integer')]
    public function testOrganizationRepositoryCountReturnsZeroOrPositiveInteger(): void
    {
        $registry = Registry::getInstance();
        $repository = new OrganizationRepository($registry);
        $this->assertIsInt($repository->count());
        $this->assertGreaterThanOrEqual(0, $repository->count());
    }

    #[Depends('testOrganizationRepositoryCountExists')]
    #[Depends('testOrganizationRepositoryCountIsPublic')]
    #[Depends('testOrganizationRepositoryCountHasNoParameters')]
    #[Depends('testOrganizationRepositoryCountReturnsZeroOrPositiveInteger')]
    #[TestDox('OrganizationRepository implements Countable interface')]
    public function testOrganizationRepositoryImplementsCountableInterface(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->implementsInterface(\Countable::class));
    }


    #[TestDox('OrganizationRepository::get exists')]
    public function testOrganizationRepositoryGetExists(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasMethod('get'));
    }

    #[Depends('testOrganizationRepositoryGetExists')]
    #[TestDox('OrganizationRepository::get is public')]
    public function testOrganizationRepositoryGetIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'get');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationRepositoryGetExists')]
    #[TestDox('OrganizationRepository::get has one REQUIRED parameter')]
    public function testOrganizationRepositoryGetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'get');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationRepositoryGetExists')]
    #[Depends('testOrganizationRepositoryGetIsPublic')]
    #[Depends('testOrganizationRepositoryGetHasOneRequiredParameter')]
    #[TestDox('OrganizationRepository::get returns existing organization')]
    public function testOrganizationRepositoryGetReturnsExistingOrganization(): void
    {
        $registry = Registry::getInstance();
        $repository = new OrganizationRepository($registry);

        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOrganization('Acme Corporation');

        $this->assertTrue($organization->hasIdentifier());
        $uuid = (string) $organization->getIdentifier();
        $this->assertNotEmpty($uuid);
        $this->assertIsString($uuid);
        $this->assertTrue($repository->has($uuid));

        unset($factory);
        unset($organization);

        $this->assertNotNull($repository->get($uuid));
        $this->assertIsObject($repository->get($uuid));
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $repository->get($uuid));
    }


    #[TestDox('OrganizationRepository::has exists')]
    public function testOrganizationRepositoryHasExists(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasMethod('has'));
    }

    #[Depends('testOrganizationRepositoryHasExists')]
    #[TestDox('OrganizationRepository::has is public')]
    public function testOrganizationRepositoryHasIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'has');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationRepositoryHasExists')]
    #[TestDox('OrganizationRepository::has has one REQUIRED parameter')]
    public function testOrganizationRepositoryHasHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'has');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }


    #[TestDox('OrganizationRepository::set exists')]
    public function testOrganizationRepositorySetExists(): void
    {
        $class = new ReflectionClass(OrganizationRepository::class);
        $this->assertTrue($class->hasMethod('set'));
    }

    #[Depends('testOrganizationRepositorySetExists')]
    #[TestDox('OrganizationRepository::set is public')]
    public function testOrganizationRepositorySetIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'set');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationRepositorySetExists')]
    #[TestDox('OrganizationRepository::set has one REQUIRED parameter')]
    public function testOrganizationRepositorySetHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(OrganizationRepository::class, 'set');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationRepositorySetExists')]
    #[Depends('testOrganizationRepositorySetIsPublic')]
    #[Depends('testOrganizationRepositorySetHasOneRequiredParameter')]
    #[TestDox('OrganizationRepository::set stores updated organization')]
    public function testOrganizationRepositorySetStoresUpdatedOrganization(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        // Use a factory to create aggregates.
        $this->assertTrue(class_exists(OrganizationFactory::class));
        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOrganization('Acme Corporation');

        $this->assertNotNull($organization->metadata);
        $this->assertNotNull($organization->metadata->dateCreated);
        $this->assertIsInt($organization->metadata->version);
        $this->assertEquals(1, $organization->metadata->version);

        // A stored aggregate root MUST have an internal integer ID
        // and a public UUID value object.
        $this->assertNotNull($organization->metadata->id);
        $this->assertIsInt($organization->metadata->id);
        $this->assertTrue($organization->hasIdentifier());
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $organization->getIdentifier());

        // Use a repository to update aggregates.
        $organization->dissolutionDate = new Date('now');
        $repository = new OrganizationRepository($registry);
        $this->assertIsString($repository->set($organization));

        // When the object has changed, `metadata.dateModified` MUST be set …
        $this->assertNotNull($organization->dateModified);
        $this->assertInstanceOf(\DateTimeInterface::class, $organization->dateModified);
        $this->assertEquals((string) $organization->dissolutionDate, $organization->dateModified->format('Y-m-d'));

        // … and the `metadata.version` MUST have been incremented by 1.
        $this->assertIsInt($organization->metadata->version);
        $this->assertEquals(2, $organization->metadata->version);
    }
}
