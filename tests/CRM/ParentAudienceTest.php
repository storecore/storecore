<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\{Thing, Intangible};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\ParentAudience::class)]
#[UsesClass(\StoreCore\CRM\Audience::class)]
#[UsesClass(\StoreCore\CRM\PeopleAudience::class)]
final class ParentAudienceTest extends TestCase
{
    #[TestDox('ParentAudience class is concrete')]
    public function testParentAudienceClassIsConcrete(): void
    {
        $class = new ReflectionClass(ParentAudience::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ParentAudience is a PeopleAudience Audience')]
    public function testParentAudienceIsPeopleAudienceAudience(): void
    {
        $audience = new ParentAudience();
        $this->assertInstanceOf(PeopleAudience::class, $audience);
        $this->assertInstanceOf(Audience::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('ParentAudience is an Intangible Thing')]
    public function testParentAudienceIsIntangibleThing(): void
    {
        $audience = new ParentAudience();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('ParentAudience is JSON serializable')]
    public function testParentAudienceIsJsonSerializable(): void
    {
        $audience = new ParentAudience();
        $this->assertInstanceOf(\JsonSerializable::class, $audience);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ParentAudience::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ParentAudience::VERSION);
        $this->assertIsString(ParentAudience::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ParentAudience::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('ParentAudience.childMinAge exists')]
    public function testParentAudienceChildMinAgeExists(): void
    {
        $class = new ReflectionClass(ParentAudience::class);
        $this->assertTrue($class->hasProperty('childMinAge'));
    }

    #[Depends('testParentAudienceChildMinAgeExists')]
    #[TestDox('ParentAudience.childMinAge is public')]
    public function testParentAudienceChildMinAgeIsPublic(): void
    {
        $property = new ReflectionProperty(ParentAudience::class, 'childMinAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testParentAudienceChildMinAgeExists')]
    #[Depends('testParentAudienceChildMinAgeIsPublic')]
    #[TestDox('ParentAudience.childMinAge is null by default')]
    public function testParentAudienceChildMinAgeIsNullByDefault(): void
    {
        $audience = new ParentAudience();
        $this->assertNull($audience->childMinAge);
    }


    #[TestDox('ParentAudience.childMaxAge exists')]
    public function testParentAudienceChildMaxAgeExists(): void
    {
        $class = new ReflectionClass(ParentAudience::class);
        $this->assertTrue($class->hasProperty('childMaxAge'));
    }

    #[Depends('testParentAudienceChildMaxAgeExists')]
    #[TestDox('ParentAudience.childMaxAge is public')]
    public function testParentAudienceChildMaxAgeIsPublic(): void
    {
        $property = new ReflectionProperty(ParentAudience::class, 'childMaxAge');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testParentAudienceChildMaxAgeExists')]
    #[Depends('testParentAudienceChildMaxAgeIsPublic')]
    #[TestDox('ParentAudience.childMaxAge is null by default')]
    public function testParentAudienceChildMaxAgeIsNullByDefault(): void
    {
        $audience = new ParentAudience();
        $this->assertNull($audience->childMaxAge);
    }
}
