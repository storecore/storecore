<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\CRM\FinancialService::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class FinancialServiceTest extends TestCase
{
    #[TestDox('FinancialService class is concrete')]
    public function testFinancialServiceClassIsConcrete(): void
    {
        $class = new ReflectionClass(FinancialService::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('FinancialService is an Organization')]
    public function testFinancialServiceIsOrganization(): void
    {
        $this->assertTrue(class_exists(Organization::class));
        $this->assertInstanceOf(Organization::class, new FinancialService());
    }

    #[Depends('testFinancialServiceIsOrganization')]
    #[Group('hmvc')]
    #[TestDox('FinancialService is a LocalBusiness')]
    public function testFinancialServiceIsLocalBusiness(): void
    {
        $this->assertTrue(class_exists(LocalBusiness::class));
        $this->assertInstanceOf(LocalBusiness::class, new FinancialService());
    }

    #[Group('hmvc')]
    #[TestDox('FinancialService implements StoreCore IdentityInterface')]
    public function testFinancialServiceImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new FinancialService());
    }

    #[Group('hmvc')]
    #[TestDox('FinancialService is JSON serializable')]
    public function testFinancialServiceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new FinancialService());
    }

    #[Group('hmvc')]
    #[TestDox('FinancialService is an SPL Subject')]
    public function testFinancialServiceIsSplSubject(): void
    {
        $this->assertInstanceOf(\SplSubject::class, new FinancialService());
    }

    #[Group('hmvc')]
    #[TestDox('FinancialService is stringable')]
    public function testFinancialServiceIsStringable(): void
    {
        $this->assertInstanceOf(\Stringable::class, new FinancialService());
    }


    #[Group('hmvc')]
    #[TestDox('FinancialService subtypes')]
    public function testFinancialServiceSubtypes(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\AccountingService'));
        $this->assertInstanceOf(FinancialService::class, new AccountingService());

        $this->assertTrue(class_exists('\\StoreCore\\CRM\\AutomatedTeller'));
        $this->assertInstanceOf(FinancialService::class, new AutomatedTeller());

        $this->assertTrue(class_exists('\\StoreCore\\CRM\\BankOrCreditUnion'));
        $this->assertInstanceOf(FinancialService::class, new BankOrCreditUnion());

        $this->assertTrue(class_exists('\\StoreCore\\CRM\\InsuranceAgency'));
        $this->assertInstanceOf(FinancialService::class, new InsuranceAgency());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(FinancialService::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(FinancialService::VERSION);
        $this->assertIsString(FinancialService::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(FinancialService::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[Group('seo')]
    #[TestDox('FinancialService.feesAndCommissionsSpecification exists')]
    public function testFinancialServiceFeesAndCommissionsSpecificationExists(): void
    {
        $this->assertObjectHasProperty('feesAndCommissionsSpecification', new FinancialService());
    }

    #[Depends('testFinancialServiceFeesAndCommissionsSpecificationExists')]
    #[Group('seo')]
    #[TestDox('FinancialService.feesAndCommissionsSpecification is public')]
    public function testFinancialServiceFeesAndCommissionsSpecificationIsPublic(): void
    {
        $property = new ReflectionProperty(FinancialService::class, 'feesAndCommissionsSpecification');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testFinancialServiceFeesAndCommissionsSpecificationExists')]
    #[Depends('testFinancialServiceFeesAndCommissionsSpecificationIsPublic')]
    #[Group('seo')]
    #[TestDox('FinancialService.feesAndCommissionsSpecification is null by default')]
    public function testFinancialServiceFeesAndCommissionsSpecificationIsNullByDefault(): void
    {
        $localBusiness = new FinancialService();
        $this->assertNull($localBusiness->feesAndCommissionsSpecification);
    }
}
