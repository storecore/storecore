<?php

declare(strict_types=1);

namespace StoreCore\CRM;

use \ReflectionClass, \ReflectionEnum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\CRM\OrganizationType::class)]
final class OrganizationTypeTest extends TestCase
{
    #[TestDox('OrganizationType is an enumeration')]
    public function testOrganizationTypeIsEnumeration(): void
    {
        $class = new ReflectionClass(OrganizationType::class);
        $this->assertTrue($class->isEnum());
    }

    #[Depends('testOrganizationTypeIsEnumeration')]
    #[TestDox('OrganizationType is a backed enumeration')]
    public function testOrganizationTypeIsBackedEnumeration(): void
    {
        $enum = new ReflectionEnum(OrganizationType::class);
        $this->assertTrue($enum->isBacked());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionEnum(OrganizationType::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrganizationType::VERSION);
        $this->assertIsString(OrganizationType::VERSION);
    }

    /**
     * @see https://schema.org/version/latest
     */
    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches latest Schema.org version')]
    public function testVersionMatchesLatestSchemaOrgVersion(): void
    {
        $this->assertTrue(
            version_compare(OrganizationType::VERSION, '23.0', '>=')
        );
    }


    #[TestDox('Common OrganizationType cases for shopping')]
    public function testCommonOrganizationTypeCasesForShopping(): void
    {
        $this->assertNotEmpty(OrganizationType::cases());
        $this->assertIsArray(OrganizationType::cases());

        // Thing > Organization
        $this->assertIsObject(OrganizationType::Organization);
        $this->assertEquals('https://schema.org/Organization', OrganizationType::Organization->value);

        // Thing > Organization > LocalBusiness
        $this->assertIsObject(OrganizationType::LocalBusiness);
        $this->assertEquals('https://schema.org/LocalBusiness', OrganizationType::LocalBusiness->value);

        // Thing > Organization > OnlineBusiness
        $this->assertIsObject(OrganizationType::OnlineBusiness);
        $this->assertEquals('https://schema.org/OnlineBusiness', OrganizationType::OnlineBusiness->value);

        // Thing > Organization > OnlineBusiness > OnlineStore
        $this->assertIsObject(OrganizationType::OnlineStore);
        $this->assertEquals('https://schema.org/OnlineStore', OrganizationType::OnlineStore->value);


        // Thing > Organization > LocalBusiness > FoodEstablishment
        $this->assertIsObject(OrganizationType::FoodEstablishment);
        $this->assertEquals('https://schema.org/FoodEstablishment', OrganizationType::FoodEstablishment->value);

        // Subtypes of Thing > Organization > LocalBusiness > FoodEstablishment
        $this->assertIsObject(OrganizationType::Bakery);
        $this->assertEquals('https://schema.org/Bakery', OrganizationType::Bakery->value);

        $this->assertIsObject(OrganizationType::BarOrPub);
        $this->assertEquals('https://schema.org/BarOrPub', OrganizationType::BarOrPub->value);

        $this->assertIsObject(OrganizationType::Brewery);
        $this->assertEquals('https://schema.org/Brewery', OrganizationType::Brewery->value);

        $this->assertIsObject(OrganizationType::CafeOrCoffeeShop);
        $this->assertEquals('https://schema.org/CafeOrCoffeeShop', OrganizationType::CafeOrCoffeeShop->value);

        $this->assertIsObject(OrganizationType::Distillery);
        $this->assertEquals('https://schema.org/Distillery', OrganizationType::Distillery->value);

        $this->assertIsObject(OrganizationType::FastFoodRestaurant);
        $this->assertEquals('https://schema.org/FastFoodRestaurant', OrganizationType::FastFoodRestaurant->value);

        $this->assertIsObject(OrganizationType::IceCreamShop);
        $this->assertEquals('https://schema.org/IceCreamShop', OrganizationType::IceCreamShop->value);

        $this->assertIsObject(OrganizationType::Restaurant);
        $this->assertEquals('https://schema.org/Restaurant', OrganizationType::Restaurant->value);

        $this->assertIsObject(OrganizationType::Winery);
        $this->assertEquals('https://schema.org/Winery', OrganizationType::Winery->value);


        // Subtypes of Thing > Organization > LocalBusiness > MedicalBusiness

        $this->assertIsObject(OrganizationType::Pharmacy);
        $this->assertEquals('https://schema.org/Pharmacy', OrganizationType::Pharmacy->value);


        // Thing > Organization > LocalBusiness > Store
        $this->assertIsObject(OrganizationType::Store);
        $this->assertEquals('https://schema.org/Store', OrganizationType::Store->value);

        // Subtypes of Thing > Organization > LocalBusiness > Store
        $this->assertIsObject(OrganizationType::AutoPartsStore);
        $this->assertEquals('https://schema.org/AutoPartsStore', OrganizationType::AutoPartsStore->value);

        $this->assertIsObject(OrganizationType::BikeStore);
        $this->assertEquals('https://schema.org/BikeStore', OrganizationType::BikeStore->value);        $this->assertIsObject(OrganizationType::BikeStore);

        $this->assertIsObject(OrganizationType::BookStore);
        $this->assertEquals('https://schema.org/BookStore', OrganizationType::BookStore->value);

        $this->assertIsObject(OrganizationType::ClothingStore);
        $this->assertEquals('https://schema.org/ClothingStore', OrganizationType::ClothingStore->value);

        $this->assertIsObject(OrganizationType::ComputerStore);
        $this->assertEquals('https://schema.org/ComputerStore', OrganizationType::ComputerStore->value);

        $this->assertIsObject(OrganizationType::ConvenienceStore);
        $this->assertEquals('https://schema.org/ConvenienceStore', OrganizationType::ConvenienceStore->value);

        $this->assertIsObject(OrganizationType::DepartmentStore);
        $this->assertEquals('https://schema.org/DepartmentStore', OrganizationType::DepartmentStore->value);

        $this->assertIsObject(OrganizationType::ElectronicsStore);
        $this->assertEquals('https://schema.org/ElectronicsStore', OrganizationType::ElectronicsStore->value);

        $this->assertIsObject(OrganizationType::Florist);
        $this->assertEquals('https://schema.org/Florist', OrganizationType::Florist->value);

        $this->assertIsObject(OrganizationType::FurnitureStore);
        $this->assertEquals('https://schema.org/FurnitureStore', OrganizationType::FurnitureStore->value);

        $this->assertIsObject(OrganizationType::GardenStore);
        $this->assertEquals('https://schema.org/GardenStore', OrganizationType::GardenStore->value);

        $this->assertIsObject(OrganizationType::GroceryStore);
        $this->assertEquals('https://schema.org/GroceryStore', OrganizationType::GroceryStore->value);

        $this->assertIsObject(OrganizationType::HardwareStore);
        $this->assertEquals('https://schema.org/HardwareStore', OrganizationType::HardwareStore->value);

        $this->assertIsObject(OrganizationType::HobbyShop);
        $this->assertEquals('https://schema.org/HobbyShop', OrganizationType::HobbyShop->value);

        $this->assertIsObject(OrganizationType::HomeGoodsStore);
        $this->assertEquals('https://schema.org/HomeGoodsStore', OrganizationType::HomeGoodsStore->value);

        $this->assertIsObject(OrganizationType::JewelryStore);
        $this->assertEquals('https://schema.org/JewelryStore', OrganizationType::JewelryStore->value);

        $this->assertIsObject(OrganizationType::LiquorStore);
        $this->assertEquals('https://schema.org/LiquorStore', OrganizationType::LiquorStore->value);

        $this->assertIsObject(OrganizationType::MensClothingStore);
        $this->assertEquals('https://schema.org/MensClothingStore', OrganizationType::MensClothingStore->value);

        $this->assertIsObject(OrganizationType::MobilePhoneStore);
        $this->assertEquals('https://schema.org/MobilePhoneStore', OrganizationType::MobilePhoneStore->value);

        $this->assertIsObject(OrganizationType::MovieRentalStore);
        $this->assertEquals('https://schema.org/MovieRentalStore', OrganizationType::MovieRentalStore->value);

        $this->assertIsObject(OrganizationType::MusicStore);
        $this->assertEquals('https://schema.org/MusicStore', OrganizationType::MusicStore->value);

        $this->assertIsObject(OrganizationType::OfficeEquipmentStore);
        $this->assertEquals('https://schema.org/OfficeEquipmentStore', OrganizationType::OfficeEquipmentStore->value);

        $this->assertIsObject(OrganizationType::OutletStore);
        $this->assertEquals('https://schema.org/OutletStore', OrganizationType::OutletStore->value);

        $this->assertIsObject(OrganizationType::PawnShop);
        $this->assertEquals('https://schema.org/PawnShop', OrganizationType::PawnShop->value);

        $this->assertIsObject(OrganizationType::PetStore);
        $this->assertEquals('https://schema.org/PetStore', OrganizationType::PetStore->value);

        $this->assertIsObject(OrganizationType::ShoeStore);
        $this->assertEquals('https://schema.org/ShoeStore', OrganizationType::ShoeStore->value);

        $this->assertIsObject(OrganizationType::SportingGoodsStore);
        $this->assertEquals('https://schema.org/SportingGoodsStore', OrganizationType::SportingGoodsStore->value);

        $this->assertIsObject(OrganizationType::TireShop);
        $this->assertEquals('https://schema.org/TireShop', OrganizationType::TireShop->value);

        $this->assertIsObject(OrganizationType::ToyStore);
        $this->assertEquals('https://schema.org/ToyStore', OrganizationType::ToyStore->value);

        $this->assertIsObject(OrganizationType::WholesaleStore);
        $this->assertEquals('https://schema.org/WholesaleStore', OrganizationType::WholesaleStore->value);
    }
}
