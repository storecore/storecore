<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\CMS\ImageObject;
use StoreCore\CMS\TextObject;
use StoreCore\OML\PostalAddress;
use StoreCore\PIM\Brand;
use StoreCore\Types\EmailAddress;
use StoreCore\Types\UUID;
use StoreCore\Types\URL;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CRM\Organization::class)]
#[CoversClass(\StoreCore\OnlineStore::class)]
#[UsesClass(\StoreCore\CMS\TextObject::class)]
#[UsesClass(\StoreCore\CRM\OrganizationProxy::class)]
#[UsesClass(\StoreCore\CRM\OrganizationType::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\PIM\Brand::class)]
#[UsesClass(\StoreCore\Types\Date::class)]
#[UsesClass(\StoreCore\Types\EmailAddress::class)]
#[UsesClass(\StoreCore\Types\Thing::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\Varchar::class)]
final class OrganizationTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('Organization class is concrete')]
    public function testOrganizationClassIsConcrete(): void
    {
        $class = new ReflectionClass(Organization::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[TestDox('Organization class implements IdentityInterface')]
    public function testOrganizationClassImplementsIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new Organization());
    }

    #[Group('seo')]
    #[TestDox('Organization is JSON serializable')]
    public function testOrganizationIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new Organization());
    }

    #[Group('seo')]
    #[TestDox('Organization is stringable')]
    public function testOrganizationIsStringable()
    {
        $this->assertInstanceOf(\Stringable::class, new Organization());
    }


    #[Group('hmvc')]
    #[TestDox('Organization class is an \SplSubject')]
    public function testOrganizationClassIsSplSubject(): void
    {
        $this->assertInstanceOf(\SplSubject::class, new Organization());

        $class = new ReflectionClass(Organization::class);
        $this->assertTrue($class->hasMethod('attach'));
        $this->assertTrue($class->hasMethod('detach'));
        $this->assertTrue($class->hasMethod('notify'));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Organization::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Organization::VERSION);
        $this->assertIsString(Organization::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Organization::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    // Organization.identifier

    #[TestDox('Organization.identifier is stringable UUID')]
    public function testOrganizationIdentifierIsStringableUUID(): void
    {
        $organization = new Organization();
        $uuid = new UUID('123e4567-e89b-12d3-a456-426655440000');
        $organization->identifier = $uuid;

        $this->assertNotNull($organization->identifier);
        $this->assertInstanceOf(\Stringable::class, $organization->identifier);
        $this->assertInstanceOf(\StoreCore\Types\UUID::class, $organization->identifier);
        $this->assertSame($uuid, $organization->identifier);
    }

    #[Depends('testOrganizationClassImplementsIdentityInterface')]
    #[Depends('testOrganizationIdentifierIsStringableUUID')]
    #[TestDox('Organization::getIdentifier() returns Organization.identifier')]
    public function testOrganizationGetIdentifierReturnsOrganizationIdentifier(): void
    {
        $organization = new Organization();
        $this->assertFalse($organization->hasIdentifier());
        $this->assertEmpty($organization->getIdentifier());

        $uuid = new UUID('7158a478-9431-4d10-8e16-d38c0578685e');
        $organization->setIdentifier($uuid);
        $this->assertTrue($organization->hasIdentifier());
        $this->assertEquals($organization->identifier, $organization->getIdentifier());
        $this->assertSame($organization->identifier, $organization->getIdentifier());
    }

    #[Depends('testOrganizationIdentifierIsStringableUUID')]
    #[TestDox('Organization.identifier is included in JSON-LD')]
    public function testOrganizationIdentifierIsIncludedInJsonLd(): void
    {
        $expectedJson = trim('
            {
              "@id": "/api/v1/organizations/d5a6ae46-a177-4503-82d4-8734aaaa9b99",
              "@context": "https://schema.org",
              "@type": "Organization",
              "identifier": "d5a6ae46-a177-4503-82d4-8734aaaa9b99",
              "name": "Example Organization"
            }
        ');

        $organization = new Organization();
        $organization->identifier = 'd5a6ae46-a177-4503-82d4-8734aaaa9b99';
        $organization->name = 'Example Organization';

        $actualJson = json_encode($organization, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    // Organization::__construct

    #[TestDox('Organization::__construct() exists')]
    public function testOrganizationConstructorExists(): void
    {
        $class = new ReflectionClass(Organization::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[TestDox('Organization::__construct is public constructor')]
    public function testOrganizationConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(Organization::class, '__construct');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertTrue($method->isConstructor());
    }

    #[TestDox('Organization::__construct has one OPTIONAL parameter')]
    public function testOrganizationConstructorHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(Organization::class, '__construct');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationConstructorIsPublicConstructor')]
    #[Depends('testOrganizationConstructorHasOneOptionalParameter')]
    #[TestDox('Organization::__construct accepts properties as an array')]
    public function testOrganizationConstructorAcceptsPropertiesAsAnArray(): void
    {
        $properties = [
            'name' => 'Coca-Cola',
            'alternateName' => 'Coke',
            'legalName' => 'The Coca-Cola Company',
        ];
        $organization = new Organization($properties);
        $this->assertEquals('Coca-Cola', $organization->name);
        $this->assertEquals('Coke', $organization->alternateName);
        $this->assertEquals('The Coca-Cola Company', $organization->legalName);
    }


    // Organization::$brand
 
    #[TestDox('Organization.brand exists')]
    public function testOrganizationBrandExists(): void
    {
        $this->assertObjectHasProperty('brand', new Organization());
    }

    #[Depends('testOrganizationBrandExists')]
    #[TestDox('Organization.brand is null by default')]
    public function testOrganizationBrandIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->brand);
    }

    #[Depends('testOrganizationBrandExists')]
    #[Depends('testOrganizationBrandIsNullByDefault')]
    #[TestDox('Organization.brand accepts Brand value object')]
    public function testOrganizationBrandAcceptsBrandValueObject(): void
    {
        $brand = new Brand();
        $brand->name = 'Nike';

        $organization = new Organization();
        $organization->brand = $brand;
        $this->assertNotNull($organization->brand);
        $this->assertSame($brand, $organization->brand);
        $this->assertEquals('Nike', $organization->brand->name);
    }

    #[Depends('testOrganizationBrandIsNullByDefault')]
    #[TestDox('Organization.brand accepts multiple brands as array')]
    public function testOrganizationBrandAcceptsMultipleBrandsAsArray(): void
    {
        $brands = [
            new Brand('Nike'),
            new Brand('Jordan'),
            new Brand('Converse'),
        ];

        $organization = new Organization('Nike, Inc.');
        $organization->brand = $brands;
        $this->assertNotNull($organization->brand);
        $this->assertIsArray($organization->brand);
        $this->assertCount(3, $organization->brand);
    }


    // Organization::$name
 
    #[TestDox('Organization.name exists')]
    public function testOrganizationNameExists(): void
    {
        $this->assertObjectHasProperty('name', new Organization());
    }

    #[Depends('testOrganizationNameExists')]
    #[TestDox('Organization.name exists')]
    public function testOrganizationNameIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'name');
        $this->assertTrue($property->isPublic());
    }

     #[Depends('testOrganizationNameExists')]
     #[TestDox('Organization.name is null by default')]
    public function testOrganizationNameIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->name);
    }

    #[Depends('testOrganizationNameExists')]
    #[TestDox('Organization.name may be set to string')]
    public function testOrganizationNameMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->name = 'Beachwalk Beachwear & Giftware';
        $this->assertNotNull($organization->name);
        $this->assertNotEmpty($organization->name);
        $this->assertIsString($organization->name);
        $this->assertEquals('Beachwalk Beachwear & Giftware', $organization->name);

        $organization = new Organization(['name' => 'Beachwalk Beachwear & Giftware']);
        $this->assertNotNull($organization->name);
        $this->assertNotEmpty($organization->name);
        $this->assertIsString($organization->name);
        $this->assertEquals('Beachwalk Beachwear & Giftware', $organization->name);
    }


    // Organization::$alternateName

    #[TestDox('Organization.alternateName exists')]
    public function testOrganizationAlternateNameExists(): void
    {
        $this->assertObjectHasProperty('alternateName', new Organization());
    }

    #[Depends('testOrganizationAlternateNameExists')]
    #[TestDox('Organization.alternateName is public')]
    public function testOrganizationAlternateNameIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'alternateName');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationAlternateNameExists')]
    #[TestDox('Organization.alternateName is null by default')]
    public function testOrganizationAlternateNameIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->alternateName);
    }

    #[Depends('testOrganizationAlternateNameExists')]
    #[TestDox('Organization.alternateName may be set to string')]
    public function testOrganizationAlternateNameMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->alternateName = 'StoreCore.io';
        $this->assertNotNull($organization->alternateName);
        $this->assertNotEmpty($organization->alternateName);
        $this->assertIsString($organization->alternateName);
        $this->assertEquals('StoreCore.io', $organization->alternateName);

        $organization = new Organization(['name' => 'StoreCore']);
        $organization->alternateName = 'StoreCore.io';
        $this->assertEquals('StoreCore', $organization->name);
        $this->assertEquals('StoreCore.io', $organization->alternateName);

        $organization = new Organization(
            [
                'name' => 'StoreCore',
                'alternateName' => 'StoreCore.io'
            ]
        );
        $this->assertEquals('StoreCore', $organization->name);
        $this->assertEquals('StoreCore.io', $organization->alternateName);
    }


    // Organization::$address

    #[TestDox('Organization.address exists')]
    public function testOrganizationAddressExists(): void
    {
        $this->assertObjectHasProperty('address', new Organization());
    }

    #[Depends('testOrganizationAddressExists')]
    #[TestDox('Organization.address is public')]
    public function testOrganizationAddressIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'address');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationAddressExists')]
    #[TestDox('Organization.address is null by default')]
    public function testOrganizationAddressIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->address);
    }


    // Organization::$description

    #[TestDox('Organization.description exists')]
    public function testOrganizationDescriptionExists(): void
    {
        $this->assertObjectHasProperty('description', new Organization());
    }

    #[Depends('testOrganizationDescriptionExists')]
    #[TestDox('Organization.description is public')]
    public function testOrganizationDescriptionIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'description');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationDescriptionExists')]
    #[TestDox('Organization.description is null by default')]
    public function testOrganizationDescriptionIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertEmpty($organization->description);
        $this->assertNull($organization->description);
    }

    /**
     * @ee https://schema.org/OnlineStore#eg-0473
     *     Schema.org OnlineStore example 1
     */
    #[Depends('testOrganizationDescriptionIsNullByDefault')]
    #[TestDox('Organization.description accepts string')]
    public function testOrganizationDescriptionAcceptsString(): void
    {
        $organization = new Organization();
        $organization->name = 'Protocabulators incorporated';
        $organization->description = 'Your premium source for first class protocabulators';

        $this->assertNotEmpty($organization->description);
        $this->assertIsString($organization->description);
        $this->assertEquals('Your premium source for first class protocabulators', $organization->description);
    }

    #[Depends('testOrganizationDescriptionIsNullByDefault')]
    #[TestDox('Organization.description accepts TextObject')]
    public function testOrganizationDescriptionAcceptsTextObject(): void
    {
        $html = new TextObject();
        $html->text = 'Your premium source for first class protocabulators';

        $organization = new Organization();
        $organization->name = 'Protocabulators incorporated';
        $organization->description = $html;

        $this->assertNotEmpty($organization->description);
        $this->assertIsNotString($organization->description);
        $this->assertIsObject($organization->description);
        $this->assertInstanceOf(\JsonSerializable::class, $organization->description);
    }


    // Organization::$email

    #[TestDox('Organization.email exists')]
    public function testOrganizationEmailExists(): void
    {
        $this->assertObjectHasProperty('email', new Organization());
    }

    #[Depends('testOrganizationEmailExists')]
    #[TestDox('Organization.email is not public')]
    public function testOrganizationEmailIsNotPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'email');
        $this->assertFalse($property->isPublic());
        $this->assertTrue($property->isProtected());
    }

    #[Depends('testOrganizationEmailExists')]
    #[TestDox('Organization.email is null by default')]
    public function testOrganizationEmailIsNullByDefault(): void
    {
        $property = new ReflectionProperty(Organization::class, 'email');
        $this->assertTrue($property->hasDefaultValue());

        $organization = new Organization();
        $this->assertNull($organization->email);
    }

    #[Depends('testOrganizationEmailExists')]
    #[Depends('testOrganizationEmailIsNullByDefault')]
    #[TestDox('Organization.email may be set to stringable EmailAddress object')]
    public function testOrganizationEmailMayBeSetToStringableEmailAddressObject(): void
    {
        $organization = new Organization();
        $organization->email = new EmailAddress('info@example.com');

        $this->assertNotNull($organization->email);
        $this->assertIsObject($organization->email);
        $this->assertInstanceOf(\Stringable::class, $organization->email);
        $this->assertInstanceOf(\StoreCore\Types\EmailAddress::class, $organization->email);
        $this->assertEquals('info@example.com', (string) $organization->email);
    }

    #[Depends('testOrganizationEmailMayBeSetToStringableEmailAddressObject')]
    #[TestDox('Organization.email may be set to string')]
    public function testOrganizationEmailMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->email = 'info@example.com';
        $this->assertNotNull($organization->email);
        $this->assertNotEmpty($organization->email);
        $this->assertIsNotString($organization->email);
        $this->assertInstanceOf(\Stringable::class, $organization->email);
        $this->assertEquals('info@example.com', (string) $organization->email);

        $organization = new Organization(['name' => 'Example.com']);
        $organization->email = 'info@example.com';
        $this->assertEquals('Example.com', $organization->name);
        $this->assertEquals('info@example.com', (string) $organization->email);

        $organization = new Organization(
            [
                'name' => 'Example.com',
                'legalName' => 'The Example Company',
                'email' => 'info@example.com'
            ]
        );
        $this->assertEquals('Example.com', $organization->name);
        $this->assertEquals('info@example.com', (string) $organization->email);
    }


    // Organization::$legalName

    #[TestDox('Organization.legalName exists')]
    public function testOrganizationLegalNameExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('legalName', new Organization());
    }

    #[Depends('testOrganizationLegalNameExists')]
    #[TestDox('Organization.legalName is public')]
    public function testOrganizationLegalNameIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'legalName');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationLegalNameExists')]
    #[TestDox('Organization.legalName is null by default')]
    public function testOrganizationLegalNameIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->legalName);
    }

    #[Depends('testOrganizationLegalNameExists')]
    #[TestDox('Organization.legalName may be set to string')]
    public function testOrganizationLegalNameMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->legalName = 'The Coca-Cola Company';
        $this->assertNotNull($organization->legalName);
        $this->assertNotEmpty($organization->legalName);
        $this->assertIsString($organization->legalName);
        $this->assertEquals('The Coca-Cola Company', $organization->legalName);

        $organization = new Organization(['name' => 'Coca-Cola']);
        $organization->legalName = 'The Coca-Cola Company';
        $this->assertNotSame($organization->name, $organization->legalName);
        $this->assertEquals('Coca-Cola', $organization->name);
        $this->assertEquals('The Coca-Cola Company', $organization->legalName);

        $organization = new Organization(
            [
                'name' => 'Coca-Cola',
                'legalName' => 'The Coca-Cola Company'
            ]
        );
        $this->assertNotSame($organization->name, $organization->legalName);
        $this->assertEquals('Coca-Cola', $organization->name);
        $this->assertEquals('The Coca-Cola Company', $organization->legalName);
    }


    // Organization::$parentOrganization

    #[Group('hmvc')]
    #[TestDox('Organization.parentOrganization exists')]
    public function testOrganizationParentOrganizationExists(): void
    {
        $this->assertObjectHasProperty('parentOrganization', new Organization());
    }

    #[Depends('testOrganizationParentOrganizationExists')]
    #[Group('hmvc')]
    #[TestDox('Organization.parentOrganization exists')]
    public function testOrganizationParentOrganizationIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'parentOrganization');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationParentOrganizationExists')]
    #[Group('hmvc')]
    #[TestDox('Organization.parentOrganization is null by default')]
    public function testOrganizationParentOrganizationIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->parentOrganization);
    }

    /**
     * @see https://schema.org/OnlineBusiness#eg-0473 OnlineBusiness example 1
     */
    #[Depends('testOrganizationParentOrganizationExists')]
    #[Depends('testOrganizationParentOrganizationIsPublic')]
    #[Group('hmvc')]
    #[TestDox('Organization.parentOrganization accepts Organization')]
    public function testOrganizationParentOrganizationAcceptsOrganization(): void
    {
        $expectedJson = trim('
            {
              "@context": "https://schema.org",
              "@type": "OnlineStore",
              "additionalType": "https://schema.org/OnlineBusiness",
              "name": "Protocabulators incorporated",
              "parentOrganization": {
                "@context": "https://schema.org",
                "@type": "OnlineBusiness",
                "name": "Awesome Marketplace"
              }
            }
        ');

        $online_business = new \StoreCore\OnlineBusiness();
        $online_business->name = 'Awesome Marketplace';
        $this->assertInstanceOf(Organization::class, $online_business);

        $online_store = new \StoreCore\OnlineStore();
        $online_store->name = 'Protocabulators incorporated';
        $this->assertInstanceOf(Organization::class, $online_store);
        $this->assertNull($online_store->parentOrganization);

        $online_store->parentOrganization = $online_business;
        $this->assertNotNull($online_store->parentOrganization);
        $this->assertIsObject($online_store->parentOrganization);
        $this->assertEquals('Protocabulators incorporated', $online_store->name);
        $this->assertEquals('Awesome Marketplace', $online_store->parentOrganization->name);

        $actualJson = json_encode($online_store, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);

    }

    #[Depends('testOrganizationParentOrganizationAcceptsOrganization')]
    #[Group('hmvc')]
    #[TestDox('Organization.parentOrganization accepts OrganizationProxy')]
    public function testOrganizationParentOrganizationAcceptsOrganizationProxy(): void
    {
        // `BookStore` in a `ShoppingCenter`
        $expectedJson = trim('
            {
              "@context": "https://schema.org",
              "@type": "BookStore",
              "name": "Startup Example Organization",
              "parentOrganization": {
                "@id": "/api/v1/organizations/995b3b9a-caef-4998-b462-e039cad6652d",
                "@context": "https://schema.org",
                "@type": "ShoppingCenter",
                "identifier": "995b3b9a-caef-4998-b462-e039cad6652d",
                "name": "Enterprise Example Organization"
              }
            }
        ');

        $organization = new Organization('Startup Example Organization');
        $organization->type = OrganizationType::BookStore;

        $organization->parentOrganization = new OrganizationProxy(
            '995b3b9a-caef-4998-b462-e039cad6652d',
            OrganizationType::ShoppingCenter,
            'Enterprise Example Organization',
        );
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $organization->parentOrganization);

        $actualJson = json_encode($organization, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }


    // Organization::$telephone

    #[TestDox('Organization.telephone exists')]
    public function testOrganizationTelephoneExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('telephone', $organization);
    }

    #[Depends('testOrganizationTelephoneExists')]
    #[TestDox('Organization.telephone is public')]
    public function testOrganizationTelephoneIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'telephone');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationTelephoneExists')]
    #[TestDox('Organization.telephone is null by default')]
    public function testOrganizationTelephoneIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->telephone);
    }

    #[Depends('testOrganizationTelephoneExists')]
    #[TestDox('Organization.telephone may be set to string')]
    public function testOrganizationTelephoneMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->telephone = '(408) 714-1489';
        $this->assertNotNull($organization->telephone);
        $this->assertNotEmpty($organization->telephone);
        $this->assertIsString($organization->telephone);
        $this->assertEquals('(408) 714-1489', $organization->telephone);

        $organization = new Organization(['name' => 'GreatFood']);
        $organization->telephone = '(408) 714-1489';
        $this->assertEquals('GreatFood', $organization->name);
        $this->assertEquals('(408) 714-1489', $organization->telephone);

        $organization = new Organization(
            [
                'name' => 'GreatFood',
                'telephone' => '(408) 714-1489',
                'url' => 'http://www.greatfood.com'
            ]
        );
        $this->assertEquals('GreatFood', $organization->name);
        $this->assertEquals('(408) 714-1489', $organization->telephone);
    }


    // Organization::$faxNumber

    #[TestDox('Organization.faxNumber exists')]
    public function testOrganizationFaxNumberExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('faxNumber', $organization);
    }

    #[Depends('testOrganizationFaxNumberExists')]
    #[TestDox('Organization.faxNumber is public')]
    public function testOrganizationFaxNumberIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'faxNumber');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationFaxNumberExists')]
    #[TestDox('Organization.faxNumber is null by default')]
    public function testOrganizationFaxNumberIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->faxNumber);
    }

    /**
     * @see https://developers.google.com/gmail/markup/reference/types/Reservation
     */
    #[Depends('testOrganizationFaxNumberExists')]
    #[TestDox('Organization.faxNumber may be set to string')]
    public function testOrganizationFaxNumberMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->faxNumber = '+1 415-202-7798';
        $this->assertNotNull($organization->faxNumber);
        $this->assertNotEmpty($organization->faxNumber);
        $this->assertIsString($organization->faxNumber);
        $this->assertEquals('+1 415-202-7798', $organization->faxNumber);

        $organization = new Organization(['name' => 'Hilton San Francisco Union Square']);
        $organization->telephone = '+1 415-771-1400';
        $organization->faxNumber = '+1 415-202-7798';
        $this->assertEquals('Hilton San Francisco Union Square', $organization->name);
        $this->assertEquals('+1 415-771-1400', $organization->telephone);
        $this->assertEquals('+1 415-202-7798', $organization->faxNumber);

        $organization = new Organization(
            [
                'name' => 'Hilton San Francisco Union Square',
                'telephone' => '+1 415-771-1400',
                'faxNumber' => '+1 415-202-7798',
                'url' => 'https://www.hilton.com/en/hotels/sfofhhh-hilton-san-francisco-union-square/'
            ]
        );
        $this->assertEquals('Hilton San Francisco Union Square', $organization->name);
        $this->assertEquals('+1 415-771-1400', $organization->telephone);
        $this->assertEquals('+1 415-202-7798', $organization->faxNumber);
    }


    // Organization::$url

    #[TestDox('Organization.url exists')]
    public function testOrganizationUrlExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('url', $organization);
    }

    #[Depends('testOrganizationUrlExists')]
    #[TestDox('Organization.url is not public')]
    public function testOrganizationUrlIsNotPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'url');
        $this->assertFalse($property->isPublic());
    }

    #[Depends('testOrganizationUrlExists')]
    #[TestDox('Organization.url is null by default')]
    public function testOrganizationUrlIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->url);
    }

    #[Depends('testOrganizationUrlIsNullByDefault')]
    #[TestDox('Organization.url is null by default')]
    public function testOrganizationUrlMayBeSetToString(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->url);

        $organization->url = 'http://www.greatfood.com';
        $this->assertNotNull($organization->url);
        $this->assertNotEmpty($organization->url);

        $organization = new Organization(
            [
                'name' => 'GreatFood',
                'telephone' => '(408) 714-1489',
                'url' => 'http://www.greatfood.com'
            ]
        );
        $this->assertNotNull($organization->url);
        $this->assertNotEmpty($organization->url);
    }

    #[Depends('testOrganizationUrlMayBeSetToString')]
    #[TestDox('Organization.url is stringable StoreCore\Types\URL')]
    public function testOrganizationUrlIsStringableStoreCoreTypesUrl(): void
    {
        $organization = new Organization();
        $organization->url = 'http://www.greatfood.com';
        $this->assertInstanceOf(\Stringable::class, $organization->url);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $organization->url);
        $this->assertNotEmpty((string) $organization->url);
        $this->assertIsString((string) $organization->url);
        $this->assertEquals('http://www.greatfood.com', (string) $organization->url);

        $organization = new Organization(['name' => 'GreatFood']);
        $organization->url = 'http://www.greatfood.com';
        $this->assertEquals('GreatFood', $organization->name);
        $this->assertEquals('http://www.greatfood.com', (string) $organization->url);

        $organization = new Organization(
            [
                'name' => 'GreatFood',
                'telephone' => '(408) 714-1489',
                'url' => 'http://www.greatfood.com'
            ]
        );
        $this->assertEquals('GreatFood', $organization->name);
        $this->assertEquals('http://www.greatfood.com', (string) $organization->url);
    }


    #[TestDox('Organization.duns exists')]
    public function testOrganizationDunsExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('duns', $organization);
    }

    #[Depends('testOrganizationDunsExists')]
    #[TestDox('Organization.duns is public')]
    public function testOrganizationDunsIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'duns');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationDunsIsPublic')]
    #[TestDox('Organization.duns is null by default')]
    public function testOrganizationDunsIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->duns);
    }


    /**
     * @see https://schema.org/leiCode
     *      Schema.org `leiCode` property of an `Organization`
     */
    #[TestDox('Organization.leiCode exists')]
    public function testOrganizationLeiCodeExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('leiCode', $organization);
    }

    #[Depends('testOrganizationLeiCodeExists')]
    #[TestDox('Organization.leiCode is public')]
    public function testOrganizationLeiCodeIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'leiCode');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationLeiCodeExists')]
    #[TestDox('Organization.leiCode is null by default')]
    public function testOrganizationLeiCodeIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->leiCode);
    }


    /**
     * @see https://schema.org/logo
     *      Schema.org `logo` property of an `Organization`
     */
    #[TestDox('Organization.logo exists')]
    public function testOrganizationLogoExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('logo', $organization);
    }

    #[Depends('testOrganizationLogoExists')]
    #[TestDox('Organization.logo is null by default')]
    public function testOrganizationLogoIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->logo);
    }

    #[Depends('testOrganizationLogoIsNullByDefault')]
    #[TestDox('Organization.logo accepts URL as object')]
    public function testOrganizationLogoAcceptsUrlAsObject(): void
    {
        $this->assertTrue(class_exists(URL::class));

        $organization = new Organization();
        $organization->logo = new URL('https://www.example.com/images/logo.png');

        $this->assertNotNull($organization->logo);
        $this->assertIsObject($organization->logo);
    }

    #[Depends('testOrganizationLogoIsNullByDefault')]
    #[TestDox('Organization.logo accepts URL as string')]
    public function testOrganizationLogoAcceptsUrlAsString(): void
    {
        $organization = new Organization();
        $organization->logo = 'https://www.example.com/images/logo.png';

        $this->assertNotNull($organization->logo);
        $this->assertInstanceOf(\Stringable::class, $organization->logo);
    }

    #[Depends('testOrganizationLogoIsNullByDefault')]
    #[TestDox('Organization.logo accepts ImageObject')]
    public function testOrganizationLogoAcceptsImageObject(): void
    {
        $this->assertTrue(class_exists(ImageObject::class));
        $logo = new ImageObject();
        $logo->url = 'https://www.example.com/images/logo.png';

        $organization = new Organization();
        $organization->logo = $logo;

        $this->assertNotNull($organization->logo);
        $this->assertIsObject($organization->logo);
        $this->assertInstanceOf(\JsonSerializable::class, $organization->logo);
    }


    /**
     * @see https://developers.google.com/search/docs/advanced/structured-data/local-business
     *      “Local business (`LocalBusiness`) structured data”, Google Search Central documentation
     */
    #[TestDox('Organization.image accepts array with multiple image URLs')]
    public function testOrganizationImageAcceptsArrayWithMultipleImageUrls(): void
    {
        $expectedJson = <<<'JSON'
            {
              "@context": "https://schema.org",
              "@type": "Restaurant",
              "name": "Dave's Steak House",
              "image": [
                "https://example.com/photos/1x1/photo.jpg",
                "https://example.com/photos/4x3/photo.jpg",
                "https://example.com/photos/16x9/photo.jpg"
              ]
            }
        JSON;

        $restaurant = new Organization();
        $restaurant->type = OrganizationType::Restaurant;
        $restaurant->name = 'Dave\'s Steak House';

        $photos = array(
            'https://example.com/photos/1x1/photo.jpg',
            'https://example.com/photos/4x3/photo.jpg',
            'https://example.com/photos/16x9/photo.jpg',
        );
        $restaurant->image = $photos;

        $this->assertNotEmpty($restaurant->image);
        $this->assertIsArray($restaurant->image);
        $this->assertSame($photos, $restaurant->image);
    }


    /**
     * @see https://schema.org/ownershipFundingInfo
     *      Schema.org `ownershipFundingInfo` property of an `Organization`
     */
    #[TestDox('Organization.ownershipFundingInfo exists')]
    public function testOrganizationOwnershipFundingInfoExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('ownershipFundingInfo', $organization);
    }

    #[Depends('testOrganizationOwnershipFundingInfoExists')]
    #[TestDox('Organization.ownershipFundingInfo is null by default')]
    public function testOrganizationOwnershipFundingInfoIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->ownershipFundingInfo);
    }

    #[Depends('testOrganizationOwnershipFundingInfoIsNullByDefault')]
    #[TestDox('Organization.ownershipFundingInfo accepts URL as Stringable value object')]
    public function testOrganizationOwnershipFundingInfoAcceptsUrlAsStringableValueObject(): void
    {
        $organization = new Organization();
        $organization->legalName = 'Google Netherlands B.V.';
        $organization->ownershipFundingInfo = new URL('https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=341985890000');

        $this->assertNotNull($organization->ownershipFundingInfo);
        $this->assertIsObject($organization->ownershipFundingInfo);
        $this->assertInstanceOf(\Stringable::class, $organization->ownershipFundingInfo);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $organization->ownershipFundingInfo);
    }

    #[Depends('testOrganizationOwnershipFundingInfoAcceptsUrlAsStringableValueObject')]
    #[TestDox('Organization.ownershipFundingInfo accepts URL as string')]
    public function testOrganizationOwnershipFundingInfoAcceptsUrlAsString(): void
    {
        $organization = new Organization();
        $organization->legalName = 'Google Netherlands B.V.';
        $organization->ownershipFundingInfo = 'https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=341985890000';

        $this->assertIsNotString($organization->ownershipFundingInfo);
        $this->assertIsObject($organization->ownershipFundingInfo);
        $this->assertInstanceOf(\Stringable::class, $organization->ownershipFundingInfo);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $organization->ownershipFundingInfo);
    }


    #[TestDox('Organization.taxID and Organization.vatID exist')]
    public function testOrganizationTaxIDAndOrganizationVatIDExist(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('taxID', $organization);
        $this->assertObjectHasProperty('vatID', $organization);
    }

    #[Depends('testOrganizationTaxIDAndOrganizationVatIDExist')]
    #[TestDox('Organization.taxID is public')]
    public function testOrganizationTaxIDIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'taxID');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationTaxIDIsPublic')]
    #[TestDox('Organization.taxID is null by default')]
    public function testOrganizationTaxIDIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->taxID);
    }

    #[Depends('testOrganizationTaxIDAndOrganizationVatIDExist')]
    #[TestDox('Organization.vatID is public')]
    public function testOrganizationVatIDIsPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'vatID');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testOrganizationVatIDIsPublic')]
    #[TestDox('Organization.vatID is null by default')]
    public function testOrganizationVatIDIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->vatID);
    }

    #[Depends('testOrganizationVatIDIsNullByDefault')]
    #[TestDox('Organization.vatID MAY be set to string')]
    public function testOrganizationVatIDMayBeSetToString(): void
    {
        $organization = new Organization();
        $organization->vatID = 'NL 001683200B54';
        $this->assertNotNull($organization->vatID);
        $this->assertNotEmpty($organization->vatID);
        $this->assertIsString($organization->vatID);
        $this->assertEquals('NL 001683200B54', $organization->vatID);
        $this->assertNull($organization->taxID);

        $organization = new Organization(['name' => 'StoreCore']);
        $organization->vatID = 'NL 001683200B54';
        $this->assertEquals('StoreCore', $organization->name);
        $this->assertEquals('NL 001683200B54', $organization->vatID);
        $this->assertNull($organization->taxID);

        $organization = new Organization(
            [
                'name' => 'StoreCore',
                'telephone' => '+31 (40) 248 23 11',
                'email' => 'info@storecore.org',
                'url' => 'https://storecore.io/',
                'ownershipFundingInfo' => 'https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=23083293',
                'vatID' => 'NL 001683200B54'
            ]
        );
        $this->assertEquals('StoreCore', $organization->name);
        $this->assertEquals('+31 (40) 248 23 11', $organization->telephone);
        $this->assertEquals('info@storecore.org', (string) $organization->email);
        $this->assertEquals('https://storecore.io/', (string) $organization->url);
        $this->assertEquals('https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=23083293', (string) $organization->ownershipFundingInfo);
        $this->assertEquals('NL 001683200B54', $organization->vatID);
        $this->assertNull($organization->taxID);
    }


    // Organization.dateModified

    #[TestDox('Organization.dateModified is null by default')]
    public function testOrganizationDateModifiedIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->dateModified);
    }

    #[Depends('testOrganizationDateModifiedIsNullByDefault')]
    #[TestDox('Organization.dateModified can be set to current date and time')]
    public function testOrganizationDateModifiedCanBeSetToCurrentDateAndTime(): void
    {
        $organization = new Organization();
        $now = new \DateTime('now');
        $organization->dateModified = $now;
        $this->assertNotNull($organization->dateModified);
        $this->assertInstanceOf(\DateTimeInterface::class, $organization->dateModified);
        $this->assertSame($organization->metadata->dateModified, $organization->dateModified);
    }


    // Organization::$dissolutionDate

    #[TestDox('Organization.dissolutionDate exists')]
    public function testOrganizationDissolutionDateExists(): void
    {
        $organization = new Organization();
        $this->assertObjectHasProperty('dissolutionDate', $organization);
    }

    #[Group('testOrganizationDissolutionDateExists')]
    #[TestDox('Organization.dissolutionDate is not public')]
    public function testOrganizationDissolutionDateIsNotPublic(): void
    {
        $property = new ReflectionProperty(Organization::class, 'dissolutionDate');
        $this->assertFalse($property->isPublic());
        $this->assertTrue($property->isProtected());
    }

    #[Group('testOrganizationDissolutionDateExists')]
    #[TestDox('Organization.dissolutionDate is null by default')]
    public function testOrganizationDissolutionDateIsNullByDefault(): void
    {
        $organization = new Organization();
        $this->assertNull($organization->dissolutionDate);
    }

    #[Group('testOrganizationDissolutionDateExists')]
    #[Group('testOrganizationDissolutionDateIsNullByDefault')]
    #[TestDox('Organization.dissolutionDate can be set to ISO date as string')]
    public function testOrganizationDissolutionDateCanBeSetToISODateAsString(): void
    {
        $organization = new Organization();
        $organization->dissolutionDate = '2014-11-07';
        $this->assertEquals('2014-11-07', (string) $organization->dissolutionDate);
    }

    #[Group('testOrganizationDissolutionDateExists')]
    #[Group('testOrganizationDissolutionDateIsNullByDefault')]
    #[TestDox('Organization.dissolutionDate can be set to PHP DateTime object')]
    public function testOrganizationDissolutionDateCanBeSetToPhpDateTimeObject(): void
    {
        $dissolution_date = \DateTime::createFromFormat('Y-m-d', '2014-11-07');
        $this->assertInstanceOf(\DateTime::class, $dissolution_date);

        $organization = new Organization();
        $organization->dissolutionDate = $dissolution_date;
        $this->assertNotNull($organization->dissolutionDate);
        $this->assertInstanceOf(\DateTimeInterface::class, $dissolution_date);
        $this->assertEquals('2014-11-07', (string) $organization->dissolutionDate);
    }


    // Organization::jsonSerialize

    #[TestDox('Organization::jsonSerialize() exists')]
    public function testOrganizationJsonSerializeExists(): void
    {
        $class = new ReflectionClass(Organization::class);
        $this->assertTrue($class->hasMethod('jsonSerialize'));
    }

    #[Depends('testOrganizationJsonSerializeExists')]
    #[TestDox('Organization::jsonSerialize() is public')]
    public function testOrganizationJsonSerializeIsPublic(): void
    {
        $method = new ReflectionMethod(Organization::class, 'jsonSerialize');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationJsonSerializeExists')]
    #[TestDox('Organization::jsonSerialize() has no parameters')]
    public function testOrganizationJsonSerializeHasNoParameters(): void
    {
        $method = new ReflectionMethod(Organization::class, 'jsonSerialize');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testOrganizationJsonSerializeExists')]
    #[Depends('testOrganizationJsonSerializeIsPublic')]
    #[Depends('testOrganizationJsonSerializeHasNoParameters')]
    #[TestDox('Organization::jsonSerialize() returns non-empty array')]
    public function testOrganizationJsonSerializeReturnsNonEmptyArray(): void
    {
        $organization = new Organization();
        $this->assertNotEmpty($organization->jsonSerialize());
        $this->assertIsArray($organization->jsonSerialize());
    }

    #[Depends('testOrganizationJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('Organization::jsonSerialize() returns Schema.org @type Organization')]
    public function testOrganizationJsonSerializeReturnsSchemaOrgTypeOrganization(): void
    {
        $organization = new Organization();

        $this->assertArrayHasKey('@context', $organization->jsonSerialize());
        $this->assertContains('https://schema.org', $organization->jsonSerialize());
        $this->assertSame('https://schema.org', $organization->jsonSerialize()['@context']);

        $this->assertArrayHasKey('@type', $organization->jsonSerialize());
        $this->assertContains('Organization', $organization->jsonSerialize());
        $this->assertSame('Organization', $organization->jsonSerialize()['@type']);

        $json = json_encode($organization, \JSON_UNESCAPED_SLASHES);
        $this->assertStringContainsString('"@context":"https://schema.org"', $json);
        $this->assertStringContainsString('"@type":"Organization"', $json);
    }

    #[Depends('testOrganizationJsonSerializeReturnsNonEmptyArray')]
    #[TestDox('Organization::jsonSerialize() does not return dateCreated')]
    public function testOrganizationGsonSerializeDoesNotReturnDateCreated(): void
    {
        $organization = new Organization();
        $organization->name = 'StoreCore';
        $this->assertArrayHasKey('name', $organization->jsonSerialize());
        $this->assertArrayNotHasKey('dateCreated', $organization->jsonSerialize());;
    }


    // Organization::setIdentifier

    #[TestDox('Organization::setIdentifier() exists')]
    public function testOrganizationSetIdentifierExists(): void
    {
        $class = new ReflectionClass(Organization::class);
        $this->assertTrue($class->hasMethod('setIdentifier'));
    }

    #[Depends('testOrganizationSetIdentifierExists')]
    #[TestDox('Organization::setIdentifier() is public')]
    public function testOrganizationSetIdentifierIsPublic(): void
    {
        $method = new ReflectionMethod(Organization::class, 'setIdentifier');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationSetIdentifierExists')]
    #[TestDox('Organization::setIdentifier() has one REQUIRED parameter')]
    public function testOrganizationSetIdentifierHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(Organization::class, 'setIdentifier');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationIdentifierIsStringableUUID')]
    #[Depends('testOrganizationSetIdentifierExists')]
    #[Depends('testOrganizationSetIdentifierIsPublic')]
    #[Depends('testOrganizationSetIdentifierHasOneRequiredParameter')]
    #[TestDox('Organization::setIdentifier() sets UUID as value object')]
    public function testOrganizationSetIdentifierSetsUUIDAsValueObject(): void
    {
        $organization = new Organization();
        $uuid = new UUID('123e4567-e89b-12d3-a456-426655440000');
        $organization->setIdentifier($uuid);
        $this->assertIsObject($organization->identifier);
        $this->assertSame($uuid, $organization->identifier);
    }

    #[Depends('testOrganizationSetIdentifierSetsUUIDAsValueObject')]
    #[TestDox('Organization::setIdentifier() accepts UUID as string')]
    public function testOrganizationSetIdentifierAcceptsUUIDAsString(): void
    {
        $organization = new Organization();
        $uuid = '123e4567-e89b-12d3-a456-426655440000';
        $this->assertIsString($uuid);

        $organization->setIdentifier($uuid);
        $this->assertIsObject($organization->identifier);
        $this->assertEquals($uuid, (string) $organization->identifier);
    }


    /**
     * @see https://developers.google.com/search/docs/appearance/structured-data/logo
     */
    #[Group('seo')]
    #[TestDox('Organization.logo structured data acceptance test')]
    public function testOrganizationLogoStructuredDataAcceptanceTest(): void
    {
        $expectedJson = trim('
            {
              "@context": "https://schema.org",
              "@type": "Organization",
              "url": "https://www.example.com",
              "logo": "https://www.example.com/images/logo.png"
            }
        ');

        $organization = new Organization();
        $organization->url = new URL('https://www.example.com');
        $organization->logo = new URL('https://www.example.com/images/logo.png');

        $actualJson = json_encode($organization, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}
