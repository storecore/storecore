<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\CMS\ArchiveComponent;

use \ReflectionClass, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;

#[CoversClass(\StoreCore\CRM\ArchiveOrganization::class)]
#[UsesClass(\StoreCore\CMS\ArchiveComponent::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class ArchiveOrganizationTest extends TestCase
{
    #[TestDox('ArchiveOrganization class is concrete')]
    public function testArchiveOrganizationClassIsConcrete(): void
    {
        $class = new ReflectionClass(ArchiveOrganization::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveOrganization is an Organization')]
    public function testArchiveOrganizationIsOrganization(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\Organization'));
        $this->assertTrue(class_exists(Organization::class));

        $this->assertInstanceOf(Organization::class, new ArchiveOrganization());
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveOrganization is a LocalBusiness')]
    public function testArchiveOrganizationIsLocalBusiness(): void
    {
        $this->assertTrue(class_exists('\\StoreCore\\CRM\\LocalBusiness'));
        $this->assertTrue(class_exists(LocalBusiness::class));

        $this->assertInstanceOf(LocalBusiness::class, new ArchiveOrganization());
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveOrganization implements StoreCore IdentityInterface')]
    public function testArchiveOrganizationImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new ArchiveOrganization());
    }

    #[Group('hmvc')]
    #[TestDox('ArchiveOrganization is JSON serializable')]
    public function testArchiveOrganizationIsJsonSerializable(): void
    {
        $archiveOrganization = new ArchiveOrganization();
        $this->assertInstanceOf(\JsonSerializable::class, $archiveOrganization);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ArchiveOrganization::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ArchiveOrganization::VERSION);
        $this->assertIsString(ArchiveOrganization::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ArchiveOrganization::VERSION, '1.0.0-rc.1', '>=')
        );
    }


    /**
     * @see https://schema.org/archiveHeld
     *      Schema.org `archiveHeld` property of an `ArchiveOrganization`
     */
    #[TestDox('ArchiveOrganization.archiveHeld exists')]
    public function testArchiveOrganizationArchiveHeldExists(): void
    {
        $this->assertObjectHasProperty('archiveHeld', new ArchiveOrganization());
    }

    #[Depends('testArchiveOrganizationArchiveHeldExists')]
    #[TestDox('ArchiveOrganization.archiveHeld is public')]
    public function testArchiveOrganizationArchiveHeldIsPublic(): void
    {
        $property = new ReflectionProperty(ArchiveOrganization::class, 'archiveHeld');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testArchiveOrganizationArchiveHeldExists')]
    #[Depends('testArchiveOrganizationArchiveHeldIsPublic')]
    #[TestDox('ArchiveOrganization.archiveHeld is null by default')]
    public function testArchiveOrganizationArchiveHeldIsNullByDefault(): void
    {
        $archiveOrganization = new ArchiveOrganization();
        $this->assertNull($archiveOrganization->archiveHeld);
    }

    #[Depends('testArchiveOrganizationArchiveHeldIsNullByDefault')]
    #[TestDox('ArchiveOrganization.archiveHeld accepts JSON serializable ArchiveComponent CreativeWork')]
    public function testArchiveOrganizationArchiveHeldAcceptsJsonSerializableArchiveComponentCreativeWork(): void
    {
        $this->assertTrue(class_exists(ArchiveComponent::class));
        $archive = new ArchiveComponent();

        $organization = new ArchiveOrganization();
        $organization->archiveHeld = $archive;
        $this->assertNotNull($organization->archiveHeld);
        $this->assertIsObject($organization->archiveHeld);
        $this->assertInstanceOf(\JsonSerializable::class, $organization->archiveHeld);
        $this->assertInstanceOf(\StoreCore\CMS\ArchiveComponent::class, $organization->archiveHeld);
        $this->assertInstanceOf(\StoreCore\CMS\AbstractCreativeWork::class, $organization->archiveHeld);
    }
}
