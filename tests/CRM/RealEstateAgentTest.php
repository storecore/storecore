<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CRM\RealEstateAgent::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class RealEstateAgentTest extends TestCase
{
    #[Group('hmvc')]
    #[TestDox('RealEstateAgent class is concrete')]
    public function testRealEstateAgentClassIsConcrete(): void
    {
        $class = new ReflectionClass(RealEstateAgent::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('RealEstateAgent is an Organization')]
    public function testRealEstateAgentIsOrganization(): void
    {
        $this->assertInstanceOf(Organization::class, new RealEstateAgent());
    }

    #[Depends('testRealEstateAgentIsOrganization')]
    #[Group('hmvc')]
    #[TestDox('RealEstateAgent is a LocalBusiness')]
    public function testRealEstateAgentIsLocalBusiness(): void
    {
        $this->assertInstanceOf(LocalBusiness::class, new RealEstateAgent());
    }

    #[Group('hmvc')]
    #[TestDox('RealEstateAgent implements StoreCore IdentityInterface')]
    public function testRealEstateAgentImplementsStoreCoreIdentityInterface(): void
    {
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, new RealEstateAgent());
    }

    #[Group('hmvc')]
    #[TestDox('RealEstateAgent is JSON serializable')]
    public function testRealEstateAgentIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new RealEstateAgent());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(RealEstateAgent::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(RealEstateAgent::VERSION);
        $this->assertIsString(RealEstateAgent::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(RealEstateAgent::VERSION, '1.0.0-rc.1', '>=')
        );
    }
}
