<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\{Intangible, Thing};

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CRM\MedicalAudience::class)]
#[UsesClass(\StoreCore\CRM\Audience::class)]
#[UsesClass(\StoreCore\CRM\PeopleAudience::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
final class MedicalAudienceTest extends TestCase
{
    #[TestDox('MedicalAudience class is concrete')]
    public function testMedicalAudienceClassIsConcrete(): void
    {
        $class = new ReflectionClass(MedicalAudience::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('MedicalAudience is a PeopleAudience')]
    public function testMedicalAudienceIsPeopleAudience(): void
    {
        $audience = new MedicalAudience();
        $this->assertInstanceOf(Audience::class, $audience);
        $this->assertInstanceOf(PeopleAudience::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('MedicalAudience is an Intangible Thing')]
    public function testMedicalAudienceIsIntangibleThing(): void
    {
        $audience = new MedicalAudience();
        $this->assertInstanceOf(Intangible::class, $audience);
        $this->assertInstanceOf(Thing::class, $audience);
    }

    #[Group('hmvc')]
    #[TestDox('MedicalAudience is JSON serializable')]
    public function testMedicalAudienceIsJsonSerializable(): void
    {
        $this->assertInstanceOf(\JsonSerializable::class, new MedicalAudience());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(MedicalAudience::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(MedicalAudience::VERSION);
        $this->assertIsString(MedicalAudience::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(MedicalAudience::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('MedicalAudience.additionalType exists')]
    public function testMedicalAudienceAdditionalTypeExists(): void
    {
        $class = new ReflectionClass(MedicalAudience::class);
        $this->assertTrue($class->hasProperty('additionalType'));
    }

    #[Depends('testMedicalAudienceAdditionalTypeExists')]
    #[TestDox('MedicalAudience.additionalType is public')]
    public function testMedicalAudienceAdditionalTypeIsPublic(): void
    {
        $property = new ReflectionProperty(MedicalAudience::class, 'additionalType');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testMedicalAudienceAdditionalTypeExists')]
    #[Depends('testMedicalAudienceAdditionalTypeIsPublic')]
    #[TestDox('MedicalAudience.additionalType is not null')]
    public function testMedicalAudienceAdditionalTypeIsNotNull(): void
    {
        $audience = new MedicalAudience();
        $this->assertNotNull($audience->additionalType);
    }

    #[Depends('testMedicalAudienceAdditionalTypeIsNotNull')]
    #[TestDox('MedicalAudience.additionalType is Schema.org PeopleAudience')]
    public function testMedicalAudienceAdditionalTypeIsSchemaOrgPeopleAudience(): void
    {
        $audience = new MedicalAudience();
        $this->assertIsObject($audience->additionalType);
        $this->assertInstanceOf(\StoreCore\Types\URL::class, $audience->additionalType);
        $this->assertEquals('https://schema.org/PeopleAudience', (string) $audience->additionalType);
    }


    #[TestDox('MedicalAudience.healthCondition exists')]
    public function testMedicalAudienceHealthConditionExists(): void
    {
        $class = new ReflectionClass(MedicalAudience::class);
        $this->assertTrue($class->hasProperty('healthCondition'));
    }

    #[Depends('testMedicalAudienceHealthConditionExists')]
    #[TestDox('MedicalAudience.healthCondition is public')]
    public function testMedicalAudienceHealthConditionIsPublic(): void
    {
        $property = new ReflectionProperty(MedicalAudience::class, 'healthCondition');
        $this->assertTrue($property->isPublic());
    }

    #[Depends('testMedicalAudienceHealthConditionExists')]
    #[TestDox('MedicalAudience.healthCondition is null by default')]
    public function testMedicalAudienceHealthConditionIsNullByDefault(): void
    {
        $audience = new MedicalAudience();
        $this->assertNull($audience->healthCondition);
    }
}
