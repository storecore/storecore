<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\CRM;

use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;
use StoreCore\Types\UUID;

use \ReflectionClass, \ReflectionMethod, \ReflectionProperty;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\CRM\OrganizationFactory::class)]
#[CoversClass(\StoreCore\CRM\OrganizationRepository::class)]
#[CoversClass(\StoreCore\Database\OrganizationMapper::class)]
#[CoversClass(\StoreCore\Database\OrganizationProperties::class)]
#[UsesClass(\StoreCore\Clock::class)]
#[UsesClass(\StoreCore\OnlineStore::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\CRM\Organization::class)]
#[UsesClass(\StoreCore\CRM\OrganizationProxy::class)]
#[UsesClass(\StoreCore\CRM\OrganizationType::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
#[UsesClass(\StoreCore\Types\URL::class)]
#[UsesClass(\StoreCore\Types\UUID::class)]
#[UsesClass(\StoreCore\Types\UUIDFactory::class)]
final class OrganizationFactoryTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $db = $registry->get('Database');
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'Acme Corporation'");
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'GreatFood'");
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'Philippa\'s Pharmacy'");
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'Protocabulators incorporated'");
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'Protocabulators Incorporated'");
        $db->exec("DELETE FROM `sc_organizations` WHERE `common_name` = 'Awesome Marketplace'");
    }


    #[Group('hmvc')]
    #[TestDox('OrganizationFactory class is concrete')]
    public function testOrganizationFactoryClassIsConcrete(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('OrganizationFactory is a database model')]
    public function testOrganizationFactoryIsDatabaseModel(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertTrue($class->isSubclassOf(\StoreCore\Database\AbstractModel::class));
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(OrganizationFactory::VERSION);
        $this->assertIsString(OrganizationFactory::VERSION);
    }

    #[Depends('testVersionConstantIsNonEmptyString')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(OrganizationFactory::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('OrganizationFactory::createOnlineBusiness() exists')]
    public function testOrganizationFactoryCreateOnlineBusinessExists(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertTrue($class->hasMethod('createOnlineBusiness'));
    }

    #[Depends('testOrganizationFactoryCreateOnlineBusinessExists')]
    #[TestDox('OrganizationFactory::createOnlineBusiness() is public')]
    public function testOrganizationFactoryCreateOnlineBusinessIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOnlineBusiness');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationFactoryCreateOnlineBusinessExists')]
    #[TestDox('OrganizationFactory::createOnlineBusiness() has one OPTIONAL parameter')]
    public function testOrganizationFactoryCreateOnlineBusinessHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOnlineBusiness');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationFactoryCreateOnlineBusinessExists')]
    #[TestDox('OrganizationFactory::createOnlineBusiness() returns Organization object')]
    public function testOrganizationFactoryCreateOnlineBusinessReturnsOrganizationObject(): void
    {
        $registry = Registry::getInstance();
        if (!$registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOnlineBusiness();
        $this->assertIsObject($organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);
        $this->assertInstanceOf(\StoreCore\OnlineBusiness::class, $organization);
        $this->assertNotInstanceOf(\StoreCore\OnlineStore::class, $organization);
    }

    #[Depends('testOrganizationFactoryCreateOnlineBusinessHasOneOptionalParameter')]
    #[Depends('testOrganizationFactoryCreateOnlineBusinessReturnsOrganizationObject')]
    #[TestDox('OrganizationFactory::createOnlineBusiness() returns Organization object')]
    public function testOrganizationFactoryCreateOnlineBusinessSetsOrganizationName(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOnlineBusiness('Acme Corporation');
        $this->assertEquals('Acme Corporation', $organization->name);
    }


    #[TestDox('OrganizationFactory::createOnlineStore() exists')]
    public function testOrganizationFactoryCreateOnlineStoreExists(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertTrue($class->hasMethod('createOnlineStore'));
    }

    #[Depends('testOrganizationFactoryCreateOnlineStoreExists')]
    #[TestDox('OrganizationFactory::createOnlineStore() is public')]
    public function testOrganizationFactoryCreateOnlineStoreIsPublic(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOnlineStore');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationFactoryCreateOnlineStoreExists')]
    #[TestDox('OrganizationFactory::createOnlineStore() has one OPTIONAL parameter')]
    public function testOrganizationFactoryCreateOnlineStoreHasOneOptionalParameter(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOnlineStore');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationFactoryCreateOnlineStoreExists')]
    #[Depends('testOrganizationFactoryCreateOnlineStoreIsPublic')]
    #[Depends('testOrganizationFactoryCreateOnlineStoreHasOneOptionalParameter')]
    #[TestDox('OrganizationFactory::createOnlineStore() returns StoreCore\OnlineStore entity')]
    public function testOrganizationFactoryCreateOnlineStoreReturnsStoreCoreOnlineStoreEntity(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOnlineStore('Protocabulators Incorporated');

        $this->assertIsObject($organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);
        $this->assertInstanceOf(\StoreCore\OnlineBusiness::class, $organization);
        $this->assertInstanceOf(\StoreCore\OnlineStore::class, $organization);

        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $organization);
        $this->assertTrue($organization->hasIdentifier());
    }


    #[TestDox('OrganizationFactory::createOrganization() exists')]
    public function testOrganizationFactoryCreateOrganizationExists(): void
    {
        $class = new ReflectionClass(OrganizationFactory::class);
        $this->assertTrue($class->hasMethod('createOrganization'));
    }

    #[Depends('testOrganizationFactoryCreateOrganizationExists')]
    #[TestDox('OrganizationFactory::createOrganization() is final public')]
    public function testOrganizationFactoryCreateOrganizationIsFinalPublic(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOrganization');
        $this->assertFalse($method->isAbstract());
        $this->assertTrue($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testOrganizationFactoryCreateOrganizationExists')]
    #[TestDox('OrganizationFactory::createOrganization() has two OPTIONAL parameters')]
    public function testOrganizationFactoryCreateOrganizationHasTwoOptionalParameters(): void
    {
        $method = new ReflectionMethod(OrganizationFactory::class, 'createOrganization');
        $this->assertEquals(2, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testOrganizationFactoryCreateOrganizationExists')]
    #[TestDox('OrganizationFactory::createOrganization() returns Organization entity')]
    public function testOrganizationFactoryCreateOrganizationReturnsOrganizationEntity(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $organization = $factory->createOrganization();
        $this->assertIsObject($organization);
        $this->assertInstanceOf(\StoreCore\CRM\Organization::class, $organization);
        $this->assertNotInstanceOf(\StoreCore\OnlineBusiness::class, $organization);
        $this->assertNotInstanceOf(\StoreCore\OnlineStore::class, $organization);

        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $organization);
        $this->assertTrue($organization->hasIdentifier());
    }

    #[Depends('testOrganizationFactoryCreateOrganizationReturnsOrganizationEntity')]
    #[TestDox('OrganizationFactory::createOrganization() sets Organization.name and Organization.type')]
    public function testOrganizationFactoryCreateOrganizationSetsOrganizationNameAndOrganizationType(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);

        $organization = $factory->createOrganization('Acme Corporation');
        $this->assertEquals('Acme Corporation', $organization->name);
        $this->assertEquals('Organization', $organization->type->name);

        $organization = $factory->createOrganization('Philippa\'s Pharmacy', OrganizationType::Pharmacy);
        $this->assertNotEquals('Organization', $organization->type->name);
        $this->assertEquals('Pharmacy', $organization->type->name);
    }


    #[TestDox('OrganizationFactory.parentOrganization exists')]
    public function testOrganizationFactoryParentOrganizationExists(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $this->assertObjectHasProperty('parentOrganization', $factory);
    }

    #[Depends('testOrganizationFactoryParentOrganizationExists')]
    #[TestDox('OrganizationFactory.parentOrganization is public')]
    public function testOrganizationFactoryParentOrganizationIsPublic(): void
    {
        $property = new ReflectionProperty(OrganizationFactory::class, 'parentOrganization');
        $this->assertTrue($property->isPublic());
        $this->assertFalse($property->isReadOnly());
    }

    #[Depends('testOrganizationFactoryParentOrganizationExists')]
    #[Depends('testOrganizationFactoryParentOrganizationIsPublic')]
    #[TestDox('OrganizationFactory.parentOrganization is public')]
    public function testOrganizationFactoryParentOrganizationIsNullByDefault(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $this->assertNull($factory->parentOrganization);
    }

    #[Depends('testOrganizationFactoryParentOrganizationIsNullByDefault')]
    #[TestDox('OrganizationFactory.parentOrganization accepts Organization')]
    public function testOrganizationFactoryParentOrganizationAcceptsOrganization(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $factory->parentOrganization = new Organization('Acme Corporation');

        $this->assertNotNull($factory->parentOrganization);
        $this->assertIsObject($factory->parentOrganization);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $factory->parentOrganization);
    }

    #[Depends('testOrganizationFactoryParentOrganizationIsNullByDefault')]
    #[Depends('testOrganizationFactoryParentOrganizationAcceptsOrganization')]
    #[TestDox('OrganizationFactory.parentOrganization accepts OrganizationProxy')]
    public function testOrganizationFactoryParentOrganizationAcceptsOrganizationProxy(): void
    {
        $parent = new OrganizationProxy(
            new UUID('11dc3dbc-6969-4363-abe8-4994f43f6438'),
            OrganizationType::OnlineBusiness,
            'Awesome Marketplace',
        );

        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);
        $factory->parentOrganization = $parent;

        $this->assertNotNull($factory->parentOrganization);
        $this->assertIsObject($factory->parentOrganization);
        $this->assertInstanceOf(\StoreCore\IdentityInterface::class, $factory->parentOrganization);
        $this->assertInstanceOf(\StoreCore\ProxyInterface::class, $factory->parentOrganization);
    }

    /**
     * @see https://schema.org/OnlineBusiness#eg-0473
     *      Schema.org `OnlineBusiness` example 1
     */
    #[TestDox('OnlineBusiness with an OnlineStore')]
    public function testOnlineBusinessWithOnlineStore(): void
    {
        $registry = Registry::getInstance();
        $factory = new OrganizationFactory($registry);

        $onlineBusiness = $factory->createOnlineBusiness('Awesome Marketplace');
        $this->assertTrue($onlineBusiness->hasIdentifier());

        $factory->parentOrganization = $onlineBusiness;
        $this->assertSame($onlineBusiness->getIdentifier(), $factory->parentOrganization->getIdentifier());

        $onlineStore = $factory->createOnlineStore('Protocabulators Incorporated');
        $this->assertTrue($onlineStore->hasIdentifier());
        $this->assertSame($onlineBusiness, $onlineStore->parentOrganization);
    }
}
