<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore;

use \StoreCore\ShoppingList;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \version_compare;

#[CoversClass(\StoreCore\ShoppingList::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Metadata::class)]
final class ShoppingListTest extends TestCase
{
    private ShoppingList $shoppingList;

    public function setUp(): void
    {
        $registry = Registry::getInstance();
        $this->shoppingList = new ShoppingList($registry);
    }

    #[Group('hmvc')]
    #[TestDox('ShoppingList class is concrete')]
    public function testShoppingListClassIsConcrete()
    {
        $class = new ReflectionClass(ShoppingList::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('ShoppingList is an Order')]
    public function testShoppingListIsAnOrder(): void
    {
        $this->assertInstanceOf(Order::class, $this->shoppingList);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(ShoppingList::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(ShoppingList::VERSION);
        $this->assertIsString(ShoppingList::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(ShoppingList::VERSION, '1.0.0-alpha.1', '>=')
        );
    }
}
