<?php

declare(strict_types=1);

namespace StoreCore\PostNL\SendAndTrack;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[UsesClass(\StoreCore\OML\Provider::class)]
final class ProviderTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Provider class exists')]
    public function testProviderClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'SendAndTrack' . DIRECTORY_SEPARATOR . 'Provider.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'SendAndTrack' . DIRECTORY_SEPARATOR . 'Provider.php');

        $this->assertTrue(class_exists('\\StoreCore\\PostNL\\SendAndTrack\\Provider'));
        $this->assertTrue(class_exists(\StoreCore\PostNL\SendAndTrack\Provider::class));
    }

    #[TestDox('Default provider.name is (string) "PostNL"')]
    public function testDefaultProviderNameIsStringPostNL(): void
    {
        $provider = new Provider();
        $this->assertEquals('PostNL', $provider->getName());
    }

    #[TestDox('Default provider.url is (string) "https://www.postnl.nl/"')]
    public function testDefaultProviderUrlIsStringHttpsWwwPostnlNl(): void
    {
        $provider = new Provider();
        $this->assertNotEmpty($provider->getUrl());
        $this->assertIsString($provider->getUrl());
        $this->assertEquals('https://www.postnl.nl/', $provider->getUrl());
    }

    #[Depends('testDefaultProviderUrlIsStringHttpsWwwPostnlNl')]
    #[TestDox('Default provider.url can be set to "https://www.postnl.be/"')]
    public function testProviderUrlCanBeSetToHttpsWwwPostnlBe(): void
    {
        $provider = new Provider();
        $provider->setURL('https://www.postnl.be/');
        $this->assertNotEquals('https://www.postnl.nl/', $provider->getUrl());
        $this->assertEquals('https://www.postnl.be/', $provider->getUrl());
    }
}
