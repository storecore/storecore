<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PostNL\SendAndTrack;

use StoreCore\Geo\CountryCodes;

use \ReflectionClass, \ReflectionMethod;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\OML\PostalCode::class)]
#[CoversClass(\StoreCore\PostNL\SendAndTrack\TrackingUrl::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class TrackingUrlTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('TrackingUrl class exists')]
    public function testTrackingUrlClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'SendAndTrack' . DIRECTORY_SEPARATOR . 'TrackingUrl.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'SendAndTrack' . DIRECTORY_SEPARATOR . 'TrackingUrl.php');

        $this->assertTrue(class_exists('\\StoreCore\\PostNL\\SendAndTrack\\TrackingUrl'));
        $this->assertTrue(class_exists(TrackingUrl::class));
    }

    #[Group('hmvc')]
    #[TestDox('TrackingUrl class is concrete')]
    public function testTrackingUrlClassIsConcrete(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('TrackingUrl is stringable')]
    public function testTrackingUrlIsStringable(): void
    {
        $url = new TrackingUrl();
        $this->assertInstanceOf(\Stringable::class, $url);
    }

    #[Group('hmvc')]
    #[TestDox('TrackingUrl class implements StoreCore\OML\TrackingInterface')]
    public function testTrackingUrlClassImplementsStoreCoreShippingTrackingInterface()
    {
        $this->assertTrue(interface_exists('\\StoreCore\\OML\\TrackingInterface'));

        $url = new TrackingUrl();
        $this->assertInstanceOf(\StoreCore\OML\TrackingInterface::class, $url);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(TrackingUrl::VERSION);
        $this->assertIsString(TrackingUrl::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(TrackingUrl::VERSION, '1.0.0-alpha.1', '>=')
        );
    }


    #[TestDox('TrackingUrl.destination exists')]
    public function testTrackingUrlDestinationExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasProperty('destination'));
    }

    #[TestDox('TrackingUrl::getDestination exists')]
    public function testTrackingUrlGetDestinationExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('getDestination'));
    }

    #[Depends('testTrackingUrlGetDestinationExists')]
    #[TestDox('TrackingUrl::getDestination is public')]
    public function testTrackingUrlGetDestinationIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getDestination');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlGetDestinationExists')]
    #[TestDox('TrackingUrl::getDestination has no parameters')]
    public function testTrackingUrlGetDestinationHasNoParameters(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getDestination');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testTrackingUrlGetDestinationExists')]
    #[Depends('testTrackingUrlGetDestinationIsPublic')]
    #[Depends('testTrackingUrlGetDestinationHasNoParameters')]
    #[TestDox('TrackingUrl::getDestination returns non-empty string with two uppercase letters')]
    public function testTrackingUrlGetDestinationReturnsNonEmptyStringWithTwoUppercaseLetters(): void
    {
        $url = new TrackingUrl();
        $this->assertNotEmpty($url->getDestination());
        $this->assertIsString($url->getDestination());
        $this->assertEquals(2, strlen($url->getDestination()));
        $this->assertTrue(ctype_upper($url->getDestination()));
    }

    #[Depends('testTrackingUrlGetDestinationReturnsNonEmptyStringWithTwoUppercaseLetters')]
    #[TestDox('TrackingUrl.destination is "NL" for the Netherlands by default')]
    public function testTrackingUrlDestinationIsNlForTheNetherlandsByDefault(): void
    {
        $url = new TrackingUrl();
        $this->assertEquals('NL', $url->destination);
        $this->assertEquals('NL', $url->getDestination());
    }

    #[TestDox('TrackingUrl::setDestination exists')]
    public function testTrackingUrlSetDestinationExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('setDestination'));
    }

    #[Depends('testTrackingUrlSetDestinationExists')]
    #[TestDox('TrackingUrl::setDestination is public')]
    public function testTrackingUrlSetDestinationIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setDestination');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlSetDestinationExists')]
    #[TestDox('TrackingUrl::setDestination has one REQUIRED parameter')]
    public function testTrackingUrlSetDestinationHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setDestination');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testTrackingUrlSetDestinationHasOneRequiredParameter')]
    #[TestDox('TrackingUrl::setDestination accepts CountryCode value object')]
    public function testTrackingUrlSetDestinationAcceptsCountryCodeValueObject(): void
    {
        $country_code = CountryCodes::createCountryCode('BEL');
        $this->assertInstanceOf(\StoreCore\Types\CountryCode::class, $country_code);

        $url = new TrackingUrl();
        $this->assertNotEquals('BE', $url->destination);
        $this->assertNotEquals('BE', $url->getDestination());

        $url->setDestination($country_code);
        $this->assertEquals('BE', $url->destination);
        $this->assertEquals('BE', $url->getDestination());
    }


    #[TestDox('TrackingUrl::__construct exists')]
    public function testTrackingUrlConstructorExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('__construct'));
    }

    #[Depends('testTrackingUrlConstructorExists')]
    #[TestDox('TrackingUrl::__construct is public constructor')]
    public function testTrackingUrlConstructorIsPublicConstructor(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, '__construct');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
        $this->assertTrue($method->isConstructor());
    }

    #[Depends('testTrackingUrlConstructorExists')]
    #[TestDox('TrackingUrl::__construct has three OPTIONAL parameters')]
    public function testTrackingUrlConstructorHasThreeOptionalParameters(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, '__construct');
        $this->assertEquals(3, $method->getNumberOfParameters());
        $this->assertEquals(0, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testTrackingUrlConstructorHasThreeOptionalParameters')]
    #[TestDox('TrackingUrl::__construct sets tracking number')]
    public function testTrackingUrlConstructorSetsTrackingNumber(): void
    {
        $url = new TrackingUrl('3SQBEF0052264');
        $this->assertEquals('3SQBEF0052264', $url->trackingNumber);
        $this->assertEquals('3SQBEF0052264', $url->getTrackingNumber());
    }

    #[Depends('testTrackingUrlConstructorHasThreeOptionalParameters')]
    #[TestDox('TrackingUrl::__construct sets destination')]
    public function testTrackingUrlConstructorSetsDestination(): void
    {
        $url = new TrackingUrl('3SQBEF0052264', 'GB');
        $this->assertEquals('GB', $url->destination);
        $this->assertEquals('GB', $url->getDestination());

        $url = new TrackingUrl('3SQBEF0052264', 'NL');
        $this->assertEquals('NL', $url->destination);
        $this->assertEquals('NL', $url->getDestination());
    }

    #[Depends('testTrackingUrlConstructorHasThreeOptionalParameters')]
    #[TestDox('TrackingUrl::__construct sets postal code')]
    public function testTrackingUrlConstructorSetsPostalCode(): void
    {
        $url = new TrackingUrl('3SQBEF0052264', 'GB', 'BD46AU');
        $this->assertEquals('BD46AU', $url->postalCode);
        $this->assertEquals('BD46AU', $url->getPostalCode());
    }


    #[TestDox('TrackingUrl::inLanguage exists')]
    public function testTrackingUrlInLanguageExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('inLanguage'));
    }

    #[Depends('testTrackingUrlInLanguageExists')]
    #[TestDox('TrackingUrl implements StoreCore LanguageInterface')]
    public function testTrackingUrlImplementsStoreCoreLanguageInterface(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\LanguageInterface'));

        $url = new TrackingUrl('3SQBEF0052264', 'GB', 'BD46AU');
        $this->assertInstanceOf(\StoreCore\LanguageInterface::class, $url);
    }

    #[Depends('testTrackingUrlInLanguageExists')]
    #[TestDox('TrackingUrl::inLanguage is "en" for English by default')]
    public function testTrackingUrInlLanguageIsEnForEnglishByDefault(): void
    {
        $url = new TrackingUrl();
        $this->assertEquals('en', $url->inLanguage());
    }

    #[Depends('testTrackingUrInlLanguageIsEnForEnglishByDefault')]
    #[TestDox('TrackingUrl::$language or TrackingUrl::$L is uppercase "EN" for English by default')]
    public function testTrackingUrlLanguageOrTrackingUrlLIsUppercaseEnForEnglishByDefault(): void
    {
        $url = new TrackingUrl();
        $this->assertEquals('EN', $url->language);
        $this->assertEquals('EN', $url->L);
    }

    #[Depends('testTrackingUrInlLanguageIsEnForEnglishByDefault')]
    #[TestDox('TrackingUrl::$language can be set to German and French')]
    public function testTrackingUrlLanguageCanBeSetToGermanAndFrench(): void
    {
        $url = new TrackingUrl();

        $url->language = 'DE';
        $this->assertEquals('DE', $url->language);

        $url->language = 'FR';
        $this->assertNotEquals('DE', $url->language);
        $this->assertEquals('FR', $url->language);
    }


    #[TestDox('TrackingUrl::$postalCode exists')]
    public function testTrackingUrlPostalCodeExists(): void
    {
        $url = new TrackingUrl();
        $this->assertObjectHasProperty('postalCode', $url);
    }

    #[Depends('testTrackingUrlPostalCodeExists')]
    #[TestDox('TrackingUrl::$postalCode is public')]
    public function testTrackingUrlPostalCodeIsPublic(): void
    {
        $url = new TrackingUrl('3SQBEF0052264');
        $url->postalCode = 'BD46AU';
        $this->assertEquals('BD46AU', $url->postalCode);
        $this->assertEquals($url->getPostalCode(), $url->postalCode);
    }

    #[TestDox('TrackingUrl::setPostalCode exists')]
    public function testTrackingUrlSetPostalCodeExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('setPostalCode'));
    }

    #[Depends('testTrackingUrlSetPostalCodeExists')]
    #[TestDox('TrackingUrl::setPostalCode is public')]
    public function testTrackingUrlSetPostalCodeIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setPostalCode');
        $this->assertFalse($method->isAbstract());
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlSetPostalCodeExists')]
    #[TestDox('TrackingUrl::setPostalCode has one REQUIRED parameter')]
    public function testTrackingUrlSetPostalCodeHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setPostalCode');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testTrackingUrlSetPostalCodeExists')]
    #[Depends('testTrackingUrlSetPostalCodeIsPublic')]
    #[Depends('testTrackingUrlSetPostalCodeHasOneRequiredParameter')]
    #[TestDox('TrackingUrl::setPostalCode sets postalCode as string')]
    public function testTrackingUrlSetPostalCodeSetsPostalCodeAsString(): void
    {
        $url = new TrackingUrl();
        $url->setPostalCode('BD46AU');
        $this->assertEquals('BD46AU', $url->postalCode);

        $url->setPostalCode('2132JB');
        $this->assertNotEquals('BD46AU', $url->postalCode);
        $this->assertEquals('2132JB', $url->postalCode);
    }

    #[Depends('testTrackingUrlSetPostalCodeSetsPostalCodeAsString')]
    #[TestDox('TrackingUrl::setPostalCode removes spaces from postal codes')]
    public function testTrackingUrlSetPostalCodeRemovesSpacesFromPostalCodes(): void
    {
        $url = new TrackingUrl();
        $url->setPostalCode('BD4 6AU');
        $this->assertEquals('BD46AU', $url->postalCode);
        $url->setPostalCode('2132 JB');
        $this->assertEquals('2132JB', $url->postalCode);
    }

    #[Depends('testTrackingUrlSetPostalCodeSetsPostalCodeAsString')]
    #[TestDox('TrackingUrl::setPostalCode converts postal codes to uppercase')]
    public function testTrackingUrlSetPostalCodeConvertsPostalCodesToUppercase(): void
    {
        $url = new TrackingUrl();
        $url->setPostalCode('bd46au');
        $this->assertEquals('BD46AU', $url->postalCode);
        $url->setPostalCode('2132jb');
        $this->assertEquals('2132JB', $url->postalCode);
    }

    #[Depends('testTrackingUrlSetPostalCodeExists')]
    #[Depends('testTrackingUrlSetPostalCodeIsPublic')]
    #[Depends('testTrackingUrlSetPostalCodeHasOneRequiredParameter')]
    #[TestDox('TrackingUrl::setPostalCode accepts StoreCore\OML\PostalCode object')]
    public function testTrackingUrlSetPostalCodeAcceptsStoreCoreOMLPostalCodeObject(): void
    {
        $url = new TrackingUrl();

        $postal_code = new \StoreCore\OML\PostalCode('BD4 6AU');
        $url->setPostalCode($postal_code);
        $this->assertEquals('BD46AU', $url->postalCode);

        $url->setPostalCode('2132jb');
        $this->assertEquals('2132JB', $url->postalCode);
    }

    #[TestDox('TrackingUrl::getPostalCode exists')]
    public function testTrackingUrlGetPostalCodeExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('getPostalCode'));
    }

    #[Depends('testTrackingUrlGetPostalCodeExists')]
    #[TestDox('TrackingUrl::getPostalCode is public')]
    public function testTrackingUrlGetPostalCodeIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getPostalCode');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlGetPostalCodeExists')]
    #[TestDox('TrackingUrl::getPostalCode has no parameters')]
    public function testTrackingUrlGetPostalCodeHasNoParameters(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getPostalCode');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testTrackingUrlGetPostalCodeExists')]
    #[Depends('testTrackingUrlGetPostalCodeIsPublic')]
    #[Depends('testTrackingUrlGetPostalCodeHasNoParameters')]
    #[TestDox('TrackingUrl::getPostalCode returns postalCode')]
    public function testTrackingUrlGetPostalCodeReturnsPostalCode(): void
    {
        $url = new TrackingUrl('3SQBEF0052264');
        $url->setPostalCode('BD46AU');
        $this->assertEquals('BD46AU', $url->getPostalCode());
        $this->assertEquals($url->postalCode, $url->getPostalCode());
    }


    #[TestDox('TrackingUrl::$trackingNumber exists')]
    public function testTrackingUrlTrackingNumberExists(): void
    {
        $url = new TrackingUrl();
        $this->assertObjectHasProperty('trackingNumber', $url);
    }

    #[Depends('testTrackingUrlTrackingNumberExists')]
    #[TestDox('TrackingUrl::$trackingNumber is public')]
    public function testTrackingUrlTrackingNumberIsPublic(): void
    {
        $url = new TrackingUrl();
        $url->trackingNumber = '3SQBEF0052264';
        $this->assertEquals('3SQBEF0052264', $url->trackingNumber);
    }

    #[Depends('testTrackingUrlTrackingNumberIsPublic')]
    #[TestDox('TrackingUrl::$trackingNumber must be initialized')]
    public function testTrackingUrlTrackingNumberMustBeInitialized(): void
    {
        $url = new TrackingUrl();
        $this->expectException('Error');
        $error = $url->trackingNumber;
    }

    #[TestDox('TrackingUrl::setTrackingNumber exists')]
    public function testTrackingUrlSetTrackingNumberExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('setTrackingNumber'));
    }

    #[Depends('testTrackingUrlSetTrackingNumberExists')]
    #[TestDox('TrackingUrl::setTrackingNumber is public')]
    public function testTrackingUrlSetTrackingNumberIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setTrackingNumber');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlSetTrackingNumberExists')]
    #[TestDox('TrackingUrl::setTrackingNumber has one REQUIRED parameter')]
    public function testTrackingUrlSetTrackingNumberHasOneRequiredParameter(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'setTrackingNumber');
        $this->assertEquals(1, $method->getNumberOfParameters());
        $this->assertEquals(1, $method->getNumberOfRequiredParameters());
    }

    #[Depends('testTrackingUrlSetTrackingNumberExists')]
    #[Depends('testTrackingUrlSetTrackingNumberIsPublic')]
    #[Depends('testTrackingUrlSetTrackingNumberHasOneRequiredParameter')]
    #[TestDox('TrackingUrl::setTrackingNumber sets trackingNumber')]
    public function testTrackingUrlSetTrackingNumberSetsTrackingNumber(): void
    {
        $url = new TrackingUrl();
        $url->setTrackingNumber('3SQBEF0052264');
        $this->assertEquals('3SQBEF0052264', $url->trackingNumber);

        $url->setTrackingNumber('3SQBEF000052310');
        $this->assertNotEquals('3SQBEF0052264', $url->trackingNumber);
        $this->assertEquals('3SQBEF000052310', $url->trackingNumber);
    }

    #[TestDox('TrackingUrl::getTrackingNumber exists')]
    public function testTrackingUrlGetTrackingNumberExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('getTrackingNumber'));
    }

    #[Depends('testTrackingUrlGetTrackingNumberExists')]
    #[TestDox('TrackingUrl::getTrackingNumber is public')]
    public function testTrackingUrlGetTrackingNumberIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getTrackingNumber');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlGetTrackingNumberExists')]
    #[TestDox('TrackingUrl::getTrackingNumber has no parameters')]
    public function testTrackingUrlGetTrackingNumberHasNoParameters(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getTrackingNumber');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testTrackingUrlGetTrackingNumberExists')]
    #[Depends('testTrackingUrlGetTrackingNumberIsPublic')]
    #[Depends('testTrackingUrlGetTrackingNumberHasNoParameters')]
    #[TestDox('TrackingUrl::getTrackingNumber returns tracking number')]
    public function testTrackingUrlGetTrackingNumberReturnsTrackingNumber(): void
    {
        $url = new TrackingUrl('3SQBEF0052264');
        $this->assertEquals('3SQBEF0052264', $url->getTrackingNumber());
        $this->assertEquals($url->trackingNumber, $url->getTrackingNumber());
    }


    #[TestDox('TrackingUrl::getTrackingUrl exists')]
    public function testTrackingUrlGetTrackingUrlExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasMethod('getTrackingUrl'));
    }

    #[Depends('testTrackingUrlGetTrackingUrlExists')]
    #[TestDox('TrackingUrl::getTrackingUrl is public')]
    public function testTrackingUrlGetTrackingUrlIsPublic(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getTrackingUrl');
        $this->assertFalse($method->isFinal());
        $this->assertTrue($method->isPublic());
        $this->assertFalse($method->isStatic());
    }

    #[Depends('testTrackingUrlGetTrackingUrlExists')]
    #[TestDox('TrackingUrl::getTrackingUrl has no parameters')]
    public function testTrackingUrlGetTrackingUrlHasNoParameters(): void
    {
        $method = new ReflectionMethod(TrackingUrl::class, 'getTrackingUrl');
        $this->assertEquals(0, $method->getNumberOfParameters());
    }

    #[Depends('testTrackingUrlGetTrackingUrlExists')]
    #[TestDox('TrackingUrl::getTrackingUrl returns URL as string')]
    public function testTrackingUrlGetTrackingUrlReturnsUrlAsString(): void
    {
        $url = new TrackingUrl('3SQBEF0052264');
        $this->assertNotEmpty($url->getTrackingUrl());
        $this->assertIsString($url->getTrackingUrl());
        $this->assertStringStartsWith('https://postnl.nl/tracktrace/', $url->getTrackingUrl());
    }

    #[Depends('testTrackingUrlGetTrackingUrlReturnsUrlAsString')]
    #[TestDox('TrackingUrl::getTrackingUrl returns URL with three REQUIRED parameters')]
    public function testTrackingUrlGetTrackingUrlReturnsUrlWithThreeRequiredParameters(): void
    {
        $url = new TrackingUrl();
        $url->trackingNumber = '3SQBEF0052264';
        $url->destination = 'GB';

        $this->assertStringContainsString('B=3SQBEF0052264', $url->getTrackingUrl());
        $this->assertStringContainsString('D=GB', $url->getTrackingUrl());
        $this->assertStringContainsString('T=C', $url->getTrackingUrl());
    }


    #[TestDox('TrackingUrl.type exists')]
    public function testTrackingUrlTyoeExists(): void
    {
        $class = new ReflectionClass(TrackingUrl::class);
        $this->assertTrue($class->hasProperty('type'));
    }

    #[Depends('testTrackingUrlTyoeExists')]
    #[TestDox('TrackingUrl.type is "C" for Consumer by default')]
    public function testTrackingUrlTyoeIsCForConsumerByDefault(): void
    {
        $url = new TrackingUrl();
        $this->assertEquals('C', $url->type);
    }

    #[Depends('testTrackingUrlTyoeExists')]
    #[TestDox('TrackingUrl.type can be set to "B" for Business')]
    public function testTrackingUrlTyoeCanBeSetToBForBusiness(): void
    {
        $url = new TrackingUrl();
        $this->assertNotEquals('B', $url->type);
        $url->type = 'B';
        $this->assertEquals('B', $url->type);
    }
}
