<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PostNL\Addresses;

use StoreCore\Registry;
use StoreCore\Geo\PostalAddress;
use StoreCore\OML\AddressIndex;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function \class_exists;
use function \version_compare;

#[CoversClass(\StoreCore\PostNL\Addresses\KIX::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\AbstractModel::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\Country::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Geo\CountryRepository::class)]
#[UsesClass(\StoreCore\Geo\PostalAddress::class)]
#[UsesClass(\StoreCore\OML\AddressIndex::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class KIXTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('KIX class exists')]
    public function testKIXClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'Addresses' . DIRECTORY_SEPARATOR . 'KIX.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'PostNL' . DIRECTORY_SEPARATOR . 'Addresses' . DIRECTORY_SEPARATOR . 'KIX.php');

        $this->assertTrue(class_exists('\\StoreCore\\PostNL\\Addresses\\KIX'));
        $this->assertTrue(class_exists(KIX::class));
    }

    #[TestDox('PostNL KIX class is concrete')]
    public function testPostnlKixClassIsConcrete(): void
    {
        $class = new ReflectionClass(KIX::class);
        $this->assertFalse($class->isAbstract());
        $this->assertTrue($class->isInstantiable());
    }

    #[Group('hmvc')]
    #[TestDox('PostNL KIX is a StoreCore\OML\AddressIndex')]
    public function testPostnlKixIsStoreCoreShippingAddressIndex(): void
    {
        $kix = new KIX();
        $this->assertInstanceOf(\StoreCore\OML\AddressIndex::class, $kix);
        $this->assertInstanceOf(AddressIndex::class, $kix);
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(KIX::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(KIX::VERSION);
        $this->assertIsString(KIX::VERSION);
    }

    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(KIX::VERSION, '1.0.0', '>=')
        );
    }


    #[TestDox('PostNL KIX.countryCode is not empty')]
    public function testPostnlKixCountryCodeIsNotEmpty(): void
    {
        $kix = new KIX();
        $this->assertNotEmpty($kix->countryCode);
    }

    #[TestDox('PostNL KIX.countryCode is string "NL" by default')]
    public function testPostnlKixCountryCodeIsStringNLByDefault(): void
    {
        $kix = new KIX();
        $this->assertIsObject($kix->countryCode);
        $this->assertInstanceOf(\Stringable::class, $kix->countryCode);
        $this->assertSame('NL', (string) $kix->countryCode);
    }


    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-nationaal/documentation-rest/
     */
    #[TestDox('PostNL KIX string contains postal code and house number')]
    public function testPostnlKixStringContainsPostalCodeAndHouseNumber(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $postnl_api_address_params = array(
            'Country'     => 'NL',
            'Street'      => 'Siriusdreef',
            'HouseNumber' => '42',
            'Addition'    => 'A',
            'PostalCode'  => '2132WT',
            'City'        => 'Hoofddorp',
        );

        $address = new PostalAddress($postnl_api_address_params);
        $kix = KIX::createFromPostalAddress($address);
        $this->assertNotEmpty((string) $kix);
        $this->assertIsString((string) $kix);
        $this->assertStringContainsString('2132WT42', (string) $kix);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-nationaal/documentation-rest/
     */
    #[TestDox('PostNL KIX string contains house number and optional house number addition')]
    public function testPostnlKixStringContainsHouseNumberAndOptionalHouseNumberAddition(): void
    {
        $postnl_api_address_params = array(
            'Country'     => 'NL',
            'Street'      => 'Siriusdreef',
            'HouseNumber' => '42',
            'Addition'    => 'A',
            'PostalCode'  => '2132WT',
            'City'        => 'Hoofddorp',
        );

        $address = new PostalAddress($postnl_api_address_params);
        $kix = KIX::createFromPostalAddress($address);
        $this->assertNotEmpty((string) $kix);
        $this->assertIsString((string) $kix);
        $this->assertStringEndsWith('42XA', (string) $kix);
    }

    /**
     * @see https://www.postnl.nl/Images/Brochure-KIX-code-van-PostNL_tcm10-10210.pdf
     */
    #[TestDox('PostNL KIX string for addresses outside Netherlands only contain country code plus postal code')]
    public function testPostnlKixStringForAddressesOutsideNetherlandsOnlyContainCountryCodePlusPostalCode(): void
    {
        $registry = Registry::getInstance();
        if (true !== $registry->has('Database')) {
            $this->markTestSkipped('StoreCore database is not available.');
        }

        $postnl_api_address_params = array(
            'CountryIso'          => 'BE',  // ISO alpha-2
            'City'                => 'Antwerpen',
            'PostalCode'          => '2000',
            'Street'              => 'Oude Koornmarkt',
            'HouseNumber'         => '39',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($postnl_api_address_params);
        $kix = KIX::createFromPostalAddress($address);
        $this->assertStringStartsWith('BE', (string) $kix);
        $this->assertSame('BE2000', (string) $kix);


        $postnl_api_address_params = array(
            'CountryIso'          => 'BEL',  // ISO alpha-3
            'City'                => 'Antwerpen',
            'PostalCode'          => '2000',
            'Street'              => 'Oude Koornmarkt',
            'HouseNumber'         => '39',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($postnl_api_address_params);
        $kix = KIX::createFromPostalAddress($address);
        $this->assertSame('BE2000', (string) $kix);
    }
}
