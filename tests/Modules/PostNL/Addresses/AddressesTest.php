<?php declare(strict_types=1);

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\PostNL\Addresses;

use StoreCore\Registry;
use StoreCore\Database\Connection;
use StoreCore\OML\PostalAddress;

use \Exception;
use PHPUnit\Framework\Attributes\{CoversClass, UsesClass};
use PHPUnit\Framework\Attributes\{Test, TestDox};
use PHPUnit\Framework\TestCase;

use function \json_decode;

/**
 * Tests all PostNL request and response address models.
 *
 * @see https://developer.postnl.nl/browse-apis/addresses/
 *      PostNL Addresses APIs
 */
#[CoversClass(\StoreCore\OML\PostalAddress::class)]
#[UsesClass(\StoreCore\Registry::class)]
#[UsesClass(\StoreCore\Database\Connection::class)]
#[UsesClass(\StoreCore\Geo\CountryCodes::class)]
#[UsesClass(\StoreCore\Types\CountryCode::class)]
final class AddressesTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        try {
            $registry = Registry::getInstance();
            if (!$registry->has('Database')) {
                $registry->set('Database', new Connection());
            }
        } catch (Exception $e) {
            $this->markTestSkipped('StoreCore database is not available.');
        }
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-basis-nationaal/documentation-rest/
     */
    #[Test]
    #[TestDox('PostNL Adrescheck Basis Nationaal (basic national address check)')]
    public function PostnlAdrescheckBasisNationaal(): void
    {
        $request = array(
            'postalcode'  => '2592AK',
            'housenumber' => '25',
        );

        $address = new PostalAddress($request);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame('2592AK', $address->postal_code);
        $this->assertSame('25', $address->house_number);


        $response = '
            {
              "status": "1", 
              "streetName": "Prinses Beatrixlaan",
              "houseNumber": "23",
              "postalCode": "2595AK",
              "city": "’S-GRAVENHAGE",
              "areaCode": "070"
            }
        ';

        $params = json_decode($response, true);
        $address = new PostalAddress($params);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame('2595AK', $address->postal_code);
        $this->assertSame('23', $address->house_number);
        $this->assertSame('Prinses Beatrixlaan', $address->street_name);
        $this->assertSame('’S-GRAVENHAGE', $address->locality);


        $request = array(
            'postalcode' => '2595AK',
        );

        $address = new PostalAddress($request);
        $this->assertSame('2595AK', $address->postal_code);
        $this->assertNull($address->house_number);

        $response = '
            {
              "streetName": "Prinses Beatrixlaan",
              "city": "’S-GRAVENHAGE"
            }
        ';

        $params = json_decode($response, true);
        $address = new PostalAddress($params);
        $this->assertNull($address->postal_code);
        $this->assertSame('Prinses Beatrixlaan', $address->street_name);
        $this->assertSame('’S-GRAVENHAGE', $address->locality);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-nationaal/documentation-rest/
     */
    #[Test]
    #[TestDox('PostNL Adrescheck Nationaal (national address check)')]
    public function PostnlAdrescheckNationaal(): void
    {
        $message = array(
            'Country'     => 'NL',
            'Street'      => 'Siriusdreef',
            'HouseNumber' => '42',
            'Addition'    => 'A',
            'PostalCode'  => '2132WT',
            'City'        => 'Hoofddorp',
        );

        $address = new PostalAddress($message);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame(528, $address->country_id);
        $this->assertSame('Siriusdreef', $address->street_name);
        $this->assertSame('42', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);
        $this->assertSame('2132WT', $address->postal_code);
        $this->assertSame('Hoofddorp', $address->locality);


        $request = '
            {
              "PostalCode": "2132WT", 
              "City": " ",
              "Street": " ",
              "HouseNumber": "42",
              "Addition": "A"
            }
        ';

        $params = json_decode($request, true);
        $address = new PostalAddress($params);
        $this->assertNull($address->country_id);
        $this->assertSame('2132WT', $address->postal_code);
        $this->assertNull($address->locality);
        $this->assertNull($address->street_name);
        $this->assertSame('42', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);


        $response = '
            [
              {
                "City": "HOOFDDORP",
                "PostalCode": "2132WT",
                "Street": "Siriusdreef",
                "HouseNumber": 42,
                "Addition": "A",
                "FormattedAddress": [
                  "Siriusdreef 42 A",
                  "2132WT HOOFDDORP"
                ]
              }
            ]
        ';

        $params = json_decode($response, true);
        $address = new PostalAddress($params);
        $this->assertSame('HOOFDDORP', $address->locality);
        $this->assertSame('2132WT', $address->postal_code);
        $this->assertSame('Siriusdreef', $address->street_name);
        $this->assertSame('42', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);


        $request = '
            {
              "AddressType": "01",
              "Countrycode": "NL",
              "Zipcode": "1015EA",
              "City": "Amsterdam",
              "Street": "Prinsengracht",
              "HouseNr": "112",
              "HouseNrExt": "a",
              "Name": "ReceiverName"
            }
        ';

        $params = json_decode($request, true);
        $address = new PostalAddress($params);
        $this->assertSame(528, $address->country_id);
        $this->assertSame('1015EA', $address->postal_code);
        $this->assertSame('Amsterdam', $address->locality);
        $this->assertSame('Prinsengracht', $address->street_name);
        $this->assertSame('112', $address->house_number);
        $this->assertSame('a', $address->house_number_addition);


        $request = '
            {
              "AddressType": "02",
              "Countrycode": "NL",
              "Zipcode": "1015NA",
              "City": "Amsterdam",
              "Street": "Noordermarkt",
              "HouseNr": "48",
              "HouseNrExt": "-56",
              "Name": "SenderName"
            }
        ';

        $params = json_decode($request, true);
        $address = new PostalAddress($params);
        $this->assertSame(528, $address->country_id);
        $this->assertSame('1015NA', $address->postal_code);
        $this->assertSame('Amsterdam', $address->locality);
        $this->assertSame('Noordermarkt', $address->street_name);
        $this->assertSame('48', $address->house_number);
        $this->assertSame('56', $address->house_number_addition);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/geo-adrescheck-nationaal/documentation-rest-v2/
     */
    #[Test]
    #[TestDox('PostNL Geo Adrescheck Nationaal V2 (national geographic address check)')]
    public function PostnlGeoAdrescheckNationaal(): void
    {
        // Address with postal code and house number only
        $request = array(
            'PostalCode'  => '1234AB',
            'HouseNumber' => '123',
        );

        $address = new PostalAddress($request);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);

        $this->assertFalse($address->has('HouseNumberAddition'));
        $this->assertNull($address->house_number_addition);

        // Address with optional house number addition
        $request = array(
            'PostalCode'          => '1234AB',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($request);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);

        $this->assertTrue($address->has('HouseNumberAddition'));
        $this->assertSame('A', $address->house_number_addition);

        // Address with city and street instead of postalcode
        $request = array(
            'City'                => 'Amsterdam',
            'Street'              => 'Dam',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($request);
        $this->assertNull($address->postal_code);
        $this->assertSame('Amsterdam', $address->locality);
        $this->assertSame('Dam', $address->street_name);
        $this->assertSame('123', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);

        // JSON API request
        $request = '{ "PostalCode": "1507TN", "City": "", "Street": "", "HouseNumber": "6", "HouseNumberAddition": "" }';
        $request_params = json_decode($request, true);
        $address = new PostalAddress($request_params);
        $this->assertSame('1507TN', $address->postal_code);
        $this->assertFalse($address->has('City'));
        $this->assertNull($address->locality);
        $this->assertSame('6', $address->house_number);
        $this->assertFalse($address->has('HouseNumberAddition'));
        $this->assertNull($address->house_number_addition);

        // JSON API response
        $response = '[ { "PostalCode": "1507TN", "HouseNumber": "6", "HouseNumberAddition": null, "Street": "Wals", "City": "ZAANDAM", "FormattedAddress": [ "Wals 6", "1507TN ZAANDAM" ], "Coordinates": { "Latitude": 52.439944, "Longitude": 4.806337, "RdCoordinateX": 115503, "RdCoordinateY": 494843, "VierkantNetCodeX": 1155, "VierkantNetCodeY": 4948 } } ]';
        $response_params = json_decode($response, true);

        $address = new PostalAddress($response_params);

        $this->assertSame('1507TN', $address->postal_code);

        $this->assertSame('6', $address->house_number);

        $this->assertFalse($address->has('HouseNumberAddition'));
        $this->assertNull($address->house_number_addition);

        $this->assertTrue($address->has('Street'));
        $this->assertSame('Wals', $address->street_name);

        $this->assertTrue($address->has('City'));
        $this->assertSame('ZAANDAM', $address->locality);

        $this->assertTrue($address->has('Coordinates'));
        $this->assertTrue($address->has('latitude'));
        $this->assertTrue($address->has('longitude'));
        $this->assertSame(52.439944, $address->latitude);
        $this->assertSame(4.806337, $address->longitude);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-benelux/documentation-rest/
     */
    #[Test]
    #[TestDox('PostNL Aflevercheck (deliverability check for the Netherlands)')]
    public function testPostnlAflevercheck(): void
    {
        $request = array(
            'PostalCode' => '1234AB',
            'HouseNumber' => '123'
        );

        $address = new PostalAddress($request);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);
        $this->assertNull($address->house_number_addition);

        // Same address with empty house number addition
        $request = array(
            'PostalCode'          => '1234AB',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => '',
        );

        $address = new PostalAddress($request);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);
        $this->assertNull($address->house_number_addition);

        // Same address with house number additions
        $request = array(
            'PostalCode'          => '1234AB',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($request);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);
        $this->assertNotNull($address->house_number_addition);
        $this->assertSame('A', $address->house_number_addition);

        $request = array(
            'PostalCode'          => '1234AB',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'a',
        );

        $address = new PostalAddress($request);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);
        $this->assertNotNull($address->house_number_addition);
        $this->assertSame('a', $address->house_number_addition);

        $request = array(
            'PostalCode'          => '1234AB',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'a2',
        );

        $address = new PostalAddress($request);
        $this->assertSame('1234AB', $address->postal_code);
        $this->assertSame('123', $address->house_number);
        $this->assertNotNull($address->house_number_addition);
        $this->assertSame('a2', $address->house_number_addition);

        // Address with city and street instead of postal code
        $request = array(
            'City'                => 'Amsterdam',
            'Street'              => 'Dam',
            'HouseNumber'         => '123',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($request);

        $this->assertFalse($address->has('postal_code'));  // StoreCore database
        $this->assertFalse($address->has('postalCode'));   // Schema.org
        $this->assertNull($address->postal_code);

        $this->assertSame('123', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);
        $this->assertSame('Dam', $address->street_name);
        $this->assertSame('Amsterdam', $address->locality);

        // JSON API request
        $request = '
            {
              "PostalCode": "1507TN",
              "City": "",
              "Street": "",
              "HouseNumber": "6",
              "HouseNumberAddition": ""
            }
        ';

        $request_params = json_decode($request, true);
        $address = new PostalAddress($request_params);
        $this->assertSame('1507TN', $address->postal_code);
        $this->assertNull($address->locality);
        $this->assertNull($address->street_name);
        $this->assertSame('6', $address->house_number);
        $this->assertNull($address->house_number_addition);

        // JSON API response
        $response = '
            [
              {
                "PostalCode": "1507TN",
                "HouseNumber": 6,
                "HouseNumberAddition": "",
                "Street": "Wals",
                "City": "ZAANDAM",
                "FormattedAddress": [
                  "Wals 6",
                  "1507TN ZAANDAM"
                ],
                "Mailboxes": 1,
                "DeliveryPoints": 1,
                "LotCode": "T"
              }
            ]
        ';

        $response_params = json_decode($response, true);
        $address = new PostalAddress($response_params);

        $this->assertSame('1507TN', $address->postal_code);
        $this->assertSame('6', $address->house_number);
        $this->assertNull($address->house_number_addition);
        $this->assertSame('Wals', $address->street_name);
        $this->assertSame('ZAANDAM', $address->locality);

        $this->assertTrue($address->has('DeliveryPoints'));
        $this->assertSame(1, $address->get('DeliveryPoints'));
        $this->assertSame(1, $address->DeliveryPoints);

        $this->assertTrue($address->has('LotCode'));
        $this->assertSame('T', $address->get('LotCode'));
        $this->assertSame('T', $address->LotCode);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-benelux/documentation-rest/
     */
    #[Test]
    #[TestDox('PostNL Adrescheck Benelux (address check for Belgium, Netherlands and Luxembourg)')]
    public function PostnlAdrescheckBenelux(): void
    {
        $request = array(
            'CountryIso' => 'BE',
            'City' => 'Antwerpen',
            'PostalCode' => '2000',
            'Street' => 'Oude Koornmarkt',
            'HouseNumber' => '39',
            'HouseNumberAddition' => 'A',
        );

        $address = new PostalAddress($request);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame(56, $address->country_id);
        $this->assertSame('Antwerpen', $address->locality);
        $this->assertSame('2000', $address->postal_code);
        $this->assertSame('Oude Koornmarkt', $address->street_name);
        $this->assertSame('39', $address->house_number);
        $this->assertSame('A', $address->house_number_addition);

        // Same address with ISO country code `BEL` instead of `BE`.
        $request = '
            {
              "City": "Antwerpen",
              "CountryIso": "BEL",
              "PostalCode": "2000",
              "Street": "Oude Koornmarkt",
              "HouseNumber": "39",
              "HouseNumberAddition": ""
            }
        ';

        $request_params = json_decode($request, true);
        $address = new PostalAddress($request_params);
        $this->assertInstanceOf(PostalAddress::class, $address);
        $this->assertSame('Antwerpen', $address->locality);
        $this->assertSame(56, $address->country_id);
        $this->assertSame('2000', $address->postal_code);
        $this->assertSame('Oude Koornmarkt', $address->street_name);
        $this->assertSame('39', $address->house_number);

        $this->assertFalse($address->has('HouseNumberAddition'));
        $this->assertNull($address->house_number_addition);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/addresses/adrescheck-internationaal/documentation-rest/
     *
     * @todo
     *   The PostNL API input parameters `Building` and `SubBuilding` are
     *   stored only but are currently not actively used.
     */
    #[Test]
    #[TestDox('PostNL Adrescheck Internationaal (international address check) API input')]
    public function PostnlAdrescheckInternationaalApiInput(): void
    {
        $api_input = array(
            'Country'     => 'BEL',
            'City'        => 'Antwerpen',
            'PostalCode'  => '2000',
            'Street'      => 'Oude Koornmarkt',
            'HouseNumber' => '39',
            'Building'    => 'Unit 28D',
            'SubBuilding' => '3 high',
        );

        $address = new PostalAddress($api_input);
        $this->assertInstanceOf(PostalAddress::class, $address);

        $this->assertSame(56, $address->country_id);
        $this->assertNull($address->country_subdivision);
        $this->assertSame('2000', $address->postal_code);
        $this->assertSame('39', $address->house_number);
        $this->assertNull($address->house_number_addition);

        $this->assertTrue($address->has('Building'));
        $this->assertSame('Unit 28D', $address->Building);

        $this->assertTrue($address->has('SubBuilding'));
        $this->assertSame('3 high', $address->SubBuilding);
    }

    /**
     * @see https://developer.postnl.nl/browse-apis/customer-overview/adres-in-beeld/documentation-rest/
     */
    #[Test]
    #[TestDox('PostNL Adres in beeld (address insights) API input')]
    public function PostnlAdresInBeeldApiInput(): void
    {
        $request_address = new PostalAddress(
            array(
                'postal_code' => '1507TN',
                'house_number' => '6',
                'house_number_addition' => null,
            )
        );

        /*
         * A PostNL “Adres in beeld” (address insights) API request is a partial
         * address plus a `Selection` specification for returned data fields.
         * Requesting fewer fields can be cheaper.
         *
         * ```json
         * {
         *   "PostalCode": "1507TN",
         *   "HouseNumber": "6",
         *   "HouseNumberAddition": "",
         *   "Selection": ["All"]
         * }
         * ```
         */

        $this->assertTrue($request_address->has('PostalCode'));
        $this->assertIsString($request_address->get('PostalCode'));
        $this->assertSame('1507TN', $request_address->get('PostalCode'));
        $this->assertSame('1507TN', $request_address->PostalCode);

        $this->assertTrue($request_address->has('HouseNumber'));
        $this->assertIsString($request_address->get('HouseNumber'));
        $this->assertSame('6', $request_address->get('HouseNumber'));
        $this->assertSame('6', $request_address->HouseNumber);

        $this->assertFalse($request_address->has('HouseNumberAddition'));
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $this->assertEmpty($request_address->get('HouseNumberAddition'));
    }
}
