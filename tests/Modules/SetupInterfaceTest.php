<?php

declare(strict_types=1);

namespace StoreCore\Modules;

use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
#[Group('distro')]
final class SetupInterfaceTest extends TestCase
{
    #[TestDox('SetupInterface interface file exists')]
    public function testSetupInterfaceInterfaceFileExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'SetupInterface.php');
    }

    #[Depends('testSetupInterfaceInterfaceFileExists')]
    #[TestDox('SetupInterface interface file is readable')]
    public function testSetupInterfaceInterfaceFileIsReadable(): void
    {
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'SetupInterface.php');
    }

    #[Depends('testSetupInterfaceInterfaceFileExists')]
    #[Depends('testSetupInterfaceInterfaceFileIsReadable')]
    #[TestDox('SetupInterface interface exists')]
    public function testSetupInterfaceInterfaceExists(): void
    {
        $this->assertTrue(interface_exists('\\StoreCore\\Modules\\SetupInterface'));
        $this->assertTrue(interface_exists(SetupInterface::class));
    }
}
