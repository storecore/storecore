<?php

declare(strict_types=1);

namespace StoreCore\Google\People;

use \ReflectionClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(\StoreCore\Google\People\Name::class)]
final class NameTest extends TestCase
{
    #[Group('distro')]
    #[TestDox('Name class exists')]
    public function testNameClassExists(): void
    {
        $this->assertFileExists(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'Google' . DIRECTORY_SEPARATOR . 'People' . DIRECTORY_SEPARATOR . 'Name.php');
        $this->assertFileIsReadable(STORECORE_FILESYSTEM_SRC_DIR . 'Modules' . DIRECTORY_SEPARATOR . 'Google' . DIRECTORY_SEPARATOR . 'People' . DIRECTORY_SEPARATOR . 'Name.php');

        $this->assertTrue(class_exists('\\StoreCore\\Google\\People\\Name'));
        $this->assertTrue(class_exists(\StoreCore\Google\People\Name::class));
    }

    #[TestDox('Name class is concrete')]
    public function testNameClassIsConcrete(): void
    {
        $class = new ReflectionClass(Name::class);
        $this->assertFalse($class->isAbstract());
        $this->assertFalse($class->isFinal());
        $this->assertTrue($class->isInstantiable());
    }


    #[Group('distro')]
    #[TestDox('VERSION constant is defined')]
    public function testVersionConstantIsDefined(): void
    {
        $class = new ReflectionClass(Name::class);
        $this->assertTrue($class->hasConstant('VERSION'));
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION constant is non-empty string')]
    public function testVersionConstantIsNonEmptyString(): void
    {
        $this->assertNotEmpty(Name::VERSION);
        $this->assertIsString(Name::VERSION);
    }

    #[Depends('testVersionConstantIsDefined')]
    #[Group('distro')]
    #[TestDox('VERSION matches master branch')]
    public function testVersionMatchesMasterBranch(): void
    {
        $this->assertTrue(
            version_compare(Name::VERSION, '0.1.0', '>=')
        );
    }
}
