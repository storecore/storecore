# Unit Testing Cheat Sheet

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”,  “MAY”, and “OPTIONAL” in this document are to be
interpreted as described in [RFC 2119].

[RFC 2119]: https://www.ietf.org/rfc/rfc2119.txt "RFC 2119: Key words for use in RFCs to Indicate Requirement Levels"


## File integrity tests

```php
/**
 * @group distro
 * @testdox Bar class file exists
 */
public function testBarClassFileExists()
{
    $this->assertFileExists(
        STORECORE_FILESYSTEM_SRC_DIR . 'Foo' . DIRECTORY_SEPARATOR . 'Bar.php'
    );
}

/**
 * @depends testBarClassFileExists
 * @group distro
 * @testdox Bar class file is readable
 */
public function testBarClassFileIsReadable()
{
    $this->assertFileIsReadable(
        STORECORE_FILESYSTEM_SRC_DIR . 'Foo' . DIRECTORY_SEPARATOR . 'Bar.php'
    );
}

/**
 * @depends testBarClassFileIsReadable
 * @group distro
 * @testdox Bar class exists
 */
public function testBarClassExists()
{
    $this->assertTrue(class_exists('\\Foo\\Bar'));
    $this->assertTrue(class_exists(Bar::class));
}
```


## Semantic versioning

Each PHP class MUST have a public `VERSION` constant.  This class constant
SHOULD contain a non-empty string identifier that implements the Semantic
Versioning (SemVer) standard.

```php
/**
 * @group distro
 * @testdox VERSION constant is defined
 */
public function testVersionConstantIsDefined()
{
    $class = new \ReflectionClass(Foo::class);
    $this->assertTrue($class->hasConstant('VERSION'));
}

/**
 * @depends testVersionConstantIsDefined
 * @group distro
 * @testdox VERSION constant is non-empty string
 */
public function testVersionConstantIsNonEmptyString()
{
    $this->assertNotEmpty(Foo::VERSION);
    $this->assertIsString(Foo::VERSION);
}

/**
 * @depends testVersionConstantIsNonEmptyString
 * @group distro
 * @testdox VERSION matches master branch
 */
public function testVersionMatchesMasterBranch()
{
    $this->assertGreaterThanOrEqual('1.0.0', Foo::VERSION);
}
```


Copyright © 2023 StoreCore™. All rights reserved.

Except as otherwise noted, the content of this document is licensed under the
[Creative Commons Attribution 4.0 License] (CC BY 4.0), and code samples are
licensed under the [GNU General Public License version 3.0] (GPLv3).

[Creative Commons Attribution 4.0 License]: https://creativecommons.org/licenses/by/4.0/

[GNU General Public License version 3.0]: https://www.gnu.org/licenses/gpl-3.0.html
