# Actions, Events, and Commands

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”,  “MAY”, and “OPTIONAL” in this document are to be
interpreted as described in [RFC 2119].

[RFC 2119]: https://www.ietf.org/rfc/rfc2119.txt "RFC 2119: Key words for use in RFCs to Indicate Requirement Levels"


## Actions class hierarchy

PHP classes for actions in the `StoreCore\Actions` namespace mimic the
[Schema.org class hierachy].

[Schema.org class hierachy]: https://schema.org/docs/full.html "Full Schema.org hierarchy"


Copyright © 2023 StoreCore™. All rights reserved.

Except as otherwise noted, the content of this document is licensed under the
[Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/),
and code samples are licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl.html).
