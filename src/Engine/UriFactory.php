<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \Exception;
use \InvalidArgumentException;
use \RuntimeException;
use Psr\Http\Message\{UriFactoryInterface, UriInterface};

use function \empty;
use function \is_string;
use function \isset;
use function \parse_url;
use function \strtolower;
use function \trim;

/**
 * URI factory.
 *
 * Factory for HTTP and HTTPS URIs that implements the PSR-17 `UriFactoryInterface`
 * and creates objects that implement the PSR-7 `UriInterface`.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://www.php-fig.org/psr/psr-17/ PSR-17: HTTP Factories
 */
class UriFactory implements UriFactoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Creates a new URI.
     *
     * Creates PSR-7 UriInterface objects from URL strings by parsing the format:
     * 
     *     scheme :// authority / path ? query # fragment
     * 
     * where authority is:
     *
     *     scheme :// [user-info@]host[:port] / path ? query # fragment
     *
     * @param string $uri
     *   The URI to parse.
     *
     * @return \Psr\Http\Message\UriInterface
     *   Instance of a PSR-7 Uniform Resource Identifier (URI).
     *
     * @throws \InvalidArgumentException
     *   If the given URI cannot be parsed.
     */
    public function createUri(string $uri = ''): UriInterface
    {
        if (!is_string($uri)) throw new InvalidArgumentException();

        $uri = trim($uri);
        if (empty($uri)) throw new InvalidArgumentException();

        $uri = parse_url($uri);
        if ($uri === false) throw new InvalidArgumentException();

        $result = new UniformResourceIdentifier();

        if (isset($uri['scheme'])) $result->setScheme($uri['scheme']);

        if (isset($uri['user'])) {
            if (isset($uri['pass'])) {
                $result->setUserInfo($uri['user'], $uri['pass']);
            } else {
                $result->setUserInfo($uri['user']);
            }
        }

        if (isset($uri['host'])) {
            $result->setHost($uri['host']);
        } elseif (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) {
            $result->setHost(strtolower($_SERVER['HTTP_HOST']));
        }

        if (isset($uri['path'])) $result->setPath($uri['path']);

        if (isset($uri['query'])) $result->setQuery($uri['query']);

        if (isset($uri['fragment'])) $result->setFragment($uri['fragment']);

        return $result;
    }

    /**
     * Creates a URI value object for the current request.
     *
     * @param void
     *
     * @return \Psr\Http\Message\UriInterface
     *   Instance of a PSR-7 Uniform Resource Identifier (URI).
     *
     * @throws \RuntimeException
     *   This method creates a `UniformResourceIdentifier` object with a 
     *   `UriInterface` by parsing PHP `$_SERVER` parameters and therefore
     *   throws a runtime exception if `$_SERVER` cannot be used to create
     *   a URL.
     *
     * @uses \StoreCore\Engine\ServerContainer
     */
    public static function getCurrentUri(): UriInterface
    {
        $uri = new UniformResourceIdentifier();

        $server = new ServerContainer();
        if (!$server->has('HTTP_HOST')) return $uri;
        if (!$server->has('REQUEST_URI')) return $uri;

        try {
            $factory = new UriFactory();
            $uri = $factory->createUri('//' . $server->get('HTTP_HOST') . $server->get('REQUEST_URI'));
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage(), (int) $e->getCode(), $e);
        }

        if (
            ($server->has('HTTPS') && $server->get('HTTPS') !== 'off')
            || ($server->has('SERVER_PORT') && $server->get('SERVER_PORT') == 443)
        ) {
            $uri->setScheme('https');
        } else {
            $uri->setScheme('http');
        }

        if ($server->has('SERVER_PORT')) {
            $uri->setPort((int) $server->get('SERVER_PORT'));
        }

        return $uri;
    }
}
