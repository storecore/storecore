<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{ResponseFactoryInterface, ResponseInterface, UriInterface};
use StoreCore\Registry;

/**
 * HTTP response factory.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2020, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://en.wikipedia.org/wiki/List_of_HTTP_status_codes List of HTTP status codes
 * @version   1.0.0
 */
class ResponseFactory implements ResponseFactoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * {@inheritDoc}
     * 
     * @uses StoreCore\Engine\ResponseCodes
     */
    public function createResponse(int $code = 200, string $reason_phrase = ''): ResponseInterface
    {
        if (!isset(ResponseCodes::RESPONSE_STATUS_CODES[$code])) throw new \ValueError();

        $response = new Response(Registry::getInstance());
        $response->setStatusCode($code);

        if (empty($reason_phrase)) $reason_phrase = ResponseCodes::getReasonPhrase($code);
        $response->setReasonPhrase($reason_phrase);

        return $response;
    }

    /**
     * Creates a client redirect response,
     *
     * @param Psr\Http\Message\UriInterface|string $url
     *   URL of the destination location; sets the HTTP `Location` header.
     *   This parameter MUST be a string or an `UriInterface` object that can
     *   be converted to an URL string.
     * 
     * @param int $code
     *   HTTP response status code.  Defaults to `303` for a `See Other`
     *   response to quickly redirect after a `PUT` or a `POST` request.
     *   It is recommended to use the `301 Moved Permanently` code only
     *   as a response for `GET` or `HEAD` methods and to use the `308
     *   Permanent Redirect` for `POST` methods instead, as a method
     *   change is explicitly prohibited with this status.  Permanent
     *   redirects with code `301` cached `public` for 1 year
     *   (the HTTP maximum).
     *
     * @throws \OutOfRangeException
     *   Throws an SPL out of range logic exception on an invalid HTTP status
     *   `$code` for redirection messages.
     */
    public function createRedirect(UriInterface|string $url, int $code = 303): ResponseInterface
    {
        if ($code < 300 || $code > 399) {
            throw new \OutOfRangeException();
        }

        $response = $this->createResponse($code);
        $response->setHeader('Location', (string) $url);
        if ($code === 301) $response->setHeader('Cache-Control', 'public, max-age=31536000, immutable');

        return $response;
    }
}
