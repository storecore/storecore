<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2019, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \DateTimeImmutable;
use \Exception;

use Psr\Clock\ClockInterface;
use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;

use StoreCore\ClockTrait;
use StoreCore\Registry;
use StoreCore\Database\RedirectCache;
use StoreCore\Database\NotFoundException;
use StoreCore\Types\CacheKey;

use function \defined;

/**
 * HTTP redirection.
 *
 * The `StoreCore\Engine\RedirectRequestHandler` class allows for soft permanent
 * redirects.  The `RedirectRequestHandler::handle()` method looks for a redirect
 * candidate and returns a permanent redirect if a redirect destination is found.
 *
 * Redirects are ignored if the global constant `STORECORE_CMS_REDIRECTS`
 * is disabled (set to `false`) or the current redirect count is `(int) 0`.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class RedirectRequestHandler extends AbstractServerRequestHandler implements
    ClockInterface,
    RequestHandlerInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Creates a HTTP response for redirection.
     *
     * @internal
     * @param StoreCore\Engine\Redirect $redirect
     * @return Psr\Http\Message\ResponseInterface
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Redirections Redirections in HTTP
     */
    public function createRedirectResponse(Redirect $redirect): ResponseInterface
    {
        if ($redirect->expiresAt === null) {
            $response = $this->responseFactory->createResponse(301);
            $response->setHeader('Cache-Control', 'public, max-age=31536000, immutable');
        } else {
            $response = $this->responseFactory->createResponse(307);
            $response->setHeader('Expires', $redirect->expiresAt->format('D, d M Y H:i:s \G\M\T'));
        }

        if ($redirect->dateCreated !== null) {
            $response->setHeader('Last-Modified', $redirect->dateCreated->format('D, d M Y H:i:s \G\M\T'));
        }

        $response->setHeader('Location', $redirect->getLocation());
        return $response;
    }

    /**
     * Finds a redirection response.
     *
     * The `process()` method looks for a cacheable redirected URL, like
     * `//www.example.com/foo/bar-baz` without a protocol prefix.
     *
     * @param Psr\Http\Message\RequestInterface $request
     *
     * @return Psr\Http\Message\ResponseInterface|null
     *   Returns an HTTP `3xx` redirect response if a match is found in 
     *   the redirects cache.  A `null` is returned on a cache miss.
     */
    protected function process(ServerRequestInterface $request): ?ResponseInterface
    {
        if (
            defined('STORECORE_CMS_REDIRECTS')
            && (STORECORE_CMS_REDIRECTS === false || STORECORE_CMS_REDIRECTS === 0)
        ) {
            return null;
        }

        try {
            $registry = Registry::getInstance();
            $cache = new RedirectCache($registry);
            if ($cache->count() === 0) {
                return null;
            }

            $key = (string) new CacheKey($request->getUri());
            if ($cache->has($key) !== true) {
                return null;
            }

            $redirect = $cache->get($key);
            $çache->hit($redirect, $this->now());
            return $this->createRedirectResponse($redirect);
        } catch (NotFoundException $e) {
            return null;
        } catch (Exception $e) {
            $response = $this->responseFactory->createResponse(502);
            $response->setHeader('Cache-Control', 'no-cache, no-store');
            return $response;
        }
    }
}
