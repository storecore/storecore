<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\FileSystem\FullPageCache;
use StoreCore\Types\CacheKey;

/**
 * Full-page cache request handler.
 *
 * @package StoreCore\Catalog
 * @version 1.0.0-alpha.1
 */
class FullPageCacheRequestHandler extends AbstractServerRequestHandler implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';


    /**
     * Matches a page request with a cache response.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     *   HTTP request message.
     *
     * @return Psr\Http\Message\ResponseInterface|null
     *   Returns a `Response` on a cache hit and `null` or a “100 Continue”
     *   response on a cache miss.
     */
    protected function process(ServerRequestInterface $request): ?ResponseInterface
    {
        // StoreCore web page URLs do not have extensions.
        $extension = pathinfo($request->getRequestTarget(), \PATHINFO_EXTENSION);
        if (!empty($extension)) {
            return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
        }

        // StoreCore does not support a trailing slash in URLs.
        // The only exception is / for the domain root and homepage.
        if (
            $request->getRequestTarget() !== '/'
            && str_ends_with($request->getRequestTarget(), '/')  
        ) {
            return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
        }

        // Create a cache key and check if it exists in the cache.
        try {
            $cache = new FullPageCache();
            $key = new CacheKey($request->getUri());
            $key = (string) $key;

            if ($cache->has($key)) {
                $cache_file_contents = $cache->get($key, false);
                if ($cache_file_contents === false) return $this->responseFactory->createResponse(503);
            } else {
                return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
            }

            $last_modified = $cache->getFileModificationTime($key);
            if ($last_modified === false) {
                return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
            }
        } catch (CacheException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new CacheException($e->getMessage(), $e->getCode(), $e);
        }

        // Only allow `GET` and `HEAD` methods.
        if ($request->getMethod() !== 'GET' && $request->getMethod() !== 'HEAD') {
            if ($this->successor !== null) return null;
            $response = $this->responseFactory->createResponse(405);
            $response->setHeader('Allow', 'GET, HEAD');
            return $response;
        }

        // Cache public for 48 hours (172800 seconds) and stale for 14 days (1209600 seconds)
        $response = $this->responseFactory->createResponse();
        $response->setHeader('Cache-Control', 'public, max-age=172800, stale-while-revalidate=1036800');
        $response->setHeader('Pragma', 'cache');
        $response->setHeader('Allow', 'GET, HEAD');
        $response->setHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->setHeader('Last-Modified', gmdate('D, d M Y H:i:s T', $last_modified));

        $etag = md5($cache_file_contents, false);
        $etag = base64_encode($etag);
        $etag = rtrim($etag, '=');
        $response->setHeader('ETag', '"' . $etag . '"');

        if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
            $http_if_none_match = filter_input(INPUT_SERVER, 'HTTP_IF_NONE_MATCH', FILTER_UNSAFE_RAW);
            $http_if_none_match = trim($http_if_none_match, '"');
        } else {
            $http_if_none_match = false;
        }

        if (
            isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])
            && ($http_if_none_match === $etag || strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) === $last_modified)
        ) {
            unset($cache_file_contents);
            $response->setStatusCode(304);
            $response->setReasonPhrase('Not Modified');
        } else {
            $stream_factory = new StreamFactory();
            $cache_file_contents = $stream_factory->createStream($cache_file_contents);
            $response->setBody($cache_file_contents);
        }

        return $response;
    }
}
