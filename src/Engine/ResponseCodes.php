<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

/**
 * HTTP response codes.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 *
 * @see https://www.rfc-editor.org/rfc/rfc7231
 *      RFC 7231: Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
 *      HTTP response status codes
 * 
 * @see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 *      List of HTTP status codes
 */
final readonly class ResponseCodes
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var array RESPONSE_STATUS_CODES
     *   HTTP response status codes.
     */
    public const RESPONSE_STATUS_CODES = [
        100 => 'Continue',
        101 => 'Switching Protocol',
        102 => 'Processing',

        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        226 => 'IM Used',

        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',

        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',

        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
    ];

    /**
     * Gets the HTTP reason phrase for a given HTTP status code number.
     *
     * @param int|string $status_code HTTP response status code.
     * @return string HTTP reason phrase.
     * @throws \ValueError Invalid HTTP status code.
     */
    final public static function getReasonPhrase(int|string $status_code): string
    {
        if (\is_string($status_code) && ctype_digit($status_code)) {
            $status_code = (int) $status_code;
        }

        if (isset(self::RESPONSE_STATUS_CODES[$status_code])) {
            return self::RESPONSE_STATUS_CODES[$status_code];
        } else {
            throw new \ValueError('Illegal HTTP response status code: ' . $status_code);
        }
    }
}
