<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\{ResponseFactoryInterface, ResponseInterface, ServerRequestInterface};

interface_exists(\Psr\Http\Server\RequestHandlerInterface::class);
interface_exists(\Psr\Http\Message\ResponseFactoryInterface::class);
interface_exists(\Psr\Http\Message\ResponseInterface::class);
class_exists(\StoreCore\Engine\ResponseFactory::class);

/**
 * Abstract server request handler.
 *
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern Chain-of-responsibility pattern
 * @see     https://www.php-fig.org/psr/psr-15/ PSR-15: HTTP Server Request Handlers
 * @version 1.0.0-alpha.1
 */
abstract class AbstractServerRequestHandler implements RequestHandlerInterface
{
    /**
     * @var Psr\Http\Message\ResponseFactoryInterface $responseFactory
     *   Factory that implements the `Psr\Http\Message\ResponseFactoryInterface`
     *   to create HTTP responses that implement the `Psr\Http\Message\ResponseInterface`.
     *   Set to the default `StoreCore\Engine\ResponseFactory` in the constructor.
     */
    protected readonly ResponseFactoryInterface $responseFactory;

    /**
     * Creates a server request handler.
     *
     * @param Psr\Http\Server\RequestHandlerInterface|null $successor
     *   OPTIONAL successor in a Chain of Responsibility.
     */
    public function __construct(protected readonly ?RequestHandlerInterface $successor = null)
    {
        $this->responseFactory = new ResponseFactory();
    }

    /**
     * Handles a request and produces a response.
     *
     * MAY call other collaborating code through the `$successor` to generate
     * the response in a Chain of Responsibility.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     * @return Psr\Http\Message\ResponseInterface
     */
    final public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->process($request);
        if ($response === null && $this->successor !== null) {
            $response = $this->successor->handle($request);
        }

        if ($response === null) {
            $response = $this->responseFactory->createResponse(100);
        }
        return $response;
    }

    /**
     * Processes the request.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     * 
     * @return Psr\Http\Message\ResponseInterface|null
     *   Returns a response if the request can be handled or `null` if
     *   a response cannot be created and the request MUST therefore be passed
     *   on to the next request handler in the Chain of Responsibility.
     */
    abstract protected function process(ServerRequestInterface $request): ?ResponseInterface;
}
