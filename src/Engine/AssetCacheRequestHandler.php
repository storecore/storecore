<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\Database\MediaLibrary;
use StoreCore\Registry;
use StoreCore\Types\CacheKey;

use function \array_key_exists;
use function \ctype_xdigit;
use function \empty;
use function \gmdate;
use function \is_dir;
use function \is_file;
use function \is_readable;
use function \is_string;
use function \isset;
use function \md5_file;
use function \pathinfo;
use function \realpath;
use function \rtrim;
use function \stripslashes;
use function \strtolower;
use function \strtotime;
use function \trim;

/**
 * Asset cache request handler.
 * 
 * The asset cache request handler tries to match an HTTP request with
 * a cacheable response.  It silently creates a cacheable response on a cache
 * hit, or returns `null` or a “404 Not Found” response on a cache miss.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class AssetCacheRequestHandler extends AbstractServerRequestHandler implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array CONTENT_TYPES
     *   Cacheable asset MIME types and subdirectories of `/public/`.
     *   The array key is a directory name, the value is a MIME type.
     *   In subclasses this set of MIME types MAY be limited.
     */
    public const CONTENT_TYPES = [
        'apng'  => 'image/apng',
        'avif'  => 'image/avif',
        'css'   => 'text/css; charset=utf-8',
        'eot'   => 'application/vnd.ms-fontobject',
        'gif'   => 'image/gif',
        'ico'   => 'image/x-icon',
        'jpeg'  => 'image/jpeg',
        'js'    => 'text/javascript; charset=utf-8',
        'mjs'   => 'text/javascript; charset=utf-8',
        'otf'   => 'application/font-sfnt',
        'pdf'   => 'application/pdf',
        'png'   => 'image/png',
        'svg'   => 'image/svg+xml',
        'ttc'   => 'application/font-sfnt',
        'ttf'   => 'application/font-sfnt',
        'webp'  => 'image/webp',
        'woff'  => 'application/font-woff',
        'woff2' => 'font/woff2',
    ];

    /**
     * Handles a request and produces a response.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     *   HTTP request message.
     *
     * @return Psr\Http\Message\ResponseInterface|null
     *   Returns a cacheable `Response` on a cache hit and a `100 Continue`
     *   or `null` on a cache miss.
     *
     * @uses Psr\Http\Message\ServerRequestInterface::getMethod()
     *   Only allows HTTP `GET` and `HEAD` methods for cache requests.
     *
     * @uses StoreCore\Types\CacheKey
     */
    protected function process(ServerRequestInterface $request): ?ResponseInterface
    {
        // Assets have filenames with extensions.
        $pathinfo = pathinfo($request->getRequestTarget());
        if (empty($pathinfo['filename']) || empty($pathinfo['extension'])) {
            return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
        }

        // Extension must be one of the supported file types.
        $pathinfo['extension'] = strtolower($pathinfo['extension']);
        if ($pathinfo['extension'] === 'jpg' || $pathinfo['extension'] === 'jpe') {
            $pathinfo['extension'] = 'jpeg';
        }
        if (!array_key_exists($pathinfo['extension'], self::CONTENT_TYPES)) {
            return ($this->successor !== null) ? null : $this->responseFactory->createResponse(100);
        }

        // Only allow `GET` and `HEAD` requests for assets.
        if ($request->getMethod() !== 'GET' && $request->getMethod() !== 'HEAD') {
            $response = $this->responseFactory->createResponse(405);
            $response->setHeader('Allow', 'GET, HEAD');
            return $response;
        }

        // Find matching assets directory.
        if (defined('STORECORE_FILESYSTEM_ASSETS_DIR') && !empty(STORECORE_FILESYSTEM_ASSETS_DIR)) {
            $cache_directory = realpath(rtrim(STORECORE_FILESYSTEM_ASSETS_DIR, '/\\') . DIRECTORY_SEPARATOR . $pathinfo['extension']);
        } else {
            $cache_directory = realpath(__DIR__ . '/../../public/' . $pathinfo['extension']);
        }

        // Return `502 Bad Gateway` on inaccessible cache.
        if ($cache_directory === false || !is_dir($cache_directory) || !is_readable($cache_directory)) {
            $response = $this->responseFactory->createResponse(502);
            $response->setHeader('Allow', 'GET, HEAD');
            return $response;
        }

        /*
         * Find asset file in one of three ways:
         *
         * - by the actual asset file name;
         * - by the SHA-1 hash of the asset SEO URL;
         * - by the asset file number in the database.
         */
        $filename = $cache_directory . DIRECTORY_SEPARATOR . $pathinfo['basename'];

        if (!is_file($filename)) {
            // First fallback: find hashed SEO URL for frequently used files.
            $seo_url_hash = new CacheKey('//' . $request->getUri()->getHost() . $request->getUri()->getPath());
            $filename = $cache_directory . DIRECTORY_SEPARATOR . $seo_url_hash . '.' . $pathinfo['extension'];

            // Second fallback: find file number in the media library database.
            if (!is_file($filename)) {
                $registry = Registry::getInstance();
                $media_library = new MediaLibrary($registry);
                $file_number = $media_library->getFileNumber($request->getUri());
                if ($file_number === null) {
                    $response = $this->responseFactory->createResponse(404);
                    $response->setHeader('Allow', 'GET, HEAD');
                    return $response;
                }

                $filename = $cache_directory . DIRECTORY_SEPARATOR . $file_number . '.' . $pathinfo['extension'];
                if (!is_file($filename)) {
                    $response = $this->responseFactory->createResponse(404);
                    $response->setHeader('Allow', 'GET, HEAD');
                    return $response;
                }
            }
        }

        // Return `502 Bad Gateway` if the cache file does exist but is not readable.
        if (!is_readable($filename)) {
            $response = $this->responseFactory->createResponse(502);
            $response->setHeader('Allow', 'GET, HEAD');
            return $response;
        }

        $response = $this->responseFactory->createResponse();
        $response->setHeader('Allow', 'GET, HEAD');

        // Cache immutable for 365 days = 31536000 seconds
        $response->setHeader('Cache-Control', 'public, max-age=31536000, immutable');
        $response->setHeader('Pragma', 'cache');
        $response->setHeader('Content-Type', self::CONTENT_TYPES[$pathinfo['extension']]);

        $last_modified = filemtime($filename);
        $response->setHeader('Last-Modified', gmdate('D, d M Y H:i:s T', $last_modified));

        // E-tag as a lowercase hexadecimal MD5 hash of the file contents.
        $etag = md5_file($filename, false);
        $response->setHeader('ETag', '"' . $etag . '"');

        $server = $request->getServerParams();
        if (isset($server['HTTP_IF_NONE_MATCH']) && is_string($server['HTTP_IF_NONE_MATCH'])) {
            $http_if_none_match = stripslashes($server['HTTP_IF_NONE_MATCH']);
            $http_if_none_match = trim($http_if_none_match);
            $http_if_none_match = trim($http_if_none_match, '"');
            $http_if_none_match = strtolower($http_if_none_match);
            if (!ctype_xdigit($http_if_none_match)) $http_if_none_match = false;
        } else {
            $http_if_none_match = false;
        }

        if (
            isset($server['HTTP_IF_MODIFIED_SINCE'])
            && ($http_if_none_match === $etag || strtotime($server['HTTP_IF_MODIFIED_SINCE']) === $last_modified)
        ) {
            $response->setStatusCode(304);
            $response->setReasonPhrase('Not Modified');
            return $response;
        }

        $stream_factory = new StreamFactory();
        $stream = $stream_factory->createStreamFromFile($filename);
        $response->setBody($stream);
        return $response;
    }
}
