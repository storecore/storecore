<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Network exception.
 *
 * Thrown when the request cannot be completed because of network issues.
 * Although this `NetworkException` is very similar to the `RequestException`,
 * a network exception always is a runtime exception and therefore extends the
 * Standard PHP Library (SPL) `RuntimeException`.
 *
 * @author     Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright  Copyright © 2022 StoreCore™
 * @implements Psr\Http\Client\NetworkExceptionInterface
 * @license    https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package    StoreCore\Core
 * @see        https://www.php-fig.org/psr/psr-18/#networkexceptioninterface
 * @see        https://www.php.net/manual/en/class.runtimeexception.php
 * @version    1.0.0
 */
class NetworkException extends \RuntimeException implements NetworkExceptionInterface
{
    /**
     * Creates a network exception.
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     * @param Psr\Http\Message\RequestInterface|null $request
     */
    public function __construct(
        $message = '',
        $code = 0,
        \Throwable $previous = null,
        protected ?RequestInterface $request = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param void
     * @return Psr\Http\Message\RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }
}
