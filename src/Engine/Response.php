<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{MessageInterface, ResponseInterface};
use StoreCore\Registry;

interface_exists(MessageInterface::class);
interface_exists(ResponseInterface::class);
class_exists(Message::class);

/**
 * HTTP response.
 *
 * @api
 * @package StoreCore\Core
 * @version 0.4.0
 */
class Response extends Message implements MessageInterface, ResponseInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.0';

    /**
     * @var string $reasonPhrase
     *   Response reason phrase associated with the status code.
     *   Defaults to 'OK' for a '200 OK' response.
     */
    private string $reasonPhrase = 'OK';

    /**
     * @var null|string $responseBody
     *   OPTIONAL contents body of the HTTP response.
     */
    protected ?string $responseBody = null;

    /**
     * @var int $statusCode
     *   HTTP response status code.  Defaults to 200.
     */
    private int $statusCode = 200;

    #[\Override]
    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }

    #[\Override]
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Outputs the full response.
     *
     * @param void
     * @return void
     */
    public function output(): void
    {
        if (headers_sent()) throw new \RuntimeException('Headers already sent');

        ob_start('ob_gzhandler');

        if ($this->getStatusCode() !== 200) {
            header($this->getReasonPhrase(), true, $this->getStatusCode());
        }

        if (!empty($this->getHeaders())) {
            foreach ($this->getHeaders() as $name => $values) {
                foreach ($values as $value) {
                    header(sprintf('%s: %s', $name, $value), false);
                }
            }
        }

        header('Content-Security-Policy: upgrade-insecure-requests', true);
        header('Referrer-Policy: same-origin', true);
        header('Strict-Transport-Security: max-age=31536000; includeSubDomains', true);
        header('X-Content-Type-Options: nosniff', true);
        header('X-DNS-Prefetch-Control: on', true);
        header('X-Frame-Options: SAMEORIGIN', true);
        header('X-Powered-By: StoreCore', true);
        header('X-UA-Compatible: IE=edge', true);
        header('X-XSS-Protection: 1; mode=block', true);

        // Retry after 1 hour (3600 seconds) on a temporary `503 Service
        // Unavailable` if the required `Retry-After` header is missing.
        if ($this->getStatusCode() === 503 && !$this->hasHeader('Retry-After')) {
            header('Retry-After: 3600');
        }

        /*
         * @todo
         *   This needs more work for specific server, database, and
         *   middleware details.  For now we are simply measuring the total
         *   server processing duration in milliseconds.
         *
         * @see https://www.w3.org/TR/server-timing/
         *      Server Timing (W3C Working Draft 30 April 2020)
         *
         * @see https://secure.php.net/microtime
         *      PHP microtime() function and $_SERVER['REQUEST_TIME_FLOAT']) superglobal
         */
        header('Server-Timing: total;dur=' . round(1000 * (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']), 1));

        if ($this->body !== null && $this->body->isReadable()) {
            echo $this->getBody();
        }

        ob_end_flush();
        flush();
    }

    /**
     * Sets the reason response phrase.
     * 
     * @param string|null $reason_phrase
     *   Reason phrase associated with the response status code.
     *   The reason phrase value MAY be empty.
     *
     * @return void
     */
    public function setReasonPhrase(?string $reason_phrase): void
    {
        if (empty($reason_phrase)) $reason_phrase = '';
        if (!\is_string($reason_phrase)) throw new \InvalidArgumentException();
        $this->reasonPhrase = $reason_phrase;
    }

    /**
     * Sets the HTTP response status code.
     *
     * @param int $code
     *   HTTP response status code as an integer.
     *
     * @return void
     */
    public function setStatusCode(int $code): void
    {
        $this->statusCode = $code;
    }

    #[\Override]
    public function withStatus(int $code, string $reason_phrase = ''): ResponseInterface
    {
        $response = clone $this;
        $response->setStatusCode($code);
        $response->setReasonPhrase($reason_phrase);
        return $response;
    }
}
