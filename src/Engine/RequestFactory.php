<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{RequestFactoryInterface, RequestInterface};
use Psr\Http\Message\UriInterface;

interface_exists(\Psr\Http\Message\RequestFactoryInterface::class);
interface_exists(\Psr\Http\Message\RequestInterface::class);
interface_exists(\Psr\Http\Message\UriInterface::class);
class_exists(\StoreCore\Engine\Request::class);

/**
 * PSR-17 compliant factory for requests.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class RequestFactory implements RequestFactoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @inheritDoc
     */
    public function createRequest(string $method, $uri): RequestInterface
    {
        $request = new Request();
        try {
            $request->setMethod($method);
            $request->setUri($uri);
            return $request;
        } catch (\Exception $e) {
            throw new \InvalidArgumentException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }
}
