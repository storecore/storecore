<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{ServerRequestFactoryInterface, ServerRequestInterface};

/**
 * PSR-17 compliant factory for server requests.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class ServerRequestFactory implements ServerRequestFactoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        $request = new ServerRequest();
        $request->setMethod($method);
        $request->setUri($uri);
        $request->setServerParams($serverParams);
        return $request;
    }
}
