<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @see       https://github.com/php-fig/simple-cache/blob/master/src/InvalidArgumentException.php
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \Throwable;

use Psr\SimpleCache\CacheException as CacheExceptionInterface;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionInterface;
use StoreCore\AbstractCacheException as CacheException;

class InvalidCacheArgumentException extends CacheException implements 
    CacheExceptionInterface,
    InvalidArgumentExceptionInterface,
    Throwable {}
