<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \RuntimeException;
use Psr\Http\Message\StreamInterface;

use function \fopen;
use function \isset;

/**
 * HTTP request stream message object.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class RequestStream extends AbstractStream implements StreamInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @inheritDoc
     */
    protected bool $writable = false;

    /**
     * Creates a client HTTP request stream.
     *
     * @param void
     */
    public function __construct()
    {
        $this->streamResource = fopen('php://input', 'rb');
        if ($this->streamResource === false) {
            $this->readable = false;
        } else {
            $this->readable = true;
            if (isset($_SERVER['CONTENT_LENGTH'])) {
                $this->size = (int) $_SERVER['CONTENT_LENGTH'];
            }
        }
    }

    #[\Override]
    public function write(string $string): int
    {
        throw new RuntimeException('Request input stream is immutable');
    }
}
