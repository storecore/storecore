<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \Exception;
use \Throwable;
use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Request exception.
 *
 * Exception for when a request failed.  Examples are a request that is invalid
 * (e.g. method is missing) and runtime request errors (e.g. the body stream is
 * not seekable).
 *
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-18/ PSR-18: HTTP Client
 * @version 1.0.0
 */
class RequestException extends Exception implements RequestExceptionInterface
{
    /**
     * Creates a request exception.
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     * @param Psr\Http\Message\RequestInterface|null $request
     * @see https://www.php.net/manual/en/language.exceptions.extending.php
     */
    public function __construct(
        $message = '',
        $code = 0,
        Throwable $previous = null,
        protected ?RequestInterface $request = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param void
     * @return Psr\Http\Message\RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }
}
