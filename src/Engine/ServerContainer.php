<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use StoreCore\AbstractContainer;
use Psr\Container\ContainerInterface;

interface_exists(ContainerInterface::class);
class_exists(AbstractContainer::class);

/**
 * Container for server parameters.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class ServerContainer extends AbstractContainer implements
    ContainerInterface,
    \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * Creates a server parameter bag.
     *
     * @param array|null $server_parameters
     *   OPTIONAL array of additional server parameters to add to existing
     *   server parameters in the PHP superglobal `$_SERVER`.  The container
     *   is always populated with the `$_SERVER` array if it is not empty.
     */
    public function __construct(?array $server_parameters = null)
    {
        if (isset($_SERVER) && !empty($_SERVER)) {
            $this->data = array_filter($this->data);
            $this->data = array_change_key_case($_SERVER, \CASE_UPPER);
        }

        if (!empty($server_parameters)) {
            $this->data = array_merge($this->data, $server_parameters);
        }

        if (!isset($this->data['REQUEST_METHOD'])) {
            $this->data['REQUEST_METHOD'] = 'GET';
        }
    }


    /**
     * Returns the server parameters as an array.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->data;
    }
}
