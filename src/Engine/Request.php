<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Client request.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class Request extends Message implements MessageInterface, RequestInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $method
     *   HTTP method of the request.  Defaults to a `GET` request.
     */
    private string $method = 'GET';

    /**
     * @var null|string $requestTarget
     *   Target of the HTTP request.
     */
    private ?string $requestTarget = null;

    /**
     * @var array $supportedMethods
     *   HTTP request methods supported by the class.  This array MAY be
     *   overwritten by extending classes to limit the types of allowed request
     *   methods for specific applications.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
     *      HTTP request methods
     */
    protected array $supportedMethods = [
        'CONNECT' => true,
        'DELETE'  => true,
        'GET'     => true,
        'HEAD'    => true,
        'OPTIONS' => true,
        'PATCH'   => true,
        'POST'    => true,
        'PUT'     => true,
        'TRACE'   => true,
    ];

    /**
     * @var null|Psr\Http\Message\UriInterface $uri
     *   Uniform resource identifier (URI) with a UriInterface.
     */
    private ?UriInterface $uri = null;

    /**
     * Gets the current HTTP request method.
     *
     * @param void
     * @return string
     */
    #[\Override]
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Gets the message’s request target.
     *
     * @param void
     *
     * @return string
     *   In most cases, this method will be the origin-form of the composed URI,
     *   unless a value was provided to the concrete implementation.  If no URI
     *   is available, and no request-target has been specifically provided,
     *   this method will return the string '/'.
     */
    #[\Override]
    public function getRequestTarget(): string
    {
        if ($this->requestTarget !== null) {
            return $this->requestTarget;
        }

        try {
            $this->requestTarget = $this->getUri()->getPath();
            if (empty($this->requestTarget)) $this->requestTarget = '/';
            if (!empty($this->getUri()->getQuery())) $this->requestTarget .= '?' . $this->getUri()->getQuery();
        } catch (\RuntimeException $e) {
            $this->requestTarget = '/';
        }
        return $this->requestTarget;
    }

    /**
     * {@inheritDoc}
     *
     * @uses StoreCore\Engine\UriFactory:getCurrentUri()
     */
    #[\Override]
    public function getUri(): UriInterface
    {
        if ($this->uri === null) {
            $this->uri = UriFactory::getCurrentUri();
        }
        return $this->uri;
    }

    /**
     * Sets the HTTP request method.
     *
     * @param string $method
     *   Case-insensitive name of the HTTP method.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the `$method` parameter is not
     *   a valid HTTP method.
     *
     * @throws \OutOfBoundsException
     *   Throws an out of bounds runtime exception if the HTTP is valid but is
     *   not supported by the current application or request end point.
     * 
     * @return void
     */
    public function setMethod($method)
    {
        if (!is_string($method) || empty($method)) throw new \InvalidArgumentException('HTTP method MUST be non-empty string');
        $method = trim($method);
        $uppercase_method = strtoupper($method);
        if (!array_key_exists($uppercase_method, $this->supportedMethods)) {
            throw new \InvalidArgumentException('Invalid HTTP method: ' . $method);
        } elseif ($this->supportedMethods[$uppercase_method] !== true) {
            throw new \OutOfBoundsException('HTTP method is not supported: ' .$method);
        } else {
            $this->method = $method;
        }
    }

    /**
     * Sets the request target.
     *
     * @param string|\Stringable $request_target
     *   Target path of the current request as a string or an object that can
     *   be converted to a string.
     *
     * @return void
     */
    public function setRequestTarget($request_target)
    {
        $this->requestTarget = (string) $request_target;
    }

    /**
     * Sets the request URI.
     *
     * @param string|Psr\Http\Message\UriInterface
     *   Uniform resource identifier (URI) of the request as an URL string or
     *   a PSR-7 compliant UriInterface object.
     *
     * @return void
     * 
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the $uri parameter is not
     *   a valid URI string or URI object.
     */
    public function setUri(string|UriInterface $uri): void
    {
        if ($uri instanceof UriInterface) {
            $this->uri = $uri;
        } elseif (\is_string($uri)) {
            try {
                $factory = new UriFactory();
                $this->uri = $factory->createUri($uri);
            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            throw new \InvalidArgumentException('Argument passed to ' . __METHOD__ . ' must be UriInterface or URI string, ' . gettype($uri) . ' given');
        }
    }

    #[\Override]
    public function withMethod(string $method): RequestInterface
    {
        $request = clone $this;
        $request->setMethod($method);
        return $request;
    }

    #[\Override]
    public function withRequestTarget(string $request_target): RequestInterface
    {
        $request = clone $this;
        $request->setRequestTarget($request_target);
        return $request;
    }

    #[\Override]
    public function withUri(UriInterface $uri, bool $preserveHost = false): RequestInterface
    {
        if ($preserveHost) {
            $current_uri = $this->getUri();
            $current_host = $current_uri->getHost();

            if (!empty($current_host)) {
                $uri->setHost($current_host);
            }
        }

        $request = clone $this;
        $request->setUri($uri);
        return $request;
    }
}
