# StoreCore RESTful HTTP messaging engine

## Message

```php
interface Psr\Http\Message\MessageInterface {
    /* Methods */
    public getBody(): Psr\Http\Message\StreamInterface
    public getHeader(string $name): array
    public getHeaderLine(string $name): string
    public getHeaders(): array
    public getProtocolVersion(): string
    public hasHeader(string $name): bool
    public withAddedHeader(string $name, string|array $value): static
    public withBody(Psr\Http\Message\StreamInterface $body): static
    public withHeader(string $name, string|array $value): static
    public withoutHeader(string $name): static
    public withProtocolVersion(string $version): static
}

class StoreCore\Engine\Message
implements Psr\Http\Message\MessageInterface {
    /* Methods */
    public getBody(): Psr\Http\Message\StreamInterface
    public getHeader(string $name): array
    public getHeaderLine(string $name): string
    public getHeaders(): array
    public getProtocolVersion(): string
    public hasHeader(string $name): bool
    public setBody(Psr\Http\Message\StreamInterface $body): void
    public setHeader(string $name, string|array $value): void
    public setProtocolVersion(string|int $version): void
    private unsetHeader(string $name): void
    public withAddedHeader(string $name, string|array $value): static
    public withBody(Psr\Http\Message\StreamInterface $body): static
    public withHeader(string $name, string|array $value): static
    public withoutHeader(string $name): static
    public withProtocolVersion(string $version): static
}
```


## HTTP response

### Response factory

```php
interface Psr\Http\Message\ResponseFactoryInterface {
    public createResponse(int $code = 200, string $reasonPhrase = ''): Psr\Http\Message\ResponseInterface
}

class Psr\Http\Message\ResponseFactory 
implements Psr\Http\Message\ResponseFactoryInterface {
    public createResponse(int $code = 200, string $reason_phrase = ''): Psr\Http\Message\ResponseInterface
    public createRedirect(Psr\Http\Message\UriInterface|string $url, int $code = 303): Psr\Http\Message\ResponseInterface
}
```


### Response

```php
interface Psr\Http\Message\ResponseInterface
extends Psr\Http\Message\MessageInterface {
    /* Methods */
    public getReasonPhrase(): string
    public getStatusCode(): int
    public withStatus(int $code, string $reasonPhrase = ''): static

    /* Inherited methods */
    public getBody(): Psr\Http\Message\StreamInterface
    public getHeader(string $name): array
    public getHeaderLine(string $name): string
    public getHeaders(): array
    public getProtocolVersion(): string
    public hasHeader(string $name): bool
    public withAddedHeader(string $name, string|array $value): static
    public withBody(Psr\Http\Message\StreamInterface $body): static
    public withHeader(string $name, string|array $value): static
    public withoutHeader(string $name): static
    public withProtocolVersion(string $version): static
}

class StoreCore\Engine\Response
extends StoreCore\Engine\Message
implements Psr\Http\Message\ResponseInterface {
    /* Methods */
    public getReasonPhrase(): string
    public getStatusCode(): int
    public output(): void
    public setReasonPhrase(string|null $reason_phrase): void
    public setStatusCode(int $code): void
    public withStatus(int $code, string $reasonPhrase = ''): static

    /* Inherited methods */
    public getBody(): Psr\Http\Message\StreamInterface
    public getHeader(string $name): array
    public getHeaderLine(string $name): string
    public getHeaders(): array
    public getProtocolVersion(): string
    public hasHeader(string $name): bool
    public setBody(Psr\Http\Message\StreamInterface $body): void
    public setHeader(string $name, string|array $value): void
    public setProtocolVersion(string|int $version): void
    private unsetHeader(string $name): void
    public withAddedHeader(string $name, string|array $value): static
    public withBody(Psr\Http\Message\StreamInterface $body): static
    public withHeader(string $name, string|array $value): static
    public withoutHeader(string $name): static
    public withProtocolVersion(string $version): static
}
```


## Full-page cache

```php
interface Psr\Http\Server\RequestHandlerInterface {
    public handle(Psr\Http\Message\ServerRequestInterface $request): Psr\Http\Message\ResponseInterface;
}

abstract class AbstractServerRequestHandler
implements Psr\Http\Server\RequestHandlerInterface {
    public __construct(protected readonly Psr\Http\Server\RequestHandlerInterface|null $successor = null)
    final public handle(Psr\Http\Message\ServerRequestInterface $request): Psr\Http\Message\ResponseInterface
    abstract protected process(Psr\Http\Message\ServerRequestInterface $request): Psr\Http\Message\ResponseInterface|null;
}

class StoreCore\Engine\FullPageCacheRequestHandler
extends StoreCore\Engine\AbstractServerRequestHandler
implements Psr\Http\Server\RequestHandlerInterface {
    /* Methods */
    protected process(Psr\Http\Message\ServerRequestInterface $request): Psr\Http\Message\ResponseInterface|null

    /* Inherited methods */
    public __construct(protected readonly Psr\Http\Server\RequestHandlerInterface|null $successor = null)
    final public handle(Psr\Http\Message\ServerRequestInterface $request): Psr\Http\Message\ResponseInterface
}
```
