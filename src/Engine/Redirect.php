<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2019, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \DateInterval;
use \DateTimeImmutable;
use \DateTimeInterface;
use \ValueError;
use Psr\Clock\ClockInterface;
use Psr\Http\Message\UriInterface;
use StoreCore\ClockTrait;
use StoreCore\Types\CacheKey;

use function \count;
use function \empty;
use function \is_array;
use function \is_int;
use function \is_string;
use function \key;
use function \reset;
use function \rtrim;
use function \str_starts_with;
use function \substr;

/**
 * Cacheable HTTP redirect.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class Redirect implements ClockInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var \DateTimeInterface $dateCreated
     *   The date and time the redirect was first created.  This date MAY be
     *   used to clean up the database, for example by dropping old permanent
     *   redirects or temporary redirects that were created for a campaign
     *   in a specific timeframe.
     */
    public DateTimeInterface $dateCreated;

    /**
     * @var null|\DateTimeInterface $dateTouched
     *   The date and time the redirect was last “touched” or “hit” by a client.
     *   MAY be `null` if a redirect was never used.
     */
    public ?DateTimeInterface $dateTouched = null;

    /**
     * @var null|\DateTimeInterface $expiresAt
     *   The point in time after which the redirect MUST be considered expired.
     *   MAY be `null` for a permanent redirect that never expires.
     */
    public ?DateTimeInterface $expiresAt = null;

    /**
     * @var string $redirectFromURL
     *   Origin URL to redirect from.
     */
    private string $redirectFromURL;

    /**
     * @var string $redirectToURL
     *   Destination URL to redirect to.  Defaults to '/' for the domain root.
     */
    private string $redirectToURL = '/';

    /**
     * Creates a redirect.
     *
     * @param Psr\Http\Message\UriInterface|string $from_url
     *   Origin URL to redirect from.
     *
     * @param Psr\Http\Message\UriInterface|string $to_url
     *   Destination URL to redirect to.
     *
     * @param null|\DateTimeInterface|\DateInterval|int $expiration
     *   OPTIONAL expiration date and time or expiration interval.
     *   An integer is converted to an interval in seconds.
     */
    public function __construct(
        UriInterface|string $from_url,
        UriInterface|string $to_url = '/',
        null|DateTimeInterface|DateInterval|int $expiration = null
    ) {
        $this->dateCreated = $this->now();

        $this->set([(string) $from_url => (string) $to_url]);

        if ($expiration !== null) {
            if ($expiration instanceof DateTimeInterface) {
                $this->expiresAt($expiration);
            } else {
                $this->expiresAfter($expiration);
            }
        }
    }

    /**
     * Sets the relative expiration time for this cached redirect.
     *
     * @param int|\DateInterval|null $time
     *   The period of time from the present after which the item MUST be considered
     *   expired. An integer parameter is understood to be the time in seconds until
     *   expiration. If `null` is passed explicitly or none is set, the redirect
     *   will never expire and is therefore treated as a permanent redirect.
     *
     * @return static
     * 
     * @see https://github.com/php-fig/cache/blob/master/src/CacheItemInterface.php#L91 
     *      \Psr\Cache\CacheItemInterface::expiresAfter()
     */
    public function expiresAfter(int|DateInterval|null $time): static
    {
        if (is_int($time)) {
            $time = new DateInterval('PT' . $time . 'S');
        }

        if ($time === null) {
            $this->expiresAt(null);
        } else {
            $this->expiresAt($this->now()->add($time));
        }

        return $this;
    }

    /**
     * Sets the expiration time for this cache item.
     *
     * @param null|\DateTimeInterface $expiration
     *   The point in time after which the item MUST be considered expired.
     *   If `null` is passed explicitly, the redirect never expires and is
     *   considered to be a permanent redirect.
     *
     * @return static
     *   The called object.
     */
    public function expiresAt(null|DateTimeInterface $expiration): static
    {
        $this->expiresAt = $expiration;
        return $this;
    }

    /**
     * Gets the redirect URLs.
     * 
     * @param void
     *
     * @return array
     *   Returns a URL key-value pair with the origin URL as the key
     *   and the destination URL as the value.
     */
    public function get(): array
    {
        return [$this->redirectFromURL => $this->redirectToURL];
    }

    /**
     * Gets the redirect destination.
     *
     * @param void
     *
     * @return string
     *   Returns the target destination as an URL string that MAY be used
     *   for the HTTP `Location` redirection header.
     */
    public function getLocation(): string
    {
        return (string) $this->redirectToURL;
    }

    /**
     * Sets the redirect URLs.
     *
     * @param array $value
     *   Redirect URLs as a key/value pair.
     *
     * @return static
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Throws a PSR-16 SimpleCache invalid argument cache exception if the
     *   `$value` is not an array or the array does not consist of a pair of
     *   strings.
     *
     * @uses StoreCore\Types\CacheKey
     *   The StoreCore `CacheKey` type class is used to generate a cache key
     *   that is derived from the source URL.  The cache key is case-insensitive
     *   and ignores the protocol prefix in de source URL.
     */
    public function set(mixed $value): static
    {
        if (!is_array($value) || count($value) !== 1) {
            throw new InvalidCacheArgumentException();
        }

        $this->redirectFromURL = key($value);
        if (!is_string($this->redirectFromURL) || empty($this->redirectFromURL)) {
            throw new InvalidCacheArgumentException();
        }
        $this->redirectFromURL = mb_strtolower($this->redirectFromURL, 'UTF-8');

        if (str_starts_with($this->redirectFromURL, 'http')) {
            if (substr($this->redirectFromURL, 0, 8) === 'https://') {
                $this->redirectFromURL = substr($this->redirectFromURL, 6);
            } elseif (substr($this->redirectFromURL, 0, 7) === 'http://') {
                $this->redirectFromURL = substr($this->redirectFromURL, 5);
            }
        }

        $this->redirectToURL = reset($value);
        if (!is_string($this->redirectToURL) || empty($this->redirectToURL)) {
            throw new ValueError();
        }

        if ($this->redirectToURL !== '/') {
            $this->redirectToURL = rtrim($this->redirectToURL, '/');
        }

        return $this;
    }
}
