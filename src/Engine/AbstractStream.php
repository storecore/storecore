<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \RuntimeException;
use \Stringable;

use Psr\Http\Message\StreamInterface;

/**
 * Abstract stream resource for HTTP messages.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
abstract class AbstractStream implements StreamInterface, Stringable
{
    /**
     * @var bool $readable
     *   Stream is readable (`true`) or not readable (default `false`).  Please
     *   use the `isReadable()` method to determine if the stream is readable.
     */
    protected bool $readable = false;

    /**
     * @var null|int $size
     *   Size of the stream in bytes if known, or `null` if unknown.
     */
    protected ?int $size = null;

    /**
     * @var resource $streamResource
     *   Resource (handle) of a stream.
     */
    protected $streamResource;

    /**
     * @var bool $writable
     *   Stream is writable (`true`) or not writable (default `false`).  Use
     *   the `isWritable()` method to determine if stream is writable.
     */
    protected bool $writable = false;

    /**
     * Closes the stream resource on destruction.
     *
     * @param void
     */
    public function __destruct()
    {
        $this->close();
    }

    #[\Override]
    public function __toString(): string
    {
        if (!$this->isReadable()) {
            return (string) null;
        }

        try {
            $this->seek(0);
            return (string) $this->getContents();
        } catch (\Exception $e) {
            return (string) null;
        }
    }

    #[\Override]
    public function close(): void
    {
        if (
            is_resource($this->streamResource)
            && get_resource_type($this->streamResource) === 'stream'
        ) {
            fclose($this->streamResource);
        }
    }

    #[\Override]
    final public function detach()
    {
        if (!isset($this->streamResource)) {
            return null;
        }

        $result = $this->streamResource;
        unset($this->streamResource);
        $this->readable = false;
        $this->writable = false;
        return $result;
    }

    #[\Override]
    final public function eof(): bool
    {
        if (is_resource($this->streamResource) && get_resource_type($this->streamResource) === 'stream') {
            $metadata = stream_get_meta_data($this->streamResource);
            if (isset($metadata['eof'])) {
                return $metadata['eof'];
            }
        }
        return false;
    }

    #[\Override]
    public function getContents(): string
    {
        if (!$this->isReadable()) {
            throw new RuntimeException();
        }

        $contents = stream_get_contents($this->streamResource);
        if ($contents === false) throw new RuntimeException();
        return $contents;
    }

    #[\Override]
    public function getMetadata(?string $key = null)
    {
        if (!is_resource($this->streamResource) || get_resource_type($this->streamResource) !== 'stream') {
            return null;
        }

        $metadata = stream_get_meta_data();
        if ($key === null) {
            return $metadata;
        } elseif (array_key_exists($key, $metadata)) {
            return $metadata[$key];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * This implementation of the `StreamInterface::getSize()` method
     * immediately returns a previously determined stream size, if it exists.
     * Operations that change the size, such as `write()`, therefore MUST
     * either set a new size or reset the size to `null`.
     */
    #[\Override]
    public function getSize(): ?int
    {
        if (!is_resource($this->streamResource)) {
            return null;
        }

        if ($this->size !== null) {
            return $this->size;
        }

        $stats = fstat($this->streamResource);
        $this->size = isset($stats['size']) ? (int) $stats['size'] : null;
        return $this->size;
    }

    #[\Override]
    final public function isReadable(): bool
    {
        return $this->readable;
    }

    #[\Override]
    final public function isSeekable(): bool
    {
        if (is_resource($this->streamResource) && get_resource_type($this->streamResource) === 'stream') {
            $metadata = stream_get_meta_data($this->streamResource);
            if (isset($metadata['seekable'])) {
                return $metadata['seekable'];
            }
        }
        return false;
    }

    #[\Override]
    final public function isWritable(): bool
    {
        return $this->writable;
    }

    #[\Override]
    public function read(int $length): string
    {
        if (!$this->isReadable()) {
            throw new RuntimeException('Stream is not readable');
        } else {
            return fread($this->stream, $length);
        }
    }

    #[\Override]
    final public function rewind(): void
    {
        $this->seek(0);
    }

    #[\Override]
    public function seek(int $offset, int $whence = \SEEK_SET): void
    {
        if (!$this->isSeekable()) {
            throw new RuntimeException('Stream is not seekable');
        } elseif (fseek($this->streamResource, $offset, $whence) === -1) {
            throw new RuntimeException('Unable to seek to stream position ' . $offset . ' with whence ' . var_export($whence, true));
        }
    }

    #[\Override]
    public function tell(): int
    {
        $result = ftell($this->streamResource);
        if ($result === false) {
            throw new RuntimeException('Unable to determine stream position');
        } else {
            return $result;
        }
    }

    #[\Override]
    abstract public function write(string $string): int;
}
