<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\AbstractController;
use StoreCore\Database\BlocklistReader;

/**
 * Access control handler.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Security
 * @version   1.0.0-alpha.1
 */
class AccessControlHandler extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';


    /**
     * {@inheritDoc}
     * 
     * Returns informational `100 Continue` HTTP status code if the StoreCore
     * blocklist is not used or the current IP address is not blacklisted.
     * The blocklist MAY be disabled by setting `STORECORE_CMS_BLOCKLIST` to
     * `(bool) false` or `(int) 0`.
     * 
     * If `STORECORE_CMS_BLOCKLIST` is not set to `(bool) false`, which
     * indicates that the blocklist SHOULD be checked, database access MAY
     * still be prevented by a count of `(int) 0` blocked IP addresses.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/100
     *   The HTTP `100 Continue` informational status response code indicates
     *   that everything so far is OK and that the client should continue with
     *   the request or ignore it if it is already finished.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/403
     *   The HTTP `403 Forbidden` response status code indicates that the
     *   server understands the request but refuses to authorize it.  This
     *   status is similar to `401 Unauthorized`, but for the `403 Forbidden`
     *   status code re-authenticating makes no difference.  The access is
     *   permanently forbidden and tied to the application logic, such as
     *   insufficient rights to a resource.
     *
     * @uses \StoreCore\Database\BlocklistReader::has()
     *
     * @uses \StoreCore\Database\BlocklistReader::log()
     *
     * @uses \StoreCore\Engine\ResponseFactory::createResponse()
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $factory = new ResponseFactory();

        if (\defined('STORECORE_CMS_BLOCKLIST') && empty(STORECORE_CMS_BLOCKLIST)) {
            return $factory->createResponse(100);
        }

        try {
            $blocklist = new BlocklistReader($this->Registry);
            if ($blocklist->has($request->getRemoteAddress())) {
                $blocklist->log($request->getRemoteAddress());
                return $factory->createResponse(403);
            }
        } catch (\Exception $e) {
            // `502 Bad Gateway` in case we run into trouble.
            return $factory->createResponse(502);
        }

        return $factory->createResponse(100);
    }
}
