<?php

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Client\ClientExceptionInterface;

class ClientException extends \Exception implements ClientExceptionInterface {}
