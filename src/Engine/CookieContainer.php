<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use StoreCore\AbstractContainer;
use Psr\Container\ContainerInterface;

use function \array_merge;
use function \isset;

/**
 * Container for cookies.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class CookieContainer extends AbstractContainer implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a cookie container.
     *
     * @param null|array $cookie_parameters
     *   OPTIONAL cookie key-value pairs with the generic data structure of the
     *   PHP superglobal `$_COOKIE` and the `ServerRequest::getCookieParams()`
     *   return value.
     */
    public function __construct(?array $cookie_parameters = null)
    {
        if (isset($_COOKIE)) $this->data = $_COOKIE;
        if ($cookie_parameters !== null) $this->data = array_merge($this->data, $cookie_parameters);
    }
}
