<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\UriInterface;

use function \empty;
use function \ltrim;
use function \strtolower;
use function \trim;

/**
 * Uniform Resource Identifier (URI).
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Identifying_resources_on_the_Web
 *      Identifying resources on the Web
 * 
 * @see https://www.php-fig.org/psr/psr-7/#35-psrhttpmessageuriinterface
 *      PSR-7 Psr\Http\Message\UriInterface
 */
class UniformResourceIdentifier implements \Stringable, UriInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $fragment
     *   Fragment component of the URI.
     */
    private string $fragment = '';

    /**
     * @var string $host;
     *   Host component of the URI.
     */
    private string $host = '';

    /**
     * @var null|string $password;
     *   Password in the user information component of the URI.
     */
    private ?string $password = null;

    /**
     * @var string $path;
     *   Path component of the URI.
     */
    private string $path = '';

    /**
     * @var null|int $port
     *   Port component of the URI as an integer or null for the default port
     *   of the current scheme.
     */
    private ?int $port = null;

    /**
     * @var string $query
     *   Query string of the URI.
     */
    private string $query = '';

    /**
     * @var string $scheme
     *   Scheme component of the URI.
     */
    private string $scheme = '';

    /**
     * @var string $userName;
     *   User name in the user information component of the URI.
     */
    private string $userName = '';

    /**
     * Converts the URL object to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the current location as a string.
     *
     * @uses \StoreCore\Engine\UniformResourceIdentifier::get()
     */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * Gets the current URI.
     *
     * @param void
     *
     * @return string
     *   Returns the URI as string.
     */
    public function get(): string
    {
        $uri = $this->getScheme();
        if (!empty($uri)) $uri .= '://';

        $uri .= $this->getAuthority();

        if (empty($uri)) {
            $uri = $this->getPath();
        } else {
            $uri .= '/' . ltrim($this->getPath(), '/');
        }

        if (!empty($this->getQuery())) {
            $uri .= '?' . $this->getQuery();
        }

        if (!empty($this->getFragment())) {
            $uri .= '#' . $this->getFragment();
        }

        return $uri;
    }

    /**
     * Retrieves the authority component of the URI.
     *
     * @param void
     *
     * @return string
     *   Returns the `[user-info@]host[:port]` authority component or an empty
     *   string if no authority information is present.
     */
    #[\Override]
    public function getAuthority(): string
    {
        $authority = $this->getHost();
        if (empty($authority)) {
            return (string) null;
        }

        if (!empty($this->getUserInfo())) {
            $authority = $this->getUserInfo() . '@' . $authority;
        }

        if (!empty($this->getPort())) {
            $authority = $authority . ':' . $this->getPort();
        }

        return $authority;
    }

    /**
     * Retrieve the fragment component of the URI.
     *
     * @param void
     *
     * @return string
     *   The URI fragment or an empty string if no fragment is present.
     */
    #[\Override]
    public function getFragment(): string
    {
        return $this->fragment;
    }

    /**
     * Retrieve the host component of the URI.
     *
     * @param void
     *
     * @return string
     *   The URI host in lowercase or an empty string if no host is present.
     */
    #[\Override]
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Retrieve the path component of the URI.
     *
     * @param void
     *
     * @return string
     *   The URI path.
     */
    #[\Override]
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Retrieves the port component of the URI.
     *
     * @param void
     *
     * @return null|int
     *   If a port number is present, and it is non-standard for the current
     *   scheme, this method returns it as an integer.  If the port is the
     *   standard port used with the current scheme, this method returns null.
     */
    #[\Override]
    public function getPort(): ?int
    {
        if ($this->port === 80 && $this->getScheme() === 'http') {
            return null;
        } elseif ($this->port === 443 && $this->getScheme() === 'https') {
            return null;
        } else {
            return $this->port;
        }
    }

    /**
     * Retrieves the query string of the URI.
     *
     * @param void
     *
     * @return string
     *   The URI query string or an empty string if no query string is present.
     */
    #[\Override]
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * Gets the scheme component of the URI.
     *
     * @param void
     *
     * @return string
     *   The URI scheme normalized to lowercase.  If no scheme is present, this
     *   method returns an empty string.
     */
    #[\Override]
    public function getScheme(): string
    {
        return $this->scheme;
    }

    /**
     * Retrieves the user information component of the URI.
     *
     * @param void
     *
     * @return string
     *   Returns the URI user information with a username and an optional
     *   password as a string in `username[:password]` format.  This method
     *   will return an empty string if there is no user information present.
     */
    #[\Override]
    public function getUserInfo(): string
    {
        $user_info = $this->userName;
        if ($this->password !== null) {
            $user_info .= ':' . $this->password;
        }
        return $user_info;
    }

    /**
     * Sets the authority component of the URI.
     *
     * @param string $authority
     *   URI authority as a string in `[user-info@]host[:port]` format.
     *
     * @return void
     *
     * @uses setHost()
     *   Sets the `host` component.
     * 
     * @uses setPort()
     *   Sets the optional `[:port]` component if it is present.
     *
     * @uses setUserInfo()
     *   Sets the optional `[user-info@]` component if it is present.
     */
    public function setAuthority($authority)
    {
        $authority = trim($authority);

        if (strpos($authority, '@') !== false) {
            $authority = explode('@', $authority, 2);
            $user_info = $authority[0];
            $authority = $authority[1];
            if (!empty($user_info)) {
                $this->setUserInfo($user_info);
            }
        }

        if (strpos($authority, ':') !== false) {
            $authority = explode(':', $authority, 2);
            $authority = $authority[0];
            $port = $authority[1];
            if (is_numeric($port)) {
                $port = (int)$port;
                $this->setPort($port);
            }
        }

        $this->setHost($authority);
    }

    /**
     * Sets the fragment component of the URI.
     *
     * @param string $fragment
     *   The URI fragment or an empty string to remove an existing fragment.
     *
     * @return void
     */
    public function setFragment($fragment)
    {
        $fragment = trim($fragment);
        $fragment = ltrim($fragment, '#');
        $this->fragment = $fragment;
    }

    /**
     * Sets the host component of the URI.
     *
     * @param string $host
     *   The URI host.
     *
     * @return void
     */
    public function setHost($host)
    {
        $host = trim($host);
        $host = strtolower($host);
        $this->host = $host;
    }

    /**
     * Sets the URI path.
     *
     * @param string $path
     *   An empty, absolute, or relative HTTP path.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     *   Throws an SPL invalid argument exception for invalid paths.
     */
    public function setPath($path)
    {
        if (!\is_string($path)) throw new \InvalidArgumentException();
        $path = trim($path);
        $this->path = $path;
    }

    /**
     * Sets the port number.
     *
     * @param null|int|string $port
     *   The port number as an integer.  A null value or empty string removes
     *   the port information.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception on an invalid port number.
     */
    public function setPort(mixed $port): void
    {
        if (empty($port)) {
            $this->unsetPort();
        } elseif (\is_int($port)) {
            $this->port = $port;
        } elseif (ctype_digit($port)) {
            $this->port = \intval($port);
        } else {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * Sets the query string part of the URI.
     *
     * @param string $query
     *   The URI query string or an empty string to remove an existing query.
     *   A trailing question mark ? is removed.
     *
     * @return void
     */
    public function setQuery(string $query): void
    {
        if (!\is_string($query)) throw new \InvalidArgumentException();
        $query = trim($query);
        $query = ltrim($query, '?');
        $this->query = $query;
    }

    /**
     * Sets the URI scheme.
     *
     * @param string $scheme
     *   The scheme component of the URI.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     *   Throws an SPL invalid argument exception if the `$scheme` parameter is
     *   not a string.
     */
    public function setScheme(string $scheme): void
    {
        if (!\is_string($scheme)) throw new \InvalidArgumentException();
        $scheme = trim($scheme);
        $scheme = strtolower($scheme);
        $this->scheme = $scheme;
    }

    /**
     * Sets the user information component of the URI.
     *
     * @param string $user
     *   User name or an empty string to remove the user information.
     *
     * @param string|null $password
     *   OPTIONAL password for `$user`.  The password is ignored and set
     *   to `null` if `$user` is empty.
     *
     * @return void
     */
    public function setUserInfo(string $user, ?string $password = null): void
    {
        $this->userName = trim($user);
        if (empty($this->userName)) {
            $password = null;
        }

        if ($password !== null) {
            $password = trim($password);
            $this->password = empty($password) ? null : $password;
        }
    }

    /**
     * Removes the port.
     *
     * @param void
     * @return void
     */
    public function unsetPort(): void
    {
        $this->port = null;
    }

    #[\Override]
    public function withFragment(string $fragment): UriInterface
    {
        $uri = clone $this;
        $uri->setFragment($fragment);
        return $uri;
    }

    #[\Override]
    public function withHost(string $host): UriInterface
    {
        $uri = clone $this;
        $uri->setHost($fragment);
        return $uri;
    }

    #[\Override]
    public function withPath(string $path): UriInterface
    {
        $uri = clone $this;
        $uri->setPath($path);
        return $uri;
    }

    #[\Override]
    public function withPort(?int $port): UriInterface
    {
        $uri = clone $this;
        $uri->setPort($port);
        return $uri;
    }

    #[\Override]
    public function withQuery(string $query): UriInterface
    {
        $uri = clone $this;
        $uri->setQuery($query);
        return $uri;
    }

    #[\Override]
    public function withScheme(string $scheme): UriInterface
    {
        $uri = clone $this;
        $uri->setScheme($scheme);
        return $uri;
    }

    #[\Override]
    public function withUserInfo(string $user, ?string $password = null): UriInterface
    {
        $uri = clone $this;
        $user_info = trim($user);
        if (!empty($user_info) && $password !== null) {
            $user_info .= ':' . $password;
        }
        $uri->setUserInfo($user_info);
        return $uri;
    }
}
