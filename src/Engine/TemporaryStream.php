<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use \RuntimeException;
use Psr\Http\Message\StreamInterface;

use function \empty;
use function \fopen;
use function \fwrite;

/**
 * Temporary HTTP message stream.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class TemporaryStream extends AbstractStream implements StreamInterface
{
    /**
     * @var string FILENAME
     *   Stream filename for `fopen()`.  Defaults to `php://temp` for
     *   a temporary PHP stream.
     */
    public const string FILENAME = 'php://temp';

    /**
     * @var string MODE
     *   Stream I/O mode for `fopen()`.
     */
    public const string MODE = 'r+';

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a temporary stream.
     *
     * @param null|string $content 
     *   OPTIONAL string content with which to populate the stream.
     */
    public function __construct(?string $content = null)
    {
        $this->streamResource = fopen(static::FILENAME, static::MODE);
        if ($this->streamResource !== false) {
            $this->readable = true;
            $this->writable = true;
            if (!empty($content)) {
                $this->size = $this->write($content);
            }
        }
    }

    #[\Override]
    public function write(string $string): int
    {
        if (!$this->isWritable()) {
            throw new RuntimeException('Temporary stream is not writable');
        }
    
        if (empty($string)) {
            return 0;
        }

        $bytes_written = fwrite($this->streamResource, $string);
        if ($bytes_written === false) throw new RuntimeException('Error writing to temporary stream');
        $this->size = null;
        return $bytes_written;
    }
}
