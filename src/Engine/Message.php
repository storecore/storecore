<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{MessageInterface, StreamInterface};

use function \array_change_key_case;
use function \array_key_exists;
use function \array_merge;
use function \array_unique;
use function \implode;
use function \isset;
use function \strtolower;
use function \strtoupper;

use const \SORT_STRING;

/**
 * HTTP message.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-7-http-message.md
 * @see     https://github.com/php-fig/http-message/blob/master/src/MessageInterface.php
 * @version 1.0.0
 */
class Message implements MessageInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var Psr\Http\Message\StreamInterface|null $body
     *   Body of the message as a stream.
     */
    protected ?StreamInterface $body = null;

    /**
     * @var array $headers
     *   HTTP message headers.
     */
    protected array $headers = [];

    /**
     * @var null|string $protocolVersion
     *   HTTP version number (e.g., “1.1”, “1.0”).
     */
    private ?string $protocolVersion = null;

    /**
     * Adds an HTTP header to the message.
     *
     * @param string $name
     *   Case-insensitive name of the HTTP header.  If the HTTP header already
     *   exists, `addHeader()` adds an extra value to the existing header.
     *   Use `setHeader()` to replace an existing header.
     *
     * @param string|string[] $value
     *   Header value(s) as a string or an array consisting of strings.
     *
     * @return void
     */
    public function addHeader(string $name, string|array $value): void
    {
        if ($name === 'Last-Modified' && \is_string($value)) {
            $value = array($value);
        }

        if (\is_string($value)) {
            $value = str_replace(', ', ',', $value);
            $value = explode(',', $value);
        }

        if (!\is_array($value)) throw new \InvalidArgumentException();

        if (!isset($this->headers[$name])) {
            $this->headers[$name] = $value;
        } elseif ($this->headers[$name] !== $value) {
            $this->headers[$name] = array_merge($this->headers[$name], $value);
        }
    }

    #[\Override]
    public function getBody(): StreamInterface
    {
        if ($this->body === null) {
            $this->setBody(new TemporaryStream(''));
        }
        return $this->body;
    }

    #[\Override]
    public function getHeader(string $name): array
    {
        $header = [];
        $name = strtoupper($name);
        foreach ($this->headers as $key => $values) {
            if (strtoupper($key) === $name) {
                $header = array_merge($header, $values);
            }
        }
        return array_unique($header, SORT_STRING);
    }

    /**
     * {@inheritDoc}
     *
     * @uses StoreCore\Engine\Message::hasHeader()
     *   Uses the `hasHeader()` method to determine of the `$name` header exists.
     *
     * @uses StoreCore\Engine\Message::getHeader()
     *   Returns the `getHeader()` result as a string with comma-separated values.
     */
    #[\Override]
    public function getHeaderLine(string $name): string
    {
        return $this->hasHeader($name) ? implode(',', $this->getHeader($name)) : (string) null;
    }

    #[\Override]
    public function getHeaders(): array
    {
        return $this->headers;
    }

    #[\Override]
    public function getProtocolVersion(): string
    {
        if ($this->protocolVersion === null) {
            $this->setProtocolVersion('1.1');
            if (isset($_SERVER['SERVER_PROTOCOL'])) {
                $protocol_version = explode('/', $_SERVER['SERVER_PROTOCOL'], 2);
                if (isset($protocol_version[1])) {
                    $this->setProtocolVersion($protocol_version[1]);
                }
            }
        }

        return $this->protocolVersion;
    }

    #[\Override]
    public function hasHeader($name): bool
    {
        if (empty($this->headers)) {
            return false;
        }
        if (array_key_exists($name, $this->headers)) {
            return true;
        }

        $name = strtolower($name);
        $headers = array_change_key_case($this->headers, CASE_LOWER);
        return array_key_exists($name, $headers);
    }

    /**
     * Sets the message body.
     *
     * @param  Psr\Http\Message\StreamInterface $body
     *   Body of the message as a stream.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception when the body is not valid.
     */
    public function setBody(StreamInterface $body): void
    {
        $this->body = $body;
    }

    /**
     * Sets and replaces an HTTP header.
     *
     * @param string $name
     *   Case-insensitive name of the HTTP header.  If the header already
     *   exists, it is replaced.
     *
     * @param string|string[] $value
     *   Header value(s) as a string or an array consisting of strings.
     *
     * @return void
     */
    public function setHeader(string $name, string|array $value): void
    {
        try {
            $this->unsetHeader($name);
            $this->addHeader($name, $value);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * Sets the HTTP protocol version.
     *
     * @param string|int $version
     *   HTTP protocol version.
     */
    public function setProtocolVersion(string|int $version): void
    {
        if (\is_int($version)) $version = (string) $version;
        $this->protocolVersion = $version;
    }

    /**
     * Removes an HTTP message header.
     *
     * @param string $name
     *   Case-insensitive name of the header to remove.
     *
     * @return void
     */
    private function unsetHeader(string $name): void
    {
        unset($this->headers[$name]);
        $name = strtolower($name);
        foreach ($this->headers as $key => $value) {
            if (strtolower($key) === $name) unset($this->headers[$key]);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @uses \StoreCore\Engine\Message::addHeader()
     *   This method executes `addHeader()` to add the new HTTP header value
     *   to an existing header.
     */
    #[\Override]
    public function withAddedHeader(string $name, $value): MessageInterface
    {
        $message = clone $this;
        $message->addHeader($name, $value);
        return $message;
    }

    #[\Override]
    public function withBody(StreamInterface $body): MessageInterface
    {
        $message = clone $this;
        $message->setBody($body);
        return $message;
    }

    /**
     * {@inheritDoc}
     *
     * @uses \StoreCore\Engine\Message::setHeader()
     *   This method executes `setHeader()` to add a new HTTP header and
     *   replace an existing header.
     */
    #[\Override]
    public function withHeader(string $name, $value): MessageInterface
    {
        $message = clone $this;
        $message->setHeader($name, $value);
        return $message;
    }

    /**
     * {@inheritDoc}
     *
     * @uses \StoreCore\Engine\Message::unsetHeader()
     *   Uses the private method `unsetHeader()` to remove an existing header
     *   from an instance of the message.
     */
    #[\Override]
    public function withoutHeader(string $name): MessageInterface
    {
        $message = clone $this;
        $message->unsetHeader($name);
        return $message;
    }

    #[\Override]
    public function withProtocolVersion(string $version): MessageInterface
    {
        $message = clone $this;
        $message->setProtocolVersion($version);
        return $message;
    }
}
