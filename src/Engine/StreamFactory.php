<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\{StreamFactoryInterface, StreamInterface};
use StoreCore\FileSystem\FileStream;

/**
 * PSR-17 HTTP stream factory.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 *
 * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-17-http-factory.md#24-streamfactoryinterface
 *      PSR-17 HTTP Factories StreamFactoryInterface
 *
 * @see https://www.php-fig.org/psr/psr-17/#24-streamfactoryinterface
 *      PSR-17 HTTP Factories StreamFactoryInterface
 */
class StreamFactory implements StreamFactoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    public function createStream(string $content = ''): StreamInterface
    {
        return new TemporaryStream($content);
    }

    /**
     * @inheritDoc
     */
    public function createStreamFromFile(string $filename, string $mode = 'r'): StreamInterface
    {
        if (!is_file($filename) || is_dir($filename)) {
            throw new \RuntimeException(__METHOD__ . ' expects parameter 1 to be the filename of an existing file');
        } 

        try {
            return new FileStream($filename, $mode);
        } catch (\Throwable $t) {
            throw new \RuntimeException($t->getMessage(), (int) $t->getCode(), $t);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws \RuntimeException
     *   According to the PSR-17 standard, the returned stream MUST be readable
     *   and MAY be writable.  This method therefore throws a runtime exception
     *   if the stream is not readable.
     */
    public function createStreamFromResource($resource): StreamInterface
    {
        if ($resource instanceof StreamInterface) {
            if (!$resource->isReadable()) {
                throw new \RuntimeException('Resource is not readable');
            }
            return $resource;
        }

        if (\is_string($resource) && is_file($resource)) {
            try {
                $stream = $this->createStreamFromFile($resource);
            } catch (\Exception $e) {
                throw new \RuntimeException('Cannot create stream from resource', $e->getCode(), $e);
            }

            if (!$stream->isReadable()) {
                throw new \RuntimeException('Resource is not readable');
            }
            return $stream;
        }

        if ($resource instanceof \Stringable || \is_string($resource)) {
            $resource = (string) $resource;
            return $this->createStream($resource);
        }
    }
}
