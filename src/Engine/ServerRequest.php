<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Engine;

use Psr\Http\Message\ServerRequestInterface;

interface_exists(\Psr\Http\Message\MessageInterface::class);
interface_exists(\Psr\Http\Message\RequestInterface::class);
interface_exists(\Psr\Http\Message\ServerRequestInterface::class);
class_exists(Message::class);
class_exists(Request::class);

/**
 * PSR-7 compliant server-side HTTP request.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-7-http-message.md#321-psrhttpmessageserverrequestinterface
 * @version 1.0.0-alpha.1
 */
class ServerRequest extends Request implements ServerRequestInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $attributes
     *   Generic data injection container for parameters that may be derived
     *   from the request.
     */
    private array $attributes = [];

    /**
     * @var array $cookieParams
     *   Cookies sent by the client to the server.  The data MUST be compatible
     *   with the structure of the PHP `$_COOKIE` superglobal.
     */
    private array $cookieParams = [];

    /**
     * @var mixed $parsedBody
     *   Deserialized body data.  This will typically be in an array or object.
     */
    private $parsedBody;

    /**
     * @var array $queryParams
     *   Deserialized query string arguments, if any.
     */
    private array $queryParams = [];

    /**
     * @var array $uploadedFiles
     *   Array tree of PSR-7 `UploadedFileInterface` instances.  This array
     *   MUST be empty if no data is present.
     */
    private array $uploadedFiles = [];

    /**
     * @var array $serverParams
     *   Server parameters including, but not limited to, the PHP superglobal
     *   array `$_SERVER`.  Defaults to a `HTTP GET` request.
     */
    private array $serverParams = [
        'REQUEST_METHOD' => 'GET',
        'REQUEST_SCHEME' => 'http',
    ];


    /**
     * Gets a request value by name.
     *
     * @param string $key
     *   Unique name of the parameter to retrieve.
     *
     * @return string|array|null
     *   Return `$_POST`, `$_GET`, or `$_COOKIE`  data, in that order.  Returns
     *   `null` if the parameter was not found, otherwise a single string
     *   or an array of string values.
     */
    public function get($key)
    {
        if (!\is_string($key)) return null;

        if (\is_array($this->parsedBody) && array_key_exists($key, $this->parsedBody)) {
            return $this->parsedBody[$key];
        } elseif (array_key_exists($key, $this->queryParams)) {
            return $this->queryParams[$key];
        } elseif (array_key_exists($key, $this->cookieParams)) {
            return $this->cookieParams[$key];
        } else {
            return null;
        }
    }

    #[\Override]
    public function getAttribute(string $name, mixed $default = null): mixed
    {
        return array_key_exists($name, $this->attributes) ? $this->attributes[$name] : $default;
    }

    #[\Override]
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    #[\Override]
    public function getCookieParams(): array
    {
        return $this->cookieParams;
    }

    #[\Override]
    public function getParsedBody(): null|array|object
    {
        return $this->parsedBody;
    }

    #[\Override]
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * Gets the client IP address.
     *
     * @param void
     *
     * @return string
     *   Returns the IPv4 or IPv6 IP address of the client or an empty string.
     */
    public function getRemoteAddress()
    {
        return array_key_exists('REMOTE_ADDR', $this->serverParams) ? $this->serverParams['REMOTE_ADDR'] : (string) null;
    }

    /**
     * Gets a single server parameter.
     *
     * @param string $name
     *   Name of the server parameter.
     * 
     * @return string|int|float|null
     *   Returns the value of the server parameter or `null` if the
     *   server parameter does not exist.
     */
    public function getServerParam(string $name)
    {
        return array_key_exists($name, $this->serverParams) ? $this->serverParams[$name] : null;
    }

    #[\Override]
    public function getServerParams(): array
    {
        return $this->serverParams;
    }

    #[\Override]
    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    /**
     * Adds an attribute.
     *
     * @param string $name
     *   Name of the attribute to add.
     * 
     * @param mixed $value
     *   Value of the attribute.
     *
     * @return void
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * Set cookies.
     *
     * @param array $cookie_parameters
     *   Array of key/value pairs representing cookies.
     *
     * @return void
     */
    public function setCookieParams(array $cookie_parameters)
    {
        if (!\is_array($cookie_parameters)) throw new \InvalidArgumentException();
        $this->cookieParams = array_merge($this->cookieParams, $cookie_parameters);
    }

    /**
     * Set the request body.
     * 
     * @param null|array|object $data
     *   Deserialized body data.
     *
     * @return void
     */
    public function setParsedBody($data)
    {
        $this->parsedBody = $data;
    }

    /**
     * Set query string parameters.
     *
     * @param array $query
     *   Query string arguments as deserialized key/value pairs.
     *
     * @return void
     */
    public function setQueryParams(array $query)
    {
        $this->queryParams = array_merge($this->queryParams, $query);
    }

    /**
     * Sets server parameters.
     *
     * @param array $server_parameters
     *   Array of Server API (SAPI) parameters for the server request instance.
     *
     * @return void
     */
    public function setServerParams(array $server_parameters)
    {
        $this->serverParams = array_merge($this->serverParams, $server_parameters);
    }

    /**
     * Add uploaded files.
     *
     * @param array $uploaded_files
     *   An array tree of PSR-7 UploadedFileInterface instances.
     *
     * @return void
     */
    public function setUploadedFiles($uploaded_files)
    {
        $this->uploadedFiles = array_merge($this->uploadedFiles, $uploaded_files);
    }

    /**
     * Remove an attribute from the request.
     *
     * @param string $name
     *   Name of the attribute to remove.
     *
     * @return bool
     *   Returns `true` on success or `false` if the attribute does not exist.
     */
    public function unsetAttribute(string $name): bool
    {
        if (array_key_exists($name, $this->attributes)) {
            unset($this->attributes[$name]);
            return true;
        }

        return false;
    }

    #[\Override]
    public function withAttribute(string $name, mixed $value): ServerRequestInterface
    {
        $request = clone $this;
        $request->setAttribute($name, $value);
        return $request;
    }

    /**
     * {@inheritDoc}
     *
     * @uses StoreCore\Engine\ServerRequest::unsetAttribute()
     *   Uses `unsetAttribute()` to remove the attribute from the new request.
     */
    #[\Override]
    public function withoutAttribute(string $name): ServerRequestInterface
    {
        $request = clone $this;
        $request->unsetAttribute($name);
        return $request;
    }

    #[\Override]
    public function withCookieParams(array $cookies): ServerRequestInterface
    {
        $request = clone $this;
        $request->setCookieParams($cookies);
        return $request;
    }

    #[\Override]
    public function withParsedBody($data): ServerRequestInterface
    {
        $request = clone $this;
        $request->setParsedBody($data);
        return $request;
    }

    #[\Override]
    public function withQueryParams(array $query): ServerRequestInterface
    {
        $request = clone $this;
        $request->setQueryParams($query);
        return $request;
    }

    /**
     * @inheritDoc
     */
    public function withServerParams(array $server_parameters)
    {
        $request = clone $this;
        $request->setServerParams($server_parameters);
        return $request;
    }

    #[\Override]
    public function withUploadedFiles(array $uploadedFiles): ServerRequestInterface
    {
        $request = clone $this;
        $request->setUploadedFiles($uploaded_files);
        return $request;
    }
}
