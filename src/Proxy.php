<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Types\UUID;

/**
 * Concrete proxy.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
readonly class Proxy implements
    IdentityInterface,
    \JsonSerializable,
    ProxyInterface,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $identifier Public universally unique identifier (UUID) as a string.
     * @var string $type       Schema.org `@type` of object.
     * @var string|null $name  OPTIONAL object name.
     */
    public string $identifier;
    public string $type;
    public ?string $name;

    /**
     * @inheritDoc
     */
    public function __construct(
        UUID|\Stringable|string $identifier,
        string $type = 'Thing',
        ?string $name = null
    ) {
        $this->setIdentifier((string) $identifier);
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * Converts the proxy to the object ID as a string.
     *
     * @param void
     *
     * @return string
     *   Returns the proxy identifier as a string.  This method MAY be used
     *   to include the proxy in an SQL query as a key or foreign key that
     *   points to the entity or aggregate root.
     */
    public function __toString(): string
    {
        return (string) $this->getIdentifier();
    }

    /**
     * Returns the UUID.
     *
     * @param void
     * @return StoreCore\Types\UUID
     * @uses StoreCore\Types\UUID::__construct()
     */
    public function getIdentifier(): UUID|null
    {
        return new UUID($this->identifier);
    }

    /**
     * Checks if the proxy object has an identifier.
     *
     * @param void
     *
     * @return true
     *   Because the proxy is a placeholder for another existing object, it
     *   MUST have an `identifier` and `hasIdentifier()` MUST therefore always
     *   return `true`.
     */
    final public function hasIdentifier(): true
    {
        assert($this->getIdentifier() !== null);
        return true;
    }

    /**
     * Creates a key-value array for JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@id' => (string) $this->getIdentifier(),
            '@context' => 'https://schema.org',
            '@type' => $this->type,
            'identifier' => (string) $this->getIdentifier(),
        ];
        if (!empty($this->name)) {
            $result['name'] = $this->name;
        }
        return $result;
    }

    /**
     * Sets the proxy identifier.
     *
     * @param StoreCore\Types\UUID|string
     * @return void
     */
    public function setIdentifier(UUID|string $uuid): void
    {
        if (\is_string($uuid)) {
            $uuid = new UUID($uuid);
        }
        $this->identifier = (string) $uuid;
    }
}
