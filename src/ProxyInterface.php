<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

interface_exists(IdentityInterface::class);

/**
 * Proxy interface.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
interface ProxyInterface extends IdentityInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a proxy.
     *
     * @param \Stringable|string $identifier
     *   Unique identifier (ID) of the object that this proxy represents.
     *
     * @param string $type
     *   The type of object being represented.  Defaults to the
     *   Schema.org generic base class `Thing`.
     *
     * @param string|null $name.
     *   OPTIONAL.  The name of the object that this proxy represents.
     */
    public function __construct(
        \Stringable|string $identifier,
        string $type = 'Thing',
        ?string $name = null
    );

    /**
     * Proxy implements PHP Stringable interface.
     *
     * When a proxy is converted to a string, the object being represented
     * MUST still be represented by what is left of the proxy, so the logical
     * thing to return is the unique object identifier.
     *
     * @see https://www.php.net/manual/en/class.stringable.php
     * @see https://www.php.net/manual/en/stringable.tostring
     */
    public function __toString(): string;

    /**
     * Proxy must have a known identity.
     *
     * @uses \StoreCore\IdentityInterface::hasIdentifier()
     */
    public function hasIdentifier(): true;

    /**
     * Proxy implements PHP JsonSerializable interface.
     *
     * @see https://www.php.net/manual/en/class.jsonserializable.php
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize(): mixed;
}
