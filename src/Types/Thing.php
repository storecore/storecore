<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ReflectionClass;

use Psr\Http\Message\UriInterface;
use StoreCore\Actions\Action;
use StoreCore\CMS\AbstractCreativeWork as CreativeWork;
use StoreCore\CMS\ImageObject;

use function \empty;
use function \explode;
use function \get_class;
use function \is_array;
use function \is_string;
use function \isset;
use function \unset;
use function \json_encode;
use function \method_exists;
use function \property_exists;
use function \str_replace;
use function \str_starts_with;
use function \ucfirst;

use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

/**
 * Schema.org Thing.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/Thing Schema.org type `Thing`
 * @version 1.0.0-beta.1
 */
class Thing implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * @var null|StoreCore\Types\URL $additionalType
     *   An additional type for the item, typically used for adding more
     *   specific types from external vocabularies in microdata syntax.  This
     *   is a relationship between something and a class that the thing is in.
     */
    public ?URL $additionalType = null;

    /**
     * @var null|string $alternateName
     *   An alias for the item.
     */
    public ?string $alternateName = null;

    /**
     * @var null|string $description
     *    A description of the item.
     */
    public ?string $description = null;

    /**
     * @var null|StoreCore\Types\URL|string $id
     *   JSON-LD `@id` identifier of an object.
     */
    public null|URL|string $id = null;

    /**
     * @var null|StoreCore\CMS\ImageObject|StoreCore\Types\URL|array $image
     *   An image of the item.  This can be a `URL` or a fully described `ImageObject`.
     */
    protected null|ImageObject|URL|array $image = null;

    /**
     * @var null|string $name
     *   The name of the item.
     */
    public ?string $name = null;

    /**
     * @var null|StoreCore\Actions\Action $potentialAction
     *   Indicates a potential `Action`, which describes an idealized action
     *   in which this thing would play an “object” role.
     */
    public ?Action $potentialAction = null;

    /**
     * @var null|StoreCore\Types\URL $sameAs
     *   URL of a reference Web page that unambiguously indicates the item’s
     *   identity, for example the URL of the item’s Wikipedia page, Wikidata
     *   entry, or official website.
     */
    public ?URL $sameAs = null;

    /**
     * @var null|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\Event $subjectOf
     *   A `CreativeWork` or an `Event` about this `Thing`.
     */
    public null|CreativeWork|Event $subjectOf = null;

    /**
     * @var StoreCore\Types\URL|null $url
     *   URL of the item.
     */
    protected ?URL $url = null;

    /**
     * Creates some thing.
     *
     * @param null|string|array $param
     *   OPTIONAL name of the thing as a string or properties of the thing
     *   as key-value pairs in an associative array.
     */
    public function __construct(null|string|array $param = null)
    {
        if (is_string($param)) {
            $this->name = $param;
        } elseif (is_array($param) && !empty($param)) {
            foreach ($param as $key => $value) {
                $this->__set($key, $value);
            }
        }
    }

    /**
     * Reads data from inaccessible (protected or private) properties.
     *
     * @param string $name
     *   Name of the protected or private property.
     *
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        } else {
            return $this->{$name};
        }
    }

    /**
     * Writes data to inaccessible or non-existing properties.
     * 
     * @param string $name
     *   Name of the property to set.  This property name is first mapped
     *   to a method name, if the method exists.
     *
     * @param mixed $value
     *   Value of the property.
     *
     * @return void
     * 
     * @throws \LogicException
     *   Throws a logic exception if the `$name` can not be mapped to a setter
     *   (mutator) or a class property.
     */
    public function __set(string $name, mixed $value): void
    {
        $method = 'set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $this->$method($value);
        } elseif (property_exists($this, $name)) {
            $this->{$name} = $value;
        } else {
            throw new \LogicException(
                'Method ' . get_class($this) . '::' . $method . '() or property '
                . get_class($this) . '::$' . $name . ' does not exist.'
            );
        }
    }

    /**
     * Converts the object to a JSON-LD string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $result = json_encode($this, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        if (str_starts_with($result, '{"@context":"https://schema.org",')) {
            $result = explode(',', $result, 2);
            $result[1] = str_replace('"@context":"https://schema.org",', '', $result[1]);
            $result = '{"@context":"https://schema.org",' . $result[1];
        }

        return $result;
    }

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return array|string
     */
    public function jsonSerialize(): array|string
    {
        $result = ['@context' => 'https://schema.org'];

        $properties = $this->toArray();
        if (isset($properties['id'])) {
            $result['@id'] = $properties['id'];
            unset($properties['id']);
        }

        if (!empty($properties)) {
            foreach ($properties as $name => $value) {
                if ($value instanceof \JsonSerializable) {
                    $value = $value->jsonSerialize();
                    if (is_array($value) && isset($value['@context']) && $value['@context'] === 'https://schema.org') {
                        unset($value['@context']);
                    }
                    $properties[$name] = $value;
                }
            }
        }

        $class = new ReflectionClass($this);
        $result['@type'] = $class->getShortName();

        return $result + $properties;
    }

    /**
     * Sets an image.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\CMS\ImageObject|StoreCore\Types\URL|string $image
     *   Image as an `ImageObject` or some type of URL.
     *
     * @return void
     */
    public function setImage(UriInterface|ImageObject|URL|array|string $image): void
    {
        if ($image instanceof UriInterface) $image = (string) $image;
        if (is_string($image)) $image = new URL($image);
        $this->image = $image;
    }

    /**
     * Sets the URL.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string $url
     * @return void
     */
    public function setUrl(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL($url);
        $this->url = $url;
    }

    /**
     * Returns the object properties as an associative array.
     *
     * @param void
     * @return array
     */
    public function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }
}
