<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Medical entity.
 * 
 * The most generic type of entity related to health and the practice of medicine.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/MedicalEntity Schema.org type `MedicalEntity`
 * @version 0.2.0
 */
class MedicalEntity extends Thing
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|StoreCore\CRM\Organization
     *   If applicable, the `Organization` that officially recognizes this
     *   entity as part of its endorsed system of medicine.
     */
    public ?Organization $recognizingAuthority = null;
}
