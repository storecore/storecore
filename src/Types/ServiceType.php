<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Schema.org service type.
 *
 * Schema.org `@type` of `Service` in JSON-LD.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Service#subtypes More specific types of a `Service`
 * @version 27.0.0
 */
enum ServiceType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  Matches Schema.org version 27.0
     *   from May 20, 2024.
     */
    public const string VERSION = '27.0.0';

    case BankAccount = 'https://schema.org/BankAccount';
    case BroadcastService = 'https://schema.org/BroadcastService';
    case BrokerageAccount = 'https://schema.org/BrokerageAccount';
    case CableOrSatelliteService = 'https://schema.org/CableOrSatelliteService';
    case CreditCard = 'https://schema.org/CreditCard';
    case CurrencyConversionService = 'https://schema.org/CurrencyConversionService';
    case DepositAccount = 'https://schema.org/DepositAccount';
    case FinancialProduct = 'https://schema.org/FinancialProduct';
    case FoodService = 'https://schema.org/FoodService';
    case GovernmentService = 'https://schema.org/GovernmentService';
    case InvestmentFund = 'https://schema.org/InvestmentFund';
    case InvestmentOrDeposit = 'https://schema.org/InvestmentOrDeposit';
    case LoanOrCredit = 'https://schema.org/LoanOrCredit';
    case MortgageLoan = 'https://schema.org/MortgageLoan';
    case PaymentCard = 'https://schema.org/PaymentCard';
    case PaymentService = 'https://schema.org/PaymentService';
    case RadioBroadcastService = 'https://schema.org/RadioBroadcastService';
    case Service = 'https://schema.org/Service';
    case Taxi = 'https://schema.org/Taxi';
    case TaxiService = 'https://schema.org/TaxiService';
    case WebAPI = 'https://schema.org/WebAPI';
}
