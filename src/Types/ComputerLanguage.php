<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \JsonSerializable;

/**
 * Computer language.
 * 
 * This Schema.org type covers computer programming languages such as Scheme
 * and Lisp, as well as other language-like computer representations. Natural
 * languages are best represented with the `Language` type.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/ComputerLanguage Schema.org type `ComputerLanguage`
 * @version 1.0.0
 */
class ComputerLanguage extends Intangible implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
