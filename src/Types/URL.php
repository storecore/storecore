<?php

/**
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 */

declare(strict_types=1);

namespace StoreCore\Types;

use Psr\Http\Message\UriInterface;

/**
 * Uniform Resource Locator (URL).
 *
 * @api
 * @author  Ward van der Put <Ward.van.der.Put@storecore.org>
 * @see     https://schema.org/URL Schema.org type `URL`
 * @version 1.0.0
 */
class URL implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $value;
     *   Current value of the URL value object.
     */
    protected string $value;

    /**
     * Creates a URL (Uniform Resource Locator).
     *
     * @param Psr\Http\Message\UriInterface|string $url
     *   URL as a string or an object that implements the PSR-7 `UriInterface`.
     *
     * @throws \ValueError
     *   Throws a type error exception if a string is not a valid URL.
     *   `UriInterface` objects are not validated.
     *
     * @see https://www.php-fig.org/psr/psr-7/#35-psrhttpmessageuriinterface
     *   PSR-7: HTTP message interfaces `UriInterface`
     */
    public function __construct(UriInterface|string $url)
    {
        if (\is_string($url) && filter_var($url, \FILTER_VALIDATE_URL) === false) {
            throw new \ValueError('Invalid URL: ' . $url);
        }
        $this->value = (string) $url;
    }

    /**
     * Returns the URL as a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return mixed
     */
    public function jsonSerialize(): mixed
    {
        return $this->__toString();
    }
}
