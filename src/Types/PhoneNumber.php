<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Telephone number.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   0.1.0
 */
class PhoneNumber implements \JsonSerializable, \Stringable, ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var string $value
     *   Telephone number as a string.
     */
    private readonly string $value;

    /**
     * Creates a phone number.
     *
     * @param \Stringable|string|int $number
     *   Telephone number as a string or stringable object.  An integer
     *   is also accepted but converted to a string.
     *
     * @throws \ValueError
     *   Throws a value error if the telephone number is too long.
     *   StoreCore does not store phone numbers longer than 31 characters.
     */
    public function __construct(\Stringable|string|int $number)
    {
        $number = (string) $number;

        $number = trim($number);
        if (\strlen($number) > 31) throw new ValueError();

        $this->value = $number;
    }

    /**
     * Converts the value object to a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return string
     * @uses __toString()
     */
    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * Validates a telephone number.
     *
     * @param mixed $variable
     *   Variable to validate.
     *
     * @return bool
     *   Return `true` if the variable is an acceptable telephone number,
     *   otherwise `false`.
     */
    public static function validate(mixed $variable): bool
    {
        try {
            $variable = new PhoneNumber($variable);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
