<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \JsonSerializable;

/**
 * Aggregate rating.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/AggregateRating Schema.org type `AggregateRating`
 * @version 1.0.0-alpha.1
 */
class AggregateRating extends Rating implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|\JsonSerializable $itemReviewed
     *   The item that is being reviewed/rated. 
     */
    public ?JsonSerializable $itemReviewed = null;

    /**
     * @var null|int $ratingCount
     *   The count of total number of ratings.
     */
    public ?int $ratingCount = null;

    /**
     * @var null|int $reviewCount
     *   The count of total number of reviews.
     */
    public ?int $reviewCount = null;

    /**
     * Creates an aggregate rating.
     *
     * @param int|float|string|null $rating_value
     *   OPTIONAL rating value.
     * 
     * @param int|null $rating_count
     *   OPTIONAL total number of ratings.
     * 
     * @param int|null $review_count
     *   OPTIONAL total number of reviews.
     */
    public function __construct(
        int|float|string|null $rating_value = null,
        int|null $rating_count = null,
        int|null $review_count = null,
    ) {
        parent::__construct($rating_value);
        $this->ratingCount = $rating_count;
        $this->reviewCount = $review_count;
    }
}
