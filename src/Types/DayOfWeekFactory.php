<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

class_exists(DayOfWeek::class);

/**
 * Day of week factory.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/DayOfWeek Schema.org enumeration type `DayOfWeek`
 * @version 1.0.0-beta.1
 */
class DayOfWeekFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * Creates a day of the week enumeration member from a string.
     *
     * @param \Stringable|string $string
     * @return StoreCore\Types\DayOfWeek
     */
    public static function createFromString(\Stringable|string $string): DayOfWeek
    {
        try {
            $string = (string) $string;
            $string = strtolower($string);

            return match($string) {
                'm' => DayOfWeek::Monday,
                'mo' => DayOfWeek::Monday,
                'mon' => DayOfWeek::Monday,
                'monday' => DayOfWeek::Monday,

                't' => DayOfWeek::Tuesday,
                'tu' => DayOfWeek::Tuesday,
                'tue' => DayOfWeek::Tuesday,
                'tuesday' => DayOfWeek::Tuesday,

                'w' => DayOfWeek::Wednesday,
                'we' => DayOfWeek::Wednesday,
                'wed' => DayOfWeek::Wednesday,
                'wednesday' => DayOfWeek::Wednesday,

                't' => DayOfWeek::Thursday,
                'th' => DayOfWeek::Thursday,
                'thu' => DayOfWeek::Thursday,
                'thursday' => DayOfWeek::Thursday,

                'f' => DayOfWeek::Friday,
                'fr' => DayOfWeek::Friday,
                'fri' => DayOfWeek::Friday,
                'friday' => DayOfWeek::Friday,

                's' => DayOfWeek::Saturday,
                'sa' => DayOfWeek::Saturday,
                'sat' => DayOfWeek::Saturday,
                'saturday' => DayOfWeek::Saturday,

                'u' => DayOfWeek::Sunday,
                'su' => DayOfWeek::Sunday,
                'sun' => DayOfWeek::Sunday,
                'sunday' => DayOfWeek::Sunday,

                default => self::createFromDateTime(new \DateTimeImmutable($string))
            };
        } catch (\Exception $e) {
            throw new \ValueError($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Creates a day of the week enumeration member from a DateTime object.
     *
     * @param \DateTimeInterface $object
     * @return StoreCore\Types\DayOfWeek
     */
    public static function createFromDateTime(\DateTimeInterface $object): DayOfWeek
    {
        return self::createFromString($object->format('l'));
    }
}
