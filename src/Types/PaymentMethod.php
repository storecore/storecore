<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Payment method.
 *
 * @package StoreCore\Core
 * @version 0.2.0
 * @see     https://schema.org/PaymentMethod Schema.org type `PaymentMethod`
 */
class PaymentMethod extends Intangible
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';
}
