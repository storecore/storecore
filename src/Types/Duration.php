<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Duration.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/ISO_8601 ISO 8601
 * @see     https://schema.org/Duration Schema.org type Duration
 * @see     https://www.php.net/manual/en/class.dateinterval.php PHP class `DateInterval`
 * @version 1.0.0
 */
class Duration extends \DateInterval implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Returns the duration as a ISO 8601 string for serialization to JSON-LD.
     *
     * @param void
     * @return string
     * @uses Duration::__toString()
     */
    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * Converts the duration to an ISO 8601 string.
     *
     * @param void
     * @return string
     * @uses \DateInterval::format()
     *
     * @see https://stackoverflow.com/questions/33787039/format-dateinterval-as-iso8601
     *      Format DateInterval as ISO8601
     */
    public function __toString(): string
    {
        static $search = ['M0S', 'H0M', 'DT0H', 'M0D', 'P0Y', 'Y0M', 'P0M'];
        static $replace = ['M', 'H', 'DT', 'M', 'P', 'Y', 'P'];
        return rtrim(str_replace($search, $replace, $this->format('P%yY%mM%dDT%hH%iM%sS')), 'PT') ?: 'PT0S';
    }

    public static function createFromDateInterval(\DateInterval $interval): Duration|false
    {
        $datetime = 'P' . $interval->y . 'Y' . $interval->m . 'M' . $interval->d . 'D'
            . 'T' . $interval->h . 'H' . $interval->i . 'M' . $interval->s . 'S';
        return new Duration($datetime);
    }

    #[\ReturnTypeWillChange]
    public static function createFromDateString(string $datetime): Duration|false
    {
        $interval = parent::createFromDateString($datetime);
        return self::createFromDateInterval($interval);
    }
}
