<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \JsonSerializable;
use \Stringable;
use \ValueError;

use function \empty;
use function \is_string;
use function \preg_match;
use function \str_contains;
use function \str_replace;
use function \str_starts_with;
use function \strlen;
use function \strtolower;
use function \substr;
use function \trim;
use function \vsprintf;

/**
 * Universally unique identifier (UUID) as a self-validating value object.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://datatracker.ietf.org/doc/html/rfc4122
 * @see     https://en.wikipedia.org/wiki/Universally_unique_identifier
 * @version 1.0.0
 */
readonly class UUID implements JsonSerializable, Stringable, ValidateInterface, ValueInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $value
     *   Value of the universally unique identifier (UUID).
     */
    private string $value;

    /**
     * Creates a universally unique identifier (UUID).
     *
     * @param string $uuid
     *   Value of the universally unique identifier (UUID) as a case-insensitive
     *   string.  Enclosing braces `{…}` or a URN prefix are ignored.  Uppercase
     *   is converted to lowercase.
     *
     * @throws \ValueError
     *   Throws a value error exception on an invalid UUID.
     */
    public function __construct(string $uuid)
    {
        $uuid = trim($uuid, '{}');
        $uuid = strtolower($uuid);

        if (str_starts_with($uuid, 'urn:uuid:')) {
            $uuid = substr($uuid, 9);
        }

        if (strlen($uuid) === 32 && !str_contains($uuid, '-')) {
            $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split($uuid, 4));
        }

        if (
            strlen($uuid) !== 36
            || preg_match('/[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}/', $uuid) !== 1
        ) {
            throw new ValueError('Invalid UUID (universally unique identifier): ' . $uuid);
        }

        $this->value = $uuid;
    }

    /**
     * Returns the UUID as a string.
     *
     * @param void
     * @return string
     */
    final public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Returns the UUID as a string for JSON-LD.
     *
     * @param void
     * @return string
     */
    final public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * @inheritDoc
     */
    public static function validate(mixed $variable): bool
    {
        if (!is_string($variable) || empty($variable)) {
            return false;
        }

        try {
            $uuid = new UUID($variable);
            return true;
        } catch (ValueError $e) {
            return false;
        }
    }

    /**
     * Returns the UUID as a lowercase hexadecimal string.
     *
     * @param void
     * @return string
     */
    final public function valueOf(): string
    {
        return str_replace('-', '', $this->__toString());
    }
}
