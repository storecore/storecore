<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017, 2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \RangeException;
use \Random\Randomizer;

use function \chunk_split;
use function \empty;
use function \rtrim;
use function \strlen;
use function \substr;

/**
 * Activation Key.
 *
 * This data object class provides the logic for something that needs some kind
 * of “activation” by a customer or user.  It can be used for user input of
 * random strings like product keys for software, license keys, and coupon codes.
 *
 * @package StoreCore\Security
 * @version 1.0.0
 */
class ActivationKey implements \Stringable
{
    /**
     * @var string CHARACTER_SET
     *   String consisting of all ASCII characters used in a key.  To allow for
     *   case-insensitive user input, only uppercase letters are included.
     *   Not included are the 0 and O, 1 and I, 2 and Z, 5 and S, and 8 and B
     *   because these characters MAY be confusing to users.
     */
    public const string CHARACTER_SET = '34679ACDEFGHJKLMNPQRTUVWXY';

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var int $chunkLength
     * @var int $chunks
     * @var string $key
     * @var string $separator
     */
    private int $chunkLength = 4;
    private int $chunks = 3;
    private string $key;
    private string $separator = '-';

    /**
     * @param int $chuncks
     * @param int $chunck_length
     * @param string $separator
     */
    public function __construct(int $chuncks = 3, int $chunck_length = 4, string $separator = '-')
    {
        $this->setChunks($chuncks);
        $this->setChunkLength($chunck_length);
        $this->setSeparator($separator);
    }

    /**
     * Gets a formatted activation key with separators.
     *
     * @param void
     * @return string
     * @uses valueOf()
     */
    public function __toString(): string
    {
        $result = chunk_split($this->valueOf(), $this->chunkLength, $this->separator);
        $result = rtrim($result, $this->separator);
        return $result;
    }

    /**
     * Generate or regenerate a random key.
     *
     * @param void
     * @return string
     */
    public function randomize(): string
    {
        $randomizer = new Randomizer();
        $characters = $randomizer->shuffleBytes(self::CHARACTER_SET);
        $max = strlen($characters) - 1;

        $this->key = (string) null;
        $length = $this->chunks * $this->chunkLength;
        for ($i = 1; $i <= $length; $i++) {
            $this->key .= substr($characters, $randomizer->getInt(0, $max), 1);
        }

        return $this->valueOf();
    }

    /**
     * Sets the number of characters per group.
     *
     * @param int $number_of_characters
     * @return void
     * @throws \RangeException
     */
    public function setChunkLength(int $number_of_characters): void
    {
        if ($number_of_characters < 1) throw new RangeException();
        $this->chunkLength = $number_of_characters;
    }

    /**
     * Sets the number of character groups.
     *
     * @param int $chuncks
     * @return void
     * @throws \RangeException
     */
    public function setChunks(int $chuncks): void
    {
        if ($chuncks < 1) throw new RangeException();
        $this->chunks = $chuncks;
    }

    /**
     * Sets the character that separates chunks.
     *
     * @param string $separator
     * @return void
     */
    public function setSeparator(string $separator): void
    {
        $this->separator = $separator;
    }

    /**
     * Gets the activation key value.
     *
     * @param void
     * @return string
     * @uses randomize()
     */
    public function valueOf(): string
    {
        if (empty($this->key)) {
            $this->randomize();
        }
        return $this->key;
    }
}
