<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \Stringable;
use Psr\Http\Message\UriInterface;

use function \bin2hex;
use function \ctype_xdigit;
use function \empty;
use function \filter_var;
use function \function_exists;
use function \is_string;
use function \random_bytes;
use function \rtrim;
use function \sha1;
use function \strlen;
use function \strtolower;
use function \substr;
use function \uniqid;

use const \FILTER_VALIDATE_URL;

/**
 * SHA-1 cache key.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
readonly class CacheKey implements Stringable, ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $value
     *   Cache key SHA-1 hash value.
     */
    private string $value;

    /**
     * @param null|string|\Stringable|Psr\Http\Message\UriInterface $string
     *   OPTIONAL case-insensitive string.  If this parameter is not set,
     *   a random unique identifier (UID) will be generated.
     *
     * @return self
     */
    public function __construct(null|string|Stringable|UriInterface $string = null)
    {
        if ($string === null) {
            $string = uniqid(bin2hex(random_bytes(20)), true);
        }
        $this->set($string);
    }

    /**
     * Converts the value object to a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Derives the cache key from a string.
     *
     * @param string|\Stringable|Psr\Http\Message\UriInterface $string
     *   Case-insensitive string or stringable to convert to a cache key.
     *
     * @return void
     *
     * @throws StoreCore\Types\CacheKeyException
     *   Throws a `CacheKeyException` that implements the PSR-16
     *   `Psr\SimpleCache\InvalidArgumentException` interface on an empty string
     *   or an incomplete URL.
     */
    private function set(string|Stringable|UriInterface $string): void
    {
        if ($string instanceof UriInterface) {
            $url = $string;
            if (empty($url->getHost())) {
                $string = '';
            } else {
                $string = '//' . $url->getHost();
            }
            $string .= $url->getPath();
        }

        $string = (string) $string;
        $string = trim($string);
        if (function_exists('mb_strtolower')) {
            $string = mb_strtolower($string, 'UTF-8');
        } else {
            $string = strtolower($string);
        }

        if ($string === 'http://' || $string === 'https://') {
            throw new CacheKeyException();
        } elseif (substr($string, 0, 8) === 'https://') {
            $string = substr($string, 6);
        } elseif (substr($string, 0, 7) === 'http://') {
            $string = substr($string, 5);
        }

        if (empty($string)) {
            throw new CacheKeyException();
        }

        // Remove trailing slash from a valid URL
        if (filter_var('https:' . $string, FILTER_VALIDATE_URL)) { 
            $string = rtrim($string, '/');
        }

        $this->value = sha1($string, false);
    }

    /**
     * Validates a cache key.
     *
     * @param mixed $variable
     * @return bool
     */
    public static function validate(mixed $variable): bool
    {
        if ($variable instanceof CacheKey) {
            return true;
        }

        if (
            !is_string($variable)
            || strlen($variable) !== 40
            || !ctype_xdigit($variable)
            || strtolower($variable) !== $variable
        ) {
            return false;
        } else {
            return true;
        }
    }
}
