<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \InvalidArgumentException;
use \Stringable;

use function \ctype_digit;
use function \date;
use function \gmdate;
use function \is_int;
use function \is_string;
use function \random_int;
use function \str_pad;
use function \strlen;
use function \substr;

/**
 * Numeric payment reference.
 *
 * @package StoreCore\OML
 * @see     https://www.betaalvereniging.nl/betalingsverkeer/giraal-betalingsverkeer/wijzigingen-verwerking-acceptgirobetalingen/specificaties-betalingskenmerk/
 * @version 1.2.1
 */
class PaymentReference implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.2.1';

    /**
     * @var string $prefix
     *   Padding added before the reference number to create a number
     *   of 15 digits.  The constructor sets this prefix to the current date
     *   in ISO format `yyyymmdd` if no other prefix is provided.
     */
    private string $prefix;

    /**
     * @var string $transactionID
     *   Transaction identifier, usually a numeric string consisting of decimal
     *   digits.
     */
    private string $transactionID;

    /**
     * Constructs a transaction identifier value object.
     *
     * @param null|int|string|\Stringable $transaction_id
     *   OPTIONAL transaction identifier to set.  If omitted, a partially random
     *   transaction ID is generated consisting of the current Greenwich Mean
     *   Time (GMT) in `hhmmss` format plus a random single digit.
     *
     * @param null}int|string $prefix
     *   OPTIONAL integer or numeric string of digits to prepend to the 
     *   transaction ID.  If the `$prefix` is omitted, the current date in
     *   ISO format `yyyymmdd` is used.
     *
     * @uses setTransactionID()
     */
    public function __construct(
        null|int|string|Stringable $transaction_id = null,
        null|int|string $prefix = null
    ) {
        if ($prefix === null) {
            $prefix = date('Ymd');
        }
        $this->setPrefix($prefix);

        if ($transaction_id === null) {
            $transaction_id = gmdate('His') . random_int(0, 9);
        }
        $this->setTransactionID($transaction_id);
    }

    /**
     * Converts the data object to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the payment reference as a numeric string with 16 digits.
     */
    public function __toString(): string
    {
        // Add a date prefix.
        $number = $this->prefix . $this->transactionID;
        if (strlen($number) > 15) {
            $number = substr($number, -15);
        }

        // Calculate the check digit.
        $sum = 0;
        $sum += substr($number,  -1, 1) *  2;
        $sum += substr($number,  -2, 1) *  4;
        $sum += substr($number,  -3, 1) *  8;
        $sum += substr($number,  -4, 1) *  5;
        $sum += substr($number,  -5, 1) * 10;
        $sum += substr($number,  -6, 1) *  9;
        $sum += substr($number,  -7, 1) *  7;
        $sum += substr($number,  -8, 1) *  3;
        $sum += substr($number,  -9, 1) *  6;
        $sum += substr($number, -10, 1) *  1;
        $sum += substr($number, -11, 1) *  2;
        $sum += substr($number, -12, 1) *  4;
        $sum += substr($number, -13, 1) *  8;
        $sum += substr($number, -14, 1) *  5;
        $sum += substr($number, -15, 1) * 10;
        $check_digit = 11 - ($sum % 11);

        // Check digit 10 becomes 1 and 11 becomes 0.
        if ($check_digit == 10) {
            $check_digit = 1;
        } elseif ($check_digit == 11) {
            $check_digit = 0;
        }

        return $check_digit . $number;
    }


    /**
     * Sets a numeric prefix.
     *
     * @param int|string $number
     *   Number as an integer or a numeric string consisting of digits only.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the prefix is not an integer
     *   and not a numeric string consisting of decimal digits.
     */
    private function setPrefix(int|string $number): void
    {
        if (is_int($number)) {
            $number = (string) $number;
        }

        if (!is_string($number) || !ctype_digit($number)) {
            throw new InvalidArgumentException();
        }
        $this->prefix = $number;
    }

    /**
     * Sets the transaction ID.
     *
     * @param int|string|Stringable $transaction_id
     *   Integer or numeric string with a transaction identifier like an
     *   invoice number or order number.  If the ID consists of more than
     *   15 digits, it is truncated and only the last 15 digits are used.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the transaction ID is not an
     *   integer and not a numeric string consisting of decimal digits.
     */
    public function setTransactionID(int|string|Stringable $transaction_id): void
    {
        if (is_int($transaction_id) || ($transaction_id instanceof Stringable)) {
            $transaction_id = (string)$transaction_id;
        } elseif (!is_string($transaction_id) || !ctype_digit($transaction_id)) {
            throw new InvalidArgumentException();
        }

        // String length must be between 7 and 15 digits.
        if (strlen($transaction_id) < 7) {
            $transaction_id = str_pad($transaction_id, 7, '0', \STR_PAD_LEFT);
        } elseif (strlen($transaction_id) > 15) {
            $transaction_id = substr($transaction_id, -15);
        }

        $this->transactionID = $transaction_id;
    }
}
