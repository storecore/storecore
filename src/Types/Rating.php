<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\{Organization, Person};

/**
 * Rating.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Rating Schema.org type `Rating`
 * @version 1.0.0-alpha.1
 */
class Rating extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|null $author
     *   The author of this content or rating.  This property MAY be set
     *   with a string through the `setAuthor()` method.
     */
    public null|Organization|Person $author = null;

    /**
     * @var int|float|string $bestRating
     *   The highest value allowed in this rating system.  If `bestRating` is
     *   omitted, `5` is assumed in Schema.org, so `(int) 5` is used as the
     *   default.
     */
    public int|float|string $bestRating = 5;

    /**
     * @var null|string $ratingExplanation
     *   A short explanation (e.g. one to two sentences) providing background
     *   context and other information that led to the conclusion expressed in
     *   the `Rating`.  This is particularly applicable to ratings associated
     *   with “fact check” markup using `ClaimReview`.
     */
    public ?string $ratingExplanation = null;

    /**
     * @var int|float|string $ratingValue
     *   The rating of a `Thing`, for example a `Product`.
     */
    public int|float|string $ratingValue;

    /**
     * @var null|string $reviewAspect
     *   This `Review` or `Rating` is relevant to this part
     *   or facet of the `itemReviewed`.
     */
    public ?string $reviewAspect = null;

    /**
     * @var int|float|string $worstRating
     *   The lowest value allowed in this rating system.  Defaults to `1`.
     */
    public int|float|string $worstRating = 1;

    /**
     * Creates a rating.
     *
     * @param int|float|string|null $rating_value
     *   OPTIONAL rating value.
     */
    public function __construct(int|float|string $rating_value = null)
    {
        if ($rating_value !== null) $this->ratingValue = $rating_value;
    }

    /**
     * Sets the author of a rating.
     *
     * @param StoreCore\CRM\Organization|StoreCore\CRM\Person|string $author
     *   The `author` of a `Rating` must be an `Organization` or `Person`.
     *   If a string is supplied, we asume this to be the `name` of a `Person`.
     *
     * @return void
     * 
     * @uses StoreCore\CRM\Person::__construct()
     */
    public function setAuthor(Organization|Person|string $author): void
    {
        if (\is_string($author)) $author = new Person($author);
        $this->author = $author;
    }
}
