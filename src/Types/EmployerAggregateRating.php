<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Employer aggregate rating.
 *
 * An aggregate rating of an `Organization` related to its role as an employer.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/EmployerAggregateRating Schema.org type `EmployerAggregateRating`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/employer-rating
 * @version 1.0.0-alpha.1
 */
class EmployerAggregateRating extends AggregateRating implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';
}
