<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2018, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \Random\Randomizer;
use StoreCore\SingletonInterface;

use function \substr;

/**
 * Form token.
 *
 * @package StoreCore\Security
 * @version 0.2.0
 */
class FormToken implements SingletonInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    // Disable object instantiation and cloning
    private function __construct() {}
    private function __clone() {}

    /**
     * Gets a random ASCII token.
     *
     * @param void
     *
     * @return string
     *   Returns a random string consisting of digits (0–9), lowercase ASCII
     *   characters (a–z), and uppercase ASCII characters (A–Z).  The current
     *   default string length is 512 characters but this MAY be changed in
     *   the future.
     */
    public static function getInstance(): string
    {
        $randomizer = new Randomizer();
        $characters = $randomizer->shuffleBytes('0123456789abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWZYZ');
        $token = (string) null;
        for ($i = 1; $i <= 512; $i++) {
            $token .= substr($characters, $randomizer->getInt(0, 61), 1);
        }
        return $token;
    }
}
