<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Schema.org gender type.
 *
 * A backed enumeration of genders.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/GenderType Schema.org enumeration type `GenderType`
 * @version 23.0.0
 */
enum GenderType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '23.0.0';

    case Female = 'https://schema.org/Female';
    case Male = 'https://schema.org/Male';
}
