<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Value interface.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
interface ValueInterface
{
    /**
     * Returns a value object as a simple primitive.
     *
     * @param void
     *
     * @return bool|int|float|string
     *   Returns a primitive value as a boolean, integer, float, or string.
     */
    public function valueOf(): bool|int|float|string;
}
