<?php

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\AbstractCacheException;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionInterface;

class CacheKeyException extends AbstractCacheException implements InvalidArgumentExceptionInterface {}
