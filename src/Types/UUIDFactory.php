<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \Exception;
use \Stringable;
use StoreCore\Registry;

use function \bin2hex;
use function \chr;
use function \hex2bin;
use function \mb_strtolower;
use function \md5;
use function \ord;
use function \random_bytes;
use function \str_replace;
use function \str_split;
use function \uniqid;
use function \vsprintf;

/**
 * Universally unique identifier (UUID) factory.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class UUIDFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\Registry $registry
     */
    private ?Registry $registry;

    /**
     * Creates a UUID factory.
     *
     * @param null|StoreCore\Registry $registry
     *   OPTIONAL StoreCore registry.
     */
    public function __construct(?Registry $registry = null)
    {
        $this->registry = $registry;
    }

    /**
     * Creates a UUIDv4 from an MD5 hash.
     *
     * @param \Stringable|string $string Case-insensitive text.
     * @return StoreCore\Types\UUID
     */
    public static function createFromString(Stringable|string $string): UUID
    {
        $string = mb_strtolower((string) $string);
        $uuid = md5((string) $string, false);
        $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split($uuid, 4));
        $uuid[14] = '4';
        $uuid[19] = ['8', '9', 'a', 'b', '8', '9', 'a', 'b', 'c' => '8', 'd' => '9', 'e' => 'a', 'f' => 'b'][$uuid[19]] ?? $uuid[19];
        return new UUID($uuid);
    }

    /**
     * Creates a UUID value object.
     *
     * @param void
     * @return StoreCore\Types\UUID
     */
    public function createUUID(): UUID
    {
        $uuid = $this->fetchUUID();
        return $uuid !== false ? $uuid : self::randomUUID();
    }

    /**
     * Fetches a new random UUID from the database.
     *
     * @param void
     *
     * @return StoreCore\Types\UUID|false
     *   Returns a `UUID` value object if one could be created through
     *   the default database or `false` on database errors.
     */
    private function fetchUUID(): UUID|false
    {
        if (
            null === $this->registry
            || true !== $this->registry->has('Database')
        ) {
            return false;
        }

        try {
            $statement = $this->registry->get('Database')->query('SELECT UUID()');
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            return new UUID($result);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Returns a “max” UUID with all bits set to one.
     * 
     * @param void
     * @return @return StoreCore\Types\UUID
     * @see https://en.wikipedia.org/wiki/Universally_unique_identifier#Special_UUIDs
     */
    public static function maxUUID(): UUID
    {
        return new UUID('ffffffff-ffff-ffff-ffff-ffffffffffff');
    }

    /**
     * Returns a “nil” UUID with all bits set to zero.
     *
     * @param void
     * @return @return StoreCore\Types\UUID
     * @see https://en.wikipedia.org/wiki/Universally_unique_identifier#Special_UUIDs
     */
    public static function nilUUID(): UUID
    {
        return new UUID('00000000-0000-0000-0000-000000000000');
    }

    /**
     * Generates a pseudo-random UUID.
     *
     * This method uses a time-based unique ID to generate UUIDs, so the UUIDs
     * that are generated at about the same time start with the same characters.
     * This MAY improve access to closely related objects.
     *
     * @param void
     * @return StoreCore\Types\UUID
     * @see https://www.php.net/uniqid
     */
    public static function pseudoRandomUUID(): UUID
    {
        $uuid = uniqid('', true);
        $uuid = str_replace('.', '', $uuid);
        $uuid .= bin2hex(random_bytes(5));

        $uuid = hex2bin($uuid);
        $uuid[6] = chr(ord($uuid[6]) & 0x0f | 0x40);
        $uuid[8] = chr(ord($uuid[8]) & 0x3f | 0x80);
        $uuid = bin2hex($uuid);

        return new UUID($uuid);
    }

    /**
     * Generates a random UUID version 4.
     *
     * This method mimics the structure of a UUID string using the PHP function
     * `random_bytes()` to allow for setting and resetting random UUIDs without
     * a database connection.
     *
     * @param void
     * @return StoreCore\Types\UUID
     */
    public static function randomUUID(): UUID
    {
        $uuid = random_bytes(16);
        $uuid[6] = chr(ord($uuid[6]) & 0x0f | 0x40);
        $uuid[8] = chr(ord($uuid[8]) & 0x3f | 0x80);
        $uuid = bin2hex($uuid);
        return new UUID($uuid);
    }
}
