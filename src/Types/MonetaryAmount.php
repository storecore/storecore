<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \ValueError;

use StoreCore\Currency;
use StoreCore\Registry;
use StoreCore\Database\Currencies;

use function \is_string;
use function \strtoupper;
use function \trim;

/**
 * Monetary amount.
 *
 * It is recommended to use `MonetaryAmount` to describe independent amounts of
 * money such as a salary, credit card limits, etc.  Use `PriceSpecification` or
 * `UnitPriceSpecification` to describe the price of an `Offer`, `Invoice`, etc.
 * 
 * @package StoreCore\Core
 * @see     https://schema.org/MonetaryAmount Schema.org type `MonetaryAmount`
 * @version 0.2.2
 */
class MonetaryAmount extends StructuredValue
{
    use ValidThroughTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.2';

    /**
     * @var null|string $currency
     *   The currency in which the monetary amount is expressed.
     *   Defaults to 'EUR' for Euro.
     */
    protected ?string $currency = 'EUR';

    /**
     * @var int|float|null $value
     *   The value of the quantitative value or property value node.
     *   For `QuantitativeValue` and `MonetaryAmount`, the recommended
     *   Schema.org type for values is `Number`.
     */
    public int|float|null $value = null;

    /**
     * Creates a monetary amount.
     *
     * @param null|int|float $value
     * @param null|string $currency
     */
    public function __construct(null|int|float $value = null, null|string $currency = 'EUR')
    {
        $this->value = $value;
        $this->setCurrency($currency);
    }

    /**
     * @inheritDoc
     */
    public function __get(string $name): mixed
    {
        if ($name === 'currency') {
            return $this->currency;
        }
        return parent::__get($name);
    }

    /**
     * @inheritDoc
     */
    public function __set(string $name, mixed $value): void
    {
        if ($name === 'currency') {
            $this->setCurrency($value);
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * Sets the currency.
     *
     * @param StoreCore\Currency|string $currency
     *   The currency in which the monetary amount is expressed.
     *
     * @uses StoreCore\Currency::__construct()
     * @uses StoreCore\Database\Currencies::has()
     * @uses StoreCore\Database\Currencies::get()
     */
    public function setCurrency(Currency|string $currency): void
    {
        if (is_string($currency)) {
            $currency = strtoupper(trim($currency));
            $registry = Registry::getInstance();
            $repository = new Currencies($registry);
            if ($repository->has($currency)) {
                $currency = $repository->get($currency);
            } else {
                throw new ValueError('Unknown currency: ' . $currency, time());
            }
        }
        $this->currency = $currency->code;
    }
}
