<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Time.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://en.wikipedia.org/wiki/ISO_8601 ISO 8601
 * @see       https://schema.org/Time Schema.org data type Time
 * @version   1.0.0
 */
class Time extends DateTime
implements \DateTimeInterface, \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * Converts the Time to an ISO 8601 string.
     *
     * @param void
     *
     * @return string
     *   String with hours and minutes in 24-hour format with leading zeros.
     *
     * @uses \DateTime::format()
     */
    public function __toString(): string
    {
        return $this->format('H:i');
    }
}
