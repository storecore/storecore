<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \DateTimeInterface;
use \DateTimeZone;

use function \is_string;
use function \strlen;

/**
 * Schema.org validFrom property.
 *
 * The Schema.org `validFrom` property is the date when an item becomes valid.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/validFrom Schema.org property `validFrom`
 * @see     https://developers.google.com/search/docs/advanced/structured-data/event
 * @version 1.0.0
 */
trait ValidFromTrait
{
    /**
     * @var StoreCore\Types\Date|StoreCore\Types\DateTime|null $validFrom
     *   The date after when the item is not valid.  For example the end of an
     *   offer, salary period, or a period of opening hours.
     */
    public null|Date|DateTime $validFrom = null;

    /**
     * Sets the `validFrom` date.
     * 
     * @param \DateTimeInterface|string $datetime
     * @return void
     */
    public function setValidFrom(DateTimeInterface|string $datetime = 'now'): void
    {
        if (is_string($datetime)) {
            if (strlen($datetime) === 10) {
                $datetime = Date::createFromFormat('Y-m-d', $datetime, new DateTimeZone('UTC'));
            } else {
                $datetime = new DateTime($datetime, new DateTimeZone('UTC'));
            }
        }

        if ($datetime instanceof Date || $datetime instanceof DateTime) {
            $this->validFrom = $datetime;
        } else {
            $this->validFrom = DateTime::createFromInterface($datetime);
        }
    }
}
