<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * DateTime.
 * 
 * The `StoreCore\Types\DateTime` class is an extension of the generic PHP
 * `DateTime` that displays dates with times as strings in ISO 8601 format.
 * The subclass `StoreCore\Types\Date` is intended for dates without times.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://en.wikipedia.org/wiki/ISO_8601 ISO 8601
 * @see       https://schema.org/DateTime Schema.org data type
 * @version   1.0.0
 */
class DateTime extends \DateTime
implements \DateTimeInterface, \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * Converts the DateTime to an ISO 8601 string.
     *
     * @param void
     * @return string
     * @uses \DateTime::format()
     * @see https://www.php.net/manual/en/datetime.format.php
     * @see https://www.php.net/manual/en/class.datetimeinterface.php#datetime.constants.types
     */
    public function __toString(): string
    {
        return $this->format(\DateTimeInterface::ATOM);
    }

    /**
     * Creates a JSON representation of the DateTime.
     *
     * @param void
     * @return string
     * @uses __toString()
     */
    final public function jsonSerialize(): string
    {
        return $this->__toString();
    }
}
