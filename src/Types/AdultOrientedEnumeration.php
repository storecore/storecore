<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022~2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Adult-oriented considerations.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/AdultOrientedEnumeration Schema.org `AdultOrientedEnumeration`
 * @version 23.0.0
 */
enum AdultOrientedEnumeration: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org v23.0
     *   from 17 October 2023.
     */
    public const string VERSION = '23.0.0';

    case AlcoholConsideration = 'https://schema.org/AlcoholConsideration';
    case DangerousGoodConsideration = 'https://schema.org/DangerousGoodConsideration';
    case HealthcareConsideration = 'https://schema.org/HealthcareConsideration';
    case NarcoticConsideration = 'https://schema.org/NarcoticConsideration';
    case ReducedRelevanceForChildrenConsideration = 'https://schema.org/ReducedRelevanceForChildrenConsideration';
    case SexualContentConsideration = 'https://schema.org/SexualContentConsideration';
    case TobaccoNicotineConsideration = 'https://schema.org/TobaccoNicotineConsideration';
    case UnclassifiedAdultConsideration = 'https://schema.org/UnclassifiedAdultConsideration';
    case ViolenceConsideration = 'https://schema.org/ViolenceConsideration';
    case WeaponConsideration = 'https://schema.org/WeaponConsideration';

    /**
     * Maps a tiny integer to an enumeration member.
     *
     * @param int $key
     * @return static
     */
    public static function fromInteger(int $key): static
    {
        if ($key < 0 || $key > 9) {
            throw new \ValueError();
        }

        return match($key) {
             0 => static::UnclassifiedAdultConsideration,
             1 => static::AlcoholConsideration,
             2 => static::DangerousGoodConsideration,
             3 => static::HealthcareConsideration,
             4 => static::NarcoticConsideration,
             5 => static::ReducedRelevanceForChildrenConsideration,
             6 => static::SexualContentConsideration,
             7 => static::TobaccoNicotineConsideration,
             8 => static::ViolenceConsideration,
             9 => static::WeaponConsideration,
        };
    }
}
