<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Item list.
 * 
 * An _item list_ is a list of items of any sort — for example, “Top 10 Movies
 * About Weathermen”, or “Top 100 Party Songs”.  Not to be confused with
 * HTML lists, which are often used only for formatting.
 * 
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://schema.org/ItemList Schema.org type ItemList
 * @version   0.1.0
 */
class ItemList extends Intangible 
implements \ArrayAccess, \Countable, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';


    /**
     * @var array|null $itemListElement
     *   Container for the list elements.  For `itemListElement` values,
     *   you can use simple strings (e.g. “Peter”, “Paul”, “Mary”),
     *   existing entities, or use `ListItem`.
     */
    protected array|null $itemListElement = null;

    /**
     * @var StoreCore\Types\ItemListOrderType|string|null $itemListOrder
     *   Type of ordering (e.g. ascending, descending, unordered).  Although
     *   this property MAY be set to any string, we recommend using one of the
     *   members from the `ItemListOrderType` enumeration.
     */
    public ItemListOrderType|string|null $itemListOrder = null;

    /**
     * @var int|null $numberOfItems
     *   The number of items in an `ItemList`.  Note that some descriptions 
     *   might not fully describe all items in a list (e.g., multi-page
     *   pagination); in such cases, the `numberOfItems` would be for the
     *   entire list.
     */
    public ?int $numberOfItems = null;

    /**
     * Counts all items in the list.
     *
     * @param void
     * @return int
     * @see https://www.php.net/manual/en/countable.count
     */
    public function count(): int
    {
        if (empty($this->itemListElement)) return 0;
        $this->numberOfItems = count($this->itemListElement);
        return $this->numberOfItems;
    }

    /**
     * Checks whether or not an offset exists.
     *
     * @param mixed $offset
     * @return bool
     * @see https://www.php.net/manual/en/arrayaccess.offsetexists.php
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->itemListElement[$offset]);
    }

    /**
     * Returns the value at specified offset.
     *
     * @param mixed $offset
     * @return void
     * @see https://www.php.net/manual/en/arrayaccess.offsetget.php
     */
    public function offsetGet(mixed $offset): mixed
    {
        return isset($this->itemListElement[$offset]) ? $this->itemListElement[$offset] : null;
    }

    /**
     * Assigns a value to the specified offset.
     *
     * @param mixed $offset
     * @param mixed $value
     * @see https://www.php.net/manual/en/arrayaccess.offsetset
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if ($offset === null) {
            $this->itemListElement[] = $value;
        } else {
            $this->itemListElement[$offset] = $value;
        }
    }

    /**
     * Unsets an offset.
     *
     * @param mixed $offset
     * @return void
     * @see https://www.php.net/manual/en/arrayaccess.offsetunset
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->itemListElement[$offset]);
    }

    /**
     * Sets the item list element or elements.
     *
     * @param array|string|StoreCore\Types\Thing $value
     * @return void
     */
    public function setItemListElement(array|string|Thing $value): void
    {
        if (\is_string($value) || $value instanceof Thing) {
            $value = array($value);
        }

        if ($this->itemListElement === null) {
            $this->itemListElement = $value;
        } else {
            $this->itemListElement = array_merge($this->itemListElement, $value);
        }
    }
}
