<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Speakable specification.
 *
 * A `SpeakableSpecification` indicates (typically via `xpath` or
 * `cssSelector`) sections of a document that are highlighted as particularly
 * speakable.  Instances of this type are expected to be used primarily
 * as values of the `speakable` property.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/SpeakableSpecification Schema.org type `SpeakableSpecification`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/speakable
 * @version 0.1.0
 */
class SpeakableSpecification extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|string $cssSelector
     *   A CSS selector, e.g. of a `SpeakableSpecification` or `WebPageElement`.
     */
    public ?string $cssSelector = null;

    /**
     * @var null|string $cssSelexpathctor
     *   An XPath, e.g. of a `SpeakableSpecification` or `WebPageElement`.
     */
    public ?string $xpath = null;
}
