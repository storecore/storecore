<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Entry point.
 *
 * An entry point within some Web-based protocol.  Instances of an `EntryPoint`
 * MAY appear as a value for the `target` property of `Action` objects.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://schema.org/EntryPoint Schema.org type EntryPoint
 * @version   0.2.0
 */
class EntryPoint extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';


    /**
     * @var string|null $httpMethod
     *   An HTTP method that specifies the appropriate HTTP method for a request
     *   to an HTTP EntryPoint.  Values are capitalized strings as used in HTTP.
     */
    protected ?string $httpMethod = null;

    /**
     * @var string|null $urlTemplate
     *   An URI Template (RFC 6570) that will be used to construct the `target`
     *   of the execution of an `Action`.
     */
    public ?string $urlTemplate = null;


    /**
     * Sets the HTTP method.
     *
     * This setter is a guard that assures the `EntryPoint.httpMethod`
     * HTTP method is set in uppercase.  It does not validate the method.
     *
     * @param string $http_method
     * @return void
     */
    public function setHttpMethod(string $http_method): void
    {
        $this->httpMethod = strtoupper($http_method);
    }
}
