<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

class_exists(Thing::class);
class_exists(Intangible::class);
class_exists(Service::class);

/**
 * Web API service.
 *
 * @api
 * @package   StoreCore\Core
 * @see       https://schema.org/WebAPI Schema.org type `WebAPI`
 * @version   0.1.0
 */
final class WebAPI extends Service implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var StoreCore\Types\CreativeWork|StoreCore\Types\URL|null $documentation
     *   Further documentation describing the Web API in more detail.
     */
    public CreativeWork|URL|null $documentation = null;
}
