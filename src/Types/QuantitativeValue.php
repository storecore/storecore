<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use Psr\Http\Message\UriInterface;

/**
 * Quantitative value.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/QuantitativeValue Schema.org type `QuantitativeValue`
 * @see     https://developers.google.com/gmail/markup/reference/types/QuantitativeValue
 * @version 1.0.0
 */
class QuantitativeValue extends StructuredValue implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var int|float|null $maxValue
     *   The upper value of some characteristic or property.
     */
    public null|int|float $maxValue;

    /**
     * @var int|float|null $minValue
     *   The lower value of some characteristic or property.
     */
    public null|int|float $minValue;

    /**
     * @var null|string $unitCode
     *   The unit of measurement given using the UN/CEFACT Common Code
     *   (as an uppercase string of 2 or 3 characters).
     */
    public ?string $unitCode;

    /**
     * @var bool|int|float|string|StoreCore\Types\StructuredValue|null $value
     *   The value of the quantitative value or property value node.  For
     *   `QuantitativeValue` and `MonetaryAmount`, the recommended type for
     *   values is `int` or `float`; for `PropertyValue`, it can be `string`, 
     *   `int`, `float`, `bool`, or `StructuredValue`.
     */
    public null|bool|int|float|string|StructuredValue $value;

    /**
     * @var null|DefinedTerm|PropertyValue|QuantitativeValue|StructuredValue|string $valueReference
     *   A secondary value that provides additional information on the original
     *   value, e.g. a reference temperature or a type of measurement.  Note that
     *   e-mail markup for Gmail only accepts `Enumaration` and `StructuredValue`.
     */
    public null|DefinedTerm|PropertyValue|QuantitativeValue|StructuredValue|string $valueReference = null;

    /**
     * Creates a quantitive value.
     *
     * @param mixed $value
     *   Value of the quantitive value.
     *
     * @param null|CommonCode $unitCode
     *   OPTIONAL unit of measurement of the quantitive value.
     *
     * @param null|int|float $minValue
     *   OPTIONAL numeric minimum value.
     *
     * @param null|int|float $maxValue
     *   OPTIONAL numeric maximum value.
     */
    public function __construct(
        mixed $value = null,
        null|CommonCode|string $unitCode = null,
        null|int|float $minValue = null,
        null|int|float $maxValue = null,
    ) {
        $this->setRange($minValue, $maxValue);
        $this->setValue($value);
        $this->setUnitCode($unitCode);
    }

    /**
     * Sets the minimum and/or maximum value.
     *
     * @param null|int|float $minValue
     *   The lower value of some characteristic or property.
     *
     * @param null|int|float $maxValue
     *   The upper value of some characteristic or property.
     */
    public function setRange(
        null|int|float $minValue = null,
        null|int|float $maxValue = null
    ): void {
        $this->minValue = $minValue;
        $this->maxValue = $maxValue;
    }

    /**
     * Sets the unit code for the unit of measurement.
     *
     * @param StoreCore\Types\CommonCode $unitCode
     *   Unit code for a standard unit of measurement as a UN/CEFACT Common Code.
     *
     * @return void
     */
    public function setUnitCode(null|CommonCode|string $unitCode): void
    {
        if (\is_string($unitCode)) {
            $unitCode = new CommonCode($unitCode);
        }

        if ($unitCode instanceof CommonCode) {
            $this->unitCode = $unitCode->value;
        } else {
            $this->unitCode = null;
        }
    }

    /**
     * Sets the quantitive value.
     *
     * @param mixed $value
     *   The actual value of the quantitative value or property value node.
     *
     * @return void
     */
    public function setValue(mixed $value): void
    {
        $this->value = $value;
    }
}
