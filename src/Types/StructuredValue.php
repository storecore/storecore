<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Structured value.
 *
 * Structured values are used when the value of a property has a more complex
 * structure than simply being a textual value or a reference to another thing.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://schema.org/StructuredValue Schema.org type `StructuredValue`
 * @version   1.0.0
 */
class StructuredValue extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
