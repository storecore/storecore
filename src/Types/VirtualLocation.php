<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Schema.org virtual location.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/VirtualLocation Schema.org type `VirtualLocation`
 * @version 1.0.0
 */
class VirtualLocation extends Intangible
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
