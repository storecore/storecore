<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use Psr\Link\LinkInterface;
use Psr\Http\Message\UriInterface;

interface_exists(LinkInterface::class);
interface_exists(UriInterface::class);

/**
 * PSR-13 compliant readable link object.
 *
 * @package StoreCore\Core
 * @version 0.3.0
 *
 * @see https://www.w3.org/TR/html5/links.html
 *      Links in HTML 5.2
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
 *      <link>: The External Resource Link element
 *
 * @see https://www.php-fig.org/psr/psr-13/
 *      PSR-13: Link definition interfaces
 *
 * @see https://evertpot.com/whats-in-a-link/
 *      What’s a link?, by Evert Pot
 *
 * @see https://www.iana.org/assignments/link-relations/link-relations.xhtml#link-relations-1
 *      Link Relation Types – IANA
 */
class Link implements LinkInterface, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var array $attributes
     *   A key-value list of attributes, where the key is a string and the
     *   value is either a PHP primitive or an array of PHP strings.
     */
    protected array $attributes = [];

    /**
     * @var string $hypertextReference
     *   The address or location this link is pointing to.  In this StoreCore
     *   implementation a link defaults to '.' assuming that an object in
     *   hypertext initially points to its own relative location if no other
     *   location is explicitly set.
     */
    protected string $hypertextReference = '.';

    /**
     * @var bool $isTemplated
     *   `true` if this link object is templated, otherwise `false` (default).
     */
    protected bool $isTemplated = false;

    /**
     * @var array $relations
     *   Zero or more relationship types for a link, expressed as an array of
     *   strings.
     */
    protected array $relations = [];

    /**
     * Constructs a link object.
     *
     * @param Psr\Http\Message\UriInterface|string|null $href
     *   Optional address or URI location of this link.
     *
     * @param string|array|null $rel
     *   Optional relation type of this link as a string or an array with
     *   multiple strings.
     *
     * @param array|null $attributes
     *   Optional additional attributes of this link as an array.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the `$href` parameter is not
     *   a string and cannot be converted to a string.  This method does not
     *   test if the `$href` is a valid URL nor if it actually exists.
     */
    public function __construct(
        UriInterface|string|null $href = null,
        string|array|null $rel = null,
        array|null $attributes = null
    ) {
        if ($href !== null) {
            if ($href instanceof UriInterface) {
                $href = (string) $href;
            }
            if (\is_string($href)) {
                $this->setAttribute('href', $href);
            } else {
                throw new \InvalidArgumentException();
            }
        }

        if ($rel !== null) {
            $this->setRel($rel);
        }

        if ($attributes !== null) {
            foreach ($attributes as $name => $value) {
                $this->setAttribute($name, $value);
            }
        }
    }

    /**
     * Generic property setter.
     *
     * @param string $name
     *   Name of the property to set.
     *
     * @param mixed $value
     *   Value of the property to set.
     *
     * @return void
     *
     * @uses setAttribute()
     */
    public function __set(string $name, $value): void
    {
        $this->setAttribute($name, $value);
    }

    /**
     * Converts the link object to an HTML <link> tag.
     *
     * @param void
     *
     * @return string
     *   Converts this Link data object to an HTML `<link>` resource link.
     *
     * @uses getAttributes()
     */
    public function __toString(): string
    {
        $html = '<link';
        foreach ($this->getAttributes() as $attribute => $value) {
            $html .= ' ' . $attribute;
            $value = (string) $value;
            if ($value !== '') $html .= '="' . $value . '"';
        }
        $html .= '>';
        return $html;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes(): array
    {
        $attributes = $this->attributes;
        if (!empty($this->getHref())) {
            $attributes['href'] = $this->getHref();
        }
        if (!empty($this->getRels())) {
            $attributes['rel'] = implode(' ', $this->getRels());
        }
        ksort($attributes);
        return $attributes;
    }

    /**
     * @inheritDoc
     */
    public function getHref(): string
    {
        return $this->hypertextReference;
    }

    /**
     * @inheritDoc
     */
    public function getRels(): array
    {
        return $this->relations;
    }

    /**
     * @inheritDoc
     */
    public function isTemplated(): bool
    {
        return $this->isTemplated;
    }

    /**
     * Generic property setter.
     *
     * @param string $name
     *   Case-insensitive name of the attribute to set.
     *
     * @param mixed $value
     *   Value of the attribute to set.  This parameter MAY explicitly be set
     *   to an empty string for HTML link attributes without values, like
     *   `crossorigin` in conjunction with `rel="preconnect"`.
     *
     * @return void
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute Element.setAttribute()
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the attribute name is not
     *   a string or an empty string.
     */
    public function setAttribute(string $name, $value): void
    {
        if (!\is_string($name)) {
            throw new \InvalidArgumentException();
        }

        $name = trim($name);
        if (empty($name)) {
            throw new \InvalidArgumentException();
        }

        $name = strtolower($name);
        if ($name === 'href') {
            $this->hypertextReference = $value;
        } elseif ($name === 'rel') {
            $this->setRel($value);
        } else {
            $this->attributes[$name] = $value;
        }
    }

    /**
     * Sets the rel attribute.
     *
     * @param string|array $rel
     *   The HTML `rel` attribute for link relations as a string or an array
     *   of strings.
     *
     * @return void
     */
    private function setRel($rel): void
    {
        if (\is_string($rel)) {
            $rel = trim($rel);
            $rel = preg_replace('!\s+!', ' ', $rel);
            $rel = explode(' ', $rel);
        }

        if (!\is_array($rel)) {
            throw new \InvalidArgumentException();
        }

        foreach ($rel as $relation) {
            if (!\is_string($relation)) {
                throw new \InvalidArgumentException();
            }
            $this->relations[] = strtolower($relation);
        }
        $this->relations = array_unique($this->relations);
    }
}
