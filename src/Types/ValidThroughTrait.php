<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \DateTimeInterface;

use function \is_string;
use function \strlen;

/**
 * Schema.org validThrough property.
 *
 * The Schema.org `validThrough` property is a date after when an item is no
 * longer valid, for example the end of an offer or salary period.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/validThrough Schema.org property `validThrough`
 * @see     https://en.wikipedia.org/wiki/ISO_8601 ISO 8601
 * @version 1.0.0
 */
trait ValidThroughTrait
{
    /**
     * @var null|StoreCore\Types\Date|StoreCore\Types\DateTime $validThrough
     *   The date after when the item is not valid.  For example the end of an
     *   offer, salary period, or a period of opening hours.
     */
    public null|Date|DateTime $validThrough = null;

    /**
     * Sets the `validThrough` date.
     * 
     * @param \DateTimeInterface|string $datetime
     * @return void
     */
    public function setValidThrough(DateTimeInterface|string $datetime = 'now'): void
    {
        if (is_string($datetime)) {
            if (strlen($datetime) === 10) {
                $datetime = Date::createFromFormat('Y-m-d', $datetime, new DateTimeZone('UTC'));
            } else {
                $datetime = new DateTime($datetime, new DateTimeZone('UTC'));
            }
        }

        if ($datetime instanceof Date || $datetime instanceof DateTime) {
            $this->validThrough = $datetime;
        } else {
            $this->validThrough = DateTime::createFromInterface($datetime);
        }
    }
}
