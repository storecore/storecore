<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Business function.
 *
 * The business function specifies the type of activity or access (i.e., the
 * bundle of rights) offered by an `Organization` or `Person` through an `Offer`.
 * Typical are sell, rental or lease, maintenance or repair, manufacture or
 * produce, recycle or dispose, engineering or construction, or installation.
 * Proprietary specifications of access rights are also instances of this class.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/BusinessFunction Schema.org enumeration type `BusinessFunction`
 * @see     https://schema.org/businessFunction Schema.org property `businessFunction`
 * @version 23.0.0
 */
enum BusinessFunction: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org
     *   version 23.0 from 17 October 2023.
     */
    public const string VERSION = '23.0.0';

    case Buy                      = 'http://purl.org/goodrelations/v1#Buy';
    case ConstructionInstallation = 'http://purl.org/goodrelations/v1#ConstructionInstallation';
    case Dispose                  = 'http://purl.org/goodrelations/v1#Dispose';
    case LeaseOut                 = 'http://purl.org/goodrelations/v1#LeaseOut';
    case Maintain                 = 'http://purl.org/goodrelations/v1#Maintain';
    case ProvideService           = 'http://purl.org/goodrelations/v1#ProvideService';
    case Repair                   = 'http://purl.org/goodrelations/v1#Repair';
    case Sell                     = 'http://purl.org/goodrelations/v1#Sell';
}
