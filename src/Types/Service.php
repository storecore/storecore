<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CRM\Audience;
use StoreCore\CRM\{Organization, Person};

class_exists(Thing::class);
class_exists(Intangible::class);
class_exists(Audience::class);
class_exists(Organization::class);
class_exists(Person::class);

/**
 * Service.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Service Schema.org type `Service`
 * @version 0.2.0
 */
class Service extends Intangible
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\CRM\Audience $audience
     *   An intended `Audience`, i.e. a group for whom something was created.
     *   Supersedes `serviceAudience`.
     */
    public ?Audience $audience = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|null $provider
     *   The service provider, service operator, or service performer; the
     *   goods producer.  Another party (a seller) may offer those services or
     *   goods on behalf of the provider.  A `provider` may also serve as the
     *   seller.  Supersedes `carrier`.
     */
    public Organization|Person|null $provider = null;

    /**
     * @var StoreCore\Types\URL|string|null $termsOfService
     *   Human-readable terms of service documentation.
     */
    public URL|string|null $termsOfService = null;
}
