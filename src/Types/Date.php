<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Date.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/ISO_8601 ISO 8601
 * @see     https://schema.org/Date Schema.org data type Date
 * @version 1.0.0
 */
class Date extends DateTime implements
    \DateTimeInterface,
    \JsonSerializable,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Converts the Date to an ISO 8601 string.
     *
     * @param void
     * @return string
     * @uses \DateTime::format()
     */
    public function __toString(): string
    {
        return $this->format('Y-m-d');
    }
}
