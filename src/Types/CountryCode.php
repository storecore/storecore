<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \JsonSerializable;
use \Stringable;
use \ValueError;

use function \ctype_upper;
use function \strlen;
use function \strtoupper;
use function \time;
use function \trim;

/**
 * Country code.
 *
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes List of ISO 3166 country codes
 * @version 1.0.0
 */
readonly class CountryCode implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $value
     *   ISO 3166-1 alpha-2 two-letter country code.
     */
    private string $value;

    /**
     * Creates a country code.
     *
     * @param string|\Stringable $country_code
     *   Case-insensitive ISO 3166-1 country code.
     *
     * @throws \ValueError
     *   Throws a value error exception if the country code does not
     *   consist of two letters.
     */
    public function __construct(string|Stringable $country_code)
    {
        $country_code = (string) $country_code;
        $country_code = trim($country_code);
        $country_code = strtoupper($country_code);

        if (strlen($country_code) !== 2 || !ctype_upper($country_code)) {
            throw new ValueError('Invalid country code', time());
        }

        $this->value = $country_code;
    }

    /**
     * Converts the country code to a string.
     *
     * @param void
     * 
     * @return string
     *   Uppercase alphanumeric ISO 3166-1 country code.
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return string
     * @uses \Stringable::__toString
     */
    public function jsonSerialize(): string
    {
        return $this->__toString();
    }
}
