<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Menu item.
 * 
 * A `MenuItem` is a food or drink item listed in a menu or menu section.
 * 
 * @package StoreCore\Core
 * @see     https://schema.org/MenuItem Schema.org type `MenuItem`
 * @version 0.1.0
 */
class MenuItem extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
