<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Unsigned tiny integer.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2021–2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://dev.mysql.com/doc/refman/8.0/en/integer-types.html MySQL Integer Types (Exact Value)
 * @version   1.0.0
 */
class TinyintUnsigned implements \Stringable, TypeInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var int $Value
     *   Current value of the value object.
     */
    protected int $value;

    /**
     * @param mixed $initial_value
     * @param bool $strict
     * @return void
     * @throws \DomainException
     * @throws \InvalidArgumentException
     */
    public function __construct($initial_value, $strict = true)
    {
        if ($strict && !is_int($initial_value)) {
            throw new \InvalidArgumentException();
        } elseif (!is_numeric($initial_value)) {
            throw new \InvalidArgumentException();
        }

        if ($initial_value < 0) {
            throw new \DomainException();
        } elseif ($initial_value > 255) {
            throw new \DomainException();
        }

        $this->value = (int) $initial_value;
    }

    /**
     * Converts the data object to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the unsigned integer as a numeric string.  This basic
     *   implementation of the `Stringable` interface for casting data objects
     *   to strings MAY be overwritten by extending classes.
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }
}
