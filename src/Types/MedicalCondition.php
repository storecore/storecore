<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Medical condition.
 * 
 * A _medical condition_ is any condition of the human body that affects the
 * normal functioning of a person, whether physically or mentally.  Includes
 * diseases, injuries, disabilities, disorders, syndromes, etc.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/MedicalCondition Schema.org type `MedicalCondition`
 * @version 0.1.0
 */
class MedicalCondition extends MedicalEntity
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
