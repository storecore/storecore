<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CMS\DefinedTermSet;

/**
 * Defined term.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/DefinedTerm Schema.org type `DefinedTerm`
 * @version 1.0.0
 */
class DefinedTerm extends Thing
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\CMS\DefinedTermSet|StoreCore\Types\URL $inDefinedTermSet
     *   A `DefinedTermSet` that contains this term.
     */
    public null|DefinedTermSet|URL $inDefinedTermSet = null;

    /**
     * @var null|string $termCode
     *   A code that identifies this `DefinedTerm` within a `DefinedTermSet`.
     */
    public ?string $termCode = null;
}
