<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Opening hours specification.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/OpeningHoursSpecification Schema.org type OpeningHoursSpecification
 * @version 0.3.0
 *
 * @see https://developers.google.com/search/docs/advanced/structured-data/local-business
 *      Local Business Structured Data - Google Search Central
 */
class OpeningHoursSpecification extends StructuredValue implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var array $openingHoursSpecification
     *   Opening hours from Monday through Sunday and on public holidays.
     */
    public array $openingHoursSpecification = array(
        'Monday'         => array('opens' => null, 'closes' => null),
        'Tuesday'        => array('opens' => null, 'closes' => null),
        'Wednesday'      => array('opens' => null, 'closes' => null),
        'Thursday'       => array('opens' => null, 'closes' => null),
        'Friday'         => array('opens' => null, 'closes' => null),
        'Saturday'       => array('opens' => null, 'closes' => null),
        'Sunday'         => array('opens' => null, 'closes' => null),
        'PublicHolidays' => array('opens' => null, 'closes' => null),
    );

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $specification = $this->openingHoursSpecification;

        // Drop weekdays where `opens` and `closes` are both null:
        foreach ($specification as $key => $value) {
            if ($value['opens'] === null && $value['closes'] === null) {
                unset($specification[$key]);
            }
        }

        $result = [];
        foreach ($specification as $key => $value) {
            $result[] = array(
                '@type' => 'OpeningHoursSpecification',
                'dayOfWeek' => $key,
                'opens' => $value['opens'],
                'closes' => $value['closes']
            );
        }

        return $result;
    }

    /**
     * Sets the opening hours for “closed all day” on a given weekday.
     *
     * @param StoreCore\Types\DayOfWeek|string $day_of_week
     *   Day of the week as a `DayOfWeek` object or a string.
     *
     * @return void
     *
     * @see https://developers.google.com/search/docs/advanced/structured-data/local-business
     *   To show a business is closed all day, set both `opens` and `closes`
     *   properties to `00:00`.
     */
    public function closeAllDay(DayOfWeek|string $day_of_week): void
    {
        $this->set($day_of_week, new Time('00:00'), new Time('00:00'));
    }

    /**
     * Sets the opening hours for “open all day” on a given weekday.
     *
     * @param StoreCore\Types\DayOfWeek|string $day_of_week
     *   Day of the week as a `DayOfWeek` object or a string.
     *
     * @return void
     *
     * @see https://developers.google.com/search/docs/advanced/structured-data/local-business
     *   To show a business as open 24 hours a day, set the `open` property
     *   to `00:00` and the `closes` property to `23:59`.
     */
    public function openAllDay(DayOfWeek|string $day_of_week): void
    {
        $this->set($day_of_week, new Time('00:00'), new Time('23:59'));
    }

    /**
     * Sets opening hours for a specific day.
     *
     * @param StoreCore\Types\DayOfWeek|string $day_of_week
     *   Day of the week as a `DayOfWeek` object or a string (`Monday`,
     *   `Tuesday`, etc.).  Use `PublicHolidays` as a placeholder for all
     *   official public holidays in some particular location.
     *
     * @param Time|string $opens
     *   The opening hour of the place or service on the given day of the week.
     *
     * @param Time|string $closes
     *   The closing hour of the place or service on the given day of the week.
     *   If the value for the `closes` property is less than the value for the
     *   `opens` property then the hour range is assumed to span over the next
     *   day.  If this OPTIONAL property is not set, a “closing” time of
     *   `23:59` is used for the remainder of the day.
     * 
     * @return void
     */
    public function set(DayOfWeek|string $day_of_week, Time|string $opens, Time|string $closes = '23:59'): void
    {
        try {
            if (\is_string($day_of_week)) $day_of_week = new DayOfWeek($day_of_week);
            if (\is_string($opens)) $opens = new Time($opens);
            if (\is_string($closes)) $closes = new Time($closes);
        } catch (\Exception $e) {
            throw new \ValueError($e->getMessage(), $e->getCode(), $e);
        }

        $this->openingHoursSpecification[$day_of_week->name] = array(
            'opens' => (string) $opens, 'closes' => (string) $closes
        );
    }
}
