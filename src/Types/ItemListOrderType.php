<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Item list order type.
 *
 * Enumerated for values for `itemListOrder` for indicating
 * how an ordered `ItemList` is organized.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ItemListOrderType Schema.org enumeration `ItemListOrderType`
 * @version 23.0.0
 */
enum ItemListOrderType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '23.0.0';

    case ItemListOrderAscending  = 'https://schema.org/ItemListOrderAscending';
    case ItemListOrderDescending = 'https://schema.org/ItemListOrderDescending';
    case ItemListUnordered       = 'https://schema.org/ItemListUnordered';
}
