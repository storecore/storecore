<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Schema.org contact point.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/ContactPoint Schema.org structured value `ContactPoint`
 * @version 0.3.0
 */
class ContactPoint extends StructuredValue
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var null|string $contactType
     *   A `Person` or `Organization` can have different `ContactPoint`s, for
     *   different purposes.  For example, a sales contact point, a customer
     *   service contact point and so on.  This property is used to specify
     *   the kind of `ContactPoint`.
     */
    public ?string $contactType = null;

    /**
     * @var null|string $telephone
     *   The telephone number.
     */
    public ?string $telephone = null;
}
