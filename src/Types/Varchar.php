<?php

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Generic VARCHAR value object.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2016, 2020–2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0
 */
class Varchar implements TypeInterface, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * @var string $value
     *   String value of the value object.
     */
    protected string $value;


    /**
     * Creates a value object for character data of variable length.
     *
     * @param mixed $initial_value
     * @param bool $strict
     * @throws \DomainException
     * @throws \TypeError
     */
    public function __construct(mixed $initial_value, bool $strict = true)
    {
        if ($initial_value instanceof \Stringable) {
            $initial_value = (string) $initial_value;
        }

        if ($strict && !\is_string($initial_value)) {
            throw new \TypeError();
        } elseif (!\is_string($initial_value)) {
            $initial_value = \strval($initial_value);
        }

        if (mb_strlen($initial_value, 'UTF-8') > 65532) {
            throw new \DomainException();
        }

        $this->value = $initial_value;
    }

    /**
     * Converts the value object to a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
