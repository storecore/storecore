<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016, 2020, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\CMS\Barcode as BarcodeModel;

/**
 * Schema.org barcode image object.
 *
 * An image of a visual machine-readable code such as a barcode or QR code.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Barcode Schema.org type `Barcode`
 * @version 1.0.0
 */
class Barcode extends BarcodeModel
{
}
