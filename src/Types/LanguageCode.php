<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Partial IETF BCP 47 language code.
 *
 * @package StoreCore\I18N
 * @version 1.0.0
 * @see     https://www.rfc-editor.org/rfc/rfc5646 RFC 5646: Tags for Identifying Languages
 * @see     https://www.w3.org/International/articles/language-tags/ Language tags in HTML and XML
 */
readonly class LanguageCode implements \Stringable, ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $value
     *   Normalized IETF BCP 47 language tag, usually consisting of an ISO 639‑1
     *   two-letter language code and an ISO 3166-1 two-letter country code,
     *   separated by a hyphen.  Defaults to 'en-GB' for British English through
     *   the class constructor.
     */
    private string $value;

    /**
     * Converts the langage code object to a string.
     *
     * @param string|\Stringable|null $code
     *   Language code, defaults to `en-GB` for British English.
     *
     * @throws \ValueError
     *   Throws a value error on an invalid language code.
     */
    public function __construct(string|\Stringable $code = 'en-GB')
    {
        $code = self::filter($code);
        if (self::validate($code)) {
            $this->value = $code;
        } else {
            throw new \ValueError('Invalid language code: ' . strval($code));
        }
    }

    /**
     * Converts the langage code object to a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Filters a language code string.
     * 
     * @param string $value
     * @return string
     */
    public static function filter(string $value): string
    {
        $value = trim($value);
        $value = str_replace('_', '-', $value);

        if (!str_contains($value, '-')) {
            $value = strtolower($value);
            return match ($value) {
                'ar', 'ara', 'arb' => 'ar-001',
                'deu', 'ger' => 'de-DE',
                'dut', 'nld' => 'nl-NL',
                'fra', 'fre' => 'fr-FR',
                default => $value,
            };
        }

        // Drop private use `-x-` subtags.
        if (str_contains($value, '-x-')) {
            $value = explode('-x-', $value, 2);
            $value = $value[0];
        }

        // Drop Unicode `-u-` extension subtags.
        if (str_contains($value, '-u-')) {
            $value = explode('-u-', $value, 2);
            $value = $value[0];
        }

        $value = explode('-', $value);

        // First element MUST be lowercase language subtag.
        $value[0] = strtolower($value[0]);

        // Limit to three elements.
        if (\count($value) > 3) {
            $value = array_slice($value, 0, 3);
        }

        if (\count($value) === 3) {
            $value[2] = strtoupper($value[2]);
        }

        if (\strlen($value[1]) === 4) {
            $value[1] = ucwords(strtolower($value[1]));
            if ($value[1] === 'Latn') {
                unset($value[1]);
            }
        } elseif (\strlen($value[1]) === 2) {
            $value[1] = strtoupper($value[1]);
        }

        $value = implode('-', $value);

        return match ($value) {
            'en-001' => 'en-US',
            'en-150' => 'en-GB',
            'zh-CN' => 'zh-Hans',
            'zh-TW' => 'zh-Hant',
            default => $value,
        };
    }

    /**
     * Validates a language code.
     *
     * @param mixed $variable
     *
     * @return bool
     * 
     * @see https://www.rfc-editor.org/rfc/rfc5646#section-4.4.1
     *      RFC 5646, section 4.4.1, “Working with Limited Buffer Sizes”
     */
    public static function validate(mixed $variable): bool
    {
        if ($variable instanceof \Stringable) {
            $variable = (string) $variable;
        } elseif (!\is_string($variable)) {
            return false;
        }

        $length = \strlen($variable);
        if ($length < 2 || $length > 35) {
            return false;
        }

        if (
            ($length === 2 || $length === 3)
            && !ctype_lower($variable)
        ) {
            return false;
        }

        if ($length > 4 && !str_contains($variable, '-')) {
            return false;
        }

        if ($length === 5) {
            $array = explode('-', $variable);
            if (
                \count($array) !== 2
                || !ctype_lower($array[0])
                || !ctype_upper($array[1])
            ) {
                return false;
            }
        }

        return true;
    }
}
