<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use \Stringable;
use \ValueError;

use function \strlen;
use function \strtoupper;
use function \trim;

/**
 * UN/CEFACT Common Code for Units of Measurement.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
readonly class CommonCode implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|string $name
     *   OPTIONAL name of the common code or unit of measurement,
     *   e.g. `kilogram` or `litre`.  This name is informational and
     *   MAY be translated.
     */
    public ?string $name;

    /**
     * @var null|string $unitOfMeasurement
     *   OPTIONAL unit of measurement representation symbol or SI (Système
     *   International) conversion factor, e.g. `kg`, `m²`, or `kg·m²`.
     */
    public ?string $unitOfMeasurement;

    /**
     * @var string $value
     *   Unit code as an uppercase string of 2 or 3 ASCII characters.
     */
    public string $value;

    /**
     * Creates a UN/CEFACT Common Code.
     * 
     * @param string $unit_code
     * @param null|string $unit_of_measurement
     * @param null|string $name
     */
    public function __construct(
        string $unit_code,
        ?string $unit_of_measurement = null,
        ?string $name = null,
    ) {
        $this->setValue($unit_code);
        $this->unitOfMeasurement = $unit_of_measurement;
        $this->name = $name;
    }

    /**
     * UN/CEFACT Common Code as an uppercase string of 2 or 3 characters.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Sets the common code string value.
     *
     * @internal
     * @param string $value
     * @return void
     */
    private function setValue(string $value): void
    {
        $value = trim($value);
        if (strlen($value) < 2 || strlen($value) > 3) {
            throw new ValueError('Invalid UN/CEFACT Common Code');
        }

        $value = strtoupper($value);
        $this->value = $value;
    }
}
