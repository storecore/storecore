<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015, 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use StoreCore\Database\Metadata;

/**
 * Email address.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class EmailAddress extends Varchar implements \JsonSerializable, \Stringable, TypeInterface, ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $displayName
     *   OPTIONAL name to display with the email address, such as `Jane Doe`
     *   in `Jane Doe <jane-doe@xyz.edu>` or `Customer Service` in
     *   `Customer Service <support@acme.com>`.
     */
    public ?string $displayName = null;

    /**
     * @var StoreCore\Database\Metadata $metadata
     *   Internal metadata.
     */
    public Metadata $metadata;

    /**
     * Creates an email address.
     * 
     * @param mixed $initial_value
     * @param bool $strict
     * @throws \DomainException
     * @throws \TypeError
     * @throws \ValueError
     *
     * @see https://support.google.com/manufacturers/answer/6184604?hl=en
     *      “Using email tags,” Manufacturer Center Help
     *
     * @see https://support.google.com/mail/answer/10313
     *      “Getting someone else’s mail,” Gmail Help Center 
     */
    public function __construct(mixed $initial_value, bool $strict = true)
    {
        mb_internal_encoding('UTF-8');
        $initial_value = trim($initial_value);

        parent::__construct($initial_value, $strict);
        $this->metadata = new Metadata();

        // Accept and remove `mailto:` prefix when not in strict mode.
        if (!$strict && str_starts_with($this->value, 'mailto:')) {
            $this->value = mb_substr($this->value, 7);
        }

        // @see https://dba.stackexchange.com/questions/37014/in-what-data-type-should-i-store-an-email-address-in-database
        if (mb_strlen($this->value) > 320) {
            throw new \ValueError();
        }

        $sanitized_email = filter_var($this->value, \FILTER_SANITIZE_EMAIL);
        if ($sanitized_email !== $this->value) {
            if ($strict === true || $sanitized_email === false) {
                throw new \ValueError();
            } else {
                $this->value = $sanitized_email;
            }
        }

        // Check for the last '@' at-sign position
        $pos = mb_strrpos($this->value, '@');
        if ($pos === false) {
            throw new \ValueError();
        }

        if (!filter_var($this->value, \FILTER_VALIDATE_EMAIL)) {
            throw new \ValueError();
        }

        $local_address  = mb_substr($this->value, 0, $pos);
        $server_address = mb_substr($this->value, $pos + 1);
        $server_address = mb_strtolower($server_address);

        if ($strict) {
            if (mb_strlen($local_address) > 64 || mb_strlen($local_address) < 1) {
                throw new \ValueError();
            }

            if (mb_strlen($server_address) > 255 || mb_strlen($server_address) < 1) {
                throw new \ValueError();
            }
        }

        $this->value = $local_address . '@' . $server_address;
        $this->metadata->hash = self::hash($this->value);
    }

    /**
     * Hashes an email address.
     *
     * @param string $string
     * @return string
     */
    final public static function hash(string $string): string
    {
        $string = mb_strtolower($string);

        $pos = mb_strrpos($string, '@');
        if ($pos === false) {
            throw new \ValueError();
        }

        $local_address  = mb_substr($string, 0, $pos);
        $server_address = mb_substr($string, $pos + 1);

        // Strip off email aliases and email tags in local address.
        $divider = $server_address === 'yahoo.com' ? '-' : '+';
        $pos = mb_strpos($local_address, $divider);
        if ($pos !== false) {
            $local_address = mb_substr($local_address, 0, $pos);
        }

        // Handle googlemail.com as gmail.com.
        if ($server_address === 'googlemail.com') {
            $server_address = 'gmail.com';
        }

        // Remove dots in local Gmail address.
        if ($server_address === 'gmail.com') {
            $local_address = str_ireplace('.', '', $local_address);
        }

        return hash('sha256', $local_address . '@' . $server_address, false);
    }

    /**
     * Returns JSON string representation of the email address.
     * 
     * @param void
     * @return string
     */
    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * @inheritDoc
     */
    public static function validate(mixed $variable): bool
    {
        try {
            $variable = new EmailAddress($variable, true);
            return true;
        } catch (\Throwable $t) {
            return false;
        }
    }
}
