<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

class_exists(Thing::class);
class_exists(Intangible::class);
class_exists(StructuredValue::class);

/**
 * Property value.
 *
 * A property-value pair, e.g. representing a feature of a `Product` or `Place`.
 * Use the `name` property for the name of the property.  If there is an
 * additional human-readable version of the value, put that into the
 * `description` property.
 *
 * Always use specific Schema.org properties when a) they exist and b) you can
 * populate them.  Using `PropertyValue` as a substitute will typically not
 * trigger the same effect as using the original, specific property.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/PropertyValue Schema.org type `PropertyValue`
 * @version 1.0.0-alpha.1
 */
class PropertyValue extends StructuredValue implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var int|float|null $maxValue
     *   The upper value of some characteristic or property.
     */
    public int|float|null $maxValue = null;

    /**
     * @var int|float|null $minValue
     *   The lower value of some characteristic or property.
     */
    public int|float|null $minValue = null;

    /**
     * @var bool|int|float|StructuredValue|string|null $value
     *   The value of the quantitative value or property value node.
     */
    public bool|int|float|StructuredValue|string|null $value = null;

    /**
     * @var string|URL|null $unitCode
     *   The unit of measurement given using the UN/CEFACT Common Code
     *   (2 or 3 characters) or a URL.  Other codes than the UN/CEFACT Common
     *   Code MAY be used with a prefix followed by a colon. 
     */
    public string|URL|null $unitCode = null;

    /**
     * @var string|null $unitText
     *   A string or text indicating the unit of measurement.  Useful if you
     *   cannot provide a standard unit code for `unitCode`.
     */
    public string|null $unitText = null;
}
