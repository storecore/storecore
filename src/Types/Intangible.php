<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Intangible thing.
 *
 * The Schema.org `Intangible` thing is a utility class that serves as the
 * umbrella for a number of ‘intangible’ things such as quantities, structured
 * values, etc.  This class is mainly included to mimic Schema.org hierarchies
 * like: Thing > Intangible > StructuredValue > ContactPoint.  Hierarchies like
 * these are implemented as an object stack:
 *
 * - class Thing
 * - class Intangible extends Thing
 * - class StructuredValue extends Intangible
 * - class ContactPoint extends StructuredValue
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Intangible Schema.org type `Intangible`
 * @version 1.0.0
 */
class Intangible extends Thing
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
