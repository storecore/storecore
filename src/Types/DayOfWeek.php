<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

/**
 * Day of week.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/DayOfWeek Schema.org enumeration type `DayOfWeek`
 * @version 1.0.0
 */
enum DayOfWeek: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    case Monday         = 'https://schema.org/Monday';
    case Tuesday        = 'https://schema.org/Tuesday';
    case Wednesday      = 'https://schema.org/Wednesday';
    case Thursday       = 'https://schema.org/Thursday';
    case Friday         = 'https://schema.org/Friday';
    case Saturday       = 'https://schema.org/Saturday';
    case Sunday         = 'https://schema.org/Sunday';

    case PublicHolidays = 'https://schema.org/PublicHolidays';
}
