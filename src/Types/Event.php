<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Types;

use function \is_string;

/**
 * Event.
 * 
 * An event happening at a certain time and location, such as a concert,
 * lecture, or festival.  Ticketing information MAY be added via the `offers`
 * property.  Repeated events may be structured as separate `Event` objects.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/Event Schema.org type `Event`
 * @version 0.1.0
 */
class Event extends Thing implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';


    /**
     * @var null|StoreCore\Types\DateTime|StoreCore\Types\Time $endTime
     *   The end date and time of the event.
     */
    protected null|DateTime|Time $endTime = null;

    /**
     * @var StoreCore\Types\DateTime|StoreCore\Types\Time|null $startTime
     *   The start date and time of the event.
     */
    protected null|DateTime|Time $startTime = null;


    /**
     * Sets the event end time.
     *
     * @param
     * @return void
     */
    public function setEndTime(DateTime|Time|string $end_time): void
    {
        if (is_string($end_time)) $end_time = new DateTime($end_time);
        $this->endTime = $end_time;
    }

    /**
     * Sets the event start time.
     *
     * @param
     * @return void
     */
    public function setStartTime(DateTime|Time|string $start_time): void
    {
        if (is_string($start_time)) $start_time = new DateTime($start_time);
        $this->startTime = $start_time;
    }
}
