<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\CRM\Parties;
use StoreCore\CRM\{Organization, OrganizationProxy};
use StoreCore\CRM\{Person, PersonProxy};
use StoreCore\Database\Metadata;
use StoreCore\Database\OrderItems;
use StoreCore\OML\OrderStatus;
use StoreCore\Types\UUID;

/**
 * Order model.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class Order extends AbstractModel implements
    \Countable,
    IdentityInterface,
    \JsonSerializable,
    VisitableInterface
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|int|string $confirmationNumber
     *   A number that confirms the given order has been received.
     */
    public null|int|string $confirmationNumber = null;

    /**
     * @var null|StoreCore\ProxyInterface $customer
     *   Customer or `null` if the order does not (or not yet) have a known
     *   customer.  The customer proxy either points to an organization (B2B)
     *   or a person (B2C), but not both.
     */
    private ?ProxyInterface $customer = null;

    /**
     * @var bool $isGift
     *   The order is a gift for someone other than the buyer.
     */
    public bool $isGift = false;

    /**
     * @var StoreCore\Database\Metadata $metadata
     *   Internal metadata for object storage.
     */
    public Metadata $metadata;

    /**
     * @var null|\DateTimeImmutable $orderDate
     *   Date order was placed.
     */
    public ?\DateTimeImmutable $orderDate = null;

    /**
     * @var null|int|string $orderNumber
     *   Unique order number of an accepted order.  The order number is created
     *   on confirmation and used as a public identifier of the order.  The
     *   order number MAY be created by an outside process but MUST be unique
     *   within a StoreCore environment.
     */
    protected null|int|string $orderNumber = null;

    /**
     * @var null|StoreCore\OML\OrderStatus
     *   The current status of the order.  Defaults to `null` for an unknown
     *   order status.
     */
    public ?OrderStatus $orderStatus = null;

    /**
     * @var null|StoreCore\ProxyInterface $seller
     *   OPTIONAL.  If the store handling the order is NOT the seller,
     *   this proxy identifies the seller.
     */
    private ?ProxyInterface $seller = null;

    /**
     * Creates an order entity model.
     *
     * @param StoreCore\Registry
     *   Global service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->metadata = new Metadata();
    }

    /**
     * Magic getter.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'customer' => $this->getCustomer(),
            'merchant', 'seller', 'vendor' => $this->getSeller(),
            'orderNumber' => $this->orderNumber,
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Magic setter.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'customer' => $this->setCustomer($value),
            'merchant', 'seller', 'vendor' => $this->setSeller($value),
            'orderNumber' => $this->setOrderNumber($value),
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Accepts order visitors.
     *
     * @param StoreCore\VisitorInterface $visitor
     * @return void
     */
    public function accept(VisitorInterface $visitor): void
    {
        $visitor->visitOrder($this);
    }

    /**
     * Counts the total number of items in the order or cart.
     *
     * @param void
     *
     * @return int
     *   Number of distinct items in the cart or order.  If a cart or order
     *   contains 2 apples and 3 bananas, this method will return the
     *   integer 5.
     */
    public function count(): int
    {
        if ($this->hasIdentifier()) {
            $model = new OrderItems($this->Registry);
            $result = $model->count($this->getIdentifier());
            if (\is_int($result)) {
                return $result;
            }
        }
        return 0;
    }

    /**
     * Gets the customer.
     *
     * @param void
     *
     * @return null|StoreCore\CRM\Organization|StoreCore\CRM\OrganizationProxy|StoreCore\CRM\Person|StoreCore\CRM\PersonProxy|StoreCore\Proxy
     *   Returns the customer as an `Organization`, a `Person`, or a `ProxyInterface`
     *   for an organizqtion or person.
     */
    public function getCustomer(): null|Organization|OrganizationProxy|Person|PersonProxy|Proxy
    {
        if ($this->getCustomerIdentifier() !== null) {
            $uuid = $this->getCustomerIdentifier()->__toString();
            $customers = new Parties($this->Registry);
            if ($customers->has($uuid)) {
                return $customers->get($uuid);
            } else {
                return $this->customer;
            }
        }
        return null;
    }

    /**
     * Gets the customer identifier.
     *
     * @param void
     * 
     * @return null|StoreCore\Types\UUID
     *   Returns the universally unique identifier (UUID) of the customer
     *   or `null` if the order has no known customer.
     */
    public function getCustomerIdentifier(): ?UUID
    {
        return $this->customer === null ? null : $this->customer->getIdentifier();
    }

    /**
     * Gets the seller.
     *
     * @param void
     * @return null|StoreCore\ProxyInterface|StoreCore\CRM\Organization|StoreCore\CRM\Person
     * @see https://schema.org/merchant
     * @see https://schema.org/seller
     * @see https://schema.org/vendor
     */
    public function getSeller(): null|ProxyInterface|Organization|Person
    {
        if ($this->getSellerIdentifier() !== null) {
            $uuid = $this->getSellerIdentifier()->__toString();
            $sellers = new Parties($this->Registry);
            if ($sellers->has($uuid)) {
                return $sellers->get($uuid);
            } else {
                return $this->seller;
            }
        }
        return null;
    }

    /**
     * Gets the seller identifier.
     *
     * @param void
     * 
     * @return null|StoreCore\Types\UUID
     *   Returns the universally unique identifier (UUID) of the seller or
     *   `null` if the `Order` has no seller.  If a seller is not set, we
     *   generally assume that the `Order` is being sold by the `OnlineStore`
     *   that is handling the `Order`.
     *
     * @see https://schema.org/seller
     *   Schema.org property `seller` of an `Order`.  Superseded `merchant`
     *   and `vendor`.
     */
    public function getSellerIdentifier(): ?UUID
    {
        return $this->seller === null ? null : $this->seller->getIdentifier();
    }

    /**
     * Specifies data which should be serialized to Schema.org JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [];
        if ($this->hasIdentifier()) {
            $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/orders/' . $this->getIdentifier();
        }

        $result['@context'] = 'https://schema.org';
        $result['@type'] = 'Order';

        if (!empty($this->getCustomerIdentifier())) {
            $result['customer'] = $this->getCustomer();
        }

        if ($this->hasIdentifier()) {
            $result['identifier'] = $this->getIdentifier()->__toString();
        }

        if ($this->isGift) {
            $result['isGift'] = true;
        }

        if (!empty($this->orderNumber)) {
            $result['orderNumber'] = (string) $this->orderNumber;
        }

        $seller = $this->getSeller();
        if ($seller !== null) {
            $seller = $seller->jsonSerialize();
            if (isset($seller['@context']) && str_contains($seller['@context'], '://schema.org')) {
                unset($seller['@context']);
            }
            $result['seller'] = $seller;
        }

        return $result;
    }

    /**
     * Sets the customer.
     *
     * @param StoreCore\ProxyInterface|StoreCore\CRM\Organization|StoreCore\CRM\Person $customer
     *   Customer as an organization (B2B) or a person (B2C).
     *
     * @return void
     *
     * @throws StoreCore\DependencyException
     *   Throws a dependency logic exception on a customer without a customer identifier
     *   to prevent orders from being linked to unknown or nonexistent customers.
     */
    public function setCustomer(ProxyInterface|Organization|Person $customer): void
    {
        if ($customer instanceof ProxyInterface) {
            $this->customer = $customer;
        } else {
            $this->customer = ProxyFactory::createFromIdentityInterface($customer);
        }
    }

    /**
     * Sets the customer identifier.
     *
     * @param StoreCore\Types\UUID|string $customer_uuid
     *   Universally unique identifier (UUID) of the customer.
     *
     * @return void
     */
    public function setCustomerIdentifier(UUID|string $customer_uuid): void
    {
        if (\is_string($customer_uuid)) {
            $customer_uuid = new UUID($customer_uuid);
        }
        $customer = new Proxy($customer_uuid);
        $this->setCustomer($customer);
    }

    /**
     * Sets the order number.
     *
     * @param string|int|float $order_number
     * @return void
     * @see https://schema.org/orderNumber Schema.org property `orderNumber`
     */
    private function setOrderNumber(string|int|float $order_number): void
    {
        if (\is_float($order_number)) {
            $order_number = (string) $order_number;
        }

        if (\is_string($order_number)) {
            $order_number = trim($order_number);
            if (ctype_digit($order_number) && !str_starts_with($order_number, '0')) {
                $order_number = (int) $order_number;
            }
        }

        $this->orderNumber = $order_number;
    }

    /**
     * Sets the seller.
     *
     * @param StoreCore\ProxyInterface|StoreCore\CRM\Organization|StoreCore\CRM\Person $seller
     *   Organization or person selling the order.
     *
     * @return void
     *
     * @throws StoreCore\DependencyException
     *   Throws a dependency exception on a seller without a seller identifier
     *   to prevent orders from being linked to unknown or nonexistent seller.
     */
    public function setSeller(ProxyInterface|Organization|Person $seller): void
    {
        if ($seller instanceof ProxyInterface) {
            $this->seller = $seller;
        } else {
            $this->seller = ProxyFactory::createFromIdentityInterface($seller);
        }
    }

    /**
     * Sets the seller identifier.
     *
     * @param StoreCore\Types\UUID|string $seller_uuid
     *   Universally unique identifier (UUID) of a seller, vendor, or merchant.
     *
     * @return void
     */
    public function setSellerIdentifier(UUID|string $seller_uuid): void
    {
        if (\is_string($seller_uuid)) {
            $seller_uuid = new UUID($seller_uuid);
        }
        $seller = new Proxy($seller_uuid);
        $this->setSeller($seller);
    }
}
