<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\OML\CartID;

/**
 * Cart model.
 *
 * In the StoreCore business logic, a shopping cart (or shopping basket)
 * is an incomplete order.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class Cart extends Order implements
    \Countable,
    IdentityInterface,
    VisitableInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\OML\CartID|null $cartID
     *   Shopping cart identifier or `null` if the cart identifier does not exist.
     */
    protected ?CartID $cartID = null;

    /**
     * Gets the shopping cart ID.
     *
     * @param void
     *
     * @return StoreCore\OML\CartID|null
     *   Returns a cart identifier data object or `null` if the cart has no cart
     *   identifier.
     */
    public function getCartID(): ?CartID
    {
        return $this->cartID;
    }

    /**
     * Sets the shopping cart ID.
     *
     * @param StoreCore\OML\CartID $cart_id
     *   Cart identifier data object.
     *
     * @return void
     */
    public function setCartID(CartID $cart_id): void
    {
        $this->cartID = $cart_id;
    }
}
