<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \DateTimeImmutable;
use Psr\Clock\ClockInterface;

/**
 * Clock trait.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-20/ PSR-20 Clock
 * @see     https://github.com/php-fig/clock PSR-20 Clock repository
 * @version 1.0.0
 */
trait ClockTrait
{
    /**
     * Returns the current date and time.
     *
     * @param void
     * @return \DateTimeImmutable Current date and time.
     */
    public function now(): DateTimeImmutable
    {
        $registry = Registry::getInstance();
        if ($registry->has('Clock')) {
            return $registry->get('Clock')->now();
        }

        $clock = new Clock();
        return $clock->now();
    }
}
