<?php

declare(strict_types=1);

namespace StoreCore;

/**
 * Observer design pattern: subject trait.
 * 
 * This trait implements the observer design pattern interface `SplSubject`.
 * It allows you to `attach()` and `detach()` observers to a subject class
 * that can be called using `notify()`.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   2.0.0
 */
trait SubjectTrait
{
    /**
     * @var array $observers
     *   Observers observing this subject.
     */
    protected array $observers = [];

    /**
     * Attaches an observer.
     *
     * @param \SplObserver $observer
     *   Observer to attach to this subject.
     *
     * @return void
     */
    public function attach(\SplObserver $observer): void
    {
        $id = spl_object_id($observer);
        $this->observers[$id] = $observer;
    }

    /**
     * Detaches an attached observer.
     *
     * @param \SplObserver $observer
     *   Observer to detach from this subject.
     *
     * @return void
     */
    public function detach(\SplObserver $observer): void
    {
        $id = spl_object_id($observer);
        unset($this->observers[$id]);
    }

    /**
     * Notifies and updates all attached observers.
     *
     * @param void
     * @return void
     * @uses \SplObserver::update()
     */
    public function notify(): void
    {
        if (!empty($this->observers)) {
            foreach ($this->observers as $observer) {
                $observer->update($this);
            }
        }
    }
}
