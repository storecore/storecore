<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \Countable;
use \JsonSerializable;

use StoreCore\PIM\{IndividualProduct, Product, Service};
use StoreCore\OML\OrderStatus;
use StoreCore\OML\ParcelDelivery;

/**
 * Order item.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/OrderItem Schema.org type `OrderItem`
 * @version 0.1.1
 */
class OrderItem implements Countable, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.1';

    /**
     * @var StoreCore\PIM\Product|StoreCore\PIM\Service $orderedItem
     *   The `Product` or `Service` ordered.  In Schema.org this MAY also be
     *   another `OrderItem`, but to keep order management clear, we don’t
     *   allow order items that contain other order items.
     */
    public readonly Product|Service $orderedItem;

    /**
     * @var null|StoreCore\OML\ParcelDelivery $orderDelivery 
     *   The delivery of the parcel related to this `Order` or `OrderItem`.
     */
    public ?ParcelDelivery $orderDelivery = null;

    /**
     * @var string $orderItemNumber
     *   The identifier of the order item.
     */
    public readonly string $orderItemNumber;

    /**
     * @var null|StoreCore\OML\OrderStatus $orderItemStatus
     *   Order status of the `OrderItem`.  Defaults
     *   to `null` for an unknown order status.
     */
    public ?OrderStatus $orderItemStatus = null;

    /**
     * @var int $orderQuantity
     *   The number of the item ordered.  If the property is not set,
     *   assume the quantity is one.
     */
    protected int $orderQuantity = 1;


    /**
     * Creates an order item.
     *
     * @param StoreCore\PIM\Product|StoreCore\PIM\Service|StoreCore\OrderItem $ordered_item
     *   The `Product`, `Service`, or `OrderItem` ordered.  An `OrderItem`
     *   is converted to the `Product` or `Service` that it contains.
     * 
     * @param int|string $order_quantity
     *   The number of the item ordered as an integer or a numeric string.
     *   Defaults to 1.  If the `ordered_item` is and `IndividualProduct`,
     *   the `order_quantity` is always 1.
     */
    public function __construct(
        Product|Service|OrderItem $ordered_item,
        int|string $order_quantity = 1,
    ) {
        if ($ordered_item instanceof IndividualProduct) $order_quantity = 1;
        $this->setOrderQuantity((int) $order_quantity);

        if ($ordered_item instanceof OrderItem) {
            $this->setOrderQuantity($this->orderQuantity * count($ordered_item));
            $ordered_item = $ordered_item->orderedItem;
        }
        $this->orderedItem = $ordered_item;
        $this->setOrderItemNumber($this->orderedItem);
    }

    /**
     * Reads data from inaccessible (protected or private) or non-existing properties.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'orderQuantity' => $this->count(),
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Writes data to inaccessible (protected or private) properties.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws \ValueError
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'orderQuantity' => $this->setOrderQuantity($value),
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Counts the number of ordered items.
     *
     * @param void
     * @return int
     */
    public function count(): int
    {
        return $this->orderQuantity;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'OrderItem',
            'orderItemNumber' => $this->orderItemNumber,
            'orderQuantity' => $this->count(),
            'orderedItem' => $this->orderedItem->jsonSerialize(),
            'orderItemStatus' => $this->orderItemStatus,
            'orderDelivery' => $this->orderDelivery === null ? null : $this->orderDelivery->jsonSerialize(),
        ];

        $result = array_filter($result);

        foreach ($result as $key => $value) {
            if (
                \is_array($result[$key])
                && isset($result[$key]['@context'])
                && $result[$key]['@context'] === 'https://schema.org'
            ) {
                unset($result[$key]['@context']);
            }
        }

        return $result;
    }

    /**
     * Sets the order item number.
     *
     * @internal
     *
     * @param StoreCore\PIM\Product|StoreCore\PIM\Service $item
     *   The `Product` or `Service` that is being ordered.  The
     *   `orderItemNumber` is derived from one of the unique identifiers
     *   of this ordered item.
     *
     * @return void
     *   Sets the read-only `OrderItem::$orderItemNumber` class property.
     * 
     * @throws StoreCore\DependencyException
     *   Throws a StoreCore dependency logic exception if the `item` is
     *   an `IndividualProduct` but it has no `serialNumber`.
     */
    private function setOrderItemNumber(Product|Service $item): void
    {
        if ($item instanceof IndividualProduct) {
            try {
                $this->orderItemNumber = $item->serialNumber;
            } catch (\Throwable $e) {
                throw new DependencyException($e->getMessage(), $e->getCode(), $e);
            }
        } elseif ($item->sku !== null) {
            $this->orderItemNumber = $item->sku;
        } else {
            $this->orderItemNumber = '';
        }
    }

    /**
     * Sets the ordered quantity.
     *
     * @param int $order_quantity
     *   The number items ordered as an integer.
     *
     * @return void
     * 
     * @throws \DomainException
     *   Throws a domain logic exception if the `$order_quantity`
     *   is less than `1` or more than `65535`.
     */
    private function setOrderQuantity(int $order_quantity): void
    {
        $order_quantity = abs($order_quantity);
        if ($order_quantity < 1 || $order_quantity > 65535) {
            throw new \DomainException();
        }
        $this->orderQuantity = $order_quantity;
    }
}
