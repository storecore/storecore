<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Un-register action.
 * 
 * The act of un-registering from a `Service`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/UnRegisterAction Schema.org type UnRegisterAction
 * @version 0.1.0
 */
class UnRegisterAction extends InteractAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
