<?php

declare(strict_types=1);

namespace StoreCore\Actions;

use Psr\Container\NotFoundExceptionInterface;
use StoreCore\NotFoundException;

class ActionNotFoundException extends NotFoundException implements NotFoundExceptionInterface
{
}
