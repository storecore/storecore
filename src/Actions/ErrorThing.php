<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\Engine\ResponseCodes;
use StoreCore\Types\Thing;
use StoreCore\Types\URL;

class_exists(Error::class);
class_exists(ResponseCodes::class);
class_exists(Thing::class);
class_exists(URL::class);

/**
 * Action error as a Schema.org Thing.
 * 
 * Maps more expressive RFC 7807 error details to a plain Schema.org `Thing`.
 * The main purpose of this wrapper is to inject an `Error` object into the
 * `error` property of an `Action`.
 * 
 * | RFC 7807                  | Schema.org                 |
 * | ------------------------- | -------------------------- |
 * | (string) Error.title      | (string) Thing.name        |
 * | (UriInterface) Error.type | (URL) Thing.additionalType |
 * | (string) Error.detail     | (string) Thing.description |
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/error Schema.org property `Action.error`
 * @see     https://schema.org/Thing Schema.org type `Thing`
 * @version 0.1.0
 */
class ErrorThing extends Thing implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * Creates a Schema.org error Thing.
     *
     * @param StoreCore\Actions\Error $error
     * @uses StoreCore\Engine\ResponseCodes::getReasonPhrase())
     */
    public function __construct(Error $error)
    {
        if (!empty($error->title)) {
            $this->name = $error->title;
        } else {
            $this->name = $error->status . ' ' . ResponseCodes::getReasonPhrase($error->status);
        }
        
        if ($error->type !== null && $error->type !== 'about:blank') {
            if ($error->type instanceof URL) {
                $this->additionalType = $error->type;
            } else {
                $this->additionalType = new URL((string) $error->type);
            }
        }

        if (!empty($error->detail)) $this->description = $error->title;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        unset($result['@context']);
        $result['@type'] = 'Thing';
        return $result;
    }
}
