<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Discover action.
 * 
 * A `discover action` is the act of discovering (or finding) an `object`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DiscoverAction Schema.org type `DiscoverAction`
 * @version 1.0.0
 */
class DiscoverAction extends FindAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
