<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\Types\Thing;

/**
 * Update action.
 * 
 * An _update action_ is the act of managing by changing/editing
 * the state of the object.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/UpdateAction Schema.org type `UpdateAction`
 * @version 1.0.0
 */
class UpdateAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var StoreCore\Types\Thing|null $targetCollection
     *   A sub property of `object`.  The collection target of the action.
     */
    public ?Thing $targetCollection = null;
}
