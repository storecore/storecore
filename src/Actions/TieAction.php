<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Win action.
 * 
 * An _lose action_ is the act of reaching a draw in a competitive activity.
 * A `TieAction` is neither a `WinAction` nor a `LoseAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TieAction Schema.org type `TieAction`
 * @version 1.0.0
 */
class TieAction extends AchieveAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
