<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use Psr\Clock\ClockInterface;
use Psr\SimpleCache\CacheInterface;
use StoreCore\IdentityInterface;

use StoreCore\ClockTrait;
use StoreCore\Registry;
use StoreCore\Database\AbstractModel;
use StoreCore\Types\UUID;

/**
 * Write-through cache for actions.
 *
 * @internal
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-16/ PSR-16: Common Interface for Caching Libraries
 * @see     https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-16-simple-cache.md
 * @version 1.0.0-alpha.1
 */
class Cache extends AbstractModel implements CacheInterface, ClockInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Action\ObjectStorage $objectStorage
     *   Temporary cache for `Action` objects.
     */
    private ObjectStorage $objectStorage;

    /**
     * Creates a cache for action objects.
     *
     * @param StoreCore\Registry $registry
     *
     * @uses \PDO::exec()
     *   Deletes up to 1000 expired cache objects in about 1 in 10 instances.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->objectStorage = ObjectStorage::getInstance();

        if (random_int(0, 9) === 0) {
            try {
                $this->Database->exec(
                    "DELETE FROM `sc_action_objects` WHERE `date_expires` < '" . $this->now()->format('Y-m-d H:i:s') . "' ORDER BY `date_expires` ASC LIMIT 1000"
                );
            } catch (\PDOException $e) {
                $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            }
        }
    }

    /**
     * Clears the actions cache.
     *
     * Clears the temporary memory cache, deletes all actions older than
     * 18 months, and deletes all stored PHP objects that have expired.
     * 
     * @param void
     *
     * @return bool
     *   Returns `true` on success, `false` otherwise.
     */
    public function clear(): bool
    {
        $this->objectStorage->removeAll($this->objectStorage);

        $result = true;
        if (false === $this->Database->exec('DELETE FROM `sc_actions` WHERE `date_modified` < DATE_SUB(NOW(), INTERVAL 18 MONTH)')) {
            $result = false;
        }

        if (false === $this->Database->exec("DELETE FROM `sc_action_objects` WHERE `date_expires` < '" . $this->now()->format('Y-m-d H:i:s.u') . "'")) {
            $result = false;
        }

        return $result;
    }

    #[\Override]
    public function delete(string $key): bool
    {
        if (!UUID::validate($key)) {
            throw new CacheKeyException();
        }
        $key = str_replace('-', '', $key);
        $key = strtolower($key);

        try {
            $statement = $this->Database->prepare('DELETE FROM `sc_actions` WHERE `action_uuid` = UNHEX(?) LIMIT 1');
            if ($statement === false) return false;
            $statement->bindValue(1, $key, \PDO::PARAM_STR);
            $result = $statement->execute();
            $statement->closeCursor();
            unset($statement);
            if ($result === false) return false;

            foreach ($this->objectStorage as $object) {
                if ($this->objectStorage->getInfo() === $key) $this->objectStorage->detach($object);
            }
            return true;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    public function deleteMultiple(iterable $keys): bool
    {
        $result = true;
        foreach ($keys as $key) {
            try {
                if ($this->delete($key) !== true) $result = false;
            } catch (\Throwable $e) {
                $result = false;
            }
        }
        return $result;
    }

    public function get(string $key, mixed $default = null): mixed
    {
        if (true !== UUID::validate($key)) {
            throw new CacheKeyException();
        }
        $key = str_replace('-', '', $key);
        $key = strtolower($key);

        if ($this->objectStorage->has($key)) {
            return $this->objectStorage->get($key);
        }

        try {
            $statement = $this->Database->prepare(
                'SELECT `serialized_php_object` FROM `sc_action_objects` WHERE `action_uuid` = UNHEX(?)'
            );
            if ($statement === false) return $default;
            $statement->bindValue(1, $key, \PDO::PARAM_STR);
            if ($statement->execute() === false) throw new ActionNotFoundException();
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            $statement = null;
            unset($statement);
            if ($result === false) return $default;

            $result = unserialize(gzinflate($result));
            $this->objectStorage->attach($result, $key);
            return $result;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return $default;
        } catch (\Exception $e) {
            return $default;
        }
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        foreach ($keys as $key => $value) {
            try {
                $keys[$key] = $this->get($key, $default);
            } catch (\Exception $e) {
                $keys[$key] = $default;
            }
        }
        return $keys;
    }

    /**
     * Checks if an action object is cached.
     *
     * @param string $key UUID as a string.
     * @return bool
     * @uses \StoreCore\Types\UUID::validate()
     */
    public function has(string $key): bool
    {
        if (!UUID::validate($key)) {
            return false;
        }
        $key = str_replace('-', '', $key);
        $key = strtolower($key);

        if ($this->objectStorage->has($key)) {
            return true;
        }

        $statement = $this->Database->prepare(
            'SELECT COUNT(*) FROM `sc_action_objects` WHERE `action_uuid` = UNHEX(?)'
        );
        if ($statement !== false) {
            $statement->bindValue(1, $key, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $count = (int) $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count === 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @uses StoreCore\Actions\CacheKeyException
     *   Implements `Psr\SimpleCache\InvalidArgumentException`.
     */
    public function set(string $key, mixed $action, null|int|\DateInterval $ttl = null): bool
    {
        if (!$action instanceof Action) throw new \ValueError();

        if (!UUID::validate($key)) throw new CacheKeyException();
        $action->setIdentifier($key);
        $key = str_replace('-', '', $key);
        $key = strtolower($key);

        /*
         * If the `$ttl` is not set but the `Action` does have an `endTime`,
         * the TTL (time to live) is based on the `endTime`.
         */
        if (
            $ttl === null && $action->endTime !== null
        ) {
            if ($action->endTime > $this->now()) {
                $ttl = $action->endTime->diff($this->now(), true);
            } else {
                $ttl = new \DateInterval('PT0S');
            }
        }

        if (\is_int($ttl)) {
            if ($ttl >= 0) {
                $ttl = new \DateInterval('PT' . $ttl . 'S');
            } else {
                throw new \ValueError();
            }
        }

        /*
         * If the TTL (time to live) is not set, then failed actions are cached for
         * 72 hours (`PT72H`) and other types of actions for 14 days (`P14D`).
         */
        if ($ttl === null) {
            if ($action->actionStatus === ActionStatusType::FailedActionStatus) {
                $ttl = new \DateInterval('PT72H');
            } else {
                $ttl = new \DateInterval(('P14D'));
            }
        }

        try {
            $data = $this->toArray($action);
            $statement = $this->Database->prepare(
                'INSERT INTO `sc_actions` (`action_uuid`, `action_status`, `action_class`, `start_time`, `end_time`, `agent_uuid`, `object_uuid`, `provider_uuid`, `result_uuid`, `action_json`) VALUES (UNHEX(:action_uuid), :action_status, :action_class, :start_time, :end_time, :agent_uuid, :object_uuid, :provider_uuid, :result_uuid, :action_json) ON DUPLICATE KEY UPDATE `action_status` = :updated_action_status, `action_class` = :updated_action_class, `start_time` = :updated_start_time, `end_time` = :updated_end_time, `agent_uuid` = :updated_agent_uuid, `object_uuid` = :updated_object_uuid, `provider_uuid` = :updated_provider_uuid, `result_uuid` = :updated_result_uuid, `action_json` = :updated_action_json'
            );
            if ($statement === false) return false;
            $statement->bindValue(':updated_action_status', $data['action_status'], \PDO::PARAM_INT);
            $statement->bindValue(':action_uuid', $key, \PDO::PARAM_STR);
            $statement->bindValue(':action_status', $data['action_status'], \PDO::PARAM_INT);
            $statement->bindValue(':action_class', $data['action_class'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_action_class', $data['action_class'], \PDO::PARAM_STR);
            $statement->bindValue(':start_time', $data['start_time'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_start_time', $data['start_time'], \PDO::PARAM_STR);
            $statement->bindValue(':end_time', $data['end_time'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_end_time', $data['end_time'], \PDO::PARAM_STR);
            $statement->bindValue(':agent_uuid', $data['agent_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_agent_uuid', $data['agent_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':object_uuid', $data['object_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_object_uuid', $data['object_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':provider_uuid', $data['provider_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_provider_uuid', $data['provider_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':result_uuid', $data['result_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_result_uuid', $data['result_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':action_json', $data['action_json'], \PDO::PARAM_STR);
            $statement->bindValue(':updated_action_json', $data['action_json'], \PDO::PARAM_STR);
            if ($statement->execute() === false) return false;
            $statement->closeCursor();
            unset($data);

            $blob = gzdeflate(serialize($action), 9, \ZLIB_ENCODING_RAW);
            if ($blob === false) return false;

            $statement = $this->Database->prepare(
                'INSERT INTO `sc_action_objects` (`action_uuid`, `date_expires`, `serialized_php_object`) VALUES (UNHEX(:action_uuid), :date_expires, :blob) ON DUPLICATE KEY UPDATE `date_expires` = :updated_date_expires, `serialized_php_object` = :updated_blob'
            );
            if ($statement === false) return false;
            $statement->bindValue(':action_uuid', $key, \PDO::PARAM_STR);
            $statement->bindValue(':date_expires', $this->now()->add($ttl)->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
            $statement->bindValue(':blob', $blob, \PDO::PARAM_LOB);
            $statement->bindValue(':updated_date_expires', $this->now()->add($ttl)->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
            $statement->bindValue(':updated_blob', $blob, \PDO::PARAM_LOB);
            $result = $statement->execute();
            $statement->closeCursor();
            $statement = null;
            unset($blob, $statement);

            if ($result !== false) $this->objectStorage->attach($action);
            return $result;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    public function setMultiple(iterable $values, null|int|\DateInterval $ttl = null): bool
    {
        if ($ttl === null) $ttl = 0;
        if (\is_int($ttl)) $ttl = new \DateInterval('PT' . abs($ttl) . 'S');

        $result = true;
        foreach ($values as $key => $value) {
            try {
                if ($this->set($key, $value, $ttl) !== true) $result = false;
            } catch (CacheKeyException $e) {
                throw $e;
            } catch (\Throwable $e) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Converts an Action object to a key-value array.
     *
     * @internal
     * @param StoreCore\Actions\Action $action
     * @return array
     */
    private function toArray(Action $action): array
    {
        // Structure of a single row in the `sc_actions` database table.
        $result = [
            'action_uuid' => $action->getIdentifier()->__toString(),
            'action_status' => null,
            'action_class' => get_class($action),
            'start_time' => $action->startTime === null ? null : $action->startTime->format('Y-m-d H:i:s'),
            'end_time' => $action->endTime === null ? null : $action->endTime->format('Y-m-d H:i:s'),
            'agent_uuid' => null,
            'object_uuid' => null,
            'provider_uuid' => null,
            'result_uuid' => null,
            'action_json' => json_encode($action, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE),
        ];

        switch ($action->actionStatus) {
            case ActionStatusType::ActiveActionStatus:
                $result['action_status'] = 0;
                break;
            case ActionStatusType::CompletedActionStatus:
                $result['action_status'] = 1;
                break;
            case ActionStatusType::FailedActionStatus:
                $result['action_status'] = 2;
                break;
            case ActionStatusType::PotentialActionStatus:
                $result['action_status'] = 3;
                break;
            default:
                $result['action_status'] = null;
                break;
        }

        if ($action->agent !== null && $action->agent instanceof IdentityInterface && $action->agent->hasIdentifier()) {
            $result['agent_uuid'] = (string) $action->agent->getIdentifier();
        }

        if ($action->object !== null && $action->object instanceof IdentityInterface && $action->object->hasIdentifier()) {
            $result['object_uuid'] = (string) $action->object->getIdentifier();
        }

        if ($action->provider !== null && $action->provider instanceof IdentityInterface && $action->provider->hasIdentifier()) {
            $result['provider_uuid'] = (string) $action->provider->getIdentifier();
        }

        if ($action->result !== null && $action->result instanceof IdentityInterface && $action->result->hasIdentifier()) {
            $result['result_uuid'] = (string) $action->result->getIdentifier();
        }

        return $result;
    }
}
