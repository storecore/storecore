<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Want action.
 * 
 * A _want action_ is the act of expressing a desire about the `object`.
 * An `agent` wants an `object`.
 * 
 * @api
 * @package   StoreCore\Core
 * @see       https://schema.org/WantAction Schema.org type WantAction
 * @version   1.0.0
 */
class WantAction extends ReactAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
