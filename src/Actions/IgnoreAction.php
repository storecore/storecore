<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Ignore action.
 * 
 * An _ignore action_ is the act of intentionally disregarding the `object`.
 * An `agent` ignores an `object`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/IgnoreAction Schema.org type `IgnoreAction`
 * @version 1.0.0
 */
class IgnoreAction extends AssessAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
