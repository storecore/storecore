<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use \JsonSerializable;
use \Stringable;

/**
 * Solve math action.
 *
 * The `Action` that takes in a math expression and directs users to a page
 * potentially capable of solving or simplifying that expression.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/SolveMathAction Schema.org type `SolveMathAction`
 * @version 0.1.0
 */
class SolveMathAction extends Action implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|string $eduQuestionType
     *   For questions that are part of learning resources (e.g. Quiz),
     *   `eduQuestionType` indicates the format of question being given.
     *   Examples: “Multiple choice”, “Open ended”, or “Flashcard”.
     */
    public ?string $eduQuestionType = null;
}
