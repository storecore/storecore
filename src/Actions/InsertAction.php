<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Insert action.
 * 
 * An _insert action_ is the act of adding at a specific location in an ordered collection.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/InsertAction Schema.org type `InsertAction`
 * @version 0.1.0
 */
class InsertAction extends AddAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
