<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Take action.
 * 
 * A _take action_ is the act of gaining ownership of an object from an origin.
 * Reciprocal of `GiveAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TakeAction Schema.org type `TakeAction`
 * @version 0.1.0
 */
class TakeAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
