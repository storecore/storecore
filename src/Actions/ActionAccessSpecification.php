<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\PIM\Offer;
use StoreCore\Types\Intangible;
use StoreCore\Types\{Date, DateTime, Time};

class_exists(Intangible::class);
class_exists(Offer::class);

/**
 * Action access specification.
 * 
 * An _action access specification_ is a set of requirements that MUST be
 * fulfilled in order to perform an `Action`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ActionAccessSpecification Schema.org type `ActionAccessSpecification`
 * @version 0.1.0
 */
class ActionAccessSpecification extends Intangible implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var StoreCore\Types\Date|StoreCore\Types\DateTime|StoreCore\Types\Time|null $availabilityEnds
     *   The end of the availability of the `Product` or `Service` included
     *   in the `Offer`.
     */
    public Date|DateTime|Time|null $availabilityEnds = null;

    /**
     * @var StoreCore\Types\Date|StoreCore\Types\DateTime|StoreCore\Types\Time|null $availabilityStarts
     *   The beginning of the availability of the `Product` or `Service`
     *   included in the `Offer`.
     */
    public Date|DateTime|Time|null $availabilityStarts = null;

    /**
     * @var StoreCore\PIM\Offer|null $expectsAcceptanceOf
     *   An `Offer` which MUST be accepted before the user can perform the
     *   `Action`.  For example, the user may need to buy a movie before being
     *   able to watch it.
     */
    public ?Offer $expectsAcceptanceOf = null;
}
