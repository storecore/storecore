<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Invite action.
 * 
 * An _invite action_ is the act of asking someone to attend an `Event`.
 * Reciprocal of `RsvpAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/InviteAction Schema.org type `InviteAction`
 * @version 0.1.0
 */
class InviteAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
