<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * React action.
 * 
 * A _react action_ is the act of responding instinctively and emotionally
 * to an `object`, expressing a sentiment.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReactAction Schema.org type `ReactAction`
 * @version 1.0.0
 */
class ReactAction extends AssessAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
