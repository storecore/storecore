<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Achieve action.
 * 
 * An _achieve action_ is the act of accomplishing something via previous efforts.
 * It is an instantaneous action rather than an ongoing process.  The `AchieveAction`
 * class has three more specific types: `LoseAction`, `TieAction`, and `WinAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AchieveAction Schema.org type `AchieveAction`
 * @version 1.0.0
 */
class AchieveAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
