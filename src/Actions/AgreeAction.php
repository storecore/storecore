<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Agree action.
 * 
 * An _agree action_ is the act of expressing a consistency of opinion with the
 * `object`.  An `agent` agrees to/about an `object` (a proposition, topic or
 * theme) with `participants`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AgreeAction Schema.org type AgreeAction
 * @version 1.0.0
 */
class AgreeAction extends ReactAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
