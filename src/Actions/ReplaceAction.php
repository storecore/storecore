<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\PIM\Product;
use StoreCore\CMS\AbstractCreativeWork as CreativeWork;
use StoreCore\Types\Thing;

class_exists(UpdateAction::class);
class_exists(Product::class);
class_exists(CreativeWork::class);
class_exists(Thing::class);

/**
 * Replace action.
 * 
 * A `replace action` is the act of editing a recipient by replacing
 * an old `object` with a new `object`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReplaceAction Schema.org type `ReplaceAction`
 * @version 1.0.0
 */
class ReplaceAction extends UpdateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\PIM\Product|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\Thing $replacee
     *   The `object` that is being replaced.  A sub property of `object`.
     */
    public null|Product|CreativeWork|Thing $replacee;

    /**
     * @var null|StoreCore\PIM\Product|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\Thing $replacer
     *   The `object` that replaces.  A sub property of `object`.
     */
    public null|Product|CreativeWork|Thing $replacer;
}
