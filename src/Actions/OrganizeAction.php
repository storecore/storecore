<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Organize action.
 * 
 * A _organize action_ is The act of manipulating, administering, supervising,
 * or controlling one or more objects.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/OrganizeAction Schema.org type `OrganizeAction`
 * @version 1.0.0
 */
class OrganizeAction extends Action
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
