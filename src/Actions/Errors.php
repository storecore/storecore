<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use Psr\Container\ContainerInterface;

use StoreCore\Database\AbstractModel;
use StoreCore\Database\ContainerException;
use StoreCore\Types\UUID;

/**
 * Errors repository.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Errors extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Loads an error from the repository.
     *
     * @param string $id
     * @return StoreCore\Actions\Error
     * @see https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
     * @throws StoreCore\Actions\ActionNotFoundException
     * @throws StoreCore\Database\ContainerException
     */
    #[\Override]
    public function get(string $id): Error
    {
        if (!UUID::validate($id)) {
            throw new ActionNotFoundException('Invalid UUID: ' . $id);
        }
        $id = str_replace('-', '', $id);
        $id = strtolower($id);

        try {
            $statement = $this->Database->prepare(
                'SELECT `status`, `type`, `title`, `detail`, `instance` FROM `sc_action_errors` WHERE `action_uuid` = UNHEX(?)'
            );
            if ($statement === false) throw new ContainerException();
            $statement->bindValue(1, $id, \PDO::PARAM_STR);
            if ($statement->execute() === false) throw new ContainerException();
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($statement);
            if ($result === false) throw new ActionNotFoundException();
            $result['action_uuid'] = $id;
            return $this->toObject($result);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if an error exists.
     *
     * @param string $id
     * @return bool
     * @see https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
     */
    public function has(string $id): bool
    {
        if (!UUID::validate($id)) return false;
        $id = str_replace('-', '', $id);
        $id = strtolower($id);

        try {
            $statement = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_action_errors` WHERE `action_uuid` = UNHEX(?)'
            );
            if ($statement !== false) {
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
                if ($statement->execute() !== false) {
                    $count = (int) $statement->fetchColumn(0);
                    $statement->closeCursor();
                    return $count === 1 ? true : false;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Saves an error to the repository.
     * 
     * @param StoreCore\Actions\Error $error
     * @return void
     */
    public function set(Error $error): void
    {
        try {
            $data = $this->toArray($error);
            $statement = $this->Database->prepare(
                'INSERT INTO `sc_action_errors` (`action_uuid`, `status`, `type`, `title`, `detail`, `instance`) VALUES (UNHEX(?), ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `status` = ?, `type` = ?, `title` = ?, `detail` = ?, `instance` = ?'
            );
            $statement->bindValue(1, $data['action_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(2, $data['status'], \PDO::PARAM_INT);
            $statement->bindValue(3, $data['type'], \PDO::PARAM_STR);
            $statement->bindValue(4, $data['title'], \PDO::PARAM_STR);
            $statement->bindValue(5, $data['detail'], \PDO::PARAM_STR);
            $statement->bindValue(6, $data['instance'], \PDO::PARAM_STR);
            $statement->bindValue(7, $data['status'], \PDO::PARAM_INT);
            $statement->bindValue(8, $data['type'], \PDO::PARAM_STR);
            $statement->bindValue(9, $data['title'], \PDO::PARAM_STR);
            $statement->bindValue(10, $data['detail'], \PDO::PARAM_STR);
            $statement->bindValue(11, $data['instance'], \PDO::PARAM_STR);
            if ($statement->execute() === false) throw new \RuntimeException('Could not execute prepared PDOStatement');

            $statement->closeCursor();
            $uuid = $data['action_uuid'];
            unset($data, $statement);

            $repository = new Repository($this->Registry);
            $action = $repository->get($uuid);
            $action->actionStatus = ActionStatusType::FailedActionStatus;
            $action->error = new ErrorThing($error);
            $repository->set($action);

            $this->Logger->info('Error in action ' . $uuid);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        } catch (\Exception $e) {
            $this->Logger->debug('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @internal
     * @param StoreCore\Actions\Error $error
     * @return array
     */
    private function toArray(Error $error): array
    {
        return [
            'action_uuid' => strtolower(str_replace('-', '', $error->getIdentifier()->__toString())),
            'status' => $error->status,
            'type' => $error->type,
            'title' => $error->title,
            'detail' => $error->detail,
            'instance' => $error->instance === null ? null : (string) $error->instance,
        ];
    }

    /**
     * @internal
     * @param array $data Key-value array.
     * @return StoreCore\Actions\Error
     */
    private function toObject(array $data): Error
    {
        $result = new Error();
        $error->setIdentifier($data['action_uuid']);
        $error->status = $data['status'];
        $error->type = $data['type'];
        $error->title = $data['title'];
        $error->detail = $data['detail'];
        $error->instance = $data['instance'];
        return $result;
    }
}
