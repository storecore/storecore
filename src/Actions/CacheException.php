<?php

namespace StoreCore\Actions;

use Psr\SimpleCache\CacheException as CacheExceptionInterface;

/**
 * Exception thrown on cache errors.
 *
 * @see https://github.com/php-fig/simple-cache/blob/master/src/CacheException.php
 * @see https://www.php.net/manual/en/class.exception.php
 * @see https://www.php.net/manual/en/class.throwable.php
 */
class CacheException extends \Exception
implements CacheExceptionInterface, \Throwable
{
}
