<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Wear action.
 * 
 * A _wear action_ is the act of dressing oneself in clothing.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/WearAction Schema.org type `WearAction`
 * @version 0.1.0
 */
class WearAction extends UseAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
