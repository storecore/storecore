<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Check in action.
 * 
 * A `check in action` is the act of an `agent` communicating (service provider,
 * social media, etc) their arrival by registering/confirming for a previously
 * reserved service (e.g. flight check in) or at a place (e.g. hotel), possibly
 * resulting in a `result` (boarding pass, etc).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CheckInAction Schema.org type `CheckInAction`
 * @version 0.1.0
 */
class CheckInAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
