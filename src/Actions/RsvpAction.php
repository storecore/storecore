<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * RSVP action.
 * 
 * An _RSVP action_ is the act act of notifying an event organizer
 * as to whether you expect to attend the event.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/RsvpAction Schema.org type `RsvpAction`
 * @version 1.0.0
 */
class RsvpAction extends InformAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
