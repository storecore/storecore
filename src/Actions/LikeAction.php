<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Like action.
 * 
 * A _like action_ is the act of expressing a positive sentiment about the
 * `object`.  An `agent` likes an `object` (a proposition, topic or theme)
 * with participants.  `LikeAction` and `DislikeAction` are opposites.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/LikeAction Schema.org type LikeAction
 * @version 1.0.0
 */
class LikeAction extends ReactAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
