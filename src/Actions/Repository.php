<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023, 2025 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use \Throwable;
use Psr\Container\ContainerInterface;
use StoreCore\{ContainerException, NotFoundException};
use StoreCore\Database\AbstractModel;
use StoreCore\Registry;
use StoreCore\Types\UUIDFactory;

/**
 * Actions repository.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Repository extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @param StoreCore\Actions\Cache $cache
     *   Storage for cached actions.
     */
    private readonly Cache $cache;

    /**
     * Creates a repository for actions.
     *
     * @param StoreCore\Registry $registry
     *   Global service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->cache = new Cache($this->Registry);
    }

    /**
     * Loads an action from the repository.
     *
     * @param string $id
     *   UUID of the `Action` as a string.
     *
     * @return StoreCore\Actions\Action
     */
    public function get(string $id): Action
    {
        try {
            return $this->cache->get($id);
        } catch (ActionNotFoundException $e) {
            throw new NotFoundException($e->getMessage(), $e->getCode(), $e);
        } catch (Throwable $t) {
            if (true !== $this->has($id)) {
                throw new NotFoundException($t->getMessage(), $t->getCode(), $t);
            } else {
                throw new ContainerException($t->getMessage(), $t->getCode(), $t);
            }
        }
    }

    /**
     * Checks if an action exists.
     *
     * @param string $id
     *   UUID of the `Action` as a string.
     *
     * @return bool
     *   Returns `true` if the `Action` exists, otherwise `false`.
     *
     * @uses \StoreCore\Actions\Cache::has()
     */
    public function has(string $id): bool
    {
        try {
            return $this->cache->has($id);
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Saves an action to the repository.
     *
     * @param StoreCore\Actions\Action &$action
     * @return void
     * @uses StoreCore\Types\UUIDFactory::randomUUID()
     */
    public function set(Action &$action): void
    {
        if (!$action->hasIdentifier()) {
            do {
                $uuid = UUIDFactory::randomUUID();
            }  while ($this->has((string) $uuid));
            $action->setIdentifier($uuid);
        }

        $this->cache->set((string) $action->getIdentifier(), $action);
    }
}
