<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Trade action.
 * 
 * A `trade action` is the act of participating in an exchange of goods and
 * services for monetary compensation.  An `agent` trades an `object`,
 * `Product` or `Service` with a `participant` in exchange for a one time
 * or periodic payment.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TradeAction Schema.org type TradeAction
 * @version 0.1.0
 */
class TradeAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
