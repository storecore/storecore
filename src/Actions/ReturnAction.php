<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Return action.
 * 
 * A _return action_ is the act of returning to the origin that which was
 * previously received (concrete objects) or taken (ownership).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReturnAction Schema.org type `ReturnAction`
 * @version 0.1.0
 */
class ReturnAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
