<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Confirm action.
 * 
 * A _confirm action_ is the act of notifying someone that a future
 * event/action is going to happen as expected.  `ConfirmAction` is
 * the logical opposite of `CancelAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ConfirmAction Schema.org type `ConfirmAction`
 * @version 1.0.0
 */
class ConfirmAction extends InformAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
