<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\PIM\Offer;

/**
 * Consume action.
 * 
 * A _consume action_ is the act of ingesting information, resources, or food.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ConsumeAction Schema.org type `ConsumeAction`
 * @version 0.2.0
 */
class ConsumeAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\Actions\ActionAccessSpecification|null $actionAccessibilityRequirement
     *   A set of requirements that must be fulfilled in order to perform an
     *   `Action`.  If more than one value is specified, fulfilling one set of
     *   requirements will allow the `Action` to be performed.
     */
    public ?ActionAccessSpecification $actionAccessibilityRequirement = null;

    /**
     * @var null|StoreCore\PIM\Offer $expectsAcceptanceOf
     *   An `Offer` which MUST be accepted before the user can perform the
     *   `Action`.  For example, the user MAY need to buy a movie before being
     *   able to watch it.
     */
    public ?Offer $expectsAcceptanceOf = null;
}
