<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Tip action.
 * 
 * A _tip action_ is the act of giving money voluntarily to a beneficiary in
 * recognition of services rendered.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TipAction Schema.org type `TipAction`
 * @version 0.1.0
 */
class TipAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
