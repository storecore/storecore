<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Register action.
 * 
 * A `register action` is the act of registering to be a user of a `Service`,
 * `Product`, or `WebPage`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/RegisterAction Schema.org type `RegisterAction`
 * @version 0.1.0
 */
class RegisterAction extends InteractAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
