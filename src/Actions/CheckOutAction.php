<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Check out action.
 * 
 * A `check out action` is the act of an `agent` communicating (service
 * provider, social media, etc) their departure of a previously reserved
 * `Service` (e.g. flight check in) or `Place` (e.g. hotel).  `CheckOutAction`
 * is the antonym of `CheckInAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CheckOutAction Schema.org type CheckOutAction
 * @version 0.1.0
 */
class CheckOutAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
