<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Prepend action.
 * 
 * An _prepend action_ is the act of inserting at the beginning if an ordered
 * collection.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/PrependAction Schema.org type `PrependAction`
 * @version 0.1.0
 */
class PrependAction extends InsertAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
