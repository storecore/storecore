<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Eat action.
 * 
 * An _eat action_ is the act of swallowing solid objects.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/EatAction Schema.org type `EatAction`
 * @version 0.1.0
 */
class EatAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
