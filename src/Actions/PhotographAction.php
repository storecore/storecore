<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Photograph action.
 * 
 * A `photograph action` is the act of capturing still images of objects using
 * a camera.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/PhotographAction Schema.org type `PhotographAction`
 * @version 0.1.0
 */
class PhotographAction extends CreateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
