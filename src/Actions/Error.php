<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use Psr\Http\Message\UriInterface;
use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Types\URL;

/**
 * Action error in RFC 7807 format.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.rfc-editor.org/rfc/rfc7807 RFC 7807: Problem Details for HTTP APIs
 * @see     https://schema.org/error Schema.org `error` property of an `Action`
 * @version 1.0.0-alpha.1
 */
class Error implements IdentityInterface, \JsonSerializable, \Stringable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $detail
     *   A human-readable explanation specific to this occurrence of the problem.
     */
    public ?string $detail = null;

    /**
     * @var null|Psr\Http\Message\UriInterface $instance
     *   A URI reference that identifies the specific occurrence of the problem.
     *   It may or may not yield further information if dereferenced.
     */
    protected ?URL $instance = null;

    /**
     * @var int $status
     *   The HTTP status code (RFC 7231, Section 6) generated by the origin
     *   server for this occurrence of the problem.  Defaults to `501` for a
     *   `Not Implemented` server error.
     */
    public int $status = 501;

    /**
     * @var null|string $title
     *   A short, human-readable summary of the problem type.  It SHOULD NOT change
     *   from occurrence to occurrence of the problem, except for purposes of
     *   localization (e.g., using proactive content negotiation; see RFC 7231,
     *   Section 3.4).
     */
    public ?string $title = null;

    /**
     * @var Psr\Http\Message\UriInterface|null $type
     *   A URI reference (RFC 3986) that identifies the problem type.  The RFC 7807
     *   specification encourages that, when dereferenced, it provide human-readable
     *   documentation for the problem type (e.g., using HTML).  When this member is
     *   not present, its value is assumed to be “about:blank”.
     */
    protected ?URL $type = null;

    /**
     * Generic accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'instance' => $this->instance,
            'type' => $this->type,
            default => throw new \ValueError('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Generic mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'instance' => $this->setInstance($value),
            'type' => $this->setType($value),
            default => throw new \ValueError('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Converts the error object to a pretty-printed JSON string,
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $result = json_encode($this, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $result = str_replace('    ', "\t", $result);
        $result = str_replace("\t", '  ', $result);
        return $result;
    }

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return array
     * @see https://www.rfc-editor.org/rfc/rfc7807#section-3.1 Members of a Problem Details Object
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize(): array
    {
        $result = [];
        $result['type'] = empty($this->type) ? 'about:blank' : (string) $this->type;
        if (!empty($this->title)) $result['title'] = $this->title;
        $result['status'] = $this->status;
        if (!empty($this->detail)) $result['detail'] = $this->detail;
        if (!empty($this->instance)) $result['instance'] = (string) $this->instance;
        return $result;
    }

    /**
     * Sets the error instance URL.
     *
     * @internal
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string
     * @return void
     */
    protected function setInstance(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL((string) $url);
        $this->instance = $url;
    }

    /**
     * Sets the error type URL.
     *
     * @internal
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string
     * @return void
     */
    protected function setType(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL((string) $url);
        $this->type = $url;
    }
}
