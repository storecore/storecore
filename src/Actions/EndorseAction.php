<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Endorse action.
 * 
 * An _endorse action_ is the act where an `agent` approves, certifies, likes,
 * supports, or sanctions an `object`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/EndorseAction Schema.org type EndorseAction
 * @version 1.0.0
 */
class EndorseAction extends ReactAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
