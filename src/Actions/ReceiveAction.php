<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Receive action.
 * 
 * A _receive action_ is the act of physically/electronically taking delivery
 * of an object that has been transferred from an origin to a destination.
 * Reciprocal of `SendAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReceiveAction Schema.org type `ReceiveAction`
 * @version 0.1.0
 */
class ReceiveAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
