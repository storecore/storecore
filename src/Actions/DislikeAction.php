<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Dislike action.
 * 
 * A _dislike action_ is the act of expressing a negative sentiment about the
 * `object`.  An `agent` dislikes an `object` (a proposition, topic or theme)
 * with participants.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DislikeAction Schema.org type DislikeAction
 * @version 1.0.0
 */
class DislikeAction extends ReactAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
