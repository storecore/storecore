<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Schema.org search action.
 *
 * A _search action_ is the act of searching for an object.
 * `SearchAction` generally leads to a `FindAction`, but not necessarily.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/SearchAction Thing > Action > SearchAction
 * @version 0.2.1
 *
 * @see https://developers.google.com/search/docs/data-types/sitelinks-searchbox
 *      Sitelinks search box - Google Search Central - Google Developers
 */
class SearchAction extends Action
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * @param null|string $query
     *   A sub property of `instrument`.  The query used on this action.
     */
    public ?string $query = null;

    /**
     * @var null|string $query-input
     *   OPTIONAL `query-input` parameter for the search URI template.
     */
    public ?string $queryInput = null;

    /**
     * Sets the search query input parameters.
     *
     * @param string $query_input
     * @return void
     */
    public function setQueryInput(string $query_input = 'required name=search_term_string'): void
    {
        $this->queryInput = $query_input;
    }

    #[\Override]
    public function toArray(): array
    {
        $result = parent::toArray();

        // Replace PHP property name 'queryInput' with JSON-LD array key 'query-input'.
        if (isset($result['queryInput'])) {
            $result['query-input'] = $result['queryInput'];
            unset($result['queryInput']);
        }

        return $result;
    }
}
