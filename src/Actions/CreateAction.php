<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Create action.
 * 
 * A _create action_ is the act of deliberately creating, producing, generating,
 * or building a `result` out of the `agent`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CreateAction Schema.org type CreateAction
 * @version 1.0.0
 */
class CreateAction extends Action
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
