<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Activate action.
 * 
 * An _activate action_ is the act of starting or activating a device or
 * application (e.g. starting a timer or turning on a flashlight).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ActivateAction Schema.org type `ActivateAction`
 * @version 1.0.0
 */
class ActivateAction extends ControlAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
