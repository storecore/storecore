<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Person;

/**
 * Vote action.
 * 
 * A _vote action_ is the act of expressing a preference from a fixed, finite,
 * or structured set of choices or options.  The `VoteAction` is a subtype of
 * a `ChooseAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/VoteAction Schema.org type `VoteAction`
 * @version 0.1.1
 */
class VoteAction extends ChooseAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.1';

    /**
     * @var null|StoreCore\CRM\Person $candidate
     *   A sub property of `Action.object`.  The candidate subject of this action.
     */
    public ?Person $candidate = null;
}
