<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\{Person, Organization};

/**
 * Buy action.
 * 
 * A `buy action` is the act of giving money to a `seller` in exchange for
 * goods or services rendered.  An `agent` buys an `object`, `Product`, or
 * `Service` from a `seller` for a `price`.  Reciprocal of `SellAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/BuyAction Schema.org type `BuyAction`
 * @version 0.1.0
 */
class BuyAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var StoreCore\CRM\Person|StoreCore\CRM\Organization|null $seller
     *   An entity which offers (sells, leases, lends, loans) the services or goods.
     *   A `seller` may also be a `provider`.
     */
    public null|Person|Organization $seller = null;
}
