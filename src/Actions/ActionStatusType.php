<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Action status type.
 *
 * The status of an `Action` indicates the current dispostion of the `Action`.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ActionStatusType Schema.org enumeration type `ActionStatusType`
 * @version 23.0.0
 */
enum ActionStatusType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '23.0.0';

    case ActiveActionStatus    = 'https://schema.org/ActiveActionStatus';
    case CompletedActionStatus = 'https://schema.org/CompletedActionStatus';
    case FailedActionStatus    = 'https://schema.org/FailedActionStatus';
    case PotentialActionStatus = 'https://schema.org/PotentialActionStatus';
}
