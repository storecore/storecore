<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Transfer action.
 * 
 * A _transfer action_ is the act of transferring/moving (abstract or concrete)
 * animate or inanimate objects from one place to another.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TransferAction Schema.org type `TransferAction`
 * @version 0.1.0
 */
class TransferAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
