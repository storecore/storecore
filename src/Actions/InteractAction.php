<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Interact action.
 *
 * An _interact action_ is the act of interacting with another person or
 * organization.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/InteractAction Schema.org type `InteractAction`
 * @version 1.0.0
 */
class InteractAction extends Action
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This subclass is tentatively considered
     *   to be stable because it is identical to the extented class `Action`.
     */
    public const string VERSION = '1.0.0';
}
