<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Suspend action.
 * 
 * A `suspend action` is the act of momentarily pausing a device or application
 * (e.g. pause music playback or pause a timer).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/SuspendAction Schema.org type SuspendAction
 * @version 1.0.0
 */
class SuspendAction extends ControlAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
