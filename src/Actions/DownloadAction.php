<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Download action.
 * 
 * A _download action_ is the act of downloading an `object`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DownloadAction Schema.org type `DownloadAction`
 * @version 0.1.0
 */
class DownloadAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
