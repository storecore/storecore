<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Append action.
 * 
 * An _append action_ is the act of inserting at the end if an ordered collection.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AppendAction Schema.org type `AppendAction`
 * @version 0.1.0
 */
class AppendAction extends InsertAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
