<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\SingletonInterface;
use StoreCore\Types\UUID;

/**
 * Actions object storage.
 *
 * @internal
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class ObjectStorage extends \SplObjectStorage implements
    \Countable,
    SingletonInterface,
    \Traversable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var ObjectStorage|null $Instance
     *   Single instance of the object storage or
     *   `null` if there is no instance yet.
     */
    private static ?ObjectStorage $Instance = null;

    // Disable object instantiation and cloning.
    private function __construct() {}
    private function __clone() {}

    /**
     * Adds an object in the storage.
     *
     * @param object $object The object to add.
     * @param mixed $info The data to associate with the object.
     * @return void No value is returned.
     * @see https://www.php.net/manual/en/splobjectstorage.attach.php
     */
    public function attach(object $object, mixed $info = null): void
    {
        if ($info === null && $object->hasIdentifier()) {
            $info = $object->getIdentifier()->__toString();
            $info = str_replace('-', '', $info);
            $info = strtolower($info);
        }

        foreach ($this as $object) {
            if ($this->getInfo() === $info) $this->detach($object);
        }
        parent::attach($object, $info);
    }

    /**
     * Gets an object from the storage.
     *
     * @param string $id UUID as a string
     * @return StoreCore\Actions\Action
     * @throws Psr\Container\NotFoundExceptionInterface
     */
    public function get(string $id): Action
    {
        if (!UUID::validate($id)) {
            throw new ActionNotFoundException();
        }
        $id = str_replace('-', '', $id);
        $id = strtolower($id);

        $result = null;
        foreach ($this as $object) {
            if ($this->getInfo() === $id) $result = $object;
        }
        if ($result === null) throw new ActionNotFoundException();
        return $result;
    }

    /**
     * Gets the single instance of the object storage.
     *
     * @param void
     * @return self
     */
    final public static function getInstance(): self
    {
        if (null === self::$Instance) self::$Instance = new ObjectStorage();
        return self::$Instance;
    }

    /**
     * Checks if an action is stored in the object storage.
     *
     * @param string $id UUID as a string
     * @return bool
     * @uses StoreCore\Types\UUID::validate()
     */
    public function has(string $id): bool
    {
        if (
            true !== UUID::validate($id)
            || 0 === count(self::$Instance)
        ) {
            return false;
        }

        $id = str_replace('-', '', $id);
        $id = strtolower($id);

        foreach ($this as $object) {
            if ($this->getInfo() === $id) {
                return true;
            }
        }
        return false;
    }
}
