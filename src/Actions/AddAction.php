<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Add action.
 * 
 * An _add action_ is the act of editing by adding an object to a collection.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AddAction Schema.org type AddAction
 * @version 1.0.0
 */
class AddAction extends UpdateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
