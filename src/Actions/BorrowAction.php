<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Borrow action.
 * 
 * A _borrow action_ is the act of obtaining an `object` under an agreement to
 * return it at a later date.  Reciprocal of `LendAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/BorrowAction Schema.org type `BorrowAction`
 * @version 0.1.0
 */
class BorrowAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
