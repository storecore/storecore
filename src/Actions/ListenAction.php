<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Listen action.
 * 
 * An _listen action_ is the act of consuming audio content.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ListenAction Schema.org type `ListenAction`
 * @version 0.1.0
 */
class ListenAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
