<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Share action.
 * 
 * A `share action` is the act of distributing content to people for their
 * amusement or edification.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ShareAction Schema.org type `ShareAction`
 * @version 0.1.0
 */
class ShareAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
