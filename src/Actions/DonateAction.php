<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Donate action.
 * 
 * A _donate action_ is the act of providing goods, services, or money without
 * compensation, often for philanthropic reasons.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DonateAction Schema.org type `DonateAction`
 * @version 0.1.0
 */
class DonateAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
