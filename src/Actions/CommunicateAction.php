<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\{Audience, Organization, Person};
use StoreCore\Types\ContactPoint;

/**
 * Communicate action.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CommunicateAction Schema.org type `CommunicateAction`
 * @version 0.2.0
 */
class CommunicateAction extends InteractAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|Audience|ContactPoint|Organization|Person $recipient
     *   A sub property of `participant`.  The `participant` who is
     *   at the receiving end of the `Action`.
     */
    public null|Audience|ContactPoint|Organization|Person $recipient = null;
}
