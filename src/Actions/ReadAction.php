<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Read action.
 * 
 * An _read action_ is the act of consuming written content.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReadAction Schema.org type `ReadAction`
 * @version 0.1.0
 */
class ReadAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
