<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Find action.
 * 
 * A _find action_ is the act of finding an object.  `FindAction` is generally
 * lead by a `SearchAction`, but not necessarily.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/FindAction Schema.org type Action
 * @version 1.0.0
 */
class FindAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
