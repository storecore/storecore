<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Install action.
 * 
 * An _install action_ is the act of installing an application.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/InstallAction Schema.org type InstallAction
 * @version 1.0.0
 */
class InstallAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
