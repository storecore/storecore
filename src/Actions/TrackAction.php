<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Track action.
 * 
 * An `agent` tracks an `object` for updates.  Unlike `FollowAction` and
 * `SubscribeAction`, `TrackAction` refers to the interest on the `location`
 * of innanimate objects.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/TrackAction Schema.org type `TrackAction`
 * @version 1.0.0-alpha.1
 */
class TrackAction extends FindAction implements \JsonSerializable, \Stringable
{
    use \StoreCore\OML\DeliveryMethodTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';
}
