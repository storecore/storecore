<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Delete action.
 * 
 * An _delete action_ is the act of editing a recipient by removing one of its
 * objects.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DeleteAction Schema.org type `DeleteAction`
 * @version 1.0.0
 */
class DeleteAction extends UpdateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
