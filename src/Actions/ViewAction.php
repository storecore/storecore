<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * View action.
 *
 * A _view action_ is the act of consuming static visual content.
 * This subclass is identical to its parent class `ConsumeAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ViewAction Schema.org type `ViewAction`
 * @version 0.1.0
 */
class ViewAction extends ConsumeAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
