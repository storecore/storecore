<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Check action.
 * 
 * An `agent` inspects, determines, investigates, inquires, or examines
 * an `object`’s accuracy, quality, condition, or state.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CheckAction Schema.org type CheckAction
 * @version 1.0.0
 */
class CheckAction extends FindAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
