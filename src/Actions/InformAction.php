<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Inform action.
 *
 * An _inform action_ is the act of notifying someone of information pertinent
 * to them, with no expectation of a response.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/InformAction Schema.org type `InformAction`
 * @version 1.0.0-alpha.1
 */
class InformAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Types\Event $target
     *   Upcoming or past event associated with this action.
     */
    public ?Event $event = null;
}
