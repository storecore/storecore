<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Reply action.
 * 
 * A _reply action_ is the act of responding to a question/message asked/sent
 * by the `object`.  Related to `AskAction`: an `AskAction` appears generally
 * as an origin of a `ReplyAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReplyAction Schema.org type `ReplyAction`
 * @version 0.1.0
 */
class ReplyAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
