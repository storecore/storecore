<?php

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Befriend action.
 * 
 * A _befriend action_ is the act of forming a personal connection with
 * someone (`object`) mutually/bidirectionally/symmetrically.
 * 
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://schema.org/BefriendAction Schema.org type `BefriendAction`
 * @version   0.1.0
 */
class BefriendAction extends InteractAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
