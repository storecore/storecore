<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Assess action.
 * 
 * An _assess action_ is the act of forming one’s opinion, reaction or sentiment.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AssessAction Schema.org type `AssessAction`
 * @version 1.0.0
 */
class AssessAction extends Action implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
