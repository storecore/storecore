<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Lose action.
 * 
 * An _lose action_ is the act of being defeated in a competitive activity.
 * A `LoseAction` is the opposite of a `WinAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/LoseAction Schema.org type `LoseAction`
 * @version 1.0.0
 */
class LoseAction extends AchieveAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
