<?php

namespace StoreCore\Actions;

use Psr\SimpleCache\CacheException as CacheExceptionInterface;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionInterface;

/**
 * Exception thrown on an invalid cache key.
 *
 * @see https://github.com/php-fig/simple-cache/blob/master/src/InvalidArgumentException.php
 */
class CacheKeyException extends CacheException
implements CacheExceptionInterface, InvalidArgumentExceptionInterface, \Throwable
{
}
