<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Deactivate action.
 * 
 * A `deactivate action` is the act of stopping or deactivating a device
 * or application (e.g. stopping a timer or turning off a flashlight).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DeactivateAction Schema.org type DeactivateAction
 * @version 1.0.0
 */
class DeactivateAction extends ControlAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
