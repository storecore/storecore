<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Draw action.
 * 
 * A `draw action` is the act of producing a visual/graphical representation of
 * an object, typically with a pen/pencil and paper as instruments.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/DrawAction Schema.org type `DrawAction`
 * @version 0.1.0
 */
class DrawAction extends CreateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
