<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\CMS\AbstractCreativeWork;
use StoreCore\CRM\{Organization, Person};
use StoreCore\PIM\Product;
use StoreCore\Types\{DateTime, Time};
use StoreCore\Types\EntryPoint;
use StoreCore\Types\Thing;

/**
 * Action.
 * 
 * An _action_ is performed by a direct `agent` and indirect `participant` upon
 * a direct `object`, and optionally happens at a `location` with the help of
 * an inanimate `instrument`.  The execution of the `Action` MAY produce a
 * `result`.  Specific `Action` subtype documentation specifies the exact
 * expectation of each argument/role.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/Action Schema.org type `Action`
 * @see     https://developers.google.com/gmail/markup/actions/declaring-actions Declare Actions, Gmail API
 * @version 1.0.0-alpha.1
 */
class Action extends Thing implements
    IdentityInterface,
    \JsonSerializable,
    \Stringable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Types\ActionStatusType $actionStatus
     *   Indicates the current disposition of the action.
     */
    public ?ActionStatusType $actionStatus = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|null
     *   The direct performer or driver of the action (animate or inanimate).
     *   e.g. “John [agent] wrote a book [result].”
     */
    public Organization|Person|null $agent = null;

    /**
     * @var StoreCore\Types\DateTime|StoreCore\Types\Time|null $endTime
     *   The `endTime` of something.  For a reserved event or service
     *   (e.g. `FoodEstablishmentReservation`), the time that it is expected
     *   to end.  For actions that span a period of time, when the `Action`
     *   was performed.  Example: John [agent] wrote a book [result] from
     *   January [startTime] to December [endTime].  For media, including audio
     *   and video, it’s the time offset of the end of a clip within a larger file.
     */
    protected DateTime|Time|null $endTime = null;

    /**
     * @var StoreCore\Types\Thing|null $error
     *   For failed actions, more information on the cause of the failure.
     */
    public ?Thing $error = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person|StoreCore\Types\Thing $instrument
     *   The object that helped the `agent` perform the `Action`.
     *   Example: John [agent] wrote a book [result] with a pen [instrument].
     */
    public null|Organization|Person|Thing $instrument;

    /**
     * @var StoreCore\Geo\Place|StoreCore\Types\PostalAddress|StoreCore\Types\VirtualLocation|string|null $location
     *   The `location` of, for example, where an `Event` is happening, where
     *   an `Organization` is located, or where an `Action` takes place.
     */
    public Place|PostalAddress|VirtualLocation|string|null $location = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|StoreCore\PIM\Product|StoreCore\Types\Thing|null $object
     *   The `object` upon which the `Action` is carried out, whose state is
     *   kept intact or changed.  Also known as the semantic roles _patient_,
     *   _affected_ or _undergoer_ (which change their state) or _theme_
     *   (which doesn't).  Example: John [agent] read a book [object].
     */
    public Organization|Person|Product|Thing|AbstractCreativeWork|null $object = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|null $participant
     *   Other co-agents that participated in the action indirectly.  Example:
     *   John [agent] wrote a book [result] with Steve [participant].
     */
    public Organization|Person|null $participant = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person|null $provider
     *   The service provider, service operator, or service performer; the goods
     *   producer.  Another party (a seller) MAY offer those services or goods
     *   on behalf of the provider.  A `provider` may also serve as the seller.
     *   Supersedes `carrier`.
     */
    public Organization|Person|null $provider = null;

    /**
     * @var null|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\Thing $result
     *   The `result` produced in the `Action`.  Example: John [agent] wrote
     *   a book [result].
     */
    public null|AbstractCreativeWork|Thing $result = null;

    /**
     * @var StoreCore\Types\DateTime|StoreCore\Types\Time|null $startTime
     *   The `startTime` of something.  For a reserved `Event` or `Service`
     *   (e.g. `FoodEstablishmentReservation`), the time that it is expected
     *   to start.  For actions that span a period of time, when the `Action`
     *   was performed.  Example: John [agent] wrote a book [result] from
     *   January [startTime] to December [endTime].  For media, including
     *   audio and video,  it’s the time offset of the start of a clip within
     *   a larger file.
     */
    protected DateTime|Time|null $startTime = null;

    /**
     * @var StoreCore\Types\EntryPoint|array|null $target
     *   Indicates a `target` `EntryPoint` for an `Action`.
     */
    protected EntryPoint|array|null $target = null;


    /**
     * Sets the action end time.
     *
     * @param StoreCore\Types\DateTime|StoreCore\Types\Time|string $end_time
     * @return void
     */
    public function setEndTime(DateTime|Time|string $end_time): void
    {
        if (\is_string($end_time)) $end_time = new DateTime($end_time);
        $this->endTime = $end_time;
    }

    /**
     * Sets the action start time.
     *
     * @param StoreCore\Types\DateTime|StoreCore\Types\Time|string $start_time
     * @return void
     */
    public function setStartTime(DateTime|Time|string $start_time): void
    {
        if (\is_string($start_time)) $start_time = new DateTime($start_time);
        $this->startTime = $start_time;
    }

    /**
     * Sets the action target entrypoint.
     *
     * @param StoreCore\Types\EntryPoint|StoreCore\Types\URL|string|array $entry_point
     * @return void
     * @uses StoreCore\Types\EntryPoint::__construct
     */
    public function setTarget(EntryPoint|URL|string|array $entry_point): void
    {
        if (
            $entry_point instanceof EntryPoint
            || \is_array($entry_point)
        ) {
            $this->target = $entry_point;
        } else {
            $this->target = new EntryPoint();
            $this->target->urlTemplate = (string) $entry_point;
        }
    }
}
