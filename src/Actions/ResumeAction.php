<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Resume action.
 * 
 * A `resume action` is the act of resuming a device or application which was
 * formerly paused (e.g. resume music playback or resume a timer).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ResumeAction Schema.org type ResumeAction
 * @version 1.0.0
 */
class ResumeAction extends ControlAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
