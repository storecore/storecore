<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Pay action.
 * 
 * An `agent` pays a `price` to a `participant`.
 * 
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/PayAction Schema.org type PayAction
 * @version 0.1.0
 */
class PayAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
