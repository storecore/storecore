<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License

 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Quote action.
 * 
 * An `agent` quotes/estimates/appraises an object/product/service with
 * a `price` at a location/store.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/QuoteAction Schema.org type `QuoteAction`
 * @version 0.1.0
 */
class QuoteAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
