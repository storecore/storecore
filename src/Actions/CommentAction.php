<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Comment action.
 * 
 * A _comment action_ is the act of conveying information to another `Person`
 * via a communication medium (`instrument`) such as speech, email, or telephone
 * conversation.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CommentAction Schema.org type `CommentAction`
 * @version 0.1.0
 */
class CommentAction extends CommunicateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
