<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Rent action.
 * 
 * A _rent action_ is the act of giving money in return for temporary use, but
 * not ownership, of an object such as a vehicle or property.  For example, an
 * `agent` rents a property from a `landlord` in exchange for a periodic payment.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/RentAction Schema.org type `RentAction`
 * @version 0.1.0
 */
class RentAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
