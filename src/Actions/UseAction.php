<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Use action.
 * 
 * An _use action_ is the act of applying an object to its intended purpose.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/UseAction Schema.org type `UseAction`
 * @version 0.1.0
 */
class UseAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
