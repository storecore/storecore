<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License

 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Cook action.
 * 
 * A `cook action` is the act of producing/preparing food.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CookAction Schema.org type `CookAction`
 * @version 0.1.0
 */
class CookAction extends CreateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
