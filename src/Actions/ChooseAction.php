<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Choose action.
 * 
 * A _choose action_ is the act of expressing a preference from a set of options
 * or a large or unbounded set of choices/options.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ChooseAction Schema.org type `ChooseAction`
 * @version 0.1.0
 */
class ChooseAction extends AssessAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
