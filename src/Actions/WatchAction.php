<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Watch action.
 * 
 * A _watch action_ is the act of consuming dynamic/moving visual content.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/WatchAction Schema.org type `WatchAction`
 * @version 1.0.0
 */
class WatchAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
