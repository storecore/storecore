<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CMS\Review;

class_exists(Action::class);
class_exists(AssessAction::class);
class_exists(Review::class);

/**
 * Review action.
 * 
 * A _review action_ is the act of producing a balanced opinion about the
 * `object` for an `audience`.  An `agent` reviews an `object` with
 * participants resulting in a `review`.
 * 
 * @api
 * @package   StoreCore\Core
 * @see       https://schema.org/ReviewAction Schema.org type `ReviewAction`
 * @version   1.0.0
 */
class ReviewAction extends AssessAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @param null|StoreCore\CMS\Review $resultReview
     *   A sub property of `result`.  The `Review` that resulted
     *   in the performing of the `ReviewAction`.
     */
    public ?Review $resultReview = null;
}
