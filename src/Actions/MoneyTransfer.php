<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Money transfer.
 * 
 * A _money transfer_ is the act of transferring money from one place to
 * another place.  This MAY occur electronically or physically.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/MoneyTransfer Schema.org type `MoneyTransfer`
 * @version 0.1.0
 */
class MoneyTransfer extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
