<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Lend action.
 * 
 * A _lend action_ is the act of providing an `object` under an agreement that
 * it will be returned at a later date.  Reciprocal of `BorrowAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/LendAction Schema.org type `LendAction`
 * @version 0.1.0
 */
class LendAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
