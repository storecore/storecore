<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Reserve action.
 * 
 * A _reserve action_ reserves a concrete object.  Unlike `ScheduleAction`,
 * `ReserveAction` reserves concrete objects (e.g. a table, a hotel) towards
 * a time slot or spatial allocation.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ReserveAction Schema.org type `ReserveAction`
 * @version 1.0.0
 */
class ReserveAction extends PlanAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
