<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Film action.
 * 
 * A `film action` is the act of capturing sound and moving images on film,
 * video, or digitally.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/FilmAction Schema.org type `FilmAction`
 * @version 0.1.0
 */
class FilmAction extends CreateAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
