<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Cancel action.
 * 
 * A _cancel action_ is the act of asserting that a future `Event` or `Action`
 * is no longer going to happen.  `ConfirmAction` is the antonym of
 * `CancelAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/CancelAction Schema.org type CancelAction
 * @version 1.0.0
 */
class CancelAction extends PlanAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
