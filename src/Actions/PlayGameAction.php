<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Play game action.
 * 
 * An _play game action_ is the act of playing a video game.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/PlayGameAction Schema.org type `PlayGameAction`
 * @version 0.1.0
 */
class PlayGameAction extends ConsumeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
