<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Give action.
 * 
 * A _give action_ is the act of transferring ownership of an object to a
 * destination.  Reciprocal of `TakeAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/GiveAction Schema.org type `GiveAction`
 * @version 0.1.0
 */
class GiveAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
