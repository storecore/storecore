<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Send action.
 * 
 * A _send action_ is the act of physically/electronically dispatching an
 * object for transfer from an origin to a destination.  `ReceiveAction` is the
 * reciprocal of `SendAction`.  Unlike `GiveAction`, `SendAction` does not
 * imply the transfer of ownership (e.g. I can send you my laptop, but I'm not
 * necessarily giving it to you).
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/SendAction Schema.org type `SendAction`
 * @version 0.1.0
 */
class SendAction extends TransferAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
