<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

/**
 * Sell action.
 * 
 * A _sell action_ is the act of taking money from a `buyer` in exchange for
 * goods or services rendered.  An `agent` sells an `object`, `Product`, or
 * `Service` to a `buyer` for a `Price`.  Reciprocal of `BuyAction`.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/SellAction Schema.org type `SellAction`
 * @version 0.2.0
 */
class SellAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|StoreCore\CRM\Person|StoreCore\CRM\Organization $buyer
     *   The `participant`, `Person`, or `Organization` that bought the `object`.
     *   A sub property of `Action.participant`.
     */
    public null|Person|Organization $buyer = null;
}
