<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Win action.
 * 
 * An _lose action_ is the act of achieving victory in a competitive activity.
 * The `WinAction` is the opposite of a `LoseAction`.
 * 
 * @api
 * @package   StoreCore\Core
 * @see       https://schema.org/WinAction Schema.org type WinAction
 * @version   1.0.0
 */
class WinAction extends AchieveAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
