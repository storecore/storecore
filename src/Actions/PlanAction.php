<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Plan action.
 * 
 * A _plan action_ is the act of planning the execution of an
 * event/task/action/reservation/plan to a future date.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/PlanAction Schema.org type `PlanAction`
 * @version 1.0.0
 */
class PlanAction extends OrganizeAction
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var StoreCore\Types\DateTime|null $scheduledTime
     *   The time the object is scheduled to.
     */
    public ?DateTime $scheduledTime = null;
}
