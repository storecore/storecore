<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Actions;

/**
 * Order action.
 * 
 * An `agent` orders an `object`, `Product`, or `Service` to be delivered
 * or sent.
 * 
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/OrderAction Schema.org type OrderAction
 * @version 1.0.0
 */
class OrderAction extends TradeAction implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
