<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;

use StoreCore\CRM\{Organization, OrganizationProxy};
use StoreCore\CRM\{Person, PersonProxy};
use StoreCore\OML\OrderProxy;
use StoreCore\PIM\{Product, ProductProxy};
use StoreCore\PIM\{Service, ServiceProxy};

/**
 * Proxy factory.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class ProxyFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Creates a proxy for an object with an identity.
     *
     * @param StoreCore\IdentityInterface $object
     *   Entity or aggregate that implements the `IdentityInterface`.
     *
     * @return StoreCore\ProxyInterface
     *
     * @throws StoreCore\DependencyException
     *   Throws a dependency logic exception if the `$object` has
     *   no known identity.
     */
    public static function createFromIdentityInterface(IdentityInterface $object): ProxyInterface
    {
        if (!$object->hasIdentifier()) {
            throw new DependencyException('Unknown entity: object has no identity.', time());
        }

        if ($object instanceof Order) {
            return static::createFromOrder($object);
        } elseif ($object instanceof Organization) {
            return static::createFromOrganization($object);
        } elseif ($object instanceof Product) {
            return static::createFromProduct($object);
        } elseif ($object instanceof Service) {
            return static::createFromService($object);
        }

        $class = new ReflectionClass($object);
        if ($class->hasProperty('name')) {
            return new Proxy($object->getIdentifier(), $class->getShortName(), (string) $object->name);
        } else {
            return new Proxy($object->getIdentifier(), $class->getShortName());
        }
    }

    /**
     * Converts a Order to a OrderProxy.
     *
     * @param StoreCore\Order $order
     *   `Order` entity object.
     *
     * @return StoreCore\OML\OrderProxy
     *   Proxy for the `Order`.
     */
    public static function createFromOrder(Order $order): OrderProxy
    {
        $class = new ReflectionClass($order);
        if (!$order->hasIdentifier()) {
            throw new DependencyException('Unknown order: ' . $class->getShortName() . ' has no identity.');
        }
        return new OrderProxy($order->getIdentifier(), $class->getShortName(), $order->orderNumber, $order->confirmationNumber);
    }

    /**
     * Converts an Organization to an OrganizationProxy.
     *
     * @param StoreCore\CRM\Organization $organization
     *   `Organization` entity object.
     *
     * @return StoreCore\CRM\OrganizationProxy
     *   Proxy for the `Organization`.
     */
    public static function createFromOrganization(Organization $organization): OrganizationProxy
    {
        $class = new ReflectionClass($organization);
        if (!$organization->hasIdentifier()) {
            throw new DependencyException('Unknown organization: ' . $class->getShortName() . ' has no identity.');
        }

        if (!empty($organization->name)) {
            return new OrganizationProxy($organization->getIdentifier(), $class->getShortName(), $organization->name);
        } else {
            return new OrganizationProxy($organization->getIdentifier(), $class->getShortName());
        }
    }

    /**
     * Converts a Product to a ProductProxy.
     *
     * @param StoreCore\PIM\Product $product
     *   `Product` entity object.
     *
     * @return StoreCore\PIM\ProductProxy
     *   Proxy for the `Product`.
     */
    public static function createFromProduct(Product $product): ProductProxy
    {
        $class = new ReflectionClass($product);
        if (!$product->hasIdentifier()) {
            throw new DependencyException('Unknown product: ' . $class->getShortName() . ' has no identity.');
        } else {
            return new ProductProxy($product->getIdentifier(), $class->getShortName(), $product->name);
        }
    }

    /**
     * Converts a Service to a ServiceProxy.
     *
     * @param StoreCore\PIM\Service $service
     *   `Service` entity object.
     *
     * @return StoreCore\PIM\ServiceProxy
     *   Proxy for the `Service`.
     */
    public static function createFromService(Service $service): ServiceProxy
    {
        $class = new ReflectionClass($service);
        if (!$service->hasIdentifier()) {
            throw new DependencyException('Unknown service: ' . $class->getShortName() . ' has no identity.');
        } else {
            return new ServiceProxy($service->getIdentifier(), $class->getShortName(), $service->name);
        }
    }
}
