<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Pricing strategy or pricing policy.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 * @see     https://en.wikipedia.org/wiki/Strategy_pattern Strategy pattern, Wikipedia
 */
interface PricingStrategyInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Returns the price of something as money.
     *
     * @param void
     * @return StoreCore\Money
     */
    public function getPrice(): Money;
}
