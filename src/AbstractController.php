<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Abstract MVC controller.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
abstract class AbstractController
{
    /**
     * @var StoreCore\Registry $Registry
     *   Global service locator.
     */
    protected Registry $Registry;

    /**
     * @param StoreCore\Registry $registry
     * @return void
     */
    public function __construct(Registry $registry)
    {
        $this->Registry = $registry;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function __get(string $key): mixed
    {
        return $this->Registry->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function __set(string $key, mixed $value): void
    {
        $this->Registry->set($key, $value);
    }
}
