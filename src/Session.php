<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\SimpleCache\CacheInterface;
use StoreCore\FileSystem\{CacheException, InvalidArgumentException};

/**
 * Session.
 *
 * The session is used as a temporary cache for the storage of cacheable values
 * and value objects.  The observer design pattern is used to inform observers
 * of changes in the session state.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-16/ PSR-16: Common Interface for Caching Libraries
 * @see     https://www.php.net/manual/en/class.splsubject.php The SplSubject interface, PHP Manual
 * @uses    StoreCore\SubjectTrait
 * @version 1.0.0-alpha.1
 */
class Session implements CacheInterface, \SplSubject
{
    use SubjectTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Creates and (re)starts a session.
     *
     * @param int $idle_timeout
     *   Idle timeout in seconds.  Defaults to `900` seconds (15 minutes).
     *   Common idle timeouts are 2 to 5 minutes for high-value applications
     *   and 15 to 30 minutes for low risk applications.
     *
     * @throws \ValueError
     *   Throws a value error if `$idle_timeout` is less than `60` seconds.
     */
    public function __construct(int $idle_timeout = 900)
    {
        if ($idle_timeout < 60) {
            throw new \ValueError();
        }

        if (session_status() === \PHP_SESSION_NONE && !headers_sent()) {
            ini_set('session.use_only_cookies', '1');
            ini_set('session.use_trans_sid', '0');
            ini_set('session.cookie_httponly', '1');

            session_cache_expire(intval(round($idle_timeout / 60)));
            session_set_cookie_params($idle_timeout, '/');
            ini_set('session.gc_maxlifetime', \strval($idle_timeout));

            session_start();
        }

        // Create superglobal $_SESSION array if it does not exist.
        if (!isset($_SESSION)) $_SESSION = [];

        // Regenerate the session ID if the request scheme changes.
        // This prevents session hijacking when moving between
        // insecure HTTP and secure HTTPS.
        $registry = Registry::getInstance();
        if ($registry->has('Request')) {
            if (!isset($_SESSION['session_request_scheme'])) {
                $_SESSION['session_request_scheme'] = $registry->get('Request')->getUri()->getScheme();
            } elseif ($_SESSION['session_request_scheme'] !== $registry->get('Request')->getUri()->getScheme()) {
                $this->regenerate();
                $_SESSION['session_request_scheme'] = $registry->get('Request')->getUri()->getScheme();
            }
        }

        // Destroy the session if the browser “fingerprint” has changed.
        if (
            $registry->has('Server')
            && $registry->get('Server')->has('HTTP_USER_AGENT') 
        ) {
            if (!isset($_SESSION['session_http_user_agent'])) {
                $_SESSION['session_http_user_agent'] = $registry->get('Server')->get('HTTP_USER_AGENT');
            } elseif ($_SESSION['session_http_user_agent'] !== $registry->get('Server')->get('HTTP_USER_AGENT')) {
                $this->destroy();
                $this->regenerate();
                $_SESSION['session_http_user_agent'] = $registry->get('Server')->get('HTTP_USER_AGENT');
            }
        }

        // Create an object pool of it does not yet exist.
        if (!isset($_SESSION['session_object_pool'])) {
            $_SESSION['session_object_pool'] = [];
        }

        // Update cache items with a TTL (time to live).
        $this->refresh();
    }

    /**
     * Clears the session cache.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success.
     */
    public function clear(): true
    {
        $_SESSION = [];
        $_SESSION['session_object_pool'] = [];
        $_SESSION['session_param_expiration'] = [];
        $this->notify();
        return true;
    }

    /**
     * Deletes an item from the session cache.
     *
     * @param string $key
     *
     * @return bool
     *   True if the item was successfully removed.  False if there was an error.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Throws an invalid argument exception if the `$key` string is not a legal value.
     *
     * @uses StoreCore\Session::deleteMultiple()
     *   Uses the `deleteMultiple()` method in order to limit the notifications
     *   of observers to just one `notify()` call.
     */
    public function delete(string $key): bool
    {
        if (!\is_string($key) || empty($key)) {
            throw new InvalidArgumentException();
        }

        $keys = array($key);
        return $this->deleteMultiple($keys);
    }

    /**
     * Deletes multiple cache items in a single operation.
     *
     * @param iterable $keys
     *   A list of string-based keys to be deleted.
     *
     * @return bool
     *   True if the items were successfully removed.  False if there was an error.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Thrown if `$keys` is neither an array nor a traversable, or if any of
     *   the `$keys` are not a legal value.
     */
    public function deleteMultiple(iterable $keys): bool
    {
        $result = true;
        foreach ($keys as $key) {
            if (!\is_string($key) || empty($key)) {
                throw new InvalidArgumentException();
            }

            if (
                isset($_SESSION['session_param_expiration'])
                && \is_array($_SESSION['session_param_expiration'])
                && array_key_exists($key, $_SESSION['session_param_expiration'])
            ) {
                unset($_SESSION['session_param_expiration'][$key]);
            }

            if (array_key_exists($key, $_SESSION['session_object_pool'])) {
                unset($_SESSION['session_object_pool'][$key]);
            } elseif (array_key_exists($key, $_SESSION)) {
                unset($_SESSION[$key]);
            } else {
                $result = false;
            }
        }
        $this->notify();
        return $result;
    }

    /**
     * Destroys the session.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function destroy(): bool
    {
        $this->clear();

        if (!headers_sent() && ini_get('session.use_cookies')) {
            $cookie_params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $cookie_params['path'], $cookie_params['domain'],
                $cookie_params['secure'], $cookie_params['httponly']
            );
        }

        session_gc();
        $result = session_destroy();
        $this->notify();
        return $result;
    }

    /**
     * Gets the value of a session variable.
     *
     * @param string $key
     *   Unique name or cache key of the cached item.
     *
     * @param mixed $default
     *   Default value to return if the key does not exist.
     *
     * @return mixed
     *   The value of the item from the cache, or `$default` in case of cache
     *   miss.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Throws an invalid argument exception on an invalid cache key.
     */
    public function get(string $key, mixed $default = null): mixed
    {
        if (!\is_string($key) || empty($key)) {
            throw new InvalidArgumentException();
        }

        if (array_key_exists($key, $_SESSION['session_object_pool'])) {
            return unserialize($_SESSION['session_object_pool'][$key]);
        } elseif (array_key_exists($key, $_SESSION)) {
            return $_SESSION[$key];
        } else {
            return $default;
        }
    }

    /**
     * Gets multiple cache items by their unique keys.
     *
     * @param iterable $keys
     *   A list of keys that can be obtained in a single operation.
     *
     * @param mixed $default
     *   Default value to return for keys that do not exist.
     *
     * @return iterable<string, mixed>
     *   A list of key-value pairs.  Cache keys that do not exist or are
     *   stale will have `$default` as value.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Thrown if `$keys` is neither an array nor a traversable, or if any of
     *   the `$keys` are not a legal value.
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        return $result;
    }

    /**
     * Checks if a session variable exists.
     *
     * @param string $key
     *   Unique ID of a cache session variable.
     *
     * @return bool
     *   Returns true if the session variable exists, otherwise false.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     */
    public function has(string $key): bool
    {
        if (!\is_string($key) || empty($key)) {
            throw new InvalidArgumentException();
        }

        // Drop expired cached items for a fresh cache.
        $this->refresh();

        if (array_key_exists($key, $_SESSION['session_object_pool'])) {
            return true;
        } 
        return array_key_exists($key, $_SESSION);
    }

    /**
     * Removes expired cache items.
     *
     * @param void
     *
     * @return void
     *
     * @uses StoreCore\Session::deleteMultiple()
     *   Deletes cache items that have expired.
     */
    private function refresh(): void
    {
        $now = new \DateTimeImmutable('now');

        // Use `$_SESSION['session_param_expiration']` array for TTL timestamps.
        if (
            !array_key_exists('session_param_expiration', $_SESSION)
            || !\is_array($_SESSION['session_param_expiration'])
        ) {
            $_SESSION['session_param_expiration'] = [];
            return;
        }

        // Nothing to do.
        if (empty($_SESSION['session_param_expiration'])) {
            return;
        }

        // Find expired TTL timestamps.
        $keys = [];
        foreach ($_SESSION['session_param_expiration'] as $key => $datetime) {
            $datetime = unserialize($datetime);
            if ($datetime < $now) $keys[] = $key;
        }

        // Delete expired keys.
        if (!empty($keys)) $this->deleteMultiple($keys);
    }

    /**
     * Generates a new session ID.
     *
     * Updates the session with a newly generated session identifier
     * and deletes the old identifier.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @see https://www.php.net/manual/en/function.session-status.php session_status()
     * @see https://www.php.net/manual/en/function.session-regenerate-id.php session_regenerate_id()
     */
    public function regenerate(): bool
    {
        if (session_status() !== \PHP_SESSION_ACTIVE) return false;
        $result = session_regenerate_id(true);
        if ($result) $this->notify();
        return $result;
    }

    /**
     * Sets a session value.
     *
     * @param string $key
     *   Unique cache key of the item to store in the session.
     *
     * @param mixed $value
     *   Value of the session cache item.
     *
     * @param null|int|\DateInterval $ttl
     *   OPTIONAL time to live (TTL) for cache expiration as an integer for
     *   seconds or a `DateInterval`.  If the TTL is not set, the value is
     *   stored for as long as the session persists.
     *
     * @return bool
     *   Returns `true` on success and `false` on failure.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Throws an invalid cache argument exception on an illegal key.
     */
    public function set(string $key, mixed $value, null|int|\DateInterval $ttl = null): bool
    {
        if (
            !\is_string($key)
            || empty($key)
            || $key === 'session_http_user_agent'
            || $key === 'session_object_pool'
            || $key === 'session_param_expiration'
            || $key === 'session_request_scheme'
        ) {
            throw new InvalidArgumentException();
        }

        if ($ttl !== null) {
            if (\is_int($ttl) && $ttl > 0) {
                // ISO 8601 period (P) with a time (T) in seconds (S)
                $ttl = 'PT' . $ttl . 'S';
                $ttl = new \DateInterval($ttl);
            }

            if ($ttl instanceof \DateInterval) {
                $expires = new \DateTime('now');
                $expires->add($ttl);
                $_SESSION['session_param_expiration'][$key] = serialize($expires);
            } else {
                throw new InvalidArgumentException();
            }
        }

        if (\is_object($value)) {
            $_SESSION['session_object_pool'][$key] = serialize($value);
        } else {
            $_SESSION[$key] = $value;
        }

        $this->notify();
        return true;
    }

    /**
     * Persists a set of key-value pairs in the cache, with an optional TTL.
     *
     * @param iterable $values
     *   A list of key-value pairs for a multiple-set operation.
     *
     * @param null|int|\DateInterval $ttl
     *   OPTIONAL.  The time to live (TTL) value of all items.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Thrown if `$values` is neither an array nor a traversable, or if any
     *   of the `$values` are not a legal value.
     */
    public function setMultiple(iterable $values, null|int|\DateInterval $ttl = null): bool
    {
        $result = true;
        foreach ($values as $key => $value) {
            if ($this->set($key, $value, $ttl) !== true) {
                $result = false;
            }
        }
        return $result;
    }
}
