<?php

declare(strict_types=1);

namespace StoreCore;

/**
 * Visitor design pattern: visitable interface.
 *
 * An object that implements the `VisitableInterface` must `accept()`
 * visits from a `$visitor` that implements the `VisitorInterface`.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0
 */
interface VisitableInterface
{
    /**
     * Accepts visits from a visitor.
     *
     * @param StoreCore\VisitorInterface $visitor
     * @return void
     */
    public function accept(VisitorInterface $visitor): void;
}
