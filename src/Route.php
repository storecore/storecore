<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Database\RouteContainer;

/**
 * HMVC route.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
readonly class Route
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $controller
     *   Controller to load and call.
     */
    public string $controller;

    /**
     * @var null|string $method
     *   OPTIONAL controller method to invoke.
     */
    public ?string $method;

    /**
     * @var null|mixed $parameter
     *   OPTIONAL parameter to pass to the controller method.
     */
    public mixed $parameter;

    /**
     * @var string $path
     *   Logical path that is controlled by this controller.
     */
    public string $path;

    /**
     * Creates a route.
     *
     * @param string $path
     *   Unique path name of the route to execute.
     *
     * @param string $controller
     *   Class name of the controller that handles the route.
     * 
     * @param null|string $method
     *   OPTIONAL controller method to call.
     * 
     * @param mixed|null $parameter
     *   OPTIONAL parameters for the controller method.  Parameters are passed
     *   to the method, and not the constructor of the controller, so
     *   parameters are only set when a method is set.  This behavior allows for
     *   calls to controllers only (without methods) as well as multiple calls
     *   to different methods of a single controller.
     */
    public function __construct(string $path, string $controller, ?string $method = null, mixed $parameter = null)
    {
        $this->setPath($path);
        $this->setController($controller);

        if ($method !== null) {
            $this->method = $method;
            $this->parameter = $parameter;
        } else {
            $this->method = null;
            $this->parameter = null;
        }
    }

    /**
     * Instantiates a controller and optionally calls a method.
     *
     * @param void
     *
     * @return void
     *
     * @uses \StoreCore\AbstractController::__construct
     *   If the route controller is a StoreCore controller that extends the
     *   core `AbstractController`, an instance of the `Registry` is passed
     *   to the constructor.
     */
    public function dispatch(): void
    {
        $controller = $this->controller;
        if (is_subclass_of($controller, AbstractController::class)) {
            $thread = new $controller(Registry::getInstance());
        } else {
            $thread = new $controller();
        }

        $method = $this->method;
        if ($method !== null) {
            $parameter = $this->parameter;
            if ($parameter !== null) {
                $thread->$method($parameter);
            } else {
                $thread->$method();
            }
        }
    }

    /**
     * Sets the route controller.
     *
     * @param string $controller
     *   Name of the controller that handles a route.
     *
     * @return void
     */
    private function setController(string $controller): void
    {
        $controller = trim($controller, '\\');
        $this->controller = $controller;
    }

    /**
     * Sets the route path.
     *
     * @param string $path
     *   Path that links a controller to a request.  The path is stored
     *   in lowercase ASCII with a leading forward slash and without a trailing
     *   forward slash.
     *
     * @return void
     *
     * @uses StoreCore\Database\RouteContainer::filterPath()
     */
    private function setPath(string $path): void
    {
        $this->path = RouteContainer::filterPath($path);
    }
}
