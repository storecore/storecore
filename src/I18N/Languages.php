<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

/**
 * Language map.
 *
 * @api
 * @package StoreCore\I18N
 * @version 1.0.0-alpha.1
 *
 * @see https://en.wikipedia.org/wiki/World_language
 *      World language
 *
 * @see https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers
 *      List of languages by number of native speakers
 *
 * @see https://en.wikipedia.org/wiki/Languages_of_Europe
 *      Languages of Europe
 *
 * @see https://european-union.europa.eu/principles-countries-history/languages_en
 *      “Languages, multilingualism, language rules”, European Union
 *
 * @see https://en.wikipedia.org/wiki/Languages_of_the_European_Union
 *      Languages of the European Union
 *
 * @see https://en.wikipedia.org/wiki/Nordic-Baltic_Eight
 *      Nordic-Baltic Eight (NB8)
 *
 * @see https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
 *      List of ISO 639-2 codes
 *
 * @see https://msdn.microsoft.com/en-US/library/ee825488(v=cs.20).aspx
 *      Table of Language Culture Names, Codes, and ISO Values Method
 */
enum Languages: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array AUSTRIA
     *   Languages of Austria (`AT`), consisting of the national language
     *   Austrian German (`de-AT`) and the regional languages Burgenland
     *   Croatian (`ckm-AT`), Hungarian (`hu-HU`), and Slovene (`sl-SI`).
     */
    public const array AUSTRIA = [
        'ckm-AT', 'de-AT', 'hu-HU', 'sl-SI'
    ];

    /**
     * @var string DEFAULT_LANGUAGE
     *   Default language to use if no other default language is set or no
     *   other language match was found.  Defaults to `en-GB` for British
     *   English.
     */
    public const string DEFAULT_LANGUAGE = 'en-GB';

    /**
     * @var array DEVELOPERS
     *   Languages commonly used by software developers.
     */
    public const array DEVELOPERS = [
        'ar-001', 'bn-BD', 'de-DE', 'en-US', 'es-419', 'es-ES', 'fr-FR',
        'he-IL', 'hi-IN', 'id-ID', 'it-IT', 'ja-JP', 'ko-KR', 'pl-PL', 'pt-BR',
        'ru-RU', 'th-TH', 'tr-TR', 'vi-VN', 'zh-Hans', 'zh-Hant'
    ];

    /**
     * @var array EFIGS
     *   Initialism of English, French, Italian, German, Spanish: in software
     *   development, used to designate five widely used languages that software
     *   is often translated to.
     */
    public const array EFIGS = [
        'de-DE', 'en-GB', 'en-US', 'es-419', 'es-ES', 'fr-CA', 'fr-FR', 'it-IT'
    ];

    /**
     * @var array EUROPEAN_UNION
     *   Official languages of the European Union (EU).
     */
    public const array EUROPEAN_UNION = [
        'bg-BG', 'cs-CZ', 'da-DK', 'de-DE', 'el-GR', 'en-GB', 'es-ES', 'et-EE',
        'fi-FI', 'fr-FR', 'ga-IE', 'hr-HR', 'hu-HU', 'it-IT', 'lt-LT', 'lv-LV',
        'mt-MT', 'nl-NL', 'pl-PL', 'pt-PT', 'ro-RO', 'sk-SK', 'sl-SI', 'sv-SE'
    ];

    /**
     * @var array EU
     *   Abbreviation of “European Union” and alias of `EUROPEAN_UNION`.
     */
    public const array EU = self::EUROPEAN_UNION;

    /**
     * @var array NORDIC_BALTIC_EIGHT
     *   Languages of the Nordic-Baltic Eight (NB8) countries Denmark, Estonia,
     *   Finland, Iceland, Latvia, Lithuania, Norway, and Sweden.
     */
    public const array NORDIC_BALTIC_EIGHT = [
        'da-DK', 'et-EE', 'fi-FI', 'is-IS', 'lt-LT', 'lv-LV', 'nb-NO', 'nn-NO', 'sv-SE'
    ];

    /**
     * @var array NB8
     *   Abbreviation of “Nordic-Baltic Eight” and alias of `NORDIC_BALTIC_EIGHT`.
     */
    public const array NB8 = self::NORDIC_BALTIC_EIGHT;

    /**
     * @var array SWITZERLAND
     *   Languages of Switzerland (`CH`), consisting of the Swiss variants of
     *   German (`de-CH`), French (`fr-CH`), and Italian (`it-CH`).
     */
    public const array SWITZERLAND = [
        'de-CH', 'fr-CH', 'it-CH'
    ];

    /**
     * @var array CH
     *   Country code of Switzerland and an alias of `SWITZERLAND`.
     */
    public const array CH = self::SWITZERLAND;

    /**
     * @var array WESTERN_EUROPE
     *   Languages of the Western European countries Austria (`AT`),
     *   Belgium (`BE`), France (`FR`), Germany (`DE`), Liechtenstein (`LI`),
     *   Luxembourg (`LU`), Monaco (`MC`), the Netherlands (`NL`), and
     *   Switzerland (`CH`).
     */
    public const array WESTERN_EUROPE = [
        'de-AT', 'de-BE', 'de-CH', 'de-DE', 'de-LI', 'fr-BE', 'fr-CH', 'fr-FR',
        'fr-MC', 'it-CH', 'lb-LU', 'nl-BE', 'nl-NL'
    ];

    /*
     * Backed enumeration cases map ISO 639-3 language codes to locales.
     * 
     * @see https://en.wikipedia.org/wiki/ISO_639-3
     * @see https://en.wikipedia.org/wiki/List_of_ISO_639-3_codes
     */
    case afr = 'af-ZA';
    case ast = 'ast-ES';
    case bel = 'be-BY';
    case bre = 'br-FR';
    case bul = 'bg-BG';
    case dan = 'da-DK';
    case deu = 'de-DE';
    case eng = 'en-GB';
    case est = 'et-EE';
    case eus = 'eu-ES';
    case fra = 'fr-FR';
    case guj = 'gu-IN';
    case heb = 'he-IL';
    case hrv = 'hr-HR';
    case hun = 'hu-HU';
    case ita = 'it-IT';
    case kat = 'ka-GE';
    case ltz = 'lb-LU';
    case nld = 'nl-NL';
    case nno = 'nn-NO';
    case nob = 'nb-NO';
    case por = 'pt-PT';
    case slv = 'sl-SI';
    case spa = 'es-ES';
    case swe = 'sv-SE';
    case ukr = 'uk-UA';
}
