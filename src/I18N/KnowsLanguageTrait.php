<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use \TypeError;
use StoreCore\Types\LanguageCode;

use function \empty;
use function \is_array;
use function \is_string;
use function \trim;

/**
 * Languages acceptable for people and organizations.
 *
 * @api
 * @package StoreCore\I18N
 * @see     https://schema.org/knowsLanguage Schema.org `knowsLanguage` property
 * @see     https://www.rfc-editor.org/info/bcp47 Best Current Practice BCP 47
 * @version 1.0.0-alpha.1
 */
trait KnowsLanguageTrait
{
    /**
     * @var array $knowsLanguage
     *   One or more languages that are knowns by an entity.
     */
    protected array $knowsLanguage = [];

    /**
     * Sets or adds known languages.
     *
     * @return array
     *   Returns an array with language codes from the IETF BCP 47 standard.
     *   The array MAY be empty if no known languages were set.
     */
    public function knowsLanguage(mixed ...$languages): array
    {
        if (!empty($languages)) {
            foreach ($languages as $language) {
                if (is_array($language)) {
                    foreach ($language as $value) {
                        $this->knowsLanguage($value);
                    }
                    break;
                }

                if (is_string($language)) {
                    $language = trim($language);
                    $language = new LanguageCode($language);
                }

                if ($language instanceof LanguageCode) {
                    $this->knowsLanguage[] = $language->__toString();
                } elseif ($language instanceof Language) {
                    $this->knowsLanguage[] = $language;
                } else {
                    throw new TypeError();
                }
            }
        }

        return $this->knowsLanguage;
    }
}
