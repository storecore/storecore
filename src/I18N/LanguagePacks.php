<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

/**
 * Language pack loader.
 *
 * This system helper class contains a static `load()` method to load a
 * StoreCore language pack.  If the static `load()` method is called with an
 * ISO language/locale, the language pack of the requested language is
 * loaded.  If the `load()` method is called without a specific language
 * string, the method is able to load a language pack through HTTP content
 * negation with the client.
 *
 * @api
 * @package StoreCore\I18N
 * @version 1.0.0-alpha.1
 */
class LanguagePacks
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array SUPPORTED_LANGUAGES
     *   Languages supported by StoreCore by default.  This default MAY be
     *   overwritten with the OPTIONAL `STORECORE_LANGUAGES` configuration
     *   parameter.
     */
    public const SUPPORTED_LANGUAGES = [
        'de-DE' => true,
        'en-GB' => true,
        'en-US' => true,
        'fr-FR' => true,
        'nl-BE' => true,
        'nl-NL' => true,
    ];


    /**
     * Loads a negotiable language pack from the file system cache.
     *
     * @param StoreCore\Types\LanguageCode|null $language
     *   OPTIONAL ISO code of the locale to load.  If this parameter is not set
     *   or the requested language is not (yet) supported, the system root
     *   language `en-GB` (British English) defined in the global constant
     *    `Languages::DEFAULT_LANGUAGE` is loaded.
     *
     * @return StoreCore\Types\LanguageCode
     *   ISO code of the loaded locale.
     *
     * @uses StoreCore\I18N\AcceptLanguage::negotiate()
     */
    public static function load(LanguageCode $language = null): LanguageCode
    {
        $supported_languages = self::getSupportedLanguages();

        if ($language !== null && array_key_exists((string) $language, $supported_languages)) {
            include STORECORE_FILESYSTEM_CACHE_DATA_DIR . $language . '.php';
            return $language;
        }

        if ($language === null) {
            $language = new LanguageCode(Languages::DEFAULT_LANGUAGE);
        }

        $negotiator = new \StoreCore\I18N\AcceptLanguage();
        $locale = $negotiator->negotiate($supported_languages, $language);
        include STORECORE_FILESYSTEM_CACHE_DATA_DIR . $locale . '.php';
        return $locale;
    }

    /**
     * Gets all supported languages.
     *
     * @param void
     *
     * @return array
     *   Returns an array that links ISO codes for supported languages to the
     *   status `true` (enabled) or `false` (disabled).  The array always
     *   contains at least one language.
     */
    public static function getSupportedLanguages(): array
    {
        // Decode the global `config.php` configuration constant `STORECORE_LANGUAGES`.
        if (\defined('STORECORE_LANGUAGES')) {
            $languages = json_decode(STORECORE_LANGUAGES, true, 2);
            if ($languages !== null) {
                $languages = array_filter($languages);
                if (!empty($languages)) {
                    return $languages;
                }
            }
        }

        // Fall back to StoreCore languages supported by default.
        return self::SUPPORTED_LANGUAGES;
    }
}
