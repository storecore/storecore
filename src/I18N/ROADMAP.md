# StoreCore internationalization roadmap

This documentation is a work in progress. It describes prerelease software,
and is subject to change.


## Stage I: Western Europe and North America

| Language      | Primary  | Secondary |
| ------------- | -------- | --------- |
| Dutch         | `nl-NL`  | `nl-BE`   |
| English       | `en-GB`  | `en-US`   |
| French        | `fr-FR`  | `fr-CA`   |
| German        | `de-DE`  | ·         |
| Luxembourgish | `lb-LU`  | ·         |


## Stage II: Southern Europe and Latin America

| Language      | Primary  | Secondary |
| ------------- | -------- | --------- |
| Catalan       | `ca-039` | ·         |
| Dutch         | `nl-NL`  | `nl-BE`   |
| English       | `en-GB`  | `en-US`   |
| French        | `fr-FR`  | `fr-CA`   |
| German        | `de-DE`  | ·         |
| Portuguese    | `pt-PT`  | `pt-BR`   |
| Italian       | `it-IT`  | ·         |
| Luxembourgish | `lb-LU`  | ·         |
| Spanish       | `es-ES`  | `es-419`  |

Greek for Greece (`el-GR`) is postponed until stage V because the language uses
the Greek (`Grek`) script.  Non-Latin alphabets like Cyrillic (`Cyrl`) will be
introduced in stage IV.


## Stage III: Nordics and Baltics (NB8)

| Language      | Primary  | Secondary |
| ------------- | -------- | --------- |
| Danish        | `da-DK`  | ·         |
| Estonian      | `et-EE`  | ·         |
| Finnish       | `fi-FI`  | ·         |
| Icelandic     | `is-IS`  | ·         |
| Latvian       | `lv-LV`  | ·         |
| Lithuanian    | `lt-LT`  | ·         |
| Norwegian     | `nb-NO`  | `nn-NO`   |
| Swedish       | `sv-SE`  | ·         |


## Stage IV: Central and Eastern Europe

| Language      | Primary  | Secondary |
| ------------- | -------- | --------- |
| Belarusian    | `be-BY`  | ·         |
| Bulgarian     | `bg-BG`  | ·         |
| Croatian      | `hr-HR`  | ·         |
| Czech         | `cs-CZ`  | ·         |
| Hungarian     | `hu-HU`  | ·         |
| Polish        | `pl-PL`  | ·         |
| Romanian      | `ro-RO`  | ·         |
| Russian       | `ru-RU`  | ·         |
| Slovak        | `sk-SK`  | ·         |
| Slovene       | `sl-SI`  | ·         |
| Turkish       | `tr-TR`  | ·         |
| Ukrainian     | `uk-UA`  | ·         |


## Stage V: European Union (EU) and European minorities

| Language      | Primary  | Secondary |
| ------------- | -------- | --------- |
| Greek         | `el-GR`  | ·         |
| Irish         | `ga-IE`  | ·         |
| Maltese       | `mt-MT`  | ·         |

Upon completion of this stage, all official languages of the European Union (EU)
will be supported.  These official languages are included in the `EUROPEAN_UNION`
class constant of the `Languages` class in the `StoreCore\I18N` namespace.


## Stage VI: Global developer community

| Language              | Primary   | Secondary |
| --------------------- | --------- | --------- |
| Arabic                | `ar-001`  | ·         |
| Bengali               | `bn-BD`   | ·         |
| Chinese (simplified)  | `zh-Hans` | ·         |
| Chinese (traditional) | `zh-Hant` | ·         |
| Hebrew                | `he-IL`   | ·         |
| Hindi                 | `hi-IN`   | ·         |
| Indonesian            | `id-ID`   | ·         |
| Japanese              | `ja-JP`   | ·         |
| Korean                | `ko-KR`   | ·         |
| Thai                  | `th-TH`   | ·         |
| Vietnamese            | `vi-VN`   | ·         |


## Stage VII: Global support for all world languages

To be determined.


Copyright © 2024 StoreCore™.  All rights reserved.

Except as otherwise noted, the content of this document is licensed under the 
[https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/),
and code samples are licensed under the
[GNU General Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl.html).
