<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

/**
 * Language.
 *
 * @api
 * @package StoreCore\I18N
 * @see     https://schema.org/Language Schema.org type `Language`
 * @see     https://en.wikipedia.org/wiki/IETF_language_tag IETF BCP 47 language tag
 * @version 1.0.0-alpha.1
 */
readonly class Language implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $identifier
     *   Language code identifier.
     */
    public string $identifier;

    /**
     * @var null|string $name
     *   Name of the language.
     */
    public ?string $name;

    /**
     * @var null|string $alternateName
     *   Alternative name of the language.
     */
    public ?string $alternateName;

    /**
     * Creates a language value object.
     *
     * @param StoreCore\Types\LanguageCode|string $identifier
     * @param null|string $name
     * @param null|string $alternate_name
     */
    public function __construct(LanguageCode|string $identifier, ?string $name = null, ?string $alternate_name = null)
    {
        $this->identifier = (string) $identifier;
        $this->name = $name;
        $this->alternateName = $alternate_name;
    }

    /**
     * Converts the Language value object to a language code.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->identifier;
    }

    /**
     * Creates a key-value array for JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'Language',
            'identifier' => $this->identifier,
        ];

        if ($this->name !== null) {
            $result['name'] = $this->name;
            if ($this->alternateName !== null) {
                $result['alternateName'] = $this->alternateName;
            }
        }

        return $result;
    }
}
