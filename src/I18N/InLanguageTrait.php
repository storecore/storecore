<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

use function \empty;
use function \is_string;
use function \isset;

/**
 * Language trait implementation of the StoreCore\LanguageInterface.
 *
 * @api
 * @package StoreCore\I18N
 * @see     https://schema.org/inLanguage Schema.org `inLanguage` property
 * @version 1.0.0
 */
trait InLanguageTrait
{
    /**
     * @var StoreCore\Types\LanguageCode|StoreCore\I18N\Language $inLanguage
     *   The language an object is in as a stringable `LanguageCode`
     *   or a `Language` value object.
     */
    protected LanguageCode|Language $inLanguage;

    public function inLanguage(null|Language|LanguageCode|string $language = null): string
    {
        if (!empty($language)) {
            if (is_string($language)) {
                $this->inLanguage = new LanguageCode($language);
            } else {
                $this->inLanguage = $language;
            }
        }

        return isset($this->inLanguage) ? $this->inLanguage->__toString() : '';
    }
}
