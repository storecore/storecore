<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2014-2017, 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\I18N;

use StoreCore\Types\LanguageCode;

use function \array_map;
use function \array_shift;
use function \arsort;
use function \count;
use function \explode;
use function \isset;
use function \str_replace;
use function \strlen;
use function \strtolower;
use function \substr;

/**
 * Content language negotiation.
 *
 * @internal
 * @package StoreCore\I18N
 * @version 0.2.2
 *
 * @see https://www.w3.org/International/questions/qa-accept-lang-locales
 *      Accept-Language used for locale setting, by Lloyd Honomichl, Lionbridge
 */
class AcceptLanguage
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '0.2.2';

    /**
     * @var StoreCore\Types\LanguageCode $defaultLanguage.
     *   Default language to use if no other language is found or set.
     *   Defaults to `en-GB` for British English on initialization.
     */
    public LanguageCode $defaultLanguage;

    /**
     * Creates a language negotiator.
     * 
     * @param StoreCore\Types\LanguageCode|null $default_language
     *   Optional default language to use if no other language match is found.
     */
    public function __construct(?LanguageCode $default_language = null)
    {
        if ($default_language === null) {
            $default_language = new LanguageCode('en-GB');
        }
        $this->defaultLanguage = $default_language;
    }

    /**
     * Matches HTTP accept header.
     *
     * Parses a weighed `Accept` HTTP header and matches it against a list
     * of supported options.
     *
     * @param string $header
     *   The HTTP `Accept` header to parse.
     *
     * @param array $supported
     *   A list of supported values.
     *
     * @return string|null
     *   Returns a matched option or `null` if there is no match.
     */
    private function matchAcceptHeader(string $header, array $supported): ?string
    {
        $matches = $this->sortAcceptHeader($header);

        // Lengthen 'de' to 'de-de', 'fr' to 'fr-fr', 'nl' to 'nl-nl', etc.
        foreach ($matches as $key => $q) {
            if (strlen($key) === 2) $key = $key . '-' . $key;
            if (isset($supported[$key])) return $supported[$key];
        }

        // If no match is found, try to find similar languages
        // by mapping 'de-CH' to 'de-de', 'fr-CA' to 'fr-fr', etc.
        foreach ($matches as $key => $q) {
            if (strlen($key) > 2) {
                $key = substr($key, 0, 2);
                $key = $key . '-' . $key;
                if (isset($supported[$key])) {
                    return $supported[$key];
                }
            }
        }

        // If any (i.e. "*") is acceptable, return the first supported format
        return (isset($matches['*'])) ? array_shift($supported) : null;
    }

    /**
     * Negotiates preferred client language.
     *
     * @param array $supported
     *   An associative array indexed by language codes (locale codes)
     *   supported by the application.  Values must evaluate to true.
     *
     * @param LanguageCode|null $default
     *   The default language that should be used if none of the other
     *   languages are found during negotiation.  Defaults to 'en-GB' for
     *   British English.
     *
     * @return StoreCore\Types\LanguageCode
     *   Returns a supported language or the default language as a language
     *   code value object.
     */
    public function negotiate(array $supported, LanguageCode $default = null): LanguageCode
    {
        if ($default !== null) {
            $this->defaultLanguage = $default;
        }

        $languages = [];
        foreach ($supported as $language_code => $language_is_supported) {
            if ($language_is_supported) {
                $languages[strtolower($language_code)] = $language_code;
            }
        }
        if (empty($languages)) {
            return $this->defaultLanguage;
        }

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $match = $this->matchAcceptHeader($_SERVER['HTTP_ACCEPT_LANGUAGE'], $languages);
            if ($match !== null && LanguageCode::validate($match)) {
                return new LanguageCode($match);
            }
        }

        return $this->defaultLanguage;
    }

    /**
     * Parses and sorts a weighed `Accept` HTTP header.
     *
     * @param string $header
     *   The HTTP `Accept` header to parse.
     *
     * @return array
     *   Sorted list of `Accept` options.
     */
    private function sortAcceptHeader(string $header): array
    {
        $matches = array();
        foreach (explode(',', $header) as $option) {
            $option = array_map('trim', explode(';', $option));
            $l = strtolower($option[0]);
            if (isset($option[1])) {
                $q = (float) str_replace('q=', '', $option[1]);
            } else {
                $q = null;
                // Assign default low weight for generic values
                if ($l === '*/*') {
                    $q = 0.01;
                } elseif (substr($l, -1) === '*') {
                    $q = 0.02;
                }
            }
            // Unweighted values get high weight by their position in the list
            $matches[$l] = isset($q) ? $q : 1000 - count($matches);
        }
        arsort($matches, SORT_NUMERIC);
        return $matches;
    }
}
