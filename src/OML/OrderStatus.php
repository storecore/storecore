<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Order status enumeration.
 *
 * Enumeration for the status of an `Order` or an `OrderItem`.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/OrderStatus Schema.org enumeration type `OrderStatus`
 * @see     https://schema.org/orderStatus Schema.org `orderStatus` property of an `Order`
 * @see     https://schema.org/orderItemStatus Schema.org `orderItemStatus` property of an `OrderItem`
 * @version 26.0.0
 */
enum OrderStatus: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  Matches Schema.org version 26.0
     *   from February 12, 2024.
     */
    public const string VERSION = '26.0.0';

    case OrderCancelled       = 'https://schema.org/OrderCancelled';
    case OrderDelivered       = 'https://schema.org/OrderDelivered';
    case OrderInTransit       = 'https://schema.org/OrderInTransit';
    case OrderPaymentDue      = 'https://schema.org/OrderPaymentDue';
    case OrderPickupAvailable = 'https://schema.org/OrderPickupAvailable';
    case OrderProblem         = 'https://schema.org/OrderProblem';
    case OrderProcessing      = 'https://schema.org/OrderProcessing';
    case OrderReturned        = 'https://schema.org/OrderReturned';

    /**
     * Maps an integer to an enumeration member.
     *
     * @internal
     * @param int $value
     * @return static
     */
    final public static function fromInteger(int $key): static
    {
        return match($key) {
            1 => static::OrderCancelled,
            2 => static::OrderDelivered,
            3 => static::OrderInTransit,
            4 => static::OrderPaymentDue,
            5 => static::OrderPickupAvailable,
            6 => static::OrderProblem,
            7 => static::OrderProcessing,
            8 => static::OrderReturned,
            default => throw new \ValueError(),
        };
    }

    /**
     * Maps an enumeration member to a unique integer.
     *
     * @internal
     * @param StoreCore\Types\OrderStatus $status
     * @return int
     */
    final public static function toInteger(OrderStatus $status): int
    {
        return match($status->name) {
            'OrderCancelled'       => 1,
            'OrderDelivered'       => 2,
            'OrderInTransit'       => 3,
            'OrderPaymentDue'      => 4,
            'OrderPickupAvailable' => 5,
            'OrderProblem'         => 6,
            'OrderProcessing'      => 7,
            'OrderReturned'        => 8,
        };
    }
}
