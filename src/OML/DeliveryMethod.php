<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use Psr\Http\Message\UriInterface;
use StoreCore\Types\URL;

/**
 * Delivery method.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/DeliveryMethod Schema.org type `DeliveryMethod`
 * @version 1.0.0
 */
class DeliveryMethod implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|string $name;
     *   Name of the delivery method.  This name MAY be replaced with a URL
     *   that uniquely identifies the delivery method.
     */
    public ?string $name = null;

    /**
     * @var null|StoreCore\Types\URL $url;
     *   URL that uniquely identifies the delivery method.
     */
    public ?URL $url = null;

    /**
     * Creates a delivery method.
     *
     * @param string|StoreCore\Types\URL|Psr\Http\Message\UriInterface|null $name_or_url
     *   OPTIONAL unique identifier of the delivery method as a name string or an URL.
     */
    public function __construct(string|URL|UriInterface|null $name_or_url = null)
    {
        if ($name_or_url instanceof UriInterface) {
            $name_or_url = (string) $name_or_url;
            $name_or_url = new URL($name_or_url);
        }

        if (is_string($name_or_url)) {
            $validated_url = filter_var($name_or_url, \FILTER_VALIDATE_URL);
            if ($validated_url !== false) {
                $name_or_url = new URL($validated_url);
            }
        }

        if ($name_or_url instanceof URL) {
            $this->url = $name_or_url;
        } elseif (is_string($name_or_url)) {
            $this->name = $name_or_url;
        }
    }

    /**
     * Converts the object to a JSON-LD representation.
     *
     * @param void
     * @return string|null
     */
    public function jsonSerialize(): ?string
    {
        if ($this->url !== null) {
            return (string) $this->url;
        } elseif ($this->name !== null) {
            return $this->name;
        } else {
            return null;
        }
    }
}
