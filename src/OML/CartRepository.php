<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use Psr\Container\ContainerInterface;

use StoreCore\Cart;
use StoreCore\Database\{ContainerException, NotFoundException};

/**
 * Cart repository.
 *
 * The cart repository maps a cart to an order, if the order exists.  This
 * mapping mechanism allows for persistent carts.  A unique key, that is shared
 * between clients and servers, unlocks a previously saved order.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class CartRepository extends OrderRepository implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Gets an existing cart.
     *
     * @param string $id
     *   Encoded cart identifier as a string.  This is not the UUID of the
     *   order, but the encoded cart `$id` is used to map the cart to an order
     *   model.
     *
     * @return StoreCore\Cart
     *   Returns a `Cart` object if the cart exists.
     *
     * @throws Psr\Container\NotFoundExceptionInterface
     *   Throws a PSR-11 NotFoundExceptionInterface if the cart does not exist.
     *
     * @throws Psr\Container\ContainerExceptionInterface
     *   Throws a PSR-11 ContainerExceptionInterface on other errors than a
     *   NotFoundExceptionInterface.
     */
    public function get(string $id): Cart
    {
        try {
            $id = CartID::createFromString($id);
            if ($id === false) {
                throw new NotFoundException();
            }

            $statement = $this->Database->prepare(
                'SELECT `order_id` FROM `sc_orders` WHERE `cart_uuid` = ? AND `cart_rand` = ? LIMIT 1'
            );
            $statement->bindValue(1, $id->getUUID(), \PDO::PARAM_STR);
            $statement->bindValue(2, $id->getToken(), \PDO::PARAM_STR);
            if ($statement->execute() === false) throw new NotFoundException;
            $order_id = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            return parent::get($order_id);
        } catch (NotFoundException $e) {
            throw $e;
        } catch (ContainerException $e) {
            throw $e;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if the cart exists.
     *
     * @param string $id
     *   Encoded cart identifier.
     *
     * @return bool
     *   Returns `true` if the cart exists, otherwise `false`.
     */
    public function has(string $id): bool
    {
        try {
            $id = CartID::createFromString($id);
            if ($id !== false) {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_orders` WHERE `cart_uuid` = ? AND `cart_rand` = ?'
                );
                if ($statement !== false) {
                    $statement->bindValue(1, $id->getUUID(), \PDO::PARAM_STR);
                    $statement->bindValue(2, $id->getToken(), \PDO::PARAM_STR);
                    if ($statement->execute() !== false) {
                        $count = (int) $statement->fetchColumn(0);
                        $statement->closeCursor();
                        unset($statement);
                        if ($count === 1) {
                            return true;
                        }
                    }
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }
}
