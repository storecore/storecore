<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\IdentityInterface;
use StoreCore\{ProxyInterface, Proxy};
use StoreCore\Types\UUID;

/**
 * Order proxy.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/Order
  * @version 1.0.0-alpha.1
 */
readonly class OrderProxy extends Proxy implements
    IdentityInterface,
    \JsonSerializable,
    ProxyInterface,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $confirmationNumber
     *   A number that confirms the given order has been received.
     *   Implements {@link https://schema.org/confirmationNumber Order.confirmationNumber}.
     */
    public ?string $confirmationNumber;

    /**
     * @var null|string $orderNumber
     *   The identifier of the transaction.
     *   Implements {@link https://schema.org/orderNumber Order.orderNumber}.
     */
    public ?string $orderNumber;

    #[\Override]
    public function __construct(
        UUID|\Stringable|string $identifier,
        string $type = 'Order',
        ?string $order_number = null,
        ?string $confirmation_number = null,
    ) {
        parent::__construct($identifier, $type);
        $this->confirmationNumber = $confirmation_number;
        $this->orderNumber = $order_number;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/orders/' . $this->getIdentifier();
        if ($this->orderNumber !== null) $result['orderNumber'] = $this->orderNumber;
        if ($this->confirmationNumber !== null) $result['confirmationNumber'] = $this->confirmationNumber;
        return $result;
    }
}
