<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \Stringable;
use StoreCore\Geo\Country;
use StoreCore\Geo\PostalAddress;
use StoreCore\Types\CountryCode;

use function \empty;
use function \preg_replace;
use function \strtoupper;

/**
 * Address index.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0
 */
class AddressIndex implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\Types\CountryCode $countryCode
     *   OPTIONAL value object for an ISO 3166-1 alpha-2 country code.
     */
    public ?CountryCode $countryCode = null;

    /**
     * @var null|string $postalCode
     *   Postal code.
     */
    public ?string $postalCode = null;

    /**
     * @var null|int|string $houseNumber
     *   House number as a string.
     */
    public null|int|string $houseNumber = null;

    /**
     * @var null|string $houseNumberAddition
     *   Addition to the house number.
     */
    public ?string $houseNumberAddition = null;

    /**
     * Creates a string representation of an address.
     * 
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $result = (string) $this->countryCode . $this->postalCode . $this->houseNumber;
        if (!empty($this->houseNumberAddition)) {
            $result = $result . 'X' . $this->houseNumberAddition;
        }

        $result = preg_replace('/\s+/', '', $result);
        $result = strtoupper($result);
        return $result;
    }

    /**
     * Creates an address index for an address.
     * 
     * @param StoreCore\Geo\PostalAddress $address
     * @return StoreCore\OML\AddressIndex
     */
    public static function createFromPostalAddress(PostalAddress $address): AddressIndex
    {
        $result = new static;

        $country = $address->getCountry();
        if ($country !== null) {
            $result->setCountry($country);
        }

        $result->postalCode = $address->postalCode;
        $result->houseNumber = $address->houseNumber;
        $result->houseNumberAddition = $address->houseNumberAddition;

        return $result;
    }

    /**
     * Sets the address country.
     *
     * @param StoreCore\Geo\Country|StoreCore\Types\CountryCode $country
     *   Country or country code value object.
     *
     * @return void
     */
    public function setCountry(Country|CountryCode $country): void
    {
        if ($country instanceof Country) {
            $country = new CountryCode($country->identifier);
        }
        $this->countryCode = $country;
    }
}
