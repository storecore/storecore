StoreCore Shipping
==================

Interface synopsis
------------------

```php
interface Psr\Container\ContainerInterface {
    public get(string $id): mixed
    public has(string $id): bool
}

interface StoreCore\OML\ProviderInterface {
    public getName(): string
    public getURL(): ?string;
}

interface StoreCore\OML\TrackingInterface {
    public getTrackingNumber(): string
    public getTrackingUrl(): string
}
```


Class synopsis
--------------

Classes listed without a namespace prefix are located in the default
`StoreCore` namespace.  Classes are listed in alphabetical order.

```php
Address implements Psr\Container\ContainerInterface {
    public __construct(?array $params = null)
    public __get(string $name): mixed
    public __set(string $name, mixed $value): void
    public get(string $id): mixed
    public has(string $id): bool
    public getCountry(): Country|null
    public getIdentifier(): int|null
    public setCountry(int|string|Country $country): void
    public setCountrySubdivision(int|string|State $country_subdivision): void
    public setIdentifier(int|string $id): void
    public toArray(): array
}

AddressIndex implements \Stringable {
    string|null $countryCode;
    int|null $countryNumber;
    string|null $postalCode;
    string|null $houseNumber;
    string|null $houseNumberAddition;

    public __toString(): string
    public static createFromPostalAddress(StoreCore\Geo\PostalAddress $address): AddressIndex
    public setCountry(Country $country): void
}

AddressRepository implements Psr\Container\ContainerInterface {
    public get(string|int $id): Address
    public has(string|int $id): bool
}

StoreCore\Geo\Country
implements \JsonSerializable, \Stringable {
    string|null $alternateName;
    string $identifier;
    string $name;

    public __construct(string $identifier, string $name)
    public __toString(): string
    public jsonSerialize(): array
}

StoreCore\Geo\CountryRepository implements Psr\Container\ContainerInterface {
    public get(string|int $id): Country
    public find(string $country_name): Country
    public has(string|int $id): bool
}

GeoCoordinates implements \JsonSerializable {
    float $latitude;
    float $longitude;
    string|null $postalCode;

    public __construct(float $latitude, float $longitude)
    public jsonSerialize() : array
}

PostalAddress extends Address implements \JsonSerializable {
    public getAddressRegion() : string|null
    public getStreetAddress() : string|null
    public setAddressRegion(State|string $country_subdivision): void
    public setStreetAddress(StreetAddress|string $street_address): void
}

State implements \JsonSerializable {
    string $identifier;
    string|null $name;
    string|null $alternateName;

    public __construct(string $identifier, ?string $name = null)
    public jsonSerialize() : array
}

StateRepository implements Psr\Container\ContainerInterface {
    public find(string $string): State
    public get(string $id): State
    public has(string $id): bool
    public list(?Country $country = null): array
}

StreetAddress {
    Country|null $country;
    string|null $houseNumber;
    string|null $houseNumberAddition;
    string|null $streetName;

    public __construct(?string $street_name = null, int|string|null $house_number = null, ?string $house_number_addition = null)
    public __get(string $name): mixed
    public __set(string $name, mixed $value): void
    public __toString(): string
    public static createFromPostalAddress(PostalAddress $address): StreetAddress
    public static createFromString(string $street_address): StreetAddress|false
    public getCountry(): Country|null
    public getHouseNumber(): string|null
    public getHouseNumberAddition() : string|null
    public getStreetName(): string|null
    public setCountry(string|int|Country $country): void
    public setHouseNumber(int|string $house_number): void
    public setHouseNumberAddition(string $house_number_addition): void
    public setStreetName(string $street_name): void
}
```

Copyright © 2021–2023 StoreCore™.  All rights reserved.
