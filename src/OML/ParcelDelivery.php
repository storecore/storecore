<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Concrete parcel delivery.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/ParcelDelivery Schema.org type `ParcelDelivery`
 * @version 0.1.0
 */
class ParcelDelivery extends AbstractParcelDelivery implements
    \JsonSerializable,
    \Stringable,
    TrackingInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Converts the object to a JSON-LD string.
     *
     * @param void
     * @return string
     * @uses AbstractParcelDelivery::jsonSerialize()
     */
    public function __toString(): string
    {
        $result = json_encode($this, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        $result = str_replace('    ', "\t", $result);
        $result = str_replace("\t", '  ', $result);
        return $result;
    }
}
