<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Provider interface for parcel delivery.
 *
 * This interface enforces two basic business rules: a provider for parcel
 * delivery MUST have a name and MAY have a URL.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 * @see https://schema.org/provider Schema.org property `provider`
 *
 * @see https://schema.org/carrier
 *   “Carrier” is an out-dated term indicating the “provider” for parcel
 *   delivery or shipping.  The Schema.org term “carrier” has been
 *   superseded by “provider”.
 */
interface ProviderInterface extends \JsonSerializable
{
    /**
     * Gets the provider’s organization name.
     *
     * @param void
     *
     * @return string
     *   Returns the name of the organization as a string.
     *   This string SHOULD NOT be empty.
     */
    public function getName(): string;

    /**
     * Gets a URL for the provider’s organization.
     *
     * @param void
     *
     * @return string|null
     *   Returns a URL as a string or null.
     */
    public function getUrl(): ?string;
}
