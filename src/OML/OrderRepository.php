<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use Psr\Clock\ClockInterface;
use Psr\Container\ContainerInterface;
use StoreCore\RepositoryInterface;

use StoreCore\Database\AbstractModel;
use StoreCore\Database\{ContainerException, NotFoundException};
use StoreCore\Database\OrderMapper;

use StoreCore\ClockTrait;

use StoreCore\{BrokerOrder, Cart, Order, WishList};
use StoreCore\Registry;
use StoreCore\OML\BackOrder;
use StoreCore\Types\CartID;
use StoreCore\Types\UUID;

/**
 * Order repository.
 *
 * The order repository handles the storage of existing orders and similar
 * domain objects, such as backorders, shopping lists, and wish lists.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderRepository extends AbstractModel implements
    ClockInterface,
    ContainerInterface,
    \Countable,
    RepositoryInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\OrderMapper $orderMapper
     *   Order mapper for reading and writing order database records.
     */
    private readonly OrderMapper $orderMapper;

    /**
     * Creates an order repository.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore registry.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->orderMapper = new OrderMapper($registry);
    }

    /**
     * Counts the number of confirmed orders.
     *
     * @param void
     *
     * @return int
     *   Returns the number of orders that were confirmed and that have
     *   not been cancelled or deleted.
     */
    public function count(): int
    {
        $query = <<<'SQL'
            SELECT COUNT(*)
              FROM `sc_orders`
             WHERE `order_status_id` <> b'0001'
               AND `date_confirmed` IS NOT NULL
               AND `date_cancelled` IS NULL
               AND `date_deleted` IS NULL
        SQL;
        $query = preg_replace('/\s+/', ' ', trim($query));

        try {
            $statement = $this->Database->query($query);
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($query, $statement);
            if (\is_int($result)) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return 0;
    }

    /**
     * Gets an existing order.
     *
     * @param string $id
     *   Universally unique identifier (UUID) of the order.
     *
     * @return StoreCore\Order
     *   Returns an `Order` object, or subclasses of an `Order` for
     *   special use cases, such as a `Cart` or a `WishList`.
     */
    public function get(string $id): Order
    {
        try {
            $result = $this->orderMapper->read($id);
            if (!\is_object($result)) {
                throw new NotFoundException('Order ' . $id . ' not found.');
            }
            return $result;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if an order exists.
     *
     * @param string $id
     *   Universally unique identifier (UUID) of the order as a string or
     *   order ID, order number, or confirmation number as a numeric string.
     *
     * @return bool
     *   Returns `true` if the order exists, otherwise `false`.
     */
    public function has(string $id): bool
    {
        try {
            if (ctype_digit($id) && !UUID::validate($id)) {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_orders` WHERE BINARY `order_id` = ? OR `order_number` = ?'
                );
                $statement->bindValue(1, (int) $id, \PDO::PARAM_INT);
                $statement->bindValue(2, $id, \PDO::PARAM_INT);
            } elseif (UUID::validate($id)) {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_orders` WHERE BINARY `order_uuid` = UNHEX(?)'
                );
                $id = strtolower($id);
                $id = str_replace('-', '', $id);
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
            } else {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_orders` WHERE `order_number` = ? OR `confirmation_number` = ?'
                );
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
                $statement->bindValue(2, $id, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $count = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count >= 1) {
                    return true;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Saves an order.
     * 
     * @param object &$order
     *   The order to save.
     *
     * @return string
     *   Returns the UUID (universally unique identifier) of the order as a string.
     */
    public function set(object &$order): string
    {
        if ($order->metadata->id === null) {
            $this->orderMapper->create($order);
        } else {
            $this->orderMapper->update($order);
        }
        return $order->getIdentifier()->__toString();
    }

    /**
     * Deletes an order.
     *
     * @param string $id
     *   Identifier of the order.
     *
     * @return bool
     *   Returns `true` on success or `false` or failure.
     */
    public function unset(string $id): bool
    {
        $id = strtolower($id);
        $id = str_replace('-', '', $id);
        if (ctype_digit($id) && !UUID::validate($id)) {
            $id = (int) $id;
        }
        return $this->orderMapper->delete($id);
    }
}
