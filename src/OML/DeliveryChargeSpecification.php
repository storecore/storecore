<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\PIM\PriceSpecification;
use StoreCore\Types\{GeoShape, Place};

assert(class_exists('StoreCore\\PIM\\PriceSpecification'));

/**
 * Delivery charge specification.
 *
 * @api
 * @package StoreCore\Core
 * @version 0.2.0
 *
 * @see https://schema.org/DeliveryChargeSpecification
 *      Schema.org type DeliveryChargeSpecification
 *
 * @see https://developers.google.cn/gmail/markup/reference/types/DeliveryChargeSpecification
 *      DeliveryChargeSpecification - Email Markup for Gmail - Google Developers
 */
class DeliveryChargeSpecification extends PriceSpecification implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\OML\DeliveryMethod|null $appliesToDeliveryMethod
     *   The delivery method(s) to which the delivery charge or payment charge
     *   specification applies.
     */
    public ?DeliveryMethod $appliesToDeliveryMethod = null;

    /**
     * @var string|StoreCore\Types\GeoShape|StoreCore\Types\Place|null $eligibleRegion
     *   The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the `Place`,
     *   or the `GeoShape` for the geo-political region(s) for which the offer
     *   or delivery charge specification is valid.
     */
    public string|GeoShape|Place|null $eligibleRegion = null;
}
