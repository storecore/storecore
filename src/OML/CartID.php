<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2019, 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \Random\Randomizer;
use StoreCore\Registry;
use StoreCore\Types\{UUID, UUIDFactory};

use function \array_key_exists;
use function \base64_decode;
use function \base64_encode;
use function \count;
use function \empty;
use function \is_array;
use function \is_string;
use function \isset;
use function \json_decode;
use function \json_encode;
use function \strlen;
use function \trim;

/**
 * Shopping cart identifier.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class CartID implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
   public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $value
     *   Indexed array consisting of a Semantic Versioning (SemVer) version ID
     *   (index 0), a universally unique identifier or UUID (index 1), and a
     *   random token string (index 2).
     */
    private array $value;

    /**
     * Constructs a cart identifier.
     *
     * @param null|StoreCore\Types\UUID|string $uuid
     *   Existing universally unique identifier (UUID) or `null`.
     *
     * @param null|string $token
     *   Existing token or `null`.
     */
    public function __construct(null|UUID|string $uuid = null, ?string $token = null)
    {
        $this->value = array(0 => self::VERSION);
        if ($uuid !== null) $this->setUUID($uuid);
        if ($token !== null) $this->setToken($token);
    }

    /**
     * Converts the cart ID data object to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the cart identifier as an encoded string.
     *
     * @uses StoreCore\OML\CartID::encode()
     */
    public function __toString(): string
    {
        return $this->encode();
    }

    /**
     * Decodes a Base64 and JSON encoded string to a shopping cart ID.
     *
     * @param string|mixed $from_string
     *   Encoded string to decode to a cart identifier.  This method accepts
     *   other data types, but will then always return false.
     *
     * @return static|false
     *   Returns a `CartID` value object on success, otherwise `false`.
     */
    public static function createFromString(mixed $from_string): static|false
    {
        if ($from_string instanceof \Stringable) {
            $from_string = (string) $from_string;
        }

        if (!is_string($from_string)) {
            return false;
        }

        $from_string = trim($from_string);
        if (empty($from_string)) {
            return false;
        }

        $from_string = base64_decode($from_string);
        $data = json_decode($from_string, true, 2);
        if (!is_array($data) || count($data) !== 3) {
            return false;
        }

        if (!array_key_exists(0, $data)) {
            return false;
        }

        if (array_key_exists(1, $data) && array_key_exists(2, $data)) {
            try {
                return new CartID($data[1], $data[2]);
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Gets the shopping cart ID as a MIME Base64 and JSON encoded string.
     *
     * @param void
     *
     * @return string
     *   Cart identifier encoded to a string.
     */
    public function encode(): string
    {
        if (!isset($this->value[1])) {
            $this->resetUUID();
        }

        if (!isset($this->value[2])) {
            $this->resetToken();
        }

        $cart_id = $this->value;
        $cart_id[1] = (string) $cart_id[1];
        $cart_id = json_encode($cart_id);
        $cart_id = base64_encode($cart_id);
        return $cart_id;
    }

    /**
     * Gets the shopping cart ID token.
     *
     * @param void
     *
     * @return string
     *   Returns the random cart token as a string with a fixed length
     *   of 192 ASCII characters.
     */
    public function getToken(): string
    {
        if (!isset($this->value[2])) {
            $this->resetToken();
        }
        return $this->value[2];
    }

    /**
     * Gets the shopping cart UUID.
     *
     * @param void
     *
     * @return StoreCore\Types\UUID
     *   Universally unique identifier (UUID).
     */
    public function getUUID(): UUID
    {
        if (!isset($this->value[1])) {
            $this->resetUUID();
        }
        return $this->value[1];
    }

    /**
     * Regenerates the random shopping cart token.
     *
     * @param void
     * @return void
     */
    public function resetToken(): void
    {
        $randomizer = new Randomizer();
        $characters = $randomizer->shuffleBytes('0123456789abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWZYZ');
        $token = (string) null;
        for ($i = 1; $i <= 192; $i++) {
            $token .= substr($characters, $randomizer->getInt(0, 61), 1);
        }
        $this->value[2] = $token;
    }

    /**
     * Generates a random UUID.
     *
     * A new UUID or GUID should be generated with a database function like
     * UUID() in MySQL when a cart is added to the database.
     *
     * @param void
     * @return void
     * @uses StoreCore\Registry::getInstance()
     * @uses StoreCore\Types\UUIDFactory::createUUID()
     */
    private function resetUUID(): void
    {
        $registry = Registry::getInstance();
        $factory = new UUIDFactory($registry);
        $this->value[1] = $factory->createUUID();
    }

    /**
     * Sets or resets the shopping cart token.
     *
     * @param string $token
     *   Shopping cart token as a string.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error unexpected value runtime exception if the token
     *   is not a string or does not have a string length of 192 characters.
     */
    public function setToken(string $token): void
    {
        if (!is_string($token) || strlen($token) !== 192) {
            throw new \ValueError('Invalid cart token', time());
        }
        $this->value[2] = $token;
    }

    /**
     * Sets the shopping cart UUID.
     *
     * @param StoreCore\Types\UUID $uuid
     *   Universally unique identifier (UUID) as a value object or string.
     *
     * @return void
     */
    public function setUUID(UUID|string $uuid): void
    {
        if (is_string($uuid)) $uuid = new UUID($uuid);
        $this->value[1] = $uuid;
    }
}
