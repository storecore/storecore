<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \DomainException;
use StoreCore\Geo\Country;

use function \is_int;
use function \strlen;
use function \strtoupper;
use function \substr;
use function \trim;

/**
 * Postal code factory.
 *
 * @package StoreCore\OML
 * @see     https://en.wikipedia.org/wiki/List_of_postal_codes List of postal codes
 * @version 1.0.0
 */
class PostalCodeFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var StoreCore\Geo\Country|null $country
     *   OPTIONAL `Country` value object.
     */
    private ?Country $country = null;

    /**
     * Creates a postal code factory.
     *
     * @param null|StoreCore\Geo\Country $country
     *   OPTIONAL destination country.  The country MAY be used to validate
     *   the syntax of the postal code and the country code MAY be added as
     *   a prefix for international parcel and mail delivery.
     *
     * @uses setCountry()
     */
    public function __construct(?Country $country = null)
    {
        if ($country !== null) $this->setCountry($country);
    }

    /**
     * Creates a postal code.
     *
     * @param string|int $code
     *   Postal code as a case-insensitive string or an integer.
     *   An integer is converted to a numeric string.
     *
     * @return StoreCore\OML\PostalCode
     */
    public function createPostalCode(string|int $code): PostalCode
    {
        if (is_int($code)) $code = (string) $code;
        $code = trim($code);

        // Use optional ISO country code, if it exists.
        if ($this->country !== null) {
            switch ($this->country->identifier) {
                /*
                 * Netherlands (NL).
                 * Postcodes for the Netherlands consist of four digits, an optional
                 * but recommended single space, and two uppercase letters.
                 */
                case 'NL':
                case 'NLD':
                case '528':
                    $code = strtoupper($code);
                    if (strlen($code) !== 7) {
                        if (strlen($code) === 6) {
                            $code = substr($code, 0, 4) . ' ' . substr($code, -2);
                        } else {
                            throw new DomainException('Invalid Dutch post code: ' . $code);
                        }
                    }
                    break;
            }
        }

        return new PostalCode($code);
    }

    /**
     * Sets the destination country.
     *
     * @param StoreCore\Geo\Country $country
     *   Destination country for parcels, letters, mail, etc.
     *
     * @return void
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }
}
