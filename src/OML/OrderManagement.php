<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\AbstractController;
use StoreCore\Order;
use StoreCore\Database\OrderMapper;

/**
 * Order management controller.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderManagement extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\OrderMapper $orderMapper
     *   Data mapper for orders.
     */
    private OrderMapper $orderMapper;

    /**
     * Cancels an order.
     *
     * @param StoreCore\Order $order
     *   Order to cancel.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function cancel(Order &$order): bool
    {
        if ($order->orderStatus === OrderStatus::OrderCancelled) {
            return true;
        }

        if (!isset($this->orderMapper)) {
            $this->orderMapper = new OrderMapper($this->Registry);
        }
        return $this->orderMapper->cancel($order);
    }

    /**
     * Confirms an order.
     *
     * @param StoreCore\Order $order
     *   Order to confirm.  An `Order` can only by confirmed if it does not
     *   yet have an `orderDate` and the `orderStatus` is not some kind of
     *   `OrderProblem`.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function confirm(Order &$order): bool
    {
        if (
            $order->orderDate !== null
            || $order->orderStatus === OrderStatus::OrderProblem
        ) {
            return false;
        }

        if (!isset($this->orderMapper)) {
            $this->orderMapper = new OrderMapper($this->Registry);
        }
        return $this->orderMapper->confirm($order);
    }
}
