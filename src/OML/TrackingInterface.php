<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Tracking interface.
 *
 * This interface provides two getters for the Schema.org properties
 * `trackingNumber` and `trackingUrl` of a `ParcelDelivery` object.
 *
 * @api
 * @package  StoreCore\OML
 * @property string $trackingNumber
 * @property string $trackingUrl
 * @see      https://schema.org/ParcelDelivery Schema.org type ParcelDelivery
 * @version  1.0.0
 */
interface TrackingInterface
{
    /**
     * Gets a tracking number.
     *
     * @param void
     *
     * @return string
     *   Shipper tracking number or similar unique identifier.
     */
    public function getTrackingNumber(): string;

    /**
     * Gets a tracking URL.
     *
     * @param void
     *
     * @return string
     *   Tracking URL for parcel delivery.
     */
    public function getTrackingUrl(): string;
}
