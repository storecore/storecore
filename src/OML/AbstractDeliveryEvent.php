<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \DateInterval, \DateTime, \DateTimeImmutable, \DateTimeInterFace;

/**
 * Abstract delivery event.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 *
 * @see https://schema.org/DeliveryEvent
 *      Schema.org type Thing > Event > DeliveryEvent
 *
 * @see https://developers.google.com/gmail/markup/reference/types/DeliveryEvent
 *      DeliveryEvent - Email Markup - Google Developers
 */
abstract class AbstractDeliveryEvent implements DeliveryEventInterface
{
    /**
     * @var null|string $accessCode
     *   Password, PIN, or access code needed for delivery (e.g. from a locker).
     */
    protected ?string $accessCode = null;

    /**
     * @var null|DateTimeImmutable $availableFrom
     *   When the item is available for pickup from the store, locker, etc.
     */
    protected ?DateTimeImmutable $availableFrom = null;

    /**
     * @var string $availableFromFormat
     *   DateTime string format of the `$availableFrom` property.
     */
    protected string $availableFromFormat = DateTimeInterface::ATOM;

    /**
     * @var DateTimeImmutable|null $availableFrom
     *   After this date, the item will no longer be available for pickup.
     */
    protected ?DateTimeImmutable $availableThrough = null;

    /**
     * @var string $availableThroughFormat
     *   String format of the `$availableThrough` property.
     */
    protected string $availableThroughFormat = DateTimeInterface::ATOM;

    /**
     * @var DeliveryMethod|null $availableFrom
     *   After this date, the item will no longer be available for pickup.
     *   This property is called `hasDeliveryMethod` in Schema.org.
     */
    protected ?DeliveryMethod $deliveryMethod = null;


    /**
     * Gets a Schema.org DeliveryEvent property.
     *
     * @param string $name
     *   Name of the Schema.org property.
     *
     * @return mixed
     *   Returns the property value or null if the property does not exist.
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'accessCode' => $this->getAccessCode(),
            'availableFrom' => $this->getAvailableFrom(),
            'availableThrough' => $this->getAvailableThrough(),
            'deliveryMethod', 'hasDeliveryMethod' => $this->getDeliveryMethod(),
            default => null,
        };
    }

    /**
     * Sets a DeliveryEvent property.
     *
     * @param string $name
     *   Name of the property.
     *
     * @param mixed $value
     *   Value of the property to set.
     *
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'accessCode' => $this->setAccessCode($value),
            'availableFrom' => $this->setAvailableFrom($value),
            'availableThrough' => $this->setAvailableThrough($value),
            'deliveryMethod', 'hasDeliveryMethod' => $this->setDeliveryMethod($value),
        };
    }

    #[\Override]
    public function getAccessCode(): ?string
    {
        return $this->accessCode;
    }

    #[\Override]
    public function getAvailableFrom(): ?string
    {
        return $this->availableFrom === null ? null : $this->availableFrom->format($this->availableFromFormat);
    }

    #[\Override]
    public function getAvailableThrough(): ?string
    {
        return $this->availableThrough === null ? null : $this->availableThrough->format($this->availableThroughFormat);
    }

    #[\Override]
    public function getDeliveryMethod(): ?DeliveryMethod
    {
        return $this->deliveryMethod;
    }

    /**
     * Sets the access code.
     *
     * @param string|int $access_code
     *   Password, PIN, or access code needed for delivery.
     *
     * @return void
     */
    public function setAccessCode(string|int $access_code): void
    {
        $this->accessCode = (string) $access_code;
    }

    /**
     * Sets the date/time when the item is available for pickup.
     *
     * @param \DateTimeInterface|string $available_from
     *   Optional date or date and time as an object that implements the PHP
     *   `DateTimeInterface` or as a string in ISO 8601 format.  If omitted,
     *   or set to the default string value 'now', the current date and time
     *   are used.
     *
     * @return void
     */
    public function setAvailableFrom(DateTimeInterface|string $available_from = 'now'): void
    {
        if ($available_from instanceof DateTimeInterface) {
            $this->availableFrom = DateTimeImmutable::createFromInterface($available_from);
            $this->availableFromFormat = DateTimeInterface::ATOM;
            return;
        }

        if (!is_string($available_from)) {
            throw new \InvalidArgumentException();
        }

        // Current date and time
        if (strtolower($available_from) === 'now') {
            $this->availableFrom = new DateTimeImmutable('now');
            $this->availableFromFormat = DateTimeInterface::ATOM;
            return;
        }
        
        // ISO 8601 date in `yyyy-mm-dd` format without time
        if (strlen($available_from) === 10) {
            $datetime = DateTime::createFromFormat('Y-m-d', $available_from);
            if ($datetime !== false) {
                $datetime->setTime(0, 0);
                $this->availableFrom = DateTimeImmutable::createFromMutable($datetime);
                $this->availableFromFormat = 'Y-m-d';
                return;
            }
        }

        $datetime = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $available_from);
        if ($datetime !== false) {
            $this->availableFrom = $datetime;
            $this->availableFromFormat = DateTimeInterface::ATOM;
            return;
        }

        $datetime = DateTimeImmutable::createFromFormat(DateTimeInterface::ISO8601, $available_from);
        if ($datetime !== false) {
            $this->availableFrom = $datetime;
            $this->availableFromFormat = DateTimeInterface::ISO8601;
            return;
        }

        $datetime = new DateTimeImmutable($available_from);
        if ($datetime !== false) {
            $this->availableFrom = $datetime;
            $this->availableFromFormat = DateTimeInterface::ATOM;
        } else {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * Sets the date/time when the item is no longer available for pickup.
     *
     * @param DateTimeInterface|DateInterval|string $available_through
     *   Date or date and time as a `DateTime` or `DateTimeImmutable`, as a
     *   `DateInterval`, or as a string.  Use the string `now` for the current
     *   date and time.  A `DateInterval` object for a period is added to an
     *   existing `availableFrom` start date to calculate the relative
     *   `availableThrough` end date; if the `availableFrom` start date
     *   is not yet set, the availability period will start now.
     *
     * @return void
     */
    public function setAvailableThrough(DateTimeInterface|DateInterval|string $available_through): void
    {
        if ($available_through instanceof DateTimeInterface) {
            $this->availableThrough = DateTimeImmutable::createFromInterface($available_through);
            $this->availableThroughFormat = DateTimeInterface::ATOM;
            return;
        }

        if ($available_through instanceof DateInterval) {
            if ($this->getAvailableFrom() === null) {
                $this->setAvailableFrom(new DateImmutable('now'));
            }
            $datetime = DateTime::createFromImmutable($this->availableFrom);
            $datetime->add($available_through);
            $this->setAvailableThrough($datetime);
            $this->availableThroughFormat = $this->availableFromFormat;
            return;
        }

        if (!is_string($available_through)) {
            throw new \InvalidArgumentException();
        }

        if (strtolower($available_through) === 'now') {
            $this->availableThrough = new DateTimeImmutable('now');
            $this->availableThroughFormat = DateTimeInterface::ATOM;
            return;
        }

        // Try to match the `$availableFromFormat` if the `$availableFrom` is set,
        // thus improving consistency between the start and end date/time.
        if ($this->availableFrom !== null) {
            $datetime = DateTimeImmutable::createFromFormat($this->availableFromFormat, $available_through);
            if ($datetime !== false) {
                $this->availableThrough = $datetime;
                $this->availableThroughFormat = $this->availableFromFormat;
                return;
            }
        }

        // ISO 8601 date in `yyyy-mm-dd` format without time
        if (strlen($available_through) === 10) {
            $datetime = DateTime::createFromFormat('Y-m-d', $available_through);
            if ($datetime !== false) {
                $datetime->setTime(23, 59, 59);
                $this->availableThrough = DateTimeImmutable::createFromMutable($datetime);
                $this->availableThroughFormat = 'Y-m-d';
                return;
            }
        }

        $datetime = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $available_through);
        if ($datetime !== false) {
            $this->availableThrough = $datetime;
            $this->availableThroughFormat = DateTimeInterface::ATOM;
            return;
        }

        $datetime = DateTimeImmutable::createFromFormat(DateTimeInterface::ISO8601, $available_through);
        if ($datetime !== false) {
            $this->availableThrough = $datetime;
            $this->availableThroughFormat = DateTimeInterface::ISO8601;
            return;
        }

        $datetime = new DateTimeImmutable($available_through);
        if ($datetime !== false) {
            $this->availableThrough = $datetime;
            $this->availableThroughFormat = DateTimeInterface::ATOM;
        } else {
            throw new \InvalidArgumentException();
        }
    }


    /**
     * Sets the delivery method.
     *
     * @param StoreCore\OML\DeliveryMethod $delivery_method
     *   Delivery method associated with this delivery event.
     *
     * @return void
     */
    public function setDeliveryMethod(DeliveryMethod $delivery_method): void
    {
        $this->deliveryMethod = $delivery_method;
    }
}
