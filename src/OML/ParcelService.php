<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Parcel service.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/ParcelService Schema.org type `ParcelService`
 * @version 1.0.0
 */
class ParcelService extends DeliveryMethod implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
