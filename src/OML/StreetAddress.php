<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \Stringable;
use StoreCore\Registry;
use StoreCore\Geo\PostalAddress;
use StoreCore\Geo\Country;

/**
 * Street address.
 *
 * A street address is a single-line string that usually consists
 * of at least a street name and a house number.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/streetAddress Schema.org property `streetAddress`
 * @version 1.0.0-alpha.1
 */
class StreetAddress implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Geo\Country $country
     *   The country the address is located in and therefore the destination
     *   country for post and parcel delivery.  The destination country
     *   determines the proper order of the composing elements in the full
     *   street address string.
     */
    private ?Country $country = null;

    /**
     * @var null|string $houseNumber
     *   Optional number of the house.
     */
    private ?string $houseNumber = null;

    /**
     * @var null|string $houseNumberAddition
     *   Optional addition to the house number, usually some kind of suffix.
     */
    private ?string $houseNumberAddition = null;

    /**
     * @var null|string $streetName
     *   Name of the street.
     */
    private ?string $streetName = null;

    /**
     * Creates a street address.
     *
     * @param null|string $street_name
     * @param null|int|string $house_number
     * @param null|string $house_number_addition
     */
    public function __construct(
        ?string $street_name = null,
        null|int|string $house_number = null,
        ?string $house_number_addition = null,
    ) {
        if ($street_name !== null) $this->setStreetName($street_name);

        if ($house_number !== null) {
            $this->setHouseNumber($house_number);
            if (!empty($house_number_addition)) $this->setHouseNumberAddition($house_number_addition);
        }
    }

    /**
     * Generic property getter.
     *
     * @param string $name
     *   Name of the property.
     *
     * @return mixed|null
     *   Returns the property value, or null if the property does not exist
     *   or is not set.
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'country' => $this->getCountry(),
            'houseNumber' => $this->getHouseNumber(),
            'houseNumberAddition' => $this->getHouseNumberAddition(),
            'streetName' => $this->getStreetName(),
            default => null
        };
    }

    /**
     * Generic property setter.
     *
     * @param string $name
     *   Name of the property.
     *
     * @param mixed $value
     *   Value of the property.
     *
     * @return void
     *
     * @throws \DomainException
     *   Throws a Standard PHP Library (SPL) domain exception if the class
     *   property does not exist.
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'country' => $this->setCountry($value),
            'houseNumber' => $this->setHouseNumber($value),
            'houseNumberAddition' => $this->setHouseNumberAddition($value),
            'streetName' => $this->setStreetName($value),
            default => throw new \DomainException(__CLASS__ . ' class property "' . $name . '" does not exist')
        };
    }

    /**
     * Converts the street address to a one-line string.
     *
     * @param void
     * @return string
     *
     * @uses StoreCore\Geo\Country::getIdentifier()
     *   The destination country determines the position of the house number
     *   (before or after the street name).
     */
    public function __toString(): string
    {
        if ($this->getStreetName() === null) return '';

        $street_address = $this->getStreetName();

        if ($this->getHouseNumber() !== null) {
            $number = $this->getHouseNumber();
            if ($this->getHouseNumberAddition() !== null) {
                $number = $number . ' ' . $this->getHouseNumberAddition();
            }

            if ($this->country === null) {
                $country_code = null;
            } else {
                $country_code = $this->country->identifier;
            }

            switch ($country_code) {
                // House number after street name
                case 'BE' || 'BEL' || '056':
                case 'DE' || 'DEU' || '276':
                case 'NL' || 'NLD' || '528':
                    $street_address = $street_address . ' ' . $number;
                    break;
                // House number before street name
                case 'FR' || 'FRA' || '250':
                case 'US' || 'USA' || '840':
                    $street_address = $number . ' ' . $street_address;
                    break;
                // Default case: house number before street name
                // (included so the default may be changed later)
                default:
                    $street_address = $number . ' ' . $street_address;
            }
        }

        return $street_address;
    }

    /**
     * Creates a street address value object from an address.
     *
     * @param StoreCore\Geo\PostalAddress $address
     *   Address as a value object.
     *
     * @return StoreCore\OML\StreetAddress
     */
    public static function createFromPostalAddress(PostalAddress $address): StreetAddress
    {
        $result = new static;

        $country = $address->getCountry();
        if ($country !== null) {
            $result->setCountry($country);
        }

        if ($address->has('streetName')) {
            $result->setStreetName($address->get('streetName'));
        }

        if ($address->has('houseNumber')) {
            $result->setHouseNumber($address->get('houseNumber'));
        }

        if ($address->has('houseNumberAddition')) {
            $result->setHouseNumberAddition($address->get('houseNumberAddition'));
        }

        return $result;
    }

    /**
     * Creates a street address value object from a single string.
     *
     * @param string $street_address
     *   Street address as a string.
     *
     * @return StoreCore\OML\StreetAddress|false
     *   Returns a `StreetAddress` value object on success or `false` on failure.
     */
    public static function createFromString(string $street_address): StreetAddress|false
    {
        $street_address = trim($street_address);
        $street_address = preg_replace('!\s+!', ' ', $street_address);
        if (empty($street_address)) return false;

        $address = explode(' ', $street_address);

        // If the array contains only one element,
        // it probably functions as a street name.
        if (count($address) === 1) {
            $street_address = new StreetAddress();
            $street_address->setStreetName($address[0]);
            return $street_address;
        }

        if (is_numeric($address[0])) {
            $street_address = new StreetAddress();
            $street_address->setHouseNumber($address[0]);
            unset($address[0]);
            $street_address->setStreetName(implode(' ', $address));
            return $street_address;
        }

        $n = array_key_last($address);
        if (\is_int($n) && is_numeric($address[$n])) {
            $street_address = new StreetAddress();
            $street_address->setHouseNumber($address[$n]);
            unset($address[$n]);
            $street_address->setStreetName(implode(' ', $address));
            return $street_address;
        }

        return false;
    }

    
    /**
     * Gets the address country.
     *
     * @param void
     *
     * @return StoreCore\Geo\Country|null
     *   Country the address is in or `null` if the country is not set.
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Gets the house number.
     *
     * @param void
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * Gets the house number addition.
     *
     * @param void
     * @return string|null
     */
    public function getHouseNumberAddition(): ?string
    {
        return $this->houseNumberAddition;
    }

    /**
     * Gets the street name.
     *
     * @param void
     * @return string|null
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     * Sets the country.
     *
     * @param string|int|StoreCore\Geo\Country $country
     *   Numeric or alphanumeric country code or a country value object.
     *
     * @return void
     */
    public function setCountry(string|int|Country $country): void
    {
        if ($country instanceof Country) {
            $this->country = $country;
        } else {
            $country_repository = new CountryRepository(Registry::getInstance());
            if ($country_repository->has($country)) {
                $this->country = $country_repository->get($country);
            } else {
                throw new \InvalidArgumentException('Unknown or invalid country identifier');
            }
        }
    }

    /**
     * Sets the house number.
     *
     * @param int|string $house_number
     *   House number as an integer or a string.  An integer is converted to
     *   a numeric string.
     *
     * @return void
     */
    public function setHouseNumber(int|string $house_number): void
    {
        if (\is_int($house_number)) $house_number = \strval($house_number);
        $this->houseNumber = $house_number;
    }

    /**
     * Sets a house number addition.
     *
     * @param string $house_number_addition
     *   Addition to a house number as a string.
     *
     * @return void
     */
    public function setHouseNumberAddition(string $house_number_addition): void
    {
        $this->houseNumberAddition = $house_number_addition;
    }

    /**
     * Sets the street name.
     *
     * @param string $street_name
     *   Name of the street as a string, without a house number.
     *
     * @return void
     *
     * @throws \TypeError
     *   Throws an type error exception if the street name is not a string.
     */
    public function setStreetName(string $street_name): void
    {
        if (!\is_string($street_name)) throw new \TypeError();

        $street_name = trim($street_name);
        if (empty($street_name)) {
            throw new \InvalidArgumentException();
        }

        $this->streetName = $street_name;
    }
}
