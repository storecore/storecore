<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\VisitableInterface;
use StoreCore\Order;

/**
 * Back-order model.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/BackOrder
 * @version 1.0.0-alpha.1
 */
class BackOrder extends Order implements
    \Countable,
    IdentityInterface,
    VisitableInterface
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    public ?OrderStatus $orderStatus = OrderStatus::OrderProcessing;
}
