<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * On-site pickup.
 *
 * An “on-site pickup” is a delivery method in which an item is collected
 * on site, e.g. in a store or at a box office.
 * 
 * @package StoreCore\OML
 * @see     https://schema.org/OnSitePickup Schema.org type `OnSitePickup`
 * @version 1.0.0
 */
class OnSitePickup extends DeliveryMethod implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
