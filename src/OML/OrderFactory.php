<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\IdentityInterface;

use StoreCore\AbstractModel;
use StoreCore\Registry;
use StoreCore\{BrokerOrder, Cart, Order, WishList};
use StoreCore\Database\OrderMapper;
use StoreCore\Types\{UUIDFactory, UUID};

/**
 * Order factory.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderFactory extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\OrderMapper $orderMapper;
     *   Data access object (DAO) to create database objects.
     */
    private readonly OrderMapper $orderMapper;

    /**
     * @var null|StoreCore\Types\UUID $sellerIdentifier
     *   OPTIONAL universally unique identifier (UUID) of the seller.
     *   This property MAY be set to the `OnlineStore` that is currently
     *   handling orders.
     */
    private ?UUID $sellerIdentifier = null;

    /**
     * Creates an order factory.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->orderMapper = new OrderMapper($this->Registry);
    }

    /**
     * Creates a new cart.
     *
     * @param StoreCore\Types\UUID|null $seller_uuid
     *   OPTIONAL seller identifier as a UUID.
     *
     * @return StoreCore\Cart
     *   Returns a `Cart` data object for a new and empty cart.
     */
    public function createCart(?UUID $seller_uuid = null): Cart
    {
        if ($seller_uuid instanceof UUID) {
            $this->setSellerIdentifier($seller_uuid);
        }

        $cart = new Cart($this->Registry);
        if ($this->sellerIdentifier instanceof UUID) {
            $cart->setSellerIdentifier($this->sellerIdentifier);
        }

        try {
            $this->orderMapper->create($cart);
        } catch (\Exception $e) {
            $this->Logger->error('Error creating new shopping: ' . $e->getMessage());
        }
        return $cart;
    }

    /**
     * Creates a new order.
     *
     * @param StoreCore\IdentityInterface|StoreCore\Types\UUID|null $seller
     *   OPTIONAL seller identity.  If omitted, the default store is used.
     *
     * @param null|int|string $order_number
     *   OPTIONAL order number.  This feature is provided in order to to link
     *   StoreCore orders to orders from a remote sales, purchase, order
     *   order management system.
     *
     * @return StoreCore\Order
     *   Returns an `Order` aggregate object for a new order.
     */
    public function createOrder(
        null|IdentityInterface|UUID $seller = null,
        null|int|string $order_number = null,
    ): Order {
        if ($seller !== null) {
            if ($seller instanceof IdentityInterface) {
                if (!$seller->hasIdentifier()) {
                    throw new \DomainException('Unknown seller');
                }
                $seller = $seller->getIdentifier();
            }
            if ($seller instanceof UUID) {
                $this->setSellerIdentifier($seller);
            }
        }

        $order = new Order($this->Registry);

        if ($this->sellerIdentifier instanceof UUID) {
            $order->setSellerIdentifier($this->sellerIdentifier);
        }

        if ($order_number !== null) {
            $order->orderNumber = $order_number;
        }

        try {
            $this->orderMapper->create($order);
        } catch (\Exception $e) {
            $this->Logger->error('Error creating new order: ' . $e->getMessage());
        }
        return $order;
    }

    /**
     * Creates a new wish list.
     *
     * @param void
     * 
     * @return StoreCore\WishList
     *   Returns a wish list object.
     */
    public function createWishList(): WishList
    {
        $result = new WishList($this->Registry);

        if ($this->sellerIdentifier instanceof UUID) {
            $result->setSellerIdentifier($this->sellerIdentifier);
        }

        try {
            $this->orderMapper->create($result);
        } catch (\Exception $e) {
            $this->Logger->error('Error creating new wish list: ' . $e->getMessage());
        }
        return $result;
    }

    /**
     * Sets the seller UUID.
     *
     * @param StoreCore\Types\UUID|string
     *   UUID of the seller as a value object or a string.
     * 
     * @return self
     */
    public function setSellerIdentifier(UUID|string $uuid): self
    {
        if (\is_string($uuid)) {
            $uuid = new UUID($uuid);
        }
        $this->sellerIdentifier = $uuid;
        return $this;
    }
}
