<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \JsonSerializable;

use function \is_string;

/**
 * Postal code range specification.
 *
 * The Schema.org `PostalCodeRangeSpecification` indicates a range of postal
 * codes, usually defined as the set of valid codes between `postalCodeBegin`
 * and `postalCodeEnd`, inclusively.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/PostalCodeRangeSpecification Schema.org type `PostalCodeRangeSpecification`
 * @version 1.0.0-beta.1
 */
readonly class PostalCodeRangeSpecification implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * @var PostalCode $postalCodeBegin
     *   First postal code in a range (included).
     */
    public PostalCode $postalCodeBegin;

    /**
     * @var PostalCode $postalCodeEnd
     *   Last postal code in the range (included).
     *   Needs to be after `$postalCodeBegin`.
     */
    public PostalCode $postalCodeEnd;

    /**
     * Creates a postal code range specification.
     *
     * @param StoreCore\OML\PostalCode|string $postal_code_begin
     * @param StoreCore\OML\PostalCode|string $postal_code_end
     */
    public function __construct(
        PostalCode|string $postal_code_begin,
        PostalCode|string $postal_code_end
    ) {
        if (is_string($postal_code_begin)) $postal_code_begin = new PostalCode($postal_code_begin);
        if (is_string($postal_code_end)) $postal_code_end = new PostalCode($postal_code_end);
        $this->postalCodeBegin = $postal_code_begin;
        $this->postalCodeEnd = $postal_code_end;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            '@context'        => 'https://schema.org',
            '@type'           => 'PostalCodeRangeSpecification',
            'postalCodeBegin' => (string) $this->postalCodeBegin,
            'postalCodeEnd' => (string) $this->postalCodeEnd,
        ];
    }
}
