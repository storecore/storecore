<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\Order;
use StoreCore\PIM\Product;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\URL;

/**
 * Abstract parcel delivery.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/ParcelDelivery Schema.org type `ParcelDelivery`
 * @see     https://developers.google.com/gmail/markup/reference/parcel-delivery Gmail API Reference
 * @version 1.0.0-alpha.1
 */
abstract class AbstractParcelDelivery implements
    \JsonSerializable,
    TrackingInterface
{
    /**
     * @var StoreCore\Types\Date|StoreCore\Types\DateTime|null $expectedArrivalFrom;
     *   The earliest date the package MAY arrive.
     */
    public null|Date|DateTime $expectedArrivalFrom = null;

    /**
     * @var StoreCore\Types\Date|StoreCore\Types\DateTime|null $expectedArrivalUntil;
     *   The latest date the package MAY arrive.
     */
    public null|Date|DateTime $expectedArrivalUntil = null;

    /**
     * @var StoreCore\PIM\Product $itemShipped
     *   Item(s) being shipped.
     */
    public Product $itemShipped;

    /**
     * @var StoreCore\Order|StoreCore\OML\OrderProxy $partOfOrder
     *   The overall order the items in this delivery were included in.
     *   Implements {@link https://schema.org/partOfOrder ParcelDelivery.partOfOrder}.
     */
    public Order|OrderProxy $partOfOrder;

    /**
     * @var StoreCore\OML\ProviderInterface|null $provider
     *   The service provider, service operator, or service performer.
     */
    public ?ProviderInterface $provider = null;

    /**
     * @var null|string $trackingNumber
     *   Shipper tracking number. 
     */
    protected ?string $trackingNumber = null;

    /**
     * @var null|StoreCore\Types\URL $trackingUrl
     *   Tracking URL for the parcel delivery. 
     */
    protected ?URL $trackingUrl = null;

    /**
     * Accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'carrier' => $this->provider,
            'trackingNumber' => $this->getTrackingNumber(),
            'trackingUrl' => $this->getTrackingUrl(),
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'carrier' => $this->provider = $value,
            'trackingNumber' => $this->trackingNumber = (string) $value,
            'trackingUrl' => $this->trackingUrl = $value,
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Gets a tracking number.
     *
     * @param void
     *
     * @return string
     *   Shipper tracking number or similar unique identifier as a string.
     *   This string MAY be empty.
     */
    public function getTrackingNumber(): string
    {
        return (string) $this->trackingNumber;
    }

    /**
     * Gets a tracking URL.
     *
     * @param void
     *
     * @return string
     *   Tracking URL for parcel delivery as a string.
     *   This string MAY be empty if there is no tracking URL.
     */
    public function getTrackingUrl(): string
    {
        return (string) $this->trackingUrl;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     * @see https://developers.google.com/gmail/markup/reference/datetime-formatting#php
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'ParcelDelivery',
        ];

        $properties = get_object_vars($this);
        $properties = array_filter($properties);

        foreach ($properties as $name => $value) {
            if ($value instanceof \JsonSerializable) {
                $properties[$name] = $value->jsonSerialize();
                if (
                    isset($properties[$name]['@context'])
                    && $properties[$name]['@context'] === 'https://schema.org'
                ) {
                    unset($properties[$name]['@context']);
                }
            }
        }

        return $result + $properties;
    }
}
