<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Delivery method trait.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/deliveryMethod Schema.org property `deliveryMethod`
 * @version 1.0.0
 */
trait DeliveryMethodTrait
{
    /**
     * @var null|StoreCore\OML\DeliveryMethod 
     *   OPTIONAL `deliveryMethod` property of an `OrderAction`,
     *   `ReceiveAction`, `SendAction`, or `TrackAction`.
     */
    public ?DeliveryMethod $deliveryMethod = null;
}
