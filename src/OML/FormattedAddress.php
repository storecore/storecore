<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use StoreCore\Geo\Country;

use function \implode;
use function \str_replace;
use function \strtoupper;

/**
 * Formatted address.
 *
 * This class takes a postal address and turns it into multiple lines
 * for a printable address on a label, envelope, packing slip, or invoice.
 *
 * The basic structure of an address with the properties of this class
 * on multiple lines is:
 *
 *     name
 *     streetAddress
 *     postalCode addressLocality
 *     countryName
 *
 * @package StoreCore\OML
 * @version 1.0.0
 */
class FormattedAddress
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|string $addressLocality
     *   Name of the locality in which the street address is, for example
     *   a the name of a city, town, or village.
     */
    private ?string $addressLocality = null;

    /**
     * @var null|StoreCore\Geo\Country $country
     *   Destination country as a value object.
     */
    private ?Country $country = null;

    /**
     * @var null string $countryName
     *   Country name in English and in uppercase for international shipments.
     */
    private ?string $countryName = null;

    /**
     * @var string $endOfLine
     *   End Of Line (EOL) character or control code, for example `<br>` in
     *   HTML.  Defaults to the global PHP constant `PHP_EOL` for the operating
     *   system default.  Use the `endOfLine()` method to set this property.
     */
    private string $endOfLine = \PHP_EOL;

    /**
     * @var null|string $name
     *   Name of the addressee.
     */
    private ?string $name = null;

    /**
     * @var null|string $postalCode
     *   Postcode or similar postal code as a string.
     */
    private ?string $postalCode = null;

    /**
     * @var null|string $streetAddress
     *   The street address, for example '1600 Amphitheatre Pkwy'.
     */
    private ?string $streetAddress = null;


    /**
     * Converts the address to a string with multiple lines.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return implode($this->endOfLine(), $this->toArray());
    }

    /**
     * Sets and gets the End Of Line (EOL) character or string.
     *
     * @param null|string $end_of_line
     *   Optional.  Character, control code or other string for line endings
     *   in a string representation of the full address with multiple lines.
     *
     * @return string 
     */
    public function endOfLine(?string $end_of_line = null): string
    {
        if ($end_of_line !== null) {
            $this->endOfLine = $end_of_line;
        }
        return $this->endOfLine;
    }

    /**
     * Gets the address locality name.
     *
     * @param void
     *
     * @return string|name
     *   Address locality as a string, or null if the locality does not exist.
     */
    public function getAddressLocality(): ?string
    {
        return $this->addressLocality;
    }

    /**
     * Gets the country name.
     *
     * @param void
     *
     * @return null|string
     *   Returns the country name if it exists, or null if it does not exist.
     */
    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    /**
     * Gets the addressee name.
     *
     * @param void
     *
     * @return null|string
     *   Returns name of the addressee if it exists, or null if it does not.
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Gets the postal code.
     *
     * @param void
     *
     * @return null|string
     *   Returns the postal code, or null if it does not exist.
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * Gets the street address.
     *
     * @param void
     *
     * @return null|string
     *   Returns the street address as a string, or null if it does not exist.
     */
    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    /**
     * Sets the address locality.
     *
     * @param string $address_locality
     *   Name of a city, municipality, town, village, etc.
     *
     * @return void
     */
    public function setAddressLocality(string $address_locality): void
    {
        $this->addressLocality = $address_locality;
    }

    /**
     * Sets the destination country.
     *
     * @param StoreCore\Geo\Country $country
     *   Destination country as a value object.
     *
     * @return void
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
        $this->setCountryName($country->name);
    }

    /**
     * Sets the country name.
     *
     * @param string $country_name
     *   Country name as a string.  Lowercase is converted to uppercase.
     *   Accents and diacritical marks are removed.
     *
     * @return void
     */
    public function setCountryName(string $country_name): void
    {
        $country_name = str_replace('ã', 'A', $country_name);
        $country_name = str_replace('é', 'E', $country_name);
        $country_name = str_replace('ô', 'O', $country_name);
        if (function_exists('iconv')) {
            $country_name = iconv('UTF-8', 'ASCII//TRANSLIT', $country_name);
        }
        $country_name = strtoupper($country_name);
        $this->countryName = $country_name;
    }

    /**
     * Sets the addressee name.
     *
     * @param string $addressee_name
     *   Name of the addressee.
     *
     * @return void
     */
    public function setName(string $addressee_name): void
    {
        $this->name = $addressee_name;
    }

    /**
     * Sets the postal code.
     *
     * @param string|int $postal_code
     *   Postcode or similar postal code as a string or an integer.
     *   An integer is converted to a numeric string.
     *
     * @return void
     */
    public function setPostalCode(string|int $postal_code): void
    {
        $this->postalCode = (string) $postal_code;
    }

    /**
     * Sets the street address.
     *
     * @param StoreCore\OML\StreetAddressInterface|string $street_address
     *   The street address as a single string or an object that implements
     *   the StreetAddressInterface.
     *
     * @return void
     */
    public function setStreetAddress(StreetAddressInterface|string $street_address): void
    {
        if ($street_address instanceof StreetAddressInterface) {
            $street_address = (string) $street_address;
        }
        $this->streetAddress = $street_address;
    }

    /**
     * Creates an array with address lines.
     *
     * @param void
     *
     * @return array
     *   Returns an indexed array with the individual lines of an address.
     */
    public function toArray(): array
    {
        $array = [];

        // Name of the addressee on the 1st line.
        if ($this->getName() !== null) {
            $array[] = $this->getName();
        }

        // Street address on the 2nd line.
        if ($this->getStreetAddress() !== null) {
            $array[] = $this->getStreetAddress();
        }

        // Postal code and locality on the 3rd line.
        if ($this->getPostalCode() !== null && $this->getAddressLocality() !== null) {
            $array[] = $this->getPostalCode() . ' ' . $this->getAddressLocality();
        } elseif ($this->getAddressLocality() !== null) {
            $array[] = $this->getAddressLocality();
        } elseif ($this->getPostalCode() !== null) {
            $array[] = $this->getPostalCode();
        }

        // Uppercase country name on the 4th and last line.
        if ($this->getCountryName() !== null) {
            $array[] = $this->getCountryName();
        }

        return $array;
    }
}
