<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use Psr\Http\Message\UriInterface;

interface_exists(ProviderInterface::class);
interface_exists(UriInterface::class);

/**
 * Abstract parcel delivery provider.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0
 */
abstract class AbstractProvider implements ProviderInterface, \JsonSerializable
{
    /**
     * @var string $name
     *   Name of the provider.  This usually is an `Organization` name,
     *   but Schema.org also allows a `Person` to act as a provider.
     */
    public string $name;

    /**
     * @var null|string $url;
     *   URL of the provider organization.
     */
    public ?string $url = null;

    #[\Override]
    public function getName(): string
    {
        return $this->name;
    }

    #[\Override]
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * Sets the provider name.
     *
     * @param string $name
     *   Name of the provider organization.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Sets the provider URL.
     * 
     * @param Psr\Http\Message\UriInterface|string $name
     *   URL of the provider’s organization as a PSR-7 URI or a string.
     *
     * @return void
     */
    public function setUrl(UriInterface|string $url): void
    {
        if ($url instanceof UriInterface) $url = (string) $url;
        $this->url = $url;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'Organization',
            'name' => $this->getName(),
        ];

        if ($this->getUrl() !== null) {
            $result['url'] = $this->getUrl();
        }

        return $result;
    }
}
