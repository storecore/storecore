<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use Psr\Http\Message\UriInterface;
use StoreCore\CRM\Organization;

/**
 * Provider.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 */
class Provider extends AbstractProvider implements
    ProviderInterface,
    \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a parcel delivery provider.
     *
     * @param null|string $name
     *   Optional name of the provider organization.
     *
     * @param null|Psr\Http\Message\UriInterface|string $url
     *   Optional URL of the provider organization.
     */
    public function __construct(
        null|string $name = null,
        null|UriInterface|string $url = null,
    ) {
        if ($name !== null) $this->setName($name);
        if ($url !== null) $this->setURL($url);
    }

    /**
     * Creates a provider from an organization.
     *
     * @param StoreCore\CRM\Organization|string $organization
     *   Organization as an object or a JSON string.
     *
     * @return StoreCore\OML\Provider
     */
    public static function createFromOrganization(Organization|string $organization): Provider
    {
        if (\is_string($organization)) {
            $organization = json_decode($organization, false, 512, \JSON_INVALID_UTF8_SUBSTITUTE);
            if (!\is_object($organization)) return new Provider();
        }

        $name = null;
        if (property_exists($organization, 'name') && $organization->name !== null) {
            $name = (string) $organization->name;
        }

        $url = null;
        if (property_exists($organization, 'url') && $organization->url !== null) {
            $url = (string) $organization->url;
        }

        return new Provider($name, $url);
    }
}
