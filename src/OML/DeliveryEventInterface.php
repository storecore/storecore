<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

/**
 * Delivery event interface.
 *
 * This interface provides getters for four properties of a Schema.org
 * `DeliveryEvent`: `accessCode`, `availableFrom`, `availableThrough`, and
 * `hasDeliveryMethod`.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 *
 * @see https://schema.org/DeliveryEvent
 *      Schema.org type Thing > Event > DeliveryEvent
 *
 * @see https://developers.google.com/gmail/markup/reference/types/DeliveryEvent
 *      DeliveryEvent - Email Markup - Google Developers
 *
 * @see https://schema.org/orderDelivery
 *      Schema.org property Thing > Property > orderDelivery
 */
interface DeliveryEventInterface
{
    /**
     * Gets the access code.
     *
     * @param void
     *
     * @return null|string
     *   Returns the password, PIN, or other access code needed for delivery
     *   (e.g. from a locker) as a string or `null` if the access code
     *   does not, or not yet, exist.
     *
     * @see https://schema.org/accessCode Schema.org property accessCode
     */
    public function getAccessCode(): ?string;

    /**
     * Gets the date the delivery is available for pickup.
     *
     * @param void
     *
     * @return null|string
     *   Date or date and time as a string, or `null` if this date is not (yet) set.
     */
    public function getAvailableFrom(): ?string;

    /**
     * Gets the date when the delivery will no longer be available for pickup.
     *
     * @param void
     *
     * @return string|null
     *   Returns a date or a date and time after which the item will no longer
     *   be available for pickup, or null if this date is not (yet) set.
     */
    public function getAvailableThrough(): ?string;

    /**
     * Gets the delivery method.
     *
     * @param void
     *
     * @return null|DeliveryMethod
     *   Returns the method used for delivery, or null if this method is not
     *   (yet) known.
     */
    public function getDeliveryMethod(): ?DeliveryMethod;
}
