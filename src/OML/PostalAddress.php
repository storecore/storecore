<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \JsonSerializable;
use \ValueError;
use StoreCore\Registry;
use StoreCore\Geo\{Country, CountryCodes};
use StoreCore\Geo\{State, StateRepository};
use StoreCore\Geo\PostalAddress as AddressModel;
use StoreCore\Types\CountryCode;

use function \array_key_exists;
use function \is_object;
use function \is_string;
use function \isset;
use function \reset;
use function \str_replace;
use function \str_starts_with;
use function \unset;

/**
 * Schema.org postal address.
 *
 * The Schema.org `PostalAddress` data object type is a normative representation
 * of an address.  It is mainly used as a value object for a delivery address in
 * parcel delivery.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 *
 * @see https://schema.org/PostalAddress
 *      Schema.org type `PostalAddress`
 *
 * @see https://developers.google.com/my-business/reference/rest/v4/PostalAddress
 *      PostalAddress - Google My Business API - Google Developers
 * 
 * @see https://support.google.com/business/answer/6397478
 *      Address format for bulk upload - Google My Business Help
 *
 * @see https://developers.google.com/gmail/markup/reference/types/PostalAddress
 *      PostalAddress - Email Markup - Google Developers
 *
 * @see https://amp.dev/documentation/examples/news-publishing/live_blog/
 *      Example: Live Blog - amp.dev
 */
class PostalAddress extends AddressModel implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Geo\Country|StoreCore\Types\CountryCode $addressCountry
     *   The country as a value object or a two-letter ISO 3166-1 alpha-2 country code.
     */
    private null|Country|CountryCode $addressCountry = null;

    /**
     * @var null|StoreCore\Geo\State $addressRegion
     *   The Schema.org `PostalAddress.addressRegion` property set as
     *   a `State` value object instead of a plain string.
     */
    private ?State $addressRegion = null;

    /**
     * @var null|StoreCore\OML\StreetAddress $streetAddress
     *   The Schema.org `PostalAddress.streetAddress` property set as
     *   a `StreetAddress` value object instead of a string.
     */
    private ?StreetAddress $streetAddress = null;

    /**
     * @inheritDoc
     */
    public function __construct(?array $params = null)
    {
        // First process a country, if it is being set.
        if ($params !== null) {
            if (isset($params['addressCountry'])) {
                $this->setCountry($params['addressCountry']);
                unset($params['addressCountry']);
            }
        }

        parent::__construct($params);
    }

    /**
     * @inheritDoc
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'addressCountry' => $this->addressCountry,
            'addressRegion'  => $this->getAddressRegion(),
            'streetAddress'  => $this->getStreetAddress(),
            default => parent::__get($name)
        };
    }

    /**
     * @inheritDoc
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'addressRegion' => $this->setAddressRegion($value),
            'streetAddress' => $this->setStreetAddress($value),
            default => parent::__set($name, $value)
        };
    }

    /**
     * Gets the address region.
     *
     * @param void
     *
     * @return string|null
     *   In Schema.org the `addressRegion` property of a `PostalAddress` object
     *   is the `State` or a similar country subdivision of an address,
     *   for example, `California` or another appropriate first-level
     *   administrative division.  This value is text, not an object, so this
     *   getter method returns a string (or `null` if the property is not set).
     *
     * @see https://schema.org/PostalAddress
     *      Schema.org type PostalAddress
     *
     * @see https://en.wikipedia.org/wiki/List_of_administrative_divisions_by_country
     *      List of administrative divisions by country
     *
     * @see https://en.wikipedia.org/wiki/ISO_3166-2
     *      ISO 3166-2: Codes for the representation of names of countries and their subdivisions – Part 2: Country subdivision code
     */
    public function getAddressRegion(): ?string
    {
        if ($this->addressRegion !== null) {
            // Use ISO 3166-2 abbreviation only for US (840)
            // and region name for other countries.
            if (str_starts_with($this->addressRegion->identifier, 'US-')) {
                return str_replace('US-', '', $this->addressRegion->identifier);
            } elseif ($this->addressRegion->alternateName !== null) {
                return $this->addressRegion->alternateName;
            } else {
                return $this->addressRegion->name;
            }
        }

        // We need a country plus country subdivision for a full ISO 3166-2 code,
        if (!$this->has('country_id') || !$this->has('country_subdivision')) {
            return null;
        }

        $iso_code = $this->getCountry()->identifier . '-' . $this->get('country_subdivision');
        $registry = Registry::getInstance();
        $repository = new StateRepository($registry);
        if ($repository->has($iso_code)) {
            unset($repository);
            $this->setAddressRegion($iso_code);
            return $this->getAddressRegion();
        }

        return null;
    }

    /**
     * Gets the street address.
     *
     * @param void
     *
     * @return string
     *   Returns the `streetAddress` property of a `PostalAddress` object as
     *   a string.
     *
     * @uses StoreCore\OML\StreetAddress::createFromPostalAddress()
     *   If the street address was not yet set, it MAY be derived from other
     *   properties of a `StoreCore\Geo\Address` object such as a street
     *   name and a house number.
     */
    public function getStreetAddress(): string|null
    {
        if ($this->streetAddress === null) {
            $this->streetAddress = StreetAddress::createFromPostalAddress($this);
        }

        $street_address = (string) $this->streetAddress;
        return empty($street_address) ? null : $street_address;
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        if ($id === 'addressRegion') {
            if (
                $this->addressRegion !== null
                || ($this->has('country_id') && $this->has('country_subdivision'))
            ) {
                return true;
            } else {
                return false;
            }
        }

        if ($id === 'streetAddress') {
            return $this->streetAddress !== null ? true : false;
        }

        return parent::has($id);
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     * @see https://schema.org/PostalAddress
     */
    public function jsonSerialize(): array
    {
        $address = [
            '@context'        => 'https://schema.org',
            '@type'           => 'PostalAddress',
            'name'            => $this->has('name') ? $this->get('name') : null, 
            'streetAddress'   => $this->getStreetAddress(),
            'addressLocality' => $this->has('addressLocality') ? $this->get('addressLocality') : null, 
            'addressRegion'   => $this->getAddressRegion(),
            'addressCountry'  => $this->addressCountry,
            'postalCode'      => $this->has('postalCode') ? $this->get('postalCode') : null, 
        ];

        $address = array_filter($address);
        return $address;
    }

    /**
     * Sets the address region.
     *
     * @param StoreCore\Geo\State|string $country_subdivision
     *   Country subdivision as a `State` object or a country subdivision.
     * 
     * @return void
     */
    public function setAddressRegion(State|string $country_subdivision): void
    {
        if ($country_subdivision instanceof State) {
            $this->addressRegion = $country_subdivision;
            return;
        }

        $registry = Registry::getInstance();
        $repository = new StateRepository($registry);
        if ($repository->has($country_subdivision)) {
            $regions = $repository->get($country_subdivision);
            if (array_key_exists('en', $regions)) {
                $this->addressRegion = $regions['en'];
            } elseif (array_key_exists('fr', $regions)) {
                $this->addressRegion = $regions['fr'];
            } else {
                $this->addressRegion = reset($regions);
            }
            return;
        }

        // Add country code when only the subdivision code is provided.
        if (strlen($country_subdivision) < 4 && $this->has('country_id')) {
            $iso_code = $this->getCountry()->identifier . '-' . $country_subdivision;
            if ($repository->has($iso_code)) {
                $this->setAddressRegion($iso_code);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function setCountry(int|string|Country|CountryCode $country): void
    {
        parent::setCountry($country);

        if (!is_object($country)) $country = CountryCodes::createCountryCode($country);
        $this->addressCountry = $country;
    }

    /**
     * Sets the street address
     *
     * @param StoreCore\OML\StreetAddress|string $street_address
     *   Street address as a `StreetAddress` object or a string.
     * 
     * @return void
     * 
     * @throws \ValueError
     *   Throws a value error exception when a supplied string could not
     *   be processed into a `StreetAddress` value object.
     *
     * @uses \StoreCore\OML\StreetAddress::createFromString()
     */
    public function setStreetAddress(StreetAddress|string $street_address): void
    {
        if (is_string($street_address) ) {
            $street_address = StreetAddress::createFromString($street_address);
            if ($street_address === false) throw new ValueError('Invalid street address', time());
        }

        $this->streetAddress = $street_address;
    }
}
