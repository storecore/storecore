<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\OML;

use \JsonSerializable;
use \Stringable;

use function \abs;
use function \is_int;
use function \preg_replace;
use function \strtoupper;
use function \trim;

/**
 * Postal code.
 * 
 * Value object class for postal codes, post codes, postcodes, and similar
 * address identifiers like ZIP codes, area codes, et cetera.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/postalCode Schema.org property `postalCode`
 * @version 1.0.0
 */
readonly class PostalCode implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $value
     *   Postal code as an uppercase string.
     */
    private string $value;

    /**
     * Creates a postal code.
     *
     * @param string|int $code
     *   Postal code as a case-insensitive string or an integer.
     */
    public function __construct(string|int $code)
    {
        if (is_int($code)) {
            $code = abs($code);
            $code = (string) $code;
        }

        $code = trim($code);
        $code = preg_replace('/\s+/', ' ', $code);
        $code = strtoupper($code);
        $this->value = $code;
    }

    /**
     * Returns the postal code as a string for JSON-LD.
     *
     * @param void
     * @return string
     */
    final public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * Converts the postal code to a string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
