<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\Container\ContainerInterface;

use function \empty;
use function \isset;

/**
 * Abstract container.
 * 
 * Basic implementation of the PSR-11 Container interface.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-11/ PSR-11: Container interface
 * @see     https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
 * @version 1.0.0
 */
abstract class AbstractContainer implements ContainerInterface
{
    /**
     * @var array $data
     *   Data stored as key-value pairs in the container.
     */
    protected array $data = [];

    #[\Override]
    public function get(string $id): mixed
    {
        if (empty($id)) throw new ContainerException();
        if (!$this->has($id)) throw new NotFoundException();
        return $this->data[$id];
    }

    #[\Override]
    public function has(string $id): bool
    {
        return isset($this->data[$id]);
    }
}
