<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \JsonSerializable;
use \Stringable;
use \ValueError;

use StoreCore\Currency;
use StoreCore\CMS\AbstractCreativeWork as CreativeWork;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;
use StoreCore\Types\BusinessFunction;
use StoreCore\Types\Date;
use StoreCore\Types\Event;
use StoreCore\Types\Intangible;
use StoreCore\Types\MenuItem;
use StoreCore\Types\Trip;

use function \ctype_digit;
use function \is_numeric;
use function \is_string;

/**
 * Offer.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/Offer Schema.org type `Offer`
 * @version 0.4.0
 */
class Offer extends Intangible implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.0';

    /**
     * @var StoreCore\PIM\ItemAvailability $availability
     *   The availability of this item, for example In stock, Out of stock,
     *   Pre-order, etc.  Defaults to `https://schema.org/InStock`.
     */
    private ItemAvailability $availability;

    /**
     * @var StoreCore\Types\BusinessFunction $businessFunction
     *   The business function (e.g. sell, lease, repair, dispose) of
     *   the `Offer` or component of a bundle (`TypeAndQuantityNode`).
     *   The default is `http://purl.org/goodrelations/v1#Sell`.
     */
    public BusinessFunction $businessFunction = BusinessFunction::Sell;

    /**
     * @var StoreCore\PIM\OfferItemCondition $itemCondition
     *   A predefined value from `OfferItemCondition` specifying the condition
     *   of the product or service, or the products or services included in the
     *   offer.  Also used for product return policies to specify the condition
     *   of products accepted for returns.
     */
    private OfferItemCondition $itemCondition;

    /**
     * @var mixed $itemOffered
     *   An item being offered (or demanded).  The transactional nature of the
     *   `Offer` or `Demand` is documented using `businessFunction`, e.g. sell,
     *   lease etc.  While several common expected types are listed explicitly
     *   in this definition, others can be used.  Using a second type, such as
     *   `Product` or a subtype of `Product`, can clarify the nature of the
     *   `Offer`.  Inverse property: `offers`.
     */
    public AggregateOffer|CreativeWork|Event|MenuItem|Product|Service|Trip|null $itemOffered = null;

    
    /**
     * @var null|int|float|string $price
     *   The offer price of a product, or of a price component when attached
     *   to `PriceSpecification` and its subtypes.
     */
    private int|float|string|null $price = null;

    /**
     * @var string $priceCurrency
     *   The currency of the price, or a price component when attached to
     *   `PriceSpecification` and its subtypes.  Use standard ISO 4217 currency
     *   format e.g. "USD".  Defaults to 'EUR' for Euro.
     */
    public string $priceCurrency = 'EUR';

    /**
     * @var null|StoreCore\Types\Date $priceValidUntil
     *   The date after which the price is no longer available.
     */
    public ?Date $priceValidUntil = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person $seller
     *   An entity which offers (sells, leases, lends, or loans) the services
     *   or goods.  A seller may also be a provider.  Supersedes `vendor` and
     *   `merchant`.
     */
    public null|Organization|Person $seller = null;

    /**
     * Creates a product offer.
     *
     * @param null|int|float|string $price
     * @param string $currency
     */
    public function __construct(int|float|string|null $price = null, string $currency = 'EUR')
    {
        if ($price !== null) {
            $this->setPrice($price);
        }
        $this->setPriceCurrency($currency);

        // Items on offer are in stock and new by default.
        $this->setAvailability(ItemAvailability::InStock);
        $this->setItemCondition(OfferItemCondition::NewCondition);
    }

    /**
     * Generic getter.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'availability' => $this->availability,
            'itemCondition' => $this->itemCondition,
            'merchant', 'vendor' => $this->seller,
            'price' => $this->price,
            default => parent::__get($name),
        };
    }

    /**
     * Generic setter.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'availability' => $this->setAvailability($value),
            'itemCondition' => $this->setItemCondition($value),
            'merchant', 'vendor' => $this->seller = $value,
            'price' => $this->setPrice($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['availability'] = $this->availability->value;
        $result['itemCondition'] = $this->itemCondition->value;
        if ($this->price !== null) $result['price'] = $this->price;

        ksort($result);
        return $result;
    }

    /**
     * Sets the offer availability.
     *
     * @param StoreCore\PIM\ItemAvailability|string $availability
     * @return void
     * @throws \ValueError
     */
    public function setAvailability(ItemAvailability|string $availability): void
    {
        if (is_string($availability)) {
            $availability = ItemAvailability::from($availability);
        }
        $this->availability = $availability;
    }

    /**
     * Sets the condition of the offered item.
     *
     * @param StoreCore\PIM\OfferItemCondition|string $item_condition
     *   A predefined value from `OfferItemCondition` for the condition of the
     *   product or service, or the products or services included in the offer.
     *   A textual description of the condition is NOT supported.
     *
     * @return void
     * 
     * @see https://schema.org/itemCondition 
     *   Schema.org property `itemCondition`.
     *
     * @throws \ValueError
     */
    public function setItemCondition(OfferItemCondition|string $item_condition): void
    {
        if (is_string($item_condition)) {
            $item_condition = OfferItemCondition::from($item_condition);
        }
        $this->itemCondition = $item_condition;
    }

    /**
     * Sets the price.
     *
     * @param float|int|string $price
     *   The offer price of a product as a float, integer, or numeric string.
     *
     * @return void
     */
    public function setPrice(float|int|string $price): void
    {
        if (is_string($price) && is_numeric($price)) {
            $price = ctype_digit($price) ? (int) $price : (float) $price;
        }
        $this->price = $price;
    }

    /**
     * Sets the price currency.
     *
     * @param StoreCore\Currency|string $currency
     *   Currency as a core `Currency` value object or a string in
     *   three-letter ISO 4217 format.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the currency is a string that
     *   could not be converted to an uppercase abbreviation of three letters.
     *   This method does not validate if the three-letter abbreviation is
     *   a valid ISO 4217 currency code, so it MAY be used for strings like
     *   'BTC' or 'XBT' for Bitcoin.
     */
    public function setPriceCurrency(Currency|string $currency): void
    {
        if ($currency instanceof Currency) {
            $currency = $currency->code;
        } else {
            $currency = trim($currency);
            $currency = strtoupper($currency);
            if (strlen($currency) !== 3) {
                throw new ValueError();
            }
        }
        $this->priceCurrency = $currency;
    }
}
