<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \JsonSerializable;
use \ValueError;
use Psr\Http\Message\UriInterface;
use StoreCore\Types\CommonCode;
use StoreCore\Types\QuantitativeValue;

/**
 * Unit price specification.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/UnitPriceSpecification Schema.org type UnitPriceSpecification
 * @version 0.3.0
 * 
 * @see https://developers.google.com/gmail/markup/reference/types/UnitPriceSpecification
 *      Email markup for Google Gmail currently only supports the three
 *      properties `billingIncrement`, `priceType`, and `unitCode`.
 * 
 * @see https://github.com/schemaorg/schemaorg/issues/2712
 *      Proposal to add support for explicit identification of price categories such as sale price
 */
class UnitPriceSpecification extends PriceSpecification implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var string|float|null $billingIncrement
     *   This property specifies the minimal quantity and rounding increment
     *   that will be the basis for the billing.  The unit of measurement is
     *   specified by the `unitCode` property.
     */
    public int|float|null $billingIncrement = null;

    /**
     * @var null|StoreCore\Types\QuantitativeValue $referenceQuantity
     *   The reference quantity for which a certain price applies.
     */
    public ?QuantitativeValue $referenceQuantity = null;

    /**
     * @var null|string $unitCode
     *   The unit of measurement given using the UN/CEFACT Common Code
     *   (2 or 3 characters).
     */
    public null|string $unitCode = null;

    /**
     * @var null|string $unitText
     *   A string or text indicating the unit of measurement.
     */
    public null|string $unitText = null;

    /**
     * @var null|StoreCore\PIM\PriceTypeEnumeration|string $priceType
     *   Defines the type of a price specified for an offered product, for
     *   example a list price, a (temporary) sale price or a manufacturer
     *   suggested retail price.  If multiple prices are specified for an offer
     *   the `priceType` property can be used to identify the type of each
     *   such specified price.  The value of `priceType` can be specified as
     *   a value from enumeration `PriceTypeEnumeration` or as a free form text
     *   string for price types that are not already predefined in
     *   `PriceTypeEnumeration`.
     */
    public null|PriceTypeEnumeration|string $priceType = null;

    /**
     * Sets the unit code and unit text.
     * 
     * @param StoreCore\Types\CommonCode $common_code
     *   UN/CEFACT Common Code for the `unitCode` with an optional `unitText`.
     *
     * @return void
     */
    public function setCommonCode(CommonCode $code): void
    {
        $this->unitCode = $code->value;
        $this->unitText = $code->unitOfMeasurement;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@type'] = 'UnitPriceSpecification';

        if ($this->referenceQuantity !== null) {
            $result['referenceQuantity'] = $this->referenceQuantity->jsonSerialize();
            unset($result['referenceQuantity']['@context']);
        }

        return $result;
    }
}
