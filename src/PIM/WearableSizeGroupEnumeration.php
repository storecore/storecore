<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \UnhandledMatchError;
use \ValueError;

/**
 * Wearable size group enumeration.
 *
 * Enumerates common size groups (also known as “size types”) for wearable products.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/WearableSizeGroupEnumeration Schema.org enumeration type `WearableSizeGroupEnumeration`
 * @version 23.0.0-beta
 */
enum WearableSizeGroupEnumeration: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '23.0.0-beta';

    case Big        = 'https://schema.org/WearableSizeGroupBig';
    case Boys       = 'https://schema.org/WearableSizeGroupBoys';
    case ExtraShort = 'https://schema.org/WearableSizeGroupExtraShort';
    case ExtraTall  = 'https://schema.org/WearableSizeGroupExtraTall';
    case Girls      = 'https://schema.org/WearableSizeGroupGirls';
    case Husky      = 'https://schema.org/WearableSizeGroupHusky';
    case Infants    = 'https://schema.org/WearableSizeGroupInfants';
    case Juniors    = 'https://schema.org/WearableSizeGroupJuniors';
    case Maternity  = 'https://schema.org/WearableSizeGroupMaternity';
    case Mens       = 'https://schema.org/WearableSizeGroupMens';
    case Misses     = 'https://schema.org/WearableSizeGroupMisses';
    case Petite     = 'https://schema.org/WearableSizeGroupPetite';
    case Plus       = 'https://schema.org/WearableSizeGroupPlus';
    case Regular    = 'https://schema.org/WearableSizeGroupRegular';
    case Short      = 'https://schema.org/WearableSizeGroupShort';
    case Tall       = 'https://schema.org/WearableSizeGroupTall';
    case Womens     = 'https://schema.org/WearableSizeGroupWomens';

    /**
     * Maps an unsigned integer to an enumeration member.
     *
     * @internal
     * @param int $id
     * @return static
     */
    final public static function fromInteger(int $id): static
    {
        try {
            return match($id) {
                 0 => static::Regular,
                 1 => static::Big,
                 2 => static::Boys,
                 3 => static::ExtraShort,
                 4 => static::ExtraTall,
                 5 => static::Girls,
                 6 => static::Husky,
                 7 => static::Infants,
                 8 => static::Juniors,
                 9 => static::Maternity,
                10 => static::Mens,
                11 => static::Misses,
                12 => static::Petite,
                13 => static::Plus,
                14 => static::Short,
                15 => static::Tall,
                16 => static::Womens,
            };
        } catch (UnhandledMatchError $e) {
            throw new ValueError($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Maps an enumeration member to a unique integer.
     *
     * @internal
     * @param StoreCore\PIM\WearableSizeGroupEnumeration $case
     * @return int
     */
    final public static function toInteger(WearableSizeGroupEnumeration $case): int
    {
        return match($case->name) {
            'Regular'     =>  0,
            'Big'         =>  1,
            'Boys'        =>  2,
            'ExtraShort'  =>  3,
            'ExtraTall'   =>  4,
            'Girls'       =>  5,
            'Husky'       =>  6,
            'Infants'     =>  7,
            'Juniors'     =>  8,
            'Maternity'   =>  9,
            'Mens'        => 10,
            'Misses'      => 11,
            'Petite'      => 12,
            'Plus'        => 13,
            'Short'       => 14,
            'Tall'        => 15,
            'Womens'      => 16,
        };
    }
}
