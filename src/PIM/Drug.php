<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Drug.
 *
 * A `Drug` is a chemical or biologic substance, used as a medical therapy, that
 * has a physiological effect on an organism.  Here the term `Drug` is used
 * interchangeably with the term _medicine_ although clinical knowledge makes
 * a clear difference between them.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Drug Schema.org type `Drug`
 * @version 0.1.0
 */
class Drug extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
