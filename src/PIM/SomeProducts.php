<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Some products.
 *
 * `SomeProducts` is a placeholder for multiple similar products of the same kind.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/SomeProducts Schema.org type `SomeProducts`
 * @version 0.1.0
 */
class SomeProducts extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
