<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Types\Date;

use \JsonSerializable;

use function \rtrim;
use function \unset;

/**
 * Abstract product.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
abstract class AbstractProduct extends AbstractProductOrService implements 
    IdentityInterface,
    JsonSerializable
{
    use IdentityTrait;

    /**
     * @var StoreCore\PIM\OfferItemCondition $itemCondition
     *   A predefined value from `OfferItemCondition` specifying the condition
     *   of the `Product` or the products included in an `Offer`.  Also used
     *   for product return policies to specify the condition of products
     *   accepted for returns.  Defaults to `https://schema.org/NewCondition`.
     */
    public OfferItemCondition $itemCondition = OfferItemCondition::NewCondition;

    /**
     * @var null|StoreCore\PIM\EnergyConsumptionDetails $hasEnergyConsumptionDetails
     *   Defines the energy efficiency category (also known as “class” or
     *   “rating”) for a `Product` according to an international energy
     *   efficiency standard.
     */
    public ?EnergyConsumptionDetails $hasEnergyConsumptionDetails = null;

    /**
     * @var null|StoreCore\Types\Date $releaseDate
     *   The release date of a `Product` or `ProductModel`.  This MAY be
     *   used to distinguish the exact variant of a product.
     */
    public ?Date $releaseDate = null;

    /**
     * Specifies data which should be serialized to Schema.org JSON-LD.
     *
     * @param void
     *
     * @return array
     *   Returns data which can be serialized by `json_encode()`.
     *   Properties are listed in alphabetical order.
     */
    public function jsonSerialize(): array
    {
        $result = [];
        if (!empty($this->id)) {
            $result['@id'] = (string) $this->id;
        } elseif ($this->hasIdentifier()) {
            $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/products/' . $this->getIdentifier();
        }
        $result['@context'] = 'https://schema.org';
        $result['@type'] = 'Product';

        if ($this->additionalType !== null) $result['additionalType'] = (string) $this->additionalType;
        if ($this->aggregateRating !== null) $result['aggregateRating'] = $this->aggregateRating->jsonSerialize();
        if ($this->brand !== null) $result['brand'] = $this->brand;
        if ($this->description !== null) $result['description'] = $this->description;

        if ($this->gtin13 !== null) {
            $result['gtin13'] = (string) $this->gtin13;
        } elseif ($this->gtin !== null) {
            $result['gtin'] = (string) $this->gtin;
        }

        if ($this->hasEnergyConsumptionDetails !== null) {
            $result['hasEnergyConsumptionDetails'] = $this->hasEnergyConsumptionDetails->jsonSerialize();
            unset($result['hasEnergyConsumptionDetails']['@context']);
        }

        if ($this->hasIdentifier()) $result['identifier'] = (string) $this->getIdentifier();
        $result['itemCondition'] = $this->itemCondition->value;
        if (!empty($this->keywords)) $result['keywords'] = $this->keywords;
        if ($this->mpn !== null) $result['mpn'] = $this->mpn;
        if ($this->name !== null) $result['name'] = $this->name;
        if ($this->releaseDate !== null) $result['releaseDate'] = $this->releaseDate->format('Y-m-d');
        if ($this->review !== null) $result['review'] = $this->review->jsonSerialize();
        if ($this->sku !== null) $result['sku'] = $this->sku;

        if (isset($result['aggregateRating']['@context'])) unset($result['aggregateRating']['@context']);
        if (isset($result['review']['@context'])) unset($result['review']['@context']);

        return $result;
    }

    /**
     * Sets the product’s release date.
     *
     * @param \DateTimeInterface|string $date
     *   The OPTIONAL date the product is officially “launched” or introduced
     *   to the target audience.  If omitted the current date is set.
     *
     * @return void
     */
    public function setReleaseDate(\DateTimeInterface|string $date = 'today'): void
    {
        if (\is_string($date)) {
            $date = new Date($date, new \DateTimeZone('UTC'));
        } elseif (!$date instanceof Date) {
            $date = Date::createFromInterface($date);
        }
        $this->releaseDate = $date;
    }
}
