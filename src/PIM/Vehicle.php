<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Vehicle.
 *
 * A `Vehicle` is a device that is designed or used to transport people or
 * cargo over land, water, air, or through space.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Vehicle Schema.org type `Vehicle`
 * @version 0.1.0
 */
class Vehicle extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
