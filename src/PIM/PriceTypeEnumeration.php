<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * Price type enumeration.
 *
 * Enumerates different price types, for example list price, invoice price, and
 * sale price.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/PriceTypeEnumeration Schema.org enumeration type `PriceTypeEnumeration`
 * @version 0.2.0
 */
enum PriceTypeEnumeration: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '0.2.0';

    case InvoicePrice           = 'https://schema.org/InvoicePrice';
    case ListPrice              = 'https://schema.org/ListPrice';
    case MinimumAdvertisedPrice = 'https://schema.org/MinimumAdvertisedPrice';
    case MSRP                   = 'https://schema.org/MSRP';
    case SalePrice              = 'https://schema.org/SalePrice';
    case SRP                    = 'https://schema.org/SRP';
}
