<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \Countable;
use \JsonSerializable;
use \SplObjectStorage;
use \TypeError;

use StoreCore\IdentityInterface;

use function \is_array;
use function \iterator_to_array;

/**
 * Product collection.
 *
 * A `ProductCollection` is a set of products (either `ProductGroup`s
 * or specific variants) that are listed together e.g. in an `Offer`.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/ProductCollection Schema.org type `ProductCollection`
 * @version 0.1.0
 */
class ProductCollection extends Product implements
    Countable,
    IdentityInterface,
    JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var int $collectionSize
     *   The number of items in the `ProductCollection`.
     */
    protected int $collectionSize;

    /**
     * @var \SplObjectStorage $includesObject
     *   This links to a node or nodes indicating the exact quantity of
     *   the products included in an `Offer` or `ProductCollection`.
     */
    protected SplObjectStorage $includesObject;

    /**
     * Accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'collectionSize' => $this->count(),
            'includesObject' => $this->getIncludesObject(),
            default => parent::__get($name),
        };
    }

    /**
     * Mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'includesObject' => $this->setIncludesObject($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Adds an object to the collection.
     *
     * @param StoreCore\PIM\TypeAndQuantityNode $object
     * @param mixed $info
     * @uses \SplObjectStorage::attach()
     */
    public function attach(TypeAndQuantityNode $object, mixed $info = null): void
    {
        if (!isset($this->includesObject)) {
            $this->includesObject = new SplObjectStorage();
        }

        if (null === $info && $object->typeOfGood->hasIdentifier()) {
            $info = (string) $object->typeOfGood->getIdentifier();
        }
        $this->includesObject->attach($object, $info);
    }

    /**
     * Counts the number of elements in the collection.
     *
     * @param void
     * @return int
     * @see https://schema.org/collectionSize
     */
    public function count(): int
    {
        $this->collectionSize = 0;
        if (isset($this->includesObject)) {
            foreach ($this->includesObject as $node) {
                $this->collectionSize += $node->count();
            }
        }

        return $this->collectionSize;
    }

    /**
     * Gets the included object or objects.
     *
     * @param void
     * @return StoreCore\PIM\TypeAndQuantityNode|array
     */
    private function getIncludesObject(): TypeAndQuantityNode|array
    {
        if (isset($this->includesObject)) {
            $result = iterator_to_array($this->includesObject, false);
            if (count($result) === 1) $result = $result[0];
            return $result;
        } else {
            return array();
        }
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@type'] = 'ProductCollection';
        $result['collectionSize'] = $this->count();

        if (isset($this->includesObject)) {
            $result['includesObject'] = $this->getIncludesObject();
        }

        return $result;
    }

    /**
     * Sets the type and quantity node or nodes in the product collection.
     *
     * @param StoreCore\PIM\TypeAndQuantityNode|array $node
     * @return void
     */
    private function setIncludesObject(TypeAndQuantityNode|array $node): void
    {
        $this->includesObject = new SplObjectStorage();

        if (!is_array($node)) {
            $this->attach($node);
        } else {
            foreach ($node as $value) {
                if (!($value instanceof TypeAndQuantityNode)) {
                    throw new TypeError();
                }
                $this->attach($node);
            }
        }
    }
}
