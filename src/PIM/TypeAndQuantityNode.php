<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \Countable;
use \JsonSerializable;

use function \strlen;
use function \strtoupper;
use function \trim;

/**
 * Type and quantity node.
 *
 * A `TypeAndQuantityNode` is a structured value indicating the quantity,
 * unit of measurement, and business function of goods included in a bundle
 * offer.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/TypeAndQuantityNode Schema.org type `TypeAndQuantityNode`
 * @version 0.1.0
 */
readonly class TypeAndQuantityNode implements Countable, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var int|float $amountOfThisGood
     *   The quantity of the goods included in the offer.
     */
    public int|float $amountOfThisGood;

    /**
     * @var StoreCore\PIM\Product|StoreCore\PIM\Service
     *   The product or service that this structured value is referring to.
     */
    public Product|Service $typeOfGood;

    /**
     * @var string $unitCode
     *   The unit of measurement given using the UN/CEFACT Common Code.
     *   Defauls to `(string) C62` for one unit.
     */
    public string $unitCode;

    /**
     * Creates a type and quantity node for a product or service.
     *
     * @param StoreCore\PIM\Product|StoreCore\PIM\Service $type
     * @param int|float $quantity
     * @param string $unit_code
     */
    public function __construct(
        Product|Service $type,
        int|float $quantity = 1,
        string $unit_code = 'C62',
    ) {
        $this->typeOfGood = $type;
        $this->amountOfThisGood = $quantity;
        $this->setUnitCode($unit_code);
    }

    /**
     * Counts the elements in the node.
     *
     * @param void
     * @return int
     * @see https://www.php.net/Countable PHP `Countable` interface
     */
    public function count(): int
    {
        if ($this->unitCode === 'C62') {
            return $this->amountOfThisGood;
        } else {
            return 1;
        }
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            '@context' => 'https://schema.org',
            '@type' => 'TypeAndQuantityNode',
            'amountOfThisGood' => $this->amountOfThisGood,
            'unitCode' => $this->unitCode,
            'typeOfGood' => $this->typeOfGood,
        ];
    }

    /**
     * Sets the unit code.
     *
     * @param string $unit_code
     *   Case-insensitive UN/CEFACT Common Code.
     *
     * @return void
     */
    private function setUnitCode(string $unit_code): void
    {
        $unit_code = trim($unit_code);
        if (strlen($unit_code) < 2 || strlen($unit_code) > 3 ) {
            throw new \ValueError('Invalid UN/CEFACT Common Code');
        }

        $unit_code = strtoupper($unit_code);
        $this->unitCode = $unit_code;
    }
}
