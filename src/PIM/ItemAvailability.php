<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * Item availability.
 * 
 * A list of possible `Product` availability options.  The most
 * common Schema.org values are `InStock` and `OutOfStock`.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/ItemAvailability Schema.org enumeration type `ItemAvailability`
 * @see     https://developers.google.com/search/docs/data-types/product
 * @see     https://support.google.com/merchants/answer/7353427?hl=en
 * @version 23.0.0
 */
enum ItemAvailability: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org version 23.0
     *   from 17 October 2023.
     */
    public const string VERSION = '23.0.0';

    case BackOrder           = 'https://schema.org/BackOrder';
    case Discontinued        = 'https://schema.org/Discontinued';
    case InStock             = 'https://schema.org/InStock';
    case InStoreOnly         = 'https://schema.org/InStoreOnly';
    case LimitedAvailability = 'https://schema.org/LimitedAvailability';
    case OnlineOnly          = 'https://schema.org/OnlineOnly';
    case OutOfStock          = 'https://schema.org/OutOfStock';
    case PreOrder            = 'https://schema.org/PreOrder';
    case PreSale             = 'https://schema.org/PreSale';
    case SoldOut             = 'https://schema.org/SoldOut';

    /**
     * Maps an unsigned tiny integer to an enumeration member.
     *
     * @param int $key
     * @return static
     * @throws \ValueError
     */
    final public static function fromInteger(int $key): static
    {
        if ($key < 1 || $key > 10) {
            throw new \ValueError();
        }

        return match($key) {
             1 => static::BackOrder,
             2 => static::Discontinued,
             3 => static::InStock,
             4 => static::InStoreOnly,
             5 => static::LimitedAvailability,
             6 => static::OnlineOnly,
             7 => static::OutOfStock,
             8 => static::PreOrder,
             9 => static::PreSale,
            10 => static::SoldOut,
        };
    }


    /**
     * Maps an enumeration member to a unique integer.
     *
     * @internal
     * @param StoreCore\PIM\ItemAvailability $availability
     * @return int
     */
    final public static function toInteger(ItemAvailability $availability): int
    {
        return match($availability->name) {
            'BackOrder'           =>  1,
            'Discontinued'        =>  2,
            'InStock'             =>  3,
            'InStoreOnly'         =>  4,
            'LimitedAvailability' =>  5,
            'OnlineOnly'          =>  6,
            'OutOfStock'          =>  7,
            'PreOrder'            =>  8,
            'PreSale'             =>  9,
            'SoldOut'             => 10,
        };
    }
}
