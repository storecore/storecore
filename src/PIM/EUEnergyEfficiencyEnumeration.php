<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * EU energy efficiency.
 *
 * Enumerates the EU energy efficiency classes A-G as well as A+, A++, and A+++
 * as defined in EU directive 2017/1369.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/EUEnergyEfficiencyEnumeration Schema.org enumeration type `EUEnergyEfficiencyEnumeration`
 * @version 23.0.0
 */
enum EUEnergyEfficiencyEnumeration: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org version 23.0
     *   from October 17, 2023.  Note that this term is in the “new” area of
     *   Schema.org: implementation feedback and adoption from applications
     *   and websites can help improve definitions.
     */
    public const string VERSION = '23.0.0';

    case EUEnergyEfficiencyCategoryA3Plus = 'https://schema.org/EUEnergyEfficiencyCategoryA3Plus';
    case EUEnergyEfficiencyCategoryA2Plus = 'https://schema.org/EUEnergyEfficiencyCategoryA2Plus';
    case EUEnergyEfficiencyCategoryA1Plus = 'https://schema.org/EUEnergyEfficiencyCategoryA1Plus';
    case EUEnergyEfficiencyCategoryA      = 'https://schema.org/EUEnergyEfficiencyCategoryA';
    case EUEnergyEfficiencyCategoryB      = 'https://schema.org/EUEnergyEfficiencyCategoryB';
    case EUEnergyEfficiencyCategoryC      = 'https://schema.org/EUEnergyEfficiencyCategoryC';
    case EUEnergyEfficiencyCategoryD      = 'https://schema.org/EUEnergyEfficiencyCategoryD';
    case EUEnergyEfficiencyCategoryE      = 'https://schema.org/EUEnergyEfficiencyCategoryE';
    case EUEnergyEfficiencyCategoryF      = 'https://schema.org/EUEnergyEfficiencyCategoryF';
    case EUEnergyEfficiencyCategoryG      = 'https://schema.org/EUEnergyEfficiencyCategoryG';
}
