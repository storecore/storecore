<?php

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\Types\PaymentMethod;

/**
 * Payment charge specification.
 * 
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://schema.org/PaymentChargeSpecification Schema.org type `PaymentChargeSpecification`
 * @version   0.2.0
 */
class PaymentChargeSpecification extends PriceSpecification implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\Types\PaymentMethod|null $appliesToPaymentMethod
     *   The payment method(s) to which the payment charge specification applies.
     */
    public ?PaymentMethod $appliesToPaymentMethod = null;
}
