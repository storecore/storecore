<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \Exception;
use \Stringable;
use \ValueError;

use StoreCore\Types\TypeInterface;
use StoreCore\Types\ValidateInterface;

use function \is_float;
use function \is_int;
use function \str_replace;
use function \str_starts_with;
use function \strlen;
use function \substr;

/**
 * International Standard Book Number (ISBN).
 *
 * @package StoreCore\PIM
 * @version 0.2.0
 */
class InternationalStandardBookNumber extends InternationalArticleNumber implements
    Stringable,
    TypeInterface,
    ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * Creates an ISBN-13.
     *
     * @param mixed $initial_value
     * @param bool $strict
     */
    public function __construct(mixed $initial_value, bool $strict = true)
    {
        if (!$strict && is_float($initial_value)) {
            $initial_value = (string) $initial_value;
            $initial_value = str_replace('.', '', $initial_value);
        }

        if (is_int($initial_value)) {
            $initial_value = (string) $initial_value;
        }

        // Add the Bookland prefix and remove the check digit to recalculate it.
        if (strlen($initial_value) === 10) {
            $initial_value = '978' . substr($initial_value, 0, -1);
        }

        parent::__construct($initial_value, $strict);

        if (!str_starts_with($this->value, '978')) {
            throw new ValueError('ISBN does not start with "978"', time());
        }
    }

    /**
     * Validates an ISBN.
     *
     * @param mixed $variable
     * @return bool
     */
    public static function validate(mixed $variable): bool
    {
        try {
            $isbn = new InternationalArticleNumber($variable, true);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
