<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Product group.
 *
 * A `ProductGroup` represents a group of `Product`s that vary only in certain
 * well-described ways, such as by `size`, `color`, `material` etc.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/ProductGroup Schema.org type `ProductGroup`
 * @version 0.1.0
 */
class ProductGroup extends Product implements 
    IdentityInterface,
    \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
