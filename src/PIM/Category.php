<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Http\Message\UriInterface;

/**
 * Category.
 *
 * @package StoreCore\PIM
 * @version 0.1.0
 *
 * @see https://schema.org/CategoryCode
 *      Schema.org type `CategoryCode`
 *
 * @see http://www.productontology.org/
 *      The Product Types Ontology
 *
 * @see http://webdatacommons.org/categorization/
 *      WDC-25 Gold Standard for Product Categorization
 */
class Category
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var StoreCore\PIM\GlobalProductClassificationBrick|null $gpc
     *   OPTIONAL Global Product Classification (GPC) brick code.
     */
    public ?GlobalProductClassificationBrick $gpc = null;

    /**
     * @var Psr\Http\Message\UriInterface $productOntology
     *   OPTIONAL Product Types Ontology URL.
     */
    public ?UriInterface $productOntology = null;
}
