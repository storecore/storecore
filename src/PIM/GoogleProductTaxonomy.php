<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Container\ContainerInterface;
use StoreCore\Database\AbstractModel as AbstractDatabaseModel;
use StoreCore\Database\{ContainerException, NotFoundException};
use StoreCore\I18N\Language;
use StoreCore\Types\LanguageCode;

/**
 * Google product taxonomy repository.
 *
 * @package StoreCore\PIM
 * @see     https://support.google.com/merchants/answer/6324436?hl=en Google product category
 * @version 1.0.0-alpha.1
 */
class GoogleProductTaxonomy extends AbstractDatabaseModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array SUPPORTED_LANGUAGES
     *   Array of currently supported languages, listed in alphabetical order
     *   by the language and locale name.  'en-AU' for Australian English
     *   is disabled because the data file appears to be outdated.
     */
    public const array SUPPORTED_LANGUAGES = [
        'cs-CZ' => 'Čeština',
        'da-DK' => 'Dansk',
        'de-DE' => 'Deutsch',
    //  'en-AU' => 'English (Australia)',
        'en-GB' => 'English (Great Britain)',
        'en-US' => 'English (United States)',
        'es-ES' => 'Español',
        'fr-FR' => 'Français',
        'it-IT' => 'Italiano (Italia)',
        'it-CH' => 'Italiano (Svizzera)',
        'nl-NL' => 'Nederlands',
        'no-NO' => 'Norsk',
        'pl-PL' => 'Polski',
        'pt-BR' => 'Português',
        'ru-RU' => 'Русский',
        'ro-RO' => 'Română',
        'sv-SE' => 'Svenska',
        'tr-TR' => 'Türkçe',
        'ja-JP' => '日本語',
    ];

    /**
     * @var array $cache
     *   Temporary storage.
     */
    private array $cache = [];

    /**
     * @var string $language
     *   Display language for category names.
     */
    private string $language = 'en-US';

    /**
     * Full-text search for product categories.
     *
     * @param string $needle
     *   The string to search for.
     *
     * @return array
     *   Returns a key-value array with up to ten product categories.
     *   The array MAY be empty if no match was found.
     */
    public function find(string $needle): array
    {
        $needle = trim($needle);
        $needle = preg_replace('/\s+/', ' ', $needle);
        if (!empty($needle)) {
            $query = <<<'SQL'
                SELECT `taxonomy_id`, `full_path`
                  FROM `sc_product_taxonomy`
                 WHERE MATCH (`full_path`) AGAINST (? IN NATURAL LANGUAGE MODE)
                 LIMIT 10
            SQL;
            $query = preg_replace('/\s+/', ' ', $query);

            $statement = $this->Database->prepare($query);
            $statement->bindValue(1, $needle, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
                $statement->closeCursor();
                unset($query, $statement);
                if (\is_array($result)) {
                    return $result;
                }
            }
        }

        return [];
    }

    /**
     * Gets a Google product category from the taxonomy.
     *
     * @param string $id
     *   Unique identifier of the product category.
     *
     * @return StoreCore\PIM\GoogleProductCategory
     */
    public function get(string $id): GoogleProductCategory
    {
        if (!ctype_digit($id)) throw new ContainerException();
        $id = (int) $id;
        if (isset($this->cache[$id])) return $this->cache[$id];

        try {
            $statement = $this->Database->prepare(
                'SELECT `full_path` FROM `sc_product_taxonomy` WHERE `taxonomy_id` = :taxonomy_id AND `language_id` = :language_id'
            );
            $statement->bindValue(':taxonomy_id', $id, \PDO::PARAM_INT);
            $statement->bindValue(':language_id', $this->language, \PDO::PARAM_STR);
            if ($statement->execute() !== true) throw new ContainerException();
            $name = $statement->fetchColumn(0);
            if ($name === false) throw new NotFoundException();
            $this->cache[$id] = new GoogleProductCategory($id, $this->language, $name);
            return $this->cache[$id];
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if a Google product category ID exists.
     *
     * @param string $id
     *   Identifier of the product category to look for.
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        if (!ctype_digit($id)) {
            return false;
        }

        $id = (int) $id;
        if (
            $id > GoogleProductCategory::INT_MAX
            || $id < GoogleProductCategory::INT_MIN
        ) {
            return false;
        }

        if (isset($this->cache[$id])) {
            return true;
        }

        try {
            $statement = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_product_taxonomy` WHERE `taxonomy_id` = :taxonomy_id AND `language_id` = :language_id'
            );
            $statement->bindValue(':taxonomy_id', $id, \PDO::PARAM_INT);
            $statement->bindValue(':language_id', $this->language, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $count = $statement->fetchColumn(0);
                $statement->closeCursor();
                if ($count === 1) {
                    return true;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Sets the repository language.
     *
     * @param StoreCore\I18N\Language|StoreCore\Types\LanguageCode|string $language
     * @return void
     */
    public function setLanguage(Language|LanguageCode|string $language): void
    {
        $language = (string) $language;
        if (
            $language !== $this->language
            && isset(self::SUPPORTED_LANGUAGES[$language])
        ) {
            $this->cache = [];
            $this->language = $language;
        }
    }
}
