<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \JsonSerializable;
use \Stringable;
use StoreCore\Types\BusinessFunction;

/**
 * Offer for purchase.
 * 
 * An `OfferForPurchase` in Schema.org represents an `Offer` to sell something,
 * i.e. an `Offer` whose `businessFunction` is `Sell`.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/OfferForPurchase Schema.org type `OfferForPurchase`
 * @version 1.0.0-beta.1
 */
class OfferForPurchase extends Offer implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * {@inheritDoc}
     *
     * The `businessFunction` of an `OfferForPurchase` MUST always be `Sell`.
     */
    public BusinessFunction $businessFunction = BusinessFunction::Sell;
}
