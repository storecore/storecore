<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Http\Message\UriInterface;
use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Database\Metadata;
use StoreCore\Types\AdultOrientedEnumeration;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\UUID;
use StoreCore\Types\URL;

/**
 * StoreCore product model.
 *
 * @package StoreCore\PIM
 * @version 0.4.0
 *
 * @see https://support.google.com/merchants/answer/160161?hl=en
 *      “About unique product identifiers”, Google Merchant Center Help
 */
class Product extends AbstractProduct implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.0';

    /**
     * @var null|string $asin
     *   An Amazon Standard Identification Number (ASIN) is a 10-character
     *   alphanumeric unique identifier assigned by Amazon.com and its partners
     *   for product identification within the Amazon organization
     */
    public ?string $asin = null;

    /**
     * @var StoreCore\PIM\ItemAvailability $availability
     *   Schema.org item `availability` as an `ItemAvailability` value
     *   object (https://schema.org/ItemAvailability).  Defaults to
     *   a `https://schema.org/InStock` product availability to prevent
     *   any disruption of ongoing product sales.
     */
    public ItemAvailability $availability = ItemAvailability::InStock;

    /**
     * @var \Stringable|string|null $gtin13
     *   The GTIN-13 code of the `Product`.  This is equivalent to 13-digit
     *   ISBN codes and EAN UCC-13.  Former 12-digit UPC codes can be converted
     *   into a GTIN-13 code by simply adding a preceding zero.
     */
    public \Stringable|string|null $gtin13 = null;

    /**
     * @var StoreCore\Types\AdultOrientedEnumeration|null $hasAdultConsideration
     *   Used to tag an item to be intended or suitable
     *   for consumption or use by adults only.
     */
    public ?AdultOrientedEnumeration $hasAdultConsideration = null;

    /**
     * @var Psr\Http\Message\UriInterface|StoreCore\Types\URL|null $id = null;
     *   OPTIONAL custom JSON-LD `@id` identifier.
     */
    public UriInterface|URL|null $id = null;

    /**
     * @var null|string $keywords
     *   Keywords used to describe some item.  Multiple textual entries
     *   in a keywords list are delimited by commas.
     */
    private ?string $keywords = null;

    /**
     * @var null|string|array $mpn
     *   The Manufacturer Part Number (MPN) of the `Product`.
     */
    public null|string|array $mpn = null;

    /**
     * @var StoreCore\Types\DateTime|null $salesDiscontinuationDate
     *   The date and time the product is no longer for sale.
     */
    private ?DateTime $salesDiscontinuationDate = null;

    /**
     * @var StoreCore\Types\DateTime|null $supportDiscontinuationDate
     *   The date and time the product is no longer supported.
     */
    private ?DateTime $supportDiscontinuationDate = null;

    /**
     * Creates a product data object.
     *
     * @param string|null $identity
     *   OPTIONAL name or UUID of the product.
     * 
     * @uses StoreCore\Types\UUID::validate()
     */
    public function __construct(UUID|string|null $identity = null)
    {
        $this->metadata = new Metadata();
        if ($identity !== null) {
            if ($identity instanceof UUID || UUID::validate($identity)) {
                $this->setIdentifier($identity);
            } else {
                $this->name = $identity;
            }
        }
    }

    /**
     * Accessor.
     *
     * @param string $name
     * @return mixed
     * @uses \StoreCore\PIM\AbstractProductOrService::__get()
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'keywords' => $this->keywords,
            'releaseDate' => $this->releaseDate,
            'salesDiscontinuationDate' => $this->salesDiscontinuationDate,
            'supportDiscontinuationDate' => $this->supportDiscontinuationDate,
            default => parent::__get($name),
        };
    }

    /**
     * Mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @uses \StoreCore\PIM\AbstractProductOrService::__set()
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'keywords' => $this->setKeywords($value),
            'salesDiscontinuationDate' => $this->setSalesDiscontinuationDate($value),
            'supportDiscontinuationDate' => $this->setSupportDiscontinuationDate($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Sets unique, comma-separated keywords.
     *
     * @param string|array $keywords
     * @return void
     */
    private function setKeywords(string|array $keywords): void
    {
        if (\is_string($keywords)) {
            $keywords = str_ireplace(', ', ',', $keywords);
            $keywords = explode(',', $keywords);
        }

        foreach ($keywords as $key => $keyword) {
            $keyword = preg_replace('!\s+!', ' ', $keyword);
            $keywords[$key] = trim($keyword);
        }
        $keywords = array_unique($keywords);
        $this->keywords = implode(', ', $keywords);
    }

    /**
     * Sets the product’s sales discontinuation date.
     *
     * Products MAY be discontinued without removing them entirely by
     * setting a sales discontinuation date and time.
     *
     * @param \DateTimeInterface|string $datetime
     *   The date and time the product is no longer for sale.  If this optional
     *   parameter is omitted, the current date and time are set.
     *
     * @return void
     *
     * @uses \StoreCore\PIM\Product::setSupportDiscontinuationDate()
     *   If the product does not yet have a support discontinuation date,
     *   setting the sales discontinuation date will also set the support
     *   discontinuation date, with a default lag of two years.
     */
    public function setSalesDiscontinuationDate(\DateTimeInterface|string $datetime = 'now'): void
    {
        if (\is_string($datetime)) {
            $datetime = new DateTime($datetime, new \DateTimeZone('UTC'));
        } elseif (!$datetime instanceof DateTime) {
            $datetime = DateTime::createFromInterface($datetime);
        }
        $this->salesDiscontinuationDate = $datetime;

        if ($this->supportDiscontinuationDate === null) {
            $support_discontinuation_date = clone $this->salesDiscontinuationDate;
            $support_discontinuation_date->add(new \DateInterval('P2Y'));
            $this->setSupportDiscontinuationDate($support_discontinuation_date);
        }
    }

    /**
     * Sets the product’s support discontinuation date.
     *
     * @param \DateTimeInterface|string $datetime
     *   The date and time the product is no longer supported.
     *
     * @return void
     */
    public function setSupportDiscontinuationDate(\DateTimeInterface|string $datetime): void
    {
        if (\is_string($datetime)) {
            $this->supportDiscontinuationDate = new DateTime($datetime, new \DateTimeZone('UTC'));
        } elseif (!$datetime instanceof DateTime) {
            $this->supportDiscontinuationDate = DateTime::createFromInterface($datetime);
        } else {
            $this->supportDiscontinuationDate = $datetime;
        }
    }
}
