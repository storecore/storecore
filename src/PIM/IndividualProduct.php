<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Individual product.
 *
 * An `IndividualProduct` is a single, identifiable `Product` instance
 * with a `serialNumber`.
 * 
 * @package StoreCore\PIM
 * @version 1.0.0
 * @see     https://schema.org/IndividualProduct Schema.org type `IndividualProduct`
 * @see     https://schema.org/serialNumber Schema.org property `serialNumber`
 */
class IndividualProduct extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $serialNumber
     *   The serial number or any alphanumeric identifier of a particular
     *   product.  When attached to an offer, the serial number is a shortcut
     *   for the serial number of the product included in the offer.
     */
    public string $serialNumber = '';

    /**
     * Clears the serial number on object cloning.
     *
     * @param void
     * @return void
     */
    public function __clone(): void
    {
        $this->serialNumber = '';
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@type'] = 'IndividualProduct';
        if (!empty($this->serialNumber)) $result['serialNumber'] = $this->serialNumber;
        return $result;
    }
}
