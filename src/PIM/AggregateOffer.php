<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use function \empty;

/**
 * Aggregate offer.
 * 
 * When a single `Product` is associated with multiple offers (for example,
 * the same pair of shoes is offered by different merchants), then
 * `AggregateOffer` can be used.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://schema.org/AggregateOffer Schema.org type `AggregateOffer`
 * @version 1.0.0-beta.1
 */
class AggregateOffer extends Offer implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * @var int|float|string|null $highPrice
     *   The highest price of all offers available.
     */
    public int|float|string|null $highPrice = null;

    /**
     * @var int|float|string|null $lowPrice
     *   The lowest price of all offers available.
     */
    public int|float|string|null $lowPrice = null;

    /**
     * @var int $offerCount
     *   The number of offers for a `Product`.
     */
    public int $offerCount = 0;

    /**
     * 
     * @var array $offers
     *   The offers to provide an item — for example, the `Offer` to `Sell`
     *   a `Product`, rent the DVD of a movie, perform a service, or give away
     *   tickets to an event.  Use `businessFunction` to indicate the kind of
     *   transaction offered, i.e. `Sell`, `LeaseOut`, etc.
     */
    public array $offers = [];

    /**
     * Counts the offers in the aggegate.
     *
     * When the `AggregateOffer.offers` container is not empty, `count()`
     * counts the current number of items in the container and sets the
     * `AggregateOffer.offerCount` counter.  If the `AggregateOffer.offers`
     * is empty, the `AggregateOffer.offerCount` counter MAY be set to a
     * different count.
     * 
     * @param void
     *
     * @return int
     *   Number of offers as an integer.
     */
    public function count(): int
    {
        if (!empty($this->offers)) {
            $this->offerCount = count($this->offers);
        }
        return $this->offerCount;
    }
}
