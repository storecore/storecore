<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \JsonSerializable;
use \SplObjectStorage;

use function \iterator_to_array;

/**
 * Compound price specification.
 *
 * A compound price is a container for multiple unit prices.
 * For example, the compound price $p for a bed $a plus a mattress $b is:
 *
 * ```php
 * $a = new \StoreCore\PIM\UnitPriceSpecification();
 * $a->description = 'Bed';
 *
 * $b = new \StoreCore\PIM\UnitPriceSpecification();
 * $b->description = 'Mattress';
 *
 * $p = new \StoreCore\PIM\CompoundPriceSpecification();
 * $p->attach($a);
 * $p->attach($b);
 * ```
 *
 * ```json
 * {
 *   "@context": "https://schema.org",
 *   "@type": "CompoundPriceSpecification",
 *   "priceComponent": [
 *     {
 *       "@context": "https://schema.org",
 *       "@type": "UnitPriceSpecification",
 *       "description": "Bed"
 *     },
 *     {
 *       "@context": "https://schema.org",
 *       "@type": "UnitPriceSpecification",
 *       "description": "Mattress"
 *     }
 *   ]
 * }
 * ```
 *
 * @api
 * @package StoreCore\Core
 * @version 0.3.0
 * @see     https://schema.org/CompoundPriceSpecification Schema.org type `CompoundPriceSpecification`
 */
class CompoundPriceSpecification extends PriceSpecification implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var \SplObjectStorage $priceComponents
     *   All `UnitPriceSpecification` nodes that apply in parallel for
     *   this `CompoundPriceSpecification` node.
     */
    private SplObjectStorage $priceComponents;

    /**
     * @var StoreCore\PIM\PriceTypeEnumeration $priceType
     *   Defines the type of a price specified for an offered product or service.
     *   Defaults to `https://schema.org/ListPrice` for a list price.
     */
    public PriceTypeEnumeration $priceType = PriceTypeEnumeration::ListPrice;

    /**
     * Creates a compound price specification.
     *
     * @param void
     */
    public function __construct()
    {
        $this->priceComponents = new SplObjectStorage();
    }

    /**
     * Adds a price component.
     *
     * @param StoreCore\Types\UnitPriceSpecification $price_component
     *   A `UnitPriceSpecification` to apply in parallel for this
     *   `CompoundPriceSpecification` node.
     *
     * @return void
     */
    public function attach(UnitPriceSpecification $price_component): void
    {
        $this->priceComponents->attach($price_component);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@type'] = 'CompoundPriceSpecification';
        $result['priceComponent'] = iterator_to_array($this->priceComponents, false);
        $result['priceType'] = $this->priceType->value;
        return $result;
    }
}
