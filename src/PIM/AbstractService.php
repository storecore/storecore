<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\{IdentityInterface, IdentityTrait};

/**
 * Prototype of a service.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
abstract class AbstractService extends AbstractProductOrService implements IdentityInterface
{
    use IdentityTrait;
}
