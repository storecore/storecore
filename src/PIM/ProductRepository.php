<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\SimpleCache\CacheInterface;
use StoreCore\Registry;
use StoreCore\Database\AbstractModel as AbstractDatabaseModel;
use StoreCore\Database\ProductMapper;
use StoreCore\Database\{ContainerException, NotFoundException};
use StoreCore\FileSystem\ObjectCache;
use StoreCore\Types\UUID;

use function \ctype_digit;

/**
 * Product repository.
 *
 * @api
 * @package StoreCore\PIM
 * @see     https://www.php.net/manual/en/class.countable.php
 * @version 0.2.0
 */
class ProductRepository extends AbstractDatabaseModel implements
    ContainerInterface,
    \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var Psr\SimpleCache\CacheInterface $cache
     *   File system cache for objects.
     */
    public CacheInterface $cache;

    /**
     * @var StoreCore\Database\ProductMapper $dataMapper
     *   Product data mapper.
     */
    private ProductMapper $dataMapper;

    /**
     * Creates a product repository.
     *
     * @param StoreCore\Registry $registry
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->cache = new ObjectCache();
        $this->dataMapper = new ProductMapper($this->Registry);
    }

    /**
     * Counts the number of active products.
     *
     * @param void
     * @return int
     * @uses StoreCore\Database\ProductMapper::count()
     */
    public function count(): int
    {
        return $this->dataMapper->count();
    }

    /**
     * Gets a product from the repository.
     *
     * @param string $id
     *   Product identifier as a UUID string or a number.
     *
     * @return StoreCore\PIM\Product
     * 
     * @uses StoreCore\Database\ProductMapper::has
     *   `ProductMapper::has()` populates the `$identityMap` of the data mapper
     *   if the `product` exists.
     *
     * @throws Psr\Container\ContainerExceptionInterface
     */
    public function get(string $id): Product
    {
        try {
            if (!$this->has($id)) {
                throw new NotFoundException();
            } else {
                return $this->dataMapper->get($id);
            }
        } catch (ContainerExceptionInterface $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new ContainerException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * Checks if the repository contains a product.
     *
     * @param string $id
     *   Product identifier as a string or a number.
     *
     * @return bool
     *   Returns `true` if the `Product` exists, otherwise `false`.  The product’s
     *   UUID and ID are cached in the identity map of the data mapper if the
     *   product exists.
     *
     * @uses \StoreCore\Types\UUID::validate()
     */
    public function has(string $id): bool
    {
        if (!UUID::validate($id)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        return $this->dataMapper->has($id);
    }
}
