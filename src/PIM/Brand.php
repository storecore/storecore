<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\CMS\ImageObject;
use StoreCore\Types\AggregateRating;
use StoreCore\Types\URL;

/**
 * Brand.
 * 
 * A _brand_ is a `name` used by an `Organization` or business `Person`
 * for labeling a `Product`, `ProductGroup`, or similar.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Brand Schema.org type `Brand`
 * @version 1.0.0-alpha.1
 */
class Brand implements IdentityInterface, \JsonSerializable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Types\AggregateRating $aggregateRating
     *   The overall rating, based on a collection of reviews or ratings, of the brand.
     */
    public ?AggregateRating $aggregateRating = null;

    /**
     * @var null|string $alternateName
     *   An alias or alternative name of the brand.
     */
    public ?string $alternateName = null;

    /**
     * @var null|StoreCore\CMS\ImageObject|StoreCore\Types\URL $logo
     *   An associated logo.
     */
    public null|ImageObject|URL $logo = null;

    /**
     * @var null|string $name
     *   The name of the brand.
     */
    public ?string $name = null;

    /**
     * Creates a brand.
     *
     * @var null|string $name
     *   OPTIONAL brand name.
     */
    public function __construct(?string $name = null)
    {
        $this->name = $name;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = array();
        if ($this->hasIdentifier()) {
            $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/brands/' . $this->getIdentifier();
        }

        $result['@context'] = 'https://schema.org';
        $result['@type'] = 'Brand';

        if ($this->logo instanceof URL) {
            $result['logo'] = (string) $this->logo;
        } elseif ($this->logo instanceof \JsonSerializable) {
            $result['logo'] = $this->logo->jsonSerialize();
            unset($result['logo']['@context']);
        }

        if ($this->hasIdentifier()) {
            $result['identifier'] = $this->getIdentifier()->__toString();
        }

        if (!empty($this->name)) {
            $result['name'] = $this->name;
            if (!empty($this->alternateName)) {
                $result['alternateName'] = $this->alternateName;
            }
        }

        return $result;
    }
}
