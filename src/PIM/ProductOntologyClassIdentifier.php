<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Http\Message\UriInterface;
use StoreCore\Types\URL;

/**
 * Product Types Ontology class identifier.
 *
 * @api
 * @package StoreCore\PIM
 * @version 1.0.0
 *
 * @see http://www.productontology.org/
 *      The Product Types Ontology: High-precision identifiers
 *      for product types based on Wikipedia
 * 
 * @see https://www.w3.org/2001/sw/wiki/Productontology
 *      W3C Semantic Web Standards
 *
 * @see https://gitlab.com/storecore/storecore/-/blob/develop/src/Types/URL.php
 *      StoreCore type `URL`
 */
class ProductOntologyClassIdentifier extends URL implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a product ontology URI.
     *
     * @param Psr\Http\Message\UriInterface|string $uri
     *   Wikipedia or Product Types Ontology URI as a string or an object
     *   that implements the PSR-7 `UriInterface`.
     */
    public function __construct(UriInterface|string $uri)
    {
        parent::__construct($uri);

        if (\str_contains($this->value, '/wiki/')) {
            $uri = explode('/wiki/', $this->value, 2);
            $this->value = 'http://www.productontology.org/id/' . $uri[1];
        }
    }
}
