<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \DomainException;
use \Exception;
use \RangeException;
use \Stringable;
use \TypeError;
use \ValueError;

use StoreCore\Types\Varchar;
use StoreCore\Types\TypeInterface;
use StoreCore\Types\ValidateInterface;

use function \ctype_digit;
use function \empty;
use function \is_float;
use function \is_int;
use function \is_string;
use function \random_int;
use function \sprintf;
use function \str_pad;
use function \str_replace;
use function \strlen;

/**
 * International article number (EAN).
 *
 * @package StoreCore\PIM
 * @see     https://ref.gs1.org/standards/genspecs GS1 General Specifications Standard
 * @see     https://schema.org/gtin13 Schema.org property `gtin13`
 * @version 0.2.0
 */
class InternationalArticleNumber extends Varchar implements
    Stringable,
    TypeInterface,
    ValidateInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * Creates an EAN.
     *
     * @param string|int $initial_value
     * @param bool $strict
     */
    public function __construct(mixed $initial_value, bool $strict = true)
    {
        if (is_float($initial_value)) {
            $initial_value = (string) $initial_value;
            $initial_value = str_replace('.', '', $initial_value);
        }

        if (is_int($initial_value)) {
            $initial_value = (string) $initial_value;
        } elseif (!is_string($initial_value)) {
            throw new TypeError('International article number MUST be string or int.', time());
        }

        $initial_value = str_replace(['-', '.', ' '], '', $initial_value);
        if (!ctype_digit($initial_value)) {
            throw new ValueError();
        }

        parent::__construct($initial_value, $strict);

        $length = strlen($this->value);
        if ($length > 13) {
            throw new ValueError();
        } elseif ($length === 13) {
            if ((int) substr($this->value, -1) !== self::calculateCheckDigit(substr($this->value, 0, 12))) {
                throw new ValueError();
            }
        } elseif ($length === 12) {
            $this->value .= self::calculateCheckDigit($this->value);
        } elseif (!$strict) {
            if ($this->value === '0') {
                $this->value = (string) random_int(200, 299);
            }
            $this->value .= str_pad((string) random_int(1, 999999999), 12 - strlen($this->value), '0');
            $this->value .= self::calculateCheckDigit($this->value);
        } else {
            throw new ValueError();
        }
    }

    /**
     * Calculates the check digit.
     *
     * @param string|int $number
     * @return int
     */
    public static function calculateCheckDigit(string|int $number): int
    {
        $number = (string) $number;
        $weight_flag = true;
        $sum = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += (int) $number[$i] * ($weight_flag ? 3 : 1);
            $weight_flag = !$weight_flag;
        }
        return (10 - ($sum % 10)) % 10;
    }

    /**
     * Gets the next EAN-13 article number.
     *
     * @param void
     * @return StoreCore\PIM\InternationalArticleNumber
     * @throws \RangeException
     */
    public function getNextNumber(): InternationalArticleNumber
    {
        $prefix = substr($this->value, 0, 7);
        $current_number = (int) substr($this->value, 7, 5);

        if ($current_number === 99999) {
            throw new RangeException();
        }

        $next_number = $current_number + 1;
        $next_number = sprintf('%05d', $next_number);
        return new InternationalArticleNumber($prefix . $next_number);
    }

    /**
     * Generates a random EAN-13 article number.
     *
     * @param null|string|int $prefix
     * @return StoreCore\PIM\InternationalArticleNumber
     */
    public static function getRandomNumber(null|string|int $prefix = null): InternationalArticleNumber
    {
        if (empty($prefix)) {
            $prefix = 0;
        }
        return new InternationalArticleNumber($prefix, false);
    }

    /**
     * Validates an EAN-13 article number.
     *
     * @param mixed $variable
     * @return bool
     */
    public static function validate(mixed $variable): bool
    {
        try {
            $ean = new InternationalArticleNumber($variable, true);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
