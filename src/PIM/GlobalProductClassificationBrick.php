<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * Global Product Classification (GPC) brick.
 *
 * @package StoreCore\PIM
 * @version 0.1.0
 *
 * @see https://www.gs1.org/standards/gpc
 *      Global Product Classification (GPC), GS1 Standards
 *
 * @see https://www.gs1.org/standards/gpc/how-gpc-works
 *      How Global Product Classification (GPC) works
 * 
 * @see https://www.gs1.org/sites/default/files/gpc_development_and_implementation_1.pdf
 *      Global Product Classification (GPC) Development & Implementation Guide
 */
readonly class GlobalProductClassificationBrick
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var string $identifier
     *   Numeric brick code as a string.
     */
    public string $identifier;

    /**
     * @var null|string $name
     *   OPTIONAL name or description of the brick code.
     */
    public ?string $name;

    /**
     * Creates a GPC brick code.
     *
     * @param string|int $identifier
     * @param string|null $name
     */
    public function __construct(string|int $identifier, ?string $name = null)
    {
        $this->identifier = (string) $identifier;
        $this->name = $name;
    }
}
