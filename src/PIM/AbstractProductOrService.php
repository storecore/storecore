<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use Psr\Http\Message\UriInterface;
use StoreCore\CMS\Review;
use StoreCore\CRM\Organization;
use StoreCore\Database\Metadata;
use StoreCore\Types\AggregateRating;
use StoreCore\Types\DateTime;
use StoreCore\Types\URL;

/**
 * Abstract model for products and services.
 * 
 * A _product_ is a physical good, whereas a _service_ is intangible.
 * This abstraction implements the common properties that apply
 * to all products (goods) and services (intangibles).
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Product Schema.org type `Product`
 * @see     https://schema.org/Service Schema.org type `Service`
 * @see     http://wiki.goodrelations-vocabulary.org/Documentation/Product_or_Service Product or Service, GoodRelations documentation
 * @see     https://schema.org/additionalType Schema.org property `additionalType` of a `Thing`
 * @version 1.0.0-alpha.1
 */
abstract class AbstractProductOrService
{
    /**
     * @var StoreCore\Types\URL|null
     *   An additional type for the item, typically used for adding more
     *   specific types from external vocabularies in microdata syntax.
     *   This is a relationship between something and a class that the thing
     *   is in.
     */
    protected ?URL $additionalType = null;

    /**
     * @var StoreCore\Types\AggregateRating|null $aggregateRating
     *   The overall rating, based on a collection of reviews or ratings,
     *   of a `Product` or `Service`.
     */
    public ?AggregateRating $aggregateRating = null;

    /**
     * @var string|null $alternateName
     *   An alias for the item.
     */
    public ?string $alternateName = null;

    /**
     * @var null|StoreCore\PIM\Brand|StoreCore\CRM\Organization $brand
     *   The brand or brand(s) associated with a `Product` or `Service`.
     */
    protected null|Brand|Organization $brand = null;

    /**
     * @var string|null $description
     *   A description of the item.
     */
    public ?string $description = null;

    /**
     * @var null|string $gtin
     *   Global Trade Item Number (GTIN).  GTINs identify trade items, including
     *   products and services, using numeric identification codes.
     */
    public ?string $gtin = null;

    /**
     * @var StoreCore\Database\Metadata $metadata
     *   Internal metadata for object storage.
     */
    public Metadata $metadata;

    /**
     * @var string|null $name
     *   The name of the item.
     */
    public ?string $name = null;

    /**
     * @var array $review
     *   One or more reviews of the item.  Supersedes `reviews`.
     */
    private array $review = [];

    /**
     * @var \Stringable|string|null $sku
     *   The Stock Keeping Unit (SKU): a merchant-specific identifier for
     *   a `Product` or `Service`, or the `Product` to which an `Offer` refers.
     */
    public \Stringable|string|null $sku = null;

    /**
     * @var string|null $slogan
     *   A slogan or motto associated with the item.
     */
    public ?string $slogan = null;

    /**
     * Reads data from inaccessible or non-existing properties.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'additionalType' => $this->additionalType,
            'brand' => $this->brand,
            'dateCreated' => $this->metadata->dateCreated,
            'dateModified' => $this->getDateModified(),
            'review', 'reviews' => $this->getReview(),
            default => null,
        };
    }

    /**
     * Writes data to inaccessible or non-existing properties.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'additionalType' => $this->setAdditionalType($value),
            'brand' => $this->setBrand($value),
            'dateCreated' => $this->metadata->dateCreated = $value,
            'dateModified' => $this->setDateModified($value),
            'review', 'reviews' => $this->setReview($value),
            default => throw new \ValueError('Unknown property: ' . $name),
        };
    }

    /**
     * Gets the product’s modification date.
     *
     * @param void
     *
     * @return StoreCore\Types\DateTime
     *   Returns the date and time the product was last modified as a
     *   `DateTime` object.
     */
    public function getDateModified(): DateTime
    {
        return $this->metadata->dateModified === null ? $this->metadata->dateCreated : $this->metadata->dateModified;
    }

    /**
     * Gets the product review or reviews.
     * 
     * @param void
     * @return null|StoreCore\CMS\Review|Review|array
     */
    private function getReview(): null|Review|array
    {
        if (empty($this->review)) return null;
        return count($this->review) === 1 ? $this->review[0] : $this->review;
    }

    /**
     * Sets the additionalType property.
     *
     * @internal
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string $url
     * @return void
     */
    private function setAdditionalType(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL((string) $url);
        $this->additionalType = $url;
    }

    /**
     * Sets the brand(s) associated with the product or service.
     *
     * @param StoreCore\PIM\Brand|StoreCore\CRM\Organization|string $brand
     *   `Brand`, `Organization`, or brand name as a string.  If an
     *   `Organization` is set and this `Organization` maintains only one
     *   `Brand`, this `Brand` is used as the product’s `brand` property.
     *   If the `Organization` maintains multiple brands, the `Organization`
     *   is used — and not all of its brands, because usually not all brands
     *   apply to a single product.
     *
     * @return void
     */
    public function setBrand(Brand|Organization|string $brand): void
    {
        if (\is_string($brand)) {
            $brand = new Brand($brand);
        }

        // Use single `Organization.brand` as the `Product.brand`:
        if (
            $brand instanceof Organization
            && $brand->brand instanceof Brand
        ) {
            $brand = $brand->brand;
        }

        $this->brand = $brand;
    }

    /**
     * Sets the product’s modification date.
     *
     * @param \DateTimeInterface|string $datetime
     *   The date and time the product data were last modified as a `DateTime`
     *   object or a date/time string.  If this optional parameter is omitted,
     *   the current date and time is set.
     *
     * @return void
     */
    public function setDateModified(\DateTimeInterface|string $datetime = 'now'): void
    {
        if (\is_string($datetime)) {
            $datetime = new DateTime($datetime, new \DateTimeZone('UTC'));
        } elseif (!$datetime instanceof DateTime) {
            $datetime = DateTime::createFromInterface($datetime);
        }
        $this->metadata->dateModified = $datetime;
    }

    /**
     * Sets one or more reviews.
     *
     * @param StoreCore\CMS\Review|array $review
     *   A review of an item or an array of reviews.
     *
     * @return void
     */
    private function setReview(Review|array $review): void
    {
        if ($review instanceof Review) {
            $review = array($review);
        } elseif (empty($review)) {
            throw new \ValueError();
        }

        foreach ($review as $value) {
            if (!$value instanceof Review) {
                throw new \ValueError();
            }
        }

        $this->review = $review;
    }
}
