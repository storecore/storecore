<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Product model.
 *
 * A `ProductModel` is a datasheet or vendor specification of a `Product`
 * (in the sense of a prototypical description).
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/ProductModel Schema.org type `ProductModel`
 * @version 0.1.0
 */
class ProductModel extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|StoreCore\PIM\ProductGroup|StoreCore\PIM\ProductModel $isVariantOf
     *   Indicates the kind of `Product` that this `ProductModel` is a variant of.
     *   This is a pointer (from a `ProductModel`) to a base product from which
     *   this `Product` is a variant.  It is safe to infer that the variant
     *   inherits all `Product` features from the base model, unless defined
     *   locally.  This is not transitive.  In the case of a `ProductGroup`, the
     *   group description also serves as a template, representing a set of
     *   products that vary on explicitly defined, specific dimensions only
     *   (so it defines both a set of variants, as well as which values
     *   distinguish amongst those variants).
     */
    public null|ProductGroup|ProductModel $isVariantOf = null;

    /**
     * @var null|StoreCore\PIM\ProductModel $predecessorOf
     *   A pointer from a previous, often discontinued variant of the `Product`
     *   to its newer variant.
     */
    public ?ProductModel $predecessorOf = null;

    /**
     * @var null|StoreCore\PIM\ProductModel $successorOf
     *   A pointer from a newer variant of a `Product` to its previous,
     *   often discontinued predecessor.
     */
    public ?ProductModel $successorOf = null;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@type'] = 'ProductModel';
        return $result;
    }
}
