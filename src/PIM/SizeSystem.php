<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * Size system enumeration.
 *
 * @package StoreCore\PIM
 * @see     https://support.google.com/merchants/answer/6324502?hl=en
 * @version 1.0.0
 */
enum SizeSystem: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    case AU  = 'AU';
    case BR  = 'BR';
    case CN  = 'CN';
    case DE  = 'DE';
    case EU  = 'EU';
    case FR  = 'FR';
    case IT  = 'IT';
    case JP  = 'JP';
    case MEX = 'MEX';
    case UK  = 'UK';
    case US  = 'US';
}
