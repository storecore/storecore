<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\CRM\{Organization, Person};
use StoreCore\Types\ServiceType;

/**
 * StoreCore service model.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Service Schema.org type `Service`
 * @version 0.4.0
 */ 
class Service extends AbstractService implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.0';

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person $provider
     *   The service provider, service operator, or service performer.  Another
     *   party (a seller) MAY offer those services or goods on behalf of the
     *   provider.  A provider MAY also serve as the seller.
     */
    public null|Organization|Person $provider = null;

    /**
     * @var StoreCore\Types\ServiceType $type
     *   Type of service, used to communicate the entity `@type` in JSON-LD;
     *   Defaults to a generic Schema.org `Service`.  This variable MUST NOT
     *   be used for the Schema.org `Service.serviceType` property.
     */
    public ServiceType $type = ServiceType::Service;

    /**
     * Specifies data which should be serialized to JSON.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = array();

        if ($this->hasIdentifier()) {
            $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/services/' . $this->getIdentifier();
        }
        $result['@context'] = 'https://schema.org';
        $result['@type'] = $this->type->name;

        $properties = $this->toArray();
        unset($properties['type']);

        return $result + $properties;
    }

    /**
     * @internal
     * @param void
     * @return array
     */
    private function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }
}
