<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\IdentityInterface;

/**
 * Dietary supplement.
 *
 * A `DietarySupplement` is a `Product` taken by mouth that contains a dietary
 * ingredient intended to supplement the diet.  Dietary ingredients MAY include
 * vitamins, minerals, herbs or other botanicals, amino acids, and substances
 * such as enzymes, organ tissues, glandulars and metabolites.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/DietarySupplement Schema.org type `DietarySupplement`
 * @version 0.1.0
 */
class DietarySupplement extends Product implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
