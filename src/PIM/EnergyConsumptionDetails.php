<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use \JsonSerializable;
use StoreCore\Types\Thing;

/**
 * Energy consumption details.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/EnergyConsumptionDetails Schema.org type `EnergyConsumptionDetails`
 * @version 1.0.0-alpha.1
 */
class EnergyConsumptionDetails extends Thing implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\PIM\EUEnergyEfficiencyEnumeration $energyEfficiencyScaleMax
     *   Specifies the most energy efficient class on the regulated EU energy
     *   consumption scale for the product category a `Product` belongs to.
     *   For example, energy consumption for televisions placed on the market
     *   after January 1, 2020 is scaled from D to A+++.
     */
    public ?EUEnergyEfficiencyEnumeration $energyEfficiencyScaleMax = null;

    /**
     * @var null|StoreCore\PIM\EUEnergyEfficiencyEnumeration $energyEfficiencyScaleMin
     *   Specifies the least energy efficient class on the regulated EU energy
     *   consumption scale for the product category a `Product` belongs to.
     */
    public ?EUEnergyEfficiencyEnumeration $energyEfficiencyScaleMin = null;

    /**
     * @var null|EUEnergyEfficiencyEnumeration $hasEnergyEfficiencyCategory
     *   Defines the energy efficiency category (which could be either a rating
     *   out of range of values or a yes/no certification) for a `Product`
     *   according to an international energy efficiency standard.
     */
    public ?EUEnergyEfficiencyEnumeration $hasEnergyEfficiencyCategory = null;
}
