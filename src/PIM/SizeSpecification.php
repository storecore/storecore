<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\Types\QuantitativeValue;

/**
 * Size specification.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/SizeSpecification Schema.org type `SizeSizeSpecification`
 * @version 0.1.0
 */
class SizeSpecification extends QuantitativeValue implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|StoreCore\PIM\WearableSizeGroupEnumeration|array $sizeGroup
     *   The size group (also known as “size type”) for the `size` of a `Product`.
     *   Size groups are common in the fashion industry to define size segments
     *   and suggested audiences for wearable products.  Multiple values can be
     *   combined, for example “men’s big and tall” or “petite maternity”.
     */
    public null|WearableSizeGroupEnumeration|array $sizeGroup = null;
}
