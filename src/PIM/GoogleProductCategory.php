<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\I18N\Language;
use StoreCore\Types\LanguageCode;

/**
 * Google product category.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 *
 * @see https://support.google.com/merchants/answer/188494?hl=en
 *      Products Feed Specification, Google Merchant Center Help
 *
 * @see https://support.google.com/merchants/answer/1705911?hl=en
 *      The Google product taxonomy, Google Merchant Center Help
 */
readonly class GoogleProductCategory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var int INT_MIN Lowest possible integer value in the Google product taxonomy.
     * @var int INT_MAX Highest possible value in the taxonomy.
     */
    public const INT_MIN = 1;
    public const INT_MAX = 543703;


    /**
     * @var int $identifier
     *   Unique Google product category ID as an integer.
     */
    public int $identifier;

    /**
     * @var string $language
     *   Language code of the category `$name`.  Defaults to `en-US`
     *   for American English.
     */
    public string $language;

    /**
     * @var null|string $name
     *   OPTIONAL name of the category in the `$language` language.
     */
    public ?string $name;


    /**
     * Creates a Google product category.
     *
     * @param string|int $identifier
     * @param StoreCore\I18N\Language|StoreCore\Types\LanguageCode|string $language
     * @param string|null $name
     */
    public function __construct(
        string|int $identifier,
        Language|LanguageCode|string $language = 'en-US',
        ?string $name = null,
    ) {
        $this->setIdentifier($identifier);
        $this->language = (string) $language;
        $this->name = $name;
    }

    /**
     * Sets the category identifier.
     * 
     * @internal
     * @param string|int $identifier
     * @return void
     */
    private function setIdentifier(string|int $identifier): void
    {
        if (\is_string($identifier)) {
            if (\ctype_digit($identifier)) {
                $identifier = (int) $identifier;
            } else {
                throw new \TypeError();
            }
        }

        if ($identifier < self::INT_MIN || $identifier > self::INT_MAX) {
            throw new \ValueError();
        }
        $this->identifier = $identifier;
    }
}
