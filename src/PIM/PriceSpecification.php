<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

use StoreCore\Currency;
use StoreCore\Money;
use StoreCore\Types\QuantitativeValue;
use StoreCore\Types\{ValidFromTrait, ValidThroughTrait};

use \JsonSerializable;

use function \round;

class_exists(Currency::class);
class_exists(Money::class);
class_exists(QuantitativeValue::class);
trait_exists(ValidFromTrait::class);
trait_exists(ValidThroughTrait::class);

/**
 * Price specification.
 *
 * @package StoreCore\Core
 * @version 1.0.0-beta.1
 *
 * @see https://schema.org/PriceSpecification
 *      Schema.org type `PriceSpecification`
 *
 * @see https://developers.google.com/gmail/markup/reference/types/PriceSpecification
 *      PriceSpecification - Email Markup - Google Developers Guides for Gmail
 * 
 * @see https://developers.google.com/gmail/markup/reference/order?hl=en
 *      Order use cases - Email Markup Reference - Gmail for Developers
 */
class PriceSpecification implements JsonSerializable
{
    use ValidFromTrait, ValidThroughTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * @var null|string $description
     *   A description of the price.
     */
    public ?string $description = null;

    /**
     * @var StoreCore\Types\QuantitativeValue|null $eligibleQuantity
     *   The interval and unit of measurement of ordering quantities for which
     *   the offer or price specification is valid.  This allows e.g. specifying
     *   that a certain freight charge is valid only for a certain quantity.
     */
    public ?QuantitativeValue $eligibleQuantity = null;

    /**
     * @var StoreCore\PIM\PriceSpecification|null $eligibleTransactionVolume
     *   The transaction volume, in a monetary unit, for which the offer or
     *   price specification is valid, e.g. for indicating a minimal purchasing
     *   volume, to express free shipping above a certain order volume, or
     *   to limit the acceptance of credit cards to purchases to a certain
     *   minimal amount.
     */
    public ?PriceSpecification $eligibleTransactionVolume = null;

    /**
     * @var int|float|null $maxPrice
     *   The highest price if the price is a range.
     */
    public int|float|null $maxPrice = null;

    /**
     * @var int|float|null $minPrice
     *   The lowest price if the price is a range.
     */
    public int|float|null $minPrice = null;

    /**
     * @var int|float|null $price
     *   The offer price of a product, or of a price component when attached to
     *   the `PriceSpecification` and its subtypes.  Although this property MAY
     *   be set to a string, it is RECOMMENDED to only use numbers and provide
     *   a `priceCurrency` string.
     */
    public int|float|string|null $price = null;

    /**
     * @var null|StoreCore\Currency $priceCurrency
     *   The currency of the price, or a price component when attached to
     *   `PriceSpecification` and its subtypes.
     */
    public ?Currency $priceCurrency = null;

    /**
     * @var null|bool $valueAddedTaxIncluded
     *   Specifies whether the applicable value-added tax (VAT) is included in
     *   the price specification or not.  Defaults to `true`.
     */
    public ?bool $valueAddedTaxIncluded = true;

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'PriceSpecification',
        ];

        if ($this->description !== null) $result['description'] = $this->description;

        if ($this->eligibleQuantity !== null) $result['eligibleQuantity'] = $this->eligibleQuantity;
        if ($this->eligibleTransactionVolume !== null) $result['eligibleTransactionVolume'] = $this->eligibleTransactionVolume;

        if ($this->price !== null || $this->minPrice !== null || $this->maxPrice !== null) {
            if ($this->priceCurrency === null) {
                $this->priceCurrency = new Currency();
            }

            if ($this->maxPrice !== null) {
                $result['maxPrice'] = round($this->maxPrice, $this->priceCurrency->precision);
            }

            if ($this->minPrice !== null) {
                $result['minPrice'] = round($this->minPrice, $this->priceCurrency->precision);
            }

            if ($this->price !== null) { 
                $result['price'] = round($this->price, $this->priceCurrency->precision);
            }

            $result['priceCurrency'] = $this->priceCurrency->code;
        }

        if ($this->validFrom !== null) $result['validFrom'] = (string) $this->validFrom;
        if ($this->validThrough !== null) $result['validThrough'] = (string) $this->validThrough;

        if ($this->valueAddedTaxIncluded !== null) $result['valueAddedTaxIncluded'] = $this->valueAddedTaxIncluded;

        return $result;
    }

    /**
     * Sets the price.
     *
     * @param StoreCore\Money $price
     * @return void
     */
    public function setPrice(Money $price): void
    {
        $this->price = $price->getAmount();
        $this->priceCurrency = $price->getCurrency();
    }
}
