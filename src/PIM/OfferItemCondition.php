<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PIM;

/**
 * Schema.org offer item conditions.
 * 
 * A list of possible conditions of an item.  Instances of `OfferItemCondition`
 * MAY appear as a value for the `itemCondition` property of a `Product`,
 * `Offer`, `Demand`, or `MerchantReturnPolicy`.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/OfferItemCondition Schema.org enumeration `OfferItemCondition`
 * @see     https://support.google.com/merchants/answer/6386198?hl=en
 * @see     https://developers.google.com/gmail/markup/reference/types/OfferItemCondition
 * @version 27.0.0
 */
enum OfferItemCondition: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org version 27.0
     *   from May 20, 2024.
     */
    public const string VERSION = '27.0.0';

    case DamagedCondition     = 'https://schema.org/DamagedCondition';
    case NewCondition         = 'https://schema.org/NewCondition';
    case RefurbishedCondition = 'https://schema.org/RefurbishedCondition';
    case UsedCondition        = 'https://schema.org/UsedCondition';

    /**
     * Converts a tiny integer to an OfferItemCondition.
     *
     * @internal
     * @param int $key
     * @return static
     */
    public static function fromInteger(int $key): static
    {
        if ($key < 0 || $key > 3) {
            throw new \ValueError();
        }

        return match ($key) {
            0 => static::NewCondition,
            1 => static::DamagedCondition,
            2 => static::RefurbishedCondition,
            3 => static::UsedCondition,
        };
    }
}
