<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \Stringable;
use \TypeError;
use \ValueError;

use function \ctype_upper;
use function \is_int;
use function \is_string;
use function \str_pad;
use function \strtoupper;
use function \trim;

use const \STR_PAD_LEFT;

/**
 * Currency model.
 *
 * @package StoreCore\Core
 * @version 0.2.3
 */
readonly class Currency implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var string $code
     *   ISO 4217 currency code.  Defaults to 'EUR' for the European euro.
     */
    public string $code;

    /**
     * @var int $id
     *   ISO 4217 currency number as an integer.
     *   Defaults to `978` for the European euro.
     */
    public int $id;

    /**
     * @var string $number
     *   ISO 4217 currency number as a string.
     *   This is a string representation of the `$id`.
     */
    public string $number;

    /**
     * @var string $symbol
     *   Currency sign.  Defaults to '€' for the euro sign.
     */
    public string $symbol;

    /**
     * @var int $precision
     *   Number of digits for cents et cetera.  Defaults to 2 digits.
     */
    public int $precision;

    /**
     * Creates currency.
     *
     * @param int|string $number ISO currency number.
     * @param string $code ISO currency nummber.
     * @param string $symbol Currency sign or currency symbol.
     * @param int $precision Number of digits for cents etc.
     */
    public function __construct(
        int|string $number = 978,
        string $code = 'EUR',
        int $precision = 2,
        string $symbol = '€',
    ) {
        $this->setCurrencyNumber($number);
        $this->setCurrencyCode($code);
        $this->setPrecision($precision);
        $this->symbol = trim($symbol);
    }

    /**
     * Converts the currency model to an alphanumeric ISO currency code.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->code;
    }

    /**
     * Sets the currency code.
     *
     * @param string $iso_currency_code
     *   Three-letter alphanumeric ISO 4217 currency code.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the currency code is not a string
     *   consisting of three uppercase characters.
     */
    private function setCurrencyCode(string $iso_currency_code): void
    {
        $iso_currency_code = trim($iso_currency_code);
        if (strlen($iso_currency_code) !== 3) {
            throw new ValueError();
        }

        $iso_currency_code = strtoupper($iso_currency_code);
        if (!ctype_upper($iso_currency_code)) {
            throw new ValueError();
        }

        $this->code = $iso_currency_code;
    }

    /**
     * Sets the numeric currency identifiers.
     *
     * @param int|string $iso_currency_number
     *   ISO 4217 currency number as an integer or a three-digit numeric
     *   string.
     *
     * @return void
     *
     * @throws \TypeError
     *   Throws an type error exception if the currency identifier is not an
     *   integer or could not be converted to an integer.
     */
    private function setCurrencyNumber(int|string $iso_currency_number): void
    {
        if (is_string($iso_currency_number)) {
            $iso_currency_number = (int) $iso_currency_number;
        }

        if ($iso_currency_number < 8 || $iso_currency_number > 999) {
            throw new ValueError();
        }

        $this->id = $iso_currency_number;
        $this->number = str_pad((string) $this->id, 3, '0', STR_PAD_LEFT);
    }

    /**
     * Sets the number of digits in amounts.
     *
     * @param int $number_of_digits
     *
     * @return void
     *
     * @throws \TypeError
     *   Throws a type error exception if the supplied number of digits
     *   is not an integer.
     *
     * @throws \ValueError
     *   Throws a value error exception if the supplied number of digits
     *   is negative or greater than 4.
     */
    private function setPrecision(int $number_of_digits): void
    {
        if ($number_of_digits < 0 || $number_of_digits > 4) {
            throw new ValueError();
        }
        $this->precision = $number_of_digits;
    }
}
