<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\Types\StructuredValue;

/**
 * GeoShape.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/GeoShape Schema.org type `GeoShape`
 * @see     https://developers.google.com/gmail/markup/reference/types/GeoShape
 * @version 0.2.0
 */
class GeoShape extends StructuredValue implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|StoreCore\Geo\PostalAddress|string $address
     *   Physical address of the item.
     */
    public PostalAddress|string|null $address = null;

    /**
     * @var StoreCore\Types\Country|string|null $addressCountry
     *   The country.  You can also provide the two-letter ISO 3166-1 alpha-2
     *   country code.
     */
    public Country|string|null $addressCountry = null;

    /**
     * @var null|string $box
     *   A box is the area enclosed by the rectangle formed by two points.  The
     *   first point is the lower corner, the second point is the upper corner.
     *   A box is expressed as two points separated by a space character.
     */
    public ?string $box = null;

    /**
     * @var null|string $circle
     *   A circle is the circular region of a specified radius centered at a
     *   specified latitude and longitude.  A circle is expressed as a pair
     *   followed by a radius in meters.
     */
    public ?string $circle = null;

    /**
     * @var null|int|float|string $elevation
     *   The elevation of a location (WGS 84).  Values may be of the form
     *   'NUMBER UNITOFMEASUREMENT' (e.g., '1,000 m', '3,200 ft') while numbers
     *   alone should be assumed to be a value in meters.
     */
    public int|float|string|null $elevation = null;

    /**
     * @var null|string $line
     *   A line is a point-to-point path consisting of two or more points.
     *   A line is expressed as a series of two or more point objects separated
     *   by space.
     */
    public ?string $line = null;

    /**
     * @var null|string $polygon
     *   A polygon is the area enclosed by a point-to-point path for which the
     *   starting and ending points are the same.  A polygon is expressed as a
     *   series of four or more space delimited points where the first and
     *   final points are identical.
     */
    public ?string $polygon = null;

    /**
     * @var null|string $postalCode
     *   The postal code, for example '94043'.
     */
    public ?string $postalCode = null;
}
