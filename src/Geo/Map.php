<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\CMS\AbstractCreativeWork;

/**
 * Map.
 *
 * @package StoreCore\Geo
 * @see     https://schema.org/Map Schema.org type `Map`
 * @version 0.2.0
 */
class Map extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|StoreCore\Geo\MapCategoryType $mapType
     *   Indicates the kind of `Map`, from the `MapCategoryType` enumeration.
     */
    public ?MapCategoryType $mapType = null;
}
