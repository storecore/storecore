<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \PDOException as DatabaseException;
use Psr\Container\ContainerInterface;
use StoreCore\Database\AbstractModel as DatabaseModel;
use StoreCore\Database\{ContainerException, NotFoundException};

use function \count;
use function \array_values;
use function \explode;
use function \implode;
use function \str_replace;
use function \strtoupper;
use function \trim;
use function \unset;

/**
 * State repository.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 */
class StateRepository extends DatabaseModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Finds a country subdivision.
     *
     * @param string $string
     *   Name or identifier of a state, province or similar country subdivision.
     *
     * @return StoreCore\Geo\State
     *   Returns a State object if one country subdivision is found.
     *
     * @throws Psr\Container\NotFoundExceptionInterface
     */
    public function find(string $string): State
    {
        $string = str_replace('_', '-', $string);
        if ($this->has($string)) {
            $array = $this->get($string);
            // Return first element of an array
            return array_values($array)[0];
        }

        $string = str_replace('%', '', $string);
        try {
            /*
                SELECT CONCAT_WS('-', `iso_alpha_two`, `iso_suffix`) AS `identifier`, `subdivision_name`
                  FROM `sc_country_subdivisions`
                 WHERE `subdivision_name` = :string
             */
            $statement = $this->Database->prepare("SELECT CONCAT_WS('-', `iso_alpha_two`, `iso_suffix`) AS `identifier`, `subdivision_name` FROM `sc_country_subdivisions` WHERE `subdivision_name` = :string");
            $statement->bindValue(':string', $string, \PDO::PARAM_STR);
            $statement->execute();
            if ($statement->rowCount() === 1) {
                $row = $statement->fetch(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                if ($row !== false) {
                    return new State($row['identifier'], $row['subdivision_name']);
                }
            }

            $string = str_replace(array('-', ' '), '_', $string);
            $string = '%' . $string . '%';
            /*
                SELECT CONCAT_WS('-', `iso_alpha_two`, `iso_suffix`) AS `identifier`, `subdivision_name`
                  FROM `sc_country_subdivisions`
                 WHERE `subdivision_name` LIKE :string
             */
            $statement = $this->Database->prepare(
                "SELECT CONCAT_WS('-', `iso_alpha_two`, `iso_suffix`) AS `identifier`, `subdivision_name` FROM `sc_country_subdivisions` WHERE `subdivision_name` LIKE :string"
            );
            $statement->bindValue(':string', $string, \PDO::PARAM_STR);
            $statement->execute();
            if ($statement->rowCount() === 1) {
                $row = $statement->fetch(\PDO::FETCH_ASSOC);
                if ($row !== false) {
                    $statement->closeCursor();
                    return new State($row['identifier'], $row['subdivision_name']);
                }
            }

            // Nothing found OR multiple results
            $statement->closeCursor();
            throw new NotFoundException();

        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new NotFoundException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function get(string $id): array
    {
        if (!is_string($id)) {
            throw new ContainerException('Invalid country subdivision identifier');
        }

        if (!$this->has($id)) {
            throw new NotFoundException('Country subdivision `' . $id . '` not found');
        }

        $id = trim($id);
        $id = strtoupper($id);
        $id = explode('-', $id);
        if (count($id) !== 2) throw new ContainerException();

        try {
            $statement = $this->Database->prepare(
                 "SELECT `language_code`, `subdivision_name`
                    FROM `sc_country_subdivisions`
                   WHERE `iso_alpha_two` = :iso_alpha_two
                     AND `iso_suffix` = :iso_suffix
                ORDER BY
                         CASE `language_code`
                           WHEN 'en' THEN 1
                           WHEN 'fr' THEN 2
                           WHEN 'es' THEN 3
                           WHEN 'pt' THEN 4
                           ELSE 5
                         END"
            );
            $statement->bindValue(':iso_alpha_two', $id[0], \PDO::PARAM_STR);
            $statement->bindValue(':iso_suffix', $id[1], \PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            $id = implode('-', $id);
            foreach ($result as $key => $value) {
                $state = new State($id, $value['subdivision_name']);
                $alternate_name = $this->getAlternateName($id, $value['subdivision_name']);
                if (!empty($alternate_name)) $state->alternateName = $alternate_name;
                $result[$value['language_code']] = $state;
                unset($result[$key]);
            }
            return $result;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * Gets an alternate name.
     *
     * @param string $id
     *   Country subdivision identifier, for example `DE-BY` for Bayern (`BY`)
     *   in Deutschland (`DE`).
     *
     * @var $name
     *   Name to find an alternative for.
     */
    public function getAlternateName(string $id, string $name): ?string
    {
        $id = trim($id);
        if (strlen($id) < 4) return null;

        $id = strtoupper($id);

        if ($id[2] !== '-') return null;
        $id = explode('-', $id, 2);

        
        $query = preg_replace('/\s+/', ' ',
            "SELECT `subdivision_name`
               FROM `sc_country_subdivisions`
              WHERE `iso_alpha_two` = :iso_alpha_two
                AND `iso_suffix` = :iso_suffix
                AND `subdivision_name` <> :subdivision_name
           ORDER BY
                    CASE `language_code`
                      WHEN 'en' THEN 1
                      WHEN 'fr' THEN 2
                      WHEN 'es' THEN 3
                      WHEN 'pt' THEN 4
                      ELSE 5
                    END
              LIMIT 1"
        );

        try {
            $statement = $this->Database->prepare($query);
            $statement->bindValue(':iso_alpha_two', $id[0], \PDO::PARAM_STR);
            $statement->bindValue(':iso_suffix', $id[1], \PDO::PARAM_STR);
            $statement->bindValue(':subdivision_name', $name, \PDO::PARAM_STR);
            if ($statement->execute() === false) return null;
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            return $result === false ? null : $result;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        if (!is_string($id)) return false;

        // Shortest ISO country subdivision code is 4 characters,
        // for example `ES-Z` for Zaragoza (Z) in Spain (ES).
        $id = trim($id);
        if (strlen($id) < 4) return false;

        // Third character must be - separator.
        $id = str_replace('_', '-', $id);
        if ($id[2] != '-') return false;

        // ISO code consists of two uppercase strings.
        $id = strtoupper($id);
        $id = explode('-', $id);
        if (count($id) !== 2) return false;

        // First two characters must be uppercase country code.
        if (!ctype_upper($id[0])) return false;

        try {
            $statement = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_country_subdivisions` WHERE `iso_alpha_two` = :iso_alpha_two AND `iso_suffix` = :iso_suffix'
            );
            $statement->bindValue(':iso_alpha_two', $id[0], \PDO::PARAM_STR);
            $statement->bindValue(':iso_suffix', $id[1], \PDO::PARAM_STR);
            $statement->execute();
            $count = (int) $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            return $count >= 1 ? true : false;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * Lists country subdivisions.
     *
     * @param Country $country
     *   Limits the listed country subdivisions to the specified country.
     *
     * @return array
     *   Returns an array of `State` objects.
     * 
     * @throws Psr\Container\NotFoundExceptionInterface
     *   Exception throws if no country subdivisions were found.
     * 
     * @uses StoreCore\Database\NotFoundException::__contruct
     *
     * @throws Psr\Container\ContainerExceptionInterface
     *   Exception thrown on other errors than a `NotFoundException`.
     *
     * @uses StoreCore\Database\ContainerException::__contruct
     */
    public function list(Country $country): array|null
    {
        try {
            $statement = $this->Database->prepare(
                 "SELECT CONCAT_WS('-', `iso_alpha_two`, `iso_suffix`) AS `identifier`, `subdivision_name` as `name`
                    FROM `sc_country_subdivisions`
                   WHERE `iso_alpha_two` = :iso_alpha_two
                ORDER BY `name` ASC"
            );
            $statement->bindValue(':iso_alpha_two', $country->identifier, \PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
            $statement->closeCursor();
            $statement = null;
            if ($result === false || empty($result)) {
                throw new NotFoundException();
            }

            foreach ($result as $identifier => $name) {
                $alternate_name = $this->getAlternateName($identifier, $name);
                if (empty($alternate_name)) {
                    $result[$identifier] = new State($identifier, $name);
                } else {
                    $result[$identifier] = new State($identifier, $name, $alternate_name);
                }
            }
           return $result;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }
}
