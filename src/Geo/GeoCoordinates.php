<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \JsonSerializable;

/**
 * Geographic coordinates.
 *
 * @package StoreCore\Geo
 * @see     https://schema.org/GeoCoordinates Schema.org type `GeoCoordinates`
 * @see     https://en.wikipedia.org/wiki/World_Geodetic_System World Geodetic System (WGS)
 * @version 1.0.0
 */
class GeoCoordinates implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates geographic coordinates.
     *
     * @param float $latitude
     *   The latitude of a location as a read-only float,
     *   for example `37.42242` (WGS 84).
     *
     * @param float $longitude
     *   The longitude of a location as a read-only float,
     *   for example `-122.08585` (WGS 84).
     *
     * @param null|int|string|PostalCode $postalCode
     *   OPTIONAL postal code associated with this geographic location.
     */
    public function __construct(
        public readonly float $latitude,
        public readonly float $longitude,
        public null|int|string|PostalCode $postalCode = null,
    ) {
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@type'     => 'GeoCoordinates',
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
        ];

        if (!empty($this->postalCode)) {
            $result['postalCode'] = (string) $this->postalCode;
        }

        return $result;
    }
}
