<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \TypeError, \ValueError;
use \JsonSerializable;

use function \empty;
use function \mb_strlen;
use function \is_string;
use function \trim;
use function \str_replace;
use function \strtoupper;

/**
 * State.
 *
 * This class is a value object model for country subdivisions such as states,
 * provinces, departments, territories, districts, etc.  In some countries
 * these subdivisions are required for valid addresses.
 *
 * @package StoreCore\OML
 * @see     https://schema.org/State Schema.org type `State`
 * @version 1.0.0
 */
class State implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $identifier
     *   ISO 3166-2 country subdivision identifier as an uppercase string.
     */
    public readonly string $identifier;

    /**
     * @var null|string $name
     *   OPTIONAL name of the state, province, etc.
     */
    public ?string $name = null;

    /**
     * @var null|string $alternateName
     *   OPTIONAL alternate name of the state, province, etc.  This usually
     *   is the English name of a geographic region if the `name` is set in
     *   another language.
     */
    public ?string $alternateName = null;

    /**
     * Creates a state, province, etc.
     *
     * @param string $identifier
     *   Unique ISO 3166-2 identifier of a country subdivision.
     *
     * @param null|string $name
     *   OPTIONAL name of the country subdivision.
     *
     * @param null|string $alternate_name
     *   OPTIONAL alternate name of the country subdivision.  The alternate
     *   name is only set when it is different from the name.
     */
    public function __construct(
        string $identifier,
        ?string $name = null,
        ?string $alternate_name = null,
    ) {
        $this->setIdentifier($identifier);
        if (!empty($name)) {
            $this->name = $name;
            if (!empty($alternate_name) && $alternate_name !== $this->name) {
                $this->alternateName = $alternate_name;
            }
        }
    }

    /**
     * Gets the Schema.org properties of a State.
     *
     * @param void
     *
     * @return array
     *   Returns data which can be serialized by `json_encode()`, which is
     *   a value of any type other than a resource.
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context'   => 'https://schema.org',
            '@type'      => 'State',
            'identifier' => $this->identifier,
        ];

        if (!empty($this->name)) {
            $result['name'] = $this->name;
            if (!empty($this->alternateName) && $this->alternateName !== $this->name) {
                $result['alternateName'] = $this->alternateName;
            }
        }

        return $result;
    }

    /**
     * Sets the country subdivision identifier.
     *
     * @param string $identifier
     *    ISO 3166-2 country subdivision ID.
     *
     * @return void
     * 
     * @throws \ValueError
     *   Throws an value error exception on an invalid ISO 3166-2 code.
     *   This method does not check if the ISO code actually exists
     *   nor if it is used in addresses.
     */
    private function setIdentifier(string $identifier): void
    {
        if (!is_string($identifier)) throw new TypeError();
        $identifier = trim($identifier);
        $identifier = strtoupper($identifier);

        $identifier = str_replace('_', '-', $identifier);
        if ($identifier[2] !== '-') {
            throw new ValueError();
        }

        if (mb_strlen($identifier) < 4) {
            throw new ValueError();
        }

        $this->identifier = $identifier;
    }
}
