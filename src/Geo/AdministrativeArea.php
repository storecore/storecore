<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

/**
 * Schema.org administrative area.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/AdministrativeArea Schema.org type `AdministrativeArea`
 * @version 1.0.0
 */
class AdministrativeArea extends Place implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
