<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \PDOException as DatabaseException;
use Psr\Container\ContainerInterface;
use StoreCore\Database\AbstractModel;
use StoreCore\Database\NotFoundException;

use function \array_flip;
use function \array_key_exists;
use function \ctype_alpha;
use function \empty;
use function \is_array;
use function \is_int;
use function \is_numeric;
use function \is_string;
use function \preg_replace;
use function \str_replace;
use function \strlen;
use function \strtoupper;
use function \time;
use function \trim;
use function \unset;

interface_exists(ContainerInterface::class);
class_exists(AbstractModel::class);
class_exists(Country::class);

/**
 * Country repository.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 */
class CountryRepository extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var array $cache
     *   Temporary cache for country objects.
     */
    private array $cache = [];

    /**
     * Finds a country by country name.
     *
     * @param string $country_name
     *   Partial or full country name or an ISO country code.
     *
     * @return StoreCore\Geo\Country
     * 
     * @throws Psr\Container\NotFoundExceptionInterface
     */
    public function find(string $country_name): Country
    {
        // Remove wildcard characters for LIKE statements
        $country_name = str_replace(['%', '_', '*', '?'], '', $country_name);
        $country_name = trim($country_name);
        $country_name = preg_replace('/\s+/', ' ', $country_name);
        if (empty($country_name)) throw new NotFoundException();

        /* If `has()` returns `true`, the “country name”
         * is actually an acceptable unique country ID.
         */
        if ($this->has($country_name)) return $this->get($country_name);

        try {
            /*
                SELECT `country_id`
                  FROM `sc_countries`
                 WHERE `global_country_name` LIKE :country_name
                 LIMIT 1
             */
            $sth = $this->Database->prepare(
                'SELECT `country_id` FROM `sc_countries` WHERE `global_country_name` LIKE :country_name LIMIT 1'
            );
            $sth->bindValue(':country_name', $country_name, \PDO::PARAM_STR);
            $sth->execute();
            $row = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            if ($row !== false) return $this->get($row['country_id']);

            /*
                   SELECT `n`.`country_id`, `c`.`global_country_name`, `n`.`local_country_name`
                     FROM `sc_country_names` AS `n`
                LEFT JOIN `sc_countries` AS `c`
                       ON `n`.`country_id` = `c`.`country_id`
                    WHERE `n`.`language_id` <> 'en-GB'
                      AND `n`.`local_country_name` LIKE '…'
                    LIMIT 1
             */
            $sth = $this->Database->prepare("SELECT `n`.`country_id`, `c`.`global_country_name`, `n`.`local_country_name` FROM `sc_country_names` AS `n` LEFT JOIN `sc_countries` AS `c` ON `n`.`country_id` = `c`.`country_id` WHERE `n`.`language_id` <> 'en-GB' AND `n`.`local_country_name` LIKE :country_name LIMIT 1");
            $sth->bindValue(':country_name', $country_name, \PDO::PARAM_STR);
            $sth->execute();
            $row = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            if ($row !== false) {
                $result = new Country($row['country_id'], $row['global_country_name'], $row['local_country_name']);
                $this->cache[$result->identifier] = $result;
                return $result;
            }

            /*
                   SELECT `n`.`country_id`, `c`.`global_country_name`, `n`.`local_country_name`
                     FROM `sc_country_names` AS `n`
                LEFT JOIN `sc_countries` AS `c`
                       ON `n`.`country_id` = `c`.`country_id`
                    WHERE `n`.`local_country_name` LIKE '%…%'
                 ORDER BY
                          CASE `n`.`language_id`
                            WHEN 'en-GB' THEN 1
                            WHEN 'fr-FR' THEN 2
                            WHEN 'es-ES' THEN 3
                            WHEN 'pt-PT' THEN 4
                            ELSE 5
                          END
             */
            $sth = $this->Database->prepare("SELECT `n`.`country_id`, `c`.`global_country_name`, `n`.`local_country_name` FROM `sc_country_names` AS `n` LEFT JOIN `sc_countries` AS `c` ON `n`.`country_id` = `c`.`country_id` WHERE `n`.`local_country_name` LIKE :country_name ORDER BY CASE `n`.`language_id` WHEN 'en-GB' THEN 1 WHEN 'fr-FR' THEN 2 WHEN 'es-ES' THEN 3 WHEN 'pt-PT' THEN 4 ELSE 5 END");
            $sth->bindValue(':country_name', '%' . $country_name . '%', \PDO::PARAM_STR);
            $sth->execute();
            $row = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            $sth = null;
            unset($sth);
            if ($row !== false) {
                $result = new Country($row['country_id'], $row['global_country_name'], $row['local_country_name']);
                $this->cache[$result->identifier] = $result;
                return $result;
            }

            throw new NotFoundException('Country name not found: ' . $country_name, time());
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new NotFoundException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * Gets a country from the repository
     *
     * @param string|int $id
     *   Numeric or alphanumeric country identifier.
     * 
     * @return StoreCore\Geo\Country
     */
    public function get($id): Country
    {
        if (is_string($id) && is_numeric($id)) {
            $id = (int) $id;
        }

        if (!is_string($id) && !is_int($id)) {
            throw new NotFoundException(
                'Country ID passed to ' . __METHOD__ . '() must be of the type string or integer, ' . gettype($id) . ' given'
            );
        }

        if (is_string($id)) {
            $id = trim($id);
            $id = strtoupper($id);
        }

        if (array_key_exists($id, $this->cache)) {
            return $this->cache[$id];
        }

        $sql = 'SELECT `country_id`, `iso_alpha_two`, `global_country_name` FROM `sc_countries` ';
        if (is_int($id)) {
            $sql .= 'WHERE `country_id` = :id';
        } elseif (strlen($id) === 2) {
            $sql .= 'WHERE `iso_alpha_two` = :id';
        } elseif (strlen($id) === 3) {
            $sql .= 'WHERE `iso_alpha_three` = :id';
        } else {
            throw new NotFoundException('Invalid country ID: ' . $id);
        }

        try {
            $sth = $this->Database->prepare($sql);
            if (is_int($id)) {
                $sth->bindValue(':id', $id, \PDO::PARAM_INT);
            } else {
                $sth->bindValue(':id', $id, \PDO::PARAM_STR);
            }
            $sth->execute();
            $row = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            if (!is_array($row)) {
                throw new NotFoundException(
                    'No entry was found for this country identifier: ' . $id
                );
            }
            $country = new Country($row['iso_alpha_two'], $row['global_country_name']);
            $this->cache[$id] = $country;
            return $country;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * Gets an ISO country number.
     *
     * @param string|StoreCore\Geo\Country $country
     *   Country object or country identifier as a string.
     *
     * @return int|null
     *   ISO 3166-2 country number as an integer without leading zero’s
     *   or `null` if the country number could not be found.
     */
    final public function getCountryNumber(string|Country $country): ?int
    {
        if ($country instanceof Country) $country = $country->identifier;

        $country = trim($country);
        $strlen = strlen($country);

        if ($strlen === 2 && ctype_alpha($country)) {
            $country_numbers = array_flip(CountryCodes::COUNTRY_NUMBERS);
            if (isset($country_numbers[strtoupper($country)])) {
                return $country_numbers[strtoupper($country)];
            }
        }

        $sql = 'SELECT `country_id` FROM `sc_countries` WHERE ';
        if ($strlen === 2) {
            $sql .= '`iso_alpha_two` = :country';
        } elseif ($strlen === 3) {
            $sql .= '`iso_alpha_three` = :country';
        } else {
            return null;
        }

        try {
            $sth = $this->Database->prepare($sql);
            $sth->bindValue(':country', $country, \PDO::PARAM_STR);
            $sth->execute();
            $row = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            return is_array($row) ? (int) $row['country_id'] : null;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return null;
        }
    }

    /**
     * Checks if a country exists.
     *
     * @param string|int $id
     *   Unique country identifier as a string or an integer.
     *
     * @return bool
     *   Return `true` if the country exists, otherwise `false`.
     */
    public function has($id): bool
    {
        if (is_string($id) && is_numeric($id)) {
            $id = (int) $id;
        }

        if (!is_string($id) && !is_int($id)) {
            return false;
        }

        if (is_string($id)) {
            $id = trim($id);
            $id = strtoupper($id);
        }

        if (array_key_exists($id, $this->cache)) {
            return true;
        }

        // Support three types of country IDs.
        $sql = 'SELECT COUNT(*) FROM `sc_countries` WHERE ';
        if (is_int($id)) {
            $sql .= '`country_id` = :id';
        } elseif (strlen($id) === 2) {
            $sql .= '`iso_alpha_two` = :id';
        } elseif (strlen($id) === 3) {
            $sql .= '`iso_alpha_three` = :id';
        } else {
            return false;
        }

        try {
            $sth = $this->Database->prepare($sql);
            if (is_int($id)) {
                $sth->bindValue(':id', $id, \PDO::PARAM_INT);
            } else {
                $sth->bindValue(':id', $id, \PDO::PARAM_STR);
            }
            $sth->execute();
            $count = (int) $sth->fetchColumn(0);
            $sth->closeCursor();
            return $count === 1 ? true : false;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }
}
