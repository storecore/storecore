<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\Types\Thing;

/**
 * Place.
 *
 * @api
 * @package StoreCore\OML
 * @see     https://schema.org/Place Schema.org type `Place`
 * @version 0.5.0
 */
class Place extends Thing implements \JsonSerializable
{
    use HasMapTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.5.0';

    /**
     * @var null|StoreCore\Geo\GeoCoordinates|StoreCore\Geo\GeoShape $geo
     *   The geographic coordinates of the `Place`.
     */
    public null|GeoCoordinates|GeoShape $geo = null;
}
