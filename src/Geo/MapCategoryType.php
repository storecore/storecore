<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

/**
 * Map category type.
 *
 * A backed enumeration of several kinds of `Map`.  Instances of
 * `MapCategoryType` MAY appear as a value for the `mapType` property
 * of a `Map`.
 *
 * @package StoreCore\Geo
 * @see     https://schema.org/AdultOrientedEnumeration Schema.org `MapCategoryType` enumeration
 * @version 23.0.0
 */
enum MapCategoryType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer), compatible with Schema.org v23.0
     *   from October 17, 2023.
     */
    public const string VERSION = '23.0.0';

    case ParkingMap = 'https://schema.org/ParkingMap';
    case SeatingMap = 'https://schema.org/SeatingMap';
    case TransitMap = 'https://schema.org/TransitMap';
    case VenueMap   = 'https://schema.org/VenueMap';    
}
