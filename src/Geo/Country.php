<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \JsonSerializable;
use \Stringable;
use \ValueError;
use StoreCore\Types\CountryCode;

/**
 * Country.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 *
 * @see https://en.wikipedia.org/wiki/ISO_3166-1
 *      ISO 3166-1
 *
 * @see https://www.iso.org/iso-3166-country-codes.html
 *      ISO 3166 Country Codes
 *
 * @see https://www.iso.org/standard/72482.html
 *      ISO 3166-1:2020: Codes for the representation of names of countries
 *      and their subdivisions — Part 1: Country code
 *
 * @see https://blog.ansi.org/2020/10/iso-3166-1-2020-country-codes-changes/
 *      ISO 3166-1:2020 – Country Codes Standard Changes, by Brad Kelechava
 */
readonly class Country implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|string $alternateName
     *   An OPTIONAL alias for the item, generally used for an alternate
     *   country name in different language.
     */
    public ?string $alternateName;

    /**
     * @var string $identifier
     *   ISO 3166-1 country code as two uppercase letters.
     */
    public string $identifier;

    /**
     * @var string $name
     *   Country name.
     */
    public string $name;

    /**
     * Creates a country.
     *
     * @param StoreCore\Types\CountryCode|string|int $identifier
     *   ISO 3166-1 country code as a value object, a string, or an integer.
     *
     * @param string $name
     *   Country name.
     *
     * @param null|string $alternate_name
     *   OPTIONAL alternate country name.
     */
    public function __construct(CountryCode|string|int $identifier, string $name, ?string $alternate_name = null)
    {
        $this->setIdentifier($identifier);
        $this->name = $name;

        if (!empty($alternate_name) && $alternate_name !== $this->name) {
            $this->alternateName = $alternate_name;
        } else {
            $this->alternateName = null;
        }
    }

    /**
     * Converts the country object to a country code.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->identifier;
    }

    /**
     * Serializes the object to a value that can be serialized natively by `json_encode()`.
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'Country',
            'identifier' => $this->identifier,
            'name' => $this->name,
        ];

        if ($this->alternateName !== null) {
            $result['alternateName'] = $this->alternateName;
        }

        return $result;
    }

    /**
     * Sets the counntry identifier.
     *
     * @internal
     * @param StoreCore\Types\CountryCode|string|int $identifier
     * @return void
     * @uses \StoreCore\Geo\CountryCodes::createCountryCode
     */
    private function setIdentifier(CountryCode|string|int $identifier): void
    {
        if (!($identifier instanceof CountryCode)) {
            $identifier = CountryCodes::createCountryCode($identifier);
            if ($identifier === null) throw new ValueError('Invalid country code', time());
        }

        $this->identifier = (string) $identifier;
    }
}
