<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \JsonSerializable;

/**
 * Airport.
 *
 * @package StoreCore\Geo
 * @see     https://schema.org/Airport Schema.org type `Airport`
 * @version 0.1.0
 */
class Airport extends CivicStructure implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
