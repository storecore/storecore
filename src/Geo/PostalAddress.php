<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \Exception;
use \ValueError;
use Psr\Container\ContainerInterface;
use StoreCore\{ContainerException, NotFoundException};
use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Registry;
use StoreCore\Types\CountryCode;

use function \array_filter;
use function \array_key_exists;
use function \count;
use function \ctype_digit;
use function \empty;
use function \gzdeflate;
use function \gzinflate;
use function \is_array;
use function \is_int;
use function \is_string;
use function \json_encode;
use function \strlen;
use function \strtoupper;
use function \trim;
use function \unset;

/**
 * Postal address model.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/PostalAddress Schema.org type `PostalAddress`
 */
class PostalAddress implements ContainerInterface, IdentityInterface
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $mappings
     *   API mappings for address properties in the `$attributes` array.
     *   For example, a `locality` is often called a `city`, even though
     *   it may be a town, village, etc.  The array is sorted alphabetically
     *   by key in the `ksort()` key sort order.
     */
    protected const MAPPINGS = [
        'Addition' => 'house_number_addition',
        'addressCountry' => 'country_id',
        'addressLocality' => 'locality',
        'city' => 'locality',
        'City' => 'locality',
        'country' => 'country_id',
        'Country' => 'country_id',
        'Countrycode' => 'country_id',
        'CountryIso' => 'country_id',
        'countryIso2' => 'country_id',
        'CountryIso2' => 'country_id',
        'CountryIso3' => 'country_id',
        'dateCreated' => 'date_created',
        'HouseNr' => 'house_number',
        'HouseNrExt' => 'house_number_addition',
        'housenumber' => 'house_number',
        'houseNumber' => 'house_number',
        'HouseNumber' => 'house_number',
        'houseNumberAddition' => 'house_number_addition',
        'HouseNumberAddition' => 'house_number_addition',
        'identifier' => 'address_uuid',
        'Latitude' => 'latitude',
        'Longitude' => 'longitude',
        'postalcode' => 'postal_code',
        'postalCode' => 'postal_code',
        'PostalCode' => 'postal_code',
        'postOfficeBoxNumber' => 'post_office_box_number',
        'Street' => 'street_name',
        'streetName' => 'street_name',
        'Zipcode' => 'postal_code',
    ];

    /**
     * @var array $attributes
     *   Class properties stored in individual database columns.
     *   The property/attribute names match column names of a database record.
     *   The `Address::__set()` method accepts synonyms and abbreviations.
     */
    protected array $attributes = [
        'address_uuid' => null,

        'global_location_number' => null,

        'post_office_box_number' => null,

        'country_id'            => null,
        'country_subdivision'   => null,
        'postal_code'           => null,
        'house_number'          => null,
        'house_number_addition' => null,

        'name'        => null,
        'street_name' => null,
        'locality'    => null,

        'latitude'  => null,
        'longitude' => null,

        'opening_hours' => null,

        'date_created'   => null,
        'date_modified'  => null,
        'date_validated' => null,
        'date_deleted'   => null,
    ];

    /**
     * @var array|null $extendedAttributes
     *   If a property is not included in the default `$attributes` and it
     *   cannot be mapped to these attributes, it MAY be stored as an
     *   extra property in an entity-attribute-value (EAV) style.
     */
    protected ?array $extendedAttributes = null;

    /**
     * Creates an address.
     *
     * @param array|null $params
     *   OPTIONAL address attributes as key/value pairs.  NULL values are
     *   ignored.
     */
    public function __construct(?array $params = null)
    {
        if ($params !== null) {
            // Handle indexed array with one element as full address.
            if (is_array($params) && 1 === count($params) && array_key_exists(0, $params)) {
                $params = $params[0];
            }

            // First set the country ID, if it exists, because some other
            // properties or setters may use country-specific settings.
            if (array_key_exists('country_id', $params)) {
                $this->__set('country_id', $params['country_id']);
                unset($params['country_id']);
            }

            foreach ($params as $key => $value) {
                if (is_string($key) && $value !== null) {
                    $this->__set($key, $value);
                }
            }
        }

        if ($this->attributes['date_created'] === null) {
            $this->attributes['date_created'] = gmdate('Y-m-d h:m:s');
        }
    }

    /**
     * Generic property getter.
     *
     * @param string $name
     * @return mixed|null
     */
    public function __get(string $name): mixed
    {
        if ($name === 'identifier') {
            return $this->hasIdentifier() ? (string) $this->getIdentifier() : null;
        }

        if (array_key_exists($name, self::MAPPINGS)) {
            $name = self::MAPPINGS[$name];
        }

        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        } elseif ($this->extendedAttributes !== null && array_key_exists($name, $this->extendedAttributes)) {
            return $this->extendedAttributes[$name];
        } else {
            return null;
        }
    }

    /**
     * Triggered by calling `isset()` or `empty()` on inaccessible (`protected`
     * or `private`) or non-existing properties.
     *
     * @param string $name
     * @return bool
     * @uses has()
     */
    public function __isset(string $name): bool
    {
        return $this->has($name);
    }

    /**
     * Generic property setter.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        $name = trim($name);
        if (array_key_exists($name, self::MAPPINGS)) {
            $name = self::MAPPINGS[$name];
        }

        if ($value instanceof PostalCode) {
            $value = (string) $value;
        }

        // Get latitude and longitude from array with coordinates.
        if ($name === 'Coordinates' && is_array($value)) {
            if (array_key_exists('Latitude', $value)) {
                $this->__set('latitude', $value['Latitude']);
            }
            if (array_key_exists('Longitude', $value)) {
                $this->__set('longitude', $value['Longitude']);
            }
        }

        // Set or reset empty string values to null.
        if (is_string($value)) {
            $value = trim($value);
            if (empty($value)) $value = null;
        }

        switch ($name) {
            case 'address_id':
            case 'address_uuid':
                $this->setIdentifier($value);
                break;
            case 'country_id':
                $this->setCountry($value);
                break;
            case 'house_number':
                $this->attributes[$name] = (string) $value;
                break;
            case 'house_number_addition':
                if (is_string($value)) {
                    $value = trim($value, '-');
                }
                $this->attributes[$name] = $value;
                break;
            case 'Province':
                $this->setState($value);
                break;
            default:
                if (array_key_exists($name, $this->attributes)) {
                    $this->attributes[$name] = $value;
                } else {
                    $this->extendedAttributes[$name] = $value;
                }
        }
    }

    /**
     * Creates an address value object from a database record.
     *
     * @param array $array
     *   The array with address data.
     *
     * @return StoreCore\Geo\Address
     *   Returns an instance of `Address` containing the array content.
     */
    public static function fromArray(array $array): Address
    {
        $array = array_filter($array);

        // Uncompress optional JSON array with additional address properties.
        if (array_key_exists('eav', $array)) {
            $eav = gzinflate($array['eav']);
            if ($eav === false) {
                unset($array['eav']);
            } else {
                $eav = json_decode($eav, true);
                if ($eav === null) {
                    unset($array['eav']);
                } else {
                    $array['eav'] = $eav;
                }
            }
        }

        return new Address($array);
    }

    /**
     * @inheritDoc
     */
    public function get(string $id)
    {
        if ($this->has($id) !== true) throw new NotFoundException();
        $result = $this->__get($id);
        if ($result === null) throw new ContainerException();
        return $result;
    }

    /**
     * Gets the address country.
     *
     * @param void
     * 
     * @return StoreCore\Geo\Country|null
     *   Returns the address country as a value object or `null` if the country
     *   has not been set or could not be fetched.
     * 
     * @uses StoreCore\Geo\CountryRepository::get()
     *   Fetches a country from the country repository.
     */
    public function getCountry(): ?Country
    {
        if (empty($this->attributes['country_id'])) {
            return null;
        }

        try {
            $registry = Registry::getInstance();
            $repository = new CountryRepository($registry);
            $country = $repository->get($this->attributes['country_id']);
            return $country;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        if (array_key_exists($id, self::MAPPINGS)) {
            $id = self::MAPPINGS[$id];
        }

        if (array_key_exists($id, $this->attributes) && $this->attributes[$id] !== null) {
            return true;
        }

        if ($this->extendedAttributes === null) {
            return false;
        }

        if (
            array_key_exists($id, $this->extendedAttributes)
            && $this->extendedAttributes[$id] !== null
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the country ID.
     *
     * @param int|string|StoreCore\Geo\Country|StoreCore\Types\CountryCode
     *   Country object or country code as a string, integer or value object.
     * 
     * @return void
     *
     * @uses StoreCore\Geo\Country::getIdentifier()
     *   If the `$country` parameter is an instance of a `Country` value object
     *   the `getIdentifier()` method is called to get the numeric or
     *   alphanumeric country ID.
     *
     * @uses StoreCore\OML\CountryRepository::getCountryNumber()
     *   The `getCountryNumber()` method is used to convert a alphanumeric
     *   country code to a numeric country identifier.
     */
    public function setCountry(int|string|Country|CountryCode $country): void
    {
        if ($country instanceof Country) {
            $country = $country->identifier;
        } elseif ($country instanceof CountryCode) {
            $country = (string) $country;
        } elseif (is_string($country) && ctype_digit($country)) {
            $country = (int) $country;
        }

        if (!is_int($country)) {
            $country_number = CountryCodes::getCountryNumber($country);
            if (is_int($country_number)) {
                $country = $country_number;
            } else {
                try {
                    $registry = Registry::getInstance();
                    $repository = new CountryRepository($registry);
                    $country = $repository->find($country);
                    $this->setCountry($country);
                } catch (NotFoundException $e) {
                    throw new ValueError('Invalid country code', time());
                }    
            }
        }

        if (is_int($country)) {
            $this->attributes['country_id'] = $country;
        } else {
            throw new ValueError('Invalid country code', time());
        }
    }

    /**
     * Sets an administrative subdivision within a country.
     *
     * @param int|string|State $country_subdivision
     *   ISO 3166-2 country subdivision code or a State object.
     *
     * @return void
     *
     * @see https://en.wikipedia.org/wiki/List_of_administrative_divisions_by_country
     *      List of administrative divisions by country
     *
     * @throws \InvalidArgumentException
     *   Throws an SPL invalid argument exception if the provided country subdivision
     *   is not an integer, string or State.
     *
     * @throws \DomainException
     *   Throws a domain exception if the ISO 3166-2 country subdivision code
     *   does not consist of two digits or one, two, or three uppercase letters.
     */
    public function setCountrySubdivision(int|string|State $country_subdivision): void
    {
        if (is_int($country_subdivision)) {
            $country_subdivision = (string) $country_subdivision;
        }

        if ($country_subdivision instanceof State) {
            $country_subdivision = $country_subdivision->identifier;
        }

        if (!is_string($country_subdivision)) {
            throw new \InvalidArgumentException(
                'Argument #1 ($country_subdivision) must be of type integer, string or State, ' . gettype($country_subdivision) . ' given'
            );
        }

        // Add leading 0 on a single digit
        $country_subdivision = trim($country_subdivision);
        if (strlen($country_subdivision) === 1 && ctype_digit($country_subdivision)) {
            $country_subdivision = '0' . $country_subdivision;
        }

        $country_subdivision = strtoupper($country_subdivision);
        if (strlen($country_subdivision) >= 4) {
            $country_subdivision = str_replace('_', '-', $country_subdivision);
            if ($country_subdivision[2] === '-') {
                $country_subdivision = explode('-', $country_subdivision, 2);
                if ($this->getCountry() === null) {
                    $this->setCountry($country_subdivision[0]);
                }
                $country_subdivision = $country_subdivision[1];
            }
        }

        if (
            strlen($country_subdivision) > 3
            || (ctype_digit($country_subdivision) && strlen($country_subdivision) > 2)
        ) {
            throw new \DomainException(
                'Invalid ISO 3166-2 country subdivision code: ' . $country_subdivision
            );
        }

        $this->attributes['country_subdivision'] = $country_subdivision;
    }

    /**
     * Sets a country subdivision by its name.
     *
     * @param string $name
     *   Name of a state, province, or similar country subdivision.
     *
     * @return void
     * 
     * @uses StoreCore\Geo\StateRepository::find()
     */
    private function setState(string $name): void
    {
        try {
            $registry = Registry::getInstance();
            $repository = new StateRepository($registry);
            $state = $repository->find($name);
            $this->setCountrySubdivision($state);
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * Converts the address to an array for database storage.
     *
     * @internal
     * @param void
     * @return array
     */
    public function toArray(): array
    {
        $array = array_filter($this->attributes);

        // Save additional properties as compressed JSON string.
        if ($this->extendedAttributes !== null) {
            $eav = array_filter($this->extendedAttributes);
            if (!empty($eav)) {
                $eav = json_encode($eav);
                $eav = gzdeflate($eav, 9);
                $array['eav'] = $eav;
            }
        }

        return $array;
    }
}
