<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use JsonSerializable;

/**
 * Museum.
 *
 * @package StoreCore\Geo
 * @see     https://schema.org/Museum Schema.org type `Museum`
 * @version 1.0.0
 */
class Museum extends CivicStructure implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
