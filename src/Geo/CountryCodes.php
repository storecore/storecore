<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use \Stringable;
use StoreCore\Types\CountryCode;

use function \array_key_exists;
use function \array_flip;
use function \in_array;
use function \is_int;
use function \is_numeric;
use function \isset;
use function \strlen;
use function \strtoupper;

class_exists(CountryCode::class);

/**
 * Country code repository and factory.
 *
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes List of ISO 3166 country codes
 * @version 1.0.0
 */
readonly class CountryCodes
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var array COUNTRY_CODES
     *   ISO 3166-1 three-letter alpha-3 and two-letter alpha-2 country codes.
     */
    public const COUNTRY_CODES = [
        'ABW' => 'AW', 'AFG' => 'AF', 'AGO' => 'AO', 'AIA' => 'AI', 'ALA' => 'AX', 'ALB' => 'AL', 'AND' => 'AD', 'ARE' => 'AE', 'ARG' => 'AR', 'ARM' => 'AM', 'ASM' => 'AS', 'ATA' => 'AQ', 'ATF' => 'TF', 'ATG' => 'AG', 'AUS' => 'AU', 'AUT' => 'AT', 'AZE' => 'AZ',
        'BDI' => 'BI', 'BEL' => 'BE', 'BEN' => 'BJ', 'BES' => 'BQ', 'BFA' => 'BF', 'BGD' => 'BD', 'BGR' => 'BG', 'BHR' => 'BH', 'BHS' => 'BS', 'BIH' => 'BA', 'BLM' => 'BL', 'BLR' => 'BY', 'BLZ' => 'BZ', 'BMU' => 'BM', 'BOL' => 'BO', 'BRA' => 'BR', 'BRB' => 'BB', 'BRN' => 'BN', 'BTN' => 'BT', 'BVT' => 'BV', 'BWA' => 'BW',
        'CAF' => 'CF', 'CAN' => 'CA', 'CCK' => 'CC', 'CHE' => 'CH', 'CHL' => 'CL', 'CHN' => 'CN', 'CIV' => 'CI', 'CMR' => 'CM', 'COD' => 'CD', 'COG' => 'CG', 'COK' => 'CK', 'COL' => 'CO', 'COM' => 'KM', 'CPV' => 'CV', 'CRI' => 'CR', 'CUB' => 'CU', 'CUW' => 'CW', 'CXR' => 'CX', 'CYM' => 'KY', 'CYP' => 'CY', 'CZE' => 'CZ',
        'DEU' => 'DE', 'DJI' => 'DJ', 'DMA' => 'DM', 'DNK' => 'DK', 'DOM' => 'DO', 'DZA' => 'DZ',
        'ECU' => 'EC', 'EGY' => 'EG', 'ERI' => 'ER', 'ESH' => 'EH', 'ESP' => 'ES', 'EST' => 'EE', 'ETH' => 'ET',
        'FIN' => 'FI', 'FJI' => 'FJ', 'FLK' => 'FK', 'FRA' => 'FR', 'FRO' => 'FO', 'FSM' => 'FM',
        'GAB' => 'GA', 'GBR' => 'GB', 'GEO' => 'GE', 'GGY' => 'GG', 'GHA' => 'GH', 'GIB' => 'GI', 'GIN' => 'GN', 'GLP' => 'GP', 'GMB' => 'GM', 'GNB' => 'GW', 'GNQ' => 'GQ', 'GRC' => 'GR', 'GRD' => 'GD', 'GRL' => 'GL', 'GTM' => 'GT', 'GUF' => 'GF', 'GUM' => 'GU', 'GUY' => 'GY',
        'HKG' => 'HK', 'HMD' => 'HM', 'HND' => 'HN', 'HRV' => 'HR', 'HTI' => 'HT', 'HUN' => 'HU',
        'IDN' => 'ID', 'IMN' => 'IM', 'IND' => 'IN', 'IOT' => 'IO', 'IRL' => 'IE', 'IRN' => 'IR', 'IRQ' => 'IQ', 'ISL' => 'IS', 'ISR' => 'IL', 'ITA' => 'IT',
        'JAM' => 'JM', 'JEY' => 'JE', 'JOR' => 'JO', 'JPN' => 'JP',
        'KAZ' => 'KZ', 'KEN' => 'KE', 'KGZ' => 'KG', 'KHM' => 'KH', 'KIR' => 'KI', 'KNA' => 'KN', 'KOR' => 'KR', 'KWT' => 'KW',
        'LAO' => 'LA', 'LBN' => 'LB', 'LBR' => 'LR', 'LBY' => 'LY', 'LCA' => 'LC', 'LIE' => 'LI', 'LKA' => 'LK', 'LSO' => 'LS', 'LTU' => 'LT', 'LUX' => 'LU', 'LVA' => 'LV',
        'MAC' => 'MO', 'MAF' => 'MF', 'MAR' => 'MA', 'MCO' => 'MC', 'MDA' => 'MD', 'MDG' => 'MG', 'MDV' => 'MV', 'MEX' => 'MX', 'MHL' => 'MH', 'MKD' => 'MK', 'MLI' => 'ML', 'MLT' => 'MT', 'MMR' => 'MM', 'MNE' => 'ME', 'MNG' => 'MN', 'MNP' => 'MP', 'MOZ' => 'MZ', 'MRT' => 'MR', 'MSR' => 'MS', 'MTQ' => 'MQ', 'MUS' => 'MU', 'MWI' => 'MW', 'MYS' => 'MY', 'MYT' => 'YT',
        'NAM' => 'NA', 'NCL' => 'NC', 'NER' => 'NE', 'NFK' => 'NF', 'NGA' => 'NG', 'NIC' => 'NI', 'NIU' => 'NU', 'NLD' => 'NL', 'NOR' => 'NO', 'NPL' => 'NP', 'NRU' => 'NR', 'NZL' => 'NZ',
        'OMN' => 'OM',
        'PAK' => 'PK', 'PAN' => 'PA', 'PCN' => 'PN', 'PER' => 'PE', 'PHL' => 'PH', 'PLW' => 'PW', 'PNG' => 'PG', 'POL' => 'PL', 'PRI' => 'PR', 'PRK' => 'KP', 'PRT' => 'PT', 'PRY' => 'PY', 'PSE' => 'PS', 'PYF' => 'PF',
        'QAT' => 'QA',
        'REU' => 'RE', 'ROM' => 'RO', 'RUS' => 'RU', 'RWA' => 'RW',
        'SAU' => 'SA', 'SDN' => 'SD', 'SEN' => 'SN', 'SGP' => 'SG', 'SGS' => 'GS', 'SHN' => 'SH', 'SJM' => 'SJ', 'SLB' => 'SB', 'SLE' => 'SL', 'SLV' => 'SV', 'SMR' => 'SM', 'SOM' => 'SO', 'SPM' => 'PM', 'SRB' => 'RS', 'SSD' => 'SS', 'STP' => 'ST', 'SUR' => 'SR', 'SVK' => 'SK', 'SVN' => 'SI', 'SWE' => 'SE', 'SWZ' => 'SZ', 'SXM' => 'SX', 'SYC' => 'SC', 'SYR' => 'SY',
        'TCA' => 'TC', 'TCD' => 'TD', 'TGO' => 'TG', 'THA' => 'TH', 'TJK' => 'TJ', 'TKL' => 'TK', 'TKM' => 'TM', 'TLS' => 'TL', 'TON' => 'TO', 'TTO' => 'TT', 'TUN' => 'TN', 'TUR' => 'TR', 'TUV' => 'TV', 'TWN' => 'TW', 'TZA' => 'TZ',
        'UGA' => 'UG', 'UKR' => 'UA', 'UMI' => 'UM', 'URY' => 'UY', 'USA' => 'US', 'UZB' => 'UZ',
        'VAT' => 'VA', 'VCT' => 'VC', 'VEN' => 'VE', 'VGB' => 'VG', 'VIR' => 'VI', 'VNM' => 'VN', 'VUT' => 'VU',
        'WLF' => 'WF', 'WSM' => 'WS',
        'YEM' => 'YE',
        'ZAF' => 'ZA', 'ZMB' => 'ZM', 'ZWE' => 'ZW',
    ];

    /**
     * @var array COUNTRY_NUMBERS
     *   ISO 3166-1 country numbers and two-letter country codes.
     *   Note that the `CountryRepository` class contains a similar
     *   array that maps country codes to numbers.
     */
    public const COUNTRY_NUMBERS = [
          4 => 'AF',   8 => 'AL',  10 => 'AQ',  12 => 'DZ',  16 => 'AS',  20 => 'AD',  24 => 'AO',  28 => 'AG',  31 => 'AZ',  32 => 'AR',  36 => 'AU',  40 => 'AT',  44 => 'BS',  48 => 'BH',  50 => 'BD', 51 => 'AM', 52 => 'BB', 56 => 'BE', 60 => 'BM', 64 => 'BT', 68 => 'BO', 70 => 'BA', 72 => 'BW', 74 => 'BV', 76 => 'BR', 84 => 'BZ', 86 => 'IO', 90 => 'SB',  92 => 'VG',  96 => 'BN',
        100 => 'BG', 104 => 'MM', 108 => 'BI', 112 => 'BY', 116 => 'KH', 120 => 'CM', 124 => 'CA', 132 => 'CV', 136 => 'KY', 140 => 'CF', 144 => 'LK', 148 => 'TD', 152 => 'CL', 156 => 'CN', 158 => 'TW', 162 => 'CX', 166 => 'CC', 170 => 'CO', 174 => 'KM', 175 => 'YT', 178 => 'CG', 180 => 'CD', 184 => 'CK', 188 => 'CR', 191 => 'HR', 192 => 'CU', 196 => 'CY',
        203 => 'CZ', 204 => 'BJ', 208 => 'DK', 212 => 'DM', 214 => 'DO', 218 => 'EC', 222 => 'SV', 226 => 'GQ', 231 => 'ET', 232 => 'ER', 233 => 'EE', 234 => 'FO', 238 => 'FK', 239 => 'GS', 242 => 'FJ', 246 => 'FI', 248 => 'AX', 250 => 'FR', 254 => 'GF', 258 => 'PF', 260 => 'TF', 262 => 'DJ', 266 => 'GA', 268 => 'GE', 270 => 'GM', 275 => 'PS', 276 => 'DE', 288 => 'GH', 292 => 'GI', 296 => 'KI',
        300 => 'GR', 304 => 'GL', 308 => 'GD', 312 => 'GP', 316 => 'GU', 320 => 'GT', 324 => 'GN', 328 => 'GY', 332 => 'HT', 334 => 'HM', 336 => 'VA', 340 => 'HN', 344 => 'HK', 348 => 'HU', 352 => 'IS', 356 => 'IN', 360 => 'ID', 364 => 'IR', 368 => 'IQ', 372 => 'IE', 376 => 'IL', 380 => 'IT', 384 => 'CI', 388 => 'JM', 392 => 'JP', 398 => 'KZ',
        400 => 'JO', 404 => 'KE', 408 => 'KP', 410 => 'KR', 414 => 'KW', 417 => 'KG', 418 => 'LA', 422 => 'LB', 426 => 'LS', 428 => 'LV', 430 => 'LR', 434 => 'LY', 438 => 'LI', 440 => 'LT', 442 => 'LU', 446 => 'MO', 450 => 'MG', 454 => 'MW', 458 => 'MY', 462 => 'MV', 466 => 'ML', 470 => 'MT', 474 => 'MQ', 478 => 'MR', 480 => 'MU', 484 => 'MX', 492 => 'MC', 496 => 'MN', 498 => 'MD', 499 => 'ME',
        500 => 'MS', 504 => 'MA', 508 => 'MZ', 512 => 'OM', 516 => 'NA', 520 => 'NR', 524 => 'NP', 528 => 'NL', 531 => 'CW', 533 => 'AW', 534 => 'SX', 535 => 'BQ', 540 => 'NC', 548 => 'VU', 554 => 'NZ', 558 => 'NI', 562 => 'NE', 566 => 'NG', 570 => 'NU', 574 => 'NF', 578 => 'NO', 580 => 'MP', 581 => 'UM', 583 => 'FM', 584 => 'MH', 585 => 'PW', 586 => 'PK', 591 => 'PA', 598 => 'PG',
        600 => 'PY', 604 => 'PE', 608 => 'PH', 612 => 'PN', 616 => 'PL', 620 => 'PT', 624 => 'GW', 626 => 'TL', 630 => 'PR', 634 => 'QA', 638 => 'RE', 642 => 'RO', 643 => 'RU', 646 => 'RW', 652 => 'BL', 654 => 'SH',659 => 'KN', 660 => 'AI', 662 => 'LC', 663 => 'MF', 666 => 'PM', 670 => 'VC', 674 => 'SM', 678 => 'ST', 682 => 'SA', 686 => 'SN', 688 => 'RS', 690 => 'SC', 694 => 'SL',
        702 => 'SG', 703 => 'SK', 704 => 'VN', 705 => 'SI', 706 => 'SO', 710 => 'ZA', 716 => 'ZW', 724 => 'ES', 728 => 'SS', 729 => 'SD', 732 => 'EH', 740 => 'SR', 744 => 'SJ', 748 => 'SZ', 752 => 'SE', 756 => 'CH', 760 => 'SY', 762 => 'TJ', 764 => 'TH', 768 => 'TG', 772 => 'TK', 776 => 'TO', 780 => 'TT', 784 => 'AE', 788 => 'TN', 792 => 'TR', 795 => 'TM', 796 => 'TC', 798 => 'TV',
        800 => 'UG', 804 => 'UA', 807 => 'MK', 818 => 'EG', 826 => 'GB', 831 => 'GG', 832 => 'JE', 833 => 'IM', 834 => 'TZ', 840 => 'US', 850 => 'VI', 854 => 'BF', 858 => 'UY', 860 => 'UZ', 862 => 'VE', 876 => 'WF', 882 => 'WS', 887 => 'YE', 894 => 'ZM',
    ];

    /**
     * Prevent instantiation of helper class.
     */
    private function __construct() { }

    /**
     * Creates a country-code value object.
     *
     * @param string|\Stringable|int $country
     *   Country identifier or other unique country information as a string,
     *   numeric string, or integer.
     *
     * @return \StoreCore\Types\CountryCode|null
     *   Returns a `CountryCode` value object or `null` if no matching country
     *   code could be found.
     */
    public static function createCountryCode(string|Stringable|int $country): ?CountryCode
    {
        if ($country instanceof Stringable) $country = (string) $country;
        if (is_numeric($country)) $country = (int) $country;

        if (is_int($country)) {
            if (isset(self::COUNTRY_NUMBERS[$country])) {
                return new CountryCode(self::COUNTRY_NUMBERS[$country]);
            }
            return null;
        }

        $country = strtoupper(trim($country));
        $strlen = strlen($country);

        if ($strlen === 2 && in_array($country, self::COUNTRY_NUMBERS)) {
            return new CountryCode($country);
        } elseif ($strlen === 3 && array_key_exists($country, self::COUNTRY_CODES)) {
            return new CountryCode(self::COUNTRY_CODES[$country]);
        } elseif ($country === 'UK') {
            return new CountryCode('GB');
        } else {
            return null;
        }
    }

    /**
     * Maps a country or country identifier to a numeric country code.
     *
     * @param StoreCore\Geo\Country|StoreCore\Types\CountryCode|string $country
     *   Country or alphanumeric country code.
     *
     * @return null|int
     *   ISO 3166 country number as an integer or `null` if no matching country
     *   could be found.
     */
    public static function getCountryNumber(Country|CountryCode|string $country): ?int
    {
        if ($country instanceof Country) $country = $country->identifier;
        if ($country instanceof CountryCode) $country = (string) $country;
        $country = strtoupper(trim($country));

        $strlen = strlen($country);
        if ($strlen === 3 && isset(self::COUNTRY_CODES[$country])) {
            return self::getCountryNumber(self::COUNTRY_CODES[$country]);
        }

        if ($strlen === 2) {
            $country_numbers = array_flip(self::COUNTRY_NUMBERS);
            if (isset($country_numbers[$country])) return $country_numbers[$country];
        }

        return null;
    }
}
