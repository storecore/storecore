<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Geo;

use StoreCore\Types\URL;

/**
 * Trait for the `hasMap` property of a `Place`.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0
 */
trait HasMapTrait
{
    /**
     * @var null|StoreCore\Geo\Map|StoreCore\Types\URL $hasMap
     *   A `Map` or a `URL` to a `Map` of the `Place`.
     */
    public null|Map|URL $hasMap = null;
}
