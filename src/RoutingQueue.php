<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \SplPriorityQueue as PriorityQueue;
use StoreCore\Route;

/**
 * Priority queue of HMVC routes.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php.net/manual/en/class.splpriorityqueue.php The SplPriorityQueue class
 * @version 1.0.0
 */
class RoutingQueue implements \Countable, \Iterator
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var PriorityQueue $routes
     *   Priority queue of routes.
     */
    private PriorityQueue $routes;

    /**
     * Creates a priority queue for routes.
     *
     * @param void
     *
     * @uses \SplPriorityQueue::__construct()
     */
    public function __construct()
    {
        $this->routes = new PriorityQueue();
    }

    /**
     * Counts the number of routes in the queue.
     *
     * @param void
     *
     * @return int
     *   Returns the number of routes in the priority queue as an integer.
     */
    public function count(): int
    {
        return $this->routes->count();
    }

    /**
     * Returns the current route pointed to by the iterator.
     *
     * @param void
     * @return mixed
     * @uses SplPriorityQueue::current()
     */
    public function current(): mixed
    {
        return $this->routes->current();
    }

    /**
     * Calls all routes.
     *
     * @param void
     * @return void
     */
    public function dispatch(): void
    {
        if ($this->isEmpty()) return;
 
        foreach ($this->routes as $route) {
            $route->dispatch();
        }
    }

    /**
     * Adds a route to the priority queue.
     *
     * @param StoreCore\Route $route
     *   The route to add to the priority queue.
     *
     * @param int|string $priority
     *   Priority of the route as an integer, from -128 through 127, or a numeric
     *   string.  A priority larger than 127 is set to the highest priority 127.
     *
     * @return bool
     *   Returns true.
     *
     * @uses \SplPriorityQueue::insert()
     */
    public function insert(Route $route, int|string $priority = 0): bool
    {
        if (!\is_int($priority)) {
            $priority = (int) $priority;
        }

        // Signed range of a TINYINT from -128 to 127:
        if ($priority < -128) {
            $priority = -128;
        } elseif ($priority > 127) {
            $priority = 127;
        }

        return $this->routes->insert($route, $priority);
    }

    /**
     * Checks whether the routing queue is empty.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` if the queue is empty, otherwise `false`.
     *
     * @uses \SplPriorityQueue::isEmpty()
     */
    public function isEmpty(): bool
    {
        return $this->routes->isEmpty();
    }

    /**
     * Returns the current node index.
     * 
     * @param void
     *
     * @return mixed
     *   The current node index.
     *
     * @uses \SplPriorityQueue::key()
     */
    public function key(): mixed
    {
        return $this->routes->key();
    }

    /**
     * Moves to the next node.
     *
     * @param void
     * @return void
     * @uses \SplPriorityQueue::next()
     * @see https://www.php.net/manual/en/splpriorityqueue.next.php
     */
    public function next(): void
    {
        $this->routes->next();
    }

    /**
     * Rewinds the iterator back to the start of the queue.
     *
     * @param void
     * @return void
     * @uses \SplPriorityQueue::rewind()
     * @see https://www.php.net/manual/en/splpriorityqueue.rewind.php
     */
    public function rewind(): void
    {
        $this->routes->rewind();
    }

    /**
     * Sets the mode of extraction.
     *
     * @param int $flags
     *   Defines what is extracted.  Must be set to `SplPriorityQueue::EXTR_DATA`, 
     *   `SplPriorityQueue::EXTR_PRIORITY ` or `SplPriorityQueue::EXTR_BOTH`.
     *
     * @return void
     *
     * @uses \SplPriorityQueue::setExtractFlags
     *
     * @see https://www.php.net/manual/en/splpriorityqueue.setextractflags.php
     */
    public function setExtractFlags(int $flags): void
    {
        $this->routes->setExtractFlags($flags);
    }

    /**
     * Checks whether the queue contains more nodes.
     *
     * @param void
     * @return bool
     * @uses \SplPriorityQueue::valid()
     * @see https://www.php.net/manual/en/splpriorityqueue.valid.php
     */
    public function valid(): bool
    {
        return $this->routes->valid();
    }
}
