<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2017, 2020, 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Observer design pattern: abstract subject.
 * 
 * This `AbstractSubject` uses the `SubjectTrait` to implement the
 * `SplSubject` interface from the observer design pattern.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/Observer_pattern Observer pattern
 * @version 1.0.0
 */
abstract class AbstractSubject implements \SplSubject
{
    use SubjectTrait;
}
