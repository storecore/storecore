<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\Http\Message\UriInterface;
use Psr\SimpleCache\CacheInterface;
use StoreCore\Engine\UriFactory;
use StoreCore\Types\CacheKey;
use StoreCore\Types\CacheKeyException as InvalidArgumentException;

class_exists(StoreCore\Types\CacheKey::class);

/**
 * Cache directory.
 * 
 * This StoreCore file system class uses a directory (folder) as a simple cache
 * for multiple cache files.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\CMS
 * @version   1.0.0
 */
class CacheDirectory implements CacheInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';


    /**
     * @var string $path
     *   Full absolute path of the cache directory.  Use the class constructor
     *   or the `setPath()` method to set this property.
     */
    private string $path;


    /**
     * Creates a cache directory.
     * 
     * @param string|null $path
     *   Absolute or relative path of the directory.  If the path is not set,
     *   StoreCore will use the global `STORECORE_FILESYSTEM_CACHE_DIR` constant;
     *   if the constant is not defined, the default `/cache/` directory is used.
     */
    public function __construct(?string $path = null)
    {
        if ($path === null) {
            if (defined('STORECORE_FILESYSTEM_CACHE_DIR')) {
                $this->setPath(STORECORE_FILESYSTEM_CACHE_DIR);
            } elseif (is_dir(__DIR__ . '/../../cache')) {
                $this->setPath(realpath(__DIR__ . '/../../cache'));
            }
        } else {
            $this->setPath($path);
        }
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $files = glob($this->path . '*');
        if ($files === false) {
            return false;
        } elseif (empty($files)) {
            return true;
        }

        $result = true;
        foreach ($files as $filename) {
            if (is_file($filename)) {
                if (unlink($filename) !== true) $result = false;
            }
        }
        return $result;
    }

    /**
     * Creates a cache key.
     *
     * @param mixed $value
     *   Value to create a cache key from.
     *
     * @return string
     *
     * @throws Psr\SimpleCache\InvalidArgumentException
     *   Throws a `StoreCore\Types\CacheKey` exception that implements the
     *   PSR-16 `Psr\SimpleCache\InvalidArgumentException` on an invalid key.
     *
     * @uses StoreCore\Engine\UriFactory::createUri()
     * @uses StoreCore\Types\CacheKey::__construct()
     * @uses StoreCore\Types\CacheKey::validate()
     */
    protected function createKey(mixed $value): string
    {
        try {
            // RECOMMENDED use of cache keys as stringable value object.
            if ($value instanceof CacheKey) return (string) $value;

            // If the value is a string that appears to be a valid URL,
            // create an object the implements `Psr\Http\Message\UriInterface`.
            if (\is_string($value) && filter_var($value, \FILTER_VALIDATE_URL)) {
                $factory = new UriFactory();
                $value = $factory->createUri($value);
                unset($factory);
            }

            // If the value implements the `Psr\Http\Message\UriInterface`,
            // create a cache key value object.
            if ($value instanceof UriInterface) $value = new CacheKey($value);

            // If the value is a cache key value object, return its string value.
            if ($value instanceof CacheKey) return (string) $value;

            // If the value is a valid cache key string, return the string.
            if ($value instanceof \Stringable) $value = (string) $value;
            if (CacheKey::validate($value) === true) return $value;

            // `StoreCore\Types\CacheKey implements Psr\SimpleCache\InvalidArgumentException`
            throw new InvalidArgumentException('Invalid cache key: ' . strval($value));
        } catch (InvalidArgumentException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function delete(string|\Stringable $key): bool
    {
        $key = $this->createKey($key);
        $filename = $this->path . $key;
        return (!is_file($filename)) ? false : unlink($filename);
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        try {
            $result = true;
            foreach ($keys as $key) {
                if ($this->delete($key) === false) $result = false;
            }
            return $result;
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function get(string|\Stringable $key, mixed $default = null): mixed
    {
        $key = $this->createKey($key);
        $filename = $this->path . $key;
        if (!is_readable($filename)) return $default;

        $file = file_get_contents($filename, false);
        return ($file === false) ? $default : $file;
    }

    
    /**
     * Gets file modification time.
     *
     * @param \Stringable|string $key
     *   Cache key.
     *
     * @return int|false
     *   Returns the time the cache file was last modified as a Unix timestamp,
     *   or `false` on failure.
     */
    public function getFileModificationTime(\Stringable|string $key): int|false
    {
        return filemtime($this->path . $key);
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        try {
            $result = array();
            foreach ($keys as $key) {
                if (!$this->has($key)) throw new InvalidArgumentException();
                $result[$key] = $this->get($key, $default);
            }
            return $result;
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function has(string|\Stringable $key): bool
    {
        $key = $this->createKey($key);
        $filename = $this->path . $key;
        return (is_file($filename) && is_readable($filename)) ? true : false; 
    }

    /**
     * @inheritDoc
     */
    public function set(string|\Stringable $key, mixed $value, null|int|\DateInterval $ttl = null): bool
    {
        $key = $this->createKey($key);
        $filename = $this->path . $key;
        $bytes_written = file_put_contents($filename, $value);
        return ($bytes_written === false) ? false : true;
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|\DateInterval $ttl = null): bool
    {
        try {
            $result = true;
            foreach ($values as $key => $value) {
                if ($this->set($key, $value, $ttl) !== true) $result = false;
            }
            return $result;
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Sets the directory path.
     *
     * @param string $path
     *   Path of the directory.
     * 
     * @return void
     *
     * @throws Psr\SimpleCache\CacheException
     *   Throws an exception that implements the PSR-16 cache exception interface
     *   if `$path` is not a directory or the cache directory is not readable.
     */
    public function setPath(string $path): void
    {
        $path = rtrim($path, '/\\');
        if (!is_dir($path) || !is_readable($path)) throw new CacheException();
        $this->path = $path . DIRECTORY_SEPARATOR;
    }
}
