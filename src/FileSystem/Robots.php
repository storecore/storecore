<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface, StreamInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\{AbstractController, Registry};
use StoreCore\Engine\{ResponseFactory, StreamFactory};

/**
 * Robots controller.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class Robots extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var int $crawlDelay
     *   Unofficial `Crawl-delay` directive as an integer in seconds.  Defaults
     *   to the global configuration setting `STORECORE_CMS_CRAWL_DELAY` or to
     *   10 seconds if the constant is undefined.
     */
    private int $crawlDelay = 10;

    /**
     * @var int|false $lastModified
     *   The date and time when the content of the robots.txt file was changed
     *   as a Unix timestamp or `false` when the timestamp is unknown.
     */
    private int|false $lastModified = false;

    /**
     * @var Psr\Http\Message\StreamInterface $view
     *   Contents of the robots.txt file as an MVC view.
     */
    private StreamInterface $view;

    /**
     * Creates and publishes robots.txt file.
     *
     * @param \StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        if (\defined('STORECORE_CMS_CRAWL_DELAY')) {
            $this->setCrawlDelay(STORECORE_CMS_CRAWL_DELAY);
        }
    }

    /**
     * Gets the `Crawl-delay` value.
     *
     * @param void
     * @return int
     *   `Crawl-delay` in seconds as an integer.
     */
    public function getCrawlDelay(): int
    {
        return $this->crawlDelay;
    }

    /**
     * Gets the contents of the default robots.txt file.
     *
     * @param void
     *
     * @return Psr\Http\Message\StreamInterface
     *   Returns the contents of a robots.txt text file with a crawl delay,
     *   an XML sitemap link, and default disallows for known user agents.
     *
     * @see https://platform.openai.com/docs/plugins/bot
     */
    private function getDeFaultFile(): StreamInterface
    {
        $this->lastModified = filemtime(__FILE__);
        $factory = new StreamFactory();
        return $factory->createStream(
            'User-agent: *' . "\n"
            . 'Crawl-delay: ' . $this->getCrawlDelay() . "\n"
            . 'Allow: /' . "\n"
            . 'Disallow: /admin/' . "\n"
            . 'Disallow: /api/' . "\n"
            . 'Disallow: /cgi-bin/' . "\n"
            . 'Disallow: /public/' . "\n"
            . 'Disallow: /wp-admin/' . "\n"
            . 'Disallow: /wp-login.php' . "\n"
            . "\n"
            . 'Sitemap: ' . $this->getSitemap() . "\n"
            . "\n"
            . 'User-agent: ChatGPT-User' . "\n"
            . 'Disallow: /' . "\n\n"
        );
    }

    /**
     * Gets the contents of a robots.txt file.
     *
     * @param void
     *
     * @return Psr\Http\Message\StreamInterface
     *   Returns the contents of a custom robots.txt text file, if it exists,
     *   or the default robots.txt file if there is no custom robots.txt file.
     */
    public function getFileContents(): StreamInterface
    {
        $filename = realpath(__DIR__ . '/../../public/txt/robots.txt');
        if ($filename !== false) {
            $this->lastModified = filemtime($filename);
            $factory = new StreamFactory();
            return $factory->createStreamFromFile($filename);
        }
        return $this->getDeFaultFile();
    }

    /**
     * Gets the URL of the XML sitemap.
     *
     * @param void
     * @return string
     */
    public function getSitemap(): string
    {
        return 'https://' . $this->Request->getUri()->getHost() . '/sitemap.xml';
    }

    /**
     * Creates a HTTP response.
     *
     * @param Psr\Http\Message\ServerRequestInterface
     * @return Psr\Http\Message\ResponseInterface
     */
    public function handle(?ServerRequestInterface $request = null): ResponseInterface
    {
        $this->view = $this->getFileContents();

        $factory = new ResponseFactory();
        $this->Response = $factory->createResponse();
        $this->Response->setHeader('Cache-Control', 'max-age=' . $this->getCrawlDelay());
        $this->Response->setHeader('Content-Type', 'text/plain; charset=UTF-8');
        if ($this->lastModified !== false) {
            $this->Response->setHeader('Last-Modified', gmdate('D, d M Y H:i:s T', $this->lastModified));
        }
        $this->Response->setBody($this->view);
        return $this->Response;
    }

    /**
     * Sets the Crawl-delay directive.
     *
     * @param int $seconds
     *   `Crawl-delay` in seconds, with a minimum of 1 second
     *   and a maximum of 300 seconds (5 minutes).
     *
     * @return void
     */
    public function setCrawlDelay(int $seconds): void
    {
        if ($seconds < 1) {
            $seconds = 1;
        } elseif ($seconds > 300) {
            $seconds = 300;
        }
        $this->crawlDelay = $seconds;
    }
}
