<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\FileSystem;

use StoreCore\LoggerFactory;

/**
 * StoreCore file system backup.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
class Backup
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Gets the backup destination directory.
     *
     * @param void
     * @return string
     */
    public static function getDirectory(): string
    {
        if (\defined('STORECORE_FILESYSTEM_CACHE_DATA_DIR')) {
            $directory = STORECORE_FILESYSTEM_CACHE_DATA_DIR;
        } elseif (\defined('STORECORE_FILESYSTEM_CACHE_DIR')) {
            $directory = STORECORE_FILESYSTEM_CACHE_DIR;
        } else {
            $directory = realpath(__DIR__ . '/../../resources/cache/data');
            if ($directory === false) {
                $directory = realpath(__DIR__);
                if ($directory === false) {
                    $directory = __DIR__;
                }
            }
        }
        return rtrim($directory, DIRECTORY_SEPARATOR);
    }

    /**
     * Saves a file system backup to a (compressed) archive file.
     *
     * @param bool $compress
     *   Compress the archive to .tar.gz (default `true`) or do not compress
     *   the .tar archive (`false`).
     *
     * @return void
     *
     * @throws \RuntimeException
     *   An SPL (Standard PHP Library) runtime exception is thrown if the
     *   current working directory is not writeable.
     */
    public static function save(bool $compress = true): void
    {
        $factory = new \StoreCore\LoggerFactory();
        $logger = $factory->createLogger();
        $logger->notice('Backup process started.');

        $working_directory = getcwd();
        if ($working_directory === false || !\is_writable($working_directory)) {
            $logger->error('Current working directory is not writeable.');
            throw new \RuntimeException('Current working directory is not writeable.');
        }

        $archive_filename = $working_directory . DIRECTORY_SEPARATOR . 'backup-' . gmdate('Ymd-Hms') . '-UTC-' . time() . '.tar';
        $archive = new \PharData($archive_filename);
        $archive->buildFromDirectory($working_directory);
        $logger->notice('File system backup saved as ' . $archive_filename . '.');

        if ($compress != false) {
            $archive->compress(\Phar::GZ);
            $logger->notice('Backup compressed to ' . $archive_filename . '.gz.');
            $archive = null;
            if (!unlink($archive_filename)) {
                $logger->error('Could not delete the archive file ' . $archive_filename . '.');
            }
        }
    }
}
