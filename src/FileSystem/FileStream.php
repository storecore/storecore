<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \Exception;
use \ValueError;
use Psr\Http\Message\StreamInterface;
use StoreCore\Engine\AbstractStream;

/**
 * PSR-7 compliant file stream.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-7/ PSR-7: HTTP message interfaces
 * @see     https://github.com/php-fig/http-message/blob/master/src/StreamInterface.php StreamInterface
 * @version 1.0.0
 */
class FileStream extends AbstractStream implements StreamInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var array READABLE_MODES
     *   Readable file stream modes.
     */
    protected const array READABLE_MODES = ['a+', 'c+', 'r', 'r+', 'w+', 'x+'];

    /**
     * @var array WRITABLE_MODES
     *   Writable file stream modes.
     */
    protected const array WRITABLE_MODES = ['a', 'a+', 'c', 'c+', 'r+', 'w', 'w+', 'x', 'x+'];

    /**
     * Creates a file stream.
     *
     * @param string $filename 
     *   The filename to use as basis of the stream.
     *
     * @param string $mode
     *   File open modes as used by `fopen()`.  Defaults to `r+` for a file
     *   stream that is readable and writable.
     */
    public function __construct(string $filename, string $mode = 'r+')
    {
        $mode = trim($mode);
        if (empty($mode)) {
            throw new ValueError(__METHOD__ . ' expects parameter 2 to be valid fopen() mode');
        }

        // Strip `b` (binary) and `t` (translate) flags
        $mode = strtolower($mode);
        $unflagged_mode = str_replace(['b', 't'], '', $mode);
        if (
            !in_array($unflagged_mode, self::READABLE_MODES)
            && !in_array($unflagged_mode, self::WRITABLE_MODES)
        ) {
            throw new ValueError(__METHOD__ . ' does not support file read/write mode ' . $mode);
        }

        /* Error abstraction with exception chaining: PSR-17 requires
         * an SPL RuntimeException if the file cannot be opened.
         */
        try {
            $this->streamResource = fopen($filename, $mode);
        } catch (\Throwable $t) {
            throw new \RuntimeException($t->getMessage(), (int) $t->getCode(), $t);
        }

        if (in_array($unflagged_mode, self::READABLE_MODES)) $this->readable = true;
        if (in_array($unflagged_mode, self::WRITABLE_MODES)) $this->writable = true;
    }

    #[\Override]
    public function write(string $string): int
    {
        if (!$this->isWritable()) {
            throw new \RuntimeException('File stream is not writable');
        }

        if (empty($string)) {
            return 0;
        }

        $bytes_written = fwrite($this->streamResource, $string);
        if ($bytes_written === false) {
            throw new \RuntimeException('Error writing to file stream');
        }
        $this->size = null;
        return $bytes_written;
    }
}
