<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\SimpleCache\CacheException as CacheExceptionInterface;

class CacheException extends \Exception implements
    CacheExceptionInterface,
    \Throwable
{
}
