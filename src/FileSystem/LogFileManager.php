<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

/**
 * Log file manager.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2017, 2022 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\BI
 * @version   0.2.1
 */
class LogFileManager implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';


    /**
     * @var array|bool $logFiles
     *   Array with the local file names of log files in the `logs` directory,
     *   or `false` if there are no log files.
     */
    private array|false $logFiles = false;


    /**
     * Creates a log file manager.
     *
     * @param void
     */
    public function __construct()
    {
        $this->logFiles = scandir(STORECORE_FILESYSTEM_LOGS_DIR);
        if ($this->logFiles !== false) $this->clear();
    }

    /**
     * Clears the logs directory.
     *
     * @param bool $all
     *   Delete all log files (`true`) or only empty log files (default `false`).
     *
     * @return void
     */
    public function clear(bool $all = false): void
    {
        if (\is_array($this->logFiles) && !empty($this->logFiles)) {
            $files = $this->logFiles;
            foreach ($files as $key => $filename) {
                if (
                    is_dir(STORECORE_FILESYSTEM_LOGS_DIR . $filename)
                    || $filename === '.htaccess'
                ) {
                    unset($files[$key]);
                } else {
                    $filename = str_ireplace('.log', '', $filename) . '.log';
                    if ($all === true || filesize(STORECORE_FILESYSTEM_LOGS_DIR . $filename) === 0) {
                        if (unlink(STORECORE_FILESYSTEM_LOGS_DIR . $filename)) {
                            unset($files[$key]);
                        }
                    }
                }
            }

            $this->logFiles = empty($files) ? false : $files;
        }
    }

    /**
     * Counts the number of log files.
     *
     * @param void
     * @return int
     */
    public function count(): int
    {
        if (!\is_array($this->logFiles)) return 0;
        return count($this->logFiles);
    }

    /**
     * Reads all log files in ascending order by date.
     *
     * @param void
     * @return string|null
     */
    public function read(): ?string
    {
        if (!\is_array($this->logFiles) || empty($this->logFiles)) return null;

        $files = $this->logFiles;
        asort($files);

        $result = (string) null;
        foreach ($files as $filename) {
            $file_contents = file_get_contents(STORECORE_FILESYSTEM_LOGS_DIR . $filename, false);
            if ($file_contents !== false && !empty($file_contents)) {
                $result .= $file_contents . "\n";
            }
        }

        $result = str_ireplace("\r\n", "\n", $result);
        $result = str_ireplace("\n\n", "\n", $result);
        $result = trim($result);

        return empty($result) ? null : $result;
    }
}
