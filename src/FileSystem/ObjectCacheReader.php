<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use \Exception;
use \Throwable;

use Psr\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use StoreCore\ContainerException;
use StoreCore\NotFoundException;
use StoreCore\Types\UUID;

use function \defined;
use function \dirname;
use function \file_get_contents;
use function \gzinflate;
use function \is_dir;
use function \is_file;
use function \is_readable;
use function \is_string;
use function \rtrim;
use function \time;
use function \unserialize;

/**
 * Object cache reader.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class ObjectCacheReader implements ContainerInterface, LoggerAwareInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $cacheDirectory
     *   Path to the cache directory, ending in the PHP `DIRECTORY_SEPARATOR`.
     */
    protected string $cacheDirectory = '';

    /**
     * @var Psr\Log\LoggerInterface|null $logger
     *   OPTIONAL instance of a PSR-3 logger to log cache errors.
     */
    protected ?LoggerInterface $logger = null;

    /**
     * Gets a single object from the object cache.
     *
     * @param StoreCore\Types\UUID|string
     *   Universally unique identifier (UUID) as a value object or a string.
     *
     * @return object
     */
    public function get(UUID|string $id): mixed
    {
        if (is_string($id) && !UUID::validate($id)) {
            throw new NotFoundException('Invalid UUID: ' . $id, time());
        }

        if (!$this->has($id)) {
            throw new NotFoundException('Object cache file with UUID ' . $id . ' not found', time());
        }

        try {
            $filename = $this->getObjectCacheDirectory() . $id . '.bin';
            $result = file_get_contents($filename, false);
            if ($result === false) {
                throw new ContainerException('Could not read object cache file: ' . $filename, time());
            }
            $result = gzinflate($result);
            if ($result === false) {
                throw new ContainerException('Could not inflate object cache file: ' . $filename, time());
            }
            $result = unserialize($result);
            if ($result === false) {
                throw new ContainerException('Could not unserialize object cache file: ' . $filename, time());
            }
            return $result;
        } catch (ContainerException $e) {
            if ($this->logger !== null) {
                $this->logger->error('Object cache exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            }
            throw $e;
        } catch (Throwable $t) {
            if ($this->logger !== null) {
                $this->logger->error('Error ' . $t->getCode() . ' in ' . $t->getFile() . ' on line ' . $t->getLine() . ': ' . $t->getMessage());
            }
            throw new ContainerException($t->getMessage(), $t->getCode(), $t);
        }
    }

    /**
     * Checks whether the object cache contains a given object.
     *
     * @param StoreCore\Types\UUID|string $id
     *   UUID (universally unique identifier) as a value object or a string.
     */
    public function has(UUID|string $id): bool
    {
        if (is_string($id) && !UUID::validate($id)) {
            return false;
        }

        try {
            $filename = $this->getObjectCacheDirectory() . $id . '.bin';
            if (is_file($filename) && !is_dir($filename) && is_readable($filename)) {
                return true;
            }
        } catch (Exception $e) {
            if ($this->logger !== null) {
                $this->logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            }
        }
        return false;
    }

    /**
     * Gets the file system path of the object cache.
     *
     * @internal
     * @param void
     * @return string
     * @throws Psr\Container\NotFoundExceptionInterface
     */
    final protected function getObjectCacheDirectory(): string
    {
        if (!empty($this->cacheDirectory)) {
            return $this->cacheDirectory;
        }

        if (defined('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR')) {
            $this->cacheDirectory = rtrim(STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR, '/\\') . DIRECTORY_SEPARATOR;
            return $this->cacheDirectory;
        }

        if (defined('STORECORE_FILESYSTEM_CACHE_DIR')) {
            $result = rtrim(STORECORE_FILESYSTEM_CACHE_DIR, '/\\') . DIRECTORY_SEPARATOR . 'objects';
            if (is_dir($result)) {
                $this->cacheDirectory = $result . DIRECTORY_SEPARATOR;
                return $this->cacheDirectory;
            }
        }

        $result = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'objects';
        if (is_dir($result)) {
            $this->cacheDirectory = $result . DIRECTORY_SEPARATOR;
            return $this->cacheDirectory;
        }

        throw new NotFoundException('Object cache directory not found.');
    }

    /**
     * Sets a logger instance on the object.
     *
     * @param Psr\Log\LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
