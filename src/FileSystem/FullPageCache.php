<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\SimpleCache\CacheInterface;
use StoreCore\Types\CacheKey;

/**
 * Full-page cache.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Catalog
 * @version   1.0.0-alpha.1
 */
class FullPageCache extends CacheDirectory implements CacheInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    public function __construct(?string $path = null)
    {
        if ($path === null) {
            if (defined('STORECORE_FILESYSTEM_CACHE_PAGES_DIR')) {
                $this->setPath(STORECORE_FILESYSTEM_CACHE_PAGES_DIR);
            } elseif (is_dir(__DIR__ . '/../../cache/pages')) {
                $this->setPath(realpath(__DIR__ . '/../../cache/pages'));
            }
        } else {
            parent::__construct($path);
        }
    }
}
