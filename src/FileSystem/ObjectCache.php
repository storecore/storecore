<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;


use \DateInterval;
use \Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\SimpleCache\CacheInterface;
use StoreCore\Types\UUID;

use function \empty;
use function \file_put_contents;
use function \glob;
use function \gzdeflate;
use function \is_file;
use function \is_string;
use function \serialize;
use function \time;
use function \unlink;

use const \LOCK_EX;
use const \ZLIB_ENCODING_RAW;

/**
 * Object cache.
 *
 * @package StoreCore\Core
 * @see     https://github.com/php-fig/simple-cache/blob/master/src/CacheInterface.php
 * @version 1.0.0-alpha.1
 */
class ObjectCache extends ObjectCacheReader implements CacheInterface, LoggerAwareInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Clears the entire object cache.
     *
     * @param void
     * @return bool
     * @uses StoreCore\FileSystem\ObjectCacheReader::getObjectCacheDirectory()
     */
    public function clear(): bool
    {
        $files = glob($this->getObjectCacheDirectory() . '*.bin');
        if ($files === false) {
            return false;
        } elseif (empty($files)) {
            return true;
        }

        $result = true;
        foreach ($files as $filename) {
            if (is_file($filename)) {
                if (unlink($filename) !== true) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Deletes a single object from the cache.
     *
     * @param StoreCore\Types\UUID|string $key
     * @return bool
     * @uses StoreCore\FileSystem\ObjectCacheReader::getObjectCacheDirectory()
     * @uses StoreCore\Types\UUID::validate()
     */
    public function delete(UUID|string $key): bool
    {
        if (is_string($key) && !UUID::validate($key)) {
            throw new InvalidArgumentException();
        }

        try {
            $filename = $this->getObjectCacheDirectory() . $key . '.bin';
            return is_file($filename) ? unlink($filename) : false;
        } catch (Exception $e) {
            if ($this->logger !== null) {
                $this->logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            }
            return false;
        }
    }

    /**
     * Deletes multiple objects from the cache.
     *
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple(iterable $keys): bool
    {
        $result = true;
        foreach ($keys as $key) {
            if ($this->delete($key) !== true) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Gets a single object from the cache.
     *
     * @param StoreCore\Types\UUID|string $key
     * @param mixed $default
     * @uses StoreCore\FileSystem\ObjectCacheReader::get()
     * @uses StoreCore\Types\UUID::validate()
     */
    public function get(UUID|string $key, mixed $default = null): mixed
    {
        if (is_string($key) && !UUID::validate($key)) {
            throw new InvalidArgumentException('Invalid UUID: ' . $key, time());
        }

        try {
            return parent::get($key);
        } catch (Exception $e) {
            return $default;
        }
    }

    /**
     * Gets multiple object from the cache.
     * 
     * @param iterable $keys
     * @param mixed $default
     * @return array
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        return $result;
    }

    /**
     * Stores an object in the cache.
     *
     * @param StoreCore\Types\UUID|string $key
     * @param mixed $value
     * @param null|int|\DateInterval $ttl
     * @return bool
     * @uses StoreCore\FileSystem\ObjectCacheReader::getObjectCacheDirectory()
     * @uses StoreCore\Types\UUID::validate()
     */
    public function set(UUID|string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        if (is_string($key) && !UUID::validate($key)) {
            throw new InvalidArgumentException('Invalid UUID: ' . $key, time());
        }

        try {
            $filename = $this->getObjectCacheDirectory() . $key . '.bin';
            $value = serialize($value);
            $value = gzdeflate($value, 9, ZLIB_ENCODING_RAW);
            $result = file_put_contents($filename, $value, LOCK_EX);
            return $result === false ? false : true;
        } catch (Exception $e) {
            throw new CacheException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Stores multiple objects in the cache.
     *
     * @param iterable $values
     * @param null|int|\DateInterval $ttl
     * @return: bool
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $result = true;
        foreach ($values as $key => $value) {
            if ($this->set($key, $value, $ttl) !== true) {
                $result = false;
            }
        }
        return $result;
    }
}
