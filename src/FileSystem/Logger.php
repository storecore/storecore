<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2014–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\Log\{AbstractLogger, LoggerInterface, LogLevel};
use StoreCore\SubjectTrait;

/**
 * File system logger.
 *
 * @api
 * @package StoreCore\Security
 * @version 0.5.0
 */
class Logger extends AbstractLogger implements LoggerInterface, \SplSubject
{
    use SubjectTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var int CONTEXT_LIMIT
     *   Number of elements logged in a chain of events.  Defaults to 5.
     */
    protected const CONTEXT_LIMIT = 5;

    /**
     * @var string $filename
     *   Filename of the log text file, optionally with a file system path.
     */
    private readonly string $filename;

    /**
     * @var string|null $logLevel
     *   Log level keyword.
     */
    private ?string $logLevel = null;

    /**
     * @var string|null $message
     *   Log message text.
     */
    private ?string $message = null;

    /**
     * Creates a logger.
     *
     * @param string|null $filename
     *   OPTIONAL name of the log file, with or without a trailing path.  If
     *   the filename is not set, it defaults to the `YYYYMMDD.log` format
     *   with a `YYYY-MM-DD` date for daily log files.
     */
    public function __construct(?string $filename = null)
    {
        if ($filename === null || empty($filename)) {
            if (\defined('STORECORE_FILESYSTEM_LOGS_FILENAME_FORMAT')) {
                $filename = gmdate(STORECORE_FILESYSTEM_LOGS_FILENAME_FORMAT);
            } else {
                $filename = gmdate('Ymd');
            }
            $filename .= '.log';

            if (\defined('STORECORE_FILESYSTEM_LOGS_DIR')) {
                $filename = STORECORE_FILESYSTEM_LOGS_DIR . $filename;
            }
        }

        ini_set('log_errors', '1');
        ini_set('error_log', $filename);
        $this->filename = $filename;
    }


    /**
     * Gets the filename of the log file.
     *
     * @param void
     *
     * @return string
     *   Returns the filename of the log file, including the full path if that
     *   was set.
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Gets the last log message.
     *
     * @param void
     *
     * @return string|null
     *   Returns the last log message for the current process/request or null
     *   if there are no messages.  The main purpose of this method is allowing
     *   observers access to critical events.
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Gets the last log level.
     *
     * @param void
     *
     * @return \Psr\Log\LogLevel|null
     *   Returns the last log level for the current process/request or null
     *   if there are no logged events.
     */
    public function getLogLevel(): ?string
    {
        return $this->logLevel;
    }

    /**
     * Logs an event.
     *
     * @param \Psr\Log\LogLevel $level
     * @param string|\Stringable $message
     * @param array $context
     * @return void
     */
    public function log($level, string|\Stringable $message, array $context = array()): void
    {
        $this->logLevel = $level;
        $output = '[' . date(DATE_ISO8601) . '] [' . $this->getLogLevel() . '] ';

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $output .= '[client ' . $_SERVER['REMOTE_ADDR'] . '] ';
        }

        $this->message = trim((string) $message);
        $output .= $message;

        /* `print_r()` caused an insufficient memory error when running PHPUnit
         * with large failing unit tests, so we limit the context output.
         * Use the class constant `CONTEXT_LIMIT` to set the limit.
         */
        if (!empty($context)) {
            $context_count = count($context);
            if ($context_count > self::CONTEXT_LIMIT) {
                $context = \array_slice($context, 0, self::CONTEXT_LIMIT);
                $this->log(
                    LogLevel::DEBUG,
                    'Context count is ' . $context_count . ': only the first ' . self::CONTEXT_LIMIT . ' elements are logged.'
                );
            }
            $output .= ' [context ' . print_r($context, true) . ']';
        }

        $output .= PHP_EOL;
        error_log($output, 3, $this->getFilename());
        unset($output);

        // Notify observers and system logger
        if ($level == LogLevel::EMERGENCY || $level == LogLevel::ALERT || $level == LogLevel::CRITICAL) {
            $this->notify();
            if (!empty($message)) {
                if ($level == LogLevel::EMERGENCY) {
                    syslog(\LOG_EMERG, $message);
                } elseif ($level == LogLevel::ALERT) {
                    syslog(\LOG_ALERT, $message);
                } else {
                    syslog(\LOG_CRIT, $message);
                }
            }
        }
    }
}
