<?php

declare(strict_types=1);

namespace StoreCore\FileSystem;

use Psr\SimpleCache\CacheException as CacheExceptionInterface;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionInterface;

class InvalidArgumentException extends CacheException
implements CacheExceptionInterface, InvalidArgumentExceptionInterface, \Throwable
{
}
