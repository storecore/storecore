<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use StoreCore\Engine\ResponseFactory;
use StoreCore\Engine\TemporaryMemoryStream;

/**
 * Status endpoint.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class Status extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->handle($this->Request);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $factory = new ResponseFactory();
        $this->Response = $factory->createResponse(200);
        $this->Response->setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');

        if ($request->getMethod() === 'GET') {
            $this->Response->setHeader('Content-Type', 'text/plain');
            $message = new TemporaryMemoryStream('StoreCore is functioning normally.');
            $this->Response->setBody($message);
        }

        return $this->Response;
    }

    public function output(): never
    {
        $this->Response->output();
        exit;
    }
}
