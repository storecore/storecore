<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\CRM\OrganizationType;
use StoreCore\Types\URL;

/**
 * Online store.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/OnlineStore Schema.org type `OnlineStore`
 * @version 1.0.0-rc.1
 */
class OnlineStore extends OnlineBusiness implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @inheritDoc
     */
    public OrganizationType $type = OrganizationType::OnlineStore;

    /**
     * {@inheritDoc}
     * 
     * Sets the `Organization.additionalType` property to an `URL` that
     * points to the Schema.org `OnlineBusiness` type.
     */
    public function __construct(null|string|array $param = null)
    {
        parent::__construct($param);
        $this->additionalType = new URL('https://schema.org/OnlineBusiness');
    }
}
