<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\CRM\{Organization, Person};
use StoreCore\Types\UUID;

/**
 * Broker order.
 *
 * A _broker order_ is an order that is handled through an entity that is not
 * the seller or buyer.  The broker MAY be an `Organization` or a `Person`.
 * In most cases a broker never acquires or releases ownership of a `Product`
 * or `Service` involved in an exchange.  Two common e-commerce use cases for
 * broker orders are online marketplaces and dropshipping.
 *
 * @api
 * @package StoreCore\OML
 * @version 1.0.0
 */
class BrokerOrder extends Order implements
    \Countable,
    IdentityInterface,
    VisitableInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var StoreCore\Types\UUID $brokerIdentifier
     *   Identifier of an `Organization` or `Persion` that arranges for an
     *   exchange between a buyer and a seller.
     */
    private UUID $brokerIdentifier;

    /**
     * Gets the broker identifier.
     *
     * @param void
     *
     * @return StoreCore\Types\UUID
     *   Broker identifier as a universally unique identifier (UUID) object.
     */
    public function getBrokerIdentifier(): UUID
    {
        return $this->brokerIdentifier;
    }

    /**
     * Sets the broker.
     *
     * @param StoreCore\CRM\Organization|StoreCore\CRM\Person $broker
     *   Broker as an organization or a person.
     *
     * @return void
     *
     * @throws \DomainException
     *   Throws a domain exception if the broker has no identifier.
     */
    public function setBroker(Organization|Person $broker): void
    {
        if (!$broker->hasIdentifier()) {
            throw new \DomainException('Unknown broker');
        }
        $this->setBrokerIdentifier($broker->getIdentifier());
    }

    /**
     * Sets the broker identifier.
     *
     * @param StoreCore\Types\UUID|string $broker_uuid
     *   Universally unique identifier (UUID) as a value object or a string.
     *
     * @return void
     */
    public function setBrokerIdentifier(UUID|string $broker_uuid): void
    {
        if (\is_string($broker_uuid)) {
            $broker_uuid = new UUID($broker_uuid);
        }
        $this->brokerIdentifier = $broker_uuid;
    }
}
