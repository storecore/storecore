<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @see       https://github.com/php-fig/simple-cache/blob/master/src/CacheException.php
 */

declare(strict_types=1);

namespace StoreCore;

use \Exception;
use \Throwable;
use Psr\SimpleCache\CacheException as CacheExceptionInterface;

abstract class AbstractCacheException extends Exception implements
    CacheExceptionInterface,
    Throwable {}
