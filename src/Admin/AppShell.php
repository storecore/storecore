<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\AbstractController;
use StoreCore\DependencyException;
use StoreCore\Registry;
use StoreCore\View;

/**
 * StoreCore administation application shell.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class AppShell extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Loads the admin app shell.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     *
     * @throws \LogicException
     *   Throws an SPL logic exception on a missing admin user or a missing user session.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if (
            false === $this->Registry->has('User')
            || false === $this->Registry->has('Session')
        ) {
            throw new DependencyException();
        }

        $this->View = new View();
        $this->View->template = __DIR__ . DIRECTORY_SEPARATOR . 'AppShell.phtml';
        $html = $this->View->render();
    }
}
