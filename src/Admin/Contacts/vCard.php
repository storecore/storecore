<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin\Contacts;

use StoreCore\CRM\{Person, Organization};

/**
 * vCard.
 *
 * @package StoreCore\CRM
 * @see     https://en.wikipedia.org/wiki/VCard
 * @see     https://datatracker.ietf.org/doc/html/rfc6350
 * @see     https://devguide.calconnect.org/vCard/vcard-4/
 * @version 0.1.2
 */
class vCard implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.1';

    /**
     * Creates a vCard.
     *
     * @param StoreCore\CRM\Person $person
     * @param null|StoreCore\CRM\Organization $organization
     */
    public function __construct(
        private readonly Person $person,
        private readonly ?Organization $organization = null,
    ) {}

    /**
     * Outputs the vCard as a string
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        // `VERSION` must come immediately after `BEGIN`.
        $result = 'BEGIN:VCARD' . PHP_EOL . 'VERSION:4.0' .  PHP_EOL;

        // Name (N) and Full Name (FN).
        if ($this->person->name !== null) {
            $result .= 'N:' . $this->person->name->familyName . ';'
                . $this->person->name->givenName . ';'
                . $this->person->name->middleName . ';'
                . $this->person->name->honorificPrefix . ';'
                . $this->person->name->honorificSuffix . PHP_EOL;

            if (!empty($this->person->name->getFullName())) {
                $result .= 'FN:' . $this->person->name->getFullName() . PHP_EOL;
            } elseif (!empty($this->person->name->getDisplayName())) {
                $result .= 'FN:' . $this->person->name->getDisplayName() . PHP_EOL;
            }
        }

        $result .= 'END:VCARD';
        return $result;
    }
}
