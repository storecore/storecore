<?php
/**
 * StoreCore™ administration front controller.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2021 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0-alpha.1
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use Psr\Log\{LoggerAwareInterface, LoggerInterface};
use StoreCore\{AbstractController, AssetCacheReader, Registry, ResponseFactory, Route, Session};
use StoreCore\Admin\AccessControl;
use StoreCore\Database\RouteRepository;

class FrontController extends AbstractController implements LoggerAwareInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Constructs the StoreCore admin front controller.
     *
     * @param StoreCore\Registry $registry
     *   Global service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        // Static cacheable asset files.
        if (substr($this->Request->getRequestTarget(), -4) === '.css') {
            if (AssetCacheReader::matchRequest($this->Request) === true) {
                exit;
            }
        }

        // Run the installer on an incomplete installation.
        if (!\defined('STORECORE_GUID')) {
            $this->install();
        }

        // Check the access control list (ACL).
        $acl = new AccessControl($this->Registry);
        $acl->check();

        // Check if there is a user signed in.
        if ($this->Session->has('User')) {
            $this->User = $this->Session->get('User');
        } else {
            $factory = new ResponseFactory();
            $response = $factory->createRedirect('/admin/sign-in', 302);
            $response->output();
            exit;
        }

        // Find a matching route or route collection.
        $router = new RouteRepository($this->Registry);
        if ($router->has($this->Request->getRequestTarget())) {
            $route = $router->get($this->Request->getRequestTarget());
            $this->Registry->set('Route', $route);
            $route->dispatch();
        } else {
            $this->Logger->debug('Unknown admin route: ' . $this->Request->getRequestTarget());
            $factory = new ResponseFactory();
            $response = $factory->createResponse(404);
            $response->output();
            exit;
        }
    }

    /**
     * Runs the installer if the Installer.php class file exists.
     *
     * @param void
     * @return void
     */
    private function install(): void
    {
        if (is_file(__DIR__ . DIRECTORY_SEPARATOR . 'Installer.php')) {
            $this->Logger->warning('Installer loaded.');
            $route = new Route('/install', 'StoreCore\Admin\Installer');
            $this->Registry->set('Route', $route);
            $route->dispatch();
        } else {
            $this->Logger->notice('StoreCore core class file Installer.php not found.');
            $factory = new ResponseFactory();
            $response = $factory->createResponse(404);
            $response->output();
            exit;
        }
    }

    /**
     * Sets a logger.
     *
     * @param Psr\Log\LoggerInterface $logger
     *   PSR-3 “Logger Interface” compliant logger object.
     *
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->Logger = $logger;
        $this->Registry->set('Logger', $this->Logger);
    }
}
