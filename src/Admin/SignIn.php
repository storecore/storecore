<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\{AbstractController, Registry, View};
use StoreCore\Database\{LoginAttempts, UserRepository};
use StoreCore\Engine\{ResponseFactory, TemporaryMemoryStream};
use StoreCore\Types\FormToken;

use function \ini_get;
use function \is_string;
use function \pow;
use function \sleep;

/**
 * Administration sign-in.
 *
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class SignIn extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $token
     *   Handshake token that links a specific form to a specific session.
     */
    private string $token;

    /**
     * @param \StoreCore\Registry $registry
     *
     * @return void
     *
     * @uses \StoreCore\Engine\Request::getMethod()
     * @uses \StoreCore\Engine\ServerRequest::get()
     * @uses \StoreCore\Database\LoginAttempts::storeAttempt()
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if ($this->Request->getMethod() == 'GET') {
            $this->resetToken();
            $this->render();
            exit;
        } elseif ($this->Request->getMethod() != 'POST') {
            $this->resetToken();
            $factory = new ResponseFactory();
            $response = $factory->createResponse(405);
            $response->setHeader('Allow', 'GET, POST');
            $response->output();
            $this->Logger->notice('HTTP/' . $this->Request->getMethod() . ' 405 Method Not Allowed');
            exit;
        }

        if (
            !is_string($this->Server->get('email'))
            || !is_string($this->Server->get('password'))
            || !is_string($this->Server->get('token'))
        ) {
            $factory = new ResponseFactory();
            $response = $factory->createResponse(303);
            $response->redirect('/admin/sign-in', 303);
            exit;
        }

        // Audit failed and successful attempts
        $loginAttempts = new LoginAttempts($this->Registry);

        // HTTP response object
        $factory = new ResponseFactory();
        $response = $factory->createResponse();

        // Token handshake
        if ($this->Server->get('token') != $this->Session->get('Token')) {
            $this->Logger->notice('Token mismatch in admin sign-in. Redirecting client.');
            $this->resetToken();
            $response->redirect('/admin/sign-in', 303);
            exit;
        }

        // Check recent failed attempts
        $minutes = 15;
        $failed_attempts = $loginAttempts->count($minutes);
        if ($failed_attempts > 10) {
            $this->Logger->warning('There were over ' . $failed_attempts . ' failed admin sign-in attempts in the last ' . $minutes . ' minutes.');
        }

        // Connection throttling: pause for 2 ^ n seconds.
        // Maximum execution is set to the PHP default minus 5 seconds.
        $seconds = pow(2, (int)($failed_attempts / 10));
        $max_execution_time = (int) ini_get('max_execution_time') - 5;
        if ($seconds > $max_execution_time) {
            $seconds = $max_execution_time;
        }
        sleep($seconds);

        // Check if the user exists.
        $user_repository = new UserRepository($this->Registry);
        if ($user_repository->has($this->Server->get('email')) !== true) {
            $this->Logger->warning('Unknown user "' . $this->Server->get('email') . '" attempted to sign in.');
            $loginAttempts->storeAttempt($this->Server->get('email'));
            $this->resetToken();
            $response->redirect('/admin/sign-in', 303);
            exit;
        }

        // Try to fetch the user.
        try {
            $user = $user_repository->get($this->Server->get('email'));
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            $loginAttempts->storeAttempt($this->Server->get('email'));
            $this->resetToken();
            $response->redirect('/admin/sign-in', 303);
            exit;
        }

        // Check the user password.
        if ($user->authenticate($this->Server->get('password')) !== true) {
            $this->Logger->warning('Known user ' . $user->getIdentifier() . ' attempted to sign in with an illegal password.');
            $loginAttempts->storeAttempt($this->Server->get('username'));
            $this->resetToken();
            $response->redirect('/admin/sign-in', 303);
            exit;
        }

        // Finally, store the user and open up the administration.
        $this->Logger->notice('User "' . $user->getIdentifier() . ' signed in.');
        $loginAttempts->storeAttempt($this->Server->get('username'), null, true);
        $this->Session->set('User', $user);
        $response->redirect('/admin/', 303);
        exit;
    }

    /**
     * Retrieves the form token.
     *
     * @param void
     *
     * @return string
     *   Returns the active form token as a string.
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Renders the view.
     *
     * @param void
     * @return void
     */
    private function render(): void
    {
        $view = new View();
        $view->template = __DIR__ . DIRECTORY_SEPARATOR . 'SignIn.phtml';
        $view->setValues(array('token' => $this->getToken()));
        $view = $view->render();

        $document = new Document();
        $document->addSection($view);
        $document->setTitle(\StoreCore\I18N\COMMAND_SIGN_IN);

        /*
         * After 1 minute (60000 milliseconds) the current window times out,
         * JavaScript then redirects the client to the lock screen.
         */
        $document->addScript("window.setTimeout(function() { top.location.href = '/admin/lock/'; }, 60000);");

        $document = Minifier::minify($document);
        $document = new TemporaryMemoryStream($document);

        $factory = new ResponseFactory();
        $response = $factory->createResponse();
        $response->setHeader('Allow', 'GET, POST');
        $response->setHeader('X-Robots-Tag', 'noindex');
        $response->setBody($document);
        $response->output();
    }

    /**
     * Resets the form token.
     *
     * @param void
     * @return void
     * @uses StoreCore\Types\FormToken::getInstance()
     * @uses StoreCore\Session::set()
     */
    private function resetToken(): void
    {
        $this->token = FormToken::getInstance();
        $this->Session->set('Token', $this->token);
    }
}
