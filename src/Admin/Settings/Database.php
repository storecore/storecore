<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin\Settings;

use StoreCore\{LoggerFactory, Registry, View};
use StoreCore\Admin\Controller;
use StoreCore\Engine\ResponseFactory;

use function \is_string;

/**
 * Database settings controller.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
class Database extends Controller
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @param \StoreCore\Registry $registry
     *
     * @return void
     *
     * @uses \PDO::getAvailableDrivers()
     * @uses \StoreCore\Engine\Request::getMethod()
     * @uses \StoreCore\Engine\ServerRequest::get()
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if ($this->Request->getMethod() === 'POST') {
            $config = new Configurator();
            $save_config = false;

            // Database server host name or IP address
            $hostname = $this->Server->get('hostname');
            if ($hostname !== STORECORE_DATABASE_DEFAULT_HOST) {
                $config->set('STORECORE_DATABASE_DEFAULT_HOST', $hostname);
                $save_config = true;
            }

            // Database name
            $databasename = $this->Server->get('databasename');
            if ($databasename !== STORECORE_DATABASE_DEFAULT_DATABASE) {
                $config->set('STORECORE_DATABASE_DEFAULT_DATABASE', $databasename);
                $save_config = true;
            }

            // Database user account
            $username = $this->Server->get('username');
            if (is_string($username)) {
                $username = trim($username);
                if ($username !== STORECORE_DATABASE_DEFAULT_USERNAME && strlen($username) <= 16) {
                    $config->set('STORECORE_DATABASE_DEFAULT_USERNAME', $username);
                    $save_config = true;
                }
            }

            $password = $this->Server->get('password');
            if ($password !== null) {
                $password = trim($password);
                if ($password !== STORECORE_DATABASE_DEFAULT_PASSWORD) {
                    $config->set('STORECORE_DATABASE_DEFAULT_PASSWORD', $password);
                    $save_config = true;
                }
            }

            // Save configuration changes
            if ($save_config) {
                $config->save();
                $factory = new LoggerFactory();
                $logger = $factory->createLogger();
                $logger->notice('Database configuration saved.');
            }

            $factory = new ResponseFactory();
            $response = $factory->createResponse(303);
            $response->redirect('/admin/settings/database/');
        } else {
            $this->View = new View();
            $this->View->template = __DIR__ . DIRECTORY_SEPARATOR . 'SettingsDatabase.phtml';

            // Only select PDO drivers that are available in PHP as well as
            // supported by the StoreCore\Database.
            $supported_drivers = array(
                'mysql' => 'MySQL (' . \StoreCore\I18N\ADJECTIVE_DEFAULT . ')',
            );
            $this->View->setValues(array('available_drivers' => \PDO::getAvailableDrivers()));
            $this->View->setValues(array('supported_drivers' => $supported_drivers));

            $view = $this->View->render();
            $view = Minifier::minify($view);

            $document = new Document();
            $document->addSection($view, 'main');
            $factory = new ResponseFactory();
            $response = $factory->createResponse();
            $factory = new StreamFactory();
            $document = $factory->createStream($document);
            $response->setBody($document);
            $response->output();
        }
    }
}
