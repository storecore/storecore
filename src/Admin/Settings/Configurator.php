<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2014-2017, 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin\Settings;

use function \array_key_exists;
use function \file_put_contents;
use function \get_defined_constants;
use function \is_array;
use function \is_numeric;
use function \is_object;
use function \json_encode;
use function \ksort;
use function \ltrim;
use function \serialize;
use function \str_ireplace;
use function \strtr;
use function \unset;

/**
 * Configurator.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Configurator
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $ignoredSettings
     *   Settings that MUST be set manually in the config.php configuration
     *   file and settings that SHOULD NOT be defined by config.php.  For
     *   example, the four version constants are defined in version.php, so
     *   these cannot be defined in config.php.
     */
    private array $ignoredSettings = [
        'STORECORE_VERSION'       => true,
        'STORECORE_MAJOR_VERSION' => true,
        'STORECORE_MINOR_VERSION' => true,
        'STORECORE_PATCH_VERSION' => true,

        'STORECORE_FILESYSTEM_ROOT_DIR' => true,
    ];

    /**
     * @var array $settings
     *   Configuration settings saved to config.php.
     */
    private array $settings = [];

    public function __construct()
    {
        foreach ($this->ignoredSettings as $name => $value) {
            if ($value === false) {
                unset($this->ignoredSettings[$name]);
            }
        }

        /*
         * Parse user-defined constants and ignore constants
         * included in `$this->ignoredSettings`.
         */
        $defined_constants = get_defined_constants(true);
        if (array_key_exists('user', $defined_constants)) {
            $defined_constants = $defined_constants['user'];
            foreach ($defined_constants as $name => $value) {
                if (!array_key_exists($name, $this->ignoredSettings)) {
                    /*
                     * Only save constants with the `STORECORE_` prefix or
                     * constants from a `\StoreCore` namespace, but ignore all
                     * language string constants from the `\StoreCore\I18N` namespace.
                     */
                    if (
                        str_starts_with($name, 'STORECORE_')
                        || (str_starts_with($name, 'StoreCore\\') && !str_starts_with($name, 'StoreCore\\I18N\\'))
                    ) {
                        $this->settings[$name] = $value;
                    }
                }
            }
        }
    }

    /**
     * Saves the config.php configuration file.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function save(): bool
    {
        $file = '<?php' . "\n";
        ksort($this->settings);
        foreach ($this->settings as $name => $value) {
            /*
             * Namespace constants like `\Foo\BAR_BAZ` with a leading backslash
             * MUST be defined without the leading backslash in a PHP define:
             * `define('Foo\\BAR_BAZ', 1)`.
             */
            $name = ltrim($name, '\\');
            $name = str_ireplace('\\', '\\\\', $name);

            if ($value === true) {
                $file .= "define('{$name}', true);";
            } elseif ($value === false) {
                $file .= "define('{$name}', false);";
            } elseif (is_numeric($value)) {
                $file .= "define('{$name}', {$value});";
            } else {
                if (is_array($value)) {
                    $value = json_encode($value);
                } elseif (is_object($value)) {
                    $value = serialize($value);
                }
                $value = str_ireplace('\\', '\\\\', $value);
                $file .= "define('{$name}', '{$value}');";
            }
            $file .= "\n";
        }

        // Save config.php in the /config/ directory.
        $filename = STORECORE_FILESYSTEM_ROOT_DIR . 'config.php';
        $result = file_put_contents($filename, $file, LOCK_EX);
        return $result === false ? false : true;
    }

    /**
     * Sets a setting as a name-value pair.
     *
     * @param string $name
     *   Case-insensitive constant name of the setting.  Lowercase is converted
     *   to uppercase.  Spaces and hyphens are replaced by a single underscore.
     *
     * @param mixed $value
     *   Scalar value of the setting.  In PHP 5, `$value` must be an integer,
     *   float, string, boolean, or null; in PHP 7 and 8, array values are also
     *   accepted.
     *
     * @return void
     */
    public function set(string $name, mixed $value): void
    {
        $name = trim($name);
        if (empty($name)) throw new \ValueError();

        // Replace spaces and hyphens by underscores.
        $name = strtr($name, ' ', '_');
        $name = strtr($name, '-', '_');
        // Replace multiple underscores by a single underscore.
        $name = preg_replace('/_{1,}/', '_', $name);
        // Strip a leading or trailing underscore.
        $name = trim($name, '_');
        // Change to uppercase for proper constant naming.
        $name = strtoupper($name);

        if (!array_key_exists($name, $this->ignoredSettings)) {
            $this->settings[$name] = $value;
        }
    }

    /**
     * Writes a single setting to the config.php configuration file.
     *
     * @param string $name
     *   Constant name of the setting.
     *
     * @param mixed $value
     *   Value of the setting.
     *
     * @return bool
     */
    public static function write(string $name, mixed $value): bool
    {
        $writer = new Configurator();
        $writer->set($name, $value);
        return $writer->save();
    }
}
