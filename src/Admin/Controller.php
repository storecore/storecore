<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\{AbstractController, DependencyException, Registry};
use StoreCore\Admin\User as AdminUser;

/**
 * Administration constroller.
 *
 * The StoreCore `Admin\Controller` is an MVC controller that is used when
 * accessing backoffice or adninistration services that require some level
 * of access control.  This controller can not be instantiated if the current
 * request was not created by or for an active administration user.
 *
 * @package StoreCore\Security
 * @version 1.0.0
 */
class Controller extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates an admin controller.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     *
     * @throws StoreCore\DependencyException
     *   Throws a dependency logic exception if the global registry does not
     *   contain an active `User` or if this user is not an instance of the
     *   `StoreCore\Admin\User` class.
     */
    public function __construct(Registry $registry)
    {
        if (!$registry->has('User')) {
            throw new DependencyException('Missing User in StoreCore registry.');
        } elseif (!($registry->get('User') instanceof AdminUser)) {
            throw new DependencyException('Registered User is not a StoreCore\Admin\User.');
        } else {
            parent::__construct($registry);
        }
    }
}
