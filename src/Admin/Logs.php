<?php

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Registry;
use StoreCore\Engine\ResponseFactory;
use StoreCore\FileSystem\LogFileManager;

/**
 * Logs.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Security
 * @version   0.2.0
 */
class Logs extends Controller
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';


    /**
     * @var StoreCore\FileSystem\LogFileManager $model
     *   File system model for the logs directory and log files.
     */
    private LogFileManager $model;


    /**
     * Downloads all log files combined to a single text file.
     *
     * @param void
     * @return void
     * @uses \StoreCore\FileSystem\LogFileManager::read()
     */
    public function download(): void
    {
        $factory = new ResponseFactory();

        $filename = gmdate('Ymd\THis\Z') . '.log';
        $this->model = new LogFileManager();
        $download = $this->model->read();

        // “204 No Content” if there is nothing to download
        if ($download === null) {
            $response = $factory->createResponse(204);
            $response->output();
            exit;
        }

        $response = $factory->createResponse();
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->setHeader('Content-Type', 'text/plain; charset=UTF-8');
        $response->setHeader('Content-Disposition', 'attachment; filename="' . $filename . '"');
        $response->setHeader('Expires', '0');
        $response->setHeader('Pragma', 'public');
        $response->setBody($download);
        $response->output();
        exit;
    }
}
