<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\User as FrameworkUser;
use StoreCore\{LoggerFactory, Registry, ResponseFactory, SessionFactory};

/**
 * StoreCore admin user.
 *
 * @package StoreCore\Security
 * @version 0.3.1
 *
 * @uses StoreCore\User
 *   The `StoreCore\Admin\User` administration user extends the more generic
 *   `StoreCore\User` framework user.  Note that the user is NOT a webshop
 *   customer or website visitor, but an entity that uses framework services.
 */
class User extends FrameworkUser
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.1';

    /**
     * Sign out an admin user.
     *
     * Log off the user by destroying the user session, log the activity,
     * and redirect the client to a lock screen.
     *
     * @param void
     *
     * @return never
     *
     * @uses StoreCore\Session::destroy()
     *   When the user signs out, the user session is destroyed.
     */
    public function signOut(): never
    {
        $registry = Registry::getInstance();
        $factory = new SessionFactory($registry);
        $session = $factory->createSession();
        $session->destroy();

        $factory = new LoggerFactory();
        $logger = $factory->createLogger();
        $logger->info('User ' . $this->getIdentifier() . ' signing out.');

        $factory = new ResponseFactory();
        $response = $factory->createResponse(303);
        $response->redirect('/admin/lock/');
        exit;
    }
}
