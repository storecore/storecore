<?php

/**
 * @author    Ward van der Put <ward.vanderput@storecore.org>
 * @copyright Copyright © 2017, 2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

/**
 * Service worker controller.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 */
class ServiceWorker
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @param void
     * @return never
     */
    public function __construct()
    {
        ob_start('ob_gzhandler');
        header('Content-Type: application/javascript');
        readfile(__DIR__ . DIRECTORY_SEPARATOR . 'ServiceWorker.js');
        exit;
    }
}
