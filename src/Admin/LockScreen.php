<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\{AbstractController, Registry, View};
use StoreCore\Admin\Document;
use StoreCore\Engine\{ResponseFactory, StreamFactory};

use function \define;
use function \defined;

/**
 * Admin lock screen.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class LockScreen extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Constructs and outputs the lock screen.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     *
     * @return void
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if (!defined('STORECORE\\I18N\\COMMAND_UNLOCK')) {
            define('STORECORE\\I18N\\COMMAND_UNLOCK', 'Unlock');
        }

        $this->View = new View();
        $this->View->template = __DIR__ .  DIRECTORY_SEPARATOR . 'LockScreen.phtml';
        $html = $this->View->render();

        $document = new Document();
        $document->write($html);
 
        $factory = new ResponseFactory();
        $this->Response = $factory->createResponse();
        $this->Response->setHeader('X-Robots-Tag', 'noindex');

        $factory = new StreamFactory();
        $document = $factory->createStream($document);
        $this->Response->setBody($document);

        $this->Response->output();
    }
}
