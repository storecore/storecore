<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2016, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use function \chr;
use function \random_int;
use function \str_shuffle;
use function \strlen;

/**
 * Random password.
 *
 * @package StoreCore\Security
 * @version 1.0.0
 */
class RandomPassword implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var int $length
     *   Length of the password in characters.
     */
    private readonly int $length;

    /**
     * @var string $password
     *   Password.
     */
    private string $password;

    /**
     * Creates a random password.
     *
     * @param int $length
     *   Length of the password.  The PCI DSS standard requires passwords to
     *   contain at least seven characters in uppercase and lowercase letters.
     */
    public function __construct(int $length = 12)
    {
        if ($length < 12) {
            $length = 12;
        }
        $this->length = $length;
        $this->randomize();
    }

    /**
     * @param void
     * @return string
     * @uses \StoreCore\Admin\RandomPassword::get()
     */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * Gets the random password.
     *
     * @param void
     *
     * @return string
     *
     * @uses \StoreCore\Admin\RandomPassword::randomize()
     *   A random password may be “published” only once.  Once the random
     *   password is used outside of the scope of this class, a new random
     *   password is generated.
     */
    public function get(): string
    {
        $result = $this->password;
        $this->randomize();
        return $result;
    }

    /**
     * @internal
     * @param void
     * @return void
     */
    private function randomize(): void
    {
        // Password must contain at least one number.
        $password = random_int(0, 9);

        // Password must contain at least one uppercase letter.
        $password .= chr(random_int(65, 90));

        // Password must contain at least one lowercase letter.
        $password .= chr(random_int(97, 122));

        // Password must contain at least one special character.
        $password .= str_shuffle('!?@#$%*(){}[]')[0];

        do {
            switch (random_int(0, 3)) {
                case 0:
                    $password .= random_int(0, 9);
                    break;
                case 1:
                    $password .= chr(random_int(65, 90));
                    break;
                case 2:
                    $password .= chr(random_int(97, 122));
                    break;
                default:
                    $password .= str_shuffle('!?@#$%*(){}[]')[0];
            }
            $password = str_shuffle($password);
        } while (strlen($password) !== $this->length);

        $this->password = $password;
    }
}
