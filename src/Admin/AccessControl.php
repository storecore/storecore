<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\AbstractController;
use StoreCore\Engine\ResponseFactory;
use StoreCore\Database\AccessControlList;

/**
 * Access control list (ACL) controller.
 *
 * @package StoreCore\Security
 * @version 0.3.0
 */
class AccessControl extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * Checks if an IP address is granted admin access.
     *
     * @param void
     * 
     * @return null|never
     *   Terminates the execution if a blocked IP address is found.
     *
     * @uses StoreCore\Database\AccessControlList::isEmpty()
     *   This method first tests if there are any active trusted IP addresses.
     *   This check assumes the access control list is not used, or can
     *   no longer be used, if it currently has no active entries.
     * 
     * @uses StoreCore\Database\AccessControlList::exists()
     *   Checks if the current IP address has admin access.
     */
    public function check(): void
    {
        try {
            $acl = new AccessControlList($this->Registry);
            if ($acl->isEmpty()) return;
        } catch (\Exception $e) {
            $this->Logger->error('Database access control list (ACL) is not accessible');
            return;
        }

        if ($acl->exists($this->Server->getRemoteAddress(), true)) {
            return;
        }

        $this->Logger->warning('Access denied to remote IP address: ' . $this->Server->getRemoteAddress());
        $factory = new ResponseFactory();
        $response = $factory->createResponse(404);
        $response->output();
        exit;
    }
}
