<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2016, 2023 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Admin;

use function \preg_replace;
use function \str_ireplace;
use function \stripos;

/**
 * Minify HTML or CSS.
 *
 * @package StoreCore\CMS
 * @version 0.2.0
 */
class Minifier
{
    /**
     * @var string VERSION
     */
    public const string VERSION = '0.2.0';

    /**
     * Minifies a string value.
     *
     * @param \Stringable|string $str
     *   String to parse.
     *
     * @param bool $css
     *   Parse CSS (`true`) or HTML (default `false`);
     *
     * @return string
     */
    public static function minify(\Stringable|string $str, bool $css = false): string
    {
        $str = str_ireplace("\r\n", "\n", (string) $str);
        $str = str_ireplace("\r", "\n", $str);
        $str = str_ireplace("\t", ' ', $str);

        $str = preg_replace('!\s+!', ' ', $str);

        if (stripos($str, '<pre') !== false) {
            $css = false;
        } else {
            $str = str_ireplace("\n ", '', $str);
            $str = str_ireplace("\n", '', $str);
        }
        $str = str_ireplace('> <', '><', $str);

        if ($css === true) {
            $str = str_ireplace(': ', ':', $str);
            $str = str_ireplace(', ', ',', $str);
            $str = str_ireplace(' {', '{', $str);
            $str = str_ireplace(';}', '}', $str);
        }

        return $str;
    }
}
