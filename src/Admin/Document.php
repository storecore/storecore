<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Types\Link;

use function \empty;
use function \str_replace;
use function \strip_tags;
use function \trim;

/**
 * Admin GUI document.
 *
 * @api
 * @package StoreCore\Core
 * @version 0.3.0
 */
class Document extends \DOMDocument implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * Creates a new StoreCore admin web page.
     *
     * @param void
     *
     * @see https://stackoverflow.com/questions/6090667/php-domdocument-errors-warnings-on-html5-tags
     *      PHP DOMDocument errors/warnings on HTML5 tags
     */
    public function __construct()
    {
        parent::__construct('1.0', 'UTF-8');
        $this->strictErrorChecking = false;
        $this->preserveWhiteSpace = false;
        $this->loadHTMLFile(__DIR__ . DIRECTORY_SEPARATOR . 'Document.phtml');
    }

    /**
     * Converts the document object to HTML.
     *
     * @param void
     *
     * @return string
     *   Returns the full normalized HTML document as a string.
     */
    public function __toString(): string
    {
        $this->normalizeDocument();
        $html = $this->saveHTML();

        // Rough minification
        $html = str_replace("\r\n", "\n", $html);
        $html = str_replace(["\n    ", "\n  ", "\n"], '', $html);

        return $html;
    }

    /**
     * Appends a child node to the document body element.
     *
     * @uses \DOMNode::appendChild()
     */
    private function appendBodyChild(\DOMNode $dom_node): \DOMNode
    {
        $parent = $this->getElementsByTagName('body')->item(0);
        return $parent->appendChild($dom_node);
    }

    /**
     * Sets the document title.
     * 
     * @param string $title
     *   New document title.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error if the title is an empty string.
     */
    public function setTitle(string $title): void
    {
        $title = strip_tags($title);
        $title = trim($title);
        if (empty($title)) {
            throw new \ValueError();
        } else {
            $this->getElementsByTagName('title')->item(0)->nodeValue = $title;
        }
    }

    /**
     * Writes HTML to the document body.
     *
     * @param string $html
     *   HTML code to add to the document’s `<body>` container.
     *
     * @return void
     */
    public function write(string $html): void
    {
        $fragment = $this->createDocumentFragment();
        $fragment->appendXML($html);
        $this->appendBodyChild($fragment);
    }
}
