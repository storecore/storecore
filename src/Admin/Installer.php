<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\{AbstractController, Registry, Route, User, View};
use StoreCore\Admin\{Document, Minifier};
use StoreCore\Admin\Configurator;
use StoreCore\Database\Maintenance as DatabaseMaintenance;
use StoreCore\Database\{DataSourceName, Password, UserRepository};
use StoreCore\Engine\{ResponseFactory, StreamFactory};
use StoreCore\Types\EmailAddress;
use StoreCore\Types\UUIDFactory;

use function \function_exists;
use function \unlink;
use function \trim;

/**
 * StoreCore installer.
 *
 * @package StoreCore\Core
 * @version 0.1.0
 */
class Installer extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var bool $selfDestruct
     *   If set to `true` (default `false`), the destructor will try to delete the
     *   class file, which effectively disables a new or repeated installation.
     */
    private bool $selfDestruct = false;

    /**
     * Executes chained installation procedures.
     *
     * @param \StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if (false === $this->Registry->has('Logger')) {
            $factory = new LoggerFactory();
            $this->Logger = $factory->createLogger();
            $this->Registry->set('Logger', $this->Logger);
        }

        // Set multibyte character encoding to UTF-8
        mb_internal_encoding('UTF-8');

        // Run subsequent tests on success (true)
        if (
            $this->checkServerRequirements()
            && $this->checkFileSystem()
            && $this->checkDatabaseConnection()
            && $this->checkDatabaseStructure()
            && $this->checkUsers()
        ) {
            $config = new Configurator();
            $config->set('STORECORE_GUID', $this->getGUID());
            $config->set('STORECORE_MAINTENANCE_MODE', true);
            $config->save();

            $this->Logger->notice('Completed installation of StoreCore version ' . STORECORE_VERSION . '.');
            $this->selfDestruct = true;

            $factory = new ResponseFactory();
            $response = $factory->createResponse(302);
            $response->redirect('/admin/sign-in');
            exit;
        }
    }

    /**
     * Destroys the class file if the installation was completed successfully.
     *
     * @param void
     * @return void
     */
    public function __destruct()
    {
        if (true === $this->SelfDestruct) {
            if (true === unlink(__FILE__)) {
                $this->Logger->notice('Deleted the StoreCore installer file ' . __FILE__ . '.');
            } else {
                $this->Logger->notice('Could not delete the StoreCore installer file ' . __FILE__ . '.');
            }
        }
    }

    /**
     * Checks the database DSN and database account.
     *
     * @param void
     *
     * @return bool
     *   Returns true if the database connection is set up correctly,
     *   otherwise false.
     */
    private function checkDatabaseConnection()
    {
        try {
            $dsn = new DataSourceName();
            $dbh = new \PDO((string) $dsn, STORECORE_DATABASE_DEFAULT_USERNAME, STORECORE_DATABASE_DEFAULT_PASSWORD, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
            $this->Registry->set('Database', $dbh);
        } catch (\PDOException $e) {
            $this->Logger->critical($e->getMessage());
            if ($this->Request->getRequestTarget() !== '/admin/settings/database/account') {
                $factory = new ResponseFactory();
                $response = $factory->createResponse(302);
                $response->redirect('/admin/settings/database/account');
            } else {
                $route = new Route('/admin/settings/database/account', 'StoreCore\Admin\SettingsDatabaseAccount');
                $route->dispatch();
            }
            return false;
        }
        $this->Logger->info('Database connection is set up correctly.');
        return true;
    }

    /**
     * Checks the database structure and optionally installs the database.
     *
     * @param void
     *
     * @return bool
     *   Returns true if the database tables appear to be set up correctly,
     *   otherwise false.
     *
     * @uses StoreCore\Admin\Configurator
     *
     * @uses StoreCore\Database\Maintenance
     */
    private function checkDatabaseStructure(): bool
    {
        try {
            $database = new DatabaseMaintenance($this->Registry);
            if ($database->updateAvailable) {
                $this->Logger->notice('Installing StoreCore database...');
                $database->restore();

                $config = new Configurator();
                $config->set('STORECORE_DATABASE_VERSION_INSTALLED', STORECORE_VERSION);
                $config->save();
                $this->Logger->notice('StoreCore database version ' . STORECORE_VERSION . ' was installed.');
            }
        } catch (\PDOException $e) {
            $this->Logger->critical($e->getMessage());
            return false;
        }
        $this->Logger->info('Database structure is set up correctly.');
        return true;
    }

    /**
     * Checks files and folders.
     *
     * @param void
     *
     * @return bool
     *   Returns true if the file system is set up correctly, otherwise false.
     */
    private function checkFileSystem(): bool
    {
        $errors = array();

        // Cache subdirectories for data, objects and pages
        if (!defined('STORECORE_FILESYSTEM_CACHE_DATA_DIR')) {
            define('STORECORE_FILESYSTEM_CACHE_DATA_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'data' . DIRECTORY_SEPARATOR);
        }
        if (!defined('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR')) {
            define('STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'objects' . DIRECTORY_SEPARATOR);
        }
        if (!defined('STORECORE_FILESYSTEM_CACHE_PAGES_DIR')) {
            define('STORECORE_FILESYSTEM_CACHE_PAGES_DIR', STORECORE_FILESYSTEM_CACHE_DIR . 'pages' . DIRECTORY_SEPARATOR);
        }

        $folders = array(
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'css'  => true,
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'ico'  => false,
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'jpeg' => true,
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'png'  => true,
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'svg'  => false,
            STORECORE_FILESYSTEM_ROOT_DIR . 'public' . DIRECTORY_SEPARATOR . 'webp' => true,
            STORECORE_FILESYSTEM_CACHE_DIR => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR => true,
            STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR => true,
            STORECORE_FILESYSTEM_CACHE_PAGES_DIR => true,
            STORECORE_FILESYSTEM_LOGS_DIR => true,
        );
        foreach ($folders as $filename => $must_be_writable) {
            if (!is_dir($filename)) {
                $errors[] = 'Bad or missing directory: ' . $filename;
            } elseif ($must_be_writable) {
                if (!is_writable($filename)) {
                    $errors[] = 'Wrong directory permissions: ' . $filename . ' is not writable.';
                }
            }
        }

        $files = [
            STORECORE_FILESYSTEM_ROOT_DIR . 'config.php' => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR . 'de-DE.php' => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR . 'en-GB.php' => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR . 'en-US.php' => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR . 'fr-FR.php' => true,
            STORECORE_FILESYSTEM_CACHE_DATA_DIR . 'nl-NL.php' => true,
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'core-mysql.sql' => false,
            STORECORE_FILESYSTEM_SRC_DIR . 'Database' . DIRECTORY_SEPARATOR . 'i18n-dml.sql' => false,
        ];
        foreach ($files as $filename => $must_be_writable) {
            if (!is_file($filename)) {
                $errors[] = 'Bad or missing file: ' . $filename;
            } elseif ($must_be_writable && !is_writable($filename)) {
                $errors[] = 'Wrong file permissions: ' . $filename . ' is not writable.';
            }
        }

        if (count($errors) == 0) {
            $this->Logger->info('File system is set up correctly.');
            return true;
        } else {
            foreach ($errors as $error) {
                $this->Logger->critical($error);
            }
            return false;
        }
    }

    /**
     * Checks if the current server meets the core requirements.
     *
     * @param void
     *
     * @return bool
     *   Returns true if the server meets all requirements, otherwise false.
     *   If a requirement is not met, the system error is logged to the file
     *   system as a critical condition.
     */
    private function checkServerRequirements(): bool
    {
        $errors = array();

        if (version_compare(phpversion(), '5.3.0', '<')) {
            $errors[] = 'PHP version ' . phpversion() . ' is not supported.';
        }

        if (!extension_loaded('PDO')) {
            $errors[] = 'PHP extension PDO is not loaded.';
        } elseif (STORECORE_DATABASE_DRIVER === 'mysql' && !extension_loaded('pdo_mysql')) {
            $errors[] = 'PHP extension PDO for MySQL (pdo_mysql) is not loaded.';
        } elseif (!in_array(STORECORE_DATABASE_DRIVER, \PDO::getAvailableDrivers(), true)) {
            $errors[] = 'PDO driver ' . STORECORE_DATABASE_DRIVER . ' is not available.';
        }

        if (count($errors) == 0) {
            $this->Logger->info('Web server configuration is set up correctly.');
            return true;
        } else {
            foreach ($errors as $error) {
                $this->Logger->critical($error);
            }
            return false;
        }
    }

    /**
     * Checks for user account(s).
     *
     * @param void
     *
     * @return bool
     *   Returns true if there is at least one active admin user account,
     *   otherwise false.
     *
     * @uses StoreCore\Database\UserRepository::count()
     * 
     * @uses StoreCore\Types\EmailAddress
     * 
     * @uses StoreCore\User
     */
    private function checkUsers(): bool
    {
        $users = new UserRepository($this->Registry);
        if ($users->count() !== 0) {
            $this->Logger->info('User accounts are set up correctly.');
            return true;
        }

        unset($users);
        $this->Logger->warning('No active user accounts were found: adding a new user with administrator privileges.');

        $user = new User();
        $user->setUserGroupID(254);
        $user_data = array(
            'email_address' => false,
            'pin_code' => $user->getPIN(),
        );

        if ($this->Request->getMethod() == 'POST') {

            // E-mail address
            if ($this->Server->get('email_address') !== null) {
                try {
                    $email_address = new EmailAddress($this->Server->get('email_address'));
                    $user->setEmailAddress($email_address);
                    $user_data['email_address'] = $user->getEmailAddress();
                } catch (\Exception $e) {
                    $this->Logger->warning('Invalid e-mail address: ' . $this->Server->get('email_address'));
                }
            }

            // Optional personal identification number (PIN number or PIN code)
            if ($this->Server->get('pin_code') !== null) {
                try {
                    $pin_code = trim($this->Server->get('pin_code'));
                    $user->setPIN($pin_code);
                    $user_data['pin_code'] = $user->getPIN();
                    unset($pin_code);
                } catch (\Exception $e) {
                    $this->Logger->notice('Invalid PIN number: ' . $this->Server->get('pin_code'));
                }
            }

            // Password
            $user_has_password = false;
            if (
                $user->getEmailAddress() !== null
                && is_string($this->Server->get('password'))
                && is_string($this->Server->get('confirm_password'))
                && $this->Server->get('password') == $this->Server->get('confirm_password')
                && mb_stristr($this->Server->get('password'), $user_data['username']) === false
                && \StoreCore\Admin\PasswordCompliance::validate($this->Server->get('password')) === true
                && \StoreCore\Database\CommonPassword::exists($this->Server->get('password')) === false
            ) {
                $password = new Password();
                $password->setPassword($this->Server->get('password'));
                $password->encrypt();
                $user->setPasswordSalt($password->getSalt());
                $user->setHashAlgorithm($password->getAlgorithm());
                $user->setPasswordHash($password->getHash());
                unset($password);
                $user_has_password = true;
            }

            // Insert user
            if ($user_has_password && !in_array(false, $user_data, true)) {
                try {
                    $user_repository = new UserRepository($this->Registry);
                    $user_repository->save($user);
                    $this->Logger->notice(
                        'User account created for: ' . $user->getIdentifier 
                        . ' at ' . $user->getEmailAddress()
                    );
                    return true;
                } catch (\Exception $e) {
                    $this->Logger->critical($e->getMessage());
                }
            }
        }

        // Try to find a known administrator or general e-mail address
        if ($user_data['email_address'] === false) {
            if (!empty($_SERVER['SERVER_ADMIN'])) {
                $email_address = filter_var($_SERVER['SERVER_ADMIN'], FILTER_SANITIZE_EMAIL);
                if (filter_var($email_address, FILTER_VALIDATE_EMAIL) !== false) {
                    $user_data['email_address'] = $email_address;
                }
            }
        }
        if ($user_data['email_address'] === false) {
            $email_address = ini_get('sendmail_from');
            if ($email_address !== false && !empty($email_address)) {
                $email_address = filter_var($email_address, FILTER_SANITIZE_EMAIL);
                if (filter_var($email_address, FILTER_VALIDATE_EMAIL) !== false) {
                    $user_data['email_address'] = $email_address;
                }
            }
        }

        // Create a view to add an account
        foreach ($user_data as $name => $value) {
            if ($value === false || $value === null) {
                $user_data[$name] = '';
            }
        }

        $view = new View();
        $view->template = __DIR__ . DIRECTORY_SEPARATOR . 'User.phtml';
        $view->setValues($user_data);

        $form = $view->render();
        $form = Minifier::minify($form);

        $document = new Document();
        $document->addSection($form, 'main');

        $factory = new StreamFactory();
        $document = $factory->createStream($document);

        $factory = new ResponseFactory();
        $response = $factory->createResponse();
        $response->setBody($document);
        $response->output();

        $this->Logger->info('Displaying user account form.');
        return false;
    }

    /**
     * Generates a Globally Unique Identifier (GUID).
     *
     * @param void
     *
     * @return string
     *   Returns the GUID (Globally Unique Identifier) as a string.
     */
    private function getGUID(): string
    {
        if (function_exists('com_create_guid')) {
            return trim(com_create_guid(), '{}');
        } else {
            return (string) UUIDFactory::pseudoRandomUUID();
        }
    }
}
