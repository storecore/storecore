<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License 
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use function \ctype_alpha;
use function \ctype_digit;
use function \ctype_lower;
use function \ctype_upper;
use function \function_exists;
use function \is_numeric;
use function \is_string;
use function \strlen;

/**
 * PCI DSS password compliance.
 *
 * @package StoreCore\Security
 * @version 0.2.0
 */
class PasswordCompliance
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var int MINIMUM_LENGTH
     *   Current minimum length of a password.
     */
    public const MINIMUM_LENGTH = 12;

    /**
     * Validate the password compliance.
     *
     * @param string $password
     *
     * @return bool
     *   Returns `true` if the password matches the current compliance
     *   rule set, otherwise `false`.
     *
     * @todo
     *   Currently there are only two strict PCI DSS requirements.  Passwords
     *   and pass phrases (i) require a minimum length of at least twelve
     *   characters and (ii) must contain both numeric and alphabetic
     *   characters.  Alternatively, the passwords/phrases MUST have
     *   complexity and strength at least equivalent to the parameters
     *   specified above, so the rule set MAY be extended.
     *
     * @see https://listings.pcisecuritystandards.org/documents/PCI-DSS-v4-0-SAQ-A.pdf
     *   Self-Assessment Questionnaire A and Attestation of Compliance,
     *   for use with PCI DSS Version 4.0.
     */
    public static function validate(#[\SensitiveParameter] string $password): bool
    {
        // Nothing to do.
        if (!is_string($password)) {
            throw new \TypeError();
        }

        // Not a number (NaN).
        if (is_numeric($password)) {
            return false;
        }

        // Require a minimum length of at least 12 characters.
        if (function_exists('mb_strlen')) {
            if (mb_strlen($password) < self::MINIMUM_LENGTH) {
                return false;
            }
        } else {
            if (strlen($password) < self::MINIMUM_LENGTH) {
                return false;
            }
        }

        // Require both numeric and alphabetic characters.
        if (ctype_digit($password) || ctype_alpha($password)) {
            return false;
        }

        // Require both uppercase and lowercase characters.
        if (ctype_lower($password) || ctype_upper($password)) {
            return false;
        }

        return true;
    }
}
