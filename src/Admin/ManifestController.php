<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Admin;

use StoreCore\Engine\ResponseFactory;
use StoreCore\{AbstractController, Registry, View};

/**
 * Administration web app manifest controller.
 *
 * @package StoreCore\CMS
 * @version 0.2.0
 *
 * @see https://web.dev/add-manifest/
 *      Add a web app manifest - Progressive Web Apps - web.dev
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/Manifest
 *      Web app manifests - Web technologies for developers - MDN web docs
 *
 * @see https://www.w3.org/TR/appmanifest/
 *      Web App Manifest - W3C Working Draft 27 July 2020
 */
class ManifestController extends AbstractController
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\Admin\ManifestModel $model
     *   MVC model.
     */
    private readonly ManifestModel $model;

    /**
     * @var StoreCore\View $view
     *   MVC view.
     */
    private View $view;

    /**
     * Creates a web app manifest in JSON.
     *
     * @param StoreCore\Registry $registry
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        $this->model = new ManifestModel($this->Registry);
        $manifest_members = [
            'manifest_version' => ManifestModel::MANIFEST_VERSION,
            'name' => $this->model->getName(),
            'short_name' => $this->model->getShortName(),
            'start_url' => 'https://' . $this->Location->getHost() . '/admin/',
            'background_color' => $this->model->getBackgroundColor(),
            'theme_color' => $this->model->getThemeColor(),
        ];

        $this->view = new View();
        $this->view->template = __DIR__ .  DIRECTORY_SEPARATOR . 'ManifestTemplate.phtml';
        $this->view->setValues($manifest_members);

        $factory = new StreamFactory();
        $manifest = $factory->createStream($this->view->render());

        $factory = new ResponseFactory();
        $this->Response = $factory->createResponse();
        $this->Response->setHeader('Content-Type', 'application/manifest+json;charset=UTF-8');
        $this->Response->setBody($manifest);
        $this->Response->output();
    }
}
