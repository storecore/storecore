<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \Countable;

/**
 * Pre-order model.
 *
 * @api
 * @package StoreCore\OML
 * @version 0.1.0
 */
class PreOrder extends Order implements
    Countable,
    IdentityInterface,
    VisitableInterface
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
