<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;

/**
 * Online business.
 *
 * A particular online business, either standalone or the online part of
 * a broader `Organization`.  Examples include an ecommerce site, an online
 * travel booking site, an online learning site, an online logistics and
 * shipping provider, an online (virtual) doctor, etc.
 *
 * Class hierarchy:
 *
 * - `class Organization`
 * - `class OnlineBusiness extends Organization`
 * - `class OnlineStore extends OnlineBusiness`
 *
 * @package StoreCore\Core
 * @see     https://schema.org/OnlineBusiness Schema.org type `OnlineBusiness`
 * @version 1.0.0-rc.1
 */
class OnlineBusiness extends Organization implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @inheritDoc
     */
    public OrganizationType $type = OrganizationType::OnlineBusiness;
}
