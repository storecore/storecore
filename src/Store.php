<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \DateTimeZone;
use \JsonSerializable;
use StoreCore\CRM\Organization;
use StoreCore\Database\Metadata;

use function \array_filter;
use function \ksort;

/**
 * StoreCore store model.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Store extends AbstractModel implements IdentityInterface, JsonSerializable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $currencies
     *   Array with `StoreCore\Currency` objects for the store’s currencies.
     */
    private array $currencies;

    /**
     * @var \DateTimeZone $dateTimeZone
     *   Default date and time zone of the store.
     */
    public DateTimeZone $dateTimeZone;

    /**
     * @var bool $defaultFlag
     *   This is the default store (`true`) or some other store (default
     *   `false`).  At any given time there MAY be only one default store.
     */
    private bool $defaultFlag = false;

    /**
     * @var bool $enabledFlag
     *   Determines whether the store is “open” to the public (`true`) or not
     *   (default `false`).
     */
    private bool $enabledFlag = false;

    /**
     * @var array $languages
     *   Languages used for this store’s storefront.  (The backoffice language
     *   is determined by the admin user’s preferences and MAY be used for
     *   multiple stores.)
     */
    private array $languages = [
        'en-GB' => true,
    ];

    /**
     * @var StoreCore\Database\Metadata|null $metadata
     *   StoreCore database metadata.
     */
    public ?Metadata $metadata = null;

    /**
     * @var string|null $name
     *   Generic short name of the store.  The store name MUST be unique.
     */
    public ?string $name = null;

    /**
     * @var null|StoreCore\CRM\Organization $organization
     *   OPTIONAL `Organization` model for the store.
     */
    public ?Organization $organization = null;

    /**
     * Creates a store model object.0
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->dateTimeZone = new DateTimeZone('UTC');

        $default_currency = new Currency();
        $this->setCurrencies([$default_currency->id => $default_currency]);
    }

    /**
     * Closes the store.
     *
     * @param void
     * @return void
     */
    public function close(): void
    {
        $this->enabledFlag = false;
    }

    /**
     * Gets the store’s currencies.
     *
     * @param void
     *
     * @return array
     *   Returns an indexed array with the store’s currencies with ISO currency
     *   numbers as the keys and `StoreCore\Currency` objects as the values.
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * Gets the store’s storefront languages.
     *
     * @param void
     *
     * @return array
     *   Returns an associative array with language IDs as the keys and a
     *   boolean as the values.  The boolean value is set to `true` if the
     *   language is currently enabled for the storefront.  The first array
     *   element is the store’s default language.
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * Gets and/or sets the default store.
     *
     * @param null|bool $is_default_store
     *   OPTIONAL parameter that makes this store the default store (`true`)
     *   or not (`false`).  By default, a store is not the default store,
     *   because there can only be one default store at any given time.
     *
     * @return bool
     *   Returns `true` if this store is the default store, otherwise `false`.
     */
    public function isDefault(?bool $is_default_store = null): bool
    {
        if ($is_default_store !== null) {
            $this->defaultFlag = $is_default_store;
        }
        return $this->defaultFlag;
    }

    /**
     * Checks if a store is open or closed.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` if the store currently is open and `false` if it is
     *   closed.  A store is closed (`false`) by default, unless it was
     *   explicitly opened with the `open()` method.
     */
    public function isOpen(): bool
    {
        return $this->enabledFlag;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = [];
        $properties = [];
        if ($this->hasIdentifier()) {
            $result['@id'] = '/api/v1/stores/' . $this->getIdentifier();
            $properties['identifier'] = $this->getIdentifier()->__toString();
        }
        $result['@context'] = 'https://schema.org';
        $result['@type'] = 'OnlineStore';

        // Inherit an `Organization.name` if the store name was not set.
        if (
            empty($this->name)
            && !empty($this->organization)
            && !empty($this->organization->name)
        ) {
            $this->name = $this->organization->name;
        }

        if (!empty($this->name)) {
            $properties['name'] = $this->name;
        }

        if (!empty($this->organization)) {
            $organization = $this->organization->jsonSerialize();
            $organization = array_filter($organization);
            if (isset($organization['@id'])) {
                $properties['sameAs'] = $organization['@id'];
            }
        }

        ksort($properties);
        return $result + $properties;
    }

    /**
     * Opens the store.
     *
     * @param void
     * @return void
     */
    public function open(): void
    {
        $this->enabledFlag = true;
    }

    /**
     * Sets the store’s currencies.
     *
     * @param array $store_currencies
     * @return void
     */
    public function setCurrencies(array $store_currencies): void
    {
        $this->currencies = $store_currencies;
    }

    /**
     * Sets the store’s storefront languages.
     *
     * @param array $store_languages
     * @return void
     */
    public function setLanguages(array $store_languages): void
    {
        $this->languages = $store_languages;
    }
}
