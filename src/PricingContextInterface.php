<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Context of a pricing strategy.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
interface PricingContextInterface extends PricingStrategyInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Sets a pricing strategy.
     *
     * @param StoreCore\PricingStrategyInterface $strategy
     * @return void
     */
    public function setPricingStrategy(PricingStrategyInterface $strategy): void;
}
