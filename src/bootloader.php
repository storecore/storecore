<?php

/**
 * StoreCore framework bootloader.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0-alpha.1
 */

declare(strict_types=1);

// Global script/request timer start
if (empty($_SERVER['REQUEST_TIME_FLOAT'])) {
    $_SERVER['REQUEST_TIME_FLOAT'] = microtime(true);
}

// Set the default character set to UTF-8.
ini_set('default_charset', 'UTF-8');
mb_internal_encoding('UTF-8');

// Coordinated Universal Time (UTC)
date_default_timezone_set('UTC');

// Set the framework library root directory in case it was not set
// (for example, if the bootloader was included/required directly).
if (!\defined('STORECORE_FILESYSTEM_SRC_DIR')) {
    $dir = realpath(__DIR__);
    if ($dir !== false && is_dir($dir)) {
        define('STORECORE_FILESYSTEM_SRC_DIR', $dir . DIRECTORY_SEPARATOR);
    } else {
        define('STORECORE_FILESYSTEM_SRC_DIR', __DIR__ . DIRECTORY_SEPARATOR);
    }
}

/**
 * Exception error handler.
 * 
 * @param int $code The error or exception number.
 * @param string $message The error or exception message.
 * @param null|string $filename The filename where the error occured.
 * @param null|int $line The line number where the error occured.
 */
function exception_error_handler(int $code = 0, string $message = '', ?string $filename = null, ?int $line = null)
{
    $factory = new \StoreCore\LoggerFactory();
    $logger = $factory->createLogger();
    $logger->debug($message . ' in ' . $filename . ' on line ' . $line);
    throw new \ErrorException($message, $code, 1, $filename, $line);
}

// Handle PHP errors as exceptions.
set_error_handler('exception_error_handler', E_ALL & ~E_NOTICE);
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 0);

// Load Composer autoloader `vendor/autoload.php`.
// Remember to include own functions BEFORE including Composer’s `vendor/autoload.php`.
$dir = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor');
if ($dir === false) {
    $dir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor';
}
require $dir . DIRECTORY_SEPARATOR . 'autoload.php';
unset($dir);

// Load and populate the global service locator.
$registry = \StoreCore\Registry::getInstance();

// Clock
$registry->set('Clock', new \StoreCore\Clock());

// Logger
$factory = new \StoreCore\LoggerFactory();
$logger = $factory->createLogger();
$registry->set('Logger', $logger);

// Server, URI, and Request
$server = new \StoreCore\Engine\ServerContainer();
$registry->set('Server', $server);

$uri = \StoreCore\Engine\UriFactory::getCurrentUri();
$registry->set('URI', $uri);

$factory = new \StoreCore\Engine\ServerRequestFactory();
$request = $factory->createServerRequest($server->get('REQUEST_METHOD'), $uri, $server->jsonSerialize());
if (isset($_COOKIE) && !empty($_COOKIE)) $request->setCookieParams($_COOKIE);
if (isset($_GET) && !empty($_GET)) $request->setQueryParams($_GET);
if (isset($_POST) && !empty($_POST)) $request->setParsedBody($_POST);
$registry->set('Request', $request);

// Check the client IP blocklist if the `STORECORE_CMS_BLOCKLIST` constant
// is defined and the constant is not set to `false` or `0`.
if (\defined('STORECORE_CMS_BLOCKLIST') && !empty(STORECORE_CMS_BLOCKLIST)) {
    $handler = new \StoreCore\Engine\AccessControlHandler($registry);
    $response = $handler->handle($registry->get('Request'));
    if ($response->getStatusCode() === 403) {
        $response->output();
        $logger->info(
            'HTTP/' . $response->getProtocolVersion() . ' 403 Forbidden: client IP address '
            . $registry->get('Request')->getRemoteAddress() . ' is blocked.'
        );
        exit;
    }
    unset($handler, $response);
}

// This leaves two variables: $registry and $logger
unset($factory, $request, $server, $uri);
