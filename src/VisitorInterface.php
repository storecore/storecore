<?php

declare(strict_types=1);

namespace StoreCore;

/**
 * Visitor design pattern: visitor interface.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @version   1.0.0
 * 
 * @see https://en.wikipedia.org/wiki/Visitor_pattern
 *      Visitor pattern, Wikipedia
 *
 * @see https://refactoring.guru/design-patterns/visitor/php/example
 *      Visitor in PHP, example by Refactoring Guru
 *
 * @see https://doeken.org/blog/visitor-pattern
 *      Using the Visitor Pattern in PHP, by Doeke Norg
 */
interface VisitorInterface
{
    public function visitOrder(Order $order): void;
}
