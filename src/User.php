<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Database\Password;
use StoreCore\I18N\Languages;
use StoreCore\Types\{EmailAddress, LanguageCode};

/**
 * StoreCore user.
 *
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class User
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var \DateTimeZone $dateTimeZone
     * @var string|null   $hashAlgorithm
     * @var string        $languageID
     * @var string|null   $passwordHash
     * @var string|null   $passwordSalt
     * @var string|null   $personUUID
     * @var string        $pinCode
     * @var int           $userGroupID
     */
    private \DateTimeZone $dateTimeZone;
    private ?string $hashAlgorithm = null;
    private string $languageID;
    private ?string $passwordHash = null;
    private ?string $passwordSalt = null;
    private ?string $personUUID = null;
    private string $pinCode = '0000';
    private int $userGroupID = 0;

    /**
     * @var StoreCore\Types\EmailAddress|null $emailAddress
     *   E-mail address as a value object.
     */
    private ?EmailAddress $emailAddress = null;

    /**
     * Creates a new user object.
     *
     * @param void
     */
    public function __construct()
    {
        $this->dateTimeZone = new \DateTimeZone('UTC');
        $this->languageID = Languages::DEFAULT_LANGUAGE;
    }

    /**
     * Checks the user credentials.
     *
     * @param string $password
     *
     * @return bool
     *   Returns true if the user credentials check out and the user may be
     *   granted access to parts of the StoreCore framework, otherwise false.
     *   Members of user group 0 (zero) are always denied access.
     */
    public function authenticate(string $password): bool
    {
        if (!\is_string($password) || empty($password)) {
            return false;
        }

        if ($this->getUserGroupID() === null || $this->getUserGroupID() < 1) {
            return false;
        }

        if ($this->passwordHash === null || $this->passwordSalt === null) {
            return false;
        }

        if ($this->hashAlgorithm == 'SHA-1') {
            $hash = sha1($this->passwordSalt . $password);
        } else {
            $hash_factory = new Password();
            $hash_factory->setPassword($password);
            $hash_factory->setSalt($this->passwordSalt);
            $hash_factory->encrypt();
            $hash = $hash_factory->getHash();
        }

        if ($hash == $this->passwordHash) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the user’s timezone.
     *
     * @param void
     *
     * @return \DateTimeZone
     *   Returns a DateTimeZone object.  The default timezone for all
     *   dates, times, and datetimes in StoreCore is 'UTC'.
     */
    public function getDateTimeZone(): \DateTimeZone
    {
        return $this->dateTimeZone;
    }

    /**
     * Gets the e-mail address.
     *
     * @param void
     * @return StoreCore\Types\EmailAddress|null
     */
    public function getEmailAddress(): ?EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * Gets the password hash algorithm.
     *
     * @param void
     * @return string|null
     */
    public function getHashAlgorithm(): ?string
    {
        return $this->hashAlgorithm;
    }

    /**
     * Gets the user’s language identifier.
     *
     * @param void
     *
     * @return string
     *   Returns a string like 'en-GB' (default) for the language ID.
     */
    public function getLanguageID(): string
    {
        return $this->languageID;
    }

    /**
     * Get the password hash.
     *
     * @param void
     * @return string|null
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * Gets the password salt.
     *
     * @param void
     * @return string|null
     */
    public function getPasswordSalt(): string
    {
        return $this->passwordSalt;
    }

    /**
     * Gets the person UUID of this user.
     *
     * @param void
     * @return string|null
     */
    public function getPersonUUID(): ?string
    {
        return $this->personUUID;
    }

    /**
     * Gets the personal identification number (PIN).
     *
     * @param void
     *
     * @return string
     *   Returns a numeric string with 4, 5 or 6 digits.  Defaults to '0000' if
     *   no other PIN code was set.
     */
    public function getPIN(): string
    {
        return $this->pinCode;
    }

    /**
     * Gets the user group identifier.
     *
     * @param void
     * @return int
     */
    public function getUserGroupID(): int
    {
        return $this->userGroupID;
    }

    /**
     * Chechks whether the user has an e-mail address.
     *
     * @param void
     *
     * @return bool
     *   Returns true if the user has an e-mail address, otherwise false.
     */
    public function hasEmailAddress(): bool
    {
        return $this->emailAddress === null ? false : true;
    }

    /**
     * Sets the user’s timezone.
     *
     * @param \DateTimeZone $timezone
     *   DateTimeZone object for the user’s current date and time zone.
     *
     * @return void
     */
    public function setDateTimeZone(\DateTimeZone $timezone): void
    {
        $this->dateTimeZone = $timezone;
    }

    /**
     * Sets the e-mail address.
     *
     * @param StoreCore\Types\EmailAddress|string $email_address
     * @return void
     */
    public function setEmailAddress(EmailAddress|string $email_address): void
    {
        if (\is_string($email_address)) $email_address = new EmailAddress($email_address);
        $this->emailAddress = $email_address;
    }

    /**
     * Sets the password hash algorithm.
     *
     * @param string $hash_algorithm
     * @return void
     */
    public function setHashAlgorithm(string $hash_algorithm): void
    {
        $this->hashAlgorithm = $hash_algorithm;
    }

    /**
     * Sets the user’s language identifier.
     *
     * @param StoreCore\Types\LanguageCode|string $language_id
     *   Alphanumeric language identifier like 'en-GB' for British English
     *   or 'en-US' for American English.  If the current timezone is set
     *   to the generic 'UTC' (default) and a single timezone exists for the
     *   country code, the country code is also used to set the timezone.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the `$language_id` is a string
     *   that cannot be converted to a valid language code.
     */
    public function setLanguageID(LanguageCode|string $language_id): void
    {
        if (\is_string($language_id)) $language_id = new LanguageCode($language_id);
        $this->languageID = (string) $language_id;
    }

    /**
     * Sets the password hash.
     *
     * @param string $password_hash
     * @return void
     */
    public function setPasswordHash(string $password_hash): void
    {
        $this->passwordHash = $password_hash;
    }

    /**
     * Sets the password salt.
     *
     * @param string $password_salt
     * @return void
     */
    public function setPasswordSalt(string $password_salt): void
    {
        $this->passwordSalt = $password_salt;
    }

    /**
     * Sets the UUID of the person associated with this user.
     *
     * @param string $person_uuid
     * @return void
     */
    public function setPersonUUID(string $person_uuid): void
    {
        $this->personUUID = $person_uuid;
    }

    /**
     * Sets the personal identification number (PIN).
     *
     * @param string|int $pin_code
     *   PIN code as a numeric string.  An integer is converted to a numeric string.
     *
     * @return void
     *
     * @throws \UnexpectedValueException
     *   Throws an unexpected value runtime exception if the `$pin_code`
     *   parameter is not a number consisting of 4, 5 or 6 digits.
     */
    public function setPIN(string|int $pin_code): void
    {
        if (\is_int($pin_code)) $pin_code = (string) abs($pin_code);

        if (!ctype_digit($pin_code) || \strlen($pin_code) < 4 || \strlen($pin_code) > 6) {
            throw new \UnexpectedValueException();
        }
        $this->pinCode = $pin_code;
    }

    /**
     * Sets the user group identifier.
     *
     * @param int $user_group_id
     *   Unique user group identifier.
     *
     * @return void
     *
     * @throws \DomainException
     *   Throws a domain logic exception if the `$user_group_id` integer value
     *   is less than 0 or greater than 255, the default unsigned range of a
     *   `TINYINT` in MySQL.
     */
    public function setUserGroupID(int $user_group_id): void
    {
        if ($user_group_id < 0 || $user_group_id > 255) {
            throw new \DomainException();
        }

        $this->userGroupID = $user_group_id;
    }
}
