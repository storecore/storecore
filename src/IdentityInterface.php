<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Types\UUID;

/**
 * Identity interface.
 *
 * The StoreCore `IdentityInterface` is used to communicate the identity
 * of a shared object.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
interface IdentityInterface
{
    public function getIdentifier(): UUID|null;
    public function hasIdentifier(): bool;
    public function setIdentifier(UUID|string $uuid): void;
}
