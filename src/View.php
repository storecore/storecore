<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \ReflectionClass;
use \Stringable;
use \ValueError;

use function \array_merge;
use function \empty;
use function \extract;
use function \is_file;
use function \is_string;
use function \ob_get_clean;
use function \ob_start;
use function \str_ends_with;
use function \str_ireplace;
use function \strval;
use function \trim;

/**
 * MVC view.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
class View implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var array $data
     *   Array containing the names and values of PHP variables to include in
     *   the MVC view.
     */
    protected array $data = [];

    /**
     * @var null|string $template
     *   OPTIONAL file name of a template to include in the view.
     */
    public ?string $template = null
    {
        get => $this->template;

        set (?string $value) {
            if (is_string($value)) {
                $value = trim($value);
            }

            if (empty($value)) {
                $this->template = null;
            } elseif (!str_ends_with($value, '.phtml')) {
                throw new ValueError('Invalid template file name: ' . $value);
            } elseif (!is_file($value)) {
                throw new DependencyException('Template file not found: ' . $value);
            } else {
                $this->template = $value;
            }
        }
    }

    /**
     * Constructs an MVC view.
     *
     * @param null|string $template
     *   OPTIONAL template to include in the view.
     *
     * @param null|array $values
     *   OPTIONAL key-value pairs to render as PHP variables in the view.
     */
    public function __construct(?string $template = null, ?array $values = null)
    {
        if ($template !== null) $this->template = $template;
        if ($values !== null) $this->setValues($values);
    }

    /**
     * Sets a view parameter.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        if ($value instanceof Stringable) $value = (string) $value;
        $this->data[$name] = is_string($value) ? $value : strval($value);
    }

    /**
     * Returns the rendered view as a string.
     *
     * @param void
     * @return string
     * @uses render()
     */
    public function __toString(): string
    {
        return $this->render();
    }

    /**
     * Sets a missing template filename.
     *
     * This method uses the class name of the MVC view to find a matching
     * `.phtml` PHP HTML template file in the same directory.
     *
     * @param void
     * @return void
     * @uses \ReflectionClass::getFileName()
     */
    public function mapTemplate(): void
    {
        $filename = (new ReflectionClass($this))->getFileName();
        $filename = str_ireplace('.php', '', $filename) . '.phtml';
        $this->template = $filename;
    }

    
    /**
     * Adds variable values to the view.
     *
     * @param array $values
     *   Flat key-value array containing the name and the value of one or more
     *   PHP variables to include in the view.  This method MAY be called
     *   several times to add more data to the view.
     *
     * @return void
     */
    public function setValues(array $values): void
    {
        $this->data = array_merge($this->data, $values);
    }

    /**
     * Renders the view.
     *
     * @param void
     *
     * @return string
     *   Returns a string with the parsed template and values.  In most cases
     *   the returned string is an HTML fragment.
     */
    public function render(): string
    {
        if ($this->template === null) $this->mapTemplate();
        if (!empty($this->data)) extract($this->data);

        ob_start();
        include $this->template;
        return ob_get_clean();
    }
}
