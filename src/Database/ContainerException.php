<?php

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerExceptionInterface;

/**
 * Generic exception in a container that implements the PSR-11 Container Interface.
 *
 * @see https://github.com/php-fig/container/blob/master/src/ContainerExceptionInterface.php
 */
class ContainerException extends \RuntimeException implements
    ContainerExceptionInterface,
    \Throwable {}
