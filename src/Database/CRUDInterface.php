<?php

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * CRUD interface.
 *
 * Interface to Create, Read, Update, and Delete (CRUD) data objects.
 * An abstraction of this CRUD interface is provided by the abstract
 * class `StoreCore\Database\AbstractDataAccessObject`.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\Core
 * @see       https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
 * @version   2.0.0
 */
interface CRUDInterface
{
    public function create(array $keyed_data): string|false;

    public function read(mixed $value, ?string $key = null): array|false;

    public function update(array $keyed_data, ?string $where_clause = null): bool;

    public function delete(mixed $value, ?string $key = null): bool;
}
