<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\CRM\Person;
use StoreCore\Types\UUID;

/**
 * Person repository.
 *
 * @package StoreCore\CRM
 * @version 0.2.1
 */
class PersonRepository extends AbstractModel implements \Countable, ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * Anonymizes PII (personally identifiable information).
     *
     * @param StoreCore\CRM\Person $person
     *   Person to anonymize.
     * 
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function anonymize(Person &$person): bool
    {
        // Nothing to do on an unknown person
        if (!$person->hasIdentifier()) {
            return false;
        }

        $person_uuid = (string) $person->getIdentifier();
        $person_uuid = str_replace('-', '', $person_uuid);

        try {
            $in_transaction = $this->Database->beginTransaction();

            // Revoke an API account or admin user account for an anonymized user:
            $statement = $this->Database->prepare(
                'UPDATE `sc_users` SET `user_group_id` = 0, `person_uuid` = NULL, `email_address` = NULL WHERE `person_uuid` = UNHEX(?)'
            );
            $statement->bindValue(1, $person_uuid, \PDO::PARAM_STR);
            $statement->execute();

            // Delete associations with organizations:
            $statement = $this->Database->prepare(
                'DELETE FROM `sc_person_organizations` WHERE `person_uuid` = UNHEX(?)'
            );
            $statement->bindValue(1, $person_uuid, \PDO::PARAM_STR);
            $statement->execute();

            // Delete associations with addresses:
            $statement = $this->Database->prepare(
                'DELETE FROM `sc_person_addresses` WHERE person_uuid = :person_uuid'
            );
            $statement->bindValue(':person_uuid', $person_uuid, \PDO::PARAM_STR);
            $statement->execute();

            /* Delete associations with audiences:
             *
                    DELETE
                      FROM sc_audience_persons
                     WHERE person_uuid = :person_uuid
             */
            $statement = $this->Database->prepare('DELETE FROM sc_audience_persons WHERE person_uuid = :person_uuid');
            $statement->bindValue(':person_uuid', $person_uuid, \PDO::PARAM_STR);
            $statement->execute();

            // Delete most personal data:
            $person->anonymize();
            $this->save($person);

            // Commit and report full transaction:
            return $this->Database->commit();

        } catch (\PDOException $e) {
            if ($in_transaction === true) {
                $this->Database->rollBack();
            }
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }


    /**
     * Counts the number of persons.
     *
     * @param void
     *
     * @return int
     *   Returns the number of known persons as an integer.  Person records
     *   marked for deletion and anonymized persons are not included in the
     *   count.
     */
    public function count(): int
    {
        try {
            $statement = $this->Database->query(
                'SELECT COUNT(*) FROM `sc_persons` WHERE `anonymized_flag` != 1 AND `date_deleted` IS NULL'
            );
            $row = $statement->fetch(\PDO::FETCH_NUM);
            return (int) $row[0];
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return 0;
        }
    }

    /**
     * Gets an existing person by the person ID.
     *
     * @param string $id
     *   Universally unique identifier (UUID) of the person as a string.
     *
     * @return StoreCore\CRM\Person
     *   Returns a `Person` model object on success.
     *
     * @throws Psr\Container\NotFoundExceptionInterface
     *   Throws a PSR-11 ContainerInterface “Not Found” exception if the
     *   requested `Person` could not be found.
     *
     * @throws Psr\Container\ContainerExceptionInterface
     *   Throws a generic PSR-11 container exception on other errors.
     */
    #[Override]
    public function get(string $id): Person
    {
        try {
            $mapper = new PersonMapper($this->Registry);
            $person = $mapper->get($id);
            if ($person === null) throw new NotFoundException();
            return $person;
        } catch (NotFoundException $e) {
            throw $e;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if a person exists.
     *
     * @param string $id
     *   Public universally unique identifier (UUID) of the person.
     *
     * @return bool
     *   Returns `true` if the person exists, otherwise `false`.
     */
    #[Override]
    public function has(string $id): bool
    {
        $id = strtolower(trim($id));
        if (UUID::validate($id) !== true) {
            return false;
        }

        try {
            $sth = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_persons` WHERE `person_uuid` = :person_uuid'
            );
            $sth->bindValue(':person_uuid', $id, \PDO::PARAM_STR);
            if ($sth->execute() === false) return false;
            $count = (int) $sth->fetchColumn(0);
            $sth->closeCursor();
            unset($sth);
            return $count === 1 ? true : false;
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * Persists a person in the repository.
     *
     * @param StoreCore\CRM\Person &$person
     * @uses PersonMapper::save()
     */
    public function set(Person &$person): bool
    {
        $mapper = new PersonMapper($this->Registry);
        return $mapper->save($person);
    }
}
