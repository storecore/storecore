<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \Countable;
use \DomainException;
use Psr\Clock\ClockInterface;
use Psr\Container\ContainerInterface;
use StoreCore\ClockTrait;
use StoreCore\Registry;
use StoreCore\PIM\{AbstractProductOrService, Product, Service};
use StoreCore\PIM\{ProductGroup, ProductModel};
use StoreCore\PIM\ItemAvailability;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\URL;
use StoreCore\Types\{UUID, UUIDFactory};

use function \array_filter;
use function \array_key_exists;
use function \array_merge;
use function \count;
use function \ctype_digit;
use function \empty;
use function \is_array;
use function \is_int;
use function \is_string;
use function \str_replace;
use function \strlen;
use function \strtolower;
use function \trim;
use function \unset;

/**
 * Product data mapper.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
class ProductMapper extends AbstractModel implements ContainerInterface, Countable
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\IdentityMap $identityMap
     *   Key-value store that maps public UUIDs to internal integer IDs.
     */
    public IdentityMap $identityMap;

    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->identityMap = new IdentityMap();
    }

    /**
     * Counts the number of active products.
     *
     * @param void
     *
     * @return int
     *   Returns the number of active products as an integer.  A product is
     *   considered to be “active” if its sales are not discontinued.
     *   A product is “inactive” if its Schema.org `ItemAvailability` is set to
     *   `Discontinued`.  A product that is sold out or out of stock is still
     *   handled as an “active” product because this limited availability is
     *   usually temporary and the product should still be listed in a catalogue.
     * 
     * @see https://schema.org/ItemAvailability
     *   Schema.org `ItemAvailability` property
     */
    public function count(): int
    {
        $query = <<<'SQL'
               SELECT COUNT(*)
                 FROM `sc_products` AS `p`
            LEFT JOIN `sc_product_availability_types` AS `a`
                   ON `p`.`availability_id` = `a`.`availability_id`
                WHERE `p`.`service_flag` <> b'1'
                  AND `p`.`product_group_flag` <> b'1'
                  AND `a`.`item_availability` <> 'https://schema.org/Discontinued'
        SQL;
        $statement = $this->Database->query($query);
        return $statement->fetchColumn(0);
    }

    /**
     * Gets a product from the database.
     *
     * @param mixed $id
     *   Unique identifier of the product.
     *
     * @return StoreCore\PIM\AbstractProductOrService
     */
    #[\Override]
    public function get(mixed $id): AbstractProductOrService
    {
        if (!is_int($id)) {
            if ($this->has($id)) {
                $id = $this->identityMap->get($id);
            } else {
                throw new NotFoundException();
            }
        }

        $associations = new ProductAssociations($this->Registry);
        $variants = $associations->getVariantsHierarchy($id);
        unset($associations);

        if (count($variants) === 1) {
            $data = $this->read($id);
            if ($data === false) {
                return null;
            }
        } else {
            $data = [];
            foreach ($variants as $id => $uuid) {
                $delta = $this->read($id);
                if (is_array($delta)) {
                    $delta = array_filter($delta);
                    $data = array_merge($data, $delta);
                }
            }
        }

        return $this->toObject($data);
    }

    #[\Override]
    public function has(mixed $id): bool
    {
        if (!is_int($id)) {
            if (UUID::validate($id)) {
                $id = str_replace('-', '', (string) $id);
                $id = strtolower($id);
            } else {
                if (ctype_digit($id)) {
                    $id = (int) $id;
                } else {
                    return false;
                }
            }
        }

        $query = 'SELECT `product_id`, HEX(`product_uuid`) AS `product_uuid` FROM `sc_products` ';
        if (is_int($id)) {
            if ($id < 1 || $id > 4294967295) {
                return false;
            }
            $query .= 'WHERE BINARY `product_id` = ?';
        } else {
            $query .= 'WHERE BINARY `product_uuid` = UNHEX(?)';
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            if (is_int($id)) {
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $row = $statement->fetch(\PDO::FETCH_NUM);
                $statement->closeCursor();
                unset($query, $statement, $id);
                if (is_array($row)) {
                    $this->identityMap->set(new UUID($row[1]), $row[0]);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Reads product data for one product from the database.
     *
     * @param int $id
     *   Internal primary key of the product.
     *
     * @return array|false
     *   Returns a key-value array on success or `false` failure.
     */
    private function read(int $id): array|false
    {
        $query = <<<'SQL'
            SELECT *, HEX(`product_uuid`) AS `uuid`
              FROM `sc_products`
             WHERE `product_id` = ?
             LIMIT 1
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $id, \PDO::PARAM_INT);
            if ($statement->execute() !== false) {
                $result = $statement->fetch(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                unset($id, $query, $statement);
                if (is_array($result)) {
                    $result['product_uuid'] = $result['uuid'];
                    unset($result['uuid']);
                    return $result;
                }
            }
        }

        return false;
    }

    /**
     * Converts a key-value array to a product or service object.
     *
     * @internal
     * @param array $data
     * @return StoreCore\PIM\AbstractProductOrService
     */
    private function toObject(array $data): AbstractProductOrService
    {
        if ($data['service_flag'] === 1) {
            $result = new Service();
        } else {
            if ($data['product_group_flag'] === 1) {
                $result = new ProductGroup();
            } elseif ($data['product_model_flag'] === 1) {
                $result = new ProductModel();
            } elseif ($data['individual_product_flag'] === 1) {
                $result = new IndividualProduct();
                if (!empty($data['serial_number'])) {
                    $result->serialNumber = $data['serial_number'];
                }
            } else {
                $result = new Product();
            }
        }

        if (!empty($data['product_uuid'])) {
            $result->setIdentifier($data['product_uuid']);
        }

        $result->metadata = new Metadata($data['date_created'], $data['date_modified']);
        $result->metadata->version = $data['version'];

        if (!empty($data['gtin'])) {
            if (strlen($data['gtin']) === 13) {
                $result->gtin13 = $data['gtin'];
            } else {
                $result->gtin = $data['gtin'];
            }
        }

        $result->availability = ItemAvailability::fromInteger($data['availability_id']);
    
        if (!empty($data['release_date'])) {
            $result->releaseDate = Date::createFromFormat('Y-m-d', $data['release_date'], new \DateTimeZone('UTC'));
        }

        $result->metadata->hash = Metadata::hash($result);
        return $result;
    }
}
