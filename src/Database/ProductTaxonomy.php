<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\PIM\GoogleProductTaxonomy;
use StoreCore\Types\LanguageCode;

/**
 * Product taxonomy.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
class ProductTaxonomy extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Adds product taxonomy entries in a given language.
     * 
     * @param StoreCore\Types\LanguageCode $language 
     *   The taxonomy language to install.
     * 
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function install(LanguageCode $language): bool
    {
        $language = $language->__toString();
        if (!isset(GoogleProductTaxonomy::SUPPORTED_LANGUAGES[$language])) {
            $this->Logger->notice('Language ' . $language . ' is not supported.');
            return false;
        }

        if (!checkdnsrr('www.google.com', 'AAAA')) {
            if (!checkdnsrr('www.google.com', 'A')) {
                $this->Logger->error('DNS check could not resolve AAAA and A records for www.google.com.');
                return false;
            }
        }

        $filename = 'https://www.google.com/basepages/producttype/taxonomy-with-ids.' . $language . '.txt';
        $file = file_get_contents($filename, false);
        if ($file === false) {
            return false;
        }

        $file = str_replace("\r\n", "\n", $file);
        $file = explode("\n", $file);

        $statement = $this->Database->prepare(
            'INSERT IGNORE INTO `sc_product_taxonomy` (`taxonomy_id`, `language_id`, `full_path`) '
            . "VALUES (:taxonomy_id, '" . $language . "', :full_path)"
        );
        $statement->bindParam(':taxonomy_id', $key, \PDO::PARAM_INT);
        $statement->bindParam(':full_path', $value, \PDO::PARAM_STR);

        foreach ($file as $line) {
            $line = explode(' - ', $line, 2);
            if (\count($line) === 2 && ctype_digit($line[0])) {
                $key = (int) $line[0];
                $value = $line[1];
                $statement->execute();
            }
        }

        $statement->closeCursor();
        return true;
    }

    /**
     * Resets the product taxonomy table.
     *
     * Removes all toxonomy entries in languages other the American English (`en-US`)
     * and then repopulates the database table with all American English entries.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function reset(): bool
    {
        $filename = __DIR__ . DIRECTORY_SEPARATOR . 'taxonomy-with-ids.en-US.sql';
        if (is_file($filename) && is_readable($filename)) {
            $sql = file_get_contents($filename);
            if ($this->Database->beginTransaction()) {
                $this->Database->exec("DELETE FROM `sc_product_taxonomy` WHERE `language_id` <> 'en-US'");
                $this->Database->exec($sql);
                return $this->Database->commit();
            }
        }
        return false;
    }

    /**
     * Removes product taxonomy entries in a given language.
     * 
     * @param StoreCore\Types\LanguageCode $language 
     *   The taxonomy language to install.  The default language American
     *   English (en-US) cannot be uninstalled.
     * 
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function uninstall(LanguageCode $language): bool
    {
        $language = $language->__toString();
        if (
            $language === 'en-US'
            || !isset(GoogleProductTaxonomy::SUPPORTED_LANGUAGES[$language])
        ) {
            return false;
        }

        $result = $this->Database->exec(
            "DELETE FROM `sc_product_taxonomy` WHERE `language_id` = '"
            . $language . "' AND `language_id` <> 'en-US'"
        );
        return $result === false ? false : true;
    }
}
