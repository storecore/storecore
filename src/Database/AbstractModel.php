<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\AbstractModel as CoreModel;

/**
 * Abstract MVC model with database access.
 *
 * This abstract model class extends the core abstract model by adding
 * a database connection.  Its main purpose therefore is to build MVC models
 * that require access to a database.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 * @uses    StoreCore\AbstractModel
 */
abstract class AbstractModel extends CoreModel
{
    /**
     * Add a database connection to the registry if none exists.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore registry.
     *
     * @throws \RuntimeException
     *   A PDO exception is (re)thrown as an SPL runtime exception if
     *   connecting to the default database fails or the proper connection
     *   attributes could not fully be set.  If the database configuration
     *   is set up correctly, the usual cause of a failed connection is a
     *   “Too many connections” timeout, which can only be noticed at runtime.
     */
    public function __construct(Registry $registry)
    {
        if (!$registry->has('Database')) {
            try {
                $dbh = new Connection();
                if ($dbh === null) {
                    throw new \RuntimeException();
                }
                if ($dbh->getAttribute(\PDO::ATTR_DRIVER_NAME) === 'mysql') {
                    $dbh->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
                }
                $registry->set('Database', $dbh);
            } catch (\PDOException $e) {
                throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
            }
        }
        parent::__construct($registry);
    }
}
