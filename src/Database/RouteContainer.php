<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\{Route, RoutingQueue};

/**
 * Route container.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class RouteContainer extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Validates and normalizes a route path string.
     *
     * @param string $path
     *   Path to parse.
     *
     * @return string
     *   Returns the parsed path as a string in lowercase ASCII with a leading
     *   forward slash.
     *
     * @throws \ValueError
     *   Throws a value error exception if the path is empty.
     */
    public static function filterPath(string $path): string
    {
        $path = trim($path);
        if (empty($path)) throw new \ValueError();
        $path = '/' . trim($path, '/');
        $path = filter_var($path, \FILTER_SANITIZE_URL);
        $path = mb_strtolower($path, 'UTF-8');
        return $path;
    }

    /**
     * Gets a routing queue with routes that match a request path.
     *
     * @param string $path
     *   Path routed to a controller in the '/path/to/controller/' format.
     *
     * @return StoreCore\RoutingQueue
     *   Returns a `RoutingQueue` object with one or more `Route` objects.
     *
     * @throws Psr\Container\NotFoundExceptionInterface
     *   Throws a PSR-11 compliant “not found” exception if the path could
     *   not be found.
     *
     * @throws Psr\Container\ContainerExceptionInterface
     *   Throws an exception that implements the PSR-11 `ContainerExceptionInterface`
     *   on other errors than a “not found” exception.
     */
    public function get(string $path): RoutingQueue
    {
        $path = self::filterPath($path);

        $query = <<<'SQL'
              SELECT `dispatch_priority`, `route_controller`, `controller_method`
                FROM `sc_routes`
               WHERE `route_path` = ?
            ORDER BY `dispatch_priority` DESC
        SQL;

        try {
            $statement = $this->Database->prepare($query);
            $statement->bindValue(1, $path, \PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll();
            $statement->closeCursor();
            unset($query, $statement);
        } catch (\Exception $e) {
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }

        if (empty($result)) {
            throw new NotFoundException();
        }

        $routes = new RoutingQueue();
        foreach ($result as $row) {
            $route = new Route($path, $row['route_controller'], $row['controller_method']);
            $routes->insert($route, $row['dispatch_priority']);
        }
        return $routes;
    }

    /**
     * Checks if a routing path exists.
     *
     * @param string $path
     *   Path to test.
     *
     * @return bool
     *   Returns `true` if the path exists and can be routed, otherwise `false`.
     */
    public function has(string $path): bool
    {
        $path = self::filterPath($path);
        $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_routes` WHERE `route_path` = ?');
        $statement->bindValue(1, $path, \PDO::PARAM_STR);
        $statement->execute();
        $count = $statement->fetchColumn(0);
        $statement->closeCursor();
        unset($path, $statement);
        return $count >= 1 ? true : false;
    }
}
