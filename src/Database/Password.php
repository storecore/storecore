<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * Password
 *
 * @package StoreCore\Security
 * @version 0.1.0
 */
class Password
{
    public const string VERSION = '0.1.0';

    /**
     * @var string DEFAULT_ALGORITHM
     *   Default password hashing algorithm that is used if it is supported
     *   on systems where the PHP crypt() function supports multiple hash
     *   types.  This constant MUST be reset if an algorithm becomes obsolete.
     */
    public const DEFAULT_ALGORITHM = 'Blowfish';

    /**
     * @var int WORK_FACTOR_BLOWFISH
     * @var int WORK_FACTOR_SHA512
     */
    public const WORK_FACTOR_BLOWFISH = 15;
    public const WORK_FACTOR_SHA512 = 1000000;

    /**
     * @var string $algorithm
     * @var null|string $hash
     * @var null|string $password
     * @var null|string $salt
     */
    private string $algorithm;
    private ?string $hash = null;
    private ?string $password = null;
    private ?string $salt = null;

    /**
     * @param null|string $password
     * @param null|string $salt
     * @return void
     * @uses \StoreCore\Database\Password::setPassword()
     * @uses \StoreCore\Database\Password::setSalt()
     */
    public function __construct(?string $password = null, ?string $salt = null)
    {
        if ($password !== null) {
            $this->setPassword($password);
            if ($salt !== null) {
                $this->setSalt($salt);
            }
        }
    }

    /**
     * Gets the password hash string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $this->password = null;
        return ($this->hash === null) ? '' : $this->getHash();
    }

    /**
     * Encrypt the password.
     *
     * Encrypt the password to a hash string using the Blowfish or SHA-512
     * algorithm.  If these algorithms are not supported, hashing will first
     * fall back to Standard DES and then to SHA-1.
     *
     * @param string|null $password
     *   Optional password.
     *
     * @param string|null $salt
     *   Optional salt.  If no salt is set, a random salt is used.
     *
     * @return float
     *   Returns the time elapsed in seconds, accurate to the nearest
     *   microsecond.  This return value may be used to balance the work
     *   factors.
     *
     * @throws BadMethodCallException
     */
    public function encrypt(?string $password = null, ?string $salt = null): float
    {
        $time_start = microtime(true);

        if ($password !== null) {
            $this->setPassword($password);
        } elseif ($this->password === null) {
            throw new \BadMethodCallException('Missing argument: ' . __METHOD__ . ' expects parameter 1 to be a password string.');
        }

        if ($salt !== null) {
            $this->setSalt($salt);
        }

        if (CRYPT_BLOWFISH == 1) {
            $this->algorithm = 'Blowfish';
            if ($this->salt === null || strlen($this->salt) !== 22) {
                $this->setSalt(new Salt(22));
            }
            $salt = '$2y$' . self::WORK_FACTOR_BLOWFISH . '$' . $this->salt . '$';
            $this->hash = crypt($this->password, $salt);
        } elseif (CRYPT_SHA512 == 1) {
            $this->algorithm = 'SHA-512';
            if ($this->salt === null || strlen($this->salt) !== 16) {
                $this->setSalt(new Salt(16));
            }
            $salt = '$6$rounds=' . self::WORK_FACTOR_SHA512 . '$' . $this->salt . '$';
            $this->hash = crypt($this->password, $salt);
        } elseif (CRYPT_STD_DES == 1) {
            $this->algorithm = 'Standard DES';
            if ($this->salt === null || strlen($this->salt) !== 2) {
                $this->setSalt(new Salt(2));
            }
            $this->hash = crypt($this->password, $this->salt);
        } else {
            $this->algorithm = 'SHA-1';
            $this->salt = new Salt();
            $this->hash = sha1($this->salt . $this->password);
        }

        return microtime(true) - $time_start;
    }

    /**
     * @param void
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param void
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param void
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Add salt to the password.
     *
     * @param \Stringable|string $salt
     * @return void
     */
    public function setSalt(\Stringable|string $salt): void
    {
        $this->salt = (string) $salt;
    }
}
