<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\User;
use StoreCore\Types\EmailAddress;
use StoreCore\Types\UUID;

/**
 * User repository.
 *
 * @package StoreCore\Security
 * @version 0.3.0
 */
class UserRepository extends UserMapper implements
    ContainerInterface,
    \Countable,
    CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * Bans a user.
     *
     * A user MAY be deleted from the database entirely.  For reference purposes
     * however, for example in historic data of user activity in order
     * processing and payment handling, it is RECOMMENDED to maintain the last
     * known user data.  Users that are no longer granted access, like former
     * employees, may therefore be “banned” by assigning them the special user
     * group ID 0 (zero).
     *
     * @param \StoreCore\User $user
     *   User to ban.
     *
     * @return bool
     *   Returns true on success, otherwise false.
     *
     * @uses StoreCore\Database\UserMapper::update()
     * @uses StoreCore\User::getIdentifier()
     * @uses StoreCore\User::setUserGroupID()
     */
    public function ban(User &$user): bool
    {
        $user->setUserGroupID(0);
        return $this->update(
            [
                self::PRIMARY_KEY => (string) $user->getIdentifier(),
                'user_group_id' => 0,
            ]
        );
    }

    /**
     * Counts the number of active users.
     *
     * @param void
     *
     * @return int
     *   User accounts may be disabled, permanently or temporarily, by setting
     *   the user group ID to `0` (zero).  This method therefore returns a count
     *   of all users from other user groups (`WHERE user_group_id != 0`).
     * 
     *   ```sql
     *   SELECT COUNT(*)
     *     FROM `sc_users`
     *    WHERE `user_group_id` != 0
     *   ```
     */
    public function count(): int
    {
        try {
            $statement = $this->Database->query(
                'SELECT COUNT(*) FROM `sc_users` WHERE `user_group_id` != 0'
            );
            return (int) $statement->fetchColumn(0);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return 0;
    }

    /**
     * Gets a user by the user ID.
     *
     * @param string $id
     *   Universally unique user identifier as a string.
     *
     * @return StoreCore\User
     *   Returns a `StoreCore\User` user model object on success or `null` if
     *   the user does not exist.
     *
     * @throws Psr\Container\NotFoundException
     *   Throws a PSR-11 ContainerInterface “Not Found” exception if the
     *   requested user could not be found.
     *
     * @throws Psr\Container\ContainerException
     *   Throws a generic PSR-11 ContainerException on other errors.
     */
    public function get(string $id): User
    {
        try {
            $user_data = $this->read($id);
            if ($user_data === false) throw new NotFoundException();
            $user_data = $user_data[0];
            return $this->getUserObject($user_data);
        } catch (NotFoundException $e) {
            throw $e;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if the user exists.
     *
     * @param string $id
     *   Universally unique identifier (UUID) or email address of the user
     *   as a string.  An email address can only be associated with one
     *   user account because the email address may have to be used to
     *   recover a lost user account when the password is forgotten.
     *
     * @return bool
     *   Returns `true` if the user exists, otherwise `false`.
     *
     * @uses StoreCore\Types\UUID::validate()
     *   The static method `UUID::validate()` is used to test if the `$id` is
     *   a valid UUID.
     * 
     * @uses StoreCore\Types\EmailAddress::validate()
     *   If the `$id` is not a valid UUID, `EmailAddress::validate()` is used
     *   to test if it is a valid email address.
     */
    public function has(string $id): bool
    {
        if (!UUID::validate($id)) {
            if (!EmailAddress::validate($id)) {
                return false;
            }
            $id = (new EmailAddress($id))->metadata->hash;
        }

        if (ctype_xdigit($id) && strlen($id) === 64) {
            $query = <<<'SQL'
                   SELECT COUNT(*)
                     FROM `sc_users` AS `u`
                LEFT JOIN `sc_email_addresses` AS `e`
                       ON `u`.`email_address_id` = `e`.`email_address_id`
                    WHERE `e`.`email_address_hash` = UNHEX(?)
                      AND `e`.`date_deleted` IS NULL
            SQL;
        } else {
            $id = str_replace('-', '', $id);
            $query = <<<'SQL'
                SELECT COUNT(*)
                  FROM `sc_users`
                 WHERE `user_uuid` = ?
            SQL;
        }
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $id, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $count = $statement->fetchColumn(0);
                $statement->closeCursor();
                if ($count >= 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
