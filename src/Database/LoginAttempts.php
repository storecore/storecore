<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use StoreCore\ClockTrait;

/**
 * Login attempts logger.
 *
 * @internal
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class LoginAttempts extends AbstractModel implements ClockInterface, \Countable
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Counts the recently failed login attempts.
     * 
     * @param int $minutes
     *   Size of the time frame in minutes, defaults to `15` minutes.
     *
     * @return int
     *   Returns the number of failed login attempts as an integer or `0` on failure.
     */
    public function count(int $minutes = 15): int
    {
        $minutes = new \DateInterval('PT' . abs($minutes) . 'M');
        $datetime = $this->now()->sub($minutes);

        $statement = $this->Database->prepare(
            "SELECT COUNT(*) FROM `sc_login_attempts` WHERE `attempted` >= ? AND `successful` = b'0'"
        );
        $statement->bindValue(1, $datetime->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchColumn(0);
        $statement->closeCursor();
        return \is_int($result) ? $result : 0;
    }

    /**
     * Stores a login attempt.
     *
     * @param \Stringable|string|null $user_identity
     *   Email address, telephone number, user name, user access token, or
     *   similar identifier of the user’s identity.  An email address is not
     *   validated nor sanitized, so it MAY be invalid.
     *
     * @param null|string $remote_address
     *   OPTIONAL remote client IP address.  If the remote address is set
     *   to `null`, the superglobal `$_SERVER['REMOTE_ADDR']` is used.
     *
     * @param bool $successful
     *   The login attempt was successful (`true`) or failed (`false`).  This
     *   parameter MUST explicitly be set to a boolean `true` in order to log
     *   a successful attempt.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @uses StoreCore\ClockTrait::now()
     */
    public function storeAttempt(
        \Stringable|string|null $user_identity = null,
        ?string $remote_address = null,
        bool $successful = false
    ): bool {
        $user_identity = (string) $user_identity;
        $user_identity = trim($user_identity);
        if (\mb_strlen($user_identity) > 320) {
            $user_identity = mb_substr($user_identity, 0, 319) . '…';
        }

        if (empty($remote_address)) {
            if ($this->Server->has('REMOTE_ADDR')) {
                $remote_address = $this->Server->get('REMOTE_ADDR');
            } else {
                $remote_address = '';
            }
        }

        if ($successful === true) {
            $successful = 1;
        } else {
            $successful = 0;
        }

        $query = <<<'SQL'
            INSERT
                INTO `sc_login_attempts` (`attempted`, `successful`, `remote_address`, `user_identity`)
              VALUES
                (:attempted, :successful, :remote_address, :user_identity)
              ON DUPLICATE KEY UPDATE
                `attempted` = UTC_TIMESTAMP(6)
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':attempted', $this->now()->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
            $statement->bindValue(':successful', $successful, \PDO::PARAM_INT);
            $statement->bindValue(':remote_address', $remote_address, \PDO::PARAM_STR);
            $statement->bindValue(':user_identity', $user_identity, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);
                return true;
            }
        }
        return false;
    }
}
