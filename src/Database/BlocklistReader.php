<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * IP blocklist reader.
 * 
 * This model class limits the database blocklist access to a `has()` to check
 * if an IP address is currently blocked and a `log()` to update the last visit
 * from that IP address.
 *
 * @api
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class BlocklistReader extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Checks if an IP address is currently blocked.
     *
     * @param string $ip_address
     *   IP address to check.
     *
     * @return bool
     *   Returns `true` if the IP address is blocked, otherwise `false`.
     */
    final public function has(string $ip_address): bool
    {
        $ip_address = filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) return false;
        $ip_address = strtolower($ip_address);

        try {
            $statement = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_ip_blocklist` WHERE `ip_address` = ? AND `from_date` < UTC_TIMESTAMP() AND (`thru_date` IS NULL OR `thru_date` > UTC_TIMESTAMP())'
            );
            $statement->bindValue(1, $ip_address, \PDO::PARAM_STR);
            if ($statement->execute()) {
                $count = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count === 1) {
                    return true;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Logs a hit by a blocked IP address.
     *
     * @param string $ip_address
     *   The IP address being logged.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    final public function log(string $ip_address): bool
    {
        $ip_address = \filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) return false;
        $ip_address = strtolower($ip_address);

        try {
            $statement = $this->Database->prepare(
                'UPDATE `sc_ip_blocklist` SET `last_seen` = UTC_TIMESTAMP() WHERE `ip_address` = ? LIMIT 1'
            );
            $statement->bindValue(1, $ip_address, \PDO::PARAM_STR);
            $result = $statement->execute();
            $statement->closeCursor();
            if ($result !== false) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }
}
