<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use StoreCore\IdentityInterface;

use StoreCore\ClockTrait;

use StoreCore\{OnlineBusiness, OnlineStore};
use StoreCore\CRM\Organization;
use StoreCore\CRM\OrganizationType;
use StoreCore\Types\{UUID, UUIDFactory};

/**
 * Organization mapper.
 *
 * @internal
 * @package StoreCore\CRM
 * @version 1.0.0-alpha.1
 */
class OrganizationMapper extends AbstractModel implements ClockInterface, DataMapperInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Saves a new organization to the database.
     *
     * @param StoreCore\CRM\Organization $organization
     *   Organization model.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function create(IdentityInterface &$organization): bool
    {
        if (!($organization instanceof Organization)) {
            throw new \TypeError();
        }

        if (\is_int($organization->metadata->id)) {
            return false;
        }

        if ($organization->hasIdentifier()) {
            $uuid = $organization->getIdentifier();
        } else {
            $factory = new UUIDFactory($this->Registry);
            $uuid = $factory->createUUID();
            unset($factory);
        }
        $uuid = (string) $uuid;
        $uuid = strtolower($uuid);
        $uuid = str_replace('-', '', $uuid);

        $statement = $this->Database->prepare(
            'INSERT INTO `sc_organizations` (`organization_uuid`, `version`, `date_created`) VALUES (UNHEX(?), 0, ?)'
        );
        if ($statement !== false) {
            $statement->bindValue(1, $uuid);
            $statement->bindValue(2, $organization->metadata->dateCreated->format('Y-m-d H:i:s'));
            if ($statement->execute() !== false) {
                $id = $this->Database->lastInsertId();
                if (\is_string($id) && ctype_digit($id)) {
                    if (!$organization->hasIdentifier()) {
                        $organization->setIdentifier($uuid);
                    }
                    $organization->metadata->id = (int) $id;
                    $organization->metadata->version = 0;
                    return $this->update($organization);
                }
            }
        }

        return false;
    }

    /**
     * Deletes an organization from the database.
     *
     * @param StoreCore\Types\UUID|int $id
     *   Unique identifier of the `Organization` as a string or an integer.
     *   An `Organization` record is not immediately removed from the
     *   database, but marked for permanent deletion with a timestamp.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function delete(int|string $id): bool
    {
        if (\is_int($id)) {
            if ($id > 16777215 || $id < 1) {
                return false;
            }
            $statement = $this->Database->prepare(
                'UPDATE `sc_organizations` SET `date_deleted` = UTC_TIMESTAMP() WHERE `organization_id` = ? LIMIT 1'
            );
        } else {
            $id = trim($id);
            $id = str_replace('-', '', $id);
            $id = strtolower($id);
            $statement = $this->Database->prepare(
                'UPDATE `sc_organizations` SET `date_deleted` = UTC_TIMESTAMP() WHERE `organization_uuid` = UNHEX(?) LIMIT 1'
            );
        }

        if ($statement !== false) {
            if (\is_int($id)) {
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
            }
            $result = $statement->execute();
            $statement->closeCursor();
            unset($statement);
            if ($result !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets an `Organization` model from the database.
     *
     * @param int|string $id
     *   Unique identifier of the `Organization` as an integer or a string.
     *
     * @return StoreCore\CRM\Organization|null|false
     *   Returns an `Organization` model, `null` if the organization does not
     *   exists, or `false` on errors.
     */
    public function read(int|string $id): Organization|null|false
    {
        if (\is_string($id)) {
            if (UUID::validate($id)) {
                $id = str_replace('-', '', $id);
                $id = strtolower($id);
            } elseif (\ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        if (
            \is_int($id)
            && ($id > 16777215 || $id < 1)
        ) {
            return null;
        };

        $query = <<<'SQL'
            SELECT
                   `organization_id`, HEX(`organization_uuid`) AS `organization_uuid`,
                   HEX(`parent_organization_uuid`) AS `parent_organization_uuid`,
                   `email_address_id`, `legal_name`, `alternate_name`,
                   `version`, `date_created`, `date_modified`,
                   `organization_type`,
                   `telephone_number`, `fax_number`, `website_url`,
                   `duns_number`, `isic_code`, `iso_6523_code`, `lei_code`, `naics_code`,
                   `tax_id`, `vat_id`,
                   `founding_date`, `dissolution_date`
              FROM `sc_organizations` WHERE
        SQL;
        if (\is_int($id)) {
            $query .= ' `organization_id` = ?';
        } else {
            $query .= ' `organization_uuid` = UNHEX(?)';
        }
        $query .= ' AND `date_deleted` IS NULL LIMIT 1';
        $query = preg_replace('/\s+/', ' ', trim($query));

        try {
            $statement = $this->Database->prepare($query);
            unset($query);

            if ($statement !== false) {
                if (\is_int($id)) {
                    $statement->bindValue(1, $id, \PDO::PARAM_INT);
                } else {
                    $statement->bindValue(1, $id, \PDO::PARAM_STR);
                }

                if ($statement->execute() !== false) {
                    $row = $statement->fetch(\PDO::FETCH_ASSOC);
                    $statement->closeCursor();
                    unset($statement);
                    if (\is_array($row)) {
                        $result = $this->toObject($row);
                        unset($row);
                        return $result;
                    } else {
                        return null;
                    }
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    /**
     * Converts an `Organization` object to a key-value array.
     *
     * @param StoreCore\CRM\Organization $object
     * @return array
     */
    final public function toArray(Organization $object): array
    {
        /* Structure of a database record in the `sc_organizations` table
         * without the `date_deleted` column.
         */
        $result = [
            'organization_id'          => \is_int($object->metadata->id) ? $object->metadata->id : null,
            'organization_uuid'        => null,
            'parent_organization_uuid' => null,
            'email_address_id'         => null,
            'common_name'              => $object->name,
            'legal_name'               => $object->legalName,
            'alternate_name'           => $object->alternateName,
            'version'                  => $object->metadata->version,
            'date_created'             => $object->metadata->dateCreated->format('Y-m-d H:i:s'),
            'date_modified'            => $object->metadata->dateModified === null ? null : $object->metadata->dateModified->format('Y-m-d H:i:s'),
            'organization_type'        => $object->type->name,
            'telephone_number'         => $object->telephone,
            'fax_number'               => $object->faxNumber,
            'website_url'              => $object->url === null ? null : (string) $object->url,
            'duns_number'              => $object->duns,
            'isic_code'                => $object->isicV4,
            'iso_6523_code'            => empty($object->iso6523Code) ? null : serialize($object->iso6523Code),
            'lei_code'                 => $object->leiCode,
            'naics_code'               => $object->naics,
            'tax_id'                   => $object->taxID,
            'vat_id'                   => $object->vatID,
            'founding_date'            => $object->foundingDate === null ? null : $object->foundingDate->format('Y-m-d'),
            'dissolution_date'         => $object->dissolutionDate === null ? null : $object->dissolutionDate->format('Y-m-d'),
        ];

        if ($object->hasIdentifier()) {
            $result['organization_uuid'] = $object->getIdentifier()->__toString();
            $result['organization_uuid'] = str_replace('-', '', $result['organization_uuid']);
            $result['organization_uuid'] = strtolower($result['organization_uuid']);
        }

        if (
            $object->parentOrganization !== null
            && $object->parentOrganization->hasIdentifier()
        ) {
            $result['parent_organization_uuid'] = $object->parentOrganization->getIdentifier()->__toString();
            $result['parent_organization_uuid'] = str_replace('-', '', $result['parent_organization_uuid']);
            $result['parent_organization_uuid'] = strtolower($result['parent_organization_uuid']);
        }

        // Store new email address.
        if ($object->email !== null) {
            if ($object->email->metadata->id === null) {
                $repository = new EmailAddressRepository($this->Registry);
                $repository->set($object->email);
                unset($repository);
            }
            $result['email_address_id'] = $object->email->metadata->id;
        }

        return $result;
    }

    /**
     * Converts a key-value array to an `Organization` object.
     *
     * @param array $data Key-value array with organization properties.
     * @return StoreCore\CRM\Organization
     * @uses StoreCore\CRM\OrganizationType::from()
     * @uses StoreCore\Types\UUID::__construct()
     */
    final public function toObject(array $data): Organization
    {
        $metadata = new Metadata($data['date_created'], $data['date_modified']);
        $metadata->id = $data['organization_id'];
        $metadata->version = $data['version'];
        unset($data['date_created'], $data['date_modified'], $data['organization_id'], $data['version']);

        $data = array_filter($data);

        // Map `organization_uuid` column to `Organization.identifier` property
        if (isset($data['organization_uuid'])) {
            $data['identifier'] = new UUID($data['organization_uuid']);
            unset($data['organization_uuid']);
        }

        // Map `(int) email_address_id` to `(object) Organization.email`.
        if (isset($data['email_address_id'])) {
            $repository = new EmailAddressRepository($this->Registry);
            if ($repository->has((string) $data['email_address_id'])) {
                $data['email'] = $repository->get((string) $data['email_address_id']);
            }
            unset($data['email_address_id']);
        }

        if (isset($data['iso_6523_code'])) {
            $data['iso6523Code'] = unserialize($data['iso_6523_code']);
            unset($data['iso_6523_code']);
        }

        $type = $data['organization_type'];
        unset($data['organization_type']);
        if ($type === 'OnlineStore') {
            $result = new OnlineStore($data);
        } elseif ($type === 'OnlineBusiness') {
            $result = new OnlineBusiness($data);
        } else {
            $result = new Organization($data);
            if ($type !== 'Organization') {
                $organization->type = OrganizationType::from('https://schema.org/' . $type);
            }
        }

        $metadata->hash = Metadata::hash($result);
        $result->metadata = $metadata;
        return $result;
    }

    /**
     * Updates an organization in the database.
     *
     * @param StoreCore\CRM\Organization $organization
     *   Organization model.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.  If the database does
     *   not need an update, `true` is returned to confirm that the data is
     *   up-to-date.
     *
     * @throws \TypeError
     *   This method accepts an `IdentityInterface` but requires an 
     *   `Organization` object or `Organization` subtype.
     */
    public function update(IdentityInterface &$organization): bool
    {
        if (!($organization instanceof Organization)) {
            throw new \TypeError();
        }

        if (!\is_int($organization->metadata->id)) {
            return false;
        }

        $hash = Metadata::hash($organization);
        if ($hash === $organization->metadata->hash) {
            return true;
        }
        $organization->metadata->hash = $hash;
        $organization->metadata->dateModified = $this->now();

        $query = <<<'SQL'
            UPDATE `sc_organizations`
               SET `organization_uuid` = UNHEX(:organization_uuid),
                   `parent_organization_uuid` = UNHEX(:parent_organization_uuid),
                   `email_address_id` = :email_address_id,
                   `common_name` = :common_name,
                   `legal_name` = :legal_name,
                   `alternate_name` = :alternate_name,
                   `version` = :update_version,
                   `date_created` = :date_created,
                   `date_modified` = :date_modified,
                   `organization_type` = :organization_type,
                   `telephone_number` = :telephone_number,
                   `fax_number` = :fax_number,
                   `website_url` = :website_url,
                   `duns_number` = :duns_number,
                   `isic_code` = :isic_code,
                   `iso_6523_code` = :iso_6523_code,
                   `lei_code` = :lei_code,
                   `naics_code` = :naics_code,
                   `tax_id` = :tax_id,
                   `vat_id` = :vat_id,
                   `founding_date` = :founding_date,
                   `dissolution_date` = :dissolution_date
             WHERE `organization_id` = :organization_id
               AND `version` < :new_version
             LIMIT 1
        SQL;
        $query = preg_replace('/\s+/', ' ', trim($query));

        $statement = $this->Database->prepare($query);
        unset($query);

        if ($statement !== false) {
            $data = $this->toArray($organization);

            $statement->bindValue(':organization_uuid', $data['organization_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':parent_organization_uuid', $data['parent_organization_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':email_address_id', $data['email_address_id'], \PDO::PARAM_INT);

            $statement->bindValue(':common_name', $data['common_name'], \PDO::PARAM_STR);
            $statement->bindValue(':legal_name', $data['legal_name'], \PDO::PARAM_STR);
            $statement->bindValue(':alternate_name', $data['alternate_name'], \PDO::PARAM_STR);

            $new_version = $data['version'] + 1;
            $statement->bindValue(':update_version', $new_version, \PDO::PARAM_INT);
            $statement->bindValue(':date_created', $data['date_created'], \PDO::PARAM_STR);
            $statement->bindValue(':date_modified', $data['date_modified'], \PDO::PARAM_STR);

            $statement->bindValue(':organization_type', $data['organization_type'], \PDO::PARAM_STR);

            $statement->bindValue(':telephone_number', $data['telephone_number'], \PDO::PARAM_STR);
            $statement->bindValue(':fax_number', $data['fax_number'], \PDO::PARAM_STR);
            $statement->bindValue(':website_url', $data['website_url'], \PDO::PARAM_STR);

            $statement->bindValue(':duns_number', $data['duns_number'], \PDO::PARAM_STR);
            $statement->bindValue(':isic_code', $data['isic_code'], \PDO::PARAM_STR);
            $statement->bindValue(':iso_6523_code', $data['iso_6523_code'], \PDO::PARAM_STR);
            $statement->bindValue(':lei_code', $data['lei_code'], \PDO::PARAM_STR);
            $statement->bindValue(':naics_code', $data['naics_code'], \PDO::PARAM_STR);

            $statement->bindValue(':tax_id', $data['tax_id'], \PDO::PARAM_STR);
            $statement->bindValue(':vat_id', $data['vat_id'], \PDO::PARAM_STR);

            $statement->bindValue(':founding_date', $data['founding_date'], \PDO::PARAM_STR);
            $statement->bindValue(':dissolution_date', $data['dissolution_date'], \PDO::PARAM_STR);

            $statement->bindValue(':organization_id', $data['organization_id'], \PDO::PARAM_INT);
            $statement->bindValue(':new_version', $new_version, \PDO::PARAM_INT);

            $result = $statement->execute();
            unset($data);

            if ($result !== false) {
                $statement->closeCursor();
                unset($statement);
                $organization->metadata->version = $new_version;
                $mapper = new OrganizationProperties($this->Registry);
                return $mapper->save($organization);
            }
        }

        return false;
    }
}
