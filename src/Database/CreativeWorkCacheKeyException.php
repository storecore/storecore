<?php

declare(strict_types=1);

namespace StoreCore\Database;

use \Throwable;
use Psr\SimpleCache\CacheException as CacheExceptionInterface;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionInterface;

class CreativeWorkCacheKeyException extends CreativeWorkCacheException implements
    CacheExceptionInterface,
    InvalidArgumentExceptionInterface,
    Throwable {}
