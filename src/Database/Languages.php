<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\I18N\Language;
use StoreCore\I18N\Languages as LanguageMapper;
use StoreCore\Types\LanguageCode;
use StoreCore\Types\UUID;

/**
 * Languages.
 *
 * @api
 * @package StoreCore\I18N
 * @version 1.0.0-alpha.1
 *
 */
class Languages extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|array $enabledLanguages
     *   Array containing the currently available languages.  A language
     *   is considered to be available when it is enabled and it has
     *   localized strings in the Translation Memory.
     */
    private ?array $availableLanguages = null;

    /**
     * Disables languages.
     *
     * @param string|array $languages
     *   One or more unique language identifiers.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function disable(string|array $languages): bool
    {
        if (\is_string($languages)) {
            $languages = [0 => $languages];
        } else {
            $languages = array_unique($languages);
        }

        try {
            $result = $this->Database->exec(
                "UPDATE `sc_languages` SET `enabled_flag` = b'0', `sort_order` = 0 WHERE `language_id` IN ('"
                . implode("', '", $languages) . "')"
            );
            if ($result !== false) {
                $this->availableLanguages = null;
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Enables languages.
     *
     * @param string|array $languages
     *   One or more unique language identifiers.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function enable(string|array $languages): bool
    {
        if (\is_string($languages)) {
            $languages = [0 => $languages];
        } else {
            $languages = array_unique($languages);
        }

        try {
            $result = $this->Database->exec(
                "UPDATE `sc_languages` SET `enabled_flag` = b'1' WHERE `language_id` IN ('"
                . implode("', '", $languages) . "')"
            );
            if ($result !== false) {
                $this->availableLanguages = null;
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Filters and possibly maps a language code.
     *
     * @internal
     * @param string $language_id Language identifier.
     * @return string Filtered and normalized language identifier.
     * @throws \TypeError
     */
    private function filter(string $language_id): string
    {
        $language_id = trim($language_id);
        $language_id = LanguageCode::filter($language_id);

        if (strlen($language_id) === 2) {
            $language_id = strtolower($language_id);
            $statement = $this->Database->prepare(
                 'SELECT `language_id`
                    FROM `sc_languages`
                   WHERE SUBSTRING(`language_id`, 1, 2) = ?
                ORDER BY `enabled_flag` DESC,
                         `language_id` = `parent_id` DESC
                   LIMIT 1'
            );
            $statement->bindValue(1, $language_id, \PDO::PARAM_STR);
            $statement->execute();
            $row = $statement->fetch(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($row !== false && count($row) === 1) {
                return $row['language_id'];
            }
        }

        // Map an ISO 639-2 B/T alpha-3 or ISO 639-3 language code to a language ID.
        if (strlen($language_id) === 3) {
            $language_id = strtolower($language_id);
            try {
                $language = LanguageMapper::{$language_id};
                return $language->value;
            } catch (\Throwable $t) {
                $this->Logger->debug('Unkown alpha-3 language code: ' . $language_id);
                return $language_id;
            }
        }

        return $language_id;
    }

    /**
     * Gets all available and enabled languages.
     *
     * @param void
     * @return array
     * @uses StoreCore\I18N\Language::__construct
     */
    public function getAvailableLanguages(): array
    {
        if ($this->availableLanguages !== null) {
            return $this->availableLanguages;
        }

        $query = <<<'SQL'
                 SELECT `language_id`, `local_name`, `english_name`
                   FROM `sc_languages`
            FORCE INDEX (`ix_languages`)
                  WHERE `enabled_flag` = b'1'
                    AND `language_id` IN (
                          SELECT DISTINCT `language_id`
                            FROM `sc_translation_memory`
                        )
               ORDER BY `sort_order` ASC,
                        `local_name` ASC,
                        `english_name` ASC
        SQL;


        $statement = $this->Database->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        unset($statement);

        $result = [];
        foreach ($rows as $row) {
            $result[$row['language_id']] = new Language($row['language_id'], $row['local_name'], $row['english_name']);
        }

        $this->availableLanguages = $result;
        return $result;
    }

    /**
     * Gets the languages for a store.
     *
     * @param StoreCore\Types\UUID|string $store_uuid
     *   Universally unique store identifier as a UUID object or a string.
     *
     * @return array
     *   Returns an associative array with the language identifier as the key
     *   and a boolean as the value.  The first array element is the store's
     *   default language.  If no languages are linked to the store, this
     *   method will first attempt to return all enabled languages and finally
     *   return the default master language British English (`en-GB`);
     */
    public function getStoreLanguages(UUID|string $store_uuid): array
    {
        $store_uuid = (string) $store_uuid;

        $query = <<<'SQL'
                SELECT `sl`.`language_id`, `l`.`enabled_flag`
                  FROM `sc_store_languages` AS `sl`
             LEFT JOIN `sc_stores` AS `s`
                    ON `sl`.`store_id` = `s`.`store_id`
            INNER JOIN `sc_languages` AS `l`
                    ON `sl`.`language_id` = `l`.`language_id`
                 WHERE `s`.`store_uuid` = ?
              ORDER BY `l`.`enabled_flag` DESC,
                       `sl`.`default_flag` DESC
        SQL;

        $statement = $this->Database->prepare('');
        $statement->bindValue(1, $store_uuid, \PDO::PARAM_STR);
        if ($statement->execute() !== false) {
            if ($statement->rowCount() !== 0) {
                $store_languages = array();
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    $k = $row['language_id'];
                    $v = $row['enabled_flag'] === 1 ? true : false;
                    $store_languages[$k] = $v;
                }
                return $store_languages;
            } else {
                // Returns all enabled languages if no store languages were found.
                $statement = $this->Database->prepare(
                    "SELECT `language_id` FROM `sc_languages` WHERE `enabled_flag` = b'1'"
                );
                if ($statement->execute() !== false && $statement->rowCount() !== 0) {
                    $enabled_languages = array();
                    while ($row = $statement->fetch(\PDO::FETCH_NUM)) {
                        $enabled_languages[$row[0]] = true;
                    }
                    return $enabled_languages;
                }
            }
        }

        // Return default language if no languages were found.
        return [LanguageMapper::DEFAULT_LANGUAGE => true];
    }

    /**
     * Checks if a language ID exists.
     *
     * @param string $id
     *   Language identifier.
     *
     * @return bool
     *   Returns `true` if the language identifier exists, otherwise `false`.
     *
     * @see https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
     *   Implements the PSR-11 Container Interface `has()` method.
     */
    public function has(string $id): bool
    {
        $id = $this->filter($id);
        if ($this->isEnabled($id)) {
            return true;
        }

        $statement = $this->Database->prepare(
            'SELECT COUNT(*) FROM `sc_languages` WHERE `language_id` = ?'
        );
        $statement->bindValue(1, $id, \PDO::PARAM_STR);
        if ($statement->execute() !== false) {
            $count = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            if ($count === 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a language is enabled.
     *
     * @param string $language_id
     *   Unique language identifier.
     *
     * @return bool
     *   Returns `true` if the language is enabled and `false` if it is not.
     */
    public function isEnabled(string $language_id): bool
    {
        return array_key_exists($language_id, $this->getAvailableLanguages());
    }
}
