<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Types\UUID;

/**
 * Order items.
 *
 * @internal
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderItems extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Counts the number of items in an order.
     * 
     * @param StoreCore\Types\UUID $order_uuid
     *   Universally unique identifier (UUID) or internal ID of an `Order` or
     *   one of the subclasses of an `Order`, such as a `WishList` or a `Cart`.
     *
     * @return int|false
     *   Returns the number of items in an `Order` as an integer or `false`
     *   on errors.  If an `Order` contains 2 apples and 3 bananas, this method
     *   will return the integer 5.
     */
    public function count(UUID|int $id): int|false
    {
        try {
            if (\is_int($id)) {
                $statement = $this->Database->prepare(
                    'SELECT SUM(`units`) FROM `sc_order_items` WHERE `order_id` = ? AND `date_deleted` IS NULL'
                );
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $id = str_replace('-', '', $id->__toString());
                $id = strtolower($id);
                $statement = $this->Database->prepare(
                    'SELECT SUM(`units`) FROM `sc_order_items` AS `i` LEFT JOIN `sc_orders` AS `o` ON `i`.`order_id` = `o`.`order_id` WHERE `o`.`order_uuid` = UNHEX(?) AND `i`.`date_deleted` IS NULL'
                );
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
            }
            if ($statement->execute()) {
                $result = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($result !== false) {
                    return (int) $result;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    /**
     * Deletes all ordered items in a given order.
     *
     * @param string|int $id
     *   ID or UUID of the order.
     *
     * @return
     *   Returns `true` on success or `false` on failure.
     */
    public function deleteOrder(string|int $id): bool
    {
        $query =
            "UPDATE `sc_order_items`
                SET `order_status_id` = b'0001',
                    `date_modified` = UTC_TIMESTAMP(),
                    `date_deleted` = UTC_TIMESTAMP() "
        ;
        if (\is_int($id)) {
            $query .= 'WHERE `order_id` = ?';
        } else {
            $query .= 'WHERE `order_id` IN (SELECT `order_id` FROM `sc_orders` WHERE `order_uuid` = UNHEX(?) LIMIT 1)';
        }
        $query .= ' AND `order_status_id` IS NULL AND `date_deleted` IS NULL';
        $query = preg_replace('/\s+/', ' ', $query);

        try {
            $statement = $this->Database->prepare($query);
            if (\is_int($id)) {
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(1, $id, \PDO::PARAM_STR);
            }
            $result = $statement->execute();
            $statement->closeCursor();
            unset($query, $statement);
            if ($result !== false) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }
}
