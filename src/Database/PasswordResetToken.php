<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015-2017, 2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \Random\Randomizer;

/**
 * Token or “passkey” to reset passwords.
 *
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class PasswordResetToken
{
    /**
     * @var string CHARACTER_SET
     *   Character set for the token, currently set to ASCII digits and
     *   uppercase letters.  The digits 0 and 1 and the letters I and O are
     *   omitted, as these characters MAY be confusing to users in case they
     *   have to be entered in a form.
     */
    public const string CHARACTER_SET = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Get a random token.
     *
     * @param int $length
     *   OPTIONAL length of the token string.  Defaults to `16` characters for
     *   a fixed-width `CHAR(16)` column in a MySQL or MariaDB database table.
     *
     * @return string
     */
    public static function getInstance(int $length = 16): string
    {
        $charset = str_shuffle(self::CHARACTER_SET);
        $randomizer = new Randomizer();
        return $randomizer->getBytesFromString($charset, $length);
    }
}
