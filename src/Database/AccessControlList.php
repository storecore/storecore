<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * IP access control list.
 *
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class AccessControlList extends AbstractModel implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Clears inactive IP addresses from the access control list.
     *
     * @param void
     *
     * @return true
     *   Returns `true` on success, otherwise `false`.
     */
    public function clear(): bool
    {
        try {
            $this->Database->exec(
                'DELETE FROM `sc_ip_access_control_list` WHERE `thru_date` IS NOT NULL AND `thru_date` < UTC_TIMESTAMP()'
            );
            return true;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            return false;
        }
    }

    /**
     * Counts the number of active addresses.
     *
     * @param void
     *
     * @return int
     *   Returns the currently active IP addresses for administration and
     *   API clients as an integer.
     */
    public function count(): int
    {
        try {
            $statement = $this->Database->prepare(
                "SELECT COUNT(*) FROM `sc_ip_access_control_list` WHERE (`admin_flag` = b'1' OR `api_flag` = b'1') AND `from_date` <= UTC_TIMESTAMP() AND (`thru_date` IS NULL OR `thru_date` > UTC_TIMESTAMP())"
            );
            $statement->execute();
            $count = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            return (int) $count;
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            throw new \RuntimeException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * Removes an IP address from the access control list.
     *
     * @param string $ip_address
     *   The IPv4 or IPv6 address to remove.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception on an invalid IP address.
     */
    public function delete(string $ip_address): bool
    {
        $ip_address = filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) {
            throw new \ValueError(__METHOD__ . ' expects parameter to be valid IP address');
        }
        $ip_address = strtolower($ip_address);
 
        try {
            $statement = $this->Database->prepare(
                'UPDATE `sc_ip_access_control_list` SET `thru_date` = UTC_TIMESTAMP() WHERE `ip_address` = ? LIMIT 1'
            );
            $statement->bindValue(1, $ip_address, \PDO::PARAM_STR);
            $result = $statement->execute();
            $statement->closeCursor();
            unset($statement);
            if ($result !== false) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }
   
    /**
     * Checks if an IP address is included and is granted access.
     *
     * @param string $ip_address
     *   Case-insensitive IPv4 or IPv6 address.
     *
     * @param bool $administration
     *   OPTIONAL flag to check access to administration services (`true`).
     *   Defaults to `false` for lesser access control to API services.  This
     *   parameter MUST explicitly be set to a boolean `true`, and not a type
     *   that evaluates to `true`.
     *
     * @return bool
     *   Returns `true` if the IP address is currently granted access,
     *   otherwise `false`.
     */
    public function has(string $ip_address, bool $administration = false): bool
    {
        $ip_address = filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) return false;
        $ip_address = strtolower($ip_address);

        /* If the (bool) $administration flag is `true`, the first query
         * for administration access is executed.  If not, the second query
         * for API access is executed.

            SELECT COUNT(*)
              FROM sc_ip_access_control_list
             WHERE ip_address = :ip_address
               AND admin_flag = 1
               AND from_date <= UTC_TIMESTAMP()
               AND (thru_date IS NULL OR thru_date > UTC_TIMESTAMP())

            SELECT COUNT(*)
              FROM sc_ip_access_control_list
             WHERE ip_address = :ip_address
               AND api_flag = 1
               AND from_date <= UTC_TIMESTAMP()
               AND (thru_date IS NULL OR thru_date > UTC_TIMESTAMP())
         */
        $query = 'SELECT COUNT(*) FROM `sc_ip_access_control_list` WHERE `ip_address` = :ip_address ';
        if (true === $administration) {
            $query .= "AND `admin_flag` = b'1'";
        } else {
            $query .= "AND `api_flag` = b'1'";
        }
        $query .= " AND `from_date` <= UTC_TIMESTAMP() AND (`thru_date` IS NULL OR `thru_date` > UTC_TIMESTAMP())";

        try {
            $statement = $this->Database->prepare($query);
            $statement->bindValue(':ip_address', $ip_address);
            if ($statement->execute() !== false) {
                $count = (int) $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count === 1) {
                    return true;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    /**
     * Checks if the access control list is empty.
     *
     * Note the differences between the `AccessControlList::count()` method and
     * the `AccessControlList::isEmpty()` method.  If `isEmpty()` returns `true`,
     * there are no database records at all and the access control list 
     * therefore SHOULD NOT be used to grant or deny access to the
     * administration or API.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` if the access control list is empty, otherwise `false`.
     */
    public function isEmpty(): bool
    {
        try {
            $statement = $this->Database->query(
                'SELECT COUNT(*) FROM `sc_ip_access_control_list`'
            );
            $count = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            if ($count === 0) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }
}
