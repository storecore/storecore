<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\{Route, RoutingQueue};

/**
 * Route repository.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class RouteRepository extends RouteContainer implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Deletes all routes that are associated with a given controller.
     *
     * @param string $controller
     *   Name of the controller to remove from the routing.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @throws \ValueError
     *   Throws a value error exception if the `$controller` parameter
     *   is empty.
     */
    public function deleteController(string $controller): bool
    {
        $controller = trim($controller);
        $controller = ltrim($controller, '\\');
        if (empty($controller)) throw new \ValueError();
        $statement = $this->Database->prepare('DELETE FROM `sc_routes` WHERE `route_controller` = ?');
        $statement->bindValue(1, $controller, \PDO::PARAM_STR);
        $result = $statement->execute();
        $statement->closeCursor();
        unset($statement);
        return $result === false ? false : true;
    }

    /**
     * Deletes all controllers within a given namespace.
     *
     * @param string $namespace
     *   Name of the namespace to remove from the routing.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception on an invalid namespace
     *   or the 'StoreCore' root namespace.
     */
    public function deleteNamespace(string $namespace): bool
    {
        $namespace = str_ireplace('_', '', $namespace);
        $namespace = str_ireplace('%', '', $namespace);
        $namespace = trim($namespace);
        $namespace = trim($namespace, '\\');
        if (empty($namespace) || $namespace === 'StoreCore') {
            throw new \InvalidArgumentException();
        }

        /*
         * @see https://stackoverflow.com/questions/41027816/what-are-the-valid-characters-in-php-namespace-names
         *      What are the valid characters in PHP namespace names?
         */
        if (preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff\\\\]*[a-zA-Z0-9_\x7f-\xff]$/', $namespace) !== 1) {
            throw new \InvalidArgumentException();
        }

        $statement = $this->Database->prepare(
            'DELETE FROM `sc_routes` WHERE `route_controller` LIKE :namespace'
        );
        $statement->bindValue(':namespace', addslashes($namespace . '\\%'), \PDO::PARAM_STR);
        $result = $statement->execute();
        $statement->closeCursor();
        unset($namespace, $statement);
        return $result === false ? false : true;
    }

    /**
     * Deletes all routes associated with a given path.
     *
     * @param string $path
     *   Name of the route path to delete.
     *
     * @return bool
     *   Return true on success, otherwise false.
     *
     * @throws \InvalidArgumentException
     *   Throws an invalid argument exception if the path is not a string
     *   or an empty string.
     */
    public function deletePath(string $path): bool
    {
        $path = self::filterPath($path);
        $statement = $this->Database->prepare('DELETE FROM `sc_routes` WHERE `route_path` = ?');
        $statement->bindValue(1, $path, \PDO::PARAM_STR);
        $result = $statement->execute();
        $statement->closeCursor();
        unset($statement);
        return $result === false ? false : true;
    }

    /**
     * Saves all routes in a queue.
     *
     * @param StoreCore\RoutingQueue $queue
     *   Priority queue with routes to save.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function saveQueue(RoutingQueue $queue): bool
    {
        if ($queue->isEmpty()) {
            return true;
        }

        $return = true;
        $queue->setExtractFlags(\SplPriorityQueue::EXTR_BOTH);
        foreach ($queue as $node) {
            if (false === $this->saveRoute($node['data'], $node['priority'])) {
                $return = false;
            }
        }
        return $return;
    }

    /**
     * Saves a route to the repository.
     *
     * @param StoreCore\Route $route
     *   The route to save.
     *
     * @param int $dispatch_priority
     *   Optional queue dispatch priority as signed integer ranging from `-128`
     *   up to and including `127`.  Defaults to `0` for normal priority.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function saveRoute(Route $route, int $dispatch_priority = 0): bool
    {
        if ($dispatch_priority < -128) {
            $dispatch_priority = -128;
        } elseif ($dispatch_priority > 127) {
            $dispatch_priority = 127;
        }

        $statement = $this->Database->prepare(
            'INSERT INTO `sc_routes` (`dispatch_priority`, `route_path`, `route_controller`, `controller_method`) VALUES (:dispatch_priority, :route_path, :route_controller, :controller_method)'
        );
        $statement->bindValue(':dispatch_priority', $dispatch_priority, \PDO::PARAM_INT);
        $statement->bindValue(':route_path', $route->path, \PDO::PARAM_STR);
        $statement->bindValue(':route_controller', $route->controller, \PDO::PARAM_STR);

        if ($route->method === null) {
            $statement->bindValue(':controller_method', null, \PDO::PARAM_NULL);
        } else {
            $statement->bindValue(':controller_method', $route->method, \PDO::PARAM_STR);
        }

        $result = $statement->execute();
        $statement->closeCursor();
        unset($statement);
        return $result === false ? false : true;
    }
}
