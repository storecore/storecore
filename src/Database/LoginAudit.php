<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * Login Audit Maintenance & Management
 *
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class LoginAudit extends AbstractModel implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Counts all stored login attempts.
     *
     * @param void
     * @return int
     */
    public function count(): int
    {
        $statement = $this->Database->query("SELECT COUNT(*) FROM `sc_login_attempts`");
        return $statement->fetchColumn(0);
    }

    /**
     * Counts the recently failed login attempts.
     *
     * @param \DateInterval|int $interval
     *   OPTIONAL size of the time frame as a DateInterval or an integer for seconds.
     *   Defaults to `3600` seconds for the last hour.  Minimum is `1` second.
     *
     * @return int
     *   Number of failed attempts to log in within the provided time frame.
     */
    public function countLastFailedAttempts(\DateInterval|int $interval = 3600): int
    {
        if (\is_int($interval)) {
            if ($interval < 1) {
                $interval = 1;
            }
            $interval = new \DateInterval('PT' . $interval . 'S');
        }

        $datetime = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
        $datetime = $datetime->sub($interval);

        $statement = $this->Database->prepare(
            "SELECT COUNT(*) FROM `sc_login_attempts` WHERE `attempted` >= ? AND `successful` = b'0'"
        );
        $statement->bindValue(1, $datetime->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchColumn(0);
    }

    /**
     * Lists logged login attemps.
     *
     * @param \DateTimeInterface|string $before = 'now'
     *   OPTIONAL date and time for pagination.
     *
     * @param int $limit
     *   OPTIONAL limit of the result set, with a minimum of `1` and
     *   a maximum of `1000`.  Defaults to `100`.
     *
     * @return iterable
     *   Returns an iterable that MAY be empty if no attempts were found
     *   (and the database table is empty).
     */
    public function list(\DateTimeInterface|string $before = 'now', int $limit = 100): iterable
    {
        if (\is_string($before)) {
            $before = new \DateTimeImmutable($before, new \DateTimeZone('UTC'));
        }

        if ($limit > 1000) {
            $limit = 1000;
        } elseif ($limit < 1) {
            $limit = 1;
        }

        $query = <<<'SQL'
              SELECT `successful`, `attempted`, `remote_address`
                FROM `sc_login_attempts`
               WHERE `attempted` < ?
            ORDER BY `attempted` DESC
               LIMIT ?
        SQL;
        $statement = $this->Database->prepare($query);
        $statement->bindValue(1, $before->format('Y-m-d H:i:s.u'), \PDO::PARAM_STR);
        $statement->bindValue(2, $limit, \PDO::PARAM_INT);
        unset($before, $limit, $query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_NUM);
        $statement->closeCursor();
        unset($statement);
        return $result;
    }

    /**
     * Clean up the login attempts table.
     *
     * This method deletes all attempts to log in after 7 years and successful
     * attempts after 90 days.  Therefore unsuccessful, and possibly harmful,
     * failed attempts are stored for up to 7 years.
     *
     * @param void
     *
     * @return int|false
     *   Number of deleted rows or `false` on failure.
     */
    public function optimize(): int|false
    {
        $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
        $result = false;

        $datetime = $now->sub(new \DateInterval('P7Y'));
        $affected_rows = $this->Database->exec(
            "DELETE FROM `sc_login_attempts` WHERE `attempted` < '"
            . $datetime->format('Y-m-d H:i:s') . "'"
        );

        if (\is_int($affected_rows)) {
            $result = $affected_rows;

            $datetime = $now->sub(new \DateInterval('P90D'));
            $affected_rows = $this->Database->exec(
                "DELETE FROM `sc_login_attempts` WHERE `attempted` < '"
                . $datetime->format('Y-m-d H:i:s') . "' AND `successful` = b'1'"
            );

            if (\is_int($affected_rows)) {
                $result = $affected_rows;

                $this->Database->exec('FLUSH TABLES `sc_login_attempts`');
                $this->Database->exec('FLUSH STATUS');
            }
        }
        return $result;
    }
}
