<?php

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\{ContainerExceptionInterface, NotFoundExceptionInterface};

/**
 * Runtime exception thrown if something cannot be found in a database.
 *
 * @see https://github.com/php-fig/container/blob/master/src/NotFoundExceptionInterface.php
 */
class NotFoundException extends ContainerException implements
    NotFoundExceptionInterface,
    ContainerExceptionInterface,
    \Throwable {}
