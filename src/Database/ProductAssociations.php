<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use StoreCore\ClockTrait;

/**
 * Product association mapping.
 *
 * @package StoreCore\PIM
 * @version 0.1.0
 */
class ProductAssociations extends AbstractModel implements ClockInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * Gets a hierarchy of product models.
     *
     * @param int $id
     *   Unique product identifier.
     *
     * @return array
     *   Returns a sorted array with one ore more product identifiers.
     *   If concrete product `7` is based on prototype `3` and prototype `3` is
     *   derived from base model `1`, then the returned array will look like this:
     * 
     *   ```
     *   {
     *       1 => "b73f99ca-721f-4655-bcb0-ae55b93a3e07",
     *       3 => "8cfed2f0-3369-4ec7-85e2-06ad680d791d",
     *       7 => "081b7346-701b-4905-aedf-9f0535d1958a
     *   }
     *   ```
     */
    public function getVariantsHierarchy(int $id): array
    {
        $sth = $this->Database->prepare(
            'SELECT `product_uuid` FROM `sc_products` WHERE `product_id` = ?'
        );
        $sth->bindValue(1, $id, \PDO::PARAM_INT);
        $sth->execute();
        $uuid = $sth->fetchColumn(0);
        $result = [$id => $uuid];

        do {
            $parent = $this->getParentVariant($id);
            if ($parent !== false) {
                $result = $parent + $result;
                $id = key($parent);
            }
        } while ($parent !== false);

        return $result;
    }

    /**
     * Gets the parent of a product model.
     *
     * @internal
     * @param int $id Product identifier
     * @return array|false
     */
    private function getParentVariant(int $id): array|false
    {
        try {
            $sth = $this->Database->prepare(
                "SELECT `a`.`associated_product_id`, `p`.`product_uuid` FROM `sc_product_associations` AS `a` LEFT JOIN `sc_products` AS `p` ON `a`.`associated_product_id` = `p`.`product_id` WHERE `a`.`product_id` = ? AND `a`.`association_type_id` = b'100000' AND `a`.`from_date` >= '" . $this->now()->format('Y-m-d H:i:s') . "' AND (`a`.`thru_date` IS NULL OR `a`.`thru_date` <= '" . $this->now()->format('Y-m-d H:i:s') . "')"
            );
            if ($sth !== false) {
                $sth->bindValue(1, $id, \PDO::PARAM_INT);
                if ($sth->execute() !== false) {
                    $result = $sth->fetch(\PDO::FETCH_KEY_PAIR);
                    return $result;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }
}
