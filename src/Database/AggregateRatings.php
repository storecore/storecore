<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \PDOException as DatabaseException;
use Psr\Container\ContainerInterface;
use StoreCore\Types\AggregateRating;
use StoreCore\Types\UUID;

/**
 * Aggregate ratings.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/AggregateRating Schema.org type `AggregateRating`
 * @version 1.0.0-alpha.1
 */
class AggregateRatings extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Gets an `AggrateRating` if it exists.
     *
     * @param string $id
     *   `Product` or `Organization` identifier as a UUID string or
     *   an `AggregateRating` identifier as a number.
     *
     * @return StoreCore\Types\AggregateRating
     * 
     * @throws StoreCore\Database\ContainerException
     *   Implements PSR-11 `Psr\Container\ContainerExceptionInterface`.
     */
    public function get(string $id): AggregateRating
    {
        if (!UUID::validate($id)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                throw new ContainerException();
            }
        }

        try {
            if (is_int($id)) {
                $statement = $this->Database->prepare('SELECT `rating_value`, `rating_count`, `review_count` FROM `sc_aggregate_ratings` WHERE `id` = ?');
                if ($statement === false) throw new ContainerException();
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement = $this->Database->prepare(
                    'SELECT `rating_value`, `rating_count`, `review_count` FROM `sc_aggregate_ratings` WHERE `item_reviewed_uuid` = UNHEX(?)'
                );
                if ($statement === false) throw new ContainerException();
                $statement->bindValue(1, str_replace('-', '', $id), \PDO::PARAM_STR);
            }
            if ($statement->execute() === false) throw new ContainerException();
            $row = $statement->fetch(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($statement);
            if ($row === false) {
                throw new NotFoundException('AggregateRating with id `' . $id . '` not found.');
            } else {
                return self::toObject($row);
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Checks if an aggregate rating exists.
     *
     * @param string $id
     *   `Product` or `Organization` identifier as a UUID string or
     *   an `AggregateRating` identifier as a number.
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        if (!UUID::validate($id)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        try {
            if (is_int($id)) {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_aggregate_ratings` WHERE `id` = ?'
                );
                if ($statement === false) return false;
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_aggregate_ratings` WHERE `item_reviewed_uuid` = UNHEX(?)'
                );
                if ($statement === false) return false;
                $statement->bindValue(1, str_replace('-', '', $id), \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $count = (int) $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count >= 1) {
                    return true;
                }
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Converts a key-value array to an `AggregateRating` object.
     *
     * @internal
     * @param array $array
     * @return StoreCore\Types\AggregateRating;
     */
    private static function toObject(array $array): AggregateRating
    {
        if (
            is_string($array['rating_value'])
            && is_numeric($array['rating_value'])
        ) {
            $array['rating_value'] = (float) $array['rating_value'];
            if (floor($array['rating_value']) == $array['rating_value']) {
                $array['rating_value'] = (int) $array['rating_value'];
            }
        }

        $result = new AggregateRating($array['rating_value'], $array['rating_count'], $array['review_count']);
        return $result;
    }
}
