<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateTimeInterface;
use \PDOException as DatabaseException;
use \Stringable;

use function \array_filter;
use function \array_keys;
use function \empty;
use function \implode;
use function \is_bool;
use function \is_int;
use function \is_string;
use function \strval;
use function \trim;
use function \unset;

/**
 * Data Access Object (DAO).
 *
 * The StoreCore abstract DAO provides an abstract CRUD interface to Create,
 * Read, Update, and Delete database records.  It executes the four basic SQL
 * data manipulation operations: `INSERT`, `SELECT`, `UPDATE`, and `DELETE`.
 * 
 * Model classes that extend this abstract DAO MUST provide two class constants
 * for late static bindings: a `TABLE_NAME` with the database table name the
 * model operates on and a `PRIMARY_KEY` for the primary key column of this table.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php.net/manual/en/language.oop5.late-static-bindings.php
 * @version 1.0.0
 */
abstract class AbstractDataAccessObject extends AbstractModel implements CRUDInterface
{
    /**
     * Creates a new database row.
     *
     * @param array $keyed_data
     *   Associative array with the structure of a single database record.  The
     *   array keys MUST match column names in the database table.
     *
     * @return string|false
     *   Returns the primary key of the inserted row on success or `false` on
     *   failure.  Usually the primary key is then used as the unique ID of an
     *   object.
     *
     * @uses PDO::lastInsertId()
     */
    public function create(array $keyed_data): string|false
    {
        // Remove all NULL values on an INSERT.
        $keyed_data = array_filter($keyed_data, function($var){return ($var !== null);});

        foreach ($keyed_data as $key => $value) {
            if ($value instanceof DateTimeInterface) {
                $keyed_data[$key] = $value->format('Y-m-d H:i:s');
            } elseif ($value instanceof Stringable) {
                $keyed_data[$key] = (string) $value;
            }
        }

        $columns = array_keys($keyed_data);
        $sql = 'INSERT INTO ' . static::TABLE_NAME . ' (' . implode(', ', $columns) . ') VALUES (:' . implode(', :', $columns) . ')';

        try {
            $sth = $this->Database->prepare($sql);
            unset($columns, $sql);

            foreach ($keyed_data as $key => $value) {
                // Store booleans as a TINYINT 1 for true or 0 for false.
                if (is_bool($value)) {
                    $value = ($value === true) ? 1 : 0;
                }

                $key = ':' . $key;
                if (is_int($value)) {
                    $sth->bindValue($key, $value, \PDO::PARAM_INT);
                } elseif (is_string($value)) {
                    $sth->bindValue($key, $value, \PDO::PARAM_STR);
                } else {
                    $sth->bindValue($key, strval($value), \PDO::PARAM_STR);
                }
            }

            if ($sth->execute()) {
                $result = $this->Database->lastInsertId();
                $sth->closeCursor();
                unset($sth);
                return $result;
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Deletes one or more database rows.
     *
     * @param mixed $value
     *   Value for the `WHERE $key = $value` SQL clause.
     *
     * @param string|null $key
     *   OPTIONAL key for the `WHERE $key = $value` clause.  If omitted,
     *   the primary key column in `static::PRIMARY_KEY` is used.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function delete(mixed $value, ?string $key = null): bool
    {
        if ($key === null) {
            $key = static::PRIMARY_KEY;
        }

        $sql = 'DELETE FROM `' . static::TABLE_NAME . '` WHERE `' . $key;
        if ($value === null) {
            $sql .= '` IS NULL';
        } else {
            $sql .= '` = :' . $key;
        }

        try {
            $sth = $this->Database->prepare($sql);
            if ($value !== null) {
                $key = ':' . $key;
                if (is_int($value)) {
                    $sth->bindValue($key, $value, \PDO::PARAM_INT);
                } elseif (is_string($value)) {
                    $sth->bindValue($key, $value, \PDO::PARAM_STR);
                } else {
                    $sth->bindValue($key, strval($value), \PDO::PARAM_STR);
                }
            }
            return $sth->execute();
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Fetches one or more rows from a single database table.
     *
     * @param mixed $value
     *   Value to search for in the `$key` column.
     *
     * @param null|string $key
     *   OPTIONAL name of a column in the `static::TABLE_NAME` database table.
     *   If omitted, the column for the primary key in `static::PRIMARY_KEY` is
     *   used.
     *
     * @return array|false
     *   Returns an associative array on success or `false` on failure.  This
     *   method also returns `false` if the read operation fails because no
     *   matching database records were found.
     */
    public function read(mixed $value, ?string $key = null): array|false
    {
        if ($key === null) {
            $key = static::PRIMARY_KEY;
        }

        $sql = 'SELECT * FROM `' . static::TABLE_NAME . '` WHERE `' . $key;
        if ($value === null) {
            $sql .= '` IS NULL';
        } else {
            $sql .= '` = :' . $key;
        }

        try {
            $sth = $this->Database->prepare($sql);
            if ($sth !== false) {
                if ($value !== null) {
                    $key = ':' . $key;
                    if (is_int($value)) {
                        $sth->bindValue($key, $value, \PDO::PARAM_INT);
                    } elseif (is_string($value)) {
                        $sth->bindValue($key, $value, \PDO::PARAM_STR);
                    } else {
                        $sth->bindValue($key, strval($value), \PDO::PARAM_STR);
                    }
                }
                if ($sth->execute()) {
                    $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
                    $sth->closeCursor();
                    unset($sth);
                    if ($result !== false && !empty($result)) {
                        return $result;
                    }
                }
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Updates one or more database rows.
     *
     * @param array $keyed_data
     *   Key-value array with table column names as the keys.
     *
     * @param null|string $where_clause
     *   OPTIONAL SQL `WHERE` clause for additional conditions.  This clause
     *   MUST NOT contain the `WHERE` keyword.  If this `WHERE` clause is
     *   omitted, the `UPDATE` is limited to a single record.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function update(array $keyed_data, ?string $where_clause = null): bool
    {
        // Limit UPDATE to a single row if no WHERE clause is provided.
        if ($where_clause === null) {
            if (is_int($keyed_data[static::PRIMARY_KEY])) {
                $where_clause = ' WHERE `' . static::PRIMARY_KEY . '` = ' . $keyed_data[static::PRIMARY_KEY];
            } else {
                $where_clause = ' WHERE `' . static::PRIMARY_KEY . '` = "' . (string) $keyed_data[static::PRIMARY_KEY] . '"';
            }
        } else {
            $where_clause = ' WHERE ' . trim($where_clause);
        }

        // Never UPDATE the primary key.
        unset($keyed_data[static::PRIMARY_KEY]);

        $updates = array();
        foreach ($keyed_data as $key => $value) {
            if (is_bool($value)) {
                $keyed_data[$key] = ($value === true) ? 1 : 0;
            } elseif ($value instanceof DateTimeInterface) {
                $keyed_data[$key] = $value->format('Y-m-d H:i:s');
            } elseif ($value instanceof Stringable) {
                $keyed_data[$key] = (string) $value;
            }

            $updates[] = $key . ' = :' . $key;
        }
        $sql = 'UPDATE ' . static::TABLE_NAME . ' SET ' . implode(', ', $updates) . $where_clause;

        try {
            $sth = $this->Database->prepare($sql);
            if ($sth !== false) {
                foreach ($keyed_data as $key => $value) {
                    $key = ':' . $key;
                    if ($value === null) {
                        $sth->bindValue($key, $value, \PDO::PARAM_NULL);
                    } elseif (is_int($value)) {
                        $sth->bindValue($key, $value, \PDO::PARAM_INT);
                    } elseif (is_string($value)) {
                        $sth->bindValue($key, $value, \PDO::PARAM_STR);
                    } else {
                        $sth->bindValue($key, strval($value), \PDO::PARAM_STR);
                    }
                }
                $result = $sth->execute();
                $sth->closeCursor();
                return $result;
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }
}
