<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * IP blocklist model.
 *
 * @api
 * @package StoreCore\Security
 * @version 1.0.0-alpha.1
 */
class Blocklist extends BlocklistReader
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Clears IP bans that are no longer in use.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function clear(): bool
    {
        try {
            $result = $this->Database->exec(
                'DELETE FROM `sc_ip_blocklist` WHERE `thru_date` IS NOT NULL AND `thru_date` < UTC_TIMESTAMP()'
            );
            if ($result !== false) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Blocks an IP address with an optional comment.
     *
     * @param string $ip_address
     *   IPv4 or IPv6 address to add to the blocklist.  To allow for easy
     *   database, blocklist, and server maintenance, IP addresses are handled
     *   and stored as plain text in dot-decimal notation (IPv4) or text with
     *   hexadecimal digits (IPv6).
     *
     * @param null|string $reason
     *   OPTIONAL short comment describing the reason for blocking the given
     *   IP address.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @see https://tools.ietf.org/html/rfc5952
     *   RFC 5952: A Recommendation for IPv6 Address Text Representation
     *
     * @throws \ValueError
     *   A value error exception is thrown if the first parameter `$ip_address`
     *   is not a valid IP address.
     *
     * @throws \LengthException
     *   Throws a length logic exception if the `$reason` string parameter has
     *   a string length of over 255 characters.
     */
    public function create(string $ip_address, ?string $reason = null): bool
    {
        $ip_address = filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) {
            throw new \ValueError();
        }
        $ip_address = strtolower($ip_address);

        try {
            $statement = $this->Database->prepare(
                'INSERT INTO `sc_ip_blocklist` (`ip_address`, `from_date`) VALUES (:ip_address, UTC_TIMESTAMP())'
            );
            $statement->bindValue(':ip_address', $ip_address, \PDO::PARAM_STR);
            $statement->execute();
            $statement->closeCursor();
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }

        if ($reason !== null) {
            $reason = trim($reason);
            if (mb_strlen($reason, 'UTF-8') > 255) {
                throw new \LengthException();
            }

            if (!empty($reason)) {
                try {
                    $statement = $this->Database->prepare(
                        'INSERT INTO `sc_ip_blocklist_comments` (`ip_address`, `comments`) VALUES (:ip_address, :comments)'
                    );
                    $statement->bindValue(':ip_address', $ip_address, \PDO::PARAM_STR);
                    $statement->bindValue(':comments', $reason, \PDO::PARAM_STR);
                    $statement->execute();
                    $statement->closeCursor();
                } catch (\PDOException $e) {
                    $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Removes an IP address block.
     *
     * This method removes an IP ban by setting the end date to the current
     * time in UTC.  The IP address and optional notes are kept
     * for reference purposes, but MAY be deleted with the `clear()` method.
     *
     * @param string $ip_address
     *   IPv4 or IPv6 address to remove from the blocklist.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @throws \ValueError
     *   Throws a value arror exception on an invalid IP address.
     */
    public function delete(string $ip_address): bool
    {
        $ip_address = filter_var($ip_address, \FILTER_VALIDATE_IP);
        if ($ip_address === false) {
            throw new \ValueError();
        }
        $ip_address = strtolower($ip_address);

        try {
            $statement = $this->Database->prepare(
                'UPDATE `sc_ip_blocklist` SET `thru_date` = UTC_TIMESTAMP() WHERE `ip_address` = ? LIMIT 1'
            );
            $statement->bindValue(1, $ip_address, \PDO::PARAM_STR);
            $result = $statement->execute();
            $statement->closeCursor();
            if ($result !== false) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Lists the active IP bans.
     *
     * @param void
     *
     * @return null|array
     *   Returns an associative key-value array with the blocklisted IP address
     *   as the key and the date and time it was blocklisted as the value.
     *   `Null` is returned if there currently are no active IP bans or an
     *   error occurred.
     */
    public function list(): ?array
    {
        $query = <<<'SQL'
              SELECT `ip_address`, `from_date`
                FROM `sc_ip_blocklist`
               WHERE `thru_date` IS NULL
                  OR `thru_date` > UTC_TIMESTAMP()
            ORDER BY `last_seen` DESC, `from_date` DESC
        SQL;
        $query = preg_replace('/\s+/', ' ', trim($query));

        try {
            $statement = $this->Database->prepare($query);
            if ($statement->execute()) {
                $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
                $statement->closeCursor();
                unset($query, $statement);
                if (!empty($result)) {
                    return $result;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return null;
    }
}
