<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

/**
 * Observers for the observer design pattern.
 *
 * This model class serves as a global registry for the observer design
 * pattern.  It stores observers and links them to their subjects.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Observers extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Gets all observer class names for a given subject.
     *
     * @param string $subject_class
     *   Class name of the observed subject class.
     *
     * @return array|null
     *   Returns a non-empty array or `null` if no observers were found.
     */
    public function getSubjectObservers(string $subject_class): ?array
    {
        $subject_class = trim($subject_class, '\\');

        try {
            $statement = $this->Database->prepare(
                'SELECT `o`.`observer_id`, `o`.`observer_class` FROM `sc_observers` AS `o` LEFT JOIN `sc_subjects` AS `s` ON `o`.`subject_id` = `s`.`subject_id` WHERE `s`.`subject_class` = :subject_class ORDER BY `o`.`dispatch_priority` DESC, `o`.`observer_id` ASC'
            );
            $statement->bindValue(':subject_class', $subject_class, \PDO::PARAM_STR);
            if ($statement->execute()) {
                $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
                $statement->closeCursor();
                unset($statement);
                if (!empty($result)) {
                    return $result;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return null;
    }
}
