<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\PIM\Brand;
use StoreCore\Types\UUID;

/**
 * Brand repository.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
class BrandRepository extends BrandMapper implements 
    ContainerInterface,
    \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Counts brands.
     *
     * @param void
     * @return int
     */
    #[\Override]
    public function count(): int
    {
        $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_brands`');
        if ($statement !== false) {
            if ($statement->execute() !== false) {
                $result = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if (\is_int($result)) {
                    return $result;
                }
            }
        }
        throw new \RuntimeException();
    }

    /**
     * Gets the last modification date.
     *
     * @param void
     * @return null|\DateTimeInterface
     */
    public function getLastModified(): ?\DateTimeInterface
    {
        try {
            $statement = $this->Database->prepare(
                "SELECT UPDATE_TIME FROM information_schema.tables WHERE TABLE_SCHEMA = '"
                . STORECORE_DATABASE_DEFAULT_DATABASE . "' AND TABLE_NAME = 'sc_brands'"
            );
            if ($statement !== false) {
                if ($statement->execute() !== false) {
                    $result = $statement->fetchColumn(0);
                    $statement->closeCursor();
                    unset($statement);
                    if (\is_string($result)) {
                        $result = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $result);
                        return $result;
                    }
                }
            }
        } catch (\Throwable $t) {
            $this->Logger->notice($t->getMessage());
        }
        return null;
    }

    /**
     * Lists brands.
     *
     * @param void
     * @return iterable
     */
    public function list(): iterable
    {
        $query = <<<'SQL'
              SELECT LOWER(CONCAT(
                       SUBSTR(HEX(`brand_uuid`), 1, 8), '-',
                       SUBSTR(HEX(`brand_uuid`), 9, 4), '-',
                       SUBSTR(HEX(`brand_uuid`), 13, 4), '-',
                       SUBSTR(HEX(`brand_uuid`), 17, 4), '-',
                       SUBSTR(HEX(`brand_uuid`), 21)
                     )) AS `uuid`,
                     `global_brand_name` AS `name`
                FROM `sc_brands`
            ORDER BY `global_brand_name` ASC
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            if ($statement->execute() !== false) {
                $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
                $statement->closeCursor();
                unset($query, $statement);
                return $result;
            }
        }
        throw new \RuntimeException();
    }
}
