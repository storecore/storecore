<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\{LoggerFactory, Registry};
use StoreCore\Admin\Controller;
use StoreCore\Engine\{ResponseFactory, StreamFactory};
use StoreCore\FileSystem\Backup as FileSystem;

/**
 * StoreCore database backup.
 *
 * @package StoreCore\Core
 * @version 0.3.0
 *
 * @todo
 *   Because the `SHOW TABLES` command lists database tables in alphabetical
 *   order, the recovery of a full backup may fail on chained `FOREIGN KEY`
 *   constraints.  Depending on the database structure and the selected
 *   tables, you MAY have to manually change the order of the `CREATE TABLE`
 *   statements.
 */
class Backup extends Controller
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * Downloads a database backup as an SQL file.
     *
     * @param void
     * @return void
     *
     * @see https://www.rfc-editor.org/info/rfc6922
     *      RFC 6922: The application/sql Media Type
     */
    public function download(): void
    {
        $factory = new LoggerFactory();
        $logger = $factory->createLogger();

        $factory = new ResponseFactory();

        $filename = self::save();
        if (
            $filename !== false
            && \is_string($filename)
            && is_file($filename)
            && is_readable($filename)
        ) {
            $response = $factory->createResponse();
            $response->setHeader('Content-Type', 'application/octet-stream');
            $response->addHeader('Content-Type', 'application/sql');
            $response->setHeader('Cache-Control', 'no-cache, must-revalidate');
            $response->setHeader('Content-Disposition', 'attachment; filename="' . basename($filename) . '"');
            $response->setHeader('Content-Length', (string) filesize($filename));
            $response->setHeader('Expires', '0');
            $response->setHeader('Pragma', 'no-cache');

            $factory = new StreamFactory();
            $response->setBody($factory->createStreamFromFile($filename));
            $response->output();

            $logger->warning('Remote client downloaded database backup: ' . $filename);
            exit;
        }

        $logger->error('Error downloading database backup.');
        $response = $factory->createResponse(500);
        $response->output();
        exit;
    }

    /**
     * Lists the available SQL backup files.
     *
     * @param void
     *
     * @return array
     *   Returns an array with file names with a `.sql` extension and
     *   the keyword `backup` in the file name.  This array MAY be empty
     *   if no `.sql` backup files were found.
     */
    public static function getFiles(): array
    {
        $files = [];

        $directory = FileSystem::getDirectory();
        if ($handle = opendir($directory)) {
            while (false !== ($entry = readdir($handle))) {
                $pathinfo = pathinfo($entry);
                if (
                    array_key_exists('extension', $pathinfo)
                    && $pathinfo['extension'] == 'sql'
                    && strpos($entry, 'backup') !== false
                ) {
                    $realpath = realpath($directory . DIRECTORY_SEPARATOR . $entry);
                    $files[] = [
                        'filename'  => $entry,
                        'filesize'  => filesize($realpath),
                        'filectime' => filectime($realpath),
                        'filemtime' => filemtime($realpath),
                    ];
                }
            }
            closedir($handle);
        }

        return $files;
    }

    /**
     * Saves a database backup to file.
     *
     * @param string|array $tables
     *   Optional comma-separated string or array with the names of database
     *   tables to save.  Defaults to '*' for all tables.
     *
     * @param bool $drop_tables
     *   If set to `true`, `DROP TABLE <tbl_name>` clauses are added to delete
     *   existing database tables.  Defaults to `false`, so only non-existent
     *   database tables are recreated with the `CREATE TABLE IF NOT EXISTS`
     *   clause.
     *
     * @return string|false
     *   Returns the filename of the SQL backup file on success or `false` on
     *   failure.
     */
    public static function save(string|array $tables = '*', bool $drop_tables = false): string|false
    {
        $factory = new LoggerFactory();
        $logger = $factory->createLogger();

        try {
            $dbh = new Connection();
        } catch (\PDOException $e) {
            $logger->error('Database connection error ' . $e->getCode() . ': ' . $e->getMessage());
            return false;
        }

        // Fetch (all) tables
        if ($tables == '*') {
            try {
                $result = $dbh->query('SHOW TABLES');
                if ($result === false) {
                    return false;
                } else {
                    $tables = array();
                    foreach ($result as $row) {
                        $tables[] = $row[0];
                    }
                }
                $result->closeCursor();
            } catch (\PDOException $e) {
                $logger->error('Database error ' . $e->getCode() . ' showing tables: ' . $e->getMessage());
                return false;
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        // Write pending changes to file.
        try {
            $dbh->exec('FLUSH TABLES');
        } catch (\PDOException $e) {
            $logger->notice('Error ' . $e->getCode() . ' flushing tables: ' . $e->getMessage());
        }

        // Cycle through all tables
        try {
            $sql_file_contents = (string)null;
            foreach ($tables as $table_name) {
                $sql = (string)null;
                $prepend = false;

                // Add an optional DROP TABLE clause.
                if ($drop_tables) {
                    $sql .= 'DROP TABLE ' . $table_name . ";\n\n";
                }

                // Add the CREATE TABLE IF NOT EXISTS clause.
                $result = $dbh->query('SHOW CREATE TABLE ' . $table_name);
                $create_table_row = $result->fetch(\PDO::FETCH_NUM);
                $create_table = preg_replace('/CREATE TABLE/', 'CREATE TABLE IF NOT EXISTS', $create_table_row[1], 1);
                $sql .= $create_table . ";\n\n";

                // Tables without foreign keys are prepended.
                if (strpos($create_table, ' FOREIGN KEY ') === false) {
                    $prepend = true;
                }

                // Add the table data from a non-empty table.
                $result = $dbh->query('SELECT COUNT(*) FROM ' . $table_name);
                if ($result->fetchColumn() > 0) {
                    $result = $dbh->query('SELECT * FROM ' . $table_name);
                    $sql .= 'INSERT IGNORE INTO ' . $table_name . ' VALUES';
                    while ($row = $result->fetch(\PDO::FETCH_NUM)) {
                        foreach ($row as $key => $value) {
                            if (is_string($value)) {
                                $row[$key] = "'" . addslashes($value) . "'";
                            }
                        }
                        $sql .= "\n  (" . implode(',', $row) . '),';
                    }
                    $sql = rtrim($sql, ',') . ";\n\n";
                }

                // Prepend or append
                if ($prepend) {
                    $sql_file_contents = $sql . $sql_file_contents;
                } else {
                    $sql_file_contents = $sql_file_contents . $sql;
                }
            }
        } catch (\PDOException $e) {
            $logger->error('Database error ' . $e->getCode() . ': ' . $e->getMessage());
            return false;
        }

        // Disconnect
        try {
            $result->closeCursor();
            $dbh = null;
            unset($result, $dbh);
        } catch (\PDOException $e) {
            $logger->notice('Error ' . $e->getCode() . ' flushing tables: ' . $e->getMessage());
        }

        // Save SQL file
        $filename = self::getDirectory() . DIRECTORY_SEPARATOR . 'backup-' . gmdate('Ymd-His') . '-UTC.sql';
        $handle = fopen($filename, 'w+');
        if ($handle !== false) {
            fwrite($handle, $sql_file_contents);
            fclose($handle);
            $logger->notice('Database backup saved as ' . $filename . '.');
            return $filename;
        } else {
            $logger->error('Could not create database backup file.');
            return false;
        }
    }
}
