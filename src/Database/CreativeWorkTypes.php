<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \ReflectionClass;
use StoreCore\Registry;
use StoreCore\CMS\AbstractCreativeWork;

use function \is_int;
use function \time;

/**
 * Creative works types.
 *
 * @internal
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/CreativeWork Schema.org type `CreativeWork`
 */
class CreativeWorkTypes extends AbstractDataAccessObject implements CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const PRIMARY_KEY = 'creative_work_type_id';
    public const TABLE_NAME  = 'sc_creative_work_types';

    /**
     * Gets the type identifier of a creative work class.
     * 
     * @param string
     *   Name of the object type.  This MUST be a concrete implementation
     *   of the abstract `AbstractCreativeWork` class.
     *
     * @return int|false
     *   Returns a tiny integer on success, `false` on failure.
     */
    public function getKey(string $type): int|false
    {
        $statement = $this->Database->prepare(
            'SELECT `creative_work_type_id` FROM `sc_creative_work_types` WHERE `creative_work_type` = ? LIMIT 1'
        );
        $statement->bindValue(1, $type, \PDO::PARAM_INT);
        if ($statement->execute() === false) return false;
        $key = $statement->fetchColumn(0);
        $statement->closeCursor();
        return is_int($key) ? $key : false;
    }

    /**
     * Maps a creative work class name to a primary key integer.
     * 
     * @param StoreCore\CMS\AbstractCreativeWork $object
     * @return int
     */
    public static function fromCreativeWork(AbstractCreativeWork $object): int
    {
        $class = new ReflectionClass($object);
        $type = $class->getShortName();

        if ($type === 'ThreeDModel') {
            $type = '3DModel';
        }

        $registry = Registry::getInstance();
        $model = new CreativeWorkTypes($registry);
        $result = $model->getKey($type);
        unset($class, $model, $registry);

        if ($result === false) {
            throw new CreativeWorkCacheException('Unknown creative work type: ' . $type, time());
        }
        return $result;
    }
}
