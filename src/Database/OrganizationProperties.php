<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\IdentityInterface;

use StoreCore\CRM\FoodEstablishment;
use StoreCore\CRM\LocalBusiness;
use StoreCore\CRM\Organization;
use StoreCore\Types\UUID;

/**
 * Organization properties.
 *
 * @internal
 * @package StoreCore\CRM
 * @version 1.0.0-alpha.1
 */
class OrganizationProperties extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Saves properties of an organization.
     * 
     * @param StoreCore\CRM\Organization $organization
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function save(Organization $organization): bool
    {
        try {
            if (
                $organization->hasIdentifier()
                && \is_int($organization->metadata->id)
            ) {
                $properties = array();
                $properties['additionalType'] = $organization->additionalType;
                $properties['description'] = $organization->description;
                $properties['ownershipFundingInfo'] = $organization->ownershipFundingInfo;
                $properties['sameAs'] = $organization->sameAs;

                if ($organization instanceof ArchiveOrganization) {
                    if ($organization->archiveHeld !== null) {
                        $properties['archiveHeld'] = $organization->archiveHeld;
                    }
                }

                if ($organization instanceof FinancialService) {
                    if ($organization->feesAndCommissionsSpecification !== null) {
                        $properties['feesAndCommissionsSpecification'] = $organization->feesAndCommissionsSpecification;
                    }
                }

                if ($organization instanceof FoodEstablishment) {
                    $properties['acceptsReservations'] = $organization->acceptsReservations;
                    $properties['hasMenu'] = $organization->hasMenu;
                    $properties['servesCuisine'] = $organization->servesCuisine;
                    $properties['starRating'] = $organization->starRating;
                }

                if ($organization instanceof LocalBusiness) {
                    $properties['currenciesAccepted'] = $organization->currenciesAccepted;
                    $properties['hasMap'] = $organization->hasMap;
                    $properties['openingHours'] = $organization->openingHours;
                    $properties['paymentAccepted'] = $organization->paymentAccepted;
                    $properties['priceRange'] = $organization->priceRange;
                }

                $properties = array_filter($properties);
                if (!empty($properties)) {
                    $statement = $this->Database->prepare(
                        'INSERT INTO `sc_organization_properties` (`entity`, `attribute`, `value`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value` = ?'
                    );
                    $statement->bindParam(1, $organization->metadata->id, \PDO::PARAM_INT);
                    $statement->bindParam(2, $attribute, \PDO::PARAM_STR);
                    $statement->bindParam(3, $value, \PDO::PARAM_STR);
                    $statement->bindParam(4, $update_value, \PDO::PARAM_STR);

                    foreach ($properties as $attribute => $value) {
                        $value = json_encode($value, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE | \JSON_THROW_ON_ERROR);
                        if ($value !== false) {
                            $update_value = $value;
                            if ($statement->execute() !== true) {
                                return false;
                            }
                        }
                    }
                }

                return true;
            }
        } catch (\Exception $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }
}
