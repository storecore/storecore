<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateTimeZone;
use \DomainException;
use StoreCore\User;
use StoreCore\Types\{UUID, UUIDFactory};

use function \gmdate;
use function \in_array;
use function \is_int;
use function \is_string;
use function \unset;

/**
 * User mapper.
 *
 * @package StoreCore\Security
 * @version 0.4.3
 */
class UserMapper extends AbstractDataAccessObject implements CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.3';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const string PRIMARY_KEY = 'user_uuid';
    public const string TABLE_NAME  = 'sc_users';

    /**
     * Maps the user’s data to a user object.
     *
     * @param array $user_data
     *   User data as a key-value array.
     *
     * @return \StoreCore\User
     *   Returns a StoreCore user model as an object.
     */
    protected function getUserObject(array $user_data): User
    {
        $user = new User();

        $user->setIdentifier($user_data['user_uuid']);
        $user->setUserGroupID($user_data['user_group_id']);
        $user->setLanguageID($user_data['language_id']);

        $user->setEmailAddress($user_data['email_address']);
        $user->setUsername($user_data['username']);
        $user->setPasswordSalt($user_data['password_salt']);
        $user->setHashAlgorithm($user_data['hash_algo']);
        $user->setPasswordHash($user_data['password_hash']);
        $user->setPIN($user_data['pin_code']);

        if ($user_data['date_time_zone'] !== 'UTC') {
            $tz = new DateTimeZone($user_data['date_time_zone']);
            $user->setDateTimeZone($tz);
        }

        if ($user_data['person_uuid'] !== null) {
            $user->setPersonUUID($user_data['person_uuid']);
        }

        return $user;
    }

    /**
     * Saves a user.
     *
     * @param \StoreCore\User $user
     *   StoreCore user model object.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @throws \DomainException
     *   A domain logic exception is thrown if required user data are missing.
     */
    public function save(User &$user): bool
    {
        if (!$user->hasIdentifier()) {
            $user_data = ['password_reset' => gmdate('Y-m-d H:i:s')];
        } else {
            $user_data = [self::PRIMARY_KEY => (string) $user->getIdentifier()];
        }

        // Save email address and create foreign key.
        if ($user->hasEmailAddress()) {
            $address = $user->getEmailAddress();
            $repository = new EmailAddressRepository($this->Registry);
            if (!$repository->has($address->metadata->hash)) {
                if ($repository->set($address) === false) {
                    return false;
                }
            }
            if (is_int($address->metadata->id)) {
                $user_data['email_address_id'] = $address->metadata->id;
            }
            unset($address, $repository);
        }

        $user_data['user_group_id'] = $user->getUserGroupID();
        $user_data['language_id'] = $user->getLanguageID();
        $user_data['password_salt'] = $user->getPasswordSalt();
        $user_data['hash_algo'] = $user->getHashAlgorithm();
        $user_data['password_hash'] = $user->getPasswordHash();
        $user_data['pin_code'] = $user->getPIN();
        $user_data['date_time_zone'] = $user->getDateTimeZone()->getName();

        if (in_array(null, $user_data, true)) {
            throw new DomainException('User properties MUST NOT be null');
        }

        if ($user->getPersonUUID() !== null) {
            $user_data['person_uuid'] = $user->getPersonUUID();
        }

        if (!$user->hasIdentifier()) {
            $uuid_factory = new UUIDFactory($this->Registry);
            $uuid = $uuid_factory->createUUID();
            $user_data[self::PRIMARY_KEY] = (string) $uuid;
            $result = $this->create($user_data);
            if (is_string($result)) {
                $user->setIdentifier($uuid);
                return true;
            } else {
                return false;
            }
        } else {
            return $this->update($user_data);
        }
    }
}
