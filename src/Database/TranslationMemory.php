<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use StoreCore\Types\LanguageCode;

/**
 * Translation Memory model.
 *
 * @package StoreCore\I18N
 * @version 1.0.0-alpha.1
 */
class TranslationMemory extends AbstractModel implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $languageID
     *   Unique identifier of the current language.
     */
    private string $languageID = 'en-GB';

    /**
     * @var string $parentLanguageID
     *   Identifier of the parent or root language.
     */
    private $parentLanguageID = 'en-GB';

    /**
     * @var null|array $translations
     *   Translations as key-value pairs.
     */
    private ?array $translations = null;

    /**
     * Counts the number of translations.
     *
     * @param void
     * @return int
     */
    public function count(): int
    {
        $statement = $this->Database->query('SELECT COUNT(*) FROM `sc_translation_memory`');
        $result = $statement->fetchColumn(0);
        $statement->closeCursor();
        return $result;
    }

    /**
     * Find language strings in enabled languages.
     *
     * @param string $needle
     *   Case-insensitive string to search for.  This method searches for
     *   partial keys (the constant names) as well as partial language strings
     *   (the constant values).
     *
     * @return array|null
     *   Returns an array of matching translations or `null` if no match
     *   was found.
     */
    public function find(string $needle): ?array
    {
        $needle = trim($needle);
        if (empty($needle)) {
            return null;
        }

        $query = <<<'SQL'
               SELECT `t`.`translation_id`, `t`.`language_id`, `t`.`translation`
                 FROM `sc_translation_memory` `t`
            LEFT JOIN `sc_languages` `l`
                   ON `t`.`language_id` = `l`.`language_id`
                WHERE `l`.`enabled_flag` = b'1'
                  AND (`t`.`translation_id` LIKE :uppercase_needle OR `t`.`translation` LIKE :needle)
        SQL;

        $needle = '%' . trim($needle, '%') . '%';
        $uppercase_needle = strtoupper($needle);

        $statement = $this->Database->prepare($query);
        $statement->bindValue(':uppercase_needle', $uppercase_needle, \PDO::PARAM_STR);
        $statement->bindValue(':needle', $needle, \PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        unset($statement);

        if (\is_array($result) && !empty($result)) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     * Removes unused translations in disabled languages.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success and `false` on failure.
     */
    public function flush(): bool
    {
        $query = <<<'SQL'
            DELETE
              FROM `sc_translation_memory`
             WHERE `language_id` <> 'en-GB'
               AND `language_id` NOT IN 
                   (
                     SELECT `language_id`
                       FROM `sc_languages`
                      WHERE `enabled_flag` = b'1'
                   )
        SQL;
        $query = trim($query);
        $query = preg_replace('/\s+/', ' ', $query);

        $result = $this->Database->exec($query);
        return $result !== false ? true : false;
    }

    /**
     * Load translations as name/value pairs.
     *
     * @param null|string $language_code
     *   OPTIONAL language identifier.  Defaults to `en-GB` for British
     *   English.
     *
     * @param bool $storefront
     *   If set to `true` (default), limits the returned translations to
     *   public front-end applications, ignoring translations that are only
     *   used in administration and back-end applications.
     *
     * @return array
     *   Returns an associative array with constant names as the keys.  If a
     *   translation is missing for the current `$language_code`, the method
     *   first falls back to translations from the parent language (if one
     *   exists) and then to British English (en-GB) as the default language.
     *   This way the method always returns a full set of language strings,
     *   even if some still have to be translated to the given language.
     */
    public function getTranslations(?string $language_code = null, bool $storefront = true): array
    {
        if ($language_code !== null) {
            $this->setLanguage($language_code);
        }

        // Populate with British English ('en-GB') as the root language
        if ($this->translations === null || $this->languageID === 'en-GB') {
            $this->translations = array();
            $this->readTranslations('en-GB', $storefront);
        }

        if ($this->languageID !== 'en-GB') {
            if ($this->parentLanguageID !== 'en-GB' && $this->parentLanguageID !== $this->languageID) {
                $this->readTranslations($this->parentLanguageID, $storefront);
            }
            $this->readTranslations($this->languageID, $storefront);
        }

        return $this->translations;
    }

    /**
     * Reads all translations for a given language.
     *
     * @param string $language_id
     * @param bool $storefront
     * @return void
     */
    private function readTranslations(string $language_id, bool $storefront): void
    {
        $query = 'SELECT `translation_id`, `translation` FROM `sc_translation_memory` WHERE language_id = :language_id';
        if ($storefront) {
            $query .= ' AND `admin_only_flag` = 0';
        }
        $query .= ' ORDER BY `translation_id` ASC';

        $statement = $this->Database->prepare($query);
        $statement->bindValue(':language_id', $language_id, \PDO::PARAM_STR);
        $statement->execute();
        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $this->translations[$row['translation_id']] = $row['translation'];
        }
    }

    /**
     * Sets the language to load.
     *
     * @param string $language_code
     *   Internal language identifier or ISO language code.
     *
     * @return bool
     *   Returns `true` if the language was set or `false` if the language
     *   does not exist or is not enabled.
     */
    public function setLanguage(string $language_code): bool
    {
        $language_code = LanguageCode::filter($language_code);
        if ($language_code == $this->languageID) {
            return true;
        }

        $query = "SELECT `language_id`, `parent_id` FROM `sc_languages` WHERE `enabled_flag` = b'1' ";
        if (strlen($language_code) >= 5) {
            $query .= 'AND `language_id` = ?';
        } else {
            $language_code = strtolower($language_code);
            $language_code = $language_code . '%';
            $query .= "AND `language_id` LIKE ? ORDER BY `enabled_flag` DESC, `sort_order` ASC, `parent_id` = 'en-GB' DESC";
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        $statement->bindValue(1, $language_code, \PDO::PARAM_STR);
        $statement->execute();
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        if ($row === false) {
            return false;
        } else {
            $this->languageID = $row['language_id'];
            $this->parentLanguageID = $row['parent_id'];
            return true;
        }
    }

    /**
     * Add a new translation or update an existing translation.
     *
     * @param string $constant_name,
     *   Constant name and key of a translation.
     *
     * @param string $translation
     *   Value of a translation.
     *
     * @param string $language_id
     *   OPTIONAL language identifier.  Defaults to `en-GB` for British English.
     *
     * @param bool $admin_only
     *   If set to true this flag limits the use of a translation key/value
     *   pair to administration and back-end applications.  Defaults to false.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the parameter `$language_id`,
     *   `$translation` or `$language_id` is not a string or the string is empty.
     */
    public function setTranslation(string $constant_name, string $translation, string $language_id = 'en-GB', bool $admin_only = false): void
    {
        $constant_name = trim($constant_name);
        $translation = trim($translation);
        $language_id = trim($language_id);
        if (empty($constant_name) || empty($translation) || empty($language_id)) {
            throw new \ValueError();
        }

        $constant_name = strtoupper($constant_name);

        if ($admin_only === true) {
            $admin_only_flag = 1;
        } else {
            $admin_only_flag = 0;
        }

        $statement = $this->Database->prepare(
            'INSERT INTO `sc_translation_memory`
                 (`translation_id`, `language_id`, `translation`, `admin_only_flag`)
               VALUES
                 (:translation_id, :language_id, :translation, :admin_only_flag)
               ON DUPLICATE KEY UPDATE
                 `translation_id` = :update_translation_id,
                 `language_id` = :update_language_id,
                 `translation` = :update_translation,
                 `admin_only_flag` = :update_admin_only_flag'
        );
        $statement->bindValue(':translation_id', $constant_name, \PDO::PARAM_STR);
        $statement->bindValue(':update_translation_id', $constant_name, \PDO::PARAM_STR);
        $statement->bindValue(':language_id', $language_id, \PDO::PARAM_STR);
        $statement->bindValue(':update_language_id', $language_id, \PDO::PARAM_STR);
        $statement->bindValue(':translation', $translation, \PDO::PARAM_STR);
        $statement->bindValue(':update_translation', $translation, \PDO::PARAM_STR);
        $statement->bindValue(':admin_only_flag', $admin_only_flag, \PDO::PARAM_INT);
        $statement->bindValue(':update_admin_only_flag', $admin_only_flag, \PDO::PARAM_INT);
        $statement->execute();
    }
}
