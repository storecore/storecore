<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2019, 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\Admin\Configurator;

/**
 * Database maintenance module.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Maintenance extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var \StoreCore\Admin\Configurator|null $configurator
     *   The admin configurator is used in the `saveConfigurationSetting()`
     *   method to write configuration settings to the global `config.php`
     *   configuration file.
     */
    private ?Configurator $configurator = null;

    /**
     * @var array $changeLog
     *   Array with SQL statements that cannot be executed by parsing the
     *   default core SQL files.  Changes to database tables are ignored by the
     *   `IF NOT EXISTS` clause in a `CREATE TABLE IF NOT EXISTS`, so changes
     *   to tables and indexes should be moved elsewhere.
     */
    private array $changeLog = array(

    );

    /**
     * @var bool $updateAvailable
     *   Boolean flag that indicates whether a database update is available
     *   (`true`) or not (defaul`false`).
     */
    public readonly bool $updateAvailable;

    /**
     * Database maintenance module constructor.
     *
     * The database maintenance module checks for updates as soon as it is
     * called (on construction) in two ways.  If the currently installed
     * database version is older than the version of this maintenance model,
     * an update is available.  If the database version is unknown, we assume
     * the database was never installed.
     *
     * @param \StoreCore\Registry $registry
     *   Single instance of the StoreCore registry.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        if (
            !\defined('STORECORE_DATABASE_VERSION_INSTALLED')
            || version_compare(STORECORE_DATABASE_VERSION_INSTALLED, self::VERSION, '<')
        ) {
            $this->updateAvailable = true;
        } else {
            $this->updateAvailable = false;
        }
    }

    /**
     * Deletes records marked for removal after 30 days.
     *
     * @param int $interval_in_days
     *   Optional number of days records marked for deletion are kept in the
     *   recycle bin.  Defaults to 30 days.  The interval can be set to 0 (zero)
     *   to fully empty the recycle bin.
     *
     * @return int
     *   Returns the number of deleted database table rows.
     */
    public function emptyRecycleBin(int $interval_in_days = 30): int
    {
        if ($interval_in_days < 0) {
            $interval_in_days = 0;
        }

        $affected_rows = 0;
        $tables = [
            'sc_orders',
            'sc_customers',
            'sc_persons',
            'sc_organizations',
            'sc_addresses',
            'sc_email_addresses',
        ];
        foreach ($tables as $table) {
            $sql = 'DELETE FROM ' . $table . ' WHERE date_deleted IS NOT NULL';
            if ($interval_in_days !== 0) {
                $sql .= ' AND date_deleted < DATE_SUB(UTC_TIMESTAMP(), INTERVAL ' . $interval_in_days . ' DAY)';
            }
            $affected_rows += $this->Database->exec($sql);
        }
        return $affected_rows;
    }

    /**
     * Gets the StoreCore database size.
     *
     * @param void
     *
     * @return int|false
     *   Returns the size of database tables plus indexes in bytes on success
     *   or `false` on failure.
     */
    public function getSize(): int|false
    {
        $result = false;
        $statement = $this->Database->prepare('SELECT SUM(data_length + index_length) FROM information_schema.tables WHERE table_schema = ?');
        $statement->execute([STORECORE_DATABASE_DEFAULT_DATABASE]); 
        $size = $statement->fetchColumn(0);
        $statement->closeCursor();
        if (\is_int($size) || \is_numeric($size)) {
            $result = (int) $size;
        }
        return $result;
    }

    /**
     * Gets all StoreCore database table names.
     *
     * @param void
     *
     * @return array|false
     *   Returns an indexed array with the names of all StoreCore database
     *   tables, or false on failure.  Tables names for StoreCore always start
     *   with the `sc_` prefix; tables without this prefix are not included.
     */
    public function getTables(): array|false
    {
        $result = false;
        try {
            $statement = $this->Database->prepare('SHOW TABLES');
            if (
                $statement !== false
                && $statement->execute() !== false
            ) {
                $result = array();
                while ($row = $statement->fetch(\PDO::FETCH_NUM)) {
                    if (str_starts_with($row[0], 'sc_')) {
                        $result[] = $row[0];
                    }
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return $result;
    }

    /**
     * Optimizes one or more database tables.
     *
     * @param null|string|array $tables
     *   OPTIONAL comma-separated list or array of table names.  If this
     *   optional parameter is not set, all StoreCore tables are optimized.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     *
     * @uses getTables()
     */
    public function optimize(null|string|array $tables = null): bool
    {
        if (empty($tables)) {
            $tables = $this->getTables();
            if ($tables === false) {
                return false;
            }
        }

        if (is_array($tables)) {
            $tables = implode(', ', $tables);
        }

        try {
            $this->Database->query('OPTIMIZE TABLE ' . $tables);
            return true;
        } catch (\PDOException $e) {
            $this->Logger->notice($e->getMessage());
            return false;
        }
    }

    /**
     * Restores the StoreCore database or a saved backup.
     *
     * @param string|null $filename
     *   Optional filename of an SQL backup file.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function restore(?string $filename = null): bool
    {
        // Write pending changes to database files
        try {
            $this->Database->exec('FLUSH TABLES');
            $this->Database->exec('FLUSH STATUS');
        } catch (\PDOException $e) {
            $this->Logger->notice($e->getMessage());
        }

        try {
            // Return `true` only if something was actually done, otherwise `false`.
            $result = false;

            // Update core tables using CREATE TABLE IF NOT EXISTS and INSERT IGNORE
            if (is_file(__DIR__ . DIRECTORY_SEPARATOR . 'core-mysql.sql')) {
                $sql = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'core-mysql.sql', false);
                if ($sql !== false) {
                    $sql = self::sanitize($sql);
                    $this->Database->exec('SET FOREIGN_KEY_CHECKS = 0');
                    $this->Database->exec($sql);
                    $this->Database->exec('SET FOREIGN_KEY_CHECKS = 1');
                    $result = true;
                }
            }

            // Add new translations using INSERT IGNORE
            if (is_file(__DIR__ . DIRECTORY_SEPARATOR . 'i18n-dml.sql')) {
                $sql = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'i18n-dml.sql', false);
                if ($sql !== false) {
                    $sql = self::sanitize($sql);
                    $this->Database->exec($sql);
                    $result = true;
                }
            }

            // Add optional SQL to restore a backup
            if ($filename !== null && is_file($filename)) {
                $sql = file_get_contents($filename, false);
                if ($sql !== false) {
                    $this->Database->exec($sql);
                    $result = true;
                }
            }

            return $result;

        } catch (\Exception $e) {
            $this->Logger->error($e->getMessage());
        }
        return false;
    }

    /**
     * Saves a configuration setting as a key/value pair.
     *
     * @param string $name
     *   Name of the configuration setting.
     *
     * @param string $value
     *   Value of the configuration setting.
     */
    private function saveConfigurationSetting(string $name, string $value): void
    {
        if ($this->configurator === null) {
            $this->configurator = new Configurator();
        }
        $this->configurator->set($name, $value);
        $this->configurator->save();
    }

    /**
     * Updates the StoreCore database.
     *
     * @param void
     *
     * @return bool
     *   Returns true on success or false on failure.
     */
    public function update(): bool
    {
        try {
            // First restore the StoreCore default database
            $this->restore();

            // Execute other SQL statements
            foreach ($this->changeLog as $version => $sql) {
                if (version_compare($version, self::VERSION, '<=') == true) {
                    $this->Database->exec($sql);
                }
            }

            $this->saveConfigurationSetting('STORECORE_DATABASE_VERSION_INSTALLED', $version);
            $this->Logger->notice('StoreCore database version ' . self::VERSION . ' was installed.');

        } catch (\Exception $e) {
            $this->Logger->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Sanitizes an installer SQL file.
     * 
     * @internal
     * @param string $sql
     * @return string
     */
    public static function sanitize(string $sql): string
    {
        // Remove empty lines
        $sql = str_replace("\r\n", "\n", $sql);
        $sql = str_replace("\r", "\n", $sql);
        $sql = str_replace("\n\n\n\n", "\n", $sql);
        $sql = str_replace("\n\n\n", "\n", $sql);
        $sql = str_replace("\n\n", "\n", $sql);

        // Strip comments
        $sql = preg_replace('!/\*.*?\*/!s', '', $sql);
        $sql = preg_replace('/\n\s*\n/', "\n", $sql);

        $sql = explode("\n", $sql);
        foreach ($sql as $line_number => $line) {
            $line = trim($line);
            if (mb_strlen($line) === 0 || str_starts_with($line, '--')) {
                unset($sql[$line_number]);
            }
        }
        $sql = implode("\n", $sql);

        return $sql;
    }
}
