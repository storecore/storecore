<?php

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Http\Message\UriInterface;

/**
 * Media Library.
 *
 * Media files are stored as assets in the file system.  This database class
 * is used for managing file relations and media metadata.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\CMS
 * @version   0.2.0
 */
class MediaLibrary extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';


    /**
     * Creates an SEO URL from an UriInterface data object.
     *
     * @param \Psr\Http\Message\UriInterface $uri
     *   PSR-7 URI (Uniform Resource Identifier).
     *
     * @return string|null
     *   Returns the core elements of a public URL as an anonymized string.
     *   This short URL string does not contain a scheme, username, password,
     *   port number, query string, or fragment.  If no string could be
     *   created, `null` is returned.
     */
    private function createSeoUrl(UriInterface $uri): ?string
    {
        $url = (empty($uri->getHost())) ? '' : '//' . $uri->getHost();
        if (!empty($uri->getPath())) $url .= $uri->getPath();
        return empty($url) ? null : $url;
    }

    /**
     * Gets the file number.
     * 
     * A single media file MAY have multiple URLs for several reasons.
     * This method maps a URL (which usually contains a file name)
     * to a unique file number.
     *
     * @param Psr\Http\Message\UriInterface $uri
     *   PSR-7 compliant object for a Uniform Resource Identifier (URI).
     *
     * @return int|null
     *   Returns a media file number as an integer or `null` if the media file
     *   could not be found.  This method also returns `null` on database
     *   errors.
     */
    public function getFileNumber(UriInterface $uri): ?int
    {
        $seo_url = $this->createSeoUrl($uri);
        if ($seo_url === null) return null;

        try {
            $stmt = $this->Database->prepare('
                    SELECT m.media_file_id
                      FROM sc_product_media m
                INNER JOIN sc_product_media_texts t
                        ON m.product_media_id = t.product_media_id
                     WHERE t.seo_url = :seo_url
                     LIMIT 1
            ');
            $stmt->bindValue(':seo_url', $seo_url, \PDO::PARAM_STR);
            if ($stmt->execute() === false) return null;
            $row = $stmt->fetch(\PDO::FETCH_NUM);
            $stmt->closeCursor();
            return ($row === false) ? null : (int) $row[0];
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return null;
        }
    }
}
