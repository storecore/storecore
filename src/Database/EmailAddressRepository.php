<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use Psr\Container\ContainerInterface;
use StoreCore\RepositoryInterface;

use StoreCore\ClockTrait;
use StoreCore\Types\EmailAddress;
use StoreCore\Types\UUID;

/**
 * Email address repository.
 *
 * @package StoreCore\Core
 * @see     https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
 * @version 1.0.0-alpha.1
 */
class EmailAddressRepository extends AbstractModel implements
    ClockInterface,
    ContainerInterface,
    \Countable,
    RepositoryInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\EmailAddressMapper $dataMapper
     *   Data mapper for email addresses.
     */
    private ?EmailAddressMapper $dataMapper = null;

    /**
     * Clears previously deleted email addresses.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function clear(): bool
    {
        $result = $this->Database->exec(
            'DELETE FROM `sc_email_addresses` WHERE `date_deleted` IS NOT NULL AND `date_deleted` < DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 DAY)'
        );
        if ($result !== false) {
            $this->Database->exec('FLUSH TABLES `sc_email_addresses`');
            $this->Database->exec('FLUSH STATUS');
            return true;
        }
        return false;
    }

    /**
     * Counts the number of active email addresses.
     *
     * @param void
     * @return int
     */
    #[\Override]
    public function count(): int
    {
        $statement = $this->Database->query(
            'SELECT COUNT(*) FROM `sc_email_addresses` WHERE `date_deleted` IS NULL'
        );
        if ($statement !== false) {
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            return (int) $result;
        } else {
            return 0;
        }
    }

    /**
     * Creates and stores an email address from a string.
     *
     * @param \Stringable|string $address
     * @return StoreCore\Types\EmailAddress|false
     *   Returns the email address as a value object or `false` on failure.
     */
    public function createFromString(\Stringable|string $address): EmailAddress|false
    {
        try {
            $address = new EmailAddress((string) $address, true);
        } catch (\Throwable $t) {
            return false;
        }

        $query = <<<'SQL'
            INSERT INTO `sc_email_addresses`
                (`email_address_hash`, `email_address`, `date_created`)
              VALUES
                (UNHEX(?), ?, ?)
              ON DUPLICATE KEY UPDATE
                `email_address` = ?,
                `date_deleted` = NULL
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $address->metadata->hash, \PDO::PARAM_STR);
            $statement->bindValue(2, $address->__toString(), \PDO::PARAM_STR);
            $statement->bindValue(3, $address->metadata->dateCreated->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
            $statement->bindValue(4, $address->__toString(), \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);
                return $this->get($address->metadata->hash);
            }
        }

        $address;
    }

    /**
     * Gets an email address from the repository.
     *
     * @param string $id
     *   Internal numeric primary key, public email address hash,
     *   or email address as a string.
     *
     * @return StoreCore\Types\EmailAddress
     *   Email address as an object.
     */
    public function get(string $id): EmailAddress
    {
        if (
            ctype_digit($id)
            && strlen($id) < 64
        ) {
            $id = (int) $id;
            if ($id > 4294967295 || $id < 1) {
                throw new NotFoundException();
            }
        }

        if (
            \is_string($id)
            && EmailAddress::validate($id)
        ) {
            $address = new EmailAddress($id);
            $id = $address->metadata->hash;
        }

        $query = 'SELECT * FROM `sc_email_addresses` WHERE ';
        if (\is_int($id)) {
            $query .= '`email_address_id` = ?';
            $type = \PDO::PARAM_INT;
        } else {
            $query .= '`email_address_hash` = UNHEX(?)';
            $type = \PDO::PARAM_STR;
        }
        $query .= ' AND `date_deleted` IS NULL LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $id, $type);
            if ($statement->execute() !== false) {
                $row = $statement->fetch(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                unset($statement);
                if ($row === false) {
                    throw new NotFoundException();
                } else {
                    return EmailAddressMapper::toObject($row);
                }
            }
        }
        throw new ContainerException();
    }

    /**
     * Checks if the repository contains an email address.
     *
     * @param string $id
     *   Email address or email address hash as a string.
     *
     * @return bool
     *   Returns `true` if the email address exists, `false` otherwise.
     *
     * @uses StoreCore\Types\EmailAddress::validate()
     *   Searches by email address if the provided `$id` string is a valid
     *   email address.
     */
    public function has(string $id): bool
    {
        $id = trim($id);

        if (!ctype_xdigit($id) && EmailAddress::validate($id)) {
            $address = new EmailAddress($id);
            $id = $address->metadata->hash;
        }

        if (ctype_digit($id) && strlen($id) < 64) {
            $id = (int) $id;
        } elseif (ctype_xdigit($id) && strlen($id) === 64) {
            $id = strtolower($id);
        } else {
            return false;
        }

        $query = 'SELECT COUNT(*) FROM `sc_email_addresses` WHERE ';
        if (\is_int($id)) {
            $query .= '`email_address_id` = ?';
            $type = \PDO::PARAM_INT;
        } else {
            $query .= '`email_address_hash` = UNHEX(?)';
            $type = \PDO::PARAM_STR;
        }
        $query .= ' AND `date_deleted` IS NULL';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            unset($query);
            $statement->bindValue(1, $id, $type);
            if ($statement->execute() !== false) {
                $count = (int) $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count === 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Saves an email address.
     *
     * @param StoreCore\Types\EmailAddress|string $address
     *   Email address as an object.
     * 
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function set(object &$object): string
    {
        if (!($object instanceof EmailAddress)) {
            throw new \TypeError();
        }

        if ($this->dataMapper === null) {
            $this->dataMapper = new EmailAddressMapper($this->Registry);
        }

        if (
            (\is_int($object->metadata->id) && $this->has((string) $object->metadata->id))
            || $this->has($object->metadata->hash)
        ) {
            $this->dataMapper->update($object);
        } else {
            $this->dataMapper->create($object);
        }
        return $object->metadata->hash;
    }
    
    /**
     * Removes an email address.
     *
     * @param string $id
     *   Email address identifier as a numeric string, email address hash, or email
     *   address as a string.
     * 
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function unset(string $id): bool
    {
        $id = trim($id);

        if (
            ctype_digit($id)
            && strlen($id) < 64
        ) {
            $id = (int) $id;
            if ($id > 4294967295 || $id < 1) {
                return false;
            }
        }

        if (\is_string($id)) {
            if (
                !ctype_xdigit($id)
                && EmailAddress::validate($id)
            ) {
                $id = new EmailAddress($id);
                $id = (string) $id->metadata->hash;
            } elseif (
                !ctype_xdigit($id)
                || strlen($id) !== 64
            ) {
                return false;
            }
        }

        $query = "UPDATE `sc_email_addresses` SET `email_address` = '', `display_name` = '', `date_deleted` = ? WHERE ";
        if (\is_int($id)) {
            $query .= '`email_address_id` = ?';
            $type = \PDO::PARAM_INT;
        } else {
            $query .= '`email_address_hash` = UNHEX(?)';
            $type = \PDO::PARAM_STR;
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $this->now()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
            $statement->bindValue(2, $id, $type);
            $result = $statement->execute();
            $statement->closeCursor();
            if ($result !== false) {
                return true;
            }
        }
        return false;
    }
}
