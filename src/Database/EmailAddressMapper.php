<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;

use StoreCore\Types\EmailAddress;

/**
 * Email address data mapper.
 *
 * @internal
 * @package StoreCore\Core
 * @see     https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
 * @version 1.0.0-alpha.1
 */
final class EmailAddressMapper extends AbstractModel implements ClockInterface
{

    /**
     * Stores a new email address.
     *
     * @param EmailAddress &$address
     * @return bool
     */
    public function create(EmailAddress &$address): bool
    {
        $query = <<<'SQL'
            INSERT INTO `sc_email_addresses`
                (`email_address_hash`, `email_address`, `date_created`, `display_name`)
              VALUES
                (UNHEX(?), ?, ?, ?)
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            if ($statement !== false) {
                $statement->bindValue(1, $address->metadata->hash, \PDO::PARAM_STR);
                $statement->bindValue(2, $address->__toString(), \PDO::PARAM_STR);
                $statement->bindValue(3, $address->metadata->dateCreated->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
                $statement->bindValue(4, (string) $address->displayName, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $id = $this->Database->lastInsertId();
                $statement->closeCursor();
                unset($query, $statement);
                if ($id !== false) {
                    $address->metadata->id = (int) $id;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Deletes an email address.
     *
     * @param int|string $id
     *   Primary key or unique hash of the email address.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function delete(int|string $id): bool
    {
        if (
            \is_string($id)
            && ctype_digit($id)
            && strlen($id) < 64
        ) {
            $id = (int) $id;
            if ($id > 4294967295 || $id < 1) {
                return false;
            }
        }

        if (\is_string($id)) {
            $id = trim($id);
            $id = strtolower($id);
            if (!ctype_xdigit($id) || strlen($id) !== 64) {
                return false;
            }
        }

        $query = "DELETE FROM `sc_email_addresses` WHERE ";
        if (\is_int($id)) {
            $query .= '`email_address_id` = ?';
            $type = \PDO::PARAM_INT;
        } else {
            $query .= '`email_address_hash` = UNHEX(?)';
            $type = \PDO::PARAM_STR;
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $id, $type);
            $result = $statement->execute();
            $statement->closeCursor();
            if ($result !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the current date and time as a DateTimeImmutable object.
     *
     * @param void
     * @return \DateTimeImmutable
     */
    public function now(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    }

    /**
     * Converts a key-value array to an object.
     *
     * @param array $data
     * @return StoreCore\Types\EmailAddress
     * @uses StoreCore\Database\Metadata::__construct()
     */
    public static function toObject(array $data): EmailAddress
    {
        $result = new EmailAddress($data['email_address'], false);
        $hash = $result->metadata->hash;

        if (isset($data['date_created'])) {
            if (isset($data['date_modified'])) {
                $result->metadata = new Metadata($data['date_created'], $data['date_modified']);
            } else {
                $result->metadata = new Metadata($data['date_created']);
            }
        }
        $result->metadata->hash = $hash;

        if (isset($data['email_address_id'])) {
            $result->metadata->id = $data['email_address_id'];
        }

        if (isset($data['display_name'])) {
            $result->displayName = $data['display_name'];
        }

        return $result;
    }

    /**
     * Updates an existing email address.
     *
     * @internal
     * @param EmailAddress &$address
     * @return bool
     */
    public function update(EmailAddress &$address): bool
    {
        $now = $this->now();
        $query = <<<'SQL'
            UPDATE `sc_email_addresses`
               SET `email_address` = ?,
                   `date_modified` = ?,
                   `display_name` = ?
             WHERE `email_address_hash` = UNHEX(?)
               AND `date_deleted` IS NULL
             LIMIT 1
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            if ($statement !== false) {
                $statement->bindValue(1, $address->__toString(), \PDO::PARAM_STR);
                $statement->bindValue(2, $now->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
                $statement->bindValue(3, (string) $address->displayName, \PDO::PARAM_STR);
                $statement->bindValue(4, $address->metadata->hash, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);
                $address->metadata->dateModified = $now;
                return true;
            }
        }
        return false;
    }
}
