<?php

declare(strict_types=1);

namespace StoreCore\Database;

use \Exception;
use \Throwable;
use Psr\SimpleCache\CacheException as CacheExceptionInterface;

class CreativeWorkCacheException extends Exception implements
    CacheExceptionInterface,
    Throwable {}
