<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use StoreCore\IdentityInterface;

use StoreCore\ClockTrait;

use StoreCore\{BrokerOrder, Cart, Order, PreOrder, WishList};
use StoreCore\OML\BackOrder;
use StoreCore\OML\CartID;
use StoreCore\OML\OrderStatus;
use StoreCore\Types\{UUID, UUIDFactory};

/**
 * Order data mapper.
 *
 * @internal
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderMapper extends AbstractModel implements ClockInterface, DataMapperInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\OrderItems $orderItemsMapper
     *   Data mapper for the items in orders.
     */
    private OrderItems $orderItemsMapper;

    /**
     * Cancels an order.
     *
     * @param StoreCore\Order &$order
     *   The `Order` to cancel.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    final public function cancel(Order &$order): bool
    {
        if (
            !$order->hasIdentifier()
            || $order->orderStatus === OrderStatus::OrderCancelled
        ) {
            return false;
        }

        $now = $this->now();
        $uuid = str_replace('-', '', (string) $order->getIdentifier());
        $version = $order->metadata->version + 1;

        $query = <<<'SQL'
            UPDATE `sc_orders`
               SET `order_status_id` = b'0001',
                   `version` = ?,
                   `date_modified` = ?,
                   `date_cancelled` = ?
             WHERE `order_uuid` = UNHEX(?)
               AND `order_status_id` <> b'0001'
               AND `date_cancelled` IS NULL
             LIMIT 1
        SQL;
        $query = preg_replace('/\s+/', ' ', $query);

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $version, \PDO::PARAM_INT);
            $statement->bindValue(2, $now->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
            $statement->bindValue(3, $now->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
            $statement->bindValue(4, $uuid, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $statement->closeCursor();

                $query = <<<'SQL'
                    UPDATE `sc_order_items`
                       SET `order_status_id` = b'0001',
                           `date_deleted` = ?
                     WHERE `order_uuid` = UNHEX(?)
                       AND `order_status_id` <> b'0001'
                       AND `date_deleted` IS NULL
                SQL;
                $query = preg_replace('/\s+/', ' ', $query);
                $statement = $this->Database->prepare($query);
                if ($statement !== false) {
                    $statement->bindValue(1, $now->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
                    $statement->bindValue(2, $uuid, \PDO::PARAM_STR);
                    $statement->execute();
                    $statement->closeCursor();
                    unset($statement);
                }

                $order->orderStatus = OrderStatus::OrderCancelled;
                $order->metadata->dateModified = $now;
                $order->metadata->version = $version;
                $order->metadata->hash = Metadata::hash($order);
                return true;
            }
        }
        return false;
    }

    /**
     * Confirms an order.
     *
     * @param StoreCore\Order $order
     *   The `Order` to confirm.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @see https://schema.org/orderDate
     *   The `Order.orderDate` is the date the order was placed.  Once the
     *   `orderDate` is set, StoreCore assumes the `Order` was already
     *   confirmed and this method returns `false`.
     *
     * @see https://schema.org/orderNumber
     *   The `orderNumber` is a unique identifier of the transaction that MAY 
     *   be set through an external mechanism.
     */
    final public function confirm(Order &$order): bool
    {
        // An `Order.orderDate` is only set on confirmed orders,
        // so if the `orderDate` is set, the order has been confirmed.
        if ($order->orderDate !== null) {
            return false;
        }

        // An unknown `Order` cannot be confirmed.
        if (!$order->hasIdentifier()) {
            return false;
        }

        // Save the `Order` “as is” to pass any changes.
        if ($this->update($order) !== true)  {
            return false;
        }

        $order_uuid = str_replace('-', '', (string) $order->getIdentifier());
        $order_date = $this->now();

        $order_number = (string) $order->orderNumber;
        $confirmation_number = (string) $order->confirmationNumber;
        if (empty($order_number) || empty($confirmation_number)) {
            $model = new OrderNumbers($this->Registry);
            if (empty($order_number)) {
                $order_number = (string) $model->getRandomOrderNumber();
            }
            if (empty($confirmation_number)) {
                $confirmation_number = (string) $model->getConfirmationNumber();
            }
            unset($model);
        }

        $query = 
            "UPDATE `sc_orders`
                SET `wishlist_flag` = b'0',
                    `order_number` = :order_number,
                    `confirmation_number` = :confirmation_number,
                    `cart_uuid` = NULL,
                    `cart_rand` = NULL,
                    `date_confirmed` = :date_confirmed
              WHERE `order_uuid` = UNHEX(:order_uuid)
                AND `date_confirmed` IS NULL
              LIMIT 1";
        $query = preg_replace('/\s+/', ' ', $query);

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':order_number', $order_number, \PDO::PARAM_STR);
            $statement->bindValue(':confirmation_number', $confirmation_number, \PDO::PARAM_STR);
            $statement->bindValue(':date_confirmed', $order_date->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
            $statement->bindValue(':order_uuid', $order_uuid, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);
                $order->confirmationNumber = $confirmation_number;
                $order->orderDate = $order_date;
                $order->orderNumber = $order_number;
                return true;
            }
        }
        return false;
    }

    /**
     * Saves a new order.
     *
     * @param StoreCore\IdentityInterface &$entity
     * @return bool
     */
    public function create(IdentityInterface &$entity): bool
    {
        if ($entity->hasIdentifier()) {
            $order_uuid = $entity->getIdentifier();
        } else {
            $factory = new UUIDFactory($this->Registry);
            $order_uuid = $factory->createUUID();
            unset($factory);
        }

        $values = self::toArray($entity);
        $values['order_uuid'] = str_replace('-', '', $order_uuid->__toString());

        $query = preg_replace('/\s+/', ' ',
            "INSERT INTO `sc_orders`
                 (
                   `order_uuid`,
                   `order_status_id`,
                   `wishlist_flag`, `preorder_flag`, `backorder_flag`,
                   `currency_id`,
                   `customer_uuid`, `broker_uuid`, `seller_uuid`,
                   `order_number`, `confirmation_number`,
                   `cart_uuid`, `cart_rand`,
                   `version`,
                   `date_created`, `date_modified`,
                   `date_confirmed`
                 )
               VALUES
                 (
                    UNHEX(:order_uuid),
                    :order_status_id,
                    :wishlist_flag, :preorder_flag, :backorder_flag,
                    :currency_id,
                    UNHEX(:customer_uuid), UNHEX(:broker_uuid), UNHEX(:seller_uuid), 
                    :order_number, :confirmation_number,
                    :cart_uuid, :cart_rand,
                    :version,
                    :date_created, :date_modified,
                    :date_confirmed
                  )"
        );

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':order_uuid', $values['order_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':order_status_id', $values['order_status_id'], \PDO::PARAM_INT);
            $statement->bindValue(':wishlist_flag', $values['wishlist_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':preorder_flag', $values['preorder_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':backorder_flag', $values['backorder_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':currency_id', $values['currency_id'], \PDO::PARAM_INT);
            $statement->bindValue(':customer_uuid', $values['customer_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':broker_uuid', $values['confirmation_number'], \PDO::PARAM_STR);
            $statement->bindValue(':seller_uuid', $values['seller_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':order_number', $values['order_number'], \PDO::PARAM_STR);
            $statement->bindValue(':confirmation_number', $values['confirmation_number'], \PDO::PARAM_STR);
            $statement->bindValue(':cart_uuid', $values['cart_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':cart_rand', $values['cart_rand'], \PDO::PARAM_STR);
            $statement->bindValue(':version', $values['version'], \PDO::PARAM_INT);
            $statement->bindValue(':date_created', $values['date_created'], \PDO::PARAM_STR);
            $statement->bindValue(':date_modified', $values['date_modified'], \PDO::PARAM_STR);
            $statement->bindValue(':date_confirmed', $values['date_confirmed'], \PDO::PARAM_STR);
            unset($query, $values);

            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($statement);
                $entity->setIdentifier($order_uuid);
                $entity->metadata->id = $order_uuid;
                $entity->metadata->hash = Metadata::hash($entity);
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes an order.
     *
     * @param int|string $id
     *   ID or UUID of the entity to delete.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function delete(int|string $id): bool
    {
        $now = $this->now()->format('Y-m-d H:i:s');

        if (\is_string($id)) {
            $id = strtolower($id);
            $id = str_replace('-', '', $id);
            if (!UUID::validate($id)) {
                if (ctype_digit($id)) {
                    $id = (int) $id;
                } else {
                    return false;
                }
            }
        }

        $query = 'UPDATE `sc_orders` SET `cart_uuid` = NULL, `cart_rand` = NULL, `date_deleted` = ? ';
        if (\is_int($id)) {
            $query .= 'WHERE `order_id` = ?';
        } else {
            $query .= 'WHERE `order_uuid` = UNHEX(?)';
        }
        $query .= ' AND `date_deleted` IS NULL LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $now, \PDO::PARAM_STR);
            if (\is_int($id)) {
                $statement->bindValue(2, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(2, $id, \PDO::PARAM_STR);
            }

            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);
                if (!isset($this->orderItemsMapper)) {
                    $this->orderItemsMapper = new OrderItems($this->Registry);
                }
                return $this->orderItemsMapper->deleteOrder($id);
            }
        }

        return false;
    }

    /**
     * Converts an `Order` object to a key-value array.
     *
     * @param StoreCore\Order $object
     * @return array
     */
    final public static function toArray(Order $object): array
    {
        $result = [
            'order_uuid' => $object->hasIdentifier() ? str_replace('-', '', (string) $object->getIdentifier()) : null,
            'order_status_id' => $object->orderStatus === null ? null : OrderStatus::toInteger($object->orderStatus),
            'wishlist_flag' => $object instanceof WishList ? 0b1 : 0b0,
            'preorder_flag' => $object instanceof PreOrder ? 0b1 : 0b0,
            'backorder_flag' => $object instanceof BackOrder ? 0b1 : 0b0,
            'currency_id' => 978,
            'customer_uuid' => $object->getCustomerIdentifier() === null ? null : str_replace('-', '', (string) $object->getCustomerIdentifier()),
            'broker_uuid' => $object instanceof BrokerOrder ? str_replace('-', '', (string) $object->getBrokerIdentifier()) : null,
            'seller_uuid' => $object->getSellerIdentifier() === null ? null : str_replace('-', '', (string) $object->getSellerIdentifier()),
            'order_number' => empty($object->orderNumber) ? null : (string) $object->orderNumber,
            'confirmation_number' => \is_string($object->confirmationNumber) ? $object->confirmationNumber : null,
            'cart_uuid' => null,
            'cart_rand' => null,
            'version' => $object->metadata->version,
            'date_created' => $object->metadata->dateCreated->format('Y-m-d H:i:s'),
            'date_modified' => $object->metadata->dateModified === null ? null : $object->metadata->dateModified->format('Y-m-d H:i:s'),
            'date_confirmed' => $object->orderDate instanceof \DateTimeInterface ? $object->orderDate->format('Y-m-d H:i:s') : null,
        ];

        if (
            $object instanceof Cart
            && $object->getCartID() !== null
        ) {
            $result['cart_uuid'] = $object->getCartID()->getUUID()->__toString();
            $result['cart_uuid'] = str_replace('-', '', $result['cart_uuid']);
            $result['cart_rand'] = $object->getCartID()->getToken();
        }

        return $result;
    }

    /**
     * Maps order data to an order object.
     *
     * @param array $data
     *   Key-value array with `Order` data.
     *
     * @return StoreCore\Order
     *   Returns a StoreCore `Order` model or one of its subclasses.
     */
    private function toObject(array $data): Order
    {
        if (
            !empty($data['cart_uuid'])
            && !empty($data['cart_rand'])
            && $data['date_confirmed'] === null
        ) {
            $result = new Cart($this->Registry);
            $cart_id = new CartID($data['cart_uuid'], $data['cart_rand']);
            $result->setCartID($cart_id);
        } elseif ($data['wishlist_flag'] === 1) {
            $result = new WishList($this->Registry);
        } elseif ($data['preorder_flag'] === 1) {
            $result = new PreOrder($this->Registry);
        } elseif ($data['backorder_flag'] === 1) {
            $result = new PreOrder($this->Registry);
        } else {
            $result = new Order($this->Registry);
        }

        $result->metadata = new Metadata($data['date_created'], $data['date_modified']);
        $result->metadata->version = $data['version'];
        $result->setIdentifier($data['order_uuid']);

        if ($data['date_confirmed'] !== null) {
            $result->orderDate = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $data['date_confirmed']);
        }

        return $result;
    }

    /**
     * Gets an order from the database.
     *
     * @param int|string $id
     * @return StoreCore\IdentityInterface|null|false
     */
    public function read(int|string $id): IdentityInterface|null|false
    {
        if (\is_string($id)) {
            if (UUID::validate($id)) {
                $id = str_replace('-', '', $id);
                $id = strtolower($id);
            } elseif (\ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        $query = preg_replace('/\s+/', ' ',
            "SELECT
                    `order_status_id`,
                    `wishlist_flag`, `preorder_flag`, `backorder_flag`,
                    `currency_id`,
                    HEX(`customer_uuid`) AS `customer_uuid`,
                    HEX(`broker_uuid`) AS `broker_uuid`, HEX(`seller_uuid`) AS `seller_uuid`,
                    `date_confirmed`, `order_number`, `confirmation_number`,
                    `cart_uuid`, `cart_rand`,
                    `date_created`, `date_modified`, `version`
               FROM `sc_orders`
              WHERE `order_uuid` = UNHEX(?)
                AND `date_deleted` IS NULL
              LIMIT 1"
        );

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(1, $id, \PDO::PARAM_STR);
            if ($statement->execute() !== false) {
                $result = $statement->fetch(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                unset($query, $statement);
                if ($result !== false) {
                    $result['order_uuid'] = $id;
                    return $this->toObject($result);
                }
            }
        }

        return false;
    }

    /**
     * Updates the order data in the database.
     *
     * @param StoreCore\IdentityInterface &$entity
     * @return bool
     */
    public function update(IdentityInterface &$entity): bool
    {
        $now = $this->now();

        if (!$entity->hasIdentifier()) {
            return false;
        }

        $values = $this->toArray($entity);
        $values['date_modified'] = $now->format('Y-m-d H:i:s');
        $values['version'] = $entity->metadata->version + 1;

        $query = preg_replace('/\s+/', ' ',
            "UPDATE `sc_orders`
                SET `order_status_id`     = :order_status_id,
                    `wishlist_flag`       = :wishlist_flag,
                    `preorder_flag`       = :preorder_flag,
                    `backorder_flag`      = :backorder_flag,
                    `currency_id`         = :currency_id,
                    `customer_uuid`       = UNHEX(:customer_uuid),
                    `broker_uuid`         = UNHEX(:broker_uuid),
                    `seller_uuid`         = UNHEX(:seller_uuid),
                    `order_number`        = :order_number,
                    `confirmation_number` = :confirmation_number,
                    `cart_uuid`           = :cart_uuid,
                    `cart_rand`           = :cart_rand,
                    `version`             = :version,
                    `date_modified`       = :date_modified,
                    `date_confirmed`      = :date_confirmed
              WHERE `order_uuid` = UNHEX(:order_uuid)
                AND `version` < :compare_version
                AND `date_deleted` IS NULL
              LIMIT 1"
        );

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':order_status_id', $values['order_status_id'], \PDO::PARAM_INT);
            $statement->bindValue(':wishlist_flag', $values['wishlist_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':preorder_flag', $values['preorder_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':backorder_flag', $values['backorder_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':currency_id', $values['currency_id'], \PDO::PARAM_INT);
            $statement->bindValue(':customer_uuid', $values['customer_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':broker_uuid', $values['confirmation_number'], \PDO::PARAM_STR);
            $statement->bindValue(':seller_uuid', $values['seller_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':order_number', $values['order_number'], \PDO::PARAM_STR);
            $statement->bindValue(':confirmation_number', $values['confirmation_number'], \PDO::PARAM_STR);
            $statement->bindValue(':cart_uuid', $values['cart_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':cart_rand', $values['cart_rand'], \PDO::PARAM_STR);
            $statement->bindValue(':version', $values['version'], \PDO::PARAM_INT);
            $statement->bindValue(':date_modified', $values['date_modified'], \PDO::PARAM_STR);
            $statement->bindValue(':date_confirmed', $values['date_confirmed'], \PDO::PARAM_STR);

            $statement->bindValue(':order_uuid', $values['order_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':compare_version', $values['version'], \PDO::PARAM_INT);

            if ($statement->execute() !== false) {
                $statement->closeCursor();
                unset($query, $statement);

                $entity->metadata->dateModified = $now;
                $entity->metadata->version = $values['version'];
                $entity->metadata->hash = Metadata::hash($entity);
                return true;
            }
        }

        return false;
    }
}
