<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \PDOException as DatabaseException;
use Psr\Container\ContainerInterface;
use StoreCore\Currency;
use StoreCore\Registry;

use function \in_array;
use function \is_int;
use function \is_string;
use function \strlen;

/**
 * Currencies.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://en.wikipedia.org/wiki/ISO_4217 ISO 4217
 * @see     https://en.wikipedia.org/wiki/List_of_currencies_in_Europe List of currencies in Europe
 */
class Currencies extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $currencyNumbers
     *   Most used world currencies and frequently used European currencies.
     */
    private array $currencyNumbers = [
         36 => 'AUD',
        124 => 'CAD', 156 => 'CNY',
        203 => 'CZK', 208 => 'DKK',
        344 => 'HKD', 348 => 'HUF', 352 => 'ISK', 356 => 'INR', 392 => 'JPY',
        410 => 'KRW', 484 => 'MXN',
        554 => 'NZD', 578 => 'NOK',
        643 => 'RUB',
        702 => 'SGD', 710 => 'ZAR', 752 => 'SEK', 756 => 'CHF', 764 => 'THB',
        826 => 'GBP', 840 => 'USD',
        901 => 'TWD', 946 => 'RON', 949 => 'TRY', 978 => 'EUR', 980 => 'UAH', 985 => 'PLN', 986 => 'BRL',
    ];

    /**
     * Converts key-value pairs into a currency model object.
     *
     * @internal
     * @param array $params
     * @return StoreCore\Currency
     */
    private function createCurrency(array $params): Currency
    {
        // Add the created currency to the class cache.
        if (!isset($this->currencyNumbers[$params['currency_id']])) {
            $this->currencyNumbers[$params['currency_id']] = $params['currency_code'];
        }

        return new Currency(
            $params['currency_id'],
            $params['currency_code'],
            $params['digits'],
            $params['currency_symbol'],
        );
    }

    /**
     * Creates a currency value object from a currency code.
     *
     * @param string $currency_code
     * @return StoreCore\Currency
     * @see https://en.wikipedia.org/wiki/Albanian_lek Albanian lek (leku shqiptar)
     * @see https://en.wikipedia.org/wiki/Swiss_franc Swiss franc
     * @see https://en.wikipedia.org/wiki/Czechoslovak_koruna Czechoslovak koruna
     * @see https://en.wikipedia.org/wiki/Danish_krone Danish krone
     * @see https://en.wikipedia.org/wiki/Euro Euro
     * @see https://en.wikipedia.org/wiki/Pound_sterling Pound sterling
     * @see https://en.wikipedia.org/wiki/Norwegian_krone Norwegian krone
     * @see https://en.wikipedia.org/wiki/Swedish_krona Swedish krona
     */
    public static function createFromString(string $currency_code): Currency
    {
        $currency_code = str_replace('.', '', $currency_code);
        $currency_code = trim($currency_code);
        $currency_code = strtoupper($currency_code);

        switch ($currency_code) {
            case 'ALL':
            case 'ALK': // Before 1990
                return new Currency(8, 'ALL', 2, 'L');
                break;
            case 'CHF':
            case 'SFR': // Matches symbol SFr.
                return new Currency(756, 'CHF', 2, 'CHF');
                break;
            case 'CZK':
                return new Currency(203, 'CZK', 2, 'Kčs');
                break;
            case 'DKK':
                return new Currency(208, 'DKK', 2, 'kr.');
                break;
            case 'EUR':
            case 'EURO':
                return new Currency(978, 'EUR', 2, '€');
                break;
            case 'GBP':
                return new Currency(826, 'GBP', 2, '£');
                break;
            case 'NOK':
                return new Currency(578, 'NOK', 2, 'kr');
                break;
            case 'SEK':
                return new Currency(752, 'SEK', 2, 'kr');
                break;
            case 'US$':
            case 'USD':
                return new Currency(840, 'USD', 2, '$');
                break;
            default:
                $registry = Registry::getInstance();
                $currencies = new Currencies($registry);
                return $currencies->get($currency_code);
                break;
        }
    }

    /**
     * Checks if a currency exists.
     *
     * @param string|int $iso_currency_code
     *   ISO 4217 currency identifier as a three-letter abbreviation (EUR, GBP, USD, etc.)
     *   or a number (978 for EUR, 826 for GBP, 840 for USD, etc.).
     *
     * @return bool
     *   Returns `true` if the currency code exists or `false` if it does not.
     */
    public function exists(string|int $iso_currency_code): bool
    {
        if (is_string($iso_currency_code)) {
            $iso_currency_code = trim($iso_currency_code);
            $iso_currency_code = strtoupper($iso_currency_code);
            if (strlen($iso_currency_code) !== 3) return false;
        }

        if (is_int($iso_currency_code) && $iso_currency_code > 999) {
            return false;
        }

        try {
            if (is_string($iso_currency_code)) {
                if (in_array($iso_currency_code, $this->currencyNumbers)) return true;
                $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_currencies` WHERE `currency_code` = :currency_code');
                $statement->bindValue(':currency_code', $iso_currency_code, \PDO::PARAM_STR);
            } elseif (is_int($iso_currency_code)) {
                if (isset($this->currencyNumbers[$iso_currency_code])) return true;
                $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_currencies` WHERE `currency_id` = :currency_code');
                $statement->bindValue(':currency_code', $iso_currency_code, \PDO::PARAM_INT);
            } else {
                return false;
            }

            if ($statement->execute() === false) return false;
            return $statement->fetchColumn(0) === 1 ? true : false;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function get(string $id)
    {
        if (!$this->has($id)) throw new NotFoundException();

        try {
            /*
                   SELECT `currency_id`, `currency_code`, `digits`, `currency_symbol`
                     FROM `sc_currencies`
                    WHERE `currency_id` = (int) $id
                       OR `currency_code` = ?
                          LIMIT 1
             */
            $statement = $this->Database->prepare(
                'SELECT `currency_id`, `currency_code`, `digits`, `currency_symbol` FROM `sc_currencies` WHERE `currency_id` = '
                . (int) $id . ' OR `currency_code` = ? LIMIT 1'
            );
            $statement->execute([$id]); 
            $row = $statement->fetch(\PDO::FETCH_ASSOC);
            return $this->createCurrency($row);
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Gets the default store currency for a given store.
     *
     * @param int $store_id
     *   Unique identifier (ID) of the store.
     *
     * @return StoreCore\Currency|null
     *   Returns an indexed array with the ISO currency number as the numeric
     *   key and a `StoreCore\Currency` object as the value.  If a store has no
     *   default currency or the store does not exist, `null` is returned.
     */
    public function getDefaultStoreCurrency(int $store_id): ?Currency
    {
        try {
            /*
                   SELECT `s`.`currency_id`, `c`.`currency_code`, `c`.`digits`, `c`.`currency_symbol`
                     FROM `sc_store_currencies` AS `s`
                LEFT JOIN `sc_currencies` AS `c`
                       ON `s`.`currency_id` = `c`.`currency_id`
                    WHERE `s`.`store_id` = :store_id
                      AND `s`.`default_flag` = 1
             */
            $statement = $this->Database->prepare('SELECT `s`.`currency_id`, `c`.`currency_code`, `c`.`digits`, `c`.`currency_symbol` FROM `sc_store_currencies` AS `s` LEFT JOIN `sc_currencies` AS `c` ON `s`.`currency_id` = `c`.`currency_id` WHERE `s`.`store_id` = :store_id AND `s`.`default_flag` = 1');
            $statement->bindValue(':store_id', $store_id, \PDO::PARAM_INT);
            if ($statement->execute() === false) return null;

            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($statement);

            if ($result === false || empty($result)) {
                return null;
            }

            $result = $result[0];
            return $this->createCurrency($result);

        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            return null;
        }
    }

    /**
     * Gets all currencies for a given store.
     *
     * @param int $store_id
     *   Unique identifier (ID) of the store.
     *
     * @return array
     *   Returns an indexed array of `StoreCore\Currency` objects with the
     *   currency ID (and numeric ISO currency code) as the key.  If the store
     *   has a default currency, it is returned as the first array element.
     *   If the store has no currencies at all, this method returns an array
     *   with the euro as the system default.
     */
    public function getStoreCurrencies(int $store_id): array
    {
        try {
            /*
                   SELECT `s`.`currency_id`, `c`.`currency_code`, `c`.`digits`, `c`.`currency_symbol`
                     FROM `sc_store_currencies` AS `s`
                LEFT JOIN `sc_currencies` AS `c`
                       ON `s`.`currency_id` = `c`.`currency_id`
                    WHERE `s`.`store_id` = :store_id
                 ORDER BY `s`.`default_flag` DESC, `c`.`currency_code` ASC
             */
            $statement = $this->Database->prepare('SELECT `s`.`currency_id`, `c`.`currency_code`, `c`.`digits`, `c`.`currency_symbol` FROM `sc_store_currencies` AS `s` LEFT JOIN `sc_currencies` AS `c` ON `s`.`currency_id` = `c`.`currency_id` WHERE `s`.`store_id` = :store_id ORDER BY `s`.`default_flag` DESC, `c`.`currency_code` ASC');
            $statement->bindValue(':store_id', $store_id, \PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($statement);
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            $result = false;
        }

        if ($result === false || empty($result)) {
            $default_currency = new Currency();
            $result = [$default_currency->id => $default_currency];
            return $result;
        }

        $store_currencies = [];
        foreach ($result as $row) {
            $currency_id = (int) $row['currency_id'];
            $currency = $this->createCurrency($row);
            $store_currencies[$currency_id] = $currency;
        }
        return $store_currencies;
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        return $this->exists($id);
    }

    /**
     * Lists all currencies.
     *
     * @param void
     *
     * @return array
     *   Returns an array of `StoreCore\Currency` objects with their ISO
     *   currency numbers as the array keys.  The array is sorted alphabetically
     *   by ISO currency code.
     */
    public function list(): array
    {
        $currencies = array();
        $result = $this->Database->query(
            'SELECT `currency_id`, `currency_code`, `digits`, `currency_symbol` FROM `sc_currencies` ORDER BY `currency_code` ASC'
        );
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $currency = $this->createCurrency($row);
            $currencies[$currency->id] = $currency;
        }
        return $currencies;
    }

    /**
     * Lists all currently available currencies by name.
     *
     * @param void
     *
     * @return array
     *   Returns an indexed array with the ISO currency number as the key and
     *   a string with the currency name and ISO currency code as the value,
     *   sorted by the currency name in alphabetical order.  Currency code `XXX`
     *   with currency number `999` for “No currency” is not included.
     */
    public function listCurrencyNames(): array
    {
        $statement = $this->Database->prepare(
            "SELECT `currency_id`, CONCAT(`currency_name`, ' (', `currency_code`, ')') AS `currency` FROM `sc_currencies` WHERE `currency_id` <> 999 ORDER BY `currency_name` ASC"
        );
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_NUM);
        $statement->closeCursor();
        $statement = null;

        $currencies = [];
        foreach ($result as $currency) {
            $currencies[$currency[0]] = $currency[1];
        }
        return $currencies;
    }
}
