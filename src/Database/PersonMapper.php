<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\CRM\Person;
use StoreCore\Geo\Place;
use StoreCore\I18N\CountryRepository;
use StoreCore\Types\{Date, DateTime, URL};
use StoreCore\Types\{UUID, UUIDFactory};

use function \json_encode;
use function \md5;

use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

/**
 * Person mapper.
 *
 * Creates and updates database records for Person value objects.
 *
 * @package StoreCore\CRM
 * @version 0.3.0
 */
class PersonMapper extends AbstractDataAccessObject implements CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    protected const PRIMARY_KEY = 'person_uuid';
    protected const TABLE_NAME  = 'sc_persons';

    /**
     * Creates a new entity tag (ETag) for a person.
     *
     * @internal
     * @param StoreCore\CRM\Person $person
     * @return string
     */
    private function createETag(Person $person): string
    {
        $result = json_encode($person, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $result = md5($result);
        return $result;
    }

    /**
     * Gets a person by the unique person ID.
     *
     * @param StoreCore\Types\UUID|string $person_uuid
     *   Universally unique person identifier as an UUID object or a string.
     *
     * @return StoreCore\CRM\Person|null
     *   Returns a `Person` object or `null` if the person could not be found.
     */
    public function get(UUID|string $person_uuid): ?Person
    {
        $data = $this->read((string) $person_uuid);
        if (\is_array($data) && count($data) === 1) {
            return $this->getPersonObject($data[0]);
        } else {
            return null;
        }
    }

    /**
     * Maps the person’s data to a person object.
     *
     * @param array $keyed_data
     *   Array consisting of key/value pairs for a single row in the
     *   `sc_persons` database table.
     *
     * @return StoreCore\CRM\Person|null
     *   Returns a core object representing a person or `null` if the person
     *   is unknown.
     */
    private function getPersonObject(array $keyed_data): ?Person
    {
        if (!array_key_exists(self::PRIMARY_KEY, $keyed_data)) {
            return null;
        }

        $person = new Person();
        $person->setIdentifier($keyed_data['person_uuid']);
        $person->isAnonymous = ($keyed_data['anonymized_flag'] === 1) ? true : false;
        $person->gender = $keyed_data['gender'];

        $person->dateCreated = new DateTime($keyed_data['date_created'], new \DateTimeZone('UTC'));
        if (isset($keyed_data['date_modified'])) $person->dateModified = new DateTime($keyed_data['date_modified'], new \DateTimeZone('UTC'));
        if (isset($keyed_data['date_deleted'])) $person->dateDeleted = new DateTime($keyed_data['date_deleted'], new \DateTimeZone('UTC'));

        // Skip other properties if the person is anonymized.
        if ($person->isAnonymous) return $person;

        $keyed_data = array_filter($keyed_data);
        if (isset($keyed_data['honorific_prefix'])) $person->honorificPrefix = $keyed_data['honorific_prefix'];
        if (isset($keyed_data['given_name'])) $person->givenName = $keyed_data['given_name'];
        if (isset($keyed_data['additional_name'])) $person->additionalName = $keyed_data['additional_name'];
        if (isset($keyed_data['family_name'])) $person->familyName = $keyed_data['family_name'];
        if (isset($keyed_data['honorific_suffix'])) $person->honorificSuffix = $keyed_data['honorific_suffix'];
        if (isset($keyed_data['full_name'])) $person->fullName = $keyed_data['full_name'];

        if (isset($keyed_data['telephone_number'])) $person->telephoneNumber = $keyed_data['telephone_number'];

        if (isset($keyed_data['nationality'])) {
            $repository = new CountryRepository($this->Registry);
            $person->nationality = $repository->get($keyed_data['nationality']);
        }

        if (isset($keyed_data['birth_date'])) $person->birthDate = new Date($keyed_data['birth_date']);
        if (isset($keyed_data['birth_place'])) $person->birthPlace = new Place($keyed_data['birth_place']);
        if (isset($keyed_data['death_date'])) $person->deathDate = new Date($keyed_data['death_date']);
        if (isset($keyed_data['death_place'])) $person->deathPlace = new Place($keyed_data['death_place']);

        if (isset($keyed_data['same_as'])) $person->sameAs = new URL($keyed_data['same_as']);

        if (isset($keyed_data['email_address_id'])) {
            $repository = new EmailAddressRepository($this->Registry);
            if ($repository->has($keyed_data['email_address_id'])) {
                $person->emailAddress = $repository->get($keyed_data['email_address_id']);
            }
        }

        $person->eTag = $this->createETag($person);

        return $person;
    }

    /**
     * Saves a person to the database.
     *
     * @param StoreCore\CRM\Person $person
     *   Person to save as a core value object.
     *
     * @return bool
     *   Returns `true` on success or `false` on failures.
     *
     * @uses \StoreCore\AbstractSubject::notify()
     *   Notifies observers of the `Person` object of changes to the data.
     */
    public function save(Person &$person): bool
    {
        // If a person with a known identity has an ETag that is identical to
        // a newly created ETag, the entity has not changed.
        if (
            $person->hasIdentifier()
            && $person->eTag === $this->createETag($person)
        ) {
            return true;
        }

        $data = array();
        if ($person->hasIdentifier()) $data[self::PRIMARY_KEY] = (string) $person->getIdentifier();

        $data['anonymized_flag'] = $person->isAnonymous ? 1 : 0;

        if ($person->emailAddress !== null) {
            try {
                $repository = new EmailAddressRepository();
                if ($repository->has($person->emailAddress) !== true) {
                    if ($repository->save($person->emailAddress) !== true) {
                        return false;
                    }
                }
                unset($repository);
                $data['email_address_id'] = EmailAddressRepository::getEmailAddressHash($person->emailAddress);
            } catch (\Exception $e) {
                $this->Logger->error('Database error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
                return false;
            }
        }

        if (isset($person->dateCreated) && $person->dateCreated !== null) $data['date_created'] = (string) $person->dateCreated;
        if ($person->dateDeleted !== null) $data['date_deleted'] = (string) $person->dateDeleted;
        if ($person->dateModified !== null) $data['date_modified'] = (string) $person->dateModified;

        $data['gender'] = $person->gender;

        $data['honorific_prefix'] = $person->honorificPrefix;
        $data['given_name'] = $person->givenName;
        $data['additional_name'] = $person->additionalName;
        $data['family_name'] = $person->familyName;
        $data['honorific_suffix'] = $person->honorificSuffix;
        $data['full_name'] = $person->fullName;

        $data['telephone_number'] = $person->telephoneNumber;
        
        $data['nationality'] = ($person !== null) ? $person->nationality->identifier : null;

        $data['birth_date'] = ($person->birthDate !== null) ? (string) $person->birthDate : null;
        $data['birth_place'] = ($person->birthPlace !== null) ? $person->birthPlace->name : null;
        $data['death_date'] = ($person->deathDate !== null) ? (string) $person->deathDate : null;
        $data['death_place'] = ($person->deathPlace !== null) ? $person->deathPlace->name : null;

        $data['same_as'] = ($person->sameAs !== null) ? (string) $person->sameAs : null;

        if ($person->hasIdentifier()) {
            $data[self::PRIMARY_KEY] = $person->getIdentifier();
            $person->dateModified = new DateTime('now', new \DateTimeZone('UTC'));
            $data['date_modified'] = (string) $person->dateModified;
            $result = $this->update($data);
        } else {
            $person->dateCreated = new DateTime('now', new \DateTimeZone('UTC'));
            $data['date_created'] = (string) $person->dateCreated;

            $uuid_factory = new UUIDFactory($this->Registry);
            $data[self::PRIMARY_KEY] = $uuid_factory->createUUID();

            $result = $this->create($data);
            if ($result !== false) {
                $person->setIdentifier($data[self::PRIMARY_KEY]);
                $result = true;
            }
        }

        // Create or update ETag.
        $person->eTag = $this->createETag($person);

        // Notify all observers of persisted changes.
        if ($result !== false) $person->notify();

        return $result;
    }
}
