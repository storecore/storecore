<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateTimeInterface;
use \DateTimeZone;
use \JsonSerializable;
use StoreCore\Types\DateTime;
use StoreCore\Types\UUID;

use function \is_string;
use function \json_encode;
use function \md5;

use const \JSON_THROW_ON_ERROR;
use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

/**
 * Technical metadata for persisted objects.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/DateTime Schema.org data type `DateTime`
 * @see     https://schema.org/dateCreated Schema.org property `dateCreated`
 * @see     https://schema.org/dateModified Schema.org property `dateModified`
 */
class Metadata
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var \DateTimeInterface $dateCreated
     *   The date and time the object was first created.
     */
    public readonly DateTimeInterface $dateCreated;

    /**
     * @var null|\DateTimeInterface $dateModified
     *   The date and time the object was last modified.  This property MAY be
     *   `null` if the object was never modified since it was first created.
     */
    public ?DateTimeInterface $dateModified = null;

    /**
     * @var string $hash
     *   Hash value derived from an object or some of the object properties to
     *   detect changes of the object state.
     */
    public string $hash = '';

    /**
     * @var null|int|StoreCore\Types\UUID $id
     *   Unique identifier and primary or unique key value of entities that are
     *   stored in a dedicated database table.  MUST be `null` if an entity was
     *   not yet saved to the local database.  MAY be an integer or an UUID,
     *   depending on the context.
     */
    public null|int|UUID $id = null;

    /**
     * @var int $version
     *   Current version of the data object.  Defaults to `1` for the initial
     *   creation of the object.
     */
    public int $version = 1;

    /**
     * Creates object metadata.
     *
     * @param \DateTimeInterface|string|null $date_created
     * @param \DateTimeInterface|string|null $date_modified
     * @uses StoreCore\Types\DateTime::__construct
     */
    public function __construct(
        DateTimeInterface|string|null $date_created = 'now',
        DateTimeInterface|string|null $date_modified = null,
    ) {
        if ($date_created === null) {
            $date_created = 'now';
        }
        if (is_string($date_created)) {
            $date_created = new DateTime($date_created, new DateTimeZone('UTC'));
        }
        $this->dateCreated = $date_created;

        if ($date_modified !== null) {
            if (is_string($date_modified)) {
                $date_modified = new DateTime($date_modified, $this->dateCreated->getTimezone());
            }
            $this->dateModified = $date_modified;
        }
    }

    /**
     * Generates a hash value to detect changes of JSON serializable objects.
     *
     * @param \JsonSerializable $object
     * @return string
     */
    public static function hash(JsonSerializable $object): string
    {
        $json = json_encode($object, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR, 512);
        return md5($json, false);
    }

    /**
     * Returns the metadata as a key-value array.
     *
     * @internal
     * @param void
     * @return array
     */
    public function toArray(): array
    {
        return [
            'date_created' => $this->dateCreated->format('Y-m-d H:i:s'),
            'date_modified' => $this->dateModified === null ? null : $this->dateModified->format('Y-m-d H:i:s'),
            'id' => $this->id instanceof \Stringable ? str_replace('-', '', $this->id->__toString()) : $this->id,
            'version' => $this->version,
        ];
    }
}
