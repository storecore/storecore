<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateInterval;
use \PDOException as DatabaseException;
use \ReflectionClass;

use Psr\Clock\ClockInterface;
use Psr\Container\ContainerInterface;

use StoreCore\ClockTrait;
use StoreCore\Registry;
use StoreCore\CMS\AbstractCreativeWork;
use StoreCore\FileSystem\ObjectCache;
use StoreCore\Types\URL;
use StoreCore\Types\{UUID, UUIDFactory};

use function \abs;
use function \array_search;
use function \ctype_digit;
use function \in_array;
use function \is_array;
use function \is_int;
use function \is_string;
use function \isset;
use function \json_encode;

use const \JSON_UNESCAPED_SLASHES;
use const \JSON_UNESCAPED_UNICODE;

/**
 * Creative work mapper.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
 * @version 1.0.0-alpha.1
 */
class CreativeWorkMapper extends AbstractDataAccessObject implements ClockInterface, ContainerInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const PRIMARY_KEY = 'creative_work_id';
    public const TABLE_NAME  = 'sc_creative_works';

    /**
     * @var array $map
     *   Key-pair array that maps public string UUIDs to internal integer IDs.
     */
    private array $map = [];

    /**
     * @var StoreCore\FileSystem\ObjectCache $objectCache
     *   Cache that stores serializable objects in the file system.
     */
    private ObjectCache $objectCache;

    /**
     * Creates a data mapper for creative works.
     * 
     * @param StoreCore\Registry $registry
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);

        $this->objectCache = new ObjectCache();
        if ($this->Registry->has('Logger')) {
            $this->objectCache->setLogger($this->Registry->get('Logger'));
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @uses StoreCore\FileSystem\ObjectCacheReader::has()
     * @uses StoreCore\FileSystem\ObjectCache::delete()
     */
    public function delete(mixed $value, ?string $key = null): bool
    {
        if (
            $key === null
            && (is_int($value) || is_string($value))
            && $this->has($value)
        ) {
            // Map integer ID to string UUID for the `ObjectCache`.
            $uuid = $value;
            if (is_int($uuid)) {
                $uuid = array_search($uuid, $this->map, true);
                if ($uuid === false) {
                    return false;
                }
            }

            // Delete the cache file, update deletion date and delete texts.
            if ($this->objectCache->has($uuid)) {
                // Delete the cached object
                $this->objectCache->delete($uuid);

                // Store the deletion date
                $key = $this->map[$uuid];
                $this->update(
                    [
                        self::PRIMARY_KEY => $key,
                        'date_deleted' => $this->now()
                    ]
                );

                // Remove the object from the full-text index
                $mapper = new CreativeWorkTextMapper($this->Registry);
                $mapper->delete($key);

                // Drop the key map reference to the object
                unset($this->map[$uuid]);
                return true;
            } else {
                return false;
            }
        }

        return parent::delete($value, $key);
    }

    /**
     * Gets a creative work from the database.
     * 
     * @var int|string $id
     *   Unique ID or UUID of the creative work.
     * 
     * @return StoreCore\CMS\AbstractCreativeWork|false
     *   Returns the requested creative work if it exists.
     * 
     * @throws Psr\Container\ContainerExceptionInterface
     *
     * @throws Psr\Container\NotFoundExceptionInterface
     */
    public function get(int|string $id): AbstractCreativeWork
    {
        if (!$this->has($id)) {
            throw new NotFoundException();
        }

        if (is_int($id)) {
            $id = array_search($id, $this->map, true);
            if ($id === false) {
                throw new NotFoundException();
            }
        }

        if ($this->objectCache->has($id)) {
            return $this->objectCache->get($id);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Determines whether a creative work exists.
     *
     * @var int|string $id
     *   Unique ID or UUID of a creative work.
     *
     * @return bool
     *   Returns `true` if the creative work exists, `false` otherwise.
     */
    public function has(int|string $id): bool
    {
        if (is_string($id)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } elseif (isset($this->map[$id])) {
                return true;
            } elseif (!UUID::validate($id)) {
                return false;
            }
        }

        if (is_int($id) && in_array($id, $this->map, true)) {
            return true;
        }

        try {
            if (is_int($id)) {
                $statement = $this->Database->prepare(
                    'SELECT `creative_work_uuid`, `creative_work_id` FROM `sc_creative_works` WHERE `creative_work_id` = :id'
                );
                $statement->bindValue(':id', $id, \PDO::PARAM_INT);
            } else {
                $statement = $this->Database->prepare(
                    'SELECT `creative_work_uuid`, `creative_work_id` FROM `sc_creative_works` WHERE `creative_work_uuid` = :uuid'
                );
                $statement->bindValue(':uuid', $id, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $key_pair = $statement->fetch(\PDO::FETCH_KEY_PAIR);
                $statement->closeCursor();
                if (is_array($key_pair)) {
                    $this->map += $key_pair;
                    return true;
                }
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }

        return false;
    }

    public function set(string $key, AbstractCreativeWork &$object, null|int|DateInterval $ttl = null): bool
    {
        $data = $this->toArray($object);
        $data['creative_work_uuid'] = $key;

        // Map a known UUID to an existing ID.
        if (
            $data['creative_work_id'] === null
            && $this->has($data['creative_work_uuid'])
        ) {
            $data['creative_work_id'] = $this->map[$data['creative_work_uuid']];
        }

        if ($ttl !== null) {
            if (is_int($ttl)) {
                // ISO 8601 period (P) with a time (T) in seconds (S)
                $ttl = new DateInterval('PT' . abs($ttl) . 'S');
            }

            $expires = $this->now();
            $expires->add($ttl);
            $object->expires = $expires;
            $data['date_expires'] = $expires->format('Y-m-d H:i:s.u');
            unset($expires, $ttl);
        }

        if ($data['creative_work_id'] === null) {
            $last_insert_id = $this->create($data);
            if ($last_insert_id === false) {
                return false;
            } else {
                $object->id = (int) $last_insert_id;
            }
        } else {
            $result = $this->update($data);
            if ($result === false) {
                return false;
            }
        }
        unset($data);

        $mapper = new CreativeWorkTextMapper($this->Registry);
        $result = $mapper->set($object);
        if ($result === false) {
            return false;
        }

        return $result;
    }

    private function toArray(AbstractCreativeWork $object): array
    {
        $result = [
            'creative_work_id' => is_int($object->id) ? $object->id : null,
            'creative_work_uuid' => $object->hasIdentifier() ? (string) $object->getIdentifier() : null,
            'creative_work_type_id' => CreativeWorkTypes::fromCreativeWork($object),
            'version' => 1,
            'date_published' => $object->datePublished === null ? null : (string) $object->datePublished,
            'date_modified' => $object->dateModified === null ? null : (string) $object->dateModified,
            'date_expires' => $object->expires === null ? null : $object->expires->format('Y-m-d H:i:s.u'),
            'creative_work_json' => json_encode($object, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
        ];

        return $result;
    }
}
