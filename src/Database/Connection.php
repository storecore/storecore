<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace StoreCore\Database;

use Psr\Log\{LoggerAwareInterface, LoggerInterface};
use StoreCore\LoggerFactory;
use StoreCore\Registry;

/**
 * Database connection.
 *
 * @package StoreCore\Core
 * @see     https://www.php.net/manual/en/pdo.construct.php PDO::__construct()
 * @see     https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html
 * @version 1.0.0-beta.1
 */
class Connection extends \PDO implements LoggerAwareInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0-beta.1';

    /**
     * @var array DRIVER_OPTIONS
     *   Options for the `PDO::__construct` constructor.  Force lower case
     *   column names, disable emulated prepares, and throw exceptions on errors.
     *
     * @see https://www.php.net/manual/en/mysqlinfo.concepts.buffering.php
     *   Queries are using the buffered mode by default.
     */
    private const array DRIVER_OPTIONS = [
        \PDO::ATTR_CASE => \PDO::CASE_LOWER,
        \PDO::ATTR_EMULATE_PREPARES => false,
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    ];

    /**
     * @var Psr\Log\LoggerInterface $Logger
     *   PSR-3 logger.
     */
    private LoggerInterface $Logger;

    /**
     * Creates a PDO instance for a connection to a database.
     *
     * @param null|StoreCore\Database\DataSourceName|string $dsn
     *   OPTIONAL data source name (DSN).  If the DNS is not supplied, it
     *   defaults to the global StoreCore database configuration provided by
     *   the `DataSourceName` class.  Using additional DSNs allows for high
     *   scalability over multiple databases, database servers, and other data
     *   sources.
     *
     * @param null|string $username
     *   OPTIONAL user name for the DSN string.  If not set, the global
     *   constant `STORECORE_DATABASE_DEFAULT_USERNAME` is used.
     *
     * @param null|string $password
     *   OPTIONAL password for the DSN string.  If the `$password` is not set,
     *   the global constant `STORECORE_DATABASE_DEFAULT_PASSWORD` is used.
     *
     * @throws \PDOException
     *   A catchable \PDOException is (re)thrown if the database connection
     *   fails.  The constructor will try to connect several times, after an
     *   increasing pause, before finally giving up.
     */
    public function __construct(
        null|DataSourceName|string $dsn = null,
        ?string $username = null,
        ?string $password = null
    ) {
        $registry = Registry::getInstance();
        if (!$registry->has('Logger')) {
            $factory = new LoggerFactory();
            $registry->set('Logger', $factory->createLogger());
            unset($factory);
        }
        $this->setLogger($registry->get('Logger'));

        if ($dsn === null) {
            $dsn = new DataSourceName();
        }
        if ($username === null) {
            $username = STORECORE_DATABASE_DEFAULT_USERNAME;
        }
        if ($password === null) {
            $password = STORECORE_DATABASE_DEFAULT_PASSWORD;
        }

        $seconds = 1;
        do {
            try {
                parent::__construct((string) $dsn, $username, $password, self::DRIVER_OPTIONS);
                $seconds = false;
            } catch (\PDOException $e) {
                if ((int) $e->getCode() === 1049) {
                    $this->Logger->alert('Unknown database (fatal error ' . $e->getCode() . '): ' . $e->getMessage());
                    throw new \RuntimeException($e->getMessage(), (int) $e->getCode(), $e);
                }

                // Retry to connect in 1, 2, 4, 8, 16, … seconds.
                $this->Logger->notice(
                    'Database connection time-out. Reconnecting in ' . $seconds . ' seconds.'
                );
                sleep($seconds);
                $seconds = $seconds * 2;
            }
        } while ($seconds !== false);
    }

    #[\Override]
    public function exec(string $statement): int|false
    {
        $statement = trim($statement);
        $statement = preg_replace('/\s+/', ' ', $statement);

        try {
            return parent::exec($statement);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    #[\Override]
    public function prepare(string $query, array $options = []): \PDOStatement|false
    {
        $query = trim($query);
        $query = preg_replace('/\s+/', ' ', $query);

        try {
            return parent::prepare($query, $options);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    #[\Override]
    public function query(string $query, ?int $fetchMode = null, mixed ...$fetchModeArgs): \PDOStatement|false
    {
        $query = trim($query);
        $query = preg_replace('/\s+/', ' ', $query);

        try {
            return parent::query($query, $fetchMode, $fetchModeArgs);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }

    /**
     * Sets a PSR-3 logger instance on the object.
     *
     * @param Psr\Log\LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->Logger = $logger;
    }
}
