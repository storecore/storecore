<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use function \defined;

/**
 * Data Source Name (DSN).
 *
 * @package StoreCore\Core
 * @see     https://www.php.net/manual/en/pdo.construct.php PDO::__construct
 * @see     https://www.php.net/manual/en/ref.pdo-mysql.connection.php PDO_MYSQL DSN
 * @version 1.0.0
 */
class DataSourceName implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var string $prefix
     *   The DSN prefix.  Defaults to 'mysql' for MySQL and MariaDB.
     */
    public string $prefix = 'mysql';

    /**
     * @var ?string $host
     *   The hostname on which the database server resides.
     *   Defaults to 'localhost', but MAY be set to null or an empty string.
     */
    public ?string $host = 'localhost';

    /**
     * @var ?string $port
     *   Optional.  The port number where the database server is listening.
     */
    public ?string $port = null;

    /**
     * @var ?string $dbname
     *   Optional.  The name of the database.  Although this property is
     *   set to the default StoreCore database name by default, it MAY be
     *   omitted if you only want to connect to a database server and not
     *   select a specific database.
     */
    public ?string $dbname = null;

    /**
     * @var ?string $charset
     *   Optional.  The character set.  Defaults to 'utf8mb4' for StoreCore.
     */
    public ?string $charset = 'utf8mb4';

    /**
     * Creates a DSN value object.
     *
     * @param void
     */
    public function __construct()
    {
        if (defined('STORECORE_DATABASE_DRIVER')) {
            $this->prefix = STORECORE_DATABASE_DRIVER;
        }

        if (defined('STORECORE_DATABASE_DEFAULT_HOST')) {
            $this->host = STORECORE_DATABASE_DEFAULT_HOST;
        }

        if (defined('STORECORE_DATABASE_DEFAULT_DATABASE')) {
            $this->dbname = STORECORE_DATABASE_DEFAULT_DATABASE;
        }
    }

    /**
     * Converts the DSN object to a DSN string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $dsn = $this->prefix . ':';
        if (!empty($this->host)) $dsn .= 'host=' . $this->host . ';';
        if (!empty($this->port)) $dsn .= 'port=' . $this->port . ';';
        if (!empty($this->dbname)) $dsn .= 'dbname=' . $this->dbname . ';';
        if (!empty($this->charset)) $dsn .= 'charset=' . $this->charset . ';';
        $dsn = rtrim($dsn, ';');
        return $dsn;
    }
}
