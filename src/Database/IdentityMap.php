<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Types\UUID;

/**
 * Identity map.
 *
 * Maps public Universally Unique IDs (UUIDs) as value objects
 * to internal primary keys as integers.
 *
 * @internal
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class IdentityMap
{
    private \WeakMap $map;

    public function __construct()
    {
        $this->map = new \WeakMap();
    }

    public function get(UUID $uuid): ?int
    {
        return $this->map->offsetGet($uuid);
    }

    public function has(UUID $uuid): bool
    {
        return $this->map->offsetExists($uuid);
    }

    public function set(UUID $uuid, int $key): void
    {
        $this->map->offsetSet($uuid, $key);
    }
}
