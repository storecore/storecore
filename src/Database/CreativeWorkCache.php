<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateInterval;
use \DateTime;
use \DateTimeImmutable;
use \Exception;
use \PDOException as DatabaseException;

use Psr\Clock\ClockInterface;
use Psr\SimpleCache\CacheInterface;

use StoreCore\ClockTrait;
use StoreCore\IdentityInterface;
use StoreCore\Registry;
use StoreCore\CMS\AbstractCreativeWork;
use StoreCore\Types\{UUID, UUIDFactory};

use function \ctype_digit;
use function \is_int;
use function \is_string;
use function \time;

/**
 * Write-through cache for creative works.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://www.php-fig.org/psr/psr-16/ PSR-16: Common Interface for Caching Libraries
 * @see     https://github.com/php-fig/simple-cache/blob/master/src/CacheInterface.php PSR-16 CacheInterface
 * @version 1.0.0-alpha.1
 */
class CreativeWorkCache extends AbstractModel implements CacheInterface, ClockInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\CreativeWorkMapper $mapper
     *   Data mapper that maps creative works to multiple database tables.
     */
    private readonly CreativeWorkMapper $mapper;

    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->mapper = new CreativeWorkMapper($this->Registry);
    }

    /**
     * Wipes clean the entire cache.
     * 
     * This method does NOT actually remove all creative works, but only the
     * creative works that were previously marked for deletion with `delete()`
     * or `deleteMultiple()`.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success and `false` on failure.
     */
    final public function clear(): bool
    {
        try {
            $result = $this->Database->exec('DELETE FROM `sc_creative_works` WHERE `date_deleted` IS NOT NULL');
            return $result === false ? false : true;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    public function delete(string $key): bool
    {
        $key = self::filterValidateCacheKey($key);
        return $this->deleteMultiple([$key]);
    }

    public function deleteMultiple(iterable $keys): bool
    {
        try {
            $result = true;
            foreach ($keys as $key) {
                $key = self::filterValidateCacheKey($key);
                if ($this->mapper->has($key)) {
                    if ($this->mapper->delete($key) === false) {
                        $result = false;
                    }
                }
            }
            return $result;
        } catch (CreativeWorkCacheKeyException $e) {
            $this->Logger->error('Cache exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw $e;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Filters and validates a cache key.
     *
     * @param mixed $key
     *
     * @return int|string
     *   Returns am `UNSIGNED MEDIUMINT` integer or a valid UUID as a string.
     * 
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   PSR-16 exception thrown if the `$key` is not a valid cache key.
     */
    private static function filterValidateCacheKey(mixed $key): int|string
    {
        if ($key instanceof UUID) {
            return (string) $key;
        }

        if (is_string($key) && ctype_digit($key)) {
            $key = (int) $key;
        }

        if (is_int($key)) {
            if ($key > 16777215 || $key < 1) {
                throw new CreativeWorkCacheKeyException('Invalid unsigned mediumint: ' . $key, time());
            } else {
                return $key;
            }
        }

        if (!is_string($key)) {
            $key = (string) $key;
        }

        if (UUID::validate($key)) {
            return $key;
        } else {
            throw new CreativeWorkCacheKeyException('Invalid UUID: (string) ' . $key, time());
        }
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $key = self::filterValidateCacheKey($key);
        if (!$this->has($key)) {
            return $default;
        }

        try {
            $result = $this->mapper->get($key);
            return ($result instanceof AbstractCreativeWork) ? $result : $default;
        } catch (Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new CreativeWorkCacheException($e->getMessage(), $e->getCode(), $e);
        }
        return $default;
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        return $result;
    }

    public function has(string $key): bool
    {
        $key = self::filterValidateCacheKey($key);
        return $this->mapper->has($key);
    }

    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $key = self::filterValidateCacheKey($key);
        return $this->mapper->set($key, $value, $ttl);
    }

    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $result = true;
        $factory = new UUIDFactory($this->Registry);
        foreach ($values as $value) {
            if ($value->hasIdentifier()) {
                $key = (string) $value->getIdentifier();
            } else {
                $key = $factory->createUUID();
                $value->setIdentifier($key);
                $key = (string) $key;
            }

            if ($this->set($key, $value, $ttl) !== true) {
                $result = false;
            }
        }
        return $result;
    }
}
