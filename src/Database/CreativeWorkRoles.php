<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \PDOException as DatabaseException;
use \SplFixedArray as FixedArray;
use StoreCore\Registry;

/**
 * Roles of organizations and persons in creative works.
 *
 * Note that Schema.org has synonyms and sub-properties.
 * For example, `creator` is a synonym of `author`.
 *
 * @internal
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class CreativeWorkRoles extends AbstractDataAccessObject implements CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const PRIMARY_KEY = 'creative_work_role_id';
    public const TABLE_NAME  = 'sc_creative_work_roles';

    /**
     * Lists all short creative work classes.
     * 
     * @param void
     *
     * @return iterable
     *
     * @throws Psr\SimpleCache\CacheException
     *   Throws a `StoreCore\Database\CreativeWorkCacheException` that
     *   implements the PSR-16 `Psr\SimpleCache\CacheException` interface.
     */
    public function list(): iterable
    {
        try {
            $sth = $this->Database->query('SELECT `creative_work_role_id`, `creative_work_role` FROM `sc_creative_work_roles` ORDER BY `creative_work_role` ASC');
            $result = $sth->fetchAll(\PDO::FETCH_KEY_PAIR);
            $sth->closeCursor();
            return FixedArray::fromArray($result, true);
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new CreativeWorkCacheException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
