<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2018, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \Countable;
use Psr\Container\ContainerInterface;
use StoreCore\IdentityInterface;
use StoreCore\RepositoryInterface;

use \Exception;
use \PDOException as DatabaseException;
use \RuntimeException;
use \TypeError;
use \ValueError;
use StoreCore\Registry;
use StoreCore\Store;
use StoreCore\Types\UUID;

use function \defined;
use function \is_int;

/**
 * Store repository.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class StoreRepository extends AbstractModel implements
    ContainerInterface,
    Countable,
    RepositoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Store $cachedDefaultStore
     *   Cached result of `getDefaultStore()`.
     */
    private ?Store $cachedDefaultStore = null;

    /**
     * @var StoreCore\Database\StoreMapper $dataMapper
     *   Data mapper for store data.
     */
    private StoreMapper $dataMapper;

    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->dataMapper = new StoreMapper($this->Registry);
    }

    /**
     * Counts the number of active stores.
     * 
     * @param void
     *
     * @return int
     *   Returns the total number of active or “open” stores as an integer.
     *   On errors `0` is returned, assuming that we currently cannot retrieve
     *   useful store information from the database anyway on database errors.
     */
    public function count(): int
    {
        try {
            $statement = $this->Database->query(
                "SELECT COUNT(*) FROM `sc_stores` WHERE `enabled_flag` = b'1' AND `date_deleted` IS NULL"
            );
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            unset($statement);
            if (is_int($result)) {
                return $result;
            }
        } catch (Exception $e) {
            $this->Logger->error('Exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return 0;
    }

    #[\Override]
    public function get(string $id): Store
    {
        return $this->dataMapper->has($id);
    }

    /**
     * Fetches the current store.
     * 
     * @param void
     * 
     * @return StoreCore\Store
     *   Returns the active or current store: the store for which requests
     *   or data are currently processed.  The current store is stored in the
     *   registry (and MAY be stored elsewhere or passed as a UUID).  If the
     *   store could not be found in the registry, the host name of the
     *   web server handling the current request is mapped to a store.
     * 
     * @uses getDefaultStore()
     *   If there is no `Store` in the registry and the host name could not
     *   be mapped to a store, the default store is returned.
     */
    public function getCurrentStore(): Store
    {
        // First return registered store.
        if ($this->Registry->has('Store')) {
            return $this->Registry->get('Store');
        }

        // Then map the host handling a request to a store.
        if ($this->Registry->has('URI')) {
            try {
                $query = <<<'SQL'
                       SELECT `s`.`store_id`
                         FROM `sc_store_hosts` AS `h`
                    LEFT JOIN `sc_stores` AS `s`
                           ON `h`.`store_id` = `s`.`store_id`
                        WHERE `h`.`host_name` = ?
                          AND `h`.`redirect_to` IS NULL
                SQL;
                $statement = $this->Database->prepare($query);
                $statement->bindValue(1, $this->Registry->get('URI')->getHost(), \PDO::PARAM_STR);
                if ($statement->execute() === true) {
                    $id = $statement->fetchColumn(0);
                    $statement->closeCursor();
                    if ($id !== false) {
                        $result = $this->dataMapper->get($id);
                        $this->Registry->set('Store', $result);
                        return $result;
                    }
                }
            } catch (Exception $e) {
                $this->Logger->error('Exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            }
        }

        // Finally return default store.
        return $this->getDefaultStore();
    }

    /**
     * Fetches the default store.
     *
     * @param void
     *
     * @return StoreCore\Store
     *   Returns the default store or main store: the store that is referenced
     *   when store information is needed but no existing store is explicitly
     *   mentioned.
     *
     * @throws Psr\Container\NotFoundException 
     *   If there is no default store, or an error occurs, a PSR-11 compliant
     *   “not found” exception is thrown.
     */
    public function getDefaultStore(): Store
    {
        if ($this->cachedDefaultStore !== null) {
            return $this->cachedDefaultStore;
        }

        if (defined('STORECORE_DEFAULT_STORE')) {
            try {
                return $this->dataMapper->get(STORECORE_DEFAULT_STORE);
            } catch (Exception $e) {
                $this->Logger->debug('Configuration mismatch ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            }
        }

        try {
            $query = "SELECT `store_id` FROM `sc_stores` WHERE `default_flag` = b'0' ORDER BY `date_created` LIMIT 1";
            $statement = $this->Database->query($query);
            $id = $statement->fetchColumn(0);
            if ($id === false) {
                throw new NotFoundException();
            }
            $this->cachedDefaultStore = $this->dataMapper->get($id);
            return $this->cachedDefaultStore;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    #[\Override]
    public function has(string $id): bool
    {
        return $this->dataMapper->has($id);
    }

    /**
     * Persists a store.
     *
     * @param StoreCore\Store &$store
     *   StoreCore `Store` model object.  The `Store` MUST have a `name` and
     *   this `name` MUST be unique.
     *
     * @return bool
     *   Returns the store’s UUID as a string on success.  The `Store` or its
     *   `Metadata` MAY be updated by the repository.
     *
     * @uses StoreCore\Admin\Configurator::write()
     *   If the `Store` being saved is the new default store, the global
     *   configuration is updated.
     */
    public function set(IdentityInterface &$store): string
    {
        if (!($store instanceof Store)) {
            throw new TypeError();
        }

        // Set missing OnlineStore.name to OnlineStore.organization.name
        if (empty($store->name)) {
            if (
                !empty($store->organization)
                && !empty($store->organization->name)
            ) {
                $store->name = $store->organization->name;
            } else {
                throw new ValueError();
            }
        }

        $names = $this->dataMapper->list();
        if (
            !empty($names)
            && in_array($store->name, $names)
        ) {
            throw new ValueError();
        }

        if ($store->metadata === null) {
            $store->metadata = new Metadata();
        }

        try {
            if (is_int($store->metadata->id)) {
                $result = $this->dataMapper->update($store);
            } else {
                $result = $this->dataMapper->create($store);
            }
            if ($result === false) {
                throw new RuntimeException();
            }
 
            // If this is the default store, all others stores are not.
            if ($store->isDefault()) {
                $statement = $this->Database->prepare(
                    "UPDATE `sc_stores` SET `default_flag` = b'0' WHERE `store_uuid` <> UNHEX(?)"
                );
                $statement->bindValue(1, $store->getIdentifier()->valueOf(), \PDO::PARAM_STR);
                if ($statement->execute() !== true) return false;

                if (
                    !defined('STORECORE_DEFAULT_STORE')
                    || STORECORE_DEFAULT_STORE !== (string) $store->getIdentifier()
                ) {
                    $config = new Configurator();
                    $config->set('STORECORE_DEFAULT_STORE', (string) $store->getIdentifier());
                    $config->save();
                }
            }

            return $store->getIdentifier()->__toString();

        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        } catch (Exception $e) {
            $this->Logger->error('Error ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Closes and deletes a store.
     *
     * @param string $id
     *   Store identifier.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    #[\Override]
    public function unset(string $id): bool
    {
        return is_int($this->dataMapper->delete($id)) ? true : false;
    }
}
