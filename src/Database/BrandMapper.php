<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Container\ContainerInterface;
use StoreCore\PIM\Brand;
use StoreCore\Types\UUID;

use function \array_filter;
use function \ctype_digit;
use function \is_array;
use function \is_int;
use function \isset;
use function \str_replace;
use function \strtolower;
use function \trim;
use function \unset;

/**
 * Brands.
 *
 * @package StoreCore\PIM
 * @version 1.0.0-alpha.1
 */
class BrandMapper extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    #[\Override]
    public function get(string $id): Brand
    {
        $id = trim($id);
        $uuid = str_replace('-', '', $id);
        $uuid = strtolower($uuid);

        if (!UUID::validate($uuid)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                throw new ContainerException();
            }
        }

        $query = 'SELECT HEX(`brand_uuid`) AS `brand_uuid`, `global_brand_name` FROM `sc_brands` WHERE ';
        if (is_int($id)) {
            $query .= '`brand_id` = ?';
        } else {
            $query .= '`brand_uuid` = UNHEX(?)';
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            if (is_int($id)) {
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(1, $uuid, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $row = $statement->fetch(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                unset($query, $statement);
                if (is_array($row)) {
                    return self::toObject($row);
                } else {
                    throw new NotFoundException();
                }
            }
        }
        throw new ContainerException();
    }

    #[\Override]
    public function has(string $id): bool
    {
        $id = trim($id);
        $uuid = str_replace('-', '', $id);
        $uuid = strtolower($uuid);

        if (!UUID::validate($uuid)) {
            if (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        if (is_int($id)) {
            $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_brands` WHERE `brand_id` = ?');
        } else {
            $statement = $this->Database->prepare('SELECT COUNT(*) FROM `sc_brands` WHERE `brand_uuid` = UNHEX(?)');
        }
        if ($statement !== false) {
            if (is_int($id)) {
                $statement->bindValue(1, $id, \PDO::PARAM_INT);
            } else {
                $statement->bindValue(1, $uuid, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $result = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($id, $statement, $uuid);
                if ($result === 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Converts a key-value array to a Brand object.
     *
     * @internal
     * @param array $data
     * @return StoreCore\PIM\Brand
     */
    public static function toObject(array $data): Brand
    {
        $result = new Brand();
        $data = array_filter($data);
        if (isset($data['brand_uuid'])) {
            $result->setIdentifier($data['brand_uuid']);
        }
        if (isset($data['global_brand_name'])) {
            $result->name = $data['global_brand_name'];
        }
        return $result;
    }
}
