<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2018, 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Currency;
use StoreCore\PIM\Product;
use StoreCore\Types\UUID;

/**
 * Product pricing model.
 *
 * @package StoreCore\Core
 * @version 0.2.1
 */
class ProductPrice extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * @var int $precision
     *   The number of digits after the decimal place in a price.
     *   Defaults to 2 for Eurocents.
     */
    private int $precision = 2;

    /**
     * @var float|null $price
     *   The offer price of a product or of a price component when attached
     *   to a price specification and its subtypes.
     */
    public ?float $price = null;

    /**
     * @var string $priceCurrency
     *   The currency of the price or a price component as an uppercase string in
     *   standard ISO 4217 currency format.  Defaults to 'EUR' for Euro.
     */
    public string $priceCurrency = 'EUR';

    /**
     * @var UUID|null $productUUID
     *   Universally unique identifier (UUID) of the product that this price
     *   applies to.
     */
    public ?UUID $productUUID = null;

    /**
     * @var DateTime $validFrom
     *   The date or date and time when the price becomes valid.
     */
    public DateTime $validFrom;

    /**
     * @var DateTime|null $validThrough
     *   Optional.  The date or date and time after when the price is no longer
     *   valid, for example the end of an offer or a period of opening hours.
     */
    public ?DateTime $validThrough = null;

    /**
     * @var bool $valueAddedTaxIncluded
     *   Specifies whether the applicable value-added tax (VAT) is included in
     *   the price specification or not.  Defaults to true for consumer prices.
     */
    public bool $valueAddedTaxIncluded = true;

    /**
     * Sets the default precision.
     *
     * @param int $precision
     *   Number of digits from 0 up to and including 6.
     *
     * @return void
     */
    public function setPrecision(int $precision): void
    {
        if ($precision < 0) {
            $precision = 0;
        } elseif ($precision > 6) {
            $precision = 6;
        }
        $this->precision = $precision;
    }

    /**
     * Sets the product.
     *
     * @param StoreCore\PIM\Product|StoreCore\Types\UUID $product
     *   Product with a UUID or UUID of the product this prices applies to.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception on a product without a UUID.  This method does
     *   not check if the UUID is valid nor if the product UUID actually exists.
     */
    public function setProduct(Product|UUID $product): void
    {
        if ($product instanceof Product) {
            if ($product->hasIdentifier()) {
                $product = $product->getIdentifier();
            } else {
                throw new \ValueError('Product has no UUID');
            }
        }
        $this->productUUID = $product;
    }
}
