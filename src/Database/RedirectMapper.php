<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \DateTime;
use \DateTimeImmutable;
use \PDOException as DatabaseException;
use StoreCore\Engine\Redirect;

use function \array_filter;
use function \isset;
use function \key;

/**
 * Cache pool for redirects.
 *
 * @internal
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class RedirectMapper extends AbstractDataAccessObject
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const PRIMARY_KEY = 'redirect_id';
    public const TABLE_NAME  = 'sc_redirects';

    /**
     * Creates or updates a redirect.
     *
     * @param StoreCore\Engine\Redirect $redirect
     * @return bool
     */
    public function save(Redirect $redirect): bool
    {
        try {
            /*
                INSERT INTO `sc_redirects`
                    (`redirect_id`, `date_expires`, `redirect_from_url`, `redirect_to_url`, `date_created`)
                  VALUES
                    (:redirect_id, :date_expires, :redirect_from_url, :redirect_to_url, UTC_TIMESTAMP())
                  ON DUPLICATE KEY UPDATE
                    `date_expires` = :updated_date_expires,
                    `redirect_from_url` = :updated_redirect_from_url,
                    `redirect_to_url` = :updated_redirect_to_url,
                    `date_touched` = :date_touched
             */
            $sth = $this->Database->prepare('INSERT INTO `sc_redirects` (`redirect_id`, `date_expires`, `redirect_from_url`, `redirect_to_url`, `date_created`) VALUES (:redirect_id, :date_expires, :redirect_from_url, :redirect_to_url, UTC_TIMESTAMP()) ON DUPLICATE KEY UPDATE `date_expires` = :updated_date_expires, `redirect_from_url` = :updated_redirect_from_url, `redirect_to_url` = :updated_redirect_to_url, `date_touched` = :date_touched');
            if ($sth === false) return false;

            $redirect = $this->toArray($redirect);
            $sth->bindValue(':redirect_id', $redirect['redirect_id'], \PDO::PARAM_STR);
            $sth->bindValue(':date_expires', $redirect['date_expires'], \PDO::PARAM_STR);
            $sth->bindValue(':redirect_from_url', $redirect['redirect_from_url'], \PDO::PARAM_STR);
            $sth->bindValue(':redirect_to_url', $redirect['redirect_to_url'], \PDO::PARAM_STR);
            $sth->bindValue(':updated_date_expires', $redirect['date_expires'], \PDO::PARAM_STR);
            $sth->bindValue(':updated_redirect_from_url', $redirect['redirect_from_url'], \PDO::PARAM_STR);
            $sth->bindValue(':updated_redirect_to_url', $redirect['redirect_to_url'], \PDO::PARAM_STR);
            $sth->bindValue(':date_touched', $redirect['date_touched'], \PDO::PARAM_STR);
            $result = $sth->execute();
            $sth->closeCursor();
            return $result;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Converts a value object to a key-value array.
     *
     * @param StoreCore\Engine\Redirect $redirect
     * @return array Key-value structure of row in the `sc_redirects` database table.
     */
    final public function toArray(Redirect $redirect): array
    {
        return [
            'redirect_id'       => RedirectCache::createKey($redirect),
            'date_expires'      => $redirect->expiresAt === null ? null : $redirect->expiresAt->format('Y-m-d H:i:s'),
            'redirect_from_url' => key($redirect->get()),
            'redirect_to_url'   => $redirect->getLocation(),
            'date_created'      => $redirect->dateCreated === null ? gmdate('Y-m-d H:i:s') : $redirect->dateCreated->format('Y-m-d H:i:s'),
            'date_touched'      => $redirect->dateTouched === null ? null : $redirect->dateCreated->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * Converts a key-value array to a value object.
     *
     * @param array $array
     * @return StoreCore\Engine\Redirect
     */
    final public function toObject(array $array): Redirect
    {
        $array = array_filter($array);
        $redirect = new Redirect($array['redirect_from_url'], $array['redirect_to_url']);

        if (isset($array['date_created'])) {
            $redirect->dateCreated = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $array['date_created']);
        }

        if (isset($array['date_touched'])) {
            $redirect->dateTouched = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $array['date_touched']);
        }

        if (isset($array['date_expires'])) {
            $redirect->expiresAt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $array['date_expires']);
        }

        return $redirect;
    }
}
