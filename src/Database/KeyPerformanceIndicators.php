<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2018, 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\Registry;
use StoreCore\PIM\ProductRepository;

/**
 * Key Performance Indicators (KPIs) model.
 *
 * @package StoreCore\BI
 * @version 0.2.1
 *
 * @see https://support.google.com/analytics/answer/2709828?hl=en
 *      Custom dimensions & metrics - Google Analytcs Help
 *
 * @see https://developers.google.com/analytics/devguides/collection/gtagjs/custom-dims-mets
 *      Custom dimensions and metrics with gtag.js - Set up Google Analytics
 */
class KeyPerformanceIndicators extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * Gets all custom string dimensions.
     *
     * |  1 | StoreCore client UUID    |
     * |  2 | StoreCore version        |
     * |  3 | host name and IP address |
     * |  4 | operating system         |
     * |  5 | web server               |
     * |  6 | application server       |
     * |  7 | database server          |
     * |  8 |                          |
     * |  9 |                          |
     * | 10 |                          |
     * 
     * @param void
     * @return array
     */
    public function getDimensions(): array
    {
        // Dimensions must be indexed starting with 1.
        $dimensions = [];

        if (\defined('STORECORE_GUID')) {
            $dimensions[1] = STORECORE_GUID;
        } else {
            $dimensions[1] = 'StoreCore client not installed';
        }

        if (defined('STORECORE_VERSION')) {
            $dimensions[2] = 'StoreCore/' . STORECORE_VERSION;
        }

        $hostname = gethostname();
        if ($hostname !== false) {
            $dimensions[3] = $hostname;
            if (!empty($_SERVER['SERVER_ADDR'])) {
                $dimensions[3] .= ' ' . $_SERVER['SERVER_ADDR'];
            }
        }

        if (function_exists('php_uname')) {
            // Operating system (s), release name (r), version information (v), and machine type (m)
            $os = php_uname('s') . ' ' . php_uname('r') . ' ' . php_uname('v') . ' ' . php_uname('m');
            $os = trim($os);
            $os = preg_replace('/\s+/', ' ', $os);

            // Maximum length of a custom dimension and custom metric is 150 bytes.
            // @see https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference#customs
            if (strlen($os) > 150) {
                // Omit version information (v) and machine type (m).
                $os = php_uname('s') . ' ' . php_uname('r');
                $os = trim($os);
                $os = preg_replace('/\s+/', ' ', $os);
                if (strlen($os) > 150) {
                    $os = substr($os, 0, 150);
                }
            }

            $dimensions[4] = $os;
            unset($os);
        }

        if (!empty($_SERVER['SERVER_SOFTWARE'])) {
            $dimensions[5] = $_SERVER['SERVER_SOFTWARE'];
        }

        $dimensions[6] = 'PHP/' . PHP_VERSION . ' ' . PHP_SAPI;

        if (defined('STORECORE_DATABASE_DRIVER')) {
            if (STORECORE_DATABASE_DRIVER === 'mysql') {
                $dimensions[7] = 'MySQL/' . $this->Database->query('SELECT VERSION()')->fetchColumn();
            }
        }

        return $dimensions;
    }

    /**
     * Gets all custom metrics.
     *
     * |  1 | number of active (open) stores     |
     * |  2 | number of active users (headcount) |
     * |  3 | number of active products          |
     * |  4 |                                    |
     * |  5 |                                    |
     * |  6 |                                    |
     * |  7 |                                    |
     * |  8 |                                    |
     * |  9 |                                    |
     * | 10 |                                    |
     * 
     * @param void
     * @return array
     */
    public function getMetrics(): array
    {
        // Metrics must be indexed starting with 1.
        $metrics = [];

        $model = new StoreRepository($this->Registry);
        $metrics[1] = $model->count();

        $model = new UserRepository($this->Registry);
        $metrics[2] = $model->count();

        $model = new ProductRepository($this->Registry);
        $metrics[3] = $model->count();

        return $metrics;
    }
}
