<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \PDOException as DatabaseException;
use \SplFixedArray;
use StoreCore\Registry;
use StoreCore\CRM\Organization;
use StoreCore\CRM\Person;

/**
 * Creative work role mapper.
 *
 * This data mapper maps roles in creative works to people and organizations.
 * 
 * | Person              | CreativeWork.accountablePerson  |
 * | Person|Organization | CreativeWork.author             |
 * | Person              | CreativeWork.character          |
 * | Person|Organization | CreativeWork.contributor        |
 * | Person|Organization | CreativeWork.copyrightHolder    |
 * | Person|Organization | CreativeWork.creator            |
 * | Person              | CreativeWork.editor             |
 * | Person|Organization | CreativeWork.funder             |
 * | Person|Organization | CreativeWork.maintainer         |
 * | Person|Organization | CreativeWork.producer           |
 * | Person|Organization | CreativeWork.provider           |
 * | Person|Organization | CreativeWork.publisher          |
 * | Organization        | CreativeWork.publisherImprint   |
 * | Person|Organization | CreativeWork.sdPublisher        |
 * | Organization        | CreativeWork.sourceOrganization |
 * | Person|Organization | CreativeWork.sponsor            |
 * | Person|Organization | CreativeWork.translator         |
 *
 * @internal
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class CreativeWorkRoleMapper extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var \SplFixedArray $roles
     *   Key-value array with all types of roles.
     */
    private SplFixedArray $roles;

    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $model = new CreativeWorkRoles($this->Registry);
        $this->roles = $model->list();
    }
}
