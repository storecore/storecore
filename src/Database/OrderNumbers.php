<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use Psr\Clock\ClockInterface;
use StoreCore\ProxyInterface;

use StoreCore\ClockTrait;

use StoreCore\OML\OrderProxy;
use StoreCore\Types\UUID;

/**
 * Order numbers.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class OrderNumbers extends AbstractModel implements ClockInterface
{
    use ClockTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Gets an order proxy by order or confirmation number.
     *
     * @param string $number
     *   Order number or order confirmation number.
     *
     * @return StoreCore\ProxyInterface
     *   Returns an `OrderProxy` that implements the `ProxyInterface`.
     */
    public function get(string $number): ProxyInterface
    {
        $number = trim($number);
        if (!$this->has($number)) {
            throw new NotFoundException();
        }

        try {
            $statement = $this->Database->prepare(
                'SELECT HEX(`order_uuid`) AS `order_uuid`, `order_number`, `confirmation_number` FROM `sc_orders` WHERE `confirmation_number` = ? OR `order_number` = ? ORDER BY `date_confirmed` DESC LIMIT 1'
            );
            $statement->bindValue(1, $number);
            $statement->bindValue(2, $number);
            $statement->execute();
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($statement);

            $result['order_uuid'] = new UUID($result['order_uuid']);
            if (empty($result['order_number'])) $result['order_number'] = null;
            if (empty($result['confirmation_number'])) $result['confirmation_number'] = null;
            return new OrderProxy(
                $result['order_uuid'],
                'Order',
                $result['order_number'],
                $result['confirmation_number'],
            );
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new ContainerException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * Creates a pseudo-random order confirmation number.
     *
     * The number contains date and time components plus a random number.
     * The number format is outlined in the table below.
     *
     * | Position            | Component                                 |
     * | :------------------ | :---------------------------------------- | 
     * | #··-·······-······· | Last digit of the year                    |
     * | ·##-·······-······· | ISO 8601 week number of the year          |
     * | ···-###····-······· | Day of the year with leading zeros        |
     * | ···-···##··-······· | Hour in 24-hour format with leading zeros |
     * | ···-·····##-······· | Minutes with leading zeros                |
     * | ···-·······-####### | Random integer with leading zeros         |
     *
     * @param void
     * @return string
     */
    public function getConfirmationNumber(): string
    {
        $now = $this->now();
        $datetime = substr($now->format('y'), -1);
        $datetime .= sprintf('%02d', $now->format('W'));
        $datetime .= '-';
        $datetime .= sprintf('%03d', $now->format('z'));
        $datetime .= $now->format('Hi');
        $datetime .= '-';

        do {
            $result = $datetime . sprintf('%07d', random_int(0, 9999999));
        } while ($this->has($result));
        return $result;
    }

    /**
     * Creates a random order number.
     *
     * @param void
     *
     * @return string
     *   Random interger as a numeric string between 5000 and the
     *   signed `INT` maximum 2147483647 of MySQL and MariaDB.
     */
    public function getRandomOrderNumber(): string
    {
        do {
            $result = (string) random_int(5000, 2147483647);
        } while ($this->has($result));
        return $result;
    }


    /**
     * Checks whether an order number or confirmation number exists.
     *
     * @param string $number
     *   Order number or confirmation number.
     *
     * @return bool
     *   Returns `true` if the number exists, otherwise `false`.
     */
    public function has(string $number): bool
    {
        $number = trim($number);
        if (!empty($number)) {
            try {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_orders` FORCE INDEX (`ix_confirmed_orders`) WHERE `confirmation_number` = ? OR `order_number` = ? ORDER BY `date_confirmed` DESC'
                );
                if ($statement !== false) {
                    $statement->bindValue(1, $number, \PDO::PARAM_STR);
                    $statement->bindValue(2, $number, \PDO::PARAM_STR);
                    if ($statement->execute()) {
                        $result = $statement->fetchColumn(0);
                        $statement->closeCursor();
                        unset($statement);
                        if ($result >= 1) {
                            return true;
                        }
                    }
                }
            } catch (\PDOException $e) {
                $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
                throw new ContainerException($e->getMessage(), $e->getCode(), $e);
            }
        }
        return false;
    }
}
