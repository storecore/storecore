<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2014–2017, 2023, 2024 StoreCore
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use function \random_int;
use function \str_shuffle;
use function \strlen;

/**
 * Salt value object.
 *
 * @package StoreCore\Security
 * @version 0.2.1
 */
readonly class Salt implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * @var string CHARACTER_SET
     *   Character set for the salt, currently set to ASCII digits (0-1),
     *   uppercase letters (A-Z), and lowercase letters (a-z).  Note that
     *   adding other characters may break code that uses the default format
     *   "./0-9A-Za-z" for Standard DES (Data Encryption Standard) or
     *   Blowfish.
     */
    public const string CHARACTER_SET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    /**
     * @var string $value
     *   Salt string value.
     */
    public string $value;

    /**
     * Creates a random salt.
     *
     * @param int $length
     *   Optional length of the salt string.  Defaults to 255 characters for
     *   a CHAR(255) column in a MySQL database table.  The salt length is
     *   reset to a minimum of 2 characters for very short salts used by
     *   Standard DES (Data Encryption Standard).
     */
    public function __construct(int $length = 255)
    {
        if ($length < 2) $length = 2;
        $charset = str_shuffle(self::CHARACTER_SET);
        $charset_size = strlen($charset) - 1;
        $salt = (string) null;
        for ($i = 0; $i < $length; $i++) {
            $salt .= $charset[random_int(0, $charset_size)];
        }
        $this->value = $salt;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
