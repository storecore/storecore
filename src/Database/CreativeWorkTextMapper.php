<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \PDOException as DatabaseException;
use StoreCore\CMS\AbstractCreativeWork;
use StoreCore\Types\LanguageCode;

assert(function_exists('mb_stripos'));
assert(function_exists('mb_strlen'));

use function \array_diff;
use function \array_unique;
use function \empty;
use function \explode;
use function \implode;
use function \is_array;
use function \is_int;
use function \is_string;
use function \mb_stripos;
use function \mb_strlen;
use function \preg_replace;
use function \str_contains;
use function \trim;

/**
 * Creative work text mapper.
 *
 * @internal
 * @package StoreCore\CMS
 * @see     https://mariadb.com/kb/en/full-text-index-stopwords/ Full-Text Index Stopwords
 * @version 1.0.0-alpha.1
 */
class CreativeWorkTextMapper extends AbstractDataAccessObject implements CRUDInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string PRIMARY_KEY DAO database table primary key.
     * @var string TABLE_NAME  DAO database table name.
     */
    public const PRIMARY_KEY = 'creative_work_id';
    public const TABLE_NAME  = 'sc_creative_work_texts';

    /**
     * @param array $stopwords
     *   List of commonly-used words that can be ignored in full-text searches.
     *   Defaults to the MariaDB stopwords list for InnoDB.
     */
    public array $stopwords = ['a', 'about', 'an', 'are', 'as', 'at', 'be',
        'by', 'com', 'de', 'en', 'for', 'from', 'how', 'i', 'in', 'is', 'it',
        'la', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what',
        'when', 'where', 'who', 'will', 'with', 'und', 'the', 'www'
    ];

    /**
     * Filters keywords.
     *
     * @param array|string $keywords
     * @return string
     */
    public static function filterKeywords(array|string $keywords): string
    {
        if (is_string($keywords)) {
            $keywords = preg_replace('/\s+/', ' ', $keywords);
            $keywords = trim($keywords);
            $keywords = str_ireplace(';', ',', $keywords);
            $keywords = str_ireplace('.', ',', $keywords);
            $keywords = str_ireplace(', ', ',', $keywords);
            $keywords = explode(',', $keywords);
        }

        $keywords = array_unique($keywords);
        $keywords = array_filter($keywords);
        $keywords = implode(', ', $keywords);

        $keywords = str_ireplace("'s", '’s', $keywords);
        $keywords = str_ireplace(" '", '‘', $keywords);
        $keywords = str_ireplace("'", '’', $keywords);

        $keywords = str_ireplace(' "', ' “', $keywords);
        $keywords = str_ireplace('"', '”', $keywords);

        // Drop keywords that are too short or too long for a full-text index.
        $keywords = explode(', ', $keywords);
        foreach ($keywords as $key => $word) {
            $length = mb_strlen($word);
            if ($length < 3 || $length > 84) {
                unset($keywords[$key]);
            }
        }

        // Drop keywords that are included in other, longer keywords.
        $words = [];
        $haystack = [];
        foreach ($keywords as $key => $word) {
            if (!str_contains($word, ' ')) {
                $words[$key] = $word;
            } else {
                $haystack[$key] = $word;
            }
        }

        $keywords = implode(', ', $haystack);
        if (!empty($words)) {
            foreach ($words as $key => $word) {
                if (mb_stripos($keywords, $word) !== false) {
                    unset($words[$key]);
                }
            }

            if (!empty($words)) {
                $keywords = $words + $haystack;
                $keywords = implode(', ', $keywords);
            }
        }

        if (mb_strlen($keywords) <= 767) {
            return $keywords;
        }

        $keywords = explode(', ', $keywords);
        $result = $keywords[0];
        unset($keywords[0]);
        foreach ($keywords as $key => $word) {
            if (mb_strlen($result) + 2 + mb_strlen($word) <= 767) {
                $result .= ', ' . $word;
            } else {
                return $result;
            }
        }
    }

    /**
     * Searches for creative works.
     *
     * @param string $needle
     *   The string to search for.
     * 
     * @param StoreCore\Types\LanguageCode|false $language
     *   Language code of the language to search in.  MAY be set to `false`
     *   to search in any language.
     *
     * @return array
     *   Returns a key-value array with a UUID as the key and the name of
     *   a creative works as the value.  The array MAY be empty if no results
     *   were found.
     */
    public function list(string $needle, LanguageCode|false $language): array
    {
        $needle = trim($needle);
        $needle = preg_replace('/\s+/', ' ', $needle);
        if (mb_strlen($needle) < 3) {
            return [];
        }

        $needle = explode(' ', $needle);
        $needle = array_diff($needle, $stopwords);
        $needle = implode(' ', $needle);
        if (mb_strlen($needle) < 3) {
            return [];
        }

        /*
                SELECT `w`.`creative_work_uuid`, `t`.`name` 
                  FROM `sc_creative_work_texts` AS `t`
            CROSS JOIN `sc_creative_works` AS `w`
                    ON `t`.`creative_work_id` = `w`.`creative_work_id`
                 WHERE `t`.`in_language` = '…'
                   AND MATCH (`t`.`name`, `t`.`alternate_name`, `t`.`headline`, `t`.`keywords`, `t`.`description`, `t`.`disambiguating_description`, `t`.`text_plain`)
                       AGAINST ('…')
         */
        $sql = 'SELECT `w`.`creative_work_uuid`, `t`.`name` FROM `sc_creative_work_texts` AS `t` CROSS JOIN `sc_creative_works` AS `w` ON `t`.`creative_work_id` = `w`.`creative_work_id` WHERE ';
        if ($language !== false) {
            $sql .= "`t`.`in_language` = '" . $language . "' AND ";
        }
        $sql .= 'MATCH (`t`.`name`, `t`.`alternate_name`, `t`.`headline`, `t`.`keywords`, `t`.`description`, `t`.`disambiguating_description`, `t`.`text_plain`) AGAINST (:needle)';

        try {
            $sth = $this->Database->prepare($sql);
            if ($sth !== false) {
                $sth->bindValue(':needle', $needle, \PDO::PARAM_STR);
                if ($sth->execute() !== false) {
                    $result = $sth->fetchAll(\PDO::FETCH_KEY_PAIR);
                    $sth->closeCursor();
                    unset($sth, $sql);
                    if (is_array($result)) {
                        return $result;
                    }
                }
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return [];
    }

    /**
     * Saves the text content of a creative work.
     *
     * @param StoreCore\CMS\AbstractCreativeWork $object
     * @return bool
     */
    public function set(AbstractCreativeWork $object): bool
    {
        $data = $this->toArray($object);
        if (empty($data['creative_work_id'])) {
            return false;
        }
        $this->delete($data['creative_work_id']);
        return $this->create($data) === false ? false : true;
    }

    /**
     * Converts a creative work object to a key-value array.
     * 
     * Saves the following Schema.org properties:
     * 
     * - `CreativeWork.inLanguage`
     * - `Thing.name`
     * - `Thing.alternateName`
     * - `CreativeWork.headline`
     * - `CreativeWork.keywords`
     * - `Thing.description`
     * - `Thing.disambiguatingDescription`
     * - `CreativeWork.text` as plain text, HTML, and/or Markdown.
     *
     * @param StoreCore\CMS\AbstractCreativeWork $object
     * @return array
     */
    final public function toArray(AbstractCreativeWork $object): array
    {
        $result = [
            'creative_work_id' => is_int($object->id) ? $object->id : null,
            'in_language' => empty($object->inLanguage) ? 'en-GB' : (string) $object->inLanguage,
            'name' => empty($object->name) ? '' : (string) $object->name,
            'alternate_name' => empty($object->alternateName) ? '' : $object->alternateName,
            'headline' => empty($object->headline) ? '' : $object->headline,
            'keywords' => empty($object->keywords) ? '' : self::filterKeywords($object->keywords),
            'description' => empty($object->description) ? '' : $object->description,
            'disambiguating_description' => empty($object->disambiguatingDescription) ? '' : $object->disambiguatingDescription,
            'text_plain' => empty($object->text) ? '' : $object->text,
            'text_html' => empty($object->html) ? '' : $object->html,
            'text_markdown' => empty($object->markdown) ? '' : $object->markdown,
        ];

        return $result;
    }
}
