<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \Stringable;
use Psr\Container\ContainerInterface;
use StoreCore\Store;
use StoreCore\Admin\Configurator;
use StoreCore\Types\{UUID, UUIDFactory};

use function \ctype_digit;
use function \in_array;
use function \is_array;
use function \is_int;
use function \is_string;
use function \isset;

/**
 * Store mapper.
 *
 * @package StoreCore\Core
 * @version 0.3.0
 */
class StoreMapper extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';

    /**
     * @var array $keyMap
     *   Key-value array that maps public string UUIDs to integer IDs.
     */
    private array $keyMap = [];

    /**
     * Saves a store.
     *
     * @param StoreCore\Store &$store
     *   New store to add to the database.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function create(Store &$store): bool
    {
        if (!$store->hasIdentifier()) {
            $factory = new UUIDFactory($this->Registry);
            $store->setIdentifier($factory->createUUID());
            unset($factory);
        }

        $data = $this->toArray($store);
        $query = <<<'SQL'
            INSERT INTO `sc_stores`
                (`store_uuid`, `default_flag`, `enabled_flag`, `organization_id`, `store_name`, `date_created`, `date_time_zone`)
              VALUES
                (UNHEX(:store_uuid), :default_flag, :enabled_flag, :organization_id, :store_name, :date_created, :date_time_zone)
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':store_uuid', $data['store_uuid'], \PDO::PARAM_STR);
            $statement->bindValue(':default_flag', $data['default_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':enabled_flag', $data['enabled_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':organization_id', $data['organization_id'], \PDO::PARAM_INT);
            $statement->bindValue(':store_name', $data['store_name'], \PDO::PARAM_STR);
            $statement->bindValue(':date_created', $data['date_created'], \PDO::PARAM_STR);
            $statement->bindValue(':date_time_zone', $data['date_time_zone'], \PDO::PARAM_STR);
            $result = $statement->execute();
            $statement->closeCursor();
            unset($data, $query, $statement);

            if ($result !== false) {
                $id = $this->Database->lastInsertId();
                if ($id !== false && ctype_digit($id)) {
                    $id = (int) $id;

                    if ($store->isDefault()) {
                        $this->Database->exec(
                            "UPDATE `sc_stores` SET `default_flag` = b'0' WHERE `store_id` <> "
                            . $id . " AND `default_flag` <> b'0'"
                        );
                    }

                    $store->metadata->id = $id;
                    $store->metadata->hash = Metadata::hash($store);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes a store.
     *
     * @param \Stringable|string|int $id
     *   Unique identifier of the store.
     *
     * @return int|false
     *   Returns the total number of database rows that were deleted
     *   on success or `false` on failure.
     */
    public function delete(Stringable|string|int $id): int|false
    {
        $id = self::sanitizeKey($id);
        if ($id === false) {
            return false;
        }

        if (is_string($id)) {
            $id = str_replace('-', '', $id);
            $query = 'SELECT `store_id` FROM `sc_stores` WHERE `store_uuid` = UNHEX(?) LIMIT 1';
            $statement = $this->Database->prepare($query);
            if (
                $statement !== false
                && $statement->bindValue(1, $id, \PDO::PARAM_STR)
                && $statement->execute()
            ) {
                $id = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($query, $statement);
                if (!is_int($id)) {
                    return false;
                }
            }
        }

        $query = <<<'SQL'
            UPDATE `sc_stores`
               SET `default_flag` = b'0',
                   `enabled_flag` = b'0',
                   `date_deleted` = ?
             WHERE `store_id` = ?
             LIMIT 1
        SQL;
        $statement = $this->Database->prepare($query);
        $now = gmdate('Y-m-d H:i:s');
        if (
            $statement === false
            || $statement->bindValue(1, $now, \PDO::PARAM_STR) !== true
            || $statement->bindValue(2, $id, \PDO::PARAM_INT) !== true
            || $statement->execute() !== true
        ) {
            return false;
        }

        // For legal reasons, it MAY be necessary to retain prices for online offers.
        $query = <<<'SQL'
            UPDATE `sc_product_prices`
               SET `thru_date` = ?
             WHERE `store_id` = ?
               AND `thru_date` IS NULL
        SQL;
        $statement = $this->Database->prepare($query);
        if (
            $statement === false
            || $statement->bindValue(1, $now, \PDO::PARAM_STR) !== true
            || $statement->bindValue(2, $id, \PDO::PARAM_INT) !== true
            || $statement->execute() !== true
        ) {
            return false;
        }

        $result = 0;
        $tables = [
            'sc_product_media',
            'sc_product_bundle_stores',
            'sc_store_product_texts',
            'sc_store_categories',
            'sc_customers',
            'sc_store_languages',
            'sc_store_hosts',
            'sc_store_currencies',
        ];
        foreach ($tables as $table) {
            $count = $this->Database->exec(
                'DELETE FROM `' . $table . '` WHERE `store_id` = ' . $id
            );
            if (is_int($count)) {
                $result = $result + $count;
            }
        }
        return $result;
    }

    /**
     * Gets a store from the database.
     *
     * @param \Stringable|string|int $id
     * @return StoreCore\Store
     */
    public function get(Stringable|string|int $id): Store
    {
        $id = self::sanitizeKey($id);
        if ($id === false) {
            throw new ContainerException();
        }

        if (!$this->has($id)) {
            throw new NotFoundException();
        }

        if (is_string($id)) {
            $id = $this->keyMap[$id];
        }

        $query = <<<'SQL'
            SELECT *, HEX(`store_uuid`) AS `uuid`
              FROM `sc_stores`
             WHERE BINARY `store_id` = ?
             LIMIT 1
        SQL;
        $statement = $this->Database->prepare($query);
        if (
            $statement !== false
            && $statement->bindValue(1, $id, \PDO::PARAM_INT)
            && $statement->execute()
        ) {
            $row = $statement->fetch(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            unset($query, $statement);
            if (is_array($row)) {
                $row['store_uuid'] = $row['uuid'];
                unset($row['uuid']);
                return self::toObject($row);
            } else {
                throw new NotFoundException();
            }
        }
        throw new ContainerException();
    }

    /**
     * Checks whether a store exists in the database.
     *
     * @param \Stringable|string|int $id
     * @return bool
     */
    public function has(Stringable|string|int $id): bool
    {
        $id = self::sanitizeKey($id);
        if ($id === false) {
            return false;
        }

        if (
            isset($this->keyMap[$id])
            || (is_int($id) && in_array($id, $this->keyMap))
        ) {
            return true;
        }

        $query = <<<'SQL'
            SELECT LOWER(CONCAT(
                     SUBSTR(HEX(`store_uuid`), 1, 8), '-',
                     SUBSTR(HEX(`store_uuid`), 9, 4), '-',
                     SUBSTR(HEX(`store_uuid`), 13, 4), '-',
                     SUBSTR(HEX(`store_uuid`), 17, 4), '-',
                     SUBSTR(HEX(`store_uuid`), 21)
                   )) AS `store_uuid`,
                   `store_id`
              FROM `sc_stores`
        SQL;
        if (is_int($id)) {
            $query .= ' WHERE `store_id` = ?';
        } else {
            $query .= ' WHERE `store_uuid` = UNHEX(?)';
        }
        $query .= ' LIMIT 1';

        $statement = $this->Database->prepare($query);
        if (
            $statement !== false
            && $statement->bindValue(1, $id, \PDO::PARAM_INT)
            && $statement->execute()
        ) {
            $key_pair = $statement->fetch(\PDO::FETCH_KEY_PAIR);
            $statement->closeCursor();
            unset($query, $statement);
            if (is_array($key_pair)) {
                $this->keyMap += $key_pair;
                return true;
            }
        }
        return false;
    }

    /**
     * Lists all stores names.
     *
     * @param void
     * 
     * @return array|false
     *   Returns a key-value array with store UUIDs as the key and store names
     *   as the value, or `false` on errors.  The result is sorted
     *   alphabetically by store name.  In the store management context
     *   store names MUST always be unique so backoffice users can distinguish
     *   stores by name.
     */
    public function list(): array|false
    {
        $query = <<<'SQL'
              SELECT LOWER(CONCAT(
                       SUBSTR(HEX(`store_uuid`), 1, 8), '-',
                       SUBSTR(HEX(`store_uuid`), 9, 4), '-',
                       SUBSTR(HEX(`store_uuid`), 13, 4), '-',
                       SUBSTR(HEX(`store_uuid`), 17, 4), '-',
                       SUBSTR(HEX(`store_uuid`), 21)
                     )) AS `uuid`,
                     `store_name`
                FROM `sc_stores`
            ORDER BY `store_name` ASC
        SQL;
        $statement = $this->Database->query($query);
        if ($statement !== false) {
            $result = $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
            $statement->closeCursor();
            if (is_array($result)) {
                return $result;
            }
        }
        return false;
    }

    /**
     * Sanitizes a public or private key value.
     *
     * @internal
     *
     * @param \Stringable|string|int $id
     *   Unique identifier to sanitize.
     *
     * @return string|int|false
     *   Returns a valid, sanitized key as a hexadecimal string or
     *   a small integer on success, or `false` on failure.
     */
    private static function sanitizeKey(Stringable|string|int $id): string|int|false
    {
        if ($id instanceof UUID) {
            return $id->valueOf();
        }

        if ($id instanceof Stringable) {
            $id = (string) $id;
        }

        if (is_string($id)) {
            if (UUID::validate($id)) {
                $id = new UUID($id);
                return $id->valueOf();
            } elseif (ctype_digit($id)) {
                $id = (int) $id;
            } else {
                return false;
            }
        }

        // Database primary key as a `SMALLINT UNSIGNED` with an `AUTO_INCREMENT`.
        if (is_int($id) && $id > 0 && $id <= 65535) {
            return $id;
        }
        return false;
    }

    /**
     * Maps a store model object to a key-value array.
     *
     * @internal
     * @param StoreCore\Store $store Store model object.
     * @return array Associative key-value array.
     */
    final public function toArray(Store $store): array
    {
        // Structure of a database record.
        $result = [
            'store_id' => null,
            'store_uuid' => null,
            'default_flag' => $store->isDefault() ? 1 : 0,
            'enabled_flag' => $store->isOpen() ? 1 : 0,
            'organization_id' => null,
            'store_name' => $store->name,
            'version' => 1,
            'date_created' => null,
            'date_modified' => null,
            'date_deleted' => null,
            'date_time_zone' => $store->dateTimeZone->getName(),
        ];

        if ($store->metadata !== null) {
            if (is_int($store->metadata->id)) {
                $result['store_id'] = $store->metadata->id;
            }
            $result['version'] = $store->metadata->version;
            $result['date_created'] = $store->metadata->dateCreated->format('Y-m-d H:i:s');
            if (!empty($store->metadata->dateModified)) {
                $result['date_modified'] = $store->metadata->dateModified->format('Y-m-d H:i:s');
            }
        }

        if ($store->hasIdentifier()) {
            $result['store_uuid'] = $store->getIdentifier()->__toString();
            $result['store_uuid'] = str_replace('-', '', $result['store_uuid']);
        }

        if (!empty($store->organization)) {
            $result['organization_id'] = $store->organization->metadata->id;
        }

        return $result;
    }


    /**
     * Maps a store’s data to a store model object.
     *
     * @internal
     * @param array $keyed_data Associative key-value array.
     * @return StoreCore\Store Store model object.
     */
    final public function toObject(array $keyed_data): Store
    {
        // Metadata
        $metadata = new Metadata($keyed_data['date_created'], $keyed_data['date_modified']);
        unset($keyed_data['date_created'], $keyed_data['date_modified']);

        if (isset($keyed_data['store_id'])) {
            $metadata->id = $keyed_data['store_id'];
            unset($keyed_data['store_id']);
        }

        if (isset($keyed_data['version'])) {
            $metadata->version = $keyed_data['version'];
            unset($keyed_data['version']);
        }

        $store = new Store($this->Registry);
        $store->metadata = $metadata;

        // Set the store UUID and store name.
        $store->setIdentifier($keyed_data['store_uuid']);
        $store->name = $keyed_data['store_name'];

        // Set the store's default timezone if it is not 'UTC'.
        if ($keyed_data['date_time_zone'] !== 'UTC') {
            $store->setDateTimeZone($keyed_data['date_time_zone']);
        }

        // Set as default store.
        if ($keyed_data['default_flag'] === 1) {
            $store->isDefault(true);
        }

        // Open the store.
        if ($keyed_data['enabled_flag'] === 1) {
            $store->open();
        }

        // Add store currencies.
        $model = new Currencies($this->Registry);
        $store->setCurrencies($model->getStoreCurrencies($store->metadata->id));

        // Add store languages.
        $model = new Languages($this->Registry);
        $store->setLanguages($model->getStoreLanguages($store->getIdentifier()));

        // Add store organization.
        if ($keyed_data['organization_id'] !== null) {
            $model = new OrganizationRepository($this->Registry);
            $store->organization = $model->get($keyed_data['organization_id']);
        }

        return $store;
    }


    /**
     * Updates a saved store.
     *
     * @param StoreCore\Store $store
     *   Existing store to update in the database.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function update(Store &$store): bool
    {
        if (!is_int($store->metadata->id)) {
            return false;
        }

        $data = $this->toArray($store);
        $query = <<<'SQL'
            UPDATE `sc_stores`
               SET `default_flag` = :default_flag,
                   `enabled_flag` = :enabled_flag,
                   `organization_id` = :organization_id,
                   `store_name` = :store_name,
                   `version` = :version,
                   `date_time_zone` = :date_time_zone
             WHERE `store_id` = :store_id
             LIMIT 1
        SQL;
        $statement = $this->Database->prepare($query);
        if ($statement !== false) {
            $statement->bindValue(':default_flag', $data['default_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':enabled_flag', $data['enabled_flag'], \PDO::PARAM_INT);
            $statement->bindValue(':organization_id', $data['organization_id'], \PDO::PARAM_INT);
            $statement->bindValue(':store_name', $data['store_name'], \PDO::PARAM_STR);
            $statement->bindValue(':version', $data['version'], \PDO::PARAM_INT);
            $statement->bindValue(':date_time_zone', $data['date_time_zone'], \PDO::PARAM_STR);
            $statement->bindValue(':store_id', $data['store_id'], \PDO::PARAM_INT);
            $result = $statement->execute();
            $statement->closeCursor();
            return $result;
        }
        return false;
    }
}
