<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use StoreCore\IdentityInterface;

/**
 * Data mapper interface.
 *
 * Interface for data mappers with the four basic CRUD operations to
 * `create()`, `read()`, `update()`, and `delete()` objects.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
 * @see     https://en.wikipedia.org/wiki/Data_mapper_pattern
 * @version 1.0.0
 */
interface DataMapperInterface
{
    public function create(IdentityInterface &$entity): bool;
    public function read(int|string $id): IdentityInterface|null|false;
    public function update(IdentityInterface &$entity): bool;
    public function delete(int|string $id): bool;
}
