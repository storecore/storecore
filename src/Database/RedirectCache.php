<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2019, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\Database;

use \Countable;
use \DateInterval;
use \Exception;
use \PDOException as DatabaseException;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\UriInterface;
use Psr\SimpleCache\CacheInterface;

use StoreCore\Registry;
use StoreCore\Engine\UriFactory;
use StoreCore\Engine\Redirect;
use StoreCore\Types\CacheKey;

use function \is_int;

/**
 * Cache for redirects.
 *
 * @package StoreCore\CMS
 * @see     https://www.php-fig.org/psr/psr-16/ PSR-16: Common Interface for Caching Libraries
 * @see     https://github.com/php-fig/simple-cache/blob/master/src/CacheInterface.php CacheInterface
 * @version 1.0.0-alpha.1
 */
class RedirectCache extends AbstractModel implements
    CacheInterface,
    ContainerInterface,
    Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\RedirectMapper $mapper
     *   Data Access Object (DAO) for database reads and writes.
     */
    private RedirectMapper $mapper;

    /**
     * Creates a redirect cache.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->mapper = new RedirectMapper($this->Registry);
    }

    /**
     * Clears the redirect cache.
     *
     * @param void
     * @return bool
     */
    public function clear(): bool
    {
        try {
            $result = $this->Database->exec('DELETE FROM ' . RedirectMapper::TABLE_NAME);
            return $result === false ? false : true;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Counts the number of active redirects.
     *
     * @param void
     *
     * @return int
     *   Returns the total number of redirects that never expire (and have no
     *   expiration date) or that have not yet expired (and have an expiration
     *   date in the future).
     */
    public function count(): int
    {
        try {
            $statement = $this->Database->query(
                'SELECT COUNT(*) FROM `sc_redirects` FORCE INDEX (`ix_redirects`) WHERE `date_expires` IS NULL OR `date_expires` > UTC_TIMESTAMP()'
            );
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            return (int) $result;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return 0;
    }

    /**
     * Creates a cache key from a URL.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\Engine\Redirect|string $url
     * @return string
     */
    public static function createKey(UriInterface|Redirect|string $url): string
    {
        if ($url instanceof Redirect) {
            $url = key($url->get());
        }
        $key = new CacheKey($url);
        return (string) $key;
    }

    public function delete(string $key): bool
    {
        if (!CacheKey::validate($key)) {
            throw new InvalidArgumentException('Invalid cache key: ' . $key);
        }

        try {
            return $this->mapper->delete($key);
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    public function deleteMultiple(iterable $keys): bool
    {
        $result = true;
        try {
            foreach ($keys as $key) {
                if ($this->deleteItem($key) !== true) {
                    $result = false;
                }
            }
        } catch (InvalidArgumentException $e) {
            throw $e;
        } catch (Exception $e) {
            $result = false;
        }
        return $result;
    }

    public function get(string $key, mixed $default = null): mixed
    {
        if (!CacheKey::validate($key)) {
            throw new InvalidArgumentException('Invalid cache key: ' . $key);
        }

        try {
            $result = $this->mapper->read($key);
            if ($result === false) {
                return $default;
            } else {
                return $this->mapper->toObject($result[0]);
            }
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new CacheException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        return $result;
    }

    public function has(string $key): bool
    {
        if (!CacheKey::validate($key)) {
            throw new InvalidArgumentException('Invalid cache key: ' . $key);
        }

        try {
            $statement = $this->Database->prepare(
                'SELECT COUNT(*) FROM `sc_redirects` FORCE INDEX (`ix_redirects`) WHERE `redirect_id` = :key AND (`date_expires` IS NULL OR `date_expires` > UTC_TIMESTAMP())'
            );
            $statement->bindValue(':key', $key, \PDO::PARAM_STR);
            if ($statement->execute() !== true) return false;
            $result = $statement->fetchColumn(0);
            $statement->closeCursor();
            return $result === 1 ? true : false;
        } catch (DatabaseException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
        }
        return false;
    }

    /**
     * Registers a cache hit.
     *
     * @param StoreCore\Engine\Redirect &$redirect
     * @param null|DateTimeInterface $datetime
     * @return void
     */
    public function hit(Redirect &$redirect, ?DateTimeInterface $datetime = null): void
    {
        if ($datetime === null) {
            $datetime = $this->now();
        }
        $redirect->dateTouched = $datetime;
        $this->mapper->save($redirect);
    }

    public function set(string $key, mixed $redirect, null|int|DateInterval $ttl = null): bool
    {
        if (!CacheKey::validate($key)) {
            throw new InvalidArgumentException('Invalid cache key: ' . $key);
        }

        if (!$redirect instanceof Redirect) {
            throw new InvalidArgumentException('Cache item is not a StoreCore\Engine\Redirect.');
        }

        $redirect->expiresAfter($ttl);
        return $this->mapper->save($redirect);
    }

    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        if (is_int($ttl)) {
            $ttl = new DateInterval('PT' . $ttl . 'S');
        }

        $result = true;
        foreach ($values as $value) {
            $key = self::createKey($value);
            if ($this->set($key, $value, $ttl) !== true) {
                $result = false;
            }
        }
        return $result;
    }
}
