--
-- MySQL data definition.
--
-- @author    Ward van der Put <Ward.van.der.Put@storecore.org>
-- @copyright Copyright © 2014–2024 StoreCore™
-- @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
-- @package   StoreCore\Database
-- @version   1.0.0-alpha.1
--

-- HMVC routing
CREATE TABLE IF NOT EXISTS `sc_routes` (
  `route_path`         VARCHAR(63)  NOT NULL,
  `route_controller`   VARCHAR(63)  NOT NULL,
  `dispatch_priority`  TINYINT      NOT NULL  DEFAULT 0,
  `controller_method`  VARCHAR(63)  NULL  DEFAULT NULL,
  PRIMARY KEY (`route_path`, `route_controller`),
  INDEX `ix_routes` (`route_path`, `dispatch_priority` DESC, `route_controller`, `controller_method`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_routes` (`route_path`, `route_controller`, `controller_method`) VALUES
  ('/admin',                           'StoreCore\\Admin\\FrontController',         NULL),
  ('/admin/lock',                      'StoreCore\\Admin\\LockScreen',              NULL),
  ('/admin/manifest.webmanifest',      'StoreCore\\Admin\\ManifestController',      NULL),
  ('/admin/settings/database/account', 'StoreCore\\Admin\\SettingsDatabaseAccount', NULL),
  ('/admin/sign-in',                   'StoreCore\\Admin\\SignIn',                  NULL),
  ('/admin/sign-out',                  'StoreCore\\Admin\\User',                    'signOut'),
  ('/admin/sw.js',                     'StoreCore\\Admin\\ServiceWorker',           NULL),
  ('/robots.txt',                      'StoreCore\\FileSystem\\Robots',             'handle'),
  ('/status',                          'StoreCore\\Status',                         'output');

-- Redirects
CREATE TABLE IF NOT EXISTS `sc_redirects` (
  `redirect_id`        CHAR(40)      CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'SHA-1 hash',
  `date_expires`       DATETIME      NULL  DEFAULT NULL,
  `redirect_from_url`  VARCHAR(252)  NOT NULL,
  `redirect_to_url`    VARCHAR(252)  NOT NULL  DEFAULT '/',
  `date_created`       DATETIME      NOT NULL,
  `date_touched`       DATETIME      NULL  DEFAULT NULL,
  PRIMARY KEY (`redirect_id`),
  INDEX `ix_redirects` (`date_expires`, `redirect_from_url`, `redirect_to_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COLLATE=utf8_general_ci;


-- Observer design pattern
CREATE TABLE IF NOT EXISTS `sc_subjects` (
  `subject_id`     SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `subject_class`  VARCHAR(127)       NOT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `uk_subject_class` (`subject_class`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_observers` (
  `observer_id`        SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `subject_id`         SMALLINT UNSIGNED  NOT NULL,
  `dispatch_priority`  TINYINT            NOT NULL  DEFAULT 0,
  `observer_class`     VARCHAR(127)       NOT NULL,
  PRIMARY KEY (`observer_id`),
  FOREIGN KEY `fk_observers_subjects` (`subject_id`) REFERENCES `sc_subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_observers_dispatch_priority` (`dispatch_priority` DESC, `observer_id` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_subjects` (`subject_class`) VALUES
  ('StoreCore\\FileSystem\\Logger'),
  ('StoreCore\\CRM\\Organization'),
  ('StoreCore\\CRM\\Person'),
  ('StoreCore\\Session');


--
-- Actions, events, and commands.
--
-- @see https://schema.org/docs/actions.html Schema.org Actions
-- @see https://schema.org/Action Schema.org type `Action`
-- @see https://schema.org/ActionStatusType Schema.org enumeration `ActionStatusType`
--
CREATE TABLE IF NOT EXISTS `sc_action_status_types` (
  `id`     BIT(2)       NOT NULL,
  `name`   VARCHAR(31)  NOT NULL,
  `value`  VARCHAR(63)  NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`),
  UNIQUE KEY (`value`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_action_status_types` (`id`, `name`, `value`) VALUES
  (b'00', 'ActiveActionStatus',    'https://schema.org/ActiveActionStatus'),
  (b'01', 'CompletedActionStatus', 'https://schema.org/CompletedActionStatus'),
  (b'10', 'FailedActionStatus',    'https://schema.org/FailedActionStatus'),
  (b'11', 'PotentialActionStatus', 'https://schema.org/PotentialActionStatus');

CREATE TABLE IF NOT EXISTS `sc_actions` (
  `action_uuid`    BINARY(16)   NOT NULL,
  `action_status`  BIT(2)       NULL  DEFAULT NULL,
  `date_modified`  DATETIME(6)  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `action_class`   VARCHAR(63)  CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `start_time`     DATETIME     NULL  DEFAULT NULL,
  `end_time`       DATETIME     NULL  DEFAULT NULL,
  `agent_uuid`     BINARY(16)   NULL  DEFAULT NULL,
  `object_uuid`    BINARY(16)   NULL  DEFAULT NULL,
  `provider_uuid`  BINARY(16)   NULL  DEFAULT NULL,
  `result_uuid`    BINARY(16)   NULL  DEFAULT NULL,
  `action_json`    TEXT         NOT NULL,
  PRIMARY KEY (`action_uuid`),
  FOREIGN KEY `fk_actions_action_status_types` (`action_status`) REFERENCES `sc_action_status_types` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX `ix_actions` (`date_modified` DESC, `action_status`, `action_class`),
  INDEX `ix_actions_entities` (`agent_uuid`, `object_uuid`, `provider_uuid`, `result_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_action_objects` (
  `action_uuid`            BINARY(16)   NOT NULL,
  `date_expires`           DATETIME(6)  NOT NULL,
  `serialized_php_object`  BLOB         NOT NULL,
  PRIMARY KEY (`action_uuid`),
  FOREIGN KEY `fk_action_objects_actions` (`action_uuid`) REFERENCES `sc_actions` (`action_uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_action_objects` (`date_expires` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

--
-- Action.error property in RFC 7807 format.
--
-- @see https://www.rfc-editor.org/rfc/rfc7807#section-3.1
--      RFC 7807, Section 3.1, Members of a Problem Details Object
--
-- @see https://www.rfc-editor.org/rfc/rfc7231#section-6
--      RFC 7231, Section 6, Response Status Codes
--
CREATE TABLE IF NOT EXISTS `sc_action_errors` (
  `action_uuid`    BINARY(16)         NOT NULL,
  `date_modified`  DATETIME(6)        DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `status`         SMALLINT UNSIGNED  NOT NULL  DEFAULT 501  COMMENT 'HTTP response status code',
  `type`           VARCHAR(252)       NULL  DEFAULT NULL  COMMENT 'URI',
  `title`          VARCHAR(127)       NULL  DEFAULT NULL,
  `detail`         VARCHAR(252)       NULL  DEFAULT NULL,
  `instance`       VARCHAR(252)       NULL  DEFAULT NULL  COMMENT 'URI',
  PRIMARY KEY (`action_uuid`),
  FOREIGN KEY `fk_action_errors_actions` (`action_uuid`) REFERENCES `sc_actions` (`action_uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_action_errors` (`date_modified` DESC, `status` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


-- Languages
CREATE TABLE IF NOT EXISTS `sc_languages` (
  `language_id`   VARCHAR(13)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `parent_id`     VARCHAR(13)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-GB',
  `enabled_flag`  BIT(1)            NOT NULL  DEFAULT b'0',
  `sort_order`    TINYINT UNSIGNED  NOT NULL  DEFAULT 0,
  `english_name`  VARCHAR(40)       NOT NULL,
  `local_name`    VARCHAR(40)       NULL  DEFAULT NULL,
  PRIMARY KEY (`language_id`),
  FOREIGN KEY `fk_languages_languages` (`parent_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  UNIQUE KEY `uk_english_name` (`english_name`),
  INDEX `ix_languages` (`enabled_flag` DESC, `sort_order` ASC, `local_name` ASC, `english_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Languages of the European Union
INSERT IGNORE INTO `sc_languages` (`language_id`, `english_name`, `local_name`, `enabled_flag`) VALUES
  ('bg-BG', 'Bulgarian (Bulgaria)', 'Български', b'0'),
  ('cs-CZ', 'Czech (Czech Republic)', 'Čeština', b'0'),
  ('da-DK', 'Danish (Denmark)', 'Dansk (Danmark)', b'0'),
  ('de-DE', 'German (Germany)', 'Deutsch (Deutschland)', b'1'),
  ('el-GR', 'Greek (Greece)', 'Eλληνικά (Ελλάδα)', b'0'),
  ('en-GB', 'English (United Kingdom)', 'English (United Kingdom)', b'1'),
  ('es-ES', 'Spanish (Spain)', 'Español (España)', b'0'),
  ('et-EE', 'Estonian (Estonia)', 'Eesti', b'0'),
  ('fi-FI', 'Finnish (Finland)', 'Suomi (Suomi)', b'0'),
  ('fr-FR', 'French (France)', 'Français (France)', b'1'),
  ('ga-IE', 'Irish (Ireland)', 'Gaeilge', b'0'),
  ('hr-HR', 'Croatian (Croatia)', 'Hrvatski (Hrvatska)', b'0'),
  ('hu-HU', 'Hungarian (Hungary)', 'Magyar', b'0'),
  ('it-IT', 'Italian (Italy)', 'Italiano (Italia)', b'0'),
  ('lt-LT', 'Lithuanian (Lithuania)', 'Lietuvių', b'0'),
  ('lv-LV', 'Latvian (Latvia)', 'Latviešu', b'0'),
  ('mt-MT', 'Maltese (Malta)', 'Malti', b'0'),
  ('nl-NL', 'Dutch (Netherlands)', 'Nederlands (Nederland)', b'1'),
  ('pl-PL', 'Polish (Poland)', 'Polski', b'0'),
  ('pt-PT', 'Portuguese (Portugal)', 'Português (Portugal)', b'0'),
  ('ro-RO', 'Romanian (Romania)', 'Română (România)', b'0'),
  ('sk-SK', 'Slovak (Slovakia)', 'Slovenčina', b'0'),
  ('sl-SI', 'Slovene (Slovenia)', 'Slovenščina', b'0'),
  ('sv-SE', 'Swedish (Sweden)', 'Svenska (Sverige)', b'0');

-- Other European languages
INSERT IGNORE INTO `sc_languages` (`language_id`, `english_name`, `local_name`, `enabled_flag`) VALUES
  ('ast-ES', 'Asturian (Spain)', 'Asturianu', b'0'),
  ('az-AZ', 'Azerbaijani (Azerbaijan)', 'Azərbaycanca', b'0'),
  ('be-BY', 'Belarusian (Belarus)', 'Беларуская', b'0'),
  ('br-FR', 'Breton (France)', 'Brezhoneg', b'0'),
  ('ca-039', 'Catalan (Southern Europe)', 'Català', b'0'),
  ('cy-GB', 'Welsh (United Kingdom)', 'Cymraeg', b'0'),
  ('eu-ES', 'Basque (Basque)', 'Euskara', b'0'),
  ('hy-AM', 'Armenian (Armenia)', 'Հայերեն', b'0'),
  ('is-IS', 'Icelandic (Iceland)', 'Íslenska', b'0'),
  ('kk-KZ', 'Kazakh (Kazakhstan)', 'қазақ тілі', b'0'),
  ('lb-LU', 'Luxembourgish (Luxembourg)', 'Lëtzebuergesch', b'1'),
  ('nb-NO', 'Norwegian Bokmål (Norway)', 'Norsk bokmål (Norge)', b'0'),
  ('ru-RU', 'Russian (Russia)', 'Русский', b'0'),
  ('sq-AL', 'Albanian (Albania)', 'Shqip (Shqipëri)', b'0'),
  ('sr-SP', 'Serbian (Serbia)', 'Српски', b'0'),
  ('tr-TR', 'Turkish (Turkey)', 'Türkçe', b'0'),
  ('uk-UA', 'Ukrainian (Ukraine)', 'Українська', b'0');

-- Secondary languages
INSERT IGNORE INTO `sc_languages` (`language_id`, `parent_id`, `english_name`, `local_name`, `enabled_flag`) VALUES
  ('en-US', 'en-GB', 'English (United States)', 'English (United States)', b'1'),
  ('es-419', 'es-ES', 'Spanish (Latin America)', 'Español (América Latina)', b'0'),
  ('fr-CA', 'fr-FR', 'French (Canada)', 'Français (Canada)', b'1'),
  ('nl-BE', 'nl-NL', 'Dutch (Belgium)', 'Nederlands (België)', b'1'),
  ('nn-NO', 'nb-NO', 'Norwegian Nynorsk (Norway)', 'Norsk nynorsk (Noreg)', b'0'),
  ('pt-BR', 'pt-PT', 'Portuguese (Brazil)', 'Português (Brasil)', b'0');

-- World languages and languages with over 50 million native speakers
INSERT IGNORE INTO `sc_languages` (`language_id`, `english_name`, `local_name`, `enabled_flag`) VALUES
  ('ar-001', 'Arabic (World)', 'العربية', b'0'),
  ('bn-BD', 'Bengali (Bangladesh)', 'বাংলা ', b'0'),
  ('fa-IR', 'Farsi (Iran)', 'فارسی', b'0'),
  ('hi-IN', 'Hindi (India)', 'हिन्दी', b'0'),
  ('id-ID', 'Indonesian (Indonesia)', 'Bahasa Indonesia', b'0'),
  ('ja-JP', 'Japanese (Japan)', '日本語', b'0'),
  ('jv-ID', 'Javanese (Indonesia)', 'ꦧꦱꦗꦮ', b'0'),
  ('ko-KR', 'Korean (South Korea)', '한국어', b'0'),
  ('ms-MY', 'Malay (Malaysia)', 'بهاس ملايو‎', b'0'),
  ('pa-IN', 'Punjabi (India)', 'پنجابی', b'0'),
  ('ta-IN', 'Tamil (India)', 'தமிழ்', b'0'),
  ('ur-PK', 'Urdu (Pakistan)', 'اردو', b'0'),
  ('vi-VN', 'Vietnamese (Vietnam)', 'Tiếng Việt', b'0'),
  ('zh-Hans', 'Chinese (simplified)', '中文（简体）', b'0'),
  ('zh-Hant', 'Chinese (traditional)', '中文（繁體）', b'0'),

-- Other primary languages
  ('af-ZA', 'Afrikaans (South Africa)', 'Afrikaans (Suid-Afrika)', b'0'),
  ('gu-IN', 'Gujarati (India)', 'ગુજરાતી', b'0'),
  ('he-IL', 'Hebrew (Israel)', 'עברית', b'0'),
  ('ka-GE', 'Georgian (Georgia)', 'ქართული', b'0'),
  ('la-001', 'Latin (World)', 'Latinus', b'0'),
  ('th-TH', 'Thai (Thailand)', 'ไทย', b'0');

-- Other secondary languages
INSERT IGNORE INTO `sc_languages` (`language_id`, `parent_id`, `english_name`, `local_name`, `enabled_flag`) VALUES
  ('af-NA', 'af-ZA', 'Afrikaans (Namibia)', 'Afrikaans (Namibië)', b'0'),
  ('ckm-AT', 'hr-HR', 'Burgenland Croatian (Austria)', 'Gradišćanskohrvatski jezik', b'0'),
  ('da-GL', 'da-DK', 'Danish (Greenland)', 'Dansk (Grønland)', b'0'),
  ('de-AT', 'de-DE', 'German (Austria)', 'Deutsch (Österreich)', b'0'),
  ('de-CH', 'de-DE', 'German (Switzerland)', 'Deutsch (Schweiz)', b'0'),
  ('de-LI', 'de-DE', 'German (Liechtenstein)', 'Deutsch (Liechtenstein)', b'0'),
  ('de-LU', 'de-DE', 'German (Luxembourg)', 'Deutsch (Luxemburg)', b'0'),
  ('el-CY', 'el-GR', 'Greek (Cyprus)', 'Ελληνικά (Κύπρος)', b'0'),
  ('en-AU', 'en-GB', 'English (Australia)', 'English (Australia)', b'0'),
  ('en-CA', 'en-GB', 'English (Canada)', 'English (Canada)', b'0'),
  ('en-IE', 'en-GB', 'English (Ireland)', 'English (Ireland)', b'0'),
  ('es-MX', 'es-419', 'Spanish (Mexico)', 'Español mexicano (México)', b'0'),
  ('fr-BE', 'fr-FR', 'French (Belgium)', 'Français (Belgique)', b'0'),
  ('fr-CH', 'fr-FR', 'French (Switzerland)', 'Français (Suisse)', b'0'),
  ('fr-LU', 'fr-FR', 'French (Luxembourg)', 'Français (Luxembourg)', b'0'),
  ('fr-MC', 'fr-FR', 'French (Monaco)', 'Français (Monaco)', b'0'),
  ('hr-BA', 'hr-HR', 'Croatian (Bosnia & Herzegovina)', 'Hrvatski (Bosna i Hercegovina)', b'0'),
  ('it-CH', 'it-IT', 'Italian (Switzerland)', 'Italiano (Svizzera)', b'0'),
  ('nb-SJ', 'nb-NO', 'Norwegian Bokmål (Svalbard & Jan Mayen)', 'Norsk bokmål (Svalbard og Jan Mayen)', b'0'),
  ('ro-MD', 'ro-RO', 'Romanian (Moldova)', 'Română (Republica Moldova)', b'0'),
  ('sv-AX', 'sv-SE', 'Swedish (Åland Islands)', 'Svenska (Åland)', b'0'),
  ('sv-FI', 'sv-SE', 'Swedish (Finland)', 'Finlandssvenska (Finland)', b'0');

--
-- E-mail addresses.
--
CREATE TABLE IF NOT EXISTS `sc_email_addresses` (
  `email_address_id`    INT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `email_address_hash`  BINARY(32)    NOT NULL,
  `email_address`       VARCHAR(320)  NULL  DEFAULT NULL,
  `date_created`        DATETIME      NOT NULL,
  `date_modified`       DATETIME      NULL  DEFAULT NULL,
  `date_deleted`        DATETIME      NULL  DEFAULT NULL,
  `display_name`        VARCHAR(252)  NULL  DEFAULT NULL,
  PRIMARY KEY (`email_address_id`),
  UNIQUE KEY `uk_email_address_hash` (`email_address_hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


--
-- Organizations.
--
-- @see https://schema.org/Organization
-- @see https://schema.org/LocalBusiness
-- @see https://schema.org/OnlineBusiness
-- @see https://schema.org/OnlineStore
--
-- @see https://developers.google.com/search/docs/advanced/structured-data/local-business
-- @see https://www.iso.org/obp/ui/#iso:std:iso-iec:6523:-1:ed-1:v1:en ISO/IEC 6523
-- @see https://en.wikipedia.org/wiki/Statistical_Classification_of_Economic_Activities_in_the_European_Community
-- @see https://www.cbs.nl/nl-nl/onze-diensten/methoden/classificaties/activiteiten/sbi-2008-standaard-bedrijfsindeling-2008
--
CREATE TABLE IF NOT EXISTS `sc_organizations` (
  `organization_id`           MEDIUMINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `organization_uuid`         BINARY(16)          NOT NULL,
  `parent_organization_uuid`  BINARY(16)          NULL  DEFAULT NULL,
  `email_address_id`          INT UNSIGNED        NULL  DEFAULT NULL,
  `common_name`               VARCHAR(191)        NULL  DEFAULT NULL,
  `legal_name`                VARCHAR(191)        NULL  DEFAULT NULL,
  `alternate_name`            VARCHAR(191)        NULL  DEFAULT NULL,
  `version`                   SMALLINT UNSIGNED   NOT NULL  DEFAULT 1,
  `date_created`              DATETIME            NOT NULL,
  `date_modified`             DATETIME            NULL  DEFAULT NULL,
  `date_deleted`              DATETIME            NULL  DEFAULT NULL,
  `organization_type`         VARCHAR(127)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'Organization',
  `telephone_number`          VARCHAR(31)         NULL  DEFAULT NULL,
  `fax_number`                VARCHAR(31)         NULL  DEFAULT NULL,
  `website_url`               VARCHAR(255)        NULL  DEFAULT NULL,
  `duns_number`               CHAR(9)             NULL  DEFAULT NULL  COMMENT "Dun & Bradstreet D-U-N-S number",
  `isic_code`                 VARCHAR(30)         NULL  DEFAULT NULL  COMMENT "International Standard Industrial Classification of All Economic Activities (ISIC)",
  `iso_6523_code`             TINYTEXT            NULL  DEFAULT NULL  COMMENT 'Serialized ISO/IEC 6523 code',
  `lei_code`                  CHAR(20)            NULL  DEFAULT NULL  COMMENT "ISO 17442 Legal Entity Identifier (LEI)",
  `naics_code`                VARCHAR(6)          NULL  DEFAULT NULL  COMMENT "North American Industry Classification System (NAICS)",
  `tax_id`                    VARCHAR(63)         NULL  DEFAULT NULL,
  `vat_id`                    VARCHAR(63)         NULL  DEFAULT NULL,
  `founding_date`             DATE                NULL  DEFAULT NULL,
  `dissolution_date`          DATE                NULL  DEFAULT NULL,
  PRIMARY KEY (`organization_id`),
  UNIQUE KEY `uk_organization_uuid` (`organization_uuid`),
  FOREIGN KEY `fk_organizations_email_addresses` (`email_address_id`) REFERENCES `sc_email_addresses` (`email_address_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FULLTEXT INDEX `ix_organizations_names` (`common_name`, `legal_name`, `alternate_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- @see https://mariadb.com/kb/en/json-data-type/
CREATE TABLE IF NOT EXISTS `sc_organization_properties` (
  `entity`     MEDIUMINT UNSIGNED  NOT NULL,
  `attribute`  VARCHAR(31)         CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `value`      TEXT                NOT NULL,
  PRIMARY KEY (`entity`, `attribute`),
  FOREIGN KEY (`entity`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


-- Persons
-- @see https://schema.org/Person
-- @see https://developers.google.com/resources/api-libraries/documentation/people/v1/php/latest/class-Google_Service_PeopleService_Name.html
CREATE TABLE IF NOT EXISTS `sc_persons` (
  `person_id`                  INT UNSIGNED         NOT NULL  AUTO_INCREMENT,
  `person_uuid`                BINARY(16)           NOT NULL,
  `email_address_id`           INT UNSIGNED         NULL  DEFAULT NULL,
  `anonymized_flag`            BIT(1)               NOT NULL  DEFAULT b'0',
  `date_created`               DATETIME             NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`              DATETIME             NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `date_deleted`               DATETIME             NULL  DEFAULT NULL,
  `gender`                     TINYINT(1) UNSIGNED  NOT NULL  DEFAULT 0  COMMENT 'ISO/IEC 5218',
  `honorific_prefix`           VARCHAR(31)          NULL  DEFAULT NULL,
  `given_name`                 VARCHAR(127)         NULL  DEFAULT NULL  COMMENT 'First name',
  `additional_name`            VARCHAR(127)         NULL  DEFAULT NULL  COMMENT 'Middle name',
  `family_name`                VARCHAR(127)         NULL  DEFAULT NULL  COMMENT 'Last name',
  `honorific_suffix`           VARCHAR(31)          NULL  DEFAULT NULL,
  `display_name`               VARCHAR(255)         NULL  DEFAULT NULL,
  `display_name_last_first`    VARCHAR(255)         NULL  DEFAULT NULL,
  `full_name`                  VARCHAR(255)         NULL  DEFAULT NULL,
  `phonetic_family_name`       VARCHAR(127)         NULL  DEFAULT NULL,
  `phonetic_full_name`         VARCHAR(127)         NULL  DEFAULT NULL,
  `phonetic_given_name`        VARCHAR(127)         NULL  DEFAULT NULL,
  `phonetic_honorific_prefix`  VARCHAR(127)         NULL  DEFAULT NULL,
  `phonetic_honorific_suffix`  VARCHAR(127)         NULL  DEFAULT NULL,
  `phonetic_middle_name`       VARCHAR(127)         NULL  DEFAULT NULL,
  `telephone_number`           VARCHAR(31)          NULL  DEFAULT NULL,
  `nationality`                CHAR(2)              NULL  DEFAULT NULL  COMMENT 'ISO 3166-1 alpha-2 country code',
  `birth_date`                 DATE                 NULL  DEFAULT NULL,
  `birth_place`                VARCHAR(127)         NULL  DEFAULT NULL,
  `death_date`                 DATE                 NULL  DEFAULT NULL,
  `death_place`                VARCHAR(127)         NULL  DEFAULT NULL,
  `same_as`                    VARCHAR(255)         NULL  DEFAULT NULL  COMMENT 'Schema.org Thing.sameAs',
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `uk_person_uuid` (`person_uuid`),
  FOREIGN KEY `fk_persons_email_addresses` (`email_address_id`) REFERENCES `sc_email_addresses` (`email_address_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  INDEX `ix_persons_dates` (`date_modified` DESC, `date_created` DESC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Roles of persons in organizations.
-- @see https://schema.org/Person
-- @see https://schema.org/Role
CREATE TABLE IF NOT EXISTS `sc_person_organization_roles` (
  `role_id`    TINYINT UNSIGNED  NOT NULL,
  `role_name`  VARCHAR(31)       NOT NULL,
  PRIMARY KEY (`role_id`), 
  UNIQUE KEY (`role_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see https://schema.org/alumni
-- @see https://schema.org/athlete
-- @see https://schema.org/coach
-- @see https://schema.org/employee
-- @see https://schema.org/founder
INSERT IGNORE INTO `sc_person_organization_roles` (`role_id`, `role_name`) VALUES
  (  0, 'employee'),

  (  1, 'alumnus'),
  (  2, 'athlete'),
  (  3, 'coach'),
  (  4, 'founder'),
  (  5, 'funder'),
  (  6, 'member'),
  (  7, 'sponsor');

-- Organizations associated with a person.
-- @see https://schema.org/OrganizationRole Schema.org type `OrganizationRole`
-- @see https://schema.org/jobTitle Schema.org property `jobTitle`
CREATE TABLE IF NOT EXISTS `sc_person_organizations` (
  `person_id`         INT UNSIGNED        NOT NULL,
  `organization_id`   MEDIUMINT UNSIGNED  NOT NULL,
  `email_address_id`  INT UNSIGNED        NULL  DEFAULT NULL,
  `role_id`           TINYINT UNSIGNED    NULL  DEFAULT NULL,
  `job_title`         VARCHAR(252)        NULL  DEFAULT NULL,
  `telephone_number`  VARCHAR(31)         NULL  DEFAULT NULL,
  `fax_number`        VARCHAR(31)         NULL  DEFAULT NULL,
  `from_date`         DATE                NULL  DEFAULT NULL,
  `thru_date`         DATE                NULL  DEFAULT NULL,
  PRIMARY KEY (`person_id`, `organization_id`),
  FOREIGN KEY `fk_person_organizations_persons` (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_person_organizations_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`email_address_id`) REFERENCES `sc_email_addresses` (`email_address_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (`role_id`) REFERENCES `sc_person_organization_roles` (`role_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


-- User groups
CREATE TABLE IF NOT EXISTS `sc_user_groups` (
  `user_group_id`    TINYINT UNSIGNED  NOT NULL,
  `user_group_name`  VARCHAR(255)      NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

INSERT IGNORE INTO `sc_user_groups` (`user_group_id`, `user_group_name`) VALUES
  (  0, 'Access denied'),
  (254, 'Administrators'),
  (255, 'Root');

-- Users and user agents
CREATE TABLE IF NOT EXISTS `sc_users` (
  `user_id`           SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `user_uuid`         BINARY(16)         NOT NULL,
  `user_group_id`     TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `email_address_id`  INT UNSIGNED       NULL  DEFAULT NULL,
  `language_id`       VARCHAR(13)        NOT NULL  DEFAULT 'en-GB',
  `person_id`         INT UNSIGNED       NULL  DEFAULT NULL,
  `password_reset`    DATETIME           NOT NULL,
  `password_salt`     VARCHAR(255)       NOT NULL,
  `hash_algo`         VARCHAR(255)       NOT NULL,
  `password_hash`     VARCHAR(255)       NOT NULL,
  `pin_code`          VARCHAR(6)         NOT NULL  DEFAULT '0000',
  `date_time_zone`    VARCHAR(63)        NOT NULL  DEFAULT 'UTC'  COMMENT 'PHP DateTimeZone identifier',
  `email_token`       VARCHAR(255)       NULL  DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uk_user_uuid` (`user_uuid`),
  FOREIGN KEY `fk_users_user_groups` (`user_group_id`) REFERENCES `sc_user_groups` (`user_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_users_email_addresses` (`email_address_id`) REFERENCES `sc_email_addresses` (`email_address_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY `fk_users_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY `fk_users_persons` (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- User password archive
CREATE TABLE IF NOT EXISTS `sc_users_password_history` (
  `user_id`        SMALLINT UNSIGNED  NOT NULL,
  `from_date`      DATETIME           NOT NULL,
  `thru_date`      DATETIME           NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `checked_flag`   BIT(1)             NOT NULL  DEFAULT b'0',
  `password_salt`  VARCHAR(255)       NOT NULL,
  `password_hash`  VARCHAR(255)       NOT NULL,
  PRIMARY KEY (`user_id`, `from_date`),
  FOREIGN KEY `fk_users_password_history_users` (`user_id`) REFERENCES `sc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Browsers and other user agents
CREATE TABLE IF NOT EXISTS `sc_user_agents` (
  `user_agent_id`    BINARY(20)  NOT NULL  COMMENT 'Binary SHA-1 hash',
  `first_sighting`   DATETIME    NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `last_sighting`    DATETIME    NOT NULL  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `http_user_agent`  TEXT        NOT NULL,
  PRIMARY KEY (`user_agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Successful and failed login attempts
CREATE TABLE IF NOT EXISTS `sc_login_attempts` (
  `attempted`       DATETIME(6)   NOT NULL,
  `successful`      BIT(1)        NOT NULL  DEFAULT b'0',
  `remote_address`  VARCHAR(45)   NOT NULL,
  `user_identity`   VARCHAR(320)  NULL  DEFAULT NULL,
  PRIMARY KEY (`attempted`, `successful`, `remote_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- IP blocklist
CREATE TABLE IF NOT EXISTS `sc_ip_blocklist` (
  `ip_address`  VARCHAR(45)  NOT NULL,
  `from_date`   DATETIME     NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`   DATETIME     NULL  DEFAULT NULL,
  `last_seen`   DATETIME     NULL  DEFAULT NULL,
  PRIMARY KEY (`ip_address`),
  INDEX `ix_date_range` (`thru_date` DESC, `last_seen` DESC, `from_date` DESC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Optional IP blocklist comments
CREATE TABLE IF NOT EXISTS `sc_ip_blocklist_comments` (
  `ip_address`     VARCHAR(45)   CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `date_modified`  TIMESTAMP     NOT NULL  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `comments`       VARCHAR(255)  NOT NULL  COMMENT 'Reason, source or other internal memo',
  PRIMARY KEY (`ip_address`, `date_modified`),
  FOREIGN KEY `fk_ip_blocklist_comments_ip_blocklist` (`ip_address`) REFERENCES `sc_ip_blocklist` (`ip_address`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_date_modified` (`date_modified` DESC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


-- IP access control list (ACL) for administrators and API consumers
CREATE TABLE IF NOT EXISTS `sc_ip_access_control_list` (
  `ip_address`  VARCHAR(45)  NOT NULL,
  `admin_flag`  BIT(1)       NOT NULL  DEFAULT b'0',
  `api_flag`    BIT(1)       NOT NULL  DEFAULT b'0',
  `from_date`   DATETIME     NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`   DATETIME     NULL  DEFAULT NULL,
  PRIMARY KEY (`ip_address`),
  INDEX `ix_date_range` (`from_date`, `thru_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


-- Currencies
CREATE TABLE IF NOT EXISTS `sc_currencies` (
  `currency_id`      SMALLINT(3) UNSIGNED  NOT NULL  COMMENT 'ISO 4217 currency number',
  `currency_code`    CHAR(3)               CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'ISO 4217 currency code',
  `digits`           SMALLINT(1) UNSIGNED  NOT NULL  DEFAULT 2,
  `currency_symbol`  VARCHAR(8)            NOT NULL  DEFAULT '¤',
  `currency_name`    VARCHAR(63)           NOT NULL  COMMENT 'Official ISO 4217 currency name',
  PRIMARY KEY (`currency_id`),
  UNIQUE KEY `uk_currency_code` (`currency_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

INSERT IGNORE INTO `sc_currencies`
    (`currency_code`, `currency_id`, `digits`, `currency_name`)
  VALUES
    ('AED', 784, 2, 'United Arab Emirates dirham'),
    ('AFN', 971, 2, 'Afghan afghani'),
    ('ALL',   8, 2, 'Albanian lek'),
    ('AMD',  51, 2, 'Armenian dram'),
    ('ANG', 532, 2, 'Netherlands Antillean guilder'),
    ('AOA', 973, 2, 'Angolan kwanza'),
    ('ARS',  32, 2, 'Argentine peso'),
    ('AUD',  36, 2, 'Australian dollar'),
    ('AWG', 533, 2, 'Aruban florin'),
    ('AZN', 944, 2, 'Azerbaijani manat'),
    ('BAM', 977, 2, 'Bosnia and Herzegovina convertible mark'),
    ('BBD',  52, 2, 'Barbados dollar'),
    ('BDT',  50, 2, 'Bangladeshi taka'),
    ('BGN', 975, 2, 'Bulgarian lev'),
    ('BHD',  48, 3, 'Bahraini dinar'),
    ('BIF', 108, 0, 'Burundian franc'),
    ('BMD',  60, 2, 'Bermudian dollar'),
    ('BND',  96, 2, 'Brunei dollar'),
    ('BOB',  68, 2, 'Boliviano'),
    ('BRL', 986, 2, 'Brazilian real'),
    ('BSD',  44, 2, 'Bahamian dollar'),
    ('BTN',  64, 2, 'Bhutanese ngultrum'),
    ('BWP',  72, 2, 'Botswana pula'),
    ('BYR', 974, 0, 'Belarusian ruble'),
    ('BZD',  84, 2, 'Belize dollar'),
    ('CAD', 124, 2, 'Canadian dollar'),
    ('CDF', 976, 2, 'Congolese franc'),
    ('CHF', 756, 2, 'Swiss franc'),
    ('CLP', 152, 0, 'Chilean peso'),
    ('CNY', 156, 2, 'Chinese yuan'),
    ('COP', 170, 2, 'Colombian peso'),
    ('CRC', 188, 2, 'Costa Rican colon'),
    ('CUC', 931, 2, 'Cuban convertible peso'),
    ('CUP', 192, 2, 'Cuban peso'),
    ('CVE', 132, 0, 'Cape Verde escudo'),
    ('CZK', 203, 2, 'Czech koruna'),
    ('DJF', 262, 0, 'Djiboutian franc'),
    ('DKK', 208, 2, 'Danish krone'),
    ('DOP', 214, 2, 'Dominican peso'),
    ('DZD',  12, 2, 'Algerian dinar'),
    ('EGP', 818, 2, 'Egyptian pound'),
    ('ERN', 232, 2, 'Eritrean nakfa'),
    ('ETB', 230, 2, 'Ethiopian birr'),
    ('EUR', 978, 2, 'Euro'),
    ('FJD', 242, 2, 'Fiji dollar'),
    ('FKP', 238, 2, 'Falkland Islands pound'),
    ('GBP', 826, 2, 'Pound sterling'),
    ('GEL', 981, 2, 'Georgian lari'),
    ('GHS', 936, 2, 'Ghanaian cedi'),
    ('GIP', 292, 2, 'Gibraltar pound'),
    ('GMD', 270, 2, 'Gambian dalasi'),
    ('GNF', 324, 0, 'Guinean franc'),
    ('GTQ', 320, 2, 'Guatemalan quetzal'),
    ('GYD', 328, 2, 'Guyanese dollar'),
    ('HKD', 344, 2, 'Hong Kong dollar'),
    ('HNL', 340, 2, 'Honduran lempira'),
    ('HRK', 191, 2, 'Croatian kuna'),
    ('HTG', 332, 2, 'Haitian gourde'),
    ('HUF', 348, 2, 'Hungarian forint'),
    ('IDR', 360, 2, 'Indonesian rupiah'),
    ('ILS', 376, 2, 'Israeli new shekel'),
    ('INR', 356, 2, 'Indian rupee'),
    ('IQD', 368, 3, 'Iraqi dinar'),
    ('IRR', 364, 2, 'Iranian rial'),
    ('ISK', 352, 0, 'Icelandic króna'),
    ('JMD', 388, 2, 'Jamaican dollar'),
    ('JOD', 400, 3, 'Jordanian dinar'),
    ('JPY', 392, 0, 'Japanese yen'),
    ('KES', 404, 2, 'Kenyan shilling'),
    ('KGS', 417, 2, 'Kyrgyzstani som'),
    ('KHR', 116, 2, 'Cambodian riel'),
    ('KMF', 174, 0, 'Comoro franc'),
    ('KPW', 408, 2, 'North Korean won'),
    ('KRW', 410, 0, 'South Korean won'),
    ('KWD', 414, 3, 'Kuwaiti dinar'),
    ('KYD', 136, 2, 'Cayman Islands dollar'),
    ('KZT', 398, 2, 'Kazakhstani tenge'),
    ('LAK', 418, 2, 'Lao kip'),
    ('LBP', 422, 2, 'Lebanese pound'),
    ('LKR', 144, 2, 'Sri Lankan rupee'),
    ('LRD', 430, 2, 'Liberian dollar'),
    ('LSL', 426, 2, 'Lesotho loti'),
    ('LYD', 434, 3, 'Libyan dinar'),
    ('MAD', 504, 2, 'Moroccan dirham'),
    ('MDL', 498, 2, 'Moldovan leu'),
    ('MGA', 969, 1, 'Malagasy ariary'),
    ('MKD', 807, 2, 'Macedonian denar'),
    ('MMK', 104, 2, 'Myanmar kyat'),
    ('MNT', 496, 2, 'Mongolian tugrik'),
    ('MOP', 446, 2, 'Macanese pataca'),
    ('MRO', 478, 1, 'Mauritanian ouguiya'),
    ('MUR', 480, 2, 'Mauritian rupee'),
    ('MVR', 462, 2, 'Maldivian rufiyaa'),
    ('MWK', 454, 2, 'Malawian kwacha'),
    ('MXN', 484, 2, 'Mexican peso'),
    ('MYR', 458, 2, 'Malaysian ringgit'),
    ('MZN', 943, 2, 'Mozambican metical'),
    ('NAD', 516, 2, 'Namibian dollar'),
    ('NGN', 566, 2, 'Nigerian naira'),
    ('NIO', 558, 2, 'Nicaraguan córdoba'),
    ('NOK', 578, 2, 'Norwegian krone'),
    ('NPR', 524, 2, 'Nepalese rupee'),
    ('NZD', 554, 2, 'New Zealand dollar'),
    ('OMR', 512, 3, 'Omani rial'),
    ('PAB', 590, 2, 'Panamanian balboa'),
    ('PEN', 604, 2, 'Peruvian nuevo sol'),
    ('PGK', 598, 2, 'Papua New Guinean kina'),
    ('PHP', 608, 2, 'Philippine peso'),
    ('PKR', 586, 2, 'Pakistani rupee'),
    ('PLN', 985, 2, 'Polish złoty'),
    ('PYG', 600, 0, 'Paraguayan guaraní'),
    ('QAR', 634, 2, 'Qatari riyal'),
    ('RON', 946, 2, 'Romanian new leu'),
    ('RSD', 941, 2, 'Serbian dinar'),
    ('RUB', 643, 2, 'Russian ruble'),
    ('RWF', 646, 0, 'Rwandan franc'),
    ('SAR', 682, 2, 'Saudi riyal'),
    ('SBD',  90, 2, 'Solomon Islands dollar'),
    ('SCR', 690, 2, 'Seychelles rupee'),
    ('SDG', 938, 2, 'Sudanese pound'),
    ('SEK', 752, 2, 'Swedish krona/kronor'),
    ('SGD', 702, 2, 'Singapore dollar'),
    ('SHP', 654, 2, 'Saint Helena pound'),
    ('SLL', 694, 2, 'Sierra Leonean leone'),
    ('SOS', 706, 2, 'Somali shilling'),
    ('SRD', 968, 2, 'Surinamese dollar'),
    ('SSP', 728, 2, 'South Sudanese pound'),
    ('STD', 678, 2, 'São Tomé and Príncipe dobra'),
    ('SYP', 760, 2, 'Syrian pound'),
    ('SZL', 748, 2, 'Swazi lilangeni'),
    ('THB', 764, 2, 'Thai baht'),
    ('TJS', 972, 2, 'Tajikistani somoni'),
    ('TMT', 934, 2, 'Turkmenistani manat'),
    ('TND', 788, 3, 'Tunisian dinar'),
    ('TOP', 776, 2, 'Tongan paʻanga'),
    ('TRY', 949, 2, 'Turkish lira'),
    ('TTD', 780, 2, 'Trinidad and Tobago dollar'),
    ('TWD', 901, 2, 'New Taiwan dollar'),
    ('TZS', 834, 2, 'Tanzanian shilling'),
    ('UAH', 980, 2, 'Ukrainian hryvnia'),
    ('UGX', 800, 0, 'Ugandan shilling'),
    ('USD', 840, 2, 'United States dollar'),
    ('UYU', 858, 2, 'Uruguayan peso'),
    ('UZS', 860, 2, 'Uzbekistan som'),
    ('VEF', 937, 2, 'Venezuelan bolívar'),
    ('VND', 704, 0, 'Vietnamese dong'),
    ('VUV', 548, 0, 'Vanuatu vatu'),
    ('WST', 882, 2, 'Samoan tala'),
    ('XAF', 950, 0, 'CFA franc BEAC'),
    ('XCD', 951, 2, 'East Caribbean dollar'),
    ('XOF', 952, 0, 'CFA franc BCEAO'),
    ('XPF', 953, 0, 'CFP franc (franc Pacifique)'),
    ('XTS', 963, 2, 'Code reserved for testing purposes'),
    ('XXX', 999, 0, 'No currency'),
    ('YER', 886, 2, 'Yemeni rial'),
    ('ZAR', 710, 2, 'South African rand'),
    ('ZMW', 967, 2, 'Zambian kwacha'),
    ('ZWL', 932, 2, 'Zimbabwean dollar');

-- Dollar
UPDATE `sc_currencies` SET `currency_symbol` = '$'
  WHERE `currency_code` IN ('ARS','AUD','BSD','BBD','BMD','BND','CAD','CLP','COP','FJD','GYD','HKD','KYD','LRD','MXN','NAD','NZD','SBD','SGD','SHP','SRD','SVC','TVD','USD','XCD')
  AND `currency_symbol` = '¤';

-- Guilder or florin
UPDATE `sc_currencies` SET `currency_symbol` = 'ƒ'
  WHERE `currency_code` IN ('ANG','AWG')
  AND `currency_symbol` = '¤';

-- Krona or krone
UPDATE `sc_currencies` SET `currency_symbol` = 'kr'
  WHERE `currency_code` IN ('DKK','EEK','ISK','NOK','SEK')
  AND `currency_symbol` = '¤';

-- Pound
UPDATE `sc_currencies` SET `currency_symbol` = '£'
  WHERE `currency_code` IN ('EGP','FKP','GBP','GGP','GIP','IMP','JEP','LBP','SYP')
  AND `currency_symbol` = '¤';

-- Rial or riyal
UPDATE `sc_currencies` SET `currency_symbol` = '﷼'
  WHERE `currency_code` IN ('IRR','OMR','QAR','SAR','YER')
  AND `currency_symbol` = '¤';

-- Rupee
UPDATE `sc_currencies` SET `currency_symbol` = '₨'
  WHERE `currency_code` IN ('LKR','MUR','NPR','PKR','SCR')
  AND `currency_symbol` = '¤';

-- Other currency symbols
-- @see https://en.wikipedia.org/wiki/Currency_symbol
UPDATE `sc_currencies` SET `currency_symbol` = 'د.إ' WHERE `currency_code` = 'AED' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Lek' WHERE `currency_code` = 'ALL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '؋' WHERE `currency_code` = 'AFN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '֏' WHERE `currency_code` = 'AMD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Kz' WHERE `currency_code` = 'AOA' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'ман' WHERE `currency_code` = 'AZN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'KM' WHERE `currency_code` = 'BAM' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '৳' WHERE `currency_code` = 'BDT' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'лв' WHERE `currency_code` = 'BGN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '.د.ب' WHERE `currency_code` = 'BHD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'FBu' WHERE `currency_code` = 'BIF' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '$b' WHERE `currency_code` = 'BOB' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Nu.' WHERE `currency_code` = 'BTN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'P' WHERE `currency_code` = 'BWP' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'p.' WHERE `currency_code` = 'BYR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'BZ$' WHERE `currency_code` = 'BZD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'R$' WHERE `currency_code` = 'BRL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'FC' WHERE `currency_code` = 'CDF' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'CHF' WHERE `currency_code` = 'CHF' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '¥' WHERE `currency_code` = 'CNY' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₱' WHERE `currency_code` = 'CUP' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₡' WHERE `currency_code` = 'CRC' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Kčs' WHERE `currency_code` = 'CZK' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'RD$' WHERE `currency_code` = 'DOP' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '€' WHERE `currency_code` = 'EUR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '¢' WHERE `currency_code` = 'GHC' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Q' WHERE `currency_code` = 'GTQ' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'L' WHERE `currency_code` = 'HNL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'kn' WHERE `currency_code` = 'HRK' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Ft' WHERE `currency_code` = 'HUF' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Rp' WHERE `currency_code` = 'IDR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₪' WHERE `currency_code` = 'ILS' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₹' WHERE `currency_code` = 'INR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'J$' WHERE `currency_code` = 'JMD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '¥' WHERE `currency_code` = 'JPY' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'лв' WHERE `currency_code` = 'KGS' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '៛' WHERE `currency_code` = 'KHR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₩' WHERE `currency_code` = 'KPW' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₩' WHERE `currency_code` = 'KRW' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'лв' WHERE `currency_code` = 'KZT' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₭' WHERE `currency_code` = 'LAK' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Ls' WHERE `currency_code` = 'LVL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Lt' WHERE `currency_code` = 'LTL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'ден' WHERE `currency_code` = 'MKD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₮' WHERE `currency_code` = 'MNT' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'RM' WHERE `currency_code` = 'MYR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'MT' WHERE `currency_code` = 'MZN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'C$' WHERE `currency_code` = 'NIO' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₦' WHERE `currency_code` = 'NGN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'B/.' WHERE `currency_code` = 'PAB' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Gs' WHERE `currency_code` = 'PYG' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'S/.' WHERE `currency_code` = 'PEN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₱' WHERE `currency_code` = 'PHP' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'zł' WHERE `currency_code` = 'PLN' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'lei' WHERE `currency_code` = 'RON' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'руб' WHERE `currency_code` = 'RUB' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Дин.' WHERE `currency_code` = 'RSD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'S' WHERE `currency_code` = 'SOS' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '฿' WHERE `currency_code` = 'THB' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₤' WHERE `currency_code` = 'TRL' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₺' WHERE `currency_code` = 'TRY' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'TT$' WHERE `currency_code` = 'TTD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'NT$' WHERE `currency_code` = 'TWD' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₴' WHERE `currency_code` = 'UAH' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '$U' WHERE `currency_code` = 'UYU' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'лв' WHERE `currency_code` = 'UZS' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Bs' WHERE `currency_code` = 'VEF' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = '₫' WHERE `currency_code` = 'VND' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'R' WHERE `currency_code` = 'ZAR' AND `currency_symbol` = '¤';
UPDATE `sc_currencies` SET `currency_symbol` = 'Z$' WHERE `currency_code` = 'ZWD' AND `currency_symbol` = '¤';


-- Translation Memory (TM)
CREATE TABLE IF NOT EXISTS `sc_translation_memory` (
  `translation_id`   VARCHAR(63)  CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `language_id`      VARCHAR(13)  CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-GB',
  `admin_only_flag`  BIT(1)       NOT NULL  DEFAULT b'0',
  `date_modified`    TIMESTAMP    NOT NULL  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `translation`      TEXT         NULL,
  PRIMARY KEY (`translation_id`, `language_id`),
  FOREIGN KEY `fk_translation_memory_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Countries and ISO country codes
CREATE TABLE IF NOT EXISTS `sc_countries` (
  `country_id`            SMALLINT(3) UNSIGNED  NOT NULL  COMMENT 'ISO 3166-1 numeric code',
  `enabled_flag`          BIT(1)                NOT NULL  DEFAULT b'1',
  `postal_code_required`  BIT(1)                NOT NULL  DEFAULT b'0',
  `subdivision_required`  BIT(1)                NOT NULL  DEFAULT b'0',
  `iso_alpha_two`         CHAR(2)               CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'ISO 3166-1 alpha-2 code',
  `iso_alpha_three`       CHAR(3)               CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'ISO 3166-1 alpha-3 code',
  `country_code_tld`      CHAR(2)               CHARACTER SET ascii  COLLATE ascii_bin  NULL  DEFAULT NULL  COMMENT 'ccTLD',
  `global_country_name`   VARCHAR(63)           NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `uk_iso_alpha_two` (`iso_alpha_two`),
  UNIQUE KEY `uk_iso_alpha_three` (`iso_alpha_three`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Localized country names
CREATE TABLE IF NOT EXISTS `sc_country_names` (
  `country_id`          SMALLINT(3) UNSIGNED  NOT NULL,
  `language_id`         VARCHAR(13)           CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `local_country_name`  VARCHAR(63)           NOT NULL,
  PRIMARY KEY (`country_id`, `language_id`),
  FOREIGN KEY `fk_country_names_countries` (`country_id`) REFERENCES `sc_countries` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_country_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Country states, provinces, regions, and other subdivisions
CREATE TABLE IF NOT EXISTS `sc_country_subdivisions` (
  `iso_alpha_two`     CHAR(2)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'ISO 3166-1 country code',
  `iso_suffix`        VARCHAR(3)    CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'ISO 3166-2 add-on',
  `language_code`     CHAR(2)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en'  COMMENT 'ISO 639-1 language code',
  `subdivision_name`  VARCHAR(63)   NOT NULL,
  PRIMARY KEY (`iso_alpha_two`, `iso_suffix`, `language_code`),
  FOREIGN KEY `fk_country_subdivisions_countries` (`iso_alpha_two`) REFERENCES `sc_countries` (`iso_alpha_two`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_subdivision_name` (`subdivision_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- ISO country data with ccTLDs
INSERT IGNORE INTO `sc_countries` (`country_id`, `global_country_name`, `iso_alpha_two`, `country_code_tld`, `iso_alpha_three`, `postal_code_required`, `enabled_flag`) VALUES
  (  4, 'Afghanistan', 'AF', 'af', 'AFG', 0, 1),
  (248, 'Åland Islands', 'AX', 'ax', 'ALA', 0, 1),
  (  8, 'Albania', 'AL', 'al', 'ALB', 0, 1),
  ( 12, 'Algeria', 'DZ', 'dz', 'DZA', 0, 1),
  ( 16, 'American Samoa', 'AS', 'as', 'ASM', 0, 1),
  ( 20, 'Andorra', 'AD', 'ad', 'AND', 0, 1),
  ( 24, 'Angola', 'AO', 'ao', 'AGO', 0, 1),
  (660, 'Anguilla', 'AI', 'ai', 'AIA', 0, 1),
  ( 10, 'Antarctica', 'AQ', 'aq', 'ATA', 0, 1),
  ( 28, 'Antigua and Barbuda', 'AG', 'ag', 'ATG', 0, 1),
  ( 32, 'Argentina', 'AR', 'ar', 'ARG', 0, 1),
  ( 51, 'Armenia', 'AM', 'am', 'ARM', 0, 1),
  (533, 'Aruba', 'AW', 'aw', 'ABW', 0, 1),
  ( 36, 'Australia', 'AU', 'au', 'AUS', 0, 1),
  ( 40, 'Austria', 'AT', 'at', 'AUT', 0, 1),
  ( 31, 'Azerbaijan', 'AZ', 'az', 'AZE', 0, 1),
  ( 44, 'Bahamas', 'BS', 'bs', 'BHS', 0, 1),
  ( 48, 'Bahrain', 'BH', 'bh', 'BHR', 0, 1),
  ( 50, 'Bangladesh', 'BD', 'bd', 'BGD', 0, 1),
  ( 52, 'Barbados', 'BB', 'bb', 'BRB', 0, 1),
  (112, 'Belarus', 'BY', 'by', 'BLR', 0, 1),
  ( 56, 'Belgium', 'BE', 'be', 'BEL', 0, 1),
  ( 84, 'Belize', 'BZ', 'bz', 'BLZ', 0, 1),
  (204, 'Benin', 'BJ', 'bj', 'BEN', 0, 1),
  ( 60, 'Bermuda', 'BM', 'bm', 'BMU', 0, 1),
  ( 64, 'Bhutan', 'BT', 'bt', 'BTN', 0, 1),
  ( 68, 'Bolivia', 'BO', 'bo', 'BOL', 0, 1),
  (535, 'Bonaire, Sint Eustatius and Saba', 'BQ', NULL, 'BES', 0, 1),
  ( 70, 'Bosnia and Herzegovina', 'BA', 'ba', 'BIH', 0, 1),
  ( 72, 'Botswana', 'BW', 'bw', 'BWA', 0, 1),
  ( 74, 'Bouvet Island', 'BV', NULL, 'BVT', 0, 1),
  ( 76, 'Brazil', 'BR', 'br', 'BRA', 0, 1),
  ( 86, 'British Indian Ocean Territory', 'IO', 'io', 'IOT', 0, 1),
  ( 96, 'Brunei Darussalam', 'BN', 'bn', 'BRN', 0, 1),
  (100, 'Bulgaria', 'BG', 'bg', 'BGR', 0, 1),
  (854, 'Burkina Faso', 'BF', 'bf', 'BFA', 0, 1),
  (108, 'Burundi', 'BI', 'bi', 'BDI', 0, 1),
  (116, 'Cambodia', 'KH', 'kh', 'KHM', 0, 1),
  (120, 'Cameroon', 'CM', 'cm', 'CMR', 0, 1),
  (124, 'Canada', 'CA', 'ca', 'CAN', 0, 1),
  (132, 'Cabo Verde', 'CV', 'cv', 'CPV', 0, 1),
  (136, 'Cayman Islands', 'KY', 'ky', 'CYM', 0, 1),
  (140, 'Central African Republic', 'CF', 'cf', 'CAF', 0, 1),
  (148, 'Chad', 'TD', 'td', 'TCD', 0, 1),
  (152, 'Chile', 'CL', 'cl', 'CHL', 0, 1),
  (156, 'China', 'CN', 'cn', 'CHN', 0, 1),
  (162, 'Christmas Island', 'CX', 'cx', 'CXR', 0, 1),
  (166, 'Cocos (Keeling) Islands', 'CC', 'cc', 'CCK', 0, 1),
  (170, 'Colombia', 'CO', 'co', 'COL', 0, 1),
  (174, 'Comoros', 'KM', 'km', 'COM', 0, 1),
  (178, 'Congo', 'CG', 'cg', 'COG', 0, 1),
  (180, 'Congo, Democratic Republic of the', 'CD', 'cd', 'COD', 0, 1),
  (184, 'Cook Islands', 'CK', 'ck', 'COK', 0, 1),
  (188, 'Costa Rica', 'CR', 'cr', 'CRI', 0, 1),
  (384, 'Côte D’Ivoire', 'CI', 'ci', 'CIV', 0, 1),
  (191, 'Croatia', 'HR', 'hr', 'HRV', 0, 1),
  (192, 'Cuba', 'CU', 'cu', 'CUB', 0, 1),
  (531, 'Curaçao', 'CW', 'cw', 'CUW', 0, 1),
  (196, 'Cyprus', 'CY', 'cy', 'CYP', 0, 1),
  (203, 'Czech Republic', 'CZ', 'cz', 'CZE', 0, 1),
  (208, 'Denmark', 'DK', 'dk', 'DNK', 0, 1),
  (262, 'Djibouti', 'DJ', 'dj', 'DJI', 0, 1),
  (212, 'Dominica', 'DM', 'dm', 'DMA', 0, 1),
  (214, 'Dominican Republic', 'DO', 'do', 'DOM', 0, 1),
  (218, 'Ecuador', 'EC', 'ec', 'ECU', 0, 1),
  (818, 'Egypt', 'EG', 'eg', 'EGY', 0, 1),
  (222, 'El Salvador', 'SV', 'sv', 'SLV', 0, 1),
  (226, 'Equatorial Guinea', 'GQ', 'gq', 'GNQ', 0, 1),
  (232, 'Eritrea', 'ER', 'er', 'ERI', 0, 1),
  (233, 'Estonia', 'EE', 'ee', 'EST', 0, 1),
  (231, 'Ethiopia', 'ET', 'et', 'ETH', 0, 1),
  (238, 'Falkland Islands (Malvinas)', 'FK', 'fk', 'FLK', 0, 1),
  (234, 'Faroe Islands', 'FO', 'fo', 'FRO', 0, 1),
  (242, 'Fiji', 'FJ', 'fj', 'FJI', 0, 1),
  (246, 'Finland', 'FI', 'fi', 'FIN', 0, 1),
  (250, 'France', 'FR', 'fr', 'FRA', 1, 1),
  (254, 'French Guiana', 'GF', 'gf', 'GUF', 0, 1),
  (258, 'French Polynesia', 'PF', 'pf', 'PYF', 0, 1),
  (260, 'French Southern Territories', 'TF', 'tf', 'ATF', 0, 1),
  (266, 'Gabon', 'GA', 'ga', 'GAB', 0, 1),
  (270, 'Gambia', 'GM', 'gm', 'GMB', 0, 1),
  (268, 'Georgia', 'GE', 'ge', 'GEO', 0, 1),
  (276, 'Germany', 'DE', 'de', 'DEU', 1, 1),
  (288, 'Ghana', 'GH', 'gh', 'GHA', 0, 1),
  (292, 'Gibraltar', 'GI', 'gi', 'GIB', 0, 1),
  (300, 'Greece', 'GR', 'gr', 'GRC', 0, 1),
  (304, 'Greenland', 'GL', 'gl', 'GRL', 0, 1),
  (308, 'Grenada', 'GD', 'gd', 'GRD', 0, 1),
  (312, 'Guadeloupe', 'GP', 'gp', 'GLP', 0, 1),
  (316, 'Guam', 'GU', 'gu', 'GUM', 0, 1),
  (320, 'Guatemala', 'GT', 'gt', 'GTM', 0, 1),
  (831, 'Guernsey', 'GG', 'gg', 'GGY', 0, 1),
  (324, 'Guinea', 'GN', 'gn', 'GIN', 0, 1),
  (624, 'Guinea-Bissau', 'GW', 'gw', 'GNB', 0, 1),
  (328, 'Guyana', 'GY', 'gy', 'GUY', 0, 1),
  (332, 'Haiti', 'HT', 'ht', 'HTI', 0, 1),
  (334, 'Heard Island and McDonald Islands', 'HM', 'hm', 'HMD', 0, 1),
  (336, 'Holy See', 'VA', 'va', 'VAT', 0, 1),
  (340, 'Honduras', 'HN', 'hn', 'HND', 0, 1),
  (344, 'Hong Kong', 'HK', 'hk', 'HKG', 0, 1),
  (348, 'Hungary', 'HU', 'hu', 'HUN', 0, 1),
  (352, 'Iceland', 'IS', 'is', 'ISL', 0, 1),
  (356, 'India', 'IN', 'in', 'IND', 0, 1),
  (360, 'Indonesia', 'ID', 'in', 'IDN', 0, 1),
  (364, 'Iran', 'IR', 'ir', 'IRN', 0, 1),
  (368, 'Iraq', 'IQ', 'iq', 'IRQ', 0, 1),
  (372, 'Ireland', 'IE', 'ie', 'IRL', 0, 1),
  (833, 'Isle of Man', 'IM', 'im', 'IMN', 0, 1),
  (376, 'Israel', 'IL', 'il', 'ISR', 0, 1),
  (380, 'Italy', 'IT', 'it', 'ITA', 0, 1),
  (388, 'Jamaica', 'JM', 'jm', 'JAM', 0, 1),
  (392, 'Japan', 'JP', 'jp', 'JPN', 0, 1),
  (832, 'Jersey', 'JE', 'je', 'JEY', 0, 1),
  (400, 'Jordan', 'JO', 'jo', 'JOR', 0, 1),
  (398, 'Kazakhstan', 'KZ', 'kz', 'KAZ', 0, 1),
  (404, 'Kenya', 'KE', 'ke', 'KEN', 0, 1),
  (296, 'Kiribati', 'KI', 'ki', 'KIR', 0, 1),
  (414, 'Kuwait', 'KW', 'kw', 'KWT', 0, 1),
  (417, 'Kyrgyzstan', 'KG', 'kg', 'KGZ', 0, 1),
  (418, 'Laos', 'LA', 'la', 'LAO', 0, 1),
  (428, 'Latvia', 'LV', 'lv', 'LVA', 0, 1),
  (422, 'Lebanon', 'LB', 'lb', 'LBN', 0, 1),
  (426, 'Lesotho', 'LS', 'ls', 'LSO', 0, 1),
  (430, 'Liberia', 'LR', 'lr', 'LBR', 0, 1),
  (434, 'Libya', 'LY', 'ly', 'LBY', 0, 1),
  (438, 'Liechtenstein', 'LI', 'li', 'LIE', 0, 1),
  (440, 'Lithuania', 'LT', 'lt', 'LTU', 0, 1),
  (442, 'Luxembourg', 'LU', 'lu', 'LUX', 0, 1),
  (446, 'Macao', 'MO', 'mo', 'MAC', 0, 1),
  (807, 'Macedonia', 'MK', 'mk', 'MKD', 0, 1),
  (450, 'Madagascar', 'MG', 'mg', 'MDG', 0, 1),
  (454, 'Malawi', 'MW', 'mw', 'MWI', 0, 1),
  (458, 'Malaysia', 'MY', 'my', 'MYS', 0, 1),
  (462, 'Maldives', 'MV', 'mv', 'MDV', 0, 1),
  (466, 'Mali', 'ML', 'ml', 'MLI', 0, 1),
  (470, 'Malta', 'MT', 'mt', 'MLT', 0, 1),
  (584, 'Marshall Islands', 'MH', 'mh', 'MHL', 0, 1),
  (474, 'Martinique', 'MQ', 'mq', 'MTQ', 0, 1),
  (478, 'Mauritania', 'MR', 'mr', 'MRT', 0, 1),
  (480, 'Mauritius', 'MU', 'mu', 'MUS', 0, 1),
  (175, 'Mayotte', 'YT', 'yt', 'MYT', 0, 1),
  (484, 'Mexico', 'MX', 'mx', 'MEX', 0, 1),
  (583, 'Micronesia', 'FM', 'fm', 'FSM', 0, 1),
  (498, 'Moldova', 'MD', 'md', 'MDA', 0, 1),
  (492, 'Monaco', 'MC', 'mc', 'MCO', 0, 1),
  (496, 'Mongolia', 'MN', 'mn', 'MNG', 0, 1),
  (499, 'Montenegro', 'ME', 'me', 'MNE', 0, 1),
  (500, 'Montserrat', 'MS', 'ms', 'MSR', 0, 1),
  (504, 'Morocco', 'MA', 'ma', 'MAR', 0, 1),
  (508, 'Mozambique', 'MZ', 'mz', 'MOZ', 0, 1),
  (104, 'Myanmar', 'MM', 'mm', 'MMR', 0, 1),
  (516, 'Namibia', 'NA', 'na', 'NAM', 0, 1),
  (520, 'Nauru', 'NR', 'nr', 'NRU', 0, 1),
  (524, 'Nepal', 'NP', 'np', 'NPL', 0, 1),
  (528, 'Netherlands', 'NL', 'nl', 'NLD', 0, 1),
  (540, 'New Caledonia', 'NC', 'nc', 'NCL', 0, 1),
  (554, 'New Zealand', 'NZ', 'nz', 'NZL', 0, 1),
  (558, 'Nicaragua', 'NI', 'ni', 'NIC', 0, 1),
  (562, 'Niger', 'NE', 'ne', 'NER', 0, 1),
  (566, 'Nigeria', 'NG', 'ng', 'NGA', 0, 1),
  (570, 'Niue', 'NU', 'nu', 'NIU', 0, 1),
  (574, 'Norfolk Island', 'NF', 'nf', 'NFK', 0, 1),
  (580, 'Northern Mariana Islands', 'MP', 'mp', 'MNP', 0, 1),
  (408, 'North Korea', 'KP', 'kp', 'PRK', 0, 1),
  (578, 'Norway', 'NO', 'no', 'NOR', 0, 1),
  (512, 'Oman', 'OM', 'om', 'OMN', 0, 1),
  (586, 'Pakistan', 'PK', 'pk', 'PAK', 0, 1),
  (585, 'Palau', 'PW', 'pw', 'PLW', 0, 1),
  (275, 'Palestine', 'PS', 'ps', 'PSE', 0, 1),
  (591, 'Panama', 'PA', 'pa', 'PAN', 0, 1),
  (598, 'Papua New Guinea', 'PG', 'pg', 'PNG', 0, 1),
  (600, 'Paraguay', 'PY', 'py', 'PRY', 0, 1),
  (604, 'Peru', 'PE', 'pe', 'PER', 0, 1),
  (608, 'Philippines', 'PH', 'ph', 'PHL', 0, 1),
  (612, 'Pitcairn', 'PN', 'pn', 'PCN', 0, 1),
  (616, 'Poland', 'PL', 'pl', 'POL', 0, 1),
  (620, 'Portugal', 'PT', 'pt', 'PRT', 0, 1),
  (630, 'Puerto Rico', 'PR', 'pr', 'PRI', 0, 1),
  (634, 'Qatar', 'QA', 'qa', 'QAT', 0, 1),
  (638, 'Réunion', 'RE', 're', 'REU', 0, 1),
  (642, 'Romania', 'RO', 'ro', 'ROM', 0, 1),
  (643, 'Russia', 'RU', 'ru', 'RUS', 0, 1),
  (646, 'Rwanda', 'RW', 'rw', 'RWA', 0, 1),
  (652, 'Saint Barthélemy', 'BL', 'bl', 'BLM', 0, 1),
  (654, 'Saint Helena, Ascension and Tristan da Cunha', 'SH', 'sh', 'SHN', 0, 1),
  (659, 'Saint Kitts and Nevis', 'KN', 'kn', 'KNA', 0, 1),
  (662, 'Saint Lucia', 'LC', 'lc', 'LCA', 0, 1),
  (663, 'Saint Martin', 'MF', NULL, 'MAF', 0, 1),
  (666, 'Saint Pierre and Miquelon', 'PM', 'pm', 'SPM', 0, 1),
  (670, 'Saint Vincent and the Grenadines', 'VC', 'vc', 'VCT', 0, 1),
  (882, 'Samoa', 'WS', 'ws', 'WSM', 0, 1),
  (674, 'San Marino', 'SM', 'sm', 'SMR', 0, 1),
  (678, 'São Tomé and Principe', 'ST', 'st', 'STP', 0, 1),
  (682, 'Saudi Arabia', 'SA', 'sa', 'SAU', 0, 1),
  (686, 'Senegal', 'SN', 'sn', 'SEN', 0, 1),
  (688, 'Serbia', 'RS', 'rs', 'SRB', 0, 1),
  (690, 'Seychelles', 'SC', 'sc', 'SYC', 0, 1),
  (694, 'Sierra Leone', 'SL', 'sl', 'SLE', 0, 1),
  (702, 'Singapore', 'SG', 'sg', 'SGP', 0, 1),
  (534, 'Sint Maarten', 'SX', 'sx', 'SXM', 0, 1),
  (703, 'Slovakia', 'SK', 'sk', 'SVK', 0, 1),
  (705, 'Slovenia', 'SI', 'si', 'SVN', 0, 1),
  ( 90, 'Solomon Islands', 'SB', 'sb', 'SLB', 0, 1),
  (706, 'Somalia', 'SO', 'so', 'SOM', 0, 1),
  (710, 'South Africa', 'ZA', 'za', 'ZAF', 0, 1),
  (239, 'South Georgia and the South Sandwich Islands', 'GS', 'gs', 'SGS', 0, 1),
  (410, 'South Korea', 'KR', 'kr', 'KOR', 0, 1),
  (728, 'South Sudan', 'SS', 'ss', 'SSD', 0, 1),
  (724, 'Spain', 'ES', 'es', 'ESP', 0, 1),
  (144, 'Sri Lanka', 'LK', 'lk', 'LKA', 0, 1),
  (729, 'Sudan', 'SD', 'sd', 'SDN', 0, 1),
  (740, 'Suriname', 'SR', 'sr', 'SUR', 0, 1),
  (744, 'Svalbard and Jan Mayen', 'SJ', NULL, 'SJM', 0, 1),
  (748, 'Swaziland', 'SZ', 'sz', 'SWZ', 0, 1),
  (752, 'Sweden', 'SE', 'se', 'SWE', 1, 1),
  (756, 'Switzerland', 'CH', 'ch', 'CHE', 0, 1),
  (760, 'Syria', 'SY', 'sy', 'SYR', 0, 1),
  (158, 'Taiwan', 'TW', 'tw', 'TWN', 0, 1),
  (762, 'Tajikistan', 'TJ', 'tj', 'TJK', 0, 1),
  (834, 'Tanzania', 'TZ', 'tz', 'TZA', 0, 1),
  (764, 'Thailand', 'TH', 'th', 'THA', 0, 1),
  (626, 'Timor-Leste', 'TL', 'tl', 'TLS', 0, 1),
  (768, 'Togo', 'TG', 'tg', 'TGO', 0, 1),
  (772, 'Tokelau', 'TK', 'tk', 'TKL', 0, 1),
  (776, 'Tonga', 'TO', 'to', 'TON', 0, 1),
  (780, 'Trinidad and Tobago', 'TT', 'tt', 'TTO', 0, 1),
  (788, 'Tunisia', 'TN', 'tn', 'TUN', 0, 1),
  (792, 'Turkey', 'TR', 'tr', 'TUR', 0, 1),
  (795, 'Turkmenistan', 'TM', 'tm', 'TKM', 0, 1),
  (796, 'Turks and Caicos Islands', 'TC', 'tc', 'TCA', 0, 1),
  (798, 'Tuvalu', 'TV', 'tv', 'TUV', 0, 1),
  (800, 'Uganda', 'UG', 'ug', 'UGA', 0, 1),
  (804, 'Ukraine', 'UA', 'ua', 'UKR', 0, 1),
  (784, 'United Arab Emirates', 'AE', 'ae', 'ARE', 0, 1),
  (826, 'United Kingdom', 'GB', 'uk', 'GBR', 1, 1),
  (840, 'United States of America', 'US', 'us', 'USA', 0, 1),
  (581, 'United States Minor Outlying Islands', 'UM', NULL, 'UMI', 0, 1),
  (858, 'Uruguay', 'UY', 'uy', 'URY', 0, 1),
  (860, 'Uzbekistan', 'UZ', 'uz', 'UZB', 0, 1),
  (548, 'Vanuatu', 'VU', 'vu', 'VUT', 0, 1),
  (862, 'Venezuela', 'VE', 've', 'VEN', 0, 1),
  (704, 'Viet Nam', 'VN', 'vn', 'VNM', 0, 1),
  ( 92, 'Virgin Islands (British)', 'VG', 'vg', 'VGB', 0, 1),
  (850, 'Virgin Islands (U.S.)', 'VI', 'vi', 'VIR', 0, 1),
  (876, 'Wallis and Futuna', 'WF', 'wf', 'WLF', 0, 1),
  (732, 'Western Sahara', 'EH', 'eh', 'ESH', 0, 1),
  (887, 'Yemen', 'YE', 'ye', 'YEM', 0, 1),
  (894, 'Zambia', 'ZM', 'zm', 'ZMB', 0, 1),
  (716, 'Zimbabwe', 'ZW', 'zw', 'ZWE', 0, 1);

INSERT IGNORE INTO `sc_country_names`
    (`country_id`, `language_id`, `local_country_name`)
  VALUES
    ( 56, 'ca-039', 'Bèlgica'),
    ( 56, 'de-DE', 'Belgien'),
    ( 56, 'en-GB', 'Belgium'),
    ( 56, 'es-ES', 'Bélgica'),
    ( 56, 'ga-IE', 'An Bheilg'),
    ( 56, 'fr-FR', 'Belgique'),
    ( 56, 'it-IT', 'Belgio'),
    ( 56, 'lb-LU', 'Belsch'),
    ( 56, 'nl-NL', 'België'),
    ( 56, 'pt-PT', 'Bélgica'),

    (276, 'ca-039', 'Alemanya'),
    (276, 'de-DE', 'Deutschland'),
    (276, 'en-GB', 'Germany'),
    (276, 'es-ES', 'Alemania'),
    (276, 'fr-FR', 'Allemagne'),
    (276, 'ga-IE', 'An Ghearmáin'),
    (276, 'it-IT', 'Germania'),
    (276, 'lb-LU', 'Däitschland'),
    (276, 'nl-NL', 'Duitsland'),
    (276, 'pt-PT', 'Alemanha'),

    (442, 'ca-039', 'Luxemburg'),
    (442, 'de-DE', 'Luxemburg'),
    (442, 'en-GB', 'Luxembourg'),
    (442, 'es-ES', 'Luxemburgo'),
    (442, 'fr-FR', 'Luxembourg'),
    (442, 'ga-IE', 'Lucsamburg'),
    (442, 'it-IT', 'Lussemburgo'),
    (442, 'lb-LU', 'Lëtzebuerg'),
    (442, 'nl-NL', 'Luxemburg'),
    (442, 'pt-PT', 'Luxemburgo'),

    (528, 'ca-039', 'Holanda'),
    (528, 'de-DE', 'Niederlande'),
    (528, 'en-GB', 'Netherlands'),
    (528, 'es-ES', 'Países Bajos'),
    (528, 'fr-FR', 'Pays-Bas'),
    (528, 'ga-IE', 'An Ísiltír'),
    (528, 'it-IT', 'Paesi Bassi'),
    (528, 'lb-LU', 'Holland'),
    (528, 'nl-NL', 'Nederland'),
    (528, 'pt-PT', 'Países Baixos'),

    (826, 'ca-039', 'Regne Unit'),
    (826, 'de-DE', 'Vereinigtes Königreich'),
    (826, 'en-GB', 'United Kingdom'),
    (826, 'es-ES', 'Reino Unido'),
    (826, 'fr-FR', 'Royaume-Uni'),
    (826, 'ga-IE', 'Ríocht Aontaithe'),
    (826, 'it-IT', 'Regno Unito'),
    (826, 'lb-LU', 'Vereenegt Kinnekräich'),
    (826, 'nl-NL', 'Verenigd Koninkrijk'),
    (826, 'pt-PT', 'Reino Unido');


-- ISO 3166-2:AU Australia states and territories in English (en).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:AU
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:AU
INSERT IGNORE INTO `sc_country_subdivisions` VALUES
  ('AU', 'ACT', 'en', 'Australian Capital Territory'),
  ('AU', 'NSW', 'en', 'New South Wales'),
  ('AU', 'NT',  'en', 'Northern Territory'),
  ('AU', 'QLD', 'en', 'Queensland'),
  ('AU', 'SA',  'en', 'South Australia'),
  ('AU', 'TAS', 'en', 'Tasmania'),
  ('AU', 'VIC', 'en', 'Victoria'),
  ('AU', 'WA',  'en', 'Western Australia');

UPDATE sc_countries SET subdivision_required = 1 WHERE iso_alpha_two = 'AU';

-- ISO 3166-2:BD Bangladesh districts in Bangla (bn) and English (en).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:BD
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:BD
-- @see https://bn.wikipedia.org/wiki/ISO_3166-2:BD
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('BD', '01', 'bn', 'বান্দরবান'),
  ('BD', '01', 'en', 'Bandarban'),

  ('BD', '02', 'bn', 'বরগুনা'),
  ('BD', '02', 'en', 'Barguna'),

  ('BD', '03', 'bn', 'বগুড়া'),
  ('BD', '03', 'en', 'Bogura'),

  ('BD', '04', 'bn', 'ব্রাহ্মণবাড়িয়া'),
  ('BD', '04', 'en', 'Brahmanbaria'),

  ('BD', '05', 'bn', 'বাগেরহাট'),
  ('BD', '05', 'en', 'Bagerhat'),

  ('BD', '06', 'bn', 'বরিশাল'),
  ('BD', '06', 'en', 'Barishal'),

  ('BD', '07', 'bn', 'ভোলা'),
  ('BD', '07', 'en', 'Bhola'),

  ('BD', '08', 'bn', 'কুমিল্লা'),
  ('BD', '08', 'en', 'Cumilla'),

  ('BD', '09', 'bn', 'চাঁদপুর'),
  ('BD', '09', 'en', 'Chandpur'),

  ('BD', '10', 'bn', 'চট্টগ্রাম'),
  ('BD', '10', 'en', 'Chattogram'),

  ('BD', '11', 'bn', 'কক্সবাজার'),
  ('BD', '11', 'en', 'Cox’s Bazar'),

  ('BD', '12', 'bn', 'চুয়াডাঙ্গা'),
  ('BD', '12', 'en', 'Chuadanga'),

  ('BD', '13', 'bn', 'ঢাকা'),
  ('BD', '13', 'en', 'Dhaka'),

  ('BD', '14', 'bn', 'দিনাজপুর'),
  ('BD', '14', 'en', 'Dinajpur'),

  ('BD', '15', 'bn', 'ফরিদপুর'),
  ('BD', '15', 'en', 'Faridpur'),

  ('BD', '16', 'bn', 'ফেনী'),
  ('BD', '16', 'en', 'Feni'),

  ('BD', '17', 'bn', 'গোপালগঞ্জ'),
  ('BD', '17', 'en', 'Gopalganj'),

  ('BD', '18', 'bn', 'গাজীপুর'),
  ('BD', '18', 'en', 'Gazipur'),

  ('BD', '19', 'bn', 'গাইবান্ধা'),
  ('BD', '19', 'en', 'Gaibandha'),

  ('BD', '20', 'bn', 'হবিগঞ্জ'),
  ('BD', '20', 'en', 'Habiganj'),

  ('BD', '21', 'bn', 'জামালপুর'),
  ('BD', '21', 'en', 'Jamalpur'),

  ('BD', '22', 'bn', 'যশোর'),
  ('BD', '22', 'en', 'Jashore'),

  ('BD', '23', 'bn', 'ঝিনাইদহ'),
  ('BD', '23', 'en', 'Jhenaidah'),

  ('BD', '24', 'bn', 'জয়পুরহাট'),
  ('BD', '24', 'en', 'Joypurhat'),

  ('BD', '25', 'bn', 'ঝালকাঠি'),
  ('BD', '25', 'en', 'Jhalakathi'),

  ('BD', '26', 'bn', 'কিশোরগঞ্জ'),
  ('BD', '26', 'en', 'Kishoreganj'),

  ('BD', '27', 'bn', 'খুলনা'),
  ('BD', '27', 'en', 'Khulna'),

  ('BD', '28', 'bn', 'কুড়িগ্রাম'),
  ('BD', '28', 'en', 'Kurigram'),

  ('BD', '29', 'bn', 'খাগড়াছড়ি'),
  ('BD', '29', 'en', 'Khagrachhari'),

  ('BD', '30', 'bn', 'কুষ্টিয়া'),
  ('BD', '30', 'en', 'Kushtia'),

  ('BD', '31', 'bn', 'লক্ষ্মীপুর'),
  ('BD', '31', 'en', 'Lakshmipur'),

  ('BD', '32', 'bn', 'লালমনিরহাট'),
  ('BD', '32', 'en', 'Lalmonirhat'),

  ('BD', '33', 'bn', 'মানিকগঞ্জ'),
  ('BD', '33', 'en', 'Manikganj'),

  ('BD', '34', 'bn', 'ময়মনসিংহ'),
  ('BD', '34', 'en', 'Mymensingh'),

  ('BD', '35', 'bn', 'মুন্সিগঞ্জ'),
  ('BD', '35', 'en', 'Munshiganj'),

  ('BD', '36', 'bn', 'মাদারীপুর'),
  ('BD', '36', 'en', 'Madaripur'),

  ('BD', '37', 'bn', 'মাগুরা'),
  ('BD', '37', 'en', 'Magura'),

  ('BD', '38', 'bn', 'মৌলভীবাজার'),
  ('BD', '38', 'en', 'Moulvibazar'),

  ('BD', '39', 'bn', 'মেহেরপুর'),
  ('BD', '39', 'en', 'Meherpur'),

  ('BD', '40', 'bn', 'নারায়ণগঞ্জ'),
  ('BD', '40', 'en', 'Narayanganj'),

  ('BD', '41', 'bn', 'নেত্রকোণা'),
  ('BD', '41', 'en', 'Netrakona'),

  ('BD', '42', 'bn', 'নরসিংদী'),
  ('BD', '42', 'en', 'Narsingdi'),

  ('BD', '43', 'bn', 'নড়াইল'),
  ('BD', '43', 'en', 'Narail'),

  ('BD', '44', 'bn', 'নাটোর'),
  ('BD', '44', 'en', 'Natore'),

  ('BD', '45', 'bn', 'নবাবগঞ্জ'),
  ('BD', '45', 'en', 'Chapai Nawabganj'),

  ('BD', '46', 'bn', 'নীলফামারী'),
  ('BD', '46', 'en', 'Nilphamari'),

  ('BD', '47', 'bn', 'নোয়াখালী'),
  ('BD', '47', 'en', 'Noakhali'),

  ('BD', '48', 'bn', 'নওগাঁ'),
  ('BD', '48', 'en', 'Naogaon'),

  ('BD', '49', 'bn', 'পাবনা'),
  ('BD', '49', 'en', 'Pabna'),

  ('BD', '50', 'bn', 'পিরোজপুর'),
  ('BD', '50', 'en', 'Pirojpur'),

  ('BD', '51', 'bn', 'পটুয়াখালী'),
  ('BD', '51', 'en', 'Patuakhali'),

  ('BD', '52', 'bn', 'পঞ্চগড়'),
  ('BD', '52', 'en', 'Panchagarh'),

  ('BD', '53', 'bn', 'রাজবাড়ী'),
  ('BD', '53', 'en', 'Rajbari'),

  ('BD', '54', 'bn', 'রাজশাহী'),
  ('BD', '54', 'en', 'Rajshahi'),

  ('BD', '55', 'bn', 'রংপুর'),
  ('BD', '55', 'en', 'Rangpur'),

  ('BD', '56', 'bn', 'রাঙামাটি'),
  ('BD', '56', 'en', 'Rangamati'),

  ('BD', '57', 'bn', 'শেরপুর'),
  ('BD', '57', 'en', 'Sherpur'),

  ('BD', '58', 'bn', 'সাতক্ষীরা'),
  ('BD', '58', 'en', 'Satkhira'),

  ('BD', '59', 'bn', 'সিরাজগঞ্জ'),
  ('BD', '59', 'en', 'Sirajganj'),

  ('BD', '60', 'bn', 'সিলেট'),
  ('BD', '60', 'en', 'Sylhet'),

  ('BD', '61', 'bn', 'সুনামগঞ্জ'),
  ('BD', '61', 'en', 'Sunamganj'),

  ('BD', '62', 'bn', 'শরীয়তপুর'),
  ('BD', '62', 'en', 'Shariatpur'),

  ('BD', '63', 'bn', 'টাঙ্গাইল'),
  ('BD', '63', 'en', 'Tangail'),

  ('BD', '64', 'bn', 'ঠাকুরগাঁও'),
  ('BD', '64', 'en', 'Thakurgaon');

-- ISO 3166-2:BE Belgium provinces in Dutch (nl) and French (fr).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:BE
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:BE
-- @see https://fr.wikipedia.org/wiki/ISO_3166-2:BE
-- @see https://nl.wikipedia.org/wiki/ISO_3166-2:BE
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('BE', 'VAN', 'nl', 'Antwerpen'),
  ('BE', 'WBR', 'fr', 'Brabant wallon'),
  ('BE', 'BRU', 'nl', 'Brussels Hoofdstedelijk Gewest'),
  ('BE', 'WHT', 'fr', 'Hainaut'),
  ('BE', 'WLG', 'fr', 'Liège'),
  ('BE', 'VLI', 'nl', 'Limburg'),
  ('BE', 'WLX', 'fr', 'Luxembourg'),
  ('BE', 'WNA', 'fr', 'Namur'),
  ('BE', 'VOV', 'nl', 'Oost-Vlaanderen'),
  ('BE', 'VBR', 'nl', 'Vlaams-Brabant'),
  ('BE', 'VWV', 'nl', 'West-Vlaanderen');

-- ISO 3166-2:BG Bulgaria oblasts in Bulgarian (bg) and Bulgarian in
-- international Latin script `bg-Latn-t-bg-Cyrl-m0-ungegn-2012` (en).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:BG
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:BG
-- @see https://bg.wikipedia.org/wiki/ISO_3166-2:BG
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('BG', '01', 'en', 'Blagoevgrad'),
  ('BG', '01', 'bg', 'Благоевград'),

  ('BG', '02', 'en', 'Burgas'),
  ('BG', '02', 'bg', 'Бургас'),

  ('BG', '03', 'en', 'Varna'),
  ('BG', '03', 'bg', 'Варна'),

  ('BG', '04', 'en', 'Veliko Tarnovo'),
  ('BG', '04', 'bg', 'Велико Търново'),

  ('BG', '05', 'en', 'Vidin'),
  ('BG', '05', 'bg', 'Видин'),

  ('BG', '06', 'en', 'Vratsa'),
  ('BG', '06', 'bg', 'Враца'),

  ('BG', '07', 'en', 'Gabrovo'),
  ('BG', '07', 'bg', 'Габрово'),

  ('BG', '08', 'en', 'Dobrich'),
  ('BG', '08', 'bg', 'Добрич'),

  ('BG', '09', 'en', 'Kardzhali'),
  ('BG', '09', 'bg', 'Кърджали'),

  ('BG', '10', 'en', 'Kyustendil'),
  ('BG', '10', 'bg', 'Кюстендил'),

  ('BG', '11', 'en', 'Lovech'),
  ('BG', '11', 'bg', 'Ловеч'),

  ('BG', '12', 'en', 'Montana'),
  ('BG', '12', 'bg', 'Монтана'),

  ('BG', '13', 'en', 'Pazardzhik'),
  ('BG', '13', 'bg', 'Пазарджик'),

  ('BG', '14', 'en', 'Pernik'),
  ('BG', '14', 'bg', 'Перник'),

  ('BG', '15', 'en', 'Pleven'),
  ('BG', '15', 'bg', 'Плевен'),

  ('BG', '16', 'en', 'Plovdiv'),
  ('BG', '16', 'bg', 'Пловдив'),

  ('BG', '17', 'en', 'Razgrad'),
  ('BG', '17', 'bg', 'Разград'),

  ('BG', '18', 'en', 'Ruse'),
  ('BG', '18', 'bg', 'Русе'),

  ('BG', '19', 'en', 'Silistra'),
  ('BG', '19', 'bg', 'Силистра'),

  ('BG', '20', 'en', 'Sliven'),
  ('BG', '20', 'bg', 'Сливен'),

  ('BG', '21', 'en', 'Smolyan'),
  ('BG', '21', 'bg', 'Смолян'),

  ('BG', '22', 'en', 'Sofia (stolitsa)'),
  ('BG', '22', 'bg', 'София'),

  ('BG', '23', 'en', 'Sofia'),
  ('BG', '23', 'bg', 'Софийска област'),

  ('BG', '24', 'en', 'Stara Zagora'),
  ('BG', '24', 'bg', 'Стара Загора'),

  ('BG', '25', 'en', 'Targovishte'),
  ('BG', '25', 'bg', 'Търговище'),

  ('BG', '26', 'en', 'Haskovo'),
  ('BG', '26', 'bg', 'Хасково'),

  ('BG', '27', 'en', 'Shumen'),
  ('BG', '27', 'bg', 'Шумен'),

  ('BG', '28', 'en', 'Yambol'),
  ('BG', '28', 'bg', 'Ямбол');

-- ISO 3166-2:BR Brazil states in Portuguese (pt).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:BR
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:BR
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('BR', 'AC', 'pt', 'Acre'),
  ('BR', 'AL', 'pt', 'Alagoas'),
  ('BR', 'AM', 'pt', 'Amazonas'),
  ('BR', 'AP', 'pt', 'Amapá'),
  ('BR', 'BA', 'pt', 'Bahia'),
  ('BR', 'CE', 'pt', 'Ceará'),
  ('BR', 'DF', 'pt', 'Distrito Federal'),
  ('BR', 'ES', 'pt', 'Espírito Santo'),
  ('BR', 'GO', 'pt', 'Goiás'),
  ('BR', 'MA', 'pt', 'Maranhão'),
  ('BR', 'MG', 'pt', 'Minas Gerais'),
  ('BR', 'MS', 'pt', 'Mato Grosso do Sul'),
  ('BR', 'MT', 'pt', 'Mato Grosso'),
  ('BR', 'PA', 'pt', 'Pará'),
  ('BR', 'PB', 'pt', 'Paraíba'),
  ('BR', 'PE', 'pt', 'Pernambuco'),
  ('BR', 'PI', 'pt', 'Piauí'),
  ('BR', 'PR', 'pt', 'Paraná'),
  ('BR', 'RJ', 'pt', 'Rio de Janeiro'),
  ('BR', 'RN', 'pt', 'Rio Grande do Norte'),
  ('BR', 'RO', 'pt', 'Rondônia'),
  ('BR', 'RR', 'pt', 'Roraima'),
  ('BR', 'RS', 'pt', 'Rio Grande do Sul'),
  ('BR', 'SC', 'pt', 'Santa Catarina'),
  ('BR', 'SE', 'pt', 'Sergipe'),
  ('BR', 'SP', 'pt', 'São Paulo'),
  ('BR', 'TO', 'pt', 'Tocantins');

-- ISO 3166-2:BY Belarus oblasts and Minsk city in Belarusian (be) and Russian (ru).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:BY
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:BY
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('BY', 'BR', 'be', 'Bresckaja voblasć'),
  ('BY', 'BR', 'ru', 'Brestskaja oblast'''),

  ('BY', 'HO', 'be', 'Homieĺskaja voblasć'),
  ('BY', 'HO', 'ru', 'Gomel''skaja oblast'''),

  ('BY', 'HR', 'be', 'Hrodzienskaja voblasć'),
  ('BY', 'HR', 'ru', 'Grodnenskaja oblast'''),

  ('BY', 'MA', 'be', 'Mahilioŭskaja voblasć'),
  ('BY', 'MA', 'ru', 'Mogilevskaja oblast'''),

  ('BY', 'MI', 'be', 'Minskaja voblasć'),
  ('BY', 'MI', 'ru', 'Minskaja oblast'''),

  ('BY', 'VI', 'be', 'Viciebskaja voblasć'),
  ('BY', 'VI', 'ru', 'Vitebskaja oblast'''),

  ('BY', 'HM', 'be', 'Horad Minsk'),
  ('BY', 'HM', 'ru', 'Gorod Minsk');

-- ISO 3166-2:CA Canada provinces and territories in English (en) and French (fr).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:CA
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:CA
-- @see https://fr.wikipedia.org/wiki/ISO_3166-2:CA
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('CA', 'AB', 'en', 'Alberta'),
  ('CA', 'AB', 'fr', 'Alberta'),

  ('CA', 'BC', 'en', 'British Columbia'),
  ('CA', 'BC', 'fr', 'Colombie-Britannique'),

  ('CA', 'MB', 'en', 'Manitoba'),
  ('CA', 'MB', 'fr', 'Manitoba'),

  ('CA', 'NB', 'en', 'New Brunswick'),
  ('CA', 'NB', 'fr', 'Nouveau-Brunswick'),

  ('CA', 'NL', 'en', 'Newfoundland and Labrador'),
  ('CA', 'NL', 'fr', 'Terre-Neuve-et-Labrador'),

  ('CA', 'NS', 'en', 'Nova Scotia'),
  ('CA', 'NS', 'fr', 'Nouvelle-Écosse'),

  ('CA', 'NT', 'en', 'Northwest Territories'),
  ('CA', 'NT', 'fr', 'Territoires du Nord-Ouest'),

  ('CA', 'NU', 'en', 'Nunavut'),
  ('CA', 'NU', 'fr', 'Nunavut'),

  ('CA', 'ON', 'en', 'Ontario'),
  ('CA', 'ON', 'fr', 'Ontario'),

  ('CA', 'PE', 'en', 'Prince Edward Island'),
  ('CA', 'PE', 'fr', 'Île-du-Prince-Édouard'),

  ('CA', 'QC', 'en', 'Quebec'),
  ('CA', 'QC', 'fr', 'Québec'),

  ('CA', 'SK', 'en', 'Saskatchewan'),
  ('CA', 'SK', 'fr', 'Saskatchewan'),

  ('CA', 'YT', 'en', 'Yukon'),
  ('CA', 'YT', 'fr', 'Yukon');

-- ISO 3166-2:CH Switzerland cantons in German (de), French (fr), Italian (it),
-- and Romansh (rm).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:CH
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:CH
-- @see https://de.wikipedia.org/wiki/ISO_3166-2:CH
-- @see https://fr.wikipedia.org/wiki/ISO_3166-2:CH
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('CH', 'AG', 'de', 'Aargau'),
  ('CH', 'AI', 'de', 'Appenzell Innerrhoden'),
  ('CH', 'AR', 'de', 'Appenzell Ausserrhoden'),
  ('CH', 'BE', 'de', 'Bern'), 
  ('CH', 'BE', 'fr', 'Berne'),
  ('CH', 'BL', 'de', 'Basel-Landschaft'),
  ('CH', 'BS', 'de', 'Basel-Stadt'),
  ('CH', 'FR', 'de', 'Freiburg'),
  ('CH', 'FR', 'fr', 'Fribourg'),
  ('CH', 'GE', 'fr', 'Genève'),
  ('CH', 'GL', 'de', 'Glaru'),
  ('CH', 'GR', 'de', 'Graubünden'),
  ('CH', 'GR', 'it', 'Grigioni'),
  ('CH', 'GR', 'rm', 'Grischun'),
  ('CH', 'JU', 'fr', 'Jura'),
  ('CH', 'LU', 'de', 'Luzern'),
  ('CH', 'NE', 'fr', 'Neuchâtel'),
  ('CH', 'NW', 'de', 'Nidwalden'),
  ('CH', 'OW', 'de', 'Obwalden'),
  ('CH', 'SG', 'de', 'Sankt Gallen'),
  ('CH', 'SH', 'de', 'Schaffhausen'),
  ('CH', 'SO', 'de', 'Solothurn'),
  ('CH', 'SZ', 'de', 'Schwyz'),
  ('CH', 'TG', 'de', 'Thurgau'),
  ('CH', 'TI', 'it', 'Ticino'),
  ('CH', 'UR', 'de', 'Uri'),
  ('CH', 'VD', 'fr', 'Vaud'),
  ('CH', 'VS', 'de', 'Wallis'),
  ('CH', 'VS', 'fr', 'Valais'),
  ('CH', 'ZG', 'de', 'Zug'),
  ('CH', 'ZH', 'de', 'Zürich');

-- ISO 3166-2:CN China provinces, municipalities, and administrative regions
-- in Chinese (zh) and romanized pinyin Chinese (en).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:CN
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('CN', 'AH', 'en', 'Anhui Sheng'),
  ('CN', 'AH', 'zh', '安徽省 (Ānhuī Shěng)'),

  ('CN', 'BJ', 'en', 'Beijing Shi'),
  ('CN', 'BJ', 'zh', '北京市 (Běijīng Shì)'),

  ('CN', 'CQ', 'en', 'Chongqing Shi'),
  ('CN', 'CQ', 'zh', '重庆市 (Chóngqìng Shì)'),

  ('CN', 'FJ', 'en', 'Fujian Sheng'),
  ('CN', 'FJ', 'zh', '福建省 (Fújiàn Shěng)'),

  ('CN', 'GD', 'en', 'Guangdong Sheng'),
  ('CN', 'GD', 'zh', '广东省 (Guǎngdōng Shěng)'),

  ('CN', 'GS', 'en', 'Gansu Sheng'),
  ('CN', 'GS', 'zh', '甘肃省 (Gānsù Shěng)'),

  ('CN', 'GX', 'en', 'Guangxi Zhuangzu Zizhiqu'),
  ('CN', 'GX', 'zh', '广西壮族自治区 (Guǎngxī Zhuàngzú Zìzhìqū)'),

  ('CN', 'GZ', 'en', 'Guizhou Sheng'),
  ('CN', 'GZ', 'zh', '贵州省 (Guìzhōu Shěng)'),

  ('CN', 'HA', 'en', 'Henan Sheng'),
  ('CN', 'HA', 'zh', '河南省 (Hénán Shěng)'),

  ('CN', 'HB', 'en', 'Hubei Sheng'),
  ('CN', 'HB', 'zh', '湖北省 (Húběi Shěng)'),

  ('CN', 'HE', 'en', 'Hebei Sheng'),
  ('CN', 'HE', 'zh', '河北省 (Héběi Shěng)'),

  ('CN', 'HI', 'en', 'Hainan Sheng'),
  ('CN', 'HI', 'zh', '海南省 (Hǎinán Shěng)'),

  ('CN', 'HK', 'en', 'Hong Kong SAR'),
  ('CN', 'HK', 'zh', '香港特别行政区 (Xiānggǎng Tèbiéxíngzhèngqū)'),

  ('CN', 'HL', 'en', 'Heilongjiang Sheng'),
  ('CN', 'HL', 'zh', '黑龙江省 (Hēilóngjiāng Shěng)'),

  ('CN', 'HN', 'en', 'Hunan Sheng'),
  ('CN', 'HN', 'zh', '湖南省 (Húnán Shěng)'),

  ('CN', 'JL', 'en', 'Jilin Sheng'),
  ('CN', 'JL', 'zh', '吉林省 (Jílín Shěng)'),

  ('CN', 'JS', 'en', 'Jiangsu Sheng'),
  ('CN', 'JS', 'zh', '江苏省 (Jiāngsū Shěng)'),

  ('CN', 'JX', 'en', 'Jiangxi Sheng'),
  ('CN', 'JX', 'zh', '江西省 (Jiāngxī Shěng)'),

  ('CN', 'LN', 'en', 'Liaoning Sheng'),
  ('CN', 'LN', 'zh', '辽宁省 (Liáoníng Shěng)'),

  ('CN', 'MO', 'en', 'Macao SAR'),
  ('CN', 'MO', 'pt', 'Macau SAR'),
  ('CN', 'MO', 'zh', '澳门特别行政区 (Àomén Tèbiéxíngzhèngqū)'),

  ('CN', 'NM', 'en', 'Nei Mongol Zizhiqu'),
  ('CN', 'NM', 'zh', '内蒙古自治区 (Nèi Ménggǔ Zìzhìqū)'),

  ('CN', 'NX', 'en', 'Ningxia Huizi Zizhiqu'),
  ('CN', 'NX', 'zh', '宁夏回族自治区 (Níngxià Huízú Zìzhìqū)'),

  ('CN', 'QH', 'en', 'Qinghai Sheng'),
  ('CN', 'QH', 'zh', '青海省 (Qīnghǎi Shěng)'),

  ('CN', 'SC', 'en', 'Sichuan Sheng'),
  ('CN', 'SC', 'zh', '四川省 (Sìchuān Shěng)'),

  ('CN', 'SD', 'en', 'Shandong Sheng'),
  ('CN', 'SD', 'zh', '山东省 (Shāndōng Shěng)'),

  ('CN', 'SH', 'en', 'Shanghai Shi'),
  ('CN', 'SH', 'zh', '上海市 (Shànghǎi Shì)'),

  ('CN', 'SN', 'en', 'Shaanxi Sheng'),
  ('CN', 'SN', 'zh', '陕西省 (Shǎnxī Shěng)'),

  ('CN', 'SX', 'en', 'Shanxi Sheng'),
  ('CN', 'SX', 'zh', '山西省 (Shānxī Shěng)'),

  ('CN', 'TJ', 'en', 'Tianjin Shi'),
  ('CN', 'TJ', 'zh', '天津市 (Tiānjīn Shì)'),

  ('CN', 'TW', 'en', 'Taiwan Sheng'),
  ('CN', 'TW', 'zh', '台湾省 (Táiwān Shěng)'),

  ('CN', 'XJ', 'en', 'Xinjiang Uygur Zizhiqu'),
  ('CN', 'XJ', 'zh', '新疆维吾尔自治区 (Xīnjiāng Wéiwú''ěr Zìzhìqū)'),

  ('CN', 'XZ', 'en', 'Xizang Zizhiqu'),
  ('CN', 'XZ', 'zh', '西藏自治区 (Xīzàng Zìzhìqū)'),

  ('CN', 'YN', 'en', 'Yunnan Sheng'),
  ('CN', 'YN', 'zh', '云南省 (Yúnnán Shěng)'),

  ('CN', 'ZJ', 'en', 'Zhejiang Sheng'),
  ('CN', 'ZJ', 'zh', '浙江省 (Zhèjiāng Shěng)');

-- ISO 3166-2:DE Germany Bundesländer in German (de).
-- @see https://de.wikipedia.org/wiki/ISO_3166-2:DE
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:DE
-- @see https://www.iso.org/obp/ui/#iso:code:3166:DE
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('DE', 'BW', 'de', 'Baden-Württemberg'),
  ('DE', 'BY', 'de', 'Bayern'),
  ('DE', 'BE', 'de', 'Berlin'),
  ('DE', 'BB', 'de', 'Brandenburg'),
  ('DE', 'HB', 'de', 'Bremen'),
  ('DE', 'HH', 'de', 'Hamburg'),
  ('DE', 'HE', 'de', 'Hessen'),
  ('DE', 'MV', 'de', 'Mecklenburg-Vorpommern'),
  ('DE', 'NI', 'de', 'Niedersachsen'),
  ('DE', 'NW', 'de', 'Nordrhein-Westfalen'),
  ('DE', 'RP', 'de', 'Rheinland-Pfalz'),
  ('DE', 'SL', 'de', 'Saarland'),
  ('DE', 'SN', 'de', 'Sachsen'),
  ('DE', 'ST', 'de', 'Sachsen-Anhalt'),
  ('DE', 'SH', 'de', 'Schleswig-Holstein'),
  ('DE', 'TH', 'de', 'Thüringen');

-- ISO 3166-ES Spain provinces in Spanish (es), Catalan (ca), Basque (eu), and Galician (gl).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:ES
-- @see https://www.iso.org/obp/ui/#iso:code:3166:ES
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('ES', 'A', 'ca', 'Alacant'),
  ('ES', 'A', 'es', 'Alicante'),
  ('ES', 'AB', 'es', 'Albacete'),
  ('ES', 'AL', 'es', 'Almería'),
  ('ES', 'AV', 'es', 'Ávila'),
  ('ES', 'B',  'es', 'Barcelona'),
  ('ES', 'BA', 'es', 'Badajoz'),
  ('ES', 'BI', 'eu', 'Bizkaia'),
  ('ES', 'BU', 'es', 'Burgos'),
  ('ES', 'C',  'es', 'La Coruña'),
  ('ES', 'C',  'gl', 'A Coruña'),
  ('ES', 'CA', 'es', 'Cádiz'),
  ('ES', 'CC', 'es', 'Cáceres'),
  ('ES', 'CO', 'es', 'Córdoba'),
  ('ES', 'CR', 'es', 'Ciudad Real'),
  ('ES', 'CS', 'ca', 'Castelló'),
  ('ES', 'CS', 'es', 'Castellón'),
  ('ES', 'CU', 'es', 'Cuenca'),
  ('ES', 'GC', 'es', 'Las Palmas'),
  ('ES', 'GI', 'ca', 'Girona'),
  ('ES', 'GI', 'es', 'Gerona'),
  ('ES', 'GR', 'es', 'Granada'),
  ('ES', 'GU', 'es', 'Guadalajara'),
  ('ES', 'H',  'es', 'Huelva'),
  ('ES', 'HU', 'es', 'Huesca'),
  ('ES', 'J',  'es', 'Jaén'),
  ('ES', 'L',  'ca', 'Lleida'),
  ('ES', 'L',  'es', 'Lérida'),
  ('ES', 'LE', 'es', 'León'),
  ('ES', 'LO', 'es', 'La Rioja'),
  ('ES', 'LU', 'es', 'Lugo'),
  ('ES', 'LU', 'gl', 'Lugo'),
  ('ES', 'M',  'es', 'Madrid'),
  ('ES', 'MA', 'es', 'Málaga'),
  ('ES', 'MU', 'es', 'Murcia'),
  ('ES', 'NA', 'es', 'Navarra'),
  ('ES', 'NA', 'eu', 'Nafarroa'),
  ('ES', 'O',  'es', 'Asturias'),
  ('ES', 'OR', 'es', 'Orense'),
  ('ES', 'OR', 'gl', 'Ourense'),
  ('ES', 'P',  'es', 'Palencia'),
  ('ES', 'PM', 'ca', 'Illes Balears'),
  ('ES', 'PM', 'es', 'Islas Baleares'),
  ('ES', 'PO', 'es', 'Pontevedra'),
  ('ES', 'PO', 'gl', 'Pontevedra'),
  ('ES', 'S',  'es', 'Cantabria'),
  ('ES', 'SA', 'es', 'Salamanca'),
  ('ES', 'SE', 'es', 'Sevilla'),
  ('ES', 'SG', 'es', 'Segovia'),
  ('ES', 'SO', 'es', 'Soria'),
  ('ES', 'SS', 'eu', 'Gipuzkoa'),
  ('ES', 'T',  'ca', 'Tarragona'),
  ('ES', 'T',  'es', 'Tarragona'),
  ('ES', 'TE', 'es', 'Teruel'),
  ('ES', 'TF', 'es', 'Santa Cruz de Tenerife'),
  ('ES', 'TO', 'es', 'Toledo'),
  ('ES', 'V',  'ca', 'València'),
  ('ES', 'V',  'es', 'Valencia'),
  ('ES', 'VA', 'es', 'Valladolid'),
  ('ES', 'VI', 'es', 'Álava'),
  ('ES', 'VI', 'eu', 'Araba'),
  ('ES', 'Z',  'es', 'Zaragoza'),
  ('ES', 'ZA', 'es', 'Zamora');

-- ISO 3166-2:ID Indonesia provinces and regions in Indonesian (id) and English (en).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:ID
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('ID', 'AC', 'id', 'Aceh'),
  ('ID', 'AC', 'en', 'Aceh'),

  ('ID', 'BA', 'id', 'Bali'),
  ('ID', 'BA', 'en', 'Bali'),

  ('ID', 'BB', 'id', 'Kepulauan Bangka Belitung'),
  ('ID', 'BB', 'en', 'Bangka Belitung Islands'),

  ('ID', 'BE', 'id', 'Bengkulu'),
  ('ID', 'BE', 'en', 'Bengkulu'),

  ('ID', 'BT', 'id', 'Banten'),
  ('ID', 'BT', 'en', 'Banten'),

  ('ID', 'GO', 'id', 'Gorontalo'),
  ('ID', 'GO', 'en', 'Gorontalo'),

  ('ID', 'JA', 'id', 'Jambi'),
  ('ID', 'JA', 'en', 'Jambi'),

  ('ID', 'JB', 'id', 'Jawa Barat'),
  ('ID', 'JB', 'en', 'West Java'),

  ('ID', 'JI', 'id', 'Jawa Timur'),
  ('ID', 'JI', 'en', 'East Java'),

  ('ID', 'JK', 'id', 'Jakarta Raya'),
  ('ID', 'JK', 'en', 'Jakarta'),

  ('ID', 'JT', 'id', 'Jawa Tengah'),
  ('ID', 'JT', 'en', 'Central Java'),

  ('ID', 'KB', 'id', 'Kalimantan Barat'),
  ('ID', 'KB', 'en', 'West Kalimantan'),

  ('ID', 'KI', 'id', 'Kalimantan Timur'),
  ('ID', 'KI', 'en', 'East Kalimantan'),

  ('ID', 'KR', 'id', 'Kepulauan Riau'),
  ('ID', 'KR', 'en', 'Riau Islands'),

  ('ID', 'KS', 'id', 'Kalimantan Selatan'),
  ('ID', 'KS', 'en', 'South Kalimantan'),

  ('ID', 'KT', 'id', 'Kalimantan Tengah'),
  ('ID', 'KT', 'en', 'Central Kalimantan'),

  ('ID', 'KU', 'id', 'Kalimantan Utara'),
  ('ID', 'KU', 'en', 'North Kalimantan'),

  ('ID', 'LA', 'id', 'Lampung'),
  ('ID', 'LA', 'en', 'Lampung'),

  ('ID', 'MA', 'id', 'Maluku'),
  ('ID', 'MA', 'en', 'Maluku'),

  ('ID', 'MU', 'id', 'Maluku Utara'),
  ('ID', 'MU', 'en', 'North Maluku'),

  ('ID', 'NB', 'id', 'Nusa Tenggara Barat'),
  ('ID', 'NB', 'en', 'West Nusa Tenggara'),

  ('ID', 'NT', 'id', 'Nusa Tenggara Timur'),
  ('ID', 'NT', 'en', 'East Nusa Tenggara'),

  ('ID', 'PA', 'id', 'Papua'),
  ('ID', 'PA', 'en', 'Papua'),

  ('ID', 'PB', 'id', 'Papua Barat'),
  ('ID', 'PB', 'en', 'West Papua'),

  ('ID', 'RI', 'id', 'Riau'),
  ('ID', 'RI', 'en', 'Riau'),

  ('ID', 'SA', 'id', 'Sulawesi Utara'),
  ('ID', 'SA', 'en', 'North Sulawesi'),

  ('ID', 'SB', 'id', 'Sumatera Barat'),
  ('ID', 'SB', 'en', 'West Sumatra'),

  ('ID', 'SG', 'id', 'Sulawesi Tenggara'),
  ('ID', 'SG', 'en', 'Southeast Sulawesi'),

  ('ID', 'SN', 'id', 'Sulawesi Selatan'),
  ('ID', 'SN', 'en', 'South Sulawesi'),

  ('ID', 'SR', 'id', 'Sulawesi Barat'),
  ('ID', 'SR', 'en', 'West Sulawesi'),

  ('ID', 'SS', 'id', 'Sumatera Selatan'),
  ('ID', 'SS', 'en', 'South Sumatra'),

  ('ID', 'ST', 'id', 'Sulawesi Tengah'),
  ('ID', 'ST', 'en', 'Central Sulawesi'),

  ('ID', 'SU', 'id', 'Sumatera Utara'),
  ('ID', 'SU', 'en', 'North Sumatra'),

  ('ID', 'YO', 'id', 'Yogyakarta'),
  ('ID', 'YO', 'en', 'Yogyakarta');

-- ISO 3166-2:IE Ireland counties in English (en) and Irish (ga).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:IE
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('IE', 'CE', 'en', 'Clare'),
  ('IE', 'CE', 'ga', 'An Clár'),

  ('IE', 'CN', 'en', 'Cavan'),
  ('IE', 'CN', 'ga', 'An Cabhán'),

  ('IE', 'CO', 'en', 'Cork'),
  ('IE', 'CO', 'ga', 'Corcaigh'),

  ('IE', 'CW', 'en', 'Carlow'),
  ('IE', 'CW', 'ga', 'Ceatharlach'),

  ('IE', 'D',  'en', 'Dublin'),
  ('IE', 'D',  'ga', 'Baile Átha Cliath'),

  ('IE', 'DL', 'en', 'Donegal'),
  ('IE', 'DL', 'ga', 'Dún na nGall'),

  ('IE', 'G',  'en', 'Galway'),
  ('IE', 'G',  'ga', 'Gaillimh'),

  ('IE', 'KE', 'en', 'Kildare'),
  ('IE', 'KE', 'ga', 'Cill Dara'),

  ('IE', 'KK', 'en', 'Kilkenny'),
  ('IE', 'KK', 'ga', 'Cill Chainnigh'),

  ('IE', 'KY', 'en', 'Kerry'),
  ('IE', 'KY', 'ga', 'Ciarraí'),

  ('IE', 'LD', 'en', 'Longford'),
  ('IE', 'LD', 'ga', 'An Longfort'),

  ('IE', 'LH', 'en', 'Louth'),
  ('IE', 'LH', 'ga', 'Lú'),

  ('IE', 'LK', 'en', 'Limerick'),
  ('IE', 'LK', 'ga', 'Luimneach'),

  ('IE', 'LM', 'en', 'Leitrim'),
  ('IE', 'LM', 'ga', 'Liatroim'),

  ('IE', 'LS', 'en', 'Laois'),
  ('IE', 'LS', 'ga', 'Laois'),

  ('IE', 'MH', 'en', 'Meath'),
  ('IE', 'MH', 'ga', 'An Mhí'),

  ('IE', 'MN', 'en', 'Monaghan'),
  ('IE', 'MN', 'ga', 'Muineachán'),

  ('IE', 'MO', 'en', 'Mayo'),
  ('IE', 'MO', 'ga', 'Maigh Eo'),

  ('IE', 'OY', 'en', 'Offaly'),
  ('IE', 'OY', 'ga', 'Uíbh Fhailí'),

  ('IE', 'RN', 'en', 'Roscommon'),
  ('IE', 'RN', 'ga', 'Ros Comáin'),

  ('IE', 'SO', 'en', 'Sligo'),
  ('IE', 'SO', 'ga', 'Sligeach'),

  ('IE', 'TA', 'en', 'Tipperary'),
  ('IE', 'TA', 'ga', 'Tiobraid Árann'),

  ('IE', 'WD', 'en', 'Waterford'),
  ('IE', 'WD', 'ga', 'Port Láirge'),

  ('IE', 'WH', 'en', 'Westmeath'),
  ('IE', 'WH', 'ga', 'An Iarmhí'),

  ('IE', 'WW', 'en', 'Wicklow'),
  ('IE', 'WW', 'ga', 'Cill Mhantáin'),

  ('IE', 'WX', 'en', 'Wexford'),
  ('IE', 'WX', 'ga', 'Loch Garman');

-- ISO 3166-2:IN India states and union territories in Indian transliteration (en).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:IN
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('IN', 'AN', 'en', 'Andaman and Nicobar Islands'),
  ('IN', 'AP', 'en', 'Andhra Pradesh'),
  ('IN', 'AR', 'en', 'Arunāchal Pradesh'),
  ('IN', 'AS', 'en', 'Assam'),
  ('IN', 'BR', 'en', 'Bihār'),
  ('IN', 'CH', 'en', 'Chandigarh'),
  ('IN', 'CT', 'en', 'Chhattīsgarh'),
  ('IN', 'DH', 'en', 'Dādra and Nagar Haveli and Damān and Diu'),
  ('IN', 'DL', 'en', 'Delhi'),
  ('IN', 'GA', 'en', 'Goa'),
  ('IN', 'GJ', 'en', 'Gujarāt'),
  ('IN', 'HP', 'en', 'Himāchal Pradesh'),
  ('IN', 'HR', 'en', 'Haryāna'),
  ('IN', 'JH', 'en', 'Jhārkhand'),
  ('IN', 'JK', 'en', 'Jammu and Kashmīr'),
  ('IN', 'KA', 'en', 'Karnātaka'),
  ('IN', 'KL', 'en', 'Kerala'),
  ('IN', 'LA', 'en', 'Ladākh'),
  ('IN', 'LD', 'en', 'Lakshadweep'),
  ('IN', 'MH', 'en', 'Mahārāshtra'),
  ('IN', 'ML', 'en', 'Meghālaya'),
  ('IN', 'MN', 'en', 'Manipur'),
  ('IN', 'MP', 'en', 'Madhya Pradesh'),
  ('IN', 'MZ', 'en', 'Mizoram'),
  ('IN', 'NL', 'en', 'Nāgāland'),
  ('IN', 'OR', 'en', 'Odisha'),
  ('IN', 'PB', 'en', 'Punjab'),
  ('IN', 'PY', 'en', 'Puducherry'),
  ('IN', 'RJ', 'en', 'Rājasthān'),
  ('IN', 'SK', 'en', 'Sikkim'),
  ('IN', 'TG', 'en', 'Telangāna'),
  ('IN', 'TN', 'en', 'Tamil Nādu'),
  ('IN', 'TR', 'en', 'Tripura'),
  ('IN', 'UP', 'en', 'Uttar Pradesh'),
  ('IN', 'UT', 'en', 'Uttarākhand'),
  ('IN', 'WB', 'en', 'West Bengal');

-- ISO 3166-2:IQ Iraq governorates in romanized Arabic (ar) and Kurdish (ku).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:IQ
-- @see https://www.iso.org/obp/ui/#iso:code:3166:IQ
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('IQ', 'AN', 'ar', 'Al Anbār'),
  ('IQ', 'AR', 'ar', 'Arbīl'),
  ('IQ', 'AR', 'ku', 'Hewlêr'),
  ('IQ', 'BA', 'ar', 'Al Başrah'),
  ('IQ', 'BB', 'ar', 'Bābil'),
  ('IQ', 'BG', 'ar', 'Baghdād'),
  ('IQ', 'DA', 'ar', 'Dahūk'),
  ('IQ', 'DA', 'ku', 'Dihok'),
  ('IQ', 'DI', 'ar', 'Diyālá'),
  ('IQ', 'DQ', 'ar', 'Dhī Qār'),
  ('IQ', 'KA', 'ar', 'Karbalā’'),
  ('IQ', 'KI', 'ar', 'Kirkūk'),
  ('IQ', 'MA', 'ar', 'Maysān'),
  ('IQ', 'MU', 'ar', 'Al Muthanná'),
  ('IQ', 'NA', 'ar', 'An Najaf'),
  ('IQ', 'NI', 'ar', 'Nīnawá'),
  ('IQ', 'QA', 'ar', 'Al Qādisīyah'),
  ('IQ', 'SD', 'ar', 'Şalāḩ ad Dīn'),
  ('IQ', 'SU', 'ar', 'As Sulaymānīyah'),
  ('IQ', 'SU', 'ku', 'Slêmanî'),
  ('IQ', 'WA', 'ar', 'Wāsiţ');

-- ISO 3166-2:IR Iran provinces in romanized Farsi (fa).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:IR
-- @see https://www.iso.org/obp/ui/#iso:code:3166:IR
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('IR', '00', 'fa', 'Markazī'),
  ('IR', '01', 'fa', 'Gīlān'),
  ('IR', '02', 'fa', 'Māzandarān'),
  ('IR', '03', 'fa', 'Āz̄ārbāyjān-e Shārqī'),
  ('IR', '04', 'fa', 'Āz̄ārbāyjān-e Ghārbī'),
  ('IR', '05', 'fa', 'Kermānshāh'),
  ('IR', '06', 'fa', 'Khūzestān'),
  ('IR', '07', 'fa', 'Fārs'),
  ('IR', '08', 'fa', 'Kermān'),
  ('IR', '09', 'fa', 'Khorāsān-e Raẕavī'),
  ('IR', '10', 'fa', 'Eşfahān'),
  ('IR', '11', 'fa', 'Sīstān va Balūchestān'),
  ('IR', '12', 'fa', 'Kordestān'),
  ('IR', '13', 'fa', 'Hamadān'),
  ('IR', '14', 'fa', 'Chahār Maḩāl va Bakhtīārī'),
  ('IR', '15', 'fa', 'Lorestān'),
  ('IR', '16', 'fa', 'Īlām'),
  ('IR', '17', 'fa', 'Kohgīlūyeh va Bowyer Aḩmad'),
  ('IR', '18', 'fa', 'Būshehr'),
  ('IR', '19', 'fa', 'Zanjān'),
  ('IR', '20', 'fa', 'Semnān'),
  ('IR', '21', 'fa', 'Yazd'),
  ('IR', '22', 'fa', 'Hormozgān'),
  ('IR', '23', 'fa', 'Tehrān'),
  ('IR', '24', 'fa', 'Ardabīl'),
  ('IR', '25', 'fa', 'Qom'),
  ('IR', '26', 'fa', 'Qazvīn'),
  ('IR', '27', 'fa', 'Golestān'),
  ('IR', '28', 'fa', 'Khorāsān-e Shomālī'),
  ('IR', '29', 'fa', 'Khorāsān-e Jonūbī'),
  ('IR', '30', 'fa', 'Alborz');

-- ISO 3166-2:IT Italy provinces in Italian (it) and German (de).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:IT#Provinces
-- @see https://www.iso.org/obp/ui/#iso:code:3166:IT
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('IT', 'AG', 'it', 'Agrigento'),
  ('IT', 'AL', 'it', 'Alessandria'),
  ('IT', 'AN', 'it', 'Ancona'),
  ('IT', 'AP', 'it', 'Ascoli Piceno'),
  ('IT', 'AQ', 'it', 'L’Aquila'),
  ('IT', 'AR', 'it', 'Arezzo'),
  ('IT', 'AT', 'it', 'Asti'),
  ('IT', 'AV', 'it', 'Avellino'),
  ('IT', 'BA', 'it', 'Bari'),
  ('IT', 'BG', 'it', 'Bergamo'),
  ('IT', 'BI', 'it', 'Biella'),
  ('IT', 'BL', 'it', 'Belluno'),
  ('IT', 'BN', 'it', 'Benevento'),
  ('IT', 'BO', 'it', 'Bologna'),
  ('IT', 'BR', 'it', 'Brindisi'),
  ('IT', 'BS', 'it', 'Brescia'),
  ('IT', 'BT', 'it', 'Barletta-Andria-Trani'),

  ('IT', 'BZ', 'de', 'Bozen'),
  ('IT', 'BZ', 'it', 'Bolzano'),

  ('IT', 'CA', 'it', 'Cagliari'),
  ('IT', 'CB', 'it', 'Campobasso'),
  ('IT', 'CE', 'it', 'Caserta'),
  ('IT', 'CH', 'it', 'Chieti'),
  ('IT', 'CL', 'it', 'Caltanissetta'),
  ('IT', 'CN', 'it', 'Cuneo'),
  ('IT', 'CO', 'it', 'Como'),
  ('IT', 'CR', 'it', 'Cremona'),
  ('IT', 'CS', 'it', 'Cosenza'),
  ('IT', 'CT', 'it', 'Catania'),
  ('IT', 'CZ', 'it', 'Catanzaro'),
  ('IT', 'EN', 'it', 'Enna'),
  ('IT', 'FC', 'it', 'Forlì-Cesena'),
  ('IT', 'FE', 'it', 'Ferrara'),
  ('IT', 'FG', 'it', 'Foggia'),
  ('IT', 'FI', 'it', 'Firenze'),
  ('IT', 'FM', 'it', 'Fermo'),
  ('IT', 'FR', 'it', 'Frosinone'),
  ('IT', 'GE', 'it', 'Genova'),
  ('IT', 'GO', 'it', 'Gorizia'),
  ('IT', 'GR', 'it', 'Grosseto'),
  ('IT', 'IM', 'it', 'Imperia'),
  ('IT', 'IS', 'it', 'Isernia'),
  ('IT', 'KR', 'it', 'Crotone'),
  ('IT', 'LC', 'it', 'Lecco'),
  ('IT', 'LE', 'it', 'Lecce'),
  ('IT', 'LI', 'it', 'Livorno'),
  ('IT', 'LO', 'it', 'Lodi'),
  ('IT', 'LT', 'it', 'Latina'),
  ('IT', 'LU', 'it', 'Lucca'),
  ('IT', 'MB', 'it', 'Monza e Brianza'),
  ('IT', 'MC', 'it', 'Macerata'),
  ('IT', 'ME', 'it', 'Messina'),
  ('IT', 'MI', 'it', 'Milano'),
  ('IT', 'MN', 'it', 'Mantova'),
  ('IT', 'MO', 'it', 'Modena'),
  ('IT', 'MS', 'it', 'Massa-Carrara'),
  ('IT', 'MT', 'it', 'Matera'),
  ('IT', 'NA', 'it', 'Napoli'),
  ('IT', 'NO', 'it', 'Novara'),
  ('IT', 'NU', 'it', 'Nuoro'),
  ('IT', 'OR', 'it', 'Oristano'),
  ('IT', 'PA', 'it', 'Palermo'),
  ('IT', 'PC', 'it', 'Piacenza'),
  ('IT', 'PD', 'it', 'Padova'),
  ('IT', 'PE', 'it', 'Pescara'),
  ('IT', 'PG', 'it', 'Perugia'),
  ('IT', 'PI', 'it', 'Pisa'),
  ('IT', 'PN', 'it', 'Pordenone'),
  ('IT', 'PO', 'it', 'Prato'),
  ('IT', 'PR', 'it', 'Parma'),
  ('IT', 'PT', 'it', 'Pistoia'),
  ('IT', 'PU', 'it', 'Pesaro e Urbino'),
  ('IT', 'PV', 'it', 'Pavia'),
  ('IT', 'PZ', 'it', 'Potenza'),
  ('IT', 'RA', 'it', 'Ravenna'),
  ('IT', 'RC', 'it', 'Reggio Calabria'),
  ('IT', 'RE', 'it', 'Reggio Emilia'),
  ('IT', 'RG', 'it', 'Ragusa'),
  ('IT', 'RI', 'it', 'Rieti'),
  ('IT', 'RM', 'it', 'Roma'),
  ('IT', 'RN', 'it', 'Rimini'),
  ('IT', 'RO', 'it', 'Rovigo'),
  ('IT', 'SA', 'it', 'Salerno'),
  ('IT', 'SI', 'it', 'Siena'),
  ('IT', 'SO', 'it', 'Sondrio'),
  ('IT', 'SP', 'it', 'La Spezia'),
  ('IT', 'SR', 'it', 'Siracusa'),
  ('IT', 'SS', 'it', 'Sassari'),
  ('IT', 'SU', 'it', 'Sud Sardegna'),
  ('IT', 'SV', 'it', 'Savona'),
  ('IT', 'TA', 'it', 'Taranto'),
  ('IT', 'TE', 'it', 'Teramo'),
  ('IT', 'TN', 'it', 'Trento'),
  ('IT', 'TO', 'it', 'Torino'),
  ('IT', 'TP', 'it', 'Trapani'),
  ('IT', 'TR', 'it', 'Terni'),
  ('IT', 'TS', 'it', 'Trieste'),
  ('IT', 'TV', 'it', 'Treviso'),
  ('IT', 'UD', 'it', 'Udine'),
  ('IT', 'VA', 'it', 'Varese'),
  ('IT', 'VB', 'it', 'Verbano-Cusio-Ossola'),
  ('IT', 'VC', 'it', 'Vercelli'),
  ('IT', 'VE', 'it', 'Venezia'),
  ('IT', 'VI', 'it', 'Vicenza'),
  ('IT', 'VR', 'it', 'Verona'),
  ('IT', 'VT', 'it', 'Viterbo'),
  ('IT', 'VV', 'it', 'Vibo Valentia');

-- ISO 3166-2:JP Japan in romanized Japanese (ja) and English (en).
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:JP
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('JP', '01', 'en', 'Hokkaido'),
  ('JP', '01', 'ja', 'Hokkaidô'),
  ('JP', '02', 'ja', 'Aomori'),
  ('JP', '03', 'ja', 'Iwate'),
  ('JP', '04', 'ja', 'Miyagi'),
  ('JP', '05', 'ja', 'Akita'),
  ('JP', '06', 'ja', 'Yamagata'),
  ('JP', '07', 'en', 'Fukushima'),
  ('JP', '07', 'ja', 'Hukusima'),
  ('JP', '08', 'ja', 'Ibaraki'),
  ('JP', '09', 'en', 'Tochigi'),
  ('JP', '09', 'ja', 'Totigi'),
  ('JP', '10', 'ja', 'Gunma'),
  ('JP', '11', 'ja', 'Saitama'),
  ('JP', '12', 'en', 'Chiba'),
  ('JP', '12', 'ja', 'Tiba'),
  ('JP', '13', 'en', 'Tokyo'),
  ('JP', '13', 'ja', 'Tôkyô'),
  ('JP', '14', 'ja', 'Kanagawa'),
  ('JP', '15', 'ja', 'Niigata'),
  ('JP', '16', 'ja', 'Toyama'),
  ('JP', '17', 'en', 'Ishikawa'),
  ('JP', '17', 'ja', 'Isikawa'),
  ('JP', '18', 'en', 'Fukui'),
  ('JP', '18', 'ja', 'Hukui'),
  ('JP', '19', 'en', 'Yamanashi'),
  ('JP', '19', 'ja', 'Yamanasi'),
  ('JP', '20', 'ja', 'Nagano'),
  ('JP', '21', 'en', 'Gifu'),
  ('JP', '21', 'ja', 'Gihu'),
  ('JP', '22', 'en', 'Shizuoka'),
  ('JP', '22', 'ja', 'Sizuoka'),
  ('JP', '23', 'en', 'Aichi'),
  ('JP', '23', 'ja', 'Aiti'),
  ('JP', '24', 'ja', 'Mie'),
  ('JP', '25', 'en', 'Shiga'),
  ('JP', '25', 'ja', 'Siga'),
  ('JP', '26', 'en', 'Kyoto'),
  ('JP', '26', 'ja', 'Kyôto'),
  ('JP', '27', 'en', 'Osaka'),
  ('JP', '27', 'ja', 'Ôsaka'),
  ('JP', '28', 'en', 'Hyogo'),
  ('JP', '28', 'ja', 'Hyôgo'),
  ('JP', '29', 'ja', 'Nara'),
  ('JP', '30', 'ja', 'Wakayama'),
  ('JP', '31', 'ja', 'Tottori'),
  ('JP', '32', 'en', 'Shimane'),
  ('JP', '32', 'ja', 'Simane'),
  ('JP', '33', 'ja', 'Okayama'),
  ('JP', '34', 'en', 'Hiroshima'),
  ('JP', '34', 'ja', 'Hirosima'),
  ('JP', '35', 'en', 'Yamaguchi'),
  ('JP', '35', 'ja', 'Yamaguti'),
  ('JP', '36', 'en', 'Tokushima'),
  ('JP', '36', 'ja', 'Tokusima'),
  ('JP', '37', 'ja', 'Kagawa'),
  ('JP', '38', 'ja', 'Ehime'),
  ('JP', '39', 'en', 'Kochi'),
  ('JP', '39', 'ja', 'Kôti'),
  ('JP', '40', 'en', 'Fukuoka'),
  ('JP', '40', 'ja', 'Hukuoka'),
  ('JP', '41', 'ja', 'Saga'),
  ('JP', '42', 'ja', 'Nagasaki'),
  ('JP', '43', 'ja', 'Kumamoto'),
  ('JP', '44', 'en', 'Oita'),
  ('JP', '44', 'ja', 'Ôita'),
  ('JP', '45', 'ja', 'Miyazaki'),
  ('JP', '46', 'en', 'Kagoshima'),
  ('JP', '46', 'ja', 'Kagosima'),
  ('JP', '47', 'ja', 'Okinawa');

-- ISO 3166-2:KR South Korea provinces and metropolitan cities in English (en)
-- and romanized Korean (ko).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:KR
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:KR
-- @see https://en.wikipedia.org/wiki/Addresses_in_South_Korea
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('KR', '11', 'en', 'Seoul'),
  ('KR', '11', 'ko', 'Seoul-teukbyeolsi'),

  ('KR', '26', 'en', 'Busan'),
  ('KR', '26', 'ko', 'Busan-gwangyeoksi'),

  ('KR', '27', 'en', 'Daegu'),
  ('KR', '27', 'ko', 'Daegu-gwangyeoksi'),

  ('KR', '28', 'en', 'Incheon'),
  ('KR', '28', 'ko', 'Incheon-gwangyeoksi'),

  ('KR', '29', 'en', 'Gwangju'),
  ('KR', '29', 'ko', 'Gwangju-gwangyeoksi'),

  ('KR', '30', 'en', 'Daejeon'),
  ('KR', '30', 'ko', 'Daejeon-gwangyeoksi'),

  ('KR', '31', 'en', 'Ulsan'),
  ('KR', '31', 'ko', 'Ulsan-gwangyeoksi'),

  ('KR', '41', 'en', 'Gyeonggi'),
  ('KR', '41', 'ko', 'Gyeonggi-do'),

  ('KR', '42', 'en', 'Gangwon'),
  ('KR', '42', 'ko', 'Gangwon-do'),

  ('KR', '43', 'en', 'North Chungcheong'),
  ('KR', '43', 'ko', 'Chungcheongbuk-do'),

  ('KR', '44', 'en', 'South Chungcheong'),
  ('KR', '44', 'ko', 'Chungcheongnam-do'),

  ('KR', '45', 'en', 'North Jeolla'),
  ('KR', '45', 'ko', 'Jeollabuk-do'),

  ('KR', '46', 'en', 'South Jeolla'),
  ('KR', '46', 'ko', 'Jeollanam-do'),

  ('KR', '47', 'en', 'North Gyeongsang'),
  ('KR', '47', 'ko', 'Gyeongsangbuk-do'),

  ('KR', '48', 'en', 'South Gyeongsang'),
  ('KR', '48', 'ko', 'Gyeongsangnam-do'),

  ('KR', '49', 'en', 'Jeju'),
  ('KR', '49', 'ko', 'Jeju-teukbyeoljachido'),

  ('KR', '50', 'en', 'Sejong'),
  ('KR', '50', 'ko', 'Sejong-teukbyeoljachisi');


-- ISO 3166-2:LU Luxembourg cantons in German (de), English (en),
-- French (fr), and Luxembourgish (lb).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:LU
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:LU
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('LU', 'CA', 'de', 'Capellen'),
  ('LU', 'CA', 'en', 'Capellen'),
  ('LU', 'CA', 'fr', 'Capellen'),
  ('LU', 'CA', 'lb', 'Kapellen'),

  ('LU', 'CL', 'de', 'Clerf'),
  ('LU', 'CL', 'en', 'Clervaux'),
  ('LU', 'CL', 'fr', 'Clervaux'),
  ('LU', 'CL', 'lb', 'Klierf'),

  ('LU', 'DI', 'de', 'Diekirch'),
  ('LU', 'DI', 'en', 'Diekirch'),
  ('LU', 'DI', 'fr', 'Diekirch'),
  ('LU', 'DI', 'lb', 'Diekrech'),

  ('LU', 'EC', 'de', 'Echternach'),
  ('LU', 'EC', 'en', 'Echternach'),
  ('LU', 'EC', 'fr', 'Echternach'),
  ('LU', 'EC', 'lb', 'Iechternach'),

  ('LU', 'ES', 'de', 'Esch an der Alzette'),
  ('LU', 'ES', 'en', 'Esch-sur-Alzette'),
  ('LU', 'ES', 'fr', 'Esch-sur-Alzette'),
  ('LU', 'ES', 'lb', 'Esch-Uelzecht'),

  ('LU', 'GR', 'de', 'Grevenmacher'),
  ('LU', 'GR', 'en', 'Grevenmacher'),
  ('LU', 'GR', 'fr', 'Grevenmacher'),
  ('LU', 'GR', 'lb', 'Gréivemaacher'),

  ('LU', 'LU', 'de', 'Luxemburg'),
  ('LU', 'LU', 'en', 'Luxembourg'),
  ('LU', 'LU', 'fr', 'Luxembourg'),
  ('LU', 'LU', 'lb', 'Lëtzebuerg'),

  ('LU', 'ME', 'de', 'Mersch'),
  ('LU', 'ME', 'en', 'Mersch'),
  ('LU', 'ME', 'fr', 'Mersch'),
  ('LU', 'ME', 'lb', 'Miersch'),

  ('LU', 'RD', 'de', 'Redingen'),
  ('LU', 'RD', 'en', 'Redange'),
  ('LU', 'RD', 'fr', 'Redange'),
  ('LU', 'RD', 'lb', 'Réiden-Atert'),

  ('LU', 'RM', 'de', 'Remich'),
  ('LU', 'RM', 'en', 'Remich'),
  ('LU', 'RM', 'fr', 'Remich'),
  ('LU', 'RM', 'lb', 'Réimech'),

  ('LU', 'VD', 'de', 'Vianden'),
  ('LU', 'VD', 'en', 'Vianden'),
  ('LU', 'VD', 'fr', 'Vianden'),
  ('LU', 'VD', 'lb', 'Veianen'),

  ('LU', 'WI', 'de', 'Wiltz'),
  ('LU', 'WI', 'en', 'Wiltz'),
  ('LU', 'WI', 'fr', 'Wiltz'),
  ('LU', 'WI', 'lb', 'Wolz');

-- ISO 3166-2:NL The Netherlands provinces in Dutch (nl) and West Frisian (fy).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:NL
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:NL
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('NL', 'DR', 'nl', 'Drenthe'),
  ('NL', 'FL', 'nl', 'Flevoland'),
  ('NL', 'FR', 'nl', 'Friesland'),
  ('NL', 'FR', 'fy', 'Fryslân'),
  ('NL', 'GE', 'nl', 'Gelderland'),
  ('NL', 'GR', 'nl', 'Groningen'),
  ('NL', 'LI', 'nl', 'Limburg'),
  ('NL', 'NB', 'nl', 'Noord-Brabant'),
  ('NL', 'NH', 'nl', 'Noord-Holland'),
  ('NL', 'OV', 'nl', 'Overijssel'),
  ('NL', 'UT', 'nl', 'Utrecht'),
  ('NL', 'ZE', 'nl', 'Zeeland'),
  ('NL', 'ZH', 'nl', 'Zuid-Holland');

-- ISO 3166-2:PE Peru in Aymara (ay), Spanish (es), and Quechua (qu).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:PE
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:PE
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('PE', 'AMA', 'ay', 'Amasunu'),
  ('PE', 'AMA', 'es', 'Amazonas'),
  ('PE', 'AMA', 'qu', 'Amarumayu'),

  ('PE', 'ANC', 'ay', 'Ankashu'),
  ('PE', 'ANC', 'es', 'Ancash'),
  ('PE', 'ANC', 'qu', 'Anqash'),

  ('PE', 'APU', 'ay', 'Apurimaq'),
  ('PE', 'APU', 'es', 'Apurímac'),
  ('PE', 'APU', 'qu', 'Apurimaq'),

  ('PE', 'ARE', 'ay', 'Arikipa'),
  ('PE', 'ARE', 'es', 'Arequipa'),
  ('PE', 'ARE', 'qu', 'Ariqipa'),

  ('PE', 'AYA', 'ay', 'Ayaquchu'),
  ('PE', 'AYA', 'es', 'Ayacucho'),
  ('PE', 'AYA', 'qu', 'Ayakuchu'),

  ('PE', 'CAJ', 'ay', 'Qajamarka'),
  ('PE', 'CAJ', 'es', 'Cajamarca'),
  ('PE', 'CAJ', 'qu', 'Kashamarka'),

  ('PE', 'CAL', 'ay', 'Kallao'),
  ('PE', 'CAL', 'es', 'El Callao'),
  ('PE', 'CAL', 'qu', 'Qallaw'),

  ('PE', 'CUS', 'ay', 'Kusku'),
  ('PE', 'CUS', 'es', 'Cusco'),
  ('PE', 'CUS', 'qu', 'Qusqu'),

  ('PE', 'HUC', 'ay', 'Wanuku'),
  ('PE', 'HUC', 'es', 'Huánuco'),
  ('PE', 'HUC', 'qu', 'Wanuku'),

  ('PE', 'HUV', 'ay', 'Wankawelika'),
  ('PE', 'HUV', 'es', 'Huancavelica'),
  ('PE', 'HUV', 'qu', 'Wankawillka'),

  ('PE', 'ICA', 'ay', 'Ika'),
  ('PE', 'ICA', 'es', 'Ica'),
  ('PE', 'ICA', 'qu', 'Ika'),

  ('PE', 'JUN', 'ay', 'Junin'),
  ('PE', 'JUN', 'es', 'Junín'),
  ('PE', 'JUN', 'qu', 'Hunin'),

  ('PE', 'LAL', 'ay', 'La Libertad'),
  ('PE', 'LAL', 'es', 'La Libertad'),
  ('PE', 'LAL', 'qu', 'Qispi kay'),

  ('PE', 'LAM', 'ay', 'Lambayeque'),
  ('PE', 'LAM', 'es', 'Lambayeque'),
  ('PE', 'LAM', 'qu', 'Lampalliqi'),

  ('PE', 'LIM', 'ay', 'Lima'),
  ('PE', 'LIM', 'es', 'Lima'),
  ('PE', 'LIM', 'qu', 'Lima'),

  ('PE', 'LMA', 'ay', 'Lima hatun llaqta'),
  ('PE', 'LMA', 'es', 'Municipalidad Metropolitana de Lima'),
  ('PE', 'LMA', 'qu', 'Lima llaqta suyu'),

  ('PE', 'LOR', 'ay', 'Luritu'),
  ('PE', 'LOR', 'es', 'Loreto'),
  ('PE', 'LOR', 'qu', 'Luritu'),

  ('PE', 'MDD', 'ay', 'Madre de Dios'),
  ('PE', 'MDD', 'es', 'Madre de Dios'),
  ('PE', 'MDD', 'qu', 'Mayutata'),

  ('PE', 'MOQ', 'ay', 'Moqwegwa'),
  ('PE', 'MOQ', 'es', 'Moquegua'),
  ('PE', 'MOQ', 'qu', 'Muqiwa'),

  ('PE', 'PAS', 'ay', 'Pasqu'),
  ('PE', 'PAS', 'es', 'Pasco'),
  ('PE', 'PAS', 'qu', 'Pasqu'),

  ('PE', 'PIU', 'ay', 'Piura'),
  ('PE', 'PIU', 'es', 'Piura'),
  ('PE', 'PIU', 'qu', 'Piwra'),

  ('PE', 'PUN', 'ay', 'Puno'),
  ('PE', 'PUN', 'es', 'Puno'),
  ('PE', 'PUN', 'qu', 'Punu'),

  ('PE', 'SAM', 'ay', 'San Martín'),
  ('PE', 'SAM', 'es', 'San Martín'),
  ('PE', 'SAM', 'qu', 'San Martin'),

  ('PE', 'TAC', 'ay', 'Takna'),
  ('PE', 'TAC', 'es', 'Tacna'),
  ('PE', 'TAC', 'qu', 'Taqna'),

  ('PE', 'TUM', 'ay', 'Tumbes'),
  ('PE', 'TUM', 'es', 'Tumbes'),
  ('PE', 'TUM', 'qu', 'Tumpis'),

  ('PE', 'UCA', 'ay', 'Ukayali'),
  ('PE', 'UCA', 'es', 'Ucayali'),
  ('PE', 'UCA', 'qu', 'Ukayali');

-- ISO 3166-2:PH Philippines provinces in English (en) and Tagalog (tl).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:PH
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:PH
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('PH', 'ABR', 'en', 'Abra'),
  ('PH', 'ABR', 'tl', 'Abra'),

  ('PH', 'AGN', 'en', 'Agusan del Norte'),
  ('PH', 'AGN', 'tl', 'Hilagang Agusan'),

  ('PH', 'AGS', 'en', 'Agusan del Sur'),
  ('PH', 'AGS', 'tl', 'Timog Agusan'),

  ('PH', 'AKL', 'en', 'Aklan'),
  ('PH', 'AKL', 'tl', 'Aklan'),

  ('PH', 'ALB', 'en', 'Albay'),
  ('PH', 'ALB', 'tl', 'Albay'),

  ('PH', 'ANT', 'en', 'Antique'),
  ('PH', 'ANT', 'tl', 'Antike'),

  ('PH', 'APA', 'en', 'Apayao'),
  ('PH', 'APA', 'tl', 'Apayaw'),

  ('PH', 'AUR', 'en', 'Aurora'),
  ('PH', 'AUR', 'tl', 'Aurora'),

  ('PH', 'BAN', 'en', 'Bataan'),
  ('PH', 'BAN', 'tl', 'Bataan'),

  ('PH', 'BAS', 'en', 'Basilan'),
  ('PH', 'BAS', 'tl', 'Basilan'),

  ('PH', 'BEN', 'en', 'Benguet'),
  ('PH', 'BEN', 'tl', 'Benget'),

  ('PH', 'BIL', 'en', 'Biliran'),
  ('PH', 'BIL', 'tl', 'Biliran'),

  ('PH', 'BOH', 'en', 'Bohol'),
  ('PH', 'BOH', 'tl', 'Bohol'),

  ('PH', 'BTG', 'en', 'Batangas'),
  ('PH', 'BTG', 'tl', 'Batangas'),

  ('PH', 'BTN', 'en', 'Batanes'),
  ('PH', 'BTN', 'tl', 'Batanes'),

  ('PH', 'BUK', 'en', 'Bukidnon'),
  ('PH', 'BUK', 'tl', 'Bukidnon'),

  ('PH', 'BUL', 'en', 'Bulacan'),
  ('PH', 'BUL', 'tl', 'Bulakan'),

  ('PH', 'CAG', 'en', 'Cagayan'),
  ('PH', 'CAG', 'tl', 'Kagayan'),

  ('PH', 'CAM', 'en', 'Camiguin'),
  ('PH', 'CAM', 'tl', 'Kamigin'),

  ('PH', 'CAN', 'en', 'Camarines Norte'),
  ('PH', 'CAN', 'tl', 'Hilagang Kamarines'),

  ('PH', 'CAP', 'en', 'Capiz'),
  ('PH', 'CAP', 'tl', 'Kapis'),

  ('PH', 'CAS', 'en', 'Camarines Sur'),
  ('PH', 'CAS', 'tl', 'Timog Kamarines'),

  ('PH', 'CAT', 'en', 'Catanduanes'),
  ('PH', 'CAT', 'tl', 'Katanduwanes'),

  ('PH', 'CAV', 'en', 'Cavite'),
  ('PH', 'CAV', 'tl', 'Kabite'),

  ('PH', 'CEB', 'en', 'Cebu'),
  ('PH', 'CEB', 'tl', 'Sebu'),

  ('PH', 'COM', 'en', 'Davao de Oro'),
  ('PH', 'COM', 'tl', 'Davao de Oro'),

  ('PH', 'DAO', 'en', 'Davao Oriental'),
  ('PH', 'DAO', 'tl', 'Silangang Dabaw'),

  ('PH', 'DAS', 'en', 'Davao del Sur'),
  ('PH', 'DAS', 'tl', 'Timog Dabaw'),

  ('PH', 'DAV', 'en', 'Davao del Norte'),
  ('PH', 'DAV', 'tl', 'Hilagang Dabaw'),

  ('PH', 'DIN', 'en', 'Dinagat Islands'),
  ('PH', 'DIN', 'tl', 'Pulo ng Dinagat'),

  ('PH', 'DVO', 'en', 'Davao Occidental'),
  ('PH', 'DVO', 'tl', 'Kanlurang Dabaw'),

  ('PH', 'EAS', 'en', 'Eastern Samar'),
  ('PH', 'EAS', 'tl', 'Silangang Samar'),

  ('PH', 'GUI', 'en', 'Guimaras'),
  ('PH', 'GUI', 'tl', 'Gimaras'),

  ('PH', 'IFU', 'en', 'Ifugao'),
  ('PH', 'IFU', 'tl', 'Ipugaw'),

  ('PH', 'ILI', 'en', 'Iloilo'),
  ('PH', 'ILI', 'tl', 'Iloilo'),

  ('PH', 'ILN', 'en', 'Ilocos Norte'),
  ('PH', 'ILN', 'tl', 'Hilagang Iloko'),

  ('PH', 'ILS', 'en', 'Ilocos Sur'),
  ('PH', 'ILS', 'tl', 'Timog Iloko'),

  ('PH', 'ISA', 'en', 'Isabela'),
  ('PH', 'ISA', 'tl', 'Isabela'),

  ('PH', 'KAL', 'en', 'Kalinga'),
  ('PH', 'KAL', 'tl', 'Kalinga'),

  ('PH', 'LAG', 'en', 'Laguna'),
  ('PH', 'LAG', 'tl', 'Laguna'),

  ('PH', 'LAN', 'en', 'Lanao del Norte'),
  ('PH', 'LAN', 'tl', 'Hilagang Lanaw'),

  ('PH', 'LAS', 'en', 'Lanao del Sur'),
  ('PH', 'LAS', 'tl', 'Timog Lanaw'),

  ('PH', 'LEY', 'en', 'Leyte'),
  ('PH', 'LEY', 'tl', 'Leyte'),

  ('PH', 'LUN', 'en', 'La Union'),
  ('PH', 'LUN', 'tl', 'La Unyon'),

  ('PH', 'MAD', 'en', 'Marinduque'),
  ('PH', 'MAD', 'tl', 'Marinduke'),

  ('PH', 'MAG', 'en', 'Maguindanao'),
  ('PH', 'MAG', 'tl', 'Magindanaw'),

  ('PH', 'MAS', 'en', 'Masbate'),
  ('PH', 'MAS', 'tl', 'Masbate'),

  ('PH', 'MDC', 'en', 'Mindoro Occidental'),
  ('PH', 'MDC', 'tl', 'Kanlurang Mindoro'),

  ('PH', 'MDR', 'en', 'Mindoro Oriental'),
  ('PH', 'MDR', 'tl', 'Silangang Mindoro'),

  ('PH', 'MOU', 'en', 'Mountain Province'),
  ('PH', 'MOU', 'tl', 'Lalawigang Bulubundukin'),

  ('PH', 'MSC', 'en', 'Misamis Occidental'),
  ('PH', 'MSC', 'tl', 'Kanlurang Misamis'),

  ('PH', 'MSR', 'en', 'Misamis Oriental'),
  ('PH', 'MSR', 'tl', 'Silangang Misamis'),

  ('PH', 'NCO', 'en', 'Cotabato'),
  ('PH', 'NCO', 'tl', 'Kotabato'),

  ('PH', 'NEC', 'en', 'Negros Occidental'),
  ('PH', 'NEC', 'tl', 'Kanlurang Negros'),

  ('PH', 'NER', 'en', 'Negros Oriental'),
  ('PH', 'NER', 'tl', 'Silangang Negros'),

  ('PH', 'NSA', 'en', 'Northern Samar'),
  ('PH', 'NSA', 'tl', 'Hilagang Samar'),

  ('PH', 'NUE', 'en', 'Nueva Ecija'),
  ('PH', 'NUE', 'tl', 'Nuweva Esiha'),

  ('PH', 'NUV', 'en', 'Nueva Vizcaya'),
  ('PH', 'NUV', 'tl', 'Nuweva Biskaya'),

  ('PH', 'PAM', 'en', 'Pampanga'),
  ('PH', 'PAM', 'tl', 'Pampanga'),

  ('PH', 'PAN', 'en', 'Pangasinan'),
  ('PH', 'PAN', 'tl', 'Pangasinan'),

  ('PH', 'PLW', 'en', 'Palawan'),
  ('PH', 'PLW', 'tl', 'Palawan'),

  ('PH', 'QUE', 'en', 'Quezon'),
  ('PH', 'QUE', 'tl', 'Keson'),

  ('PH', 'QUI', 'en', 'Quirino'),
  ('PH', 'QUI', 'tl', 'Kirino'),

  ('PH', 'RIZ', 'en', 'Rizal'),
  ('PH', 'RIZ', 'tl', 'Risal'),

  ('PH', 'ROM', 'en', 'Romblon'),
  ('PH', 'ROM', 'tl', 'Romblon'),

  ('PH', 'SAR', 'en', 'Sarangani'),
  ('PH', 'SAR', 'tl', 'Sarangani'),

  ('PH', 'SCO', 'en', 'South Cotabato'),
  ('PH', 'SCO', 'tl', 'Timog Kotabato'),

  ('PH', 'SIG', 'en', 'Siquijor'),
  ('PH', 'SIG', 'tl', 'Sikihor'),

  ('PH', 'SLE', 'en', 'Southern Leyte'),
  ('PH', 'SLE', 'tl', 'Katimogang Leyte'),

  ('PH', 'SLU', 'en', 'Sulu'),
  ('PH', 'SLU', 'tl', 'Sulu'),

  ('PH', 'SOR', 'en', 'Sorsogon'),
  ('PH', 'SOR', 'tl', 'Sorsogon'),

  ('PH', 'SUK', 'en', 'Sultan Kudarat'),
  ('PH', 'SUK', 'tl', 'Sultan Kudarat'),

  ('PH', 'SUN', 'en', 'Surigao del Norte'),
  ('PH', 'SUN', 'tl', 'Hilagang Surigaw'),

  ('PH', 'SUR', 'en', 'Surigao del Sur'),
  ('PH', 'SUR', 'tl', 'Timog Surigaw'),

  ('PH', 'TAR', 'en', 'Tarlac'),
  ('PH', 'TAR', 'tl', 'Tarlak'),

  ('PH', 'TAW', 'en', 'Tawi-Tawi'),
  ('PH', 'TAW', 'tl', 'Tawi-Tawi'),

  ('PH', 'WSA', 'en', 'Samar'),
  ('PH', 'WSA', 'tl', 'Samar'),

  ('PH', 'ZAN', 'en', 'Zamboanga del Norte'),
  ('PH', 'ZAN', 'tl', 'Hilagang Sambuwangga'),

  ('PH', 'ZAS', 'en', 'Zamboanga del Sur'),
  ('PH', 'ZAS', 'tl', 'Timog Sambuwangga'),

  ('PH', 'ZMB', 'en', 'Zambales'),
  ('PH', 'ZMB', 'tl', 'Sambales'),

  ('PH', 'ZSI', 'en', 'Zamboanga Sibugay'),
  ('PH', 'ZSI', 'tl', 'Sambuwangga Sibugay');

-- ISO 3166-2:RO Romania departments and Bucharest municipality in Romanian (ro).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:RO
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:RO
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('RO', 'AB', 'ro', 'Alba'),
  ('RO', 'AG', 'ro', 'Argeș'),
  ('RO', 'AR', 'ro', 'Arad'),
  ('RO', 'B',  'ro', 'București'),
  ('RO', 'BC', 'ro', 'Bacău'),
  ('RO', 'BH', 'ro', 'Bihor'),
  ('RO', 'BN', 'ro', 'Bistrița-Năsăud'),
  ('RO', 'BR', 'ro', 'Brăila'),
  ('RO', 'BT', 'ro', 'Botoșani'),
  ('RO', 'BV', 'ro', 'Brașov'),
  ('RO', 'BZ', 'ro', 'Buzău'),
  ('RO', 'CJ', 'ro', 'Cluj'),
  ('RO', 'CL', 'ro', 'Călărași'),
  ('RO', 'CS', 'ro', 'Caraș-Severin'),
  ('RO', 'CT', 'ro', 'Constanța'),
  ('RO', 'CV', 'ro', 'Covasna'),
  ('RO', 'DB', 'ro', 'Dâmbovița'),
  ('RO', 'DJ', 'ro', 'Dolj'),
  ('RO', 'GJ', 'ro', 'Gorj'),
  ('RO', 'GL', 'ro', 'Galați'),
  ('RO', 'GR', 'ro', 'Giurgiu'),
  ('RO', 'HD', 'ro', 'Hunedoara'),
  ('RO', 'HR', 'ro', 'Harghita'),
  ('RO', 'IF', 'ro', 'Ilfov'),
  ('RO', 'IL', 'ro', 'Ialomița'),
  ('RO', 'IS', 'ro', 'Iași'),
  ('RO', 'MH', 'ro', 'Mehedinți'),
  ('RO', 'MM', 'ro', 'Maramureș'),
  ('RO', 'MS', 'ro', 'Mureș'),
  ('RO', 'NT', 'ro', 'Neamț'),
  ('RO', 'OT', 'ro', 'Olt'),
  ('RO', 'PH', 'ro', 'Prahova'),
  ('RO', 'SB', 'ro', 'Sibiu'),
  ('RO', 'SJ', 'ro', 'Sălaj'),
  ('RO', 'SM', 'ro', 'Satu Mare'),
  ('RO', 'SV', 'ro', 'Suceava'),
  ('RO', 'TL', 'ro', 'Tulcea'),
  ('RO', 'TM', 'ro', 'Timiș'),
  ('RO', 'TR', 'ro', 'Teleorman'),
  ('RO', 'VL', 'ro', 'Vâlcea'),
  ('RO', 'VN', 'ro', 'Vrancea'),
  ('RO', 'VS', 'ro', 'Vaslui');

-- ISO 3166-2:RU Russia republics. autonomous district, and oblasts in Russian (ru).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:RU
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:RU
-- @see https://ru.wikipedia.org/wiki/ISO_3166-2:RU
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('RU', 'ALT', 'ru', 'Altajskij kraj'),
  ('RU', 'AMU', 'ru', 'Amurskaja oblast'''),
  ('RU', 'ARK', 'ru', 'Arhangel''skaja oblast'''),
  ('RU', 'AST', 'ru', 'Astrahanskaja oblast'''),
  ('RU', 'BA',  'ru', 'Baškortostan, Respublika'),
  ('RU', 'BEL', 'ru', 'Belgorodskaja oblast'''),
  ('RU', 'BRY', 'ru', 'Brjanskaja oblast'''),
  ('RU', 'BU',  'ru', 'Burjatija, Respublika'),
  ('RU', 'CE',  'ru', 'Čečenskaja Respublika'),
  ('RU', 'CHE', 'ru', 'Čeljabinskaja oblast'''),
  ('RU', 'CHU', 'ru', 'Čukotskij avtonomnyj okrug'),
  ('RU', 'CU',  'ru', 'Čuvašskaja Respublika'),
  ('RU', 'DA',  'ru', 'Dagestan, Respublika'),
  ('RU', 'IN',  'ru', 'Ingušetija, Respublika'),
  ('RU', 'IRK', 'ru', 'Irkutskaja oblast'''),
  ('RU', 'IVA', 'ru', 'Ivanovskaja oblast'''),
  ('RU', 'KAM', 'ru', 'Kamčatskij kraj'),
  ('RU', 'KB',  'ru', 'Kabardino-Balkarskaja Respublika'),
  ('RU', 'KC',  'ru', 'Karačaevo-Čerkesskaja Respublika'),
  ('RU', 'KDA', 'ru', 'Krasnodarskij kraj'),
  ('RU', 'KEM', 'ru', 'Kemerovskaja oblast'''),
  ('RU', 'KGD', 'ru', 'Kaliningradskaja oblast'''),
  ('RU', 'KGN', 'ru', 'Kurganskaja oblast'''),
  ('RU', 'KHA', 'ru', 'Habarovskij kraj'),
  ('RU', 'KHM', 'ru', 'Hanty-Mansijskij avtonomnyj okrug'),
  ('RU', 'KIR', 'ru', 'Kirovskaja oblast'''),
  ('RU', 'KK',  'ru', 'Hakasija, Respublika'),
  ('RU', 'KL',  'ru', 'Kalmykija, Respublika'),
  ('RU', 'KLU', 'ru', 'Kalužskaja oblast'''),
  ('RU', 'KO',  'ru', 'Komi, Respublika'),
  ('RU', 'KOS', 'ru', 'Kostromskaja oblast'''),
  ('RU', 'KR',  'ru', 'Karelija, Respublika'),
  ('RU', 'KRS', 'ru', 'Kurskaja oblast'''),
  ('RU', 'KYA', 'ru', 'Krasnojarskij kraj'),
  ('RU', 'LEN', 'ru', 'Leningradskaja oblast'''),
  ('RU', 'LIP', 'ru', 'Lipeckaja oblast'''),
  ('RU', 'MAG', 'ru', 'Magadanskaja oblast'''),
  ('RU', 'ME',  'ru', 'Marij Èl, Respublika'),
  ('RU', 'MO',  'ru', 'Mordovija, Respublika'),
  ('RU', 'MOS', 'ru', 'Moskovskaja oblast'''),
  ('RU', 'MOW', 'ru', 'Moskva'),
  ('RU', 'MUR', 'ru', 'Murmanskaja oblast'''),
  ('RU', 'NEN', 'ru', 'Neneckij avtonomnyj okrug'),
  ('RU', 'NGR', 'ru', 'Novgorodskaja oblast'''),
  ('RU', 'NIZ', 'ru', 'Nižegorodskaja oblast'''),
  ('RU', 'NVS', 'ru', 'Novosibirskaja oblast'''),
  ('RU', 'OMS', 'ru', 'Omskaja oblast'''),
  ('RU', 'ORE', 'ru', 'Orenburgskaja oblast'''),
  ('RU', 'ORL', 'ru', 'Orlovskaja oblast'''),
  ('RU', 'PER', 'ru', 'Permskij kraj'),
  ('RU', 'PNZ', 'ru', 'Penzenskaja oblast'''),
  ('RU', 'PRI', 'ru', 'Primorskij kraj'),
  ('RU', 'PSK', 'ru', 'Pskovskaja oblast'''),
  ('RU', 'ROS', 'ru', 'Rostovskaja oblast'''),
  ('RU', 'RYA', 'ru', 'Rjazanskaja oblast'''),
  ('RU', 'SA',  'ru', 'Sakha, Respublika'),
  ('RU', 'SAK', 'ru', 'Sahalinskaja oblast'''),
  ('RU', 'SAM', 'ru', 'Samarskaja oblast'''),
  ('RU', 'SAR', 'ru', 'Saratovskaja oblast'''),
  ('RU', 'SE',  'ru', 'Severnaja Osetija, Respublika'),
  ('RU', 'SMO', 'ru', 'Smolenskaja oblast'''),
  ('RU', 'SPE', 'ru', 'Sankt-Peterburg'),
  ('RU', 'STA', 'ru', 'Stavropol''skij kraj'),
  ('RU', 'SVE', 'ru', 'Sverdlovskaja oblast'''),
  ('RU', 'TA',  'ru', 'Tatarstan, Respublika'),
  ('RU', 'TAM', 'ru', 'Tambovskaja oblast'''),
  ('RU', 'TOM', 'ru', 'Tomskaja oblast'''),
  ('RU', 'TUL', 'ru', 'Tul''skaja oblast'''),
  ('RU', 'TVE', 'ru', 'Tverskaja oblast'''),
  ('RU', 'TY',  'ru', 'Tyva, Respublika'),
  ('RU', 'TYU', 'ru', 'Tjumenskaja oblast'''),
  ('RU', 'UD',  'ru', 'Udmurtskaja Respublika'),
  ('RU', 'ULY', 'ru', 'Ul''janovskaja oblast'''),
  ('RU', 'VGG', 'ru', 'Volgogradskaja oblast'''),
  ('RU', 'VLA', 'ru', 'Vladimirskaja oblast'''),
  ('RU', 'VLG', 'ru', 'Vologodskaja oblast'''),
  ('RU', 'VOR', 'ru', 'Voronežskaja oblast'''),
  ('RU', 'YAN', 'ru', 'Jamalo-Neneckij avtonomnyj okrug'),
  ('RU', 'YAR', 'ru', 'Jaroslavskaja oblast'''),
  ('RU', 'YEV', 'ru', 'Evrejskaja avtonomnaja oblast'''),
  ('RU', 'ZAB', 'ru', 'Zabajkal''skij kraj');

-- ISO 3166-2:TH Thailand provinces in Thai (th),
-- @see https://www.iso.org/obp/ui/#iso:code:3166:TH
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:TH
-- @see https://th.wikipedia.org/wiki/ISO_3166-2:TH
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('TH', '10', 'th', 'Krung Thep Maha Nakhon'),
  ('TH', '11', 'th', 'Samut Prakan'),
  ('TH', '12', 'th', 'Nonthaburi'),
  ('TH', '13', 'th', 'Pathum Thani'),
  ('TH', '14', 'th', 'Phra Nakhon Si Ayutthaya'),
  ('TH', '15', 'th', 'Ang Thong'),
  ('TH', '16', 'th', 'Lop Buri'),
  ('TH', '17', 'th', 'Sing Buri'),
  ('TH', '18', 'th', 'Chai Nat'),
  ('TH', '19', 'th', 'Saraburi'),
  ('TH', '20', 'th', 'Chon Buri'),
  ('TH', '21', 'th', 'Rayong'),
  ('TH', '22', 'th', 'Chanthaburi'),
  ('TH', '23', 'th', 'Trat'),
  ('TH', '24', 'th', 'Chachoengsao'),
  ('TH', '25', 'th', 'Prachin Buri'),
  ('TH', '26', 'th', 'Nakhon Nayok'),
  ('TH', '27', 'th', 'Sa Kaeo'),
  ('TH', '30', 'th', 'Nakhon Ratchasima'),
  ('TH', '31', 'th', 'Buri Ram'),
  ('TH', '32', 'th', 'Surin'),
  ('TH', '33', 'th', 'Si Sa Ket'),
  ('TH', '34', 'th', 'Ubon Ratchathani'),
  ('TH', '35', 'th', 'Yasothon'),
  ('TH', '36', 'th', 'Chaiyaphum'),
  ('TH', '37', 'th', 'Amnat Charoen'),
  ('TH', '38', 'th', 'Bueng Kan'),
  ('TH', '39', 'th', 'Nong Bua Lam Phu'),
  ('TH', '40', 'th', 'Khon Kaen'),
  ('TH', '41', 'th', 'Udon Thani'),
  ('TH', '42', 'th', 'Loei'),
  ('TH', '43', 'th', 'Nong Khai'),
  ('TH', '44', 'th', 'Maha Sarakham'),
  ('TH', '45', 'th', 'Roi Et'),
  ('TH', '46', 'th', 'Kalasin'),
  ('TH', '47', 'th', 'Sakon Nakhon'),
  ('TH', '48', 'th', 'Nakhon Phanom'),
  ('TH', '49', 'th', 'Mukdahan'),
  ('TH', '50', 'th', 'Chiang Mai'),
  ('TH', '51', 'th', 'Lamphun'),
  ('TH', '52', 'th', 'Lampang'),
  ('TH', '53', 'th', 'Uttaradit'),
  ('TH', '54', 'th', 'Phrae'),
  ('TH', '55', 'th', 'Nan'),
  ('TH', '56', 'th', 'Phayao'),
  ('TH', '57', 'th', 'Chiang Rai'),
  ('TH', '58', 'th', 'Mae Hong Son'),
  ('TH', '60', 'th', 'Nakhon Sawan'),
  ('TH', '61', 'th', 'Uthai Thani'),
  ('TH', '62', 'th', 'Kamphaeng Phet'),
  ('TH', '63', 'th', 'Tak'),
  ('TH', '64', 'th', 'Sukhothai'),
  ('TH', '65', 'th', 'Phitsanulok'),
  ('TH', '66', 'th', 'Phichit'),
  ('TH', '67', 'th', 'Phetchabun'),
  ('TH', '70', 'th', 'Ratchaburi'),
  ('TH', '71', 'th', 'Kanchanaburi'),
  ('TH', '72', 'th', 'Suphan Buri'),
  ('TH', '73', 'th', 'Nakhon Pathom'),
  ('TH', '74', 'th', 'Samut Sakhon'),
  ('TH', '75', 'th', 'Samut Songkhram'),
  ('TH', '76', 'th', 'Phetchaburi'),
  ('TH', '77', 'th', 'Prachuap Khiri Khan'),
  ('TH', '80', 'th', 'Nakhon Si Thammarat'),
  ('TH', '81', 'th', 'Krabi'),
  ('TH', '82', 'th', 'Phangnga'),
  ('TH', '83', 'th', 'Phuket'),
  ('TH', '84', 'th', 'Surat Thani'),
  ('TH', '85', 'th', 'Ranong'),
  ('TH', '86', 'th', 'Chumphon'),
  ('TH', '90', 'th', 'Songkhla'),
  ('TH', '91', 'th', 'Satun'),
  ('TH', '92', 'th', 'Trang'),
  ('TH', '93', 'th', 'Phatthalung'),
  ('TH', '94', 'th', 'Pattani'),
  ('TH', '95', 'th', 'Yala'),
  ('TH', '96', 'th', 'Narathiwat'),
  ('TH', 'S',  'th', 'Phatthaya');

-- ISO 3166-2:TR Turkey provinces in Turkish (tr).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:TR
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:TR
-- @see https://tr.wikipedia.org/wiki/ISO_3166-2:TR
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('TR', '01', 'tr', 'Adana'),
  ('TR', '02', 'tr', 'Adıyaman'),
  ('TR', '03', 'tr', 'Afyonkarahisar'),
  ('TR', '04', 'tr', 'Ağrı'),
  ('TR', '05', 'tr', 'Amasya'),
  ('TR', '06', 'tr', 'Ankara'),
  ('TR', '07', 'tr', 'Antalya'),
  ('TR', '08', 'tr', 'Artvin'),
  ('TR', '09', 'tr', 'Aydın'),
  ('TR', '10', 'tr', 'Balıkesir'),
  ('TR', '11', 'tr', 'Bilecik'),
  ('TR', '12', 'tr', 'Bingöl'),
  ('TR', '13', 'tr', 'Bitlis'),
  ('TR', '14', 'tr', 'Bolu'),
  ('TR', '15', 'tr', 'Burdur'),
  ('TR', '16', 'tr', 'Bursa'),
  ('TR', '17', 'tr', 'Çanakkale'),
  ('TR', '18', 'tr', 'Çankırı'),
  ('TR', '19', 'tr', 'Çorum'),
  ('TR', '20', 'tr', 'Denizli'),
  ('TR', '21', 'tr', 'Diyarbakır'),
  ('TR', '22', 'tr', 'Edirne'),
  ('TR', '23', 'tr', 'Elazığ'),
  ('TR', '24', 'tr', 'Erzincan'),
  ('TR', '25', 'tr', 'Erzurum'),
  ('TR', '26', 'tr', 'Eskişehir'),
  ('TR', '27', 'tr', 'Gaziantep'),
  ('TR', '28', 'tr', 'Giresun'),
  ('TR', '29', 'tr', 'Gümüşhane'),
  ('TR', '30', 'tr', 'Hakkâri'),
  ('TR', '31', 'tr', 'Hatay'),
  ('TR', '32', 'tr', 'Isparta'),
  ('TR', '33', 'tr', 'Mersin'),
  ('TR', '34', 'tr', 'İstanbul'),
  ('TR', '35', 'tr', 'İzmir'),
  ('TR', '36', 'tr', 'Kars'),
  ('TR', '37', 'tr', 'Kastamonu'),
  ('TR', '38', 'tr', 'Kayseri'),
  ('TR', '39', 'tr', 'Kırklareli'),
  ('TR', '40', 'tr', 'Kırşehir'),
  ('TR', '41', 'tr', 'Kocaeli'),
  ('TR', '42', 'tr', 'Konya'),
  ('TR', '43', 'tr', 'Kütahya'),
  ('TR', '44', 'tr', 'Malatya'),
  ('TR', '45', 'tr', 'Manisa'),
  ('TR', '46', 'tr', 'Kahramanmaraş'),
  ('TR', '47', 'tr', 'Mardin'),
  ('TR', '48', 'tr', 'Muğla'),
  ('TR', '49', 'tr', 'Muş'),
  ('TR', '50', 'tr', 'Nevşehir'),
  ('TR', '51', 'tr', 'Niğde'),
  ('TR', '52', 'tr', 'Ordu'),
  ('TR', '53', 'tr', 'Rize'),
  ('TR', '54', 'tr', 'Sakarya'),
  ('TR', '55', 'tr', 'Samsun'),
  ('TR', '56', 'tr', 'Siirt'),
  ('TR', '57', 'tr', 'Sinop'),
  ('TR', '58', 'tr', 'Sivas'),
  ('TR', '59', 'tr', 'Tekirdağ'),
  ('TR', '60', 'tr', 'Tokat'),
  ('TR', '61', 'tr', 'Trabzon'),
  ('TR', '62', 'tr', 'Tunceli'),
  ('TR', '63', 'tr', 'Şanlıurfa'),
  ('TR', '64', 'tr', 'Uşak'),
  ('TR', '65', 'tr', 'Van'),
  ('TR', '66', 'tr', 'Yozgat'),
  ('TR', '67', 'tr', 'Zonguldak'),
  ('TR', '68', 'tr', 'Aksaray'),
  ('TR', '69', 'tr', 'Bayburt'),
  ('TR', '70', 'tr', 'Karaman'),
  ('TR', '71', 'tr', 'Kırıkkale'),
  ('TR', '72', 'tr', 'Batman'),
  ('TR', '73', 'tr', 'Şırnak'),
  ('TR', '74', 'tr', 'Bartın'),
  ('TR', '75', 'tr', 'Ardahan'),
  ('TR', '76', 'tr', 'Iğdır'),
  ('TR', '77', 'tr', 'Yalova'),
  ('TR', '78', 'tr', 'Karabük'),
  ('TR', '79', 'tr', 'Kilis'),
  ('TR', '80', 'tr', 'Osmaniye'),
  ('TR', '81', 'tr', 'Düzce');

-- ISO 3166-2:TW Taiwan counties, cities, and special municipalities in Chinese (zh).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:TW
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:TW
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('TW', 'CHA', 'zh', 'Changhua'),
  ('TW', 'CYI', 'zh', 'Chiayi'),
  ('TW', 'CYQ', 'zh', 'Chiayi'),
  ('TW', 'HSQ', 'zh', 'Hsinchu'),
  ('TW', 'HSZ', 'zh', 'Hsinchu'),
  ('TW', 'HUA', 'zh', 'Hualien'),
  ('TW', 'ILA', 'zh', 'Yilan'),
  ('TW', 'KEE', 'zh', 'Keelung'),
  ('TW', 'KHH', 'zh', 'Kaohsiung'),
  ('TW', 'KIN', 'zh', 'Kinmen'),
  ('TW', 'LIE', 'zh', 'Lienchiang'),
  ('TW', 'MIA', 'zh', 'Miaoli'),
  ('TW', 'NAN', 'zh', 'Nantou'),
  ('TW', 'NWT', 'zh', 'New Taipei'),
  ('TW', 'PEN', 'zh', 'Penghu'),
  ('TW', 'PIF', 'zh', 'Pingtung'),
  ('TW', 'TAO', 'zh', 'Taoyuan'),
  ('TW', 'TNN', 'zh', 'Tainan'),
  ('TW', 'TPE', 'zh', 'Taipei'),
  ('TW', 'TTT', 'zh', 'Taitung'),
  ('TW', 'TXG', 'zh', 'Taichung'),
  ('TW', 'YUN', 'zh', 'Yunlin');

-- ISO 3166-2:TW Ukrain regions in romanized Ukranian (uk).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:UA
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:UA
-- @see https://uk.wikipedia.org/wiki/ISO_3166-2:UA
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('UA', '05', 'uk', 'Vinnytska oblast'),
  ('UA', '07', 'uk', 'Volynska oblast'),
  ('UA', '09', 'uk', 'Luhanska oblast'),
  ('UA', '12', 'uk', 'Dnipropetrovska oblast'),
  ('UA', '14', 'uk', 'Donetska oblast'),
  ('UA', '18', 'uk', 'Zhytomyrska oblast'),
  ('UA', '21', 'uk', 'Zakarpatska oblast'),
  ('UA', '23', 'uk', 'Zaporizka oblast'),
  ('UA', '26', 'uk', 'Ivano-Frankivska oblast'),
  ('UA', '30', 'uk', 'Kyiv'),
  ('UA', '32', 'uk', 'Kyivska oblast'),
  ('UA', '35', 'uk', 'Kirovohradska oblast'),
  ('UA', '40', 'uk', 'Sevastopol'),
  ('UA', '43', 'uk', 'Avtonomna Respublika Krym'),
  ('UA', '46', 'uk', 'Lvivska oblast'),
  ('UA', '48', 'uk', 'Mykolaivska oblast'),
  ('UA', '51', 'uk', 'Odeska oblast'),
  ('UA', '53', 'uk', 'Poltavska oblast'),
  ('UA', '56', 'uk', 'Rivnenska oblast'),
  ('UA', '59', 'uk', 'Sumska oblast'),
  ('UA', '61', 'uk', 'Ternopilska oblast'),
  ('UA', '63', 'uk', 'Kharkivska oblast'),
  ('UA', '65', 'uk', 'Khersonska oblast'),
  ('UA', '68', 'uk', 'Khmelnytska oblast'),
  ('UA', '71', 'uk', 'Cherkaska oblast'),
  ('UA', '74', 'uk', 'Chernihivska oblast'),
  ('UA', '77', 'uk', 'Chernivetska oblast');

-- ISO 3166-2:US United States states in English (en).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:US
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:US
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('US', 'AL', 'en', 'Alabama'),
  ('US', 'AK', 'en', 'Alaska'),
  ('US', 'AZ', 'en', 'Arizona'),
  ('US', 'AR', 'en', 'Arkansas'),
  ('US', 'CA', 'en', 'California'),
  ('US', 'CO', 'en', 'Colorado'),
  ('US', 'CT', 'en', 'Connecticut'),
  ('US', 'DE', 'en', 'Delaware'),
  ('US', 'DC', 'en', 'District of Columbia'),
  ('US', 'FL', 'en', 'Florida'),
  ('US', 'GA', 'en', 'Georgia'),
  ('US', 'HI', 'en', 'Hawaii'),
  ('US', 'ID', 'en', 'Idaho'),
  ('US', 'IL', 'en', 'Illinois'),
  ('US', 'IN', 'en', 'Indiana'),
  ('US', 'IA', 'en', 'Iowa'),
  ('US', 'KS', 'en', 'Kansas'),
  ('US', 'KY', 'en', 'Kentucky'),
  ('US', 'LA', 'en', 'Louisiana'),
  ('US', 'ME', 'en', 'Maine'),
  ('US', 'MD', 'en', 'Maryland'),
  ('US', 'MA', 'en', 'Massachusetts'),
  ('US', 'MI', 'en', 'Michigan'),
  ('US', 'MN', 'en', 'Minnesota'),
  ('US', 'MS', 'en', 'Mississippi'),
  ('US', 'MO', 'en', 'Missouri'),
  ('US', 'MT', 'en', 'Montana'),
  ('US', 'NE', 'en', 'Nebraska'),
  ('US', 'NV', 'en', 'Nevada'),
  ('US', 'NH', 'en', 'New Hampshire'),
  ('US', 'NJ', 'en', 'New Jersey'),
  ('US', 'NM', 'en', 'New Mexico'),
  ('US', 'NY', 'en', 'New York'),
  ('US', 'NC', 'en', 'North Carolina'),
  ('US', 'ND', 'en', 'North Dakota'),
  ('US', 'OH', 'en', 'Ohio'),
  ('US', 'OK', 'en', 'Oklahoma'),
  ('US', 'OR', 'en', 'Oregon'),
  ('US', 'PA', 'en', 'Pennsylvania'),
  ('US', 'RI', 'en', 'Rhode Island'),
  ('US', 'SC', 'en', 'South Carolina'),
  ('US', 'SD', 'en', 'South Dakota'),
  ('US', 'TN', 'en', 'Tennessee'),
  ('US', 'TX', 'en', 'Texas'),
  ('US', 'UT', 'en', 'Utah'),
  ('US', 'VT', 'en', 'Vermont'),
  ('US', 'VA', 'en', 'Virginia'),
  ('US', 'WA', 'en', 'Washington'),
  ('US', 'WV', 'en', 'West Virginia'),
  ('US', 'WI', 'en', 'Wisconsin'),
  ('US', 'WY', 'en', 'Wyoming');

UPDATE sc_countries SET subdivision_required = 1 WHERE iso_alpha_two = 'US';

-- ISO 3166-2:VN Viet Nam provinces and municipalities in Vietnamese (vi).
-- @see https://www.iso.org/obp/ui/#iso:code:3166:VN
-- @see https://en.wikipedia.org/wiki/ISO_3166-2:VN
-- @see https://vi.wikipedia.org/wiki/ISO_3166-2:VN
INSERT IGNORE INTO sc_country_subdivisions VALUES
  ('VN', '01', 'vi', 'Lai Châu'),
  ('VN', '02', 'vi', 'Lào Cai'),
  ('VN', '03', 'vi', 'Hà Giang'),
  ('VN', '04', 'vi', 'Cao Bằng'),
  ('VN', '05', 'vi', 'Sơn La'),
  ('VN', '06', 'vi', 'Yên Bái'),
  ('VN', '07', 'vi', 'Tuyên Quang'),
  ('VN', '09', 'vi', 'Lạng Sơn'),
  ('VN', '13', 'vi', 'Quảng Ninh'),
  ('VN', '14', 'vi', 'Hòa Bình'),
  ('VN', '18', 'vi', 'Ninh Bình'),
  ('VN', '20', 'vi', 'Thái Bình'),
  ('VN', '21', 'vi', 'Thanh Hóa'),
  ('VN', '22', 'vi', 'Nghệ An'),
  ('VN', '23', 'vi', 'Hà Tĩnh'),
  ('VN', '24', 'vi', 'Quảng Bình'),
  ('VN', '25', 'vi', 'Quảng Trị'),
  ('VN', '26', 'vi', 'Thừa Thiên-Huế'),
  ('VN', '27', 'vi', 'Quảng Nam'),
  ('VN', '28', 'vi', 'Kon Tum'),
  ('VN', '29', 'vi', 'Quảng Ngãi'),
  ('VN', '30', 'vi', 'Gia Lai'),
  ('VN', '31', 'vi', 'Bình Định'),
  ('VN', '32', 'vi', 'Phú Yên'),
  ('VN', '33', 'vi', 'Đắk Lắk'),
  ('VN', '34', 'vi', 'Khánh Hòa'),
  ('VN', '35', 'vi', 'Lâm Đồng'),
  ('VN', '36', 'vi', 'Ninh Thuận'),
  ('VN', '37', 'vi', 'Tây Ninh'),
  ('VN', '39', 'vi', 'Đồng Nai'),
  ('VN', '40', 'vi', 'Bình Thuận'),
  ('VN', '41', 'vi', 'Long An'),
  ('VN', '43', 'vi', 'Bà Rịa - Vũng Tàu'),
  ('VN', '44', 'vi', 'An Giang'),
  ('VN', '45', 'vi', 'Đồng Tháp'),
  ('VN', '46', 'vi', 'Tiền Giang'),
  ('VN', '47', 'vi', 'Kiến Giang'),
  ('VN', '49', 'vi', 'Vĩnh Long'),
  ('VN', '50', 'vi', 'Bến Tre'),
  ('VN', '51', 'vi', 'Trà Vinh'),
  ('VN', '52', 'vi', 'Sóc Trăng'),
  ('VN', '53', 'vi', 'Bắc Kạn'),
  ('VN', '54', 'vi', 'Bắc Giang'),
  ('VN', '55', 'vi', 'Bạc Liêu'),
  ('VN', '56', 'vi', 'Bắc Ninh'),
  ('VN', '57', 'vi', 'Bình Dương'),
  ('VN', '58', 'vi', 'Bình Phước'),
  ('VN', '59', 'vi', 'Cà Mau'),
  ('VN', '61', 'vi', 'Hải Dương'),
  ('VN', '63', 'vi', 'Hà Nam'),
  ('VN', '66', 'vi', 'Hưng Yên'),
  ('VN', '67', 'vi', 'Nam Định'),
  ('VN', '68', 'vi', 'Phú Thọ'),
  ('VN', '69', 'vi', 'Thái Nguyên'),
  ('VN', '70', 'vi', 'Vĩnh Phúc'),
  ('VN', '71', 'vi', 'Điện Biên'),
  ('VN', '72', 'vi', 'Đắk Nông'),
  ('VN', '73', 'vi', 'Hậu Giang'),
  ('VN', 'CT', 'vi', 'Cần Thơ'),
  ('VN', 'DN', 'vi', 'Đà Nẵng'),
  ('VN', 'HN', 'vi', 'Hà Nội'),
  ('VN', 'HP', 'vi', 'Hải Phòng'),
  ('VN', 'SG', 'vi', 'Hồ Chí Minh');

-- Stores
CREATE TABLE IF NOT EXISTS `sc_stores` (
  `store_id`         SMALLINT UNSIGNED    NOT NULL  AUTO_INCREMENT,
  `store_uuid`       BINARY(16)           NOT NULL,
  `default_flag`     BIT(1)               NOT NULL  DEFAULT b'0',
  `enabled_flag`     BIT(1)               NOT NULL  DEFAULT b'0',
  `organization_id`  MEDIUMINT UNSIGNED   NULL  DEFAULT NULL,
  `store_name`       VARCHAR(63)          NOT NULL,
  `version`          SMALLINT UNSIGNED    NOT NULL  DEFAULT 1,
  `date_created`     DATETIME             NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`    DATETIME             NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `date_deleted`     DATETIME             NULL  DEFAULT NULL,
  `date_time_zone`   VARCHAR(63)          NOT NULL  DEFAULT 'UTC'  COMMENT 'PHP DateTimeZone identifier',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `uk_store_uuid` (`store_uuid`),
  FOREIGN KEY `fk_stores_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  UNIQUE KEY `uk_store_name` (`store_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Store currencies
CREATE TABLE IF NOT EXISTS `sc_store_currencies` (
  `store_id`      SMALLINT UNSIGNED     NOT NULL,
  `currency_id`   SMALLINT(3) UNSIGNED  NOT NULL  DEFAULT 978,
  `default_flag`  BIT(1)                NOT NULL  DEFAULT b'0',
  PRIMARY KEY (`store_id`, `currency_id`),
  FOREIGN KEY `fk_store_currencies_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_store_currencies_currencies` (`currency_id`) REFERENCES `sc_currencies` (`currency_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Store hosts
-- @see https://stackoverflow.com/questions/166132/maximum-length-of-the-textual-representation-of-an-ipv6-address
-- @see https://stackoverflow.com/questions/8724954/what-is-the-maximum-number-of-characters-for-a-host-name-in-unix
CREATE TABLE IF NOT EXISTS `sc_store_hosts` (
  `host_id`      SMALLINT UNSIGNED    NOT NULL  AUTO_INCREMENT,
  `store_id`     SMALLINT UNSIGNED    NOT NULL,
  `host_name`    VARCHAR(253)         NULL  DEFAULT NULL,
  `https_flag`   BIT(1)               NOT NULL  DEFAULT b'1',
  `host_ip`      VARCHAR(45)          CHARACTER SET ascii  COLLATE ascii_bin  NULL  DEFAULT NULL,
  `redirect_to`  VARCHAR(253)         NULL  DEFAULT NULL,
  PRIMARY KEY (`host_id`),
  FOREIGN KEY `fk_store_hosts_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE KEY `uk_host_name` (`host_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Store languages
CREATE TABLE IF NOT EXISTS `sc_store_languages` (
  `store_id`      SMALLINT UNSIGNED  NOT NULL,
  `language_id`   VARCHAR(13)        NOT NULL  DEFAULT 'en-GB',
  `default_flag`  BIT(1)             NOT NULL  DEFAULT b'0',
  PRIMARY KEY (`store_id`, `language_id`),
  FOREIGN KEY `fk_store_languages_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_store_languages_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


-- Customer groups
CREATE TABLE IF NOT EXISTS `sc_customer_groups` (
  `customer_group_id`    SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `customer_group_uuid`  BINARY(16)         NOT NULL,
  `date_created`         DATETIME           NOT NULL,
  `date_modified`        DATETIME           NULL  DEFAULT NULL,
  `date_deleted`         DATETIME           NULL  DEFAULT NULL,
  PRIMARY KEY (`customer_group_id`),
  UNIQUE KEY (`customer_group_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_customer_group_names` (
  `customer_group_id`  SMALLINT UNSIGNED  NOT NULL,
  `language_id`        VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`               VARCHAR(63)        NOT NULL,
  `alternate_name`     VARCHAR(63)        NULL  DEFAULT NULL,
  PRIMARY KEY (`customer_group_id`, `language_id`),
  FOREIGN KEY `fk_customer_group_names_customer_groups` (`customer_group_id`) REFERENCES `sc_customer_groups` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_customer_group_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE KEY `uk_customer_group_name` (`language_id`, `name`),
  INDEX `ix_customer_group_names` (`name` ASC, `alternate_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Customers
CREATE TABLE IF NOT EXISTS `sc_customers` (
  `customer_id`        INT UNSIGNED        NOT NULL  AUTO_INCREMENT,
  `customer_uuid`      BINARY(16)          NOT NULL,
  `store_id`           SMALLINT UNSIGNED   NULL  DEFAULT NULL,
  `organization_id`    MEDIUMINT UNSIGNED  NULL  DEFAULT NULL,
  `person_id`          INT UNSIGNED        NULL  DEFAULT NULL,  
  `customer_group_id`  SMALLINT UNSIGNED   NULL  DEFAULT NULL,
  `date_created`       DATETIME            NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`      DATETIME            NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `date_deleted`       DATETIME            NULL  DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY (`customer_uuid`),
  FOREIGN KEY `fk_customers_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_customers_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_customers_persons` (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY `fk_customers_customer_groups` (`customer_group_id`) REFERENCES `sc_customer_groups` (`customer_group_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

--
-- Customer accounts.
--
-- There is a one-to-many relation between customers and customer accounts.
-- A single customer MAY have multiple customer accounts, for example for
-- different stores.  Customer accounts can be deleted without deleting all
-- customer data.
--
CREATE TABLE IF NOT EXISTS `sc_customer_accounts` (
  `customer_account_id`    INT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `customer_account_uuid`  BINARY(16)    NOT NULL,
  `customer_id`            INT UNSIGNED  NOT NULL,
  `email_address_id`       INT UNSIGNED  NOT NULL,
  `date_created`           DATETIME      NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`          DATETIME      NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `password_hash`          VARCHAR(255)  NOT NULL,
  `reset_token`            CHAR(16)      NULL  DEFAULT NULL,
  PRIMARY KEY (`customer_account_id`),
  UNIQUE KEY (`customer_account_uuid`),
  FOREIGN KEY `fk_customer_accounts_customers` (`customer_id`) REFERENCES `sc_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_customer_accounts_email_addresses` (`email_address_id`) REFERENCES `sc_email_addresses` (`email_address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

--
-- Customer and shipping addresses.
--
-- @see https://schema.org/PostalAddress Schema.org type `PostalAddress`
-- @see https://schema.org/billingAddress Schema.org property `Order.billingAddress`
-- @see https://schema.org/deliveryAddress Schema.org property `ParcelDelivery.deliveryAddress`
--
CREATE TABLE IF NOT EXISTS `sc_addresses` (
  `address_id`              INT UNSIGNED          NOT NULL  AUTO_INCREMENT,
  `address_uuid`            BINARY(16)            NOT NULL,
  `global_location_number`  CHAR(13)              NULL  DEFAULT NULL  COMMENT 'GLN',
  `country_id`              SMALLINT(3) UNSIGNED  NOT NULL,
  `country_subdivision`     VARCHAR(3)            NULL  DEFAULT NULL,
  `postal_code`             VARCHAR(16)           NULL  DEFAULT NULL,
  `house_number`            VARCHAR(16)           NULL  DEFAULT NULL,
  `house_number_addition`   VARCHAR(16)           NULL  DEFAULT NULL,
  `post_office_box_number`  VARCHAR(16)           NULL  DEFAULT NULL,
  `latitude`                DECIMAL(10, 8)        NULL  DEFAULT NULL,
  `longitude`               DECIMAL(11, 8)        NULL  DEFAULT NULL,
  `date_created`            DATETIME              NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`           DATETIME              NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `date_validated`          DATETIME              NULL  DEFAULT NULL,
  `date_deleted`            DATETIME              NULL  DEFAULT NULL,
  `name`                    VARCHAR(255)          NULL  DEFAULT NULL,
  `street_name`             VARCHAR(255)          NULL  DEFAULT NULL,
  `locality`                VARCHAR(255)          NULL  DEFAULT NULL,
  `opening_hours`           VARCHAR(255)          NULL  DEFAULT NULL,
  `eav`                     TEXT                  COLLATE utf8mb4_bin  NULL  DEFAULT NULL  COMMENT 'Entity-attribute-value (EAV)',
  PRIMARY KEY (`address_id`),
  UNIQUE KEY (`address_uuid`),
  UNIQUE KEY `uk_global_location_number` (`global_location_number`),
  FOREIGN KEY `fk_addresses_countries` (`country_id`) REFERENCES `sc_countries` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_address_index` (`country_id`, `country_subdivision`, `postal_code`, `house_number`, `house_number_addition`),
  INDEX `ix_latitude_longitude` (`latitude`, `longitude`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_organization_addresses` (
  `organization_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `address_id`       INT UNSIGNED        NOT NULL,
  PRIMARY KEY (`organization_id`, `address_id`),
  FOREIGN KEY `fk_organization_addresses_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_organization_addresses_addresses` (`address_id`) REFERENCES `sc_addresses` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_person_addresses` (
  `person_id`   INT UNSIGNED  NOT NULL,
  `address_id`  INT UNSIGNED  NOT NULL,
  PRIMARY KEY (`person_id`, `address_id`),
  FOREIGN KEY `fk_person_addresses_persons` (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_person_addresses_addresses` (`address_id`) REFERENCES `sc_addresses` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

--
-- Brands and global brand names.
--
-- @see https://schema.org/brand
-- @see https://support.google.com/merchants/answer/6324351?hl=en
CREATE TABLE IF NOT EXISTS `sc_brands` (
  `brand_id`           SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `brand_uuid`         BINARY(16)         NOT NULL,
  `global_brand_name`  VARCHAR(70)        NOT NULL,
  PRIMARY KEY (`brand_id`),
  UNIQUE KEY (`brand_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- OPTIONAL localized brand names.
-- @see https://support.google.com/merchants/answer/6324351
CREATE TABLE IF NOT EXISTS `sc_brand_names` (
  `brand_id`          SMALLINT UNSIGNED  NOT NULL,
  `language_id`       VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `local_brand_name`  VARCHAR(70)        NOT NULL,
  PRIMARY KEY (`brand_id`, `language_id`),
  FOREIGN KEY `fk_brand_names_brands` (`brand_id`) REFERENCES `sc_brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_brand_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Brand(s) owned or maintained by organizations.
CREATE TABLE IF NOT EXISTS `sc_organization_brands` (
  `organization_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `brand_id`         SMALLINT UNSIGNED   NOT NULL,
  PRIMARY KEY (`organization_id`, `brand_id`),
  FOREIGN KEY `fk_organization_brands_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_organization_brands_brands` (`brand_id`) REFERENCES `sc_brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


-- Google product taxonomy.
CREATE TABLE IF NOT EXISTS `sc_product_taxonomy` (
  `taxonomy_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `language_id`  VARCHAR(13)         CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-US',
  `full_path`    VARCHAR(267)        NOT NULL,
  PRIMARY KEY (`taxonomy_id`, `language_id`),
  FOREIGN KEY `fk_product_taxonomy_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FULLTEXT INDEX `ix_product_taxonomy` (`full_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Product categories.
CREATE TABLE IF NOT EXISTS `sc_categories` (
  `category_id`       SMALLINT UNSIGNED   NOT NULL,
  `category_uuid`     BINARY(16)          NOT NULL,
  `parent_uuid`       BINARY(16)          NULL  DEFAULT NULL,
  `taxonomy_id`       MEDIUMINT UNSIGNED  NULL  DEFAULT NULL,
  `gpc_brick_code`    CHAR(8)             NULL  DEFAULT NULL,
  `product_ontology`  VARCHAR(255)        NULL  DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY (`category_uuid`),
  FOREIGN KEY `fk_categories_categories` (`parent_uuid`) REFERENCES `sc_categories` (`category_uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_categories_product_taxonomy` (`taxonomy_id`) REFERENCES `sc_product_taxonomy` (`taxonomy_id`) ON DELETE CASCADE ON UPDATE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Localized category names.
CREATE TABLE IF NOT EXISTS `sc_category_names` (
  `category_id`     SMALLINT UNSIGNED  NOT NULL,
  `language_id`     VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`            VARCHAR(255)       NOT NULL,
  `alternate_name`  VARCHAR(255)       NULL  DEFAULT NULL,
  `gpc_brick_name`  VARCHAR(255)       NULL  DEFAULT NULL,
  PRIMARY KEY (`category_id`, `language_id`),
  FOREIGN KEY `fk_category_names_categories` (`category_id`) REFERENCES `sc_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_category_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Categories per store
CREATE TABLE IF NOT EXISTS `sc_store_categories` (
  `store_id`     SMALLINT UNSIGNED  NOT NULL,
  `category_id`  SMALLINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`store_id`, `category_id`),
  FOREIGN KEY `fk_store_categories_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_store_categories_categories` (`category_id`) REFERENCES `sc_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Colors, color values and color names.
--
-- The `color_code` is a unique color or paint number of a brand or
-- manufacturer.  The `hex_color` is an optional CSS hex color value
-- for screen examples, without the trailing #.
--
CREATE TABLE IF NOT EXISTS `sc_colors` (
  `color_id`    SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `sort_order`  TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `color_code`  VARCHAR(30)        NULL  DEFAULT NULL,
  `hex_color`   VARCHAR(6)         NULL  DEFAULT NULL,
  PRIMARY KEY (`color_id`),
  INDEX sc_colors_sort_order (sort_order ASC),
  UNIQUE uk_color_code (color_code)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_general_ci;

CREATE TABLE IF NOT EXISTS `sc_color_names` (
  `color_id`     SMALLINT UNSIGNED  NOT NULL,
  `language_id`  VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `color_name`   VARCHAR(32)        NOT NULL,
  PRIMARY KEY (`color_id`, `language_id`),
  FOREIGN KEY `fk_color_names_colors` (`color_id`) REFERENCES `sc_colors` (`color_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_color_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_color_name` (`color_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


-- @see https://schema.org/ItemAvailability
--      Schema.org `ItemAvailability` property
CREATE TABLE IF NOT EXISTS `sc_product_availability_types` (
  `availability_id`    TINYINT UNSIGNED  NOT NULL,
  `item_availability`  VARCHAR(63)       NOT NULL  COMMENT 'Schema.org ItemAvailability',
  PRIMARY KEY (`availability_id`),
  UNIQUE KEY (`item_availability`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_product_availability_types`
    (`availability_id`, `item_availability`)
  VALUES
    ( 1, 'https://schema.org/BackOrder'),
    ( 2, 'https://schema.org/Discontinued'),
    ( 3, 'https://schema.org/InStock'),
    ( 4, 'https://schema.org/InStoreOnly'),
    ( 5, 'https://schema.org/LimitedAvailability'),
    ( 6, 'https://schema.org/OnlineOnly'),
    ( 7, 'https://schema.org/OutOfStock'),
    ( 8, 'https://schema.org/PreOrder'),
    ( 9, 'https://schema.org/PreSale'),
    (10, 'https://schema.org/SoldOut');

-- @see https://schema.org/OfferItemCondition
--      Schema.org `OfferItemCondition` property
CREATE TABLE IF NOT EXISTS `sc_product_condition_types` (
  `condition_id`          TINYINT UNSIGNED  NOT NULL,
  `offer_item_condition`  VARCHAR(63)       NOT NULL  COMMENT 'Schema.org OfferItemCondition',
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_product_condition_types`
    (`condition_id`, `offer_item_condition`)
  VALUES
    (0, 'https://schema.org/NewCondition'),
    (1, 'https://schema.org/DamagedCondition'),
    (2, 'https://schema.org/RefurbishedCondition'),
    (3, 'https://schema.org/UsedCondition');

-- Gender a product is designed for.
-- @see https://support.google.com/merchants/answer/6324479?hl=en
CREATE TABLE IF NOT EXISTS `sc_product_genders` (
  `gender_id`  TINYINT UNSIGNED  NOT NULL,
  `gender`     VARCHAR(14)       COLLATE ascii_general_ci  NOT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see https://en.wikipedia.org/wiki/ISO/IEC_5218 ISO/IEC 5218
INSERT IGNORE INTO `sc_product_genders`
    (`gender_id`, `gender`)
  VALUES
    (0, 'not known'),
    (1, 'male'),
    (2, 'female'),
    (3, 'unisex'),
    (9, 'not applicable');

--
-- Adult oriented considerations.
-- @see https://schema.org/AdultOrientedEnumeration
-- @see https://schema.org/hasAdultConsideration
--
CREATE TABLE IF NOT EXISTS `sc_adult_considerations` (
  `adult_consideration_id`  TINYINT UNSIGNED  NOT NULL,
  `adult_consideration`     VARCHAR(63)       COLLATE ascii_bin  NOT NULL,
  PRIMARY KEY (`adult_consideration_id`),
  UNIQUE KEY (`adult_consideration`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_adult_considerations`
    (`adult_consideration_id`, `adult_consideration`)
  VALUES
    (0, 'https://schema.org/UnclassifiedAdultConsideration'),
    (1, 'https://schema.org/AlcoholConsideration'),
    (2, 'https://schema.org/DangerousGoodConsideration'),
    (3, 'https://schema.org/HealthcareConsideration'),
    (4, 'https://schema.org/NarcoticConsideration'),
    (5, 'https://schema.org/ReducedRelevanceForChildrenConsideration'),
    (6, 'https://schema.org/SexualContentConsideration'),
    (7, 'https://schema.org/TobaccoNicotineConsideration'),
    (8, 'https://schema.org/ViolenceConsideration'),
    (9, 'https://schema.org/WeaponConsideration');


--
-- Product materials.
-- @see https://schema.org/material Schema.org property `material`
--
CREATE TABLE IF NOT EXISTS `sc_product_materials` (
  `material_id`    SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `sort_order`     TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `material_code`  VARCHAR(32)        NULL  DEFAULT NULL,
  PRIMARY KEY (`material_id`),
  INDEX `sc_product_materials_sort_order` (`sort_order` ASC),
  UNIQUE `uk_material_code` (`material_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_product_material_names` (
  `material_id`     SMALLINT UNSIGNED  NOT NULL,
  `language_id`     VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `material_name`   VARCHAR(32)        NOT NULL,
  `image_filename`  VARCHAR(32)        NULL  DEFAULT NULL,
  PRIMARY KEY (`material_id`, `language_id`),
  FOREIGN KEY `fk_product_material_names_product_materials` (`material_id`) REFERENCES `sc_product_materials` (`material_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_material_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_material_name` (`material_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

--
-- Product patterns, pattern codes, pattern images, and pattern names.
--
-- @see https://schema.org/pattern Schema.org property `pattern`
--
CREATE TABLE IF NOT EXISTS `sc_product_patterns` (
  `pattern_id`      SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `sort_order`      TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `pattern_code`    VARCHAR(32)        NULL  DEFAULT NULL,
  `image_filename`  VARCHAR(32)        NULL  DEFAULT NULL,
  PRIMARY KEY (`pattern_id`),
  INDEX `sc_product_patterns_sort_order` (`sort_order` ASC),
  UNIQUE `uk_pattern_code` (`pattern_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_product_pattern_names` (
  `pattern_id`    SMALLINT UNSIGNED  NOT NULL,
  `language_id`   VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `pattern_name`  VARCHAR(32)        NOT NULL,
  PRIMARY KEY (`pattern_id`, `language_id`),
  FOREIGN KEY `fk_product_pattern_names_product_patterns` (`pattern_id`) REFERENCES `sc_product_patterns` (`pattern_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_pattern_names_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_pattern_name` (`pattern_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

--
-- Products.
--
-- @see https://schema.org/Product           Schema.org type `Product`
-- @see https://schema.org/Service           Schema.org type `Service`
-- @see https://schema.org/ProductGroup      Schema.org type `ProductGroup`
-- @see https://schema.org/ProductModel      Schema.org type `ProductModel`
-- @see https://schema.org/IndividualProduct Schema.org type `IndividualProduct`
--
-- @see https://schema.org/releaseDate    Schema.org property `Product.releaseDate`
-- @see https://schema.org/productionDate Schema.org property `Product.productionDate`
--
CREATE TABLE IF NOT EXISTS `sc_products` (
  `product_id`                    INT UNSIGNED       NOT NULL  AUTO_INCREMENT,
  `product_uuid`                  BINARY(16)         NOT NULL,
  `service_flag`                  BIT(1)             NOT NULL  DEFAULT b'0'  COMMENT 'Good (0) or service (1)',
  `product_group_flag`            BIT(1)             NOT NULL  DEFAULT b'0'  COMMENT 'ProductGroup (1) or not (0)',
  `product_model_flag`            BIT(1)             NOT NULL  DEFAULT b'0'  COMMENT 'ProductModel (1) or not (0)',
  `individual_product_flag`       BIT(1)             NOT NULL  DEFAULT b'0'  COMMENT 'IndividualProduct (1) or not (0)',
  `availability_id`               TINYINT UNSIGNED   NOT NULL  DEFAULT 3,
  `condition_id`                  TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `gender_id`                     TINYINT UNSIGNED   NULL  DEFAULT NULL,
  `adult_consideration_id`        TINYINT UNSIGNED   NULL  DEFAULT NULL,
  `color_id`                      SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `material_id`                   SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `pattern_id`                    SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `version`                       SMALLINT UNSIGNED  NOT NULL  DEFAULT 1,
  `date_created`                  DATETIME           NOT NULL,
  `date_modified`                 DATETIME           NULL  DEFAULT NULL,
  `serial_number`                 VARCHAR(70)        COLLATE ascii_general_ci  NULL  DEFAULT NULL,
  `sku`                           VARCHAR(70)        COLLATE ascii_general_ci  NULL  DEFAULT NULL,
  `gtin`                          VARCHAR(14)        NULL  DEFAULT NULL,
  `asin`                          CHAR(10)           NULL  DEFAULT NULL,
  `release_date`                  DATE               NULL  DEFAULT NULL,
  `production_date`               DATE               NULL  DEFAULT NULL,
  `sales_discontinuation_date`    DATETIME           NULL  DEFAULT NULL,
  `support_discontinuation_date`  DATETIME           NULL  DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `uk_product_uuid` (`product_uuid`),
  FOREIGN KEY `fk_products_product_availability_types` (`availability_id`) REFERENCES `sc_product_availability_types` (`availability_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_product_condition_types` (`condition_id`) REFERENCES `sc_product_condition_types` (`condition_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_product_genders` (`gender_id`) REFERENCES `sc_product_genders` (`gender_id`)  ON DELETE SET NULL  ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_adult_considerations` (`adult_consideration_id`) REFERENCES `sc_adult_considerations` (`adult_consideration_id`)  ON DELETE SET NULL  ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_colors` (`color_id`) REFERENCES `sc_colors` (`color_id`)  ON DELETE NO ACTION  ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_product_materials` (`material_id`) REFERENCES `sc_product_materials` (`material_id`)  ON DELETE NO ACTION  ON UPDATE CASCADE,
  FOREIGN KEY `fk_products_product_patterns` (`pattern_id`) REFERENCES `sc_product_patterns` (`pattern_id`)  ON DELETE NO ACTION  ON UPDATE CASCADE,
  UNIQUE `uk_serial_number` (`serial_number`),
  INDEX `ix_product_identifiers` (`product_uuid` DESC, `serial_number` DESC, `sku` DESC, `gtin` DESC, `asin` DESC),
  INDEX `ix_release_date` (`release_date` DESC, `sales_discontinuation_date` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- EU energy efficiency.
-- @see https://schema.org/EUEnergyEfficiencyEnumeration
CREATE TABLE IF NOT EXISTS `sc_energy_efficiency_categories` (
  `id`     TINYINT UNSIGNED  NOT NULL,
  `name`   VARCHAR(32)       UNIQUE  NOT NULL,
  `value`  VARCHAR(51)       UNIQUE  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_energy_efficiency_categories`
    (`id`, `name`, `value`)
  VALUES
    ( 1, 'EUEnergyEfficiencyCategoryG',      'https://schema.org/EUEnergyEfficiencyCategoryG'),
    ( 2, 'EUEnergyEfficiencyCategoryF',      'https://schema.org/EUEnergyEfficiencyCategoryF'),
    ( 3, 'EUEnergyEfficiencyCategoryE',      'https://schema.org/EUEnergyEfficiencyCategoryE'),
    ( 4, 'EUEnergyEfficiencyCategoryD',      'https://schema.org/EUEnergyEfficiencyCategoryD'),
    ( 5, 'EUEnergyEfficiencyCategoryC',      'https://schema.org/EUEnergyEfficiencyCategoryC'),
    ( 6, 'EUEnergyEfficiencyCategoryB',      'https://schema.org/EUEnergyEfficiencyCategoryB'),
    ( 7, 'EUEnergyEfficiencyCategoryA',      'https://schema.org/EUEnergyEfficiencyCategoryA'),
    ( 8, 'EUEnergyEfficiencyCategoryA1Plus', 'https://schema.org/EUEnergyEfficiencyCategoryA1Plus'),
    ( 9, 'EUEnergyEfficiencyCategoryA2Plus', 'https://schema.org/EUEnergyEfficiencyCategoryA2Plus'),
    (10, 'EUEnergyEfficiencyCategoryA3Plus', 'https://schema.org/EUEnergyEfficiencyCategoryA3Plus');

-- @see https://schema.org/EnergyConsumptionDetails
CREATE TABLE IF NOT EXISTS `sc_energy_consumption_details` (
  `id`         SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `category`   TINYINT UNSIGNED   NULL,
  `scale_min`  TINYINT UNSIGNED   NULL,
  `scale_max`  TINYINT UNSIGNED   NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see http://schema.org/hasEnergyConsumptionDetails
CREATE TABLE IF NOT EXISTS `sc_product_energy_consumption_details` (
  `product_id`  INT UNSIGNED       NOT NULL,
  `details_id`  SMALLINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`product_id`, `details_id`),
  FOREIGN KEY (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`details_id`) REFERENCES `sc_energy_consumption_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Product groups.
-- @see https://schema.org/ProductGroup Schema.org type `ProductGroup`
-- @see https://schema.org/variesBy Schema.org property `ProductGroup.variesBy`
CREATE TABLE IF NOT EXISTS `sc_product_variants` (
  `varies_by_id`  TINYINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `varies_by`     VARCHAR(31)       NOT NULL,
  PRIMARY KEY (`varies_by_id`),
  UNIQUE KEY (`varies_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_product_variants`
    (`varies_by`)
  VALUES
    ('https://schema.org/color'),
    ('https://schema.org/depth'),
    ('https://schema.org/height'),
    ('https://schema.org/material'),
    ('https://schema.org/pattern'),
    ('https://schema.org/size'),
    ('https://schema.org/weight'),
    ('https://schema.org/width');

CREATE TABLE IF NOT EXISTS `sc_product_groups` (
  `product_id`    INT UNSIGNED      NOT NULL,
  `varies_by_id`  TINYINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`product_id`, `varies_by_id`),
  FOREIGN KEY `fk_product_groups_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_groups_product_variants` (`varies_by_id`) REFERENCES `sc_product_variants` (`varies_by_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Manufacturers of products.
-- @see https://schema.org/mpn Schema.org property `mpn`
-- @see https://support.google.com/merchants/answer/6324482?hl=en
CREATE TABLE IF NOT EXISTS `sc_product_manufacturers` (
  `product_id`       INT UNSIGNED        NOT NULL,
  `organization_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `mpn`              VARCHAR(70)         NULL,
  PRIMARY KEY (`product_id`, `organization_id`),
  FOREIGN KEY `fk_product_manufacturers_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_manufacturers_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_general_ci;

-- International and local product size systems.
CREATE TABLE IF NOT EXISTS `sc_product_size_systems` (
  `size_system_id`  VARCHAR(3)   NOT NULL,
  `description`     VARCHAR(30)  NULL,
  PRIMARY KEY (`size_system_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see https://support.google.com/merchants/answer/6324502?hl=en `size_system`
INSERT IGNORE INTO `sc_product_size_systems`
    (`size_system_id`, `description`)
  VALUES
    ('AU',  'Australia'),
    ('BR',  'Brasil'),
    ('CN',  'China'),
    ('DE',  'Germany'),
    ('EU',  'Europe'),
    ('FR',  'France'),
    ('IT',  'Italy'),
    ('JP',  'Japan'),
    ('MEX', 'Mexico'),
    ('UK',  'United Kingdom'),
    ('US',  'United States');

CREATE TABLE IF NOT EXISTS `sc_product_size_values` (
  `size_system_id`  VARCHAR(3)  NOT NULL,
  `size_value`      CHAR(100)   NOT NULL,
  `sort_order`      TINYINT     NOT NULL  DEFAULT 0,
  PRIMARY KEY (`size_system_id`, `size_value`),
  FOREIGN KEY `fk_product_size_values_product_size_systems` (`size_system_id`) REFERENCES `sc_product_size_systems` (`size_system_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_product_size_values_sort_order` (`size_system_id` ASC, `sort_order` ASC, `size_value` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Wearable size groups or “size types” describe the cut or body fit of a product.
-- @see https://support.google.com/merchants/answer/6324497?hl=en `size_type`
-- @see https://schema.org/WearableSizeGroupEnumeration `WearableSizeGroupEnumeration`
CREATE TABLE IF NOT EXISTS `sc_wearable_size_groups` (
  `id`     TINYINT UNSIGNED  NOT NULL,
  `name`   VARCHAR(10)       UNIQUE  NOT NULL,
  `value`  VARCHAR(46)       UNIQUE  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_wearable_size_groups`
    (`id`, `name`, `value`)
  VALUES
    ( 0, 'Regular',    'https://schema.org/WearableSizeGroupRegular'),
    ( 1, 'Big',        'https://schema.org/WearableSizeGroupBig'),
    ( 2, 'Boys',       'https://schema.org/WearableSizeGroupBoys'),
    ( 3, 'ExtraShort', 'https://schema.org/WearableSizeGroupExtraShort'),
    ( 4, 'ExtraTall',  'https://schema.org/WearableSizeGroupExtraTall'),
    ( 5, 'Girls',      'https://schema.org/WearableSizeGroupGirls'),
    ( 6, 'Husky',      'https://schema.org/WearableSizeGroupHusky'),
    ( 7, 'Infants',    'https://schema.org/WearableSizeGroupInfants'),
    ( 8, 'Juniors',    'https://schema.org/WearableSizeGroupJuniors'),
    ( 9, 'Maternity',  'https://schema.org/WearableSizeGroupMaternity'),
    (10, 'Mens',       'https://schema.org/WearableSizeGroupMens'),
    (11, 'Misses',     'https://schema.org/WearableSizeGroupMisses'),
    (12, 'Petite',     'https://schema.org/WearableSizeGroupPetite'),
    (13, 'Plus',       'https://schema.org/WearableSizeGroupPlus'),
    (14, 'Short',      'https://schema.org/WearableSizeGroupShort'),
    (15, 'Tall',       'https://schema.org/WearableSizeGroupTall'),
    (16, 'Womens',     'https://schema.org/WearableSizeGroupWomens');

-- Product sizes, consisting of a size value in a specific size system
-- and an OPTIONAL size type for the cut.
CREATE TABLE IF NOT EXISTS `sc_product_sizes` (
  `product_id`      INT UNSIGNED      NOT NULL,
  `size_system_id`  VARCHAR(3)        NOT NULL,
  `size_value`      VARCHAR(100)      NOT NULL,
  `size_type_id`    TINYINT UNSIGNED  NULL,
  PRIMARY KEY (`product_id`, `size_system_id`),
  FOREIGN KEY `fk_product_sizes_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_sizes_product_size_systems` (`size_system_id`) REFERENCES `sc_product_size_systems` (`size_system_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_sizes_size_types` (`size_type_id`) REFERENCES `sc_wearable_size_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  INDEX `ix_product_size` (`size_system_id`, `size_value`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Includes a wearable product in one or more wearable size groups,
-- ignoring the measerument size in a specific local size system.
CREATE TABLE IF NOT EXISTS `sc_product_wearable_size_groups` (
  `product_id`              INT UNSIGNED      NOT NULL,
  `wearable_size_group_id`  TINYINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`product_id`, `wearable_size_group_id`),
  FOREIGN KEY `fk_product_wearable_size_groups_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_wearable_size_groups` (`wearable_size_group_id`) REFERENCES `sc_wearable_size_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Product names and product descriptions
CREATE TABLE IF NOT EXISTS `sc_product_texts` (
  `description_id`  INT UNSIGNED   NOT NULL  AUTO_INCREMENT,
  `product_id`      INT UNSIGNED   NOT NULL,
  `language_id`     VARCHAR(13)    CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`            VARCHAR(150)   NULL  DEFAULT NULL,
  `alternate_name`  VARCHAR(150)   NULL  DEFAULT NULL,
  `keywords`        VARCHAR(252)   NULL  DEFAULT NULL  COMMENT 'Comma-separated list',
  `headline`        VARCHAR(252)   NULL  DEFAULT NULL,
  `slogan`          VARCHAR(252)   NULL  DEFAULT NULL,
  `summary`         VARCHAR(252)   NULL  DEFAULT NULL,
  `description`     VARCHAR(5000)  NULL  DEFAULT NULL  COMMENT 'Markdown',
  PRIMARY KEY (`description_id`),
  FOREIGN KEY `fk_product_texts_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_texts_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FULLTEXT INDEX `ix_product_texts` (`name`, `alternate_name`, `keywords`, `headline`, `slogan`, `summary`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Link product descriptions to specific stores
CREATE TABLE IF NOT EXISTS `sc_store_product_texts` (
  `store_id`        SMALLINT UNSIGNED  NOT NULL,
  `description_id`  INT UNSIGNED       NOT NULL,
  PRIMARY KEY (`store_id`, `description_id`),
  FOREIGN KEY `fk_store_product_texts_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_store_product_texts_product_texts` (`description_id`) REFERENCES `sc_product_texts` (`description_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

--
-- Units of measurement (UOM)
--
-- @see http://wiki.goodrelations-vocabulary.org/Documentation/UN/CEFACT_Common_Codes
--      Popular UN/CEFACT Common Codes for Units of Measurement
--
-- @see https://www.unece.org/fileadmin/DAM/cefact/recommendations/rec20/rec20.zip
--      Codes for Units of Measurement used in the International Trade
--
CREATE TABLE IF NOT EXISTS `sc_units_of_measurement` (
  `uom_id`      VARCHAR(3)   CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  COMMENT 'UN/CEFACT Common Code',
  `uom_symbol`  VARCHAR(14)  NOT NULL,
  `uom_name`    VARCHAR(30)  NOT NULL,
  PRIMARY KEY (`uom_id`),
  INDEX `ix_units_of_measurement` (`uom_symbol` ASC, `uom_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- International System of Units (SI)
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('AMP', 'A',   'ampere'),
    ('C62', '1',   'one'),
    ('CDL', 'cd',  'candela'),
    ('KEL', 'K',   'kelvin'),
    ('KGM', 'kg',  'kilogram'),
    ('MTR', 'm',   'metre'),
    ('C34', 'mol', 'mole'),
    ('SEC', 's',   'second');

-- Length units
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('C45', 'nm', 'nanometre'),
    ('CMT', 'cm', 'centimetre'),
    ('DMT', 'dm', 'decimetre'),
    ('FOT', 'ft', 'foot'),
    ('HMT', 'hm', 'hectometre'),
    ('INH', 'in', 'inch'),
    ('KMT', 'km', 'kilometre'),
    ('MMT', 'mm', 'millimetre'),
    ('YRD', 'yd', 'yard');

-- Volume units
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('CLT', 'cl',         'centilitre'),
    ('GLI', 'gal (US)',   'gallon (US)'),
    ('GLL', 'gal (UK)',   'gallon (UK)'),
    ('HLT', 'hl',         'hectolitre'),
    ('LTR', 'l',          'litre'),
    ('MLT', 'ml',         'millilitre'),
    ('MTQ', 'm³',         'cubic metre'),
    ('OZA', 'fl oz (US)', 'fluid ounce (US)'),
    ('OZI', 'fl oz (UK)', 'fluid ounce (UK)');

-- Weight units
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('GRM', 'g',  'gram'),
    ('LBR', 'lb', 'pound'),
    ('MGM', 'mg', 'milligram'),
    ('TNE', 't',  'tonne (metric ton)');

-- Time units
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('ANN', 'a',   'year'),
    ('C26', 'ms',  'millisecond'),
    ('DAY', 'd',   'day'),
    ('HUR', 'h',   'hour'),
    ('LH',  '',    'labour hour'),
    ('MIN', 'min', 'minute'),
    ('MON', 'mo',  'month'),
    ('OT',  '',    'overtime hour'),
    ('QAN', '',    'quarter (of a year)'),
    ('QH',  '',    'quarter hour'),
    ('SAN', '',    'half year (6 months)'),
    ('WEE', 'wk',  'week');

-- Other units
-- @see http://wiki.goodrelations-vocabulary.org/Documentation/UN/CEFACT_Common_Codes
INSERT IGNORE INTO `sc_units_of_measurement`
    (`uom_id`, `uom_symbol`, `uom_name`)
  VALUES
    ('28',  'kg/m²',     'kilogram per square metre'),
    ('2N',  'dB',        'decibel'),
    ('4H',  'μm',        'micrometre'),
    ('4K',  'mA',        'milliampere'),
    ('4P',  'N/m',       'newton per metre'),
    ('A24', 'cd/m²',     'candela per square metre'),
    ('A86', 'GHz',       'gigahertz'),
    ('A94', 'g/mol',     'gram per mole'),
    ('B22', 'kA',        'kiloampere'),
    ('B32', 'kg·m²',     'kilogram metre squared'),
    ('B43', 'kJ/(kg·K)', 'kilojoule per kilogram kelvin'),
    ('B49', 'kΩ',        'kiloohm'),
    ('B61', 'lm/W',      'lumen per watt'),
    ('BAR', 'bar',       'bar'),
    ('C16', 'mm/s',      'millimetre per second'),
    ('C24', 'mPa·s',     'millipascal second'),
    ('C65', 'Pa·s',      'pascal second'),
    ('C91', 'K⁻¹',       'reciprocal kelvin'),
    ('C94', 'min⁻¹',     'reciprocal minute'),
    ('CEL', '°C',        'degree Celsius'),
    ('CMQ', 'cm³',       'cubic centimetre'),
    ('D33', 'T',         'tesla'),
    ('D52', 'W/K',       'watt per kelvin'),
    ('D74', 'kg/mol',    'kilogram per mole'),
    ('DD',  '°',         'degree'),
    ('DPC', '',          'dozen piece'),
    ('DPR', '',          'dozen pair'),
    ('E01', 'N/cm²',     'newton per square centimetre'),
    ('E32', 'l/h',       'litre per hour'),
    ('EP',  '',          'eleven pack'),
    ('FAR', 'F',         'farad'),
    ('GM',  'g/m²',      'gram per square metre'),
    ('H2',  '',          'half litre'),
    ('HD',  '',          'half dozen'),
    ('HTZ', 'Hz',        'hertz'),
    ('JOU', 'J',         'joule'),
    ('KGS', 'kg/s',      'kilogram per second'),
    ('KHZ', 'kHz',       'kilohertz'),
    ('KL',  'kg/m',      'kilogram per metre'),
    ('KMQ', 'kg/m³',     'kilogram per cubic metre'),
    ('KVT', 'kV',        'kilovolt'),
    ('KWT', 'kW',        'kilowatt'),
    ('L2',  'l/min',     'litre per minute'),
    ('LUM', 'lm',        'lumen'),
    ('LUX', 'lx',        'lux'),
    ('MBR', 'mbar',      'millibar'),
    ('MHZ', 'MHz',       'megahertz'),
    ('MMK', 'mm²',       'square millimetre'),
    ('MMQ', 'mm³',       'cubic millimetre'),
    ('MPA', 'MPa',       'megapascal'),
    ('MQH', 'm³/h',      'cubic metre per hour'),
    ('MQS', 'm³/s',      'cubic metre per second'),
    ('MTK', 'm²',        'square metre'),
    ('MTS', 'm/s',       'metre per second'),
    ('NEW', 'N',         'newton'),
    ('NU',  'N·m',       'newton metre'),
    ('OHM', 'Ω',         'ohm'),
    ('OP',  '',          'two pack'),
    ('P1',  '%',         'percent'),
    ('P3',  '',          'three pack'),
    ('P4',  '',          'four pack'),
    ('P5',  '',          'five pack'),
    ('P6',  '',          'six pack'),
    ('P7',  '',          'seven pack'),
    ('P8',  '',          'eight pack'),
    ('P9',  '',          'nine pack'),
    ('PAL', 'Pa',        'pascal'),
    ('PR',  '',          'pair'),
    ('Q3',  '',          'meal'),
    ('QB',  '',          'page'),
    ('QD',  '',          'quarter dozen'),
    ('R4',  'cal',       'calorie'),
    ('TP',  'cal',       'ten pack'),
    ('VLT', 'V',         'volt'),
    ('WTT', 'W',         'watt');

-- Optional product and product shipment dimensions
-- @see https://support.google.com/merchants/answer/6324498?hl=en
CREATE TABLE IF NOT EXISTS `sc_product_dimensions` (
  `product_id`       INT UNSIGNED   NOT NULL,
  `uom_id`           VARCHAR(3)     NOT NULL  DEFAULT 'MTR',
  `product_width`    DECIMAL(18,9)  NULL  DEFAULT NULL,
  `product_height`   DECIMAL(18,9)  NULL  DEFAULT NULL,
  `product_depth`    DECIMAL(18,9)  NULL  DEFAULT NULL,
  `shipping_length`  DECIMAL(18,9)  NULL  DEFAULT NULL,
  `shipping_width`   DECIMAL(18,9)  NULL  DEFAULT NULL,
  `shipping_height`  DECIMAL(18,9)  NULL  DEFAULT NULL,
  PRIMARY KEY (`product_id`, `uom_id`),
  FOREIGN KEY `fk_product_dimensions_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_dimensions_units_of_measurement` (`uom_id`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Optional product and product shipment weights
-- @see https://support.google.com/merchants/answer/6324503?hl=en `shipping_weight`
CREATE TABLE IF NOT EXISTS `sc_product_weights` (
  `product_id`       INT UNSIGNED   NOT NULL,
  `uom_id`           VARCHAR(3)     NOT NULL  DEFAULT 'KGM',
  `product_weight`   DECIMAL(18,9)  NOT NULL,
  `shipping_weight`  DECIMAL(18,9)  NOT NULL,
  PRIMARY KEY (`product_id`, `uom_id`),
  FOREIGN KEY `fk_product_weights_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_weights_units_of_measurement` (`uom_id`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Price types
-- @see https://schema.org/PriceTypeEnumeration Schema.org enumeration `PriceTypeEnumeration`
CREATE TABLE IF NOT EXISTS `sc_price_types` (
  `id`          BIT(6)       NOT NULL,
  `price_type`  VARCHAR(41)  NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_price_type` (`price_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_price_types`
    (`id`, `price_type`)
  VALUES
    (b'000001', 'https://schema.org/MinimumAdvertisedPrice'),
    (b'000010', 'https://schema.org/MSRP'),
    (b'000100', 'https://schema.org/SRP'),
    (b'001000', 'https://schema.org/ListPrice'),
    (b'010000', 'https://schema.org/SalePrice'),
    (b'100000', 'https://schema.org/InvoicePrice');

-- Price specifications
-- @see https://schema.org/PriceSpecification Schema.org type `PriceSpecification`
CREATE TABLE IF NOT EXISTS `sc_price_specifications` (
  `price_specification_id`        BIGINT UNSIGNED    NOT NULL  AUTO_INCREMENT,
  `price_type`                    BIT(6)             NULL  DEFAULT NULL,
  `vat_included_flag`             BIT(1)             NULL  DEFAULT b'1',
  `currency_id`                   SMALLINT UNSIGNED  NOT NULL  DEFAULT 978,
  `unit_code`                     VARCHAR(3)         NULL  DEFAULT NULL,
  `eligible_quantity_unit_code`   VARCHAR(3)         NULL  DEFAULT NULL,
  `eligible_quantity_value`       DECIMAL(20,6)      NULL  DEFAULT NULL,
  `eligible_transaction_volume`   BIGINT UNSIGNED    NULL  DEFAULT NULL,
  `reference_quantity_unit_code`  VARCHAR(3)         NULL  DEFAULT NULL,
  `reference_quantity_value`      DECIMAL(20,6)      NULL  DEFAULT NULL,
  `from_date`                     DATETIME           NULL  DEFAULT NULL,
  `thru_date`                     DATETIME           NULL  DEFAULT NULL,
  `min_price`                     DECIMAL(20,6)      NULL  DEFAULT NULL,
  `price`                         DECIMAL(20,6)      NULL  DEFAULT NULL,
  `max_price`                     DECIMAL(20,6)      NULL  DEFAULT NULL,
  `serialized_php_object`         BLOB               NULL  DEFAULT NULL,
  PRIMARY KEY (`price_specification_id`),
  FOREIGN KEY `fk_price_specifications_currencies` (`currency_id`) REFERENCES `sc_currencies` (`currency_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_price_specifications_price_types` (`price_type`) REFERENCES `sc_price_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_price_specifications_units_of_measurement` (`unit_code`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_eligible_quantity_unit_code` (`eligible_quantity_unit_code`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_reference_quantity_unit_code` (`reference_quantity_unit_code`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_eligible_transaction_volume` (`eligible_transaction_volume`) REFERENCES `sc_price_specifications` (`price_specification_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Product prices
CREATE TABLE IF NOT EXISTS `sc_product_prices` (
  `product_id`              INT UNSIGNED       NOT NULL,
  `price_specification_id`  BIGINT UNSIGNED    NOT NULL,
  `from_date`               DATETIME           NOT NULL,
  `thru_date`               DATETIME           NULL  DEFAULT NULL,
  `store_id`                SMALLINT UNSIGNED  NULL  DEFAULT NULL  COMMENT 'Optional filter',
  `customer_group_id`       SMALLINT UNSIGNED  NULL  DEFAULT NULL  COMMENT 'Optional filter',
  PRIMARY KEY (`product_id`, `price_specification_id`),
  FOREIGN KEY `fk_product_prices_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_prices_price_specifications` (`price_specification_id`) REFERENCES `sc_price_specifications` (`price_specification_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_prices_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_prices_customer_groups` (`customer_group_id`) REFERENCES `sc_customer_groups` (`customer_group_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Products per brand
CREATE TABLE IF NOT EXISTS `sc_brand_products` (
  `brand_id`    SMALLINT UNSIGNED  NOT NULL,
  `product_id`  INT UNSIGNED       NOT NULL,
  PRIMARY KEY (`brand_id`, `product_id`),
  FOREIGN KEY `fk_brand_products_brands` (`brand_id`) REFERENCES `sc_brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_brand_products_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Products in categories
CREATE TABLE IF NOT EXISTS `sc_category_products` (
  `category_id`   SMALLINT UNSIGNED  NOT NULL,
  `product_id`    INT UNSIGNED       NOT NULL,
  `primary_flag`  BIT(1)             NOT NULL  DEFAULT b'0',
  `from_date`     DATETIME           NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`     DATETIME           NULL  DEFAULT NULL,
  PRIMARY KEY (`category_id`, `product_id`),
  FOREIGN KEY `fk_category_products_categories` (`category_id`) REFERENCES `sc_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_category_products_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


-- Tags or labels.
-- @see https://html.spec.whatwg.org/multipage/links.html#link-type-tag
CREATE TABLE IF NOT EXISTS `sc_tags` (
  `tag_id`    SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `tag_uuid`  BINARY(16)         NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `uk_tag_uuid` (`tag_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_tag_keywords` (
  `tag_id`       SMALLINT UNSIGNED  NOT NULL,
  `language_id`  VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `keyword`      VARCHAR(255)       NOT NULL,
  PRIMARY KEY (`tag_id`, `language_id`),
  FOREIGN KEY `fk_tag_keywords_tags` (`tag_id`) REFERENCES `sc_tags` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_tag_keywords_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_product_tags` (
  `product_id`  INT UNSIGNED       NOT NULL,
  `tag_id`      SMALLINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`product_id`, `tag_id`),
  FOREIGN KEY `fk_product_tags_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_tags_tags` (`tag_id`) REFERENCES `sc_tags` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


-- Product attributes.
CREATE TABLE IF NOT EXISTS `sc_attributes` (
  `attribute_id`  MEDIUMINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `uom_id`        VARCHAR(3)          NOT NULL  DEFAULT 'C62',
  `enabled_flag`  BIT(1)              NOT NULL  DEFAULT b'1',
  PRIMARY KEY (`attribute_id`),
  FOREIGN KEY `fk_attributes_units_of_measurement` (`uom_id`) REFERENCES `sc_units_of_measurement` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_attribute_descriptions` (
  `attribute_id`  MEDIUMINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `language_id`   VARCHAR(13)         CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`          VARCHAR(252)        NOT NULL,
  `description`   VARCHAR(252)        NOT NULL,
  PRIMARY KEY (`attribute_id`, `language_id`),
  FOREIGN KEY `fk_attribute_descriptions_attributes` (`attribute_id`) REFERENCES `sc_attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_attribute_descriptions_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- @see https://schema.org/additionalProperty Schema.org property `additionalProperty`
CREATE TABLE IF NOT EXISTS `sc_product_attributes` (
  `product_id`    INT UNSIGNED        NOT NULL,
  `attribute_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `from_date`     DATETIME            NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`     DATETIME            NULL  DEFAULT NULL,
  `num_value`     DECIMAL(18,9)       NULL  DEFAULT NULL,
  `str_value`     VARCHAR(252)        NULL  DEFAULT NULL,
  PRIMARY KEY (`product_id`, `attribute_id`),
  FOREIGN KEY `fk_product_attributes_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_attributes_attributes` (`attribute_id`) REFERENCES `sc_attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- `shared_flag`: attribute applies to all products in a category;
-- `mandatory_flag`: attribute MUST be set on all products in a category;
-- `common_flag`: attribute applies to many products, but not all.
CREATE TABLE IF NOT EXISTS `sc_category_attributes` (
  `category_id`     SMALLINT UNSIGNED   NOT NULL,
  `attribute_id`    MEDIUMINT UNSIGNED  NOT NULL,
  `shared_flag`     BIT(1)              NOT NULL  DEFAULT b'0',
  `mandatory_flag`  BIT(1)              NOT NULL  DEFAULT b'0',
  `common_flag`     BIT(1)              NOT NULL  DEFAULT b'0',
  PRIMARY KEY (`category_id`, `attribute_id`),
  FOREIGN KEY `fk_category_attributes_categories` (`category_id`) REFERENCES `sc_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_category_attributes_attributes` (`attribute_id`) REFERENCES `sc_attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Attribute groups and attribute filters
CREATE TABLE IF NOT EXISTS `sc_attribute_groups` (
  `group_id`           SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `group_name`         VARCHAR(252)       NOT NULL,
  `group_description`  VARCHAR(252)       NULL  DEFAULT NULL,
  PRIMARY KEY (group_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_attribute_group_attributes` (
  `group_id`      SMALLINT UNSIGNED   NOT NULL,
  `attribute_id`  MEDIUMINT UNSIGNED  NOT NULL,
  `from_date`     DATETIME            NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`     DATETIME            NULL  DEFAULT NULL,
  PRIMARY KEY (`group_id`, `attribute_id`),
  FOREIGN KEY `fk_attribute_group_attributes_attribute_groups` (`group_id`) REFERENCES `sc_attribute_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_attribute_group_attributes_attributes` (`attribute_id`) REFERENCES `sc_attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_attribute_filters` (
  `group_id`         SMALLINT UNSIGNED  NOT NULL,
  `enabled_flag`     BIT(1)             NOT NULL  DEFAULT b'0',
  `filter_position`  TINYINT UNSIGNED   DEFAULT 0,
  PRIMARY KEY (`group_id`),
  FOREIGN KEY `fk_attribute_filters_attribute_groups` (`group_id`) REFERENCES `sc_attribute_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_attribute_filter_descriptions` (
  `group_id`     SMALLINT UNSIGNED  NOT NULL,
  `language_id`  VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `description`  VARCHAR(252)       NOT NULL,
  PRIMARY KEY (`group_id`, `language_id`),
  FOREIGN KEY `fk_attribute_filter_descriptions_attribute_filters` (`group_id`) REFERENCES `sc_attribute_filters` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_attribute_filter_descriptions_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Product associations
CREATE TABLE IF NOT EXISTS `sc_product_association_types` (
  `association_type_id`   BIT(6)      NOT NULL,
  `association_property`  VARCHAR(31) NOT NULL,
  PRIMARY KEY (`association_type_id`),
  UNIQUE KEY (`association_property`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_product_association_types` (`association_type_id`, `association_property`) VALUES
  (b'000000', 'isRelatedTo'),
  (b'000001', 'isSimilarTo'),
  (b'000010', 'isAccessoryOrSparePartFor'),
  (b'000100', 'isConsumableFor'),
  (b'001000', 'predecessorOf'),
  (b'010000', 'successorOf'),
  (b'100000', 'isVariantOf');

CREATE TABLE IF NOT EXISTS `sc_product_associations` (
  `product_id`             INT UNSIGNED   NOT NULL,
  `association_type_id`    BIT(6)         NOT NULL  DEFAULT b'000000',
  `associated_product_id`  INT UNSIGNED   NOT NULL,
  `from_date`              DATETIME       NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`              DATETIME       NULL  DEFAULT NULL,
  PRIMARY KEY (`product_id`, `association_type_id`, `associated_product_id`),
  FOREIGN KEY `fk_product_associations_products_product_id` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_associations_product_association_types` (`association_type_id`) REFERENCES `sc_product_association_types` (`association_type_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_associations_products_associated_product_id` (`associated_product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Product bundles for cross-selling
CREATE TABLE IF NOT EXISTS `sc_product_bundles` (
  `bundle_id`        MEDIUMINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `bundle_discount`  DECIMAL(4,3)        NULL  DEFAULT NULL,
  PRIMARY KEY (`bundle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_product_bundle_stores` (
  `bundle_id`        MEDIUMINT UNSIGNED  NOT NULL,
  `store_id`         SMALLINT UNSIGNED   NOT NULL,
  `from_date`        DATETIME            NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`        DATETIME            NULL  DEFAULT NULL,
  `bundle_discount`  DECIMAL(4,3)        NULL  DEFAULT NULL,
  PRIMARY KEY (`bundle_id`, `store_id`),
  FOREIGN KEY `fk_product_bundle_stores_product_bundles` (`bundle_id`) REFERENCES `sc_product_bundles` (`bundle_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_bundle_stores_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_product_bundle_products` (
  `bundle_id`         MEDIUMINT UNSIGNED  NOT NULL,
  `product_id`        INT UNSIGNED        NOT NULL,
  `display_position`  TINYINT UNSIGNED    NOT NULL  DEFAULT 0,
  `total_quantity`    SMALLINT UNSIGNED   NOT NULL  DEFAULT 1,
  `free_quantity`     SMALLINT UNSIGNED   NOT NULL  DEFAULT 0,
  `product_discount`  DECIMAL(4,3)        NULL  DEFAULT NULL,
  PRIMARY KEY (`bundle_id`, `product_id`),
  FOREIGN KEY `fk_product_bundle_products_product_bundles` (`bundle_id`) REFERENCES `sc_product_bundles` (`bundle_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_bundle_products_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_product_bundle_products_display_position` (`display_position` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Images and other media files.
--

-- Media MIME types.
-- @see http://www.iana.org/assignments/media-types/media-types.xhtml
-- @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
-- @see https://schema.org/encodingFormat
CREATE TABLE IF NOT EXISTS `sc_media_types` (
  `media_type_id`  TINYINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `mime_type`      VARCHAR(31)       NOT NULL,
  PRIMARY KEY (`media_type_id`),
  UNIQUE KEY `uk_mime_type` (`mime_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_media_types` (`mime_type`) VALUES
  ('application/pdf'),
  ('image/apng'),
  ('image/avif'),
  ('image/gif'),
  ('image/jpeg'),
  ('image/png'),
  ('image/svg+xml'),
  ('image/webp'),
  ('video/mpeg'),
  ('video/webm');

CREATE TABLE IF NOT EXISTS `sc_media_files` (
  `media_file_id`    INT UNSIGNED       NOT NULL  AUTO_INCREMENT,
  `parent_id`        INT UNSIGNED       NULL  DEFAULT NULL,
  `media_type_id`    TINYINT UNSIGNED   NULL  DEFAULT NULL,
  `content_hash`     CHAR(64)           CHARACTER SET ascii  COLLATE ascii_bin  NULL  DEFAULT NULL  COMMENT 'SHA-2 SHA256 hash',
  `width`            SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `height`           SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `upload_date`      DATE               NULL  DEFAULT NULL,
  `upload_filename`  VARCHAR(252)       NULL  DEFAULT NULL,
  PRIMARY KEY (`media_file_id`),
  FOREIGN KEY `fk_media_files_media_files` (`parent_id`) REFERENCES `sc_media_files` (`media_file_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (`media_type_id`) REFERENCES `sc_media_types` (`media_type_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  INDEX `ix_media_files_width_height` (`width`, `height`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_product_media` (
  `product_media_id`  INT UNSIGNED       NOT NULL  AUTO_INCREMENT,
  `media_file_id`     INT UNSIGNED       NOT NULL,
  `product_id`        INT UNSIGNED       NOT NULL,
  `store_id`          SMALLINT UNSIGNED  NOT NULL,
  `display_position`  TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  PRIMARY KEY (`product_media_id`),
  FOREIGN KEY `fk_product_media_media_files` (`media_file_id`) REFERENCES `sc_media_files` (`media_file_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_media_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_media_stores` (`store_id`) REFERENCES `sc_stores` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_product_media_display_position` (`display_position`, `product_media_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_product_media_texts` (
  `product_media_id`  INT UNSIGNED  NOT NULL,
  `language_id`       VARCHAR(13)   CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `seo_url_hash`      CHAR(40)      NULL  DEFAULT NULL,
  `seo_url`           VARCHAR(252)  NULL  DEFAULT NULL,
  `alternative_text`  VARCHAR(252)  NULL  DEFAULT NULL  COMMENT 'HTML img alt attribute',
  `figure_caption`    VARCHAR(252)  NULL  DEFAULT NULL  COMMENT 'HTML figcaption element',
  PRIMARY KEY (`product_media_id`, `language_id`),
  FOREIGN KEY `fk_product_media_texts_product_media` (`product_media_id`) REFERENCES `sc_product_media` (`product_media_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_media_texts_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE KEY `uk_product_media_seo_url_hash` (`seo_url_hash`),
  UNIQUE KEY `uk_product_media_seo_url` (`seo_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;


--
-- Audiences.
--
-- @see https://schema.org/Audience Schema.org `Audience`
-- @see https://schema.org/PeopleAudience `PeopleAudience` extends `Audience`
-- @see https://schema.org/ParentAudience `ParentAudience` extends `PeopleAudience`
-- @see https://github.com/schemaorg/schemaorg/issues/2898 Issue on gender
-- @see https://www.iso.org/news/ref2512.html ISO on gender neutrality
--
CREATE TABLE IF NOT EXISTS `sc_audience_types` (
  `audience_type_id`  TINYINT UNSIGNED  NOT NULL,
  `audience_type`     VARCHAR(31)       NOT NULL,
  PRIMARY KEY (`audience_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_audience_types`
    (`audience_type_id`, `audience_type`)
  VALUES
    (  0, 'Audience'),
    (  1, 'BusinessAudience'),
    (  2, 'EducationalAudience'),
    (  3, 'MedicalAudience'),
    (  4, 'ParentAudience'),
    (  5, 'Patient'),
    (  6, 'PeopleAudience'),
    (  7, 'Researcher');

-- @see https://schema.org/MedicalCondition `MedicalCondition` for `MedicalAudience.healthCondition`
-- @see https://developers.google.com/gmail/markup/reference/types/MedicalCondition
-- @see https://schema.org/healthCondition
CREATE TABLE IF NOT EXISTS `sc_medical_conditions` (
  `medical_condition_id`    SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `medical_condition_uuid`  BINARY(16)         NOT NULL,
  `medical_condition`       TEXT               NULL  DEFAULT NULL,
  PRIMARY KEY (`medical_condition_id`),
  UNIQUE KEY (`medical_condition_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_bin;

-- Table for `MedicalCondition.name` and `MedicalCondition.alternateName` in
-- multiple languages.  Other `MedicalCondition` properties are stored
-- as JSON in the `sc_medical_conditions,medical_condition` column.
CREATE TABLE IF NOT EXISTS `sc_medical_conditions_texts` (
  `medical_condition_id`  SMALLINT UNSIGNED  NOT NULL,
  `language_id`           VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`                  VARCHAR(127)       NULL  DEFAULT NULL,
  `alternate_name`        VARCHAR(127)       NULL  DEFAULT NULL,
  PRIMARY KEY (`medical_condition_id`, `language_id`),
  FOREIGN KEY `fk_medical_conditions_texts_medical_conditions` (`medical_condition_id`) REFERENCES `sc_medical_conditions` (`medical_condition_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_medical_conditions_texts_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_medical_condition_names` (`name` ASC, `alternate_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_audiences` (
  `audience_id`           SMALLINT UNSIGNED    NOT NULL  AUTO_INCREMENT,
  `audience_uuid`         BINARY(16)           NOT NULL,
  `audience_type_id`      TINYINT UNSIGNED     NOT NULL  DEFAULT 0,
  `medical_condition_id`  SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `date_created`          DATETIME             NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`         DATETIME             NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `required_gender`       TINYINT(1) UNSIGNED  NULL  DEFAULT NULL,
  `suggested_gender`      TINYINT(1) UNSIGNED  NULL  DEFAULT NULL,
  `required_min_age`      SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `suggested_min_age`     SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `child_min_age`         SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `required_max_age`      SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `suggested_max_age`     SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  `child_max_age`         SMALLINT UNSIGNED    NULL  DEFAULT NULL,
  PRIMARY KEY (`audience_id`),
  UNIQUE KEY (`audience_uuid`),
  FOREIGN KEY `fk_audiences_audience_types` (`audience_type_id`) REFERENCES `sc_audience_types` (`audience_type_id`)  ON DELETE NO ACTION  ON UPDATE CASCADE,
  FOREIGN KEY `fk_audiences_medical_conditions` (`medical_condition_id`) REFERENCES `sc_medical_conditions` (`medical_condition_id`)  ON DELETE SET NULL  ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_audience_texts` (
  `audience_id`                 SMALLINT UNSIGNED  NOT NULL,
  `language_id`                 VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `name`                        VARCHAR(127)       NULL  DEFAULT NULL,
  `alternate_name`              VARCHAR(127)       NULL  DEFAULT NULL,
  `description`                 VARCHAR(255)       NULL  DEFAULT NULL,
  `disambiguating_description`  VARCHAR(255)       NULL  DEFAULT NULL,
  PRIMARY KEY (`audience_id`, `language_id`),
  FOREIGN KEY `fk_audience_texts_audiences` (`audience_id`) REFERENCES `sc_audiences`(`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_audience_texts_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `ix_audience_names` (`name` ASC, `alternate_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

-- Build audiences using customer groups, organizations, and persons.
-- A organization or person MAY be targetted through a customer group.
CREATE TABLE IF NOT EXISTS `sc_audience_customer_groups` (
  `audience_id`        SMALLINT UNSIGNED  NOT NULL,
  `customer_group_id`  SMALLINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`audience_id`, `customer_group_id`),
  FOREIGN KEY `fk_audience_customer_groups_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_audience_customer_groups_customer_groups` (`customer_group_id`) REFERENCES `sc_customer_groups` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_audience_organizations` (
  `audience_id`      SMALLINT UNSIGNED   NOT NULL,
  `organization_id`  MEDIUMINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`audience_id`, `organization_id`),
  FOREIGN KEY `fk_audience_organizations_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_audience_organizations_organizations` (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_audience_persons` (
  `audience_id`  SMALLINT UNSIGNED  NOT NUll,
  `person_id`    INT UNSIGNED       NOT NULL,
  PRIMARY KEY (`audience_id`, `person_id`),
  FOREIGN KEY `fk_audience_persons_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_audience_persons_persons` (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- Target audiences with selected brands, product categories,
-- and individual products.
CREATE TABLE IF NOT EXISTS `sc_brand_audiences` (
  `brand_id`     SMALLINT UNSIGNED  NOT NULL,
  `audience_id`  SMALLINT UNSIGNED  NOT NUll,
  PRIMARY KEY (`brand_id`, `audience_id`),
  FOREIGN KEY `fk_brand_audiences_brands` (`brand_id`) REFERENCES `sc_brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_brand_audiences_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_category_audiences` (
  `category_id`  SMALLINT UNSIGNED  NOT NULL,
  `audience_id`  SMALLINT UNSIGNED  NOT NUll,
  PRIMARY KEY (`category_id`, `audience_id`),
  FOREIGN KEY `fk_category_audiences_categories` (`category_id`) REFERENCES `sc_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_category_audiences_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_product_audiences` (
  `product_id`   INT UNSIGNED       NOT NULL,
  `audience_id`  SMALLINT UNSIGNED  NOT NUll,
  PRIMARY KEY (`product_id`, `audience_id`),
  FOREIGN KEY `fk_product_audiences_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_product_audiences_audiences` (`audience_id`) REFERENCES `sc_audiences` (`audience_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Order Management & Logistics (OML)
--
CREATE TABLE IF NOT EXISTS `sc_order_status` (
  `order_status_id`  BIT(4)       NOT NULL,
  `name`             VARCHAR(31)  NOT NULL,
  `value`            VARCHAR(63)  NOT NULL,
  PRIMARY KEY (`order_status_id`),
  INDEX `ix_order_status` (`name` ASC, `value` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see https://schema.org/OrderStatus Schema.org enumeration `OrderStatus`
INSERT IGNORE INTO `sc_order_status` VALUES
  (b'0001', 'OrderCancelled',       'https://schema.org/OrderCancelled'),
  (b'0010', 'OrderDelivered',       'https://schema.org/OrderDelivered'),
  (b'0011', 'OrderInTransit',       'https://schema.org/OrderInTransit'),
  (b'0100', 'OrderPaymentDue',      'https://schema.org/OrderPaymentDue'),
  (b'0101', 'OrderPickupAvailable', 'https://schema.org/OrderPickupAvailable'),
  (b'0110', 'OrderProblem',         'https://schema.org/OrderProblem'),
  (b'0111', 'OrderProcessing',      'https://schema.org/OrderProcessing'),
  (b'1000', 'OrderReturned',        'https://schema.org/OrderReturned');

CREATE TABLE IF NOT EXISTS `sc_orders` (
  `order_id`             INT UNSIGNED          NOT NULL  AUTO_INCREMENT,
  `order_uuid`           BINARY(16)            NOT NULL,
  `currency_id`          SMALLINT(3) UNSIGNED  NOT NULL  DEFAULT 978,
  `order_status_id`      BIT(4)                NULL  DEFAULT NULL,
  `wishlist_flag`        BIT(1)                NOT NULL  DEFAULT b'0',
  `preorder_flag`        BIT(1)                NOT NULL  DEFAULT b'0',
  `backorder_flag`       BIT(1)                NOT NULL  DEFAULT b'0',
  `customer_uuid`        BINARY(16)            NULL  DEFAULT NULL,
  `broker_uuid`          BINARY(16)            NULL  DEFAULT NULL,
  `seller_uuid`          BINARY(16)            NULL  DEFAULT NULL,
  `order_number`         VARCHAR(31)           COLLATE ascii_general_ci  NULL  DEFAULT NULL,
  `confirmation_number`  VARCHAR(31)           COLLATE ascii_general_ci  NULL  DEFAULT NULL,
  `cart_uuid`            BINARY(16)            NULL  DEFAULT NULL,
  `cart_rand`            CHAR(192)             NULL  DEFAULT NULL,
  `version`              SMALLINT UNSIGNED     NOT NULL  DEFAULT 1,
  `date_created`         DATETIME              NOT NULL,
  `date_modified`        DATETIME              NULL  DEFAULT NULL,
  `date_confirmed`       DATETIME              NULL  DEFAULT NULL,
  `date_cancelled`       DATETIME              NULL  DEFAULT NULL,
  `date_deleted`         DATETIME              NULL  DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `uk_order_uuid` (`order_uuid`),
  UNIQUE KEY `uk_order_number` (`order_number` DESC),
  FOREIGN KEY `fk_orders_currencies` (`currency_id`) REFERENCES `sc_currencies` (`currency_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_orders_order_status` (`order_status_id`) REFERENCES `sc_order_status` (`order_status_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_orders_customers` (`customer_uuid`) REFERENCES `sc_customers` (`customer_uuid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX `ix_confirmed_orders` (`date_confirmed` DESC, `confirmation_number` DESC, `order_number` DESC),
  INDEX `ix_cart` (`cart_uuid`, `cart_rand`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_order_items` (
  `order_id`         INT UNSIGNED       NOT NULL,
  `product_id`       INT UNSIGNED       NOT NULL,
  `order_status_id`  BIT(4)             NULL  DEFAULT NULL,
  `sort_order`       TINYINT UNSIGNED   NOT NULL  DEFAULT 0,
  `date_added`       DATETIME           NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_modified`    DATETIME           NULL  DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP,
  `date_deleted`     DATETIME           NULL  DEFAULT NULL,
  `units`            SMALLINT UNSIGNED  NOT NULL  DEFAULT 1,
  `unit_price`       DECIMAL(18,9)      NOT NULL  DEFAULT 0,
  PRIMARY KEY (`order_id`, `product_id`),
  FOREIGN KEY `fk_order_items_orders` (`order_id`) REFERENCES `sc_orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_order_items_products` (`product_id`) REFERENCES `sc_products` (`product_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_order_items_order_status` (`order_status_id`) REFERENCES `sc_order_status` (`order_status_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX `ix_order_items_sort_order` (`sort_order` ASC, `date_added` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_shipping_carriers` (
  `carrier_id`           TINYINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `carrier_uuid`         BINARY(16)        NOT NULL,
  `global_carrier_name`  VARCHAR(127)      NOT NULL  DEFAULT '',
  `from_date`            DATETIME          NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`            DATETIME          NULL  DEFAULT NULL,
  PRIMARY KEY (`carrier_id`),
  UNIQUE KEY (`carrier_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_shipping_carrier_descriptions` (
  `carrier_id`          TINYINT UNSIGNED  NOT NULL,
  `language_id`         VARCHAR(13)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `local_carrier_name`  VARCHAR(127)      NOT NULL  DEFAULT '',
  `description`         VARCHAR(255)      NULL  DEFAULT NULL,
  PRIMARY KEY (`carrier_id`, `language_id`),
  FOREIGN KEY `fk_shipping_carrier_descriptions_shipping_carriers` (`carrier_id`) REFERENCES `sc_shipping_carriers` (`carrier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_shipping_carrier_descriptions_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_shipments` (
  `shipment_id`     INT UNSIGNED      NOT NULL  AUTO_INCREMENT,
  `shipment_uuid`   BINARY(16)        NOT NULL,
  `address_id`      INT UNSIGNED      NOT NULL,
  `carrier_id`      TINYINT UNSIGNED  NOT NULL,
  `date_created`    DATETIME          NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_notified`   DATETIME          NULL  DEFAULT NULL,
  `date_picked`     DATETIME          NULL  DEFAULT NULL,
  `date_packed`     DATETIME          NULL  DEFAULT NULL,
  `date_shipped`    DATETIME          NULL  DEFAULT NULL,
  `date_delivered`  DATETIME          NULL  DEFAULT NULL,
  `tracking_code`   VARCHAR(255)      NULL  DEFAULT NULL,
  `tracking_uri`    VARCHAR(255)      NULL  DEFAULT NULL,
  PRIMARY KEY (`shipment_id`),
  UNIQUE KEY `uk_shipment_uuid` (`shipment_uuid`),
  FOREIGN KEY `fk_shipments_addresses` (`address_id`) REFERENCES `sc_addresses` (`address_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_shipments_shipping_carriers` (`carrier_id`) REFERENCES `sc_shipping_carriers` (`carrier_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_order_shipments` (
  `order_id`     INT UNSIGNED  NOT NULL,
  `shipment_id`  INT UNSIGNED  NOT NULL,
  PRIMARY KEY (`order_id`, `shipment_id`),
  FOREIGN KEY `fk_order_shipments_orders` (`order_id`) REFERENCES `sc_orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_order_shipments_shipments` (`shipment_id`) REFERENCES `sc_shipments` (`shipment_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_invoices` (
  `invoice_id`      INT UNSIGNED          NOT NULL,
  `invoice_uuid`    BINARY(16)            NOT NULL,
  `currency_id`     SMALLINT(3) UNSIGNED  NOT NULL  DEFAULT 978,
  `invoice_date`    DATE                  NOT NULL,
  `invoice_number`  INT(10) UNSIGNED      NULL  DEFAULT NULL,
  `terms_days`      TINYINT UNSIGNED      NOT NULL  DEFAULT 30,
  `subtotal`        DECIMAL(18,3)         NULL  DEFAULT NULL,
  `total`           DECIMAL(18,3)         NULL  DEFAULT NULL,
  `amount_due`      DECIMAL(18,3)         NULL  DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  UNIQUE KEY `uk_invoice_uuid` (`invoice_uuid`),
  FOREIGN KEY fk_invoices_currencies (currency_id) REFERENCES sc_currencies (currency_id) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX ix_invoice_date (invoice_date DESC, invoice_number DESC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_invoice_orders` (
  `invoice_id`  INT UNSIGNED  NOT NULL,
  `order_id`    INT UNSIGNED  NOT NULL,
  PRIMARY KEY (`invoice_id`, `order_id`),
  FOREIGN KEY `fk_invoice_orders_invoices` (`invoice_id`) REFERENCES `sc_invoices` (`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_invoice_orders_orders` (`order_id`) REFERENCES `sc_orders` (`order_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Payments.
--
-- Financial transactions are handled as payments through payments services
-- provided by payment service providers (PSP’s).  Payments MAY be linked to
-- orders as well as invoices.
--
CREATE TABLE IF NOT EXISTS `sc_payment_service_providers` (
  `payment_service_provider_id`  SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `global_provider_name`         VARCHAR(255)       NOT NULL  DEFAULT '',
  PRIMARY KEY (`payment_service_provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS sc_payment_services (
  `payment_service_id`           SMALLINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `payment_service_provider_id`  SMALLINT UNSIGNED  NULL  DEFAULT NULL,
  `global_service_name`          VARCHAR(255)       NOT NULL  DEFAULT '',
  `from_date`                    DATETIME           NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `thru_date`                    DATETIME           NULL  DEFAULT NULL,
  PRIMARY KEY (`payment_service_id`),
  FOREIGN KEY `fk_payment_services_payment_service_providers` (`payment_service_provider_id`) REFERENCES `sc_payment_service_providers` (`payment_service_provider_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_payment_service_descriptions` (
  `payment_service_id`  SMALLINT UNSIGNED  NOT NULL,
  `language_id`         VARCHAR(13)        CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `local_service_name`  VARCHAR(255)       NOT NULL  DEFAULT '',
  `description`         VARCHAR(255)       NULL  DEFAULT NULL,
  PRIMARY KEY (`payment_service_id`, `language_id`),
  FOREIGN KEY `fk_payment_service_descriptions_payment_services` (`payment_service_id`) REFERENCES `sc_payment_services` (`payment_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_payment_service_descriptions_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_payments` (
  `payment_id`          INT UNSIGNED          NOT NULL  AUTO_INCREMENT,
  `payment_uuid`        BINARY(16)            NOT NULL,
  `currency_id`         SMALLINT(3) UNSIGNED  NOT NULL  DEFAULT 978,
  `payment_service_id`  SMALLINT(5) UNSIGNED  NULL  DEFAULT NULL,
  `credit_flag`         BIT(1)                NOT NULL  DEFAULT b'0'  COMMENT 'Debit (0) or credit (1)',
  `transaction_id`      VARCHAR(128)          NULL  DEFAULT NULL,
  `transaction_date`    DATETIME              NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `amount`              DECIMAL(18,3)         NOT NULL,
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `uk_payment_uuid` (`payment_uuid`),
  FOREIGN KEY `fk_payments_currencies` (`currency_id`) REFERENCES `sc_currencies` (`currency_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY `fk_payments_payment_services` (`payment_service_id`) REFERENCES `sc_payment_services` (`payment_service_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX `ix_transaction_id` (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_order_payments` (
  `order_id`    INT UNSIGNED  NOT NULL,
  `payment_id`  INT UNSIGNED  NOT NULL,
  PRIMARY KEY (`order_id`, `payment_id`),
  FOREIGN KEY `fk_order_payments_orders` (`order_id`) REFERENCES `sc_orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_order_payments_payments` (`payment_id`) REFERENCES `sc_payments` (`payment_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_invoice_payments` (
  `invoice_id`  INT UNSIGNED  NOT NULL,
  `payment_id`  INT UNSIGNED  NOT NULL,
  PRIMARY KEY (`invoice_id`, `payment_id`),
  FOREIGN KEY `fk_invoice_payments_invoices` (`invoice_id`) REFERENCES `sc_invoices` (`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_invoice_payments_payments` (`payment_id`) REFERENCES `sc_payments` (`payment_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Content Management System (CMS)
--
-- @see https://schema.org/CreativeWork Schema.org type `CreativeWork`
--
CREATE TABLE IF NOT EXISTS `sc_creative_work_types` (
  `creative_work_type_id`  TINYINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `creative_work_type`     VARCHAR(36)       NOT NULL,
  PRIMARY KEY (`creative_work_type_id`),
  UNIQUE KEY (`creative_work_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

-- @see https://schema.org/docs/full.html
INSERT IGNORE INTO `sc_creative_work_types` (`creative_work_type_id`, `creative_work_type`) VALUES
  (NULL, '3DModel'),

  (NULL, 'AboutPage'),
  (NULL, 'AdvertiserContentArticle'),
  (NULL, 'AmpStory'),
  (NULL, 'AnalysisNewsArticle'),
  (NULL, 'Answer'),
  (NULL, 'APIReference'),
  (NULL, 'ArchiveComponent'),
  (NULL, 'Article'),
  (NULL, 'AskPublicNewsArticle'),
  (NULL, 'Atlas'),
  (NULL, 'Audiobook'),
  (NULL, 'AudioObject'),
  (NULL, 'AudioObjectSnapshot'),

  (NULL, 'BackgroundNewsArticle'),
  (NULL, 'Barcode'),
  (NULL, 'Blog'),
  (NULL, 'BlogPosting'),
  (NULL, 'Book'),
  (NULL, 'BookSeries'),

  (NULL, 'CategoryCodeSet'),
  (NULL, 'Chapter'),
  (NULL, 'CheckoutPage'),
  (NULL, 'Claim'),
  (NULL, 'ClaimReview'),
  (NULL, 'Clip'),
  (NULL, 'Code'),
  (NULL, 'Collection'),
  (NULL, 'CollectionPage'),
  (NULL, 'ComicCoverArt'),
  (NULL, 'ComicIssue'),
  (NULL, 'ComicSeries'),
  (NULL, 'ComicStory'),
  (NULL, 'Comment'),
  (NULL, 'CompleteDataFeed'),
  (NULL, 'ContactPage'),
  (NULL, 'Conversation'),
  (NULL, 'CorrectionComment'),
  (NULL, 'Course'),
  (NULL, 'CoverArt'),
  (NULL, 'CreativeWorkSeason'),
  (NULL, 'CreativeWorkSeries'),
  (NULL, 'CriticReview'),

  (NULL, 'DataCatalog'),
  (NULL, 'DataDownload'),
  (NULL, 'DataFeed'),
  (NULL, 'Dataset'),
  (NULL, 'DefinedTermSet'),
  (NULL, 'DigitalDocument'),
  (NULL, 'DiscussionForumPosting'),
  (NULL, 'Drawing'),

  (NULL, 'EducationalOccupationalCredential'),
  (NULL, 'EmailMessage'),
  (NULL, 'EmployerReview'),
  (NULL, 'Episode'),
  (NULL, 'ExercisePlan'),

  (NULL, 'FAQPage'),

  (NULL, 'Game'),
  (NULL, 'Guide'),

  (NULL, 'HealthTopicContent'),
  (NULL, 'HowTo'),
  (NULL, 'HowToDirection'),
  (NULL, 'HowToSection'),
  (NULL, 'HowToStep'),
  (NULL, 'HowToTip'),
  (NULL, 'HyperToc'),
  (NULL, 'HyperTocEntry'),

  (NULL, 'ImageGallery'),
  (NULL, 'ImageObject'),
  (NULL, 'ImageObjectSnapshot'),
  (NULL, 'ItemPage'),

  (NULL, 'LearningResource'),
  (NULL, 'Legislation'),
  (NULL, 'LegislationObject'),
  (NULL, 'LiveBlogPosting'),

  (NULL, 'Manuscript'),
  (NULL, 'Map'),
  (NULL, 'MathSolver'),
  (NULL, 'MediaGallery'),
  (NULL, 'MediaObject'),
  (NULL, 'MediaReview'),
  (NULL, 'MediaReviewItem'),
  (NULL, 'MedicalScholarlyArticle'),
  (NULL, 'MedicalWebPage'),
  (NULL, 'Menu'),
  (NULL, 'MenuSection'),
  (NULL, 'Message'),
  (NULL, 'MobileApplication'),
  (NULL, 'Movie'),
  (NULL, 'MovieClip'),
  (NULL, 'MovieSeries'),
  (NULL, 'MusicAlbum'),
  (NULL, 'MusicComposition'),
  (NULL, 'MusicPlaylist'),
  (NULL, 'MusicRecording'),
  (NULL, 'MusicRelease'),
  (NULL, 'MusicVideoObject'),

  (NULL, 'NewsArticle'),
  (NULL, 'Newspaper'),
  (NULL, 'NoteDigitalDocument'),

  (NULL, 'OpinionNewsArticle'),

  (NULL, 'Painting'),
  (NULL, 'Periodical'),
  (NULL, 'Photograph'),
  (NULL, 'Play'),
  (NULL, 'PodcastEpisode'),
  (NULL, 'PodcastSeason'),
  (NULL, 'PodcastSeries'),
  (NULL, 'Poster'),
  (NULL, 'PresentationDigitalDocument'),
  (NULL, 'ProductCollection'),
  (NULL, 'ProfilePage'),
  (NULL, 'PublicationIssue'),
  (NULL, 'PublicationVolume'),

  (NULL, 'QAPage'),
  (NULL, 'Question'),
  (NULL, 'Quiz'),
  (NULL, 'Quotation'),

  (NULL, 'RadioClip'),
  (NULL, 'RadioEpisode'),
  (NULL, 'RadioSeason'),
  (NULL, 'RadioSeries'),
  (NULL, 'RealEstateListing'),
  (NULL, 'Recipe'),
  (NULL, 'Recommendation'),
  (NULL, 'Report'),
  (NULL, 'ReportageNewsArticle'),
  (NULL, 'Review'),
  (NULL, 'ReviewNewsArticle'),

  (NULL, 'SatiricalArticle'),
  (NULL, 'ScholarlyArticle'),
  (NULL, 'Sculpture'),
  (NULL, 'SearchResultsPage'),
  (NULL, 'Season'),
  (NULL, 'SheetMusic'),
  (NULL, 'ShortStory'),
  (NULL, 'SiteNavigationElement'),
  (NULL, 'SocialMediaPosting'),
  (NULL, 'SoftwareApplication'),
  (NULL, 'SoftwareSourceCode'),
  (NULL, 'SpecialAnnouncement'),
  (NULL, 'SpreadsheetDigitalDocument'),
  (NULL, 'Statement'),
  (NULL, 'Syllabus'),

  (NULL, 'Table'),
  (NULL, 'TechArticle'),
  (NULL, 'TextDigitalDocument'),
  (NULL, 'TextObject'),
  (NULL, 'Thesis'),
  (NULL, 'TVClip'),
  (NULL, 'TVEpisode'),
  (NULL, 'TVSeason'),
  (NULL, 'TVSeries'),

  (NULL, 'UserReview'),

  (NULL, 'VideoGallery'),
  (NULL, 'VideoGame'),
  (NULL, 'VideoGameClip'),
  (NULL, 'VideoGameSeries'),
  (NULL, 'VideoObject'),
  (NULL, 'VideoObjectSnapshot'),
  (NULL, 'VisualArtwork'),

  (NULL, 'WebApplication'),
  (NULL, 'WebContent'),
  (NULL, 'WebPage'),
  (NULL, 'WebPageElement'),
  (NULL, 'WebSite'),
  (NULL, 'WPAdBlock'),
  (NULL, 'WPFooter'),
  (NULL, 'WPHeader'),
  (NULL, 'WPSideBar');

CREATE TABLE IF NOT EXISTS `sc_creative_works` (
  `creative_work_id`       MEDIUMINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `creative_work_uuid`     BINARY(16)          NOT NULL,
  `creative_work_type_id`  TINYINT UNSIGNED    NOT NULL,
  `version`                SMALLINT UNSIGNED   NOT NULL  DEFAULT 1,
  `date_created`           DATETIME            NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `date_published`         DATETIME            NULL  DEFAULT NULL,
  `date_modified`          DATETIME            NULL  DEFAULT NULL,
  `date_expires`           DATETIME            NULL  DEFAULT NULL,
  `date_deleted`           DATETIME            NULL  DEFAULT NULL,
  `creative_work_json`     TEXT                NOT NULL,
  PRIMARY KEY (`creative_work_id`),
  UNIQUE KEY `uk_creative_work_uuid` (`creative_work_uuid`),
  FOREIGN KEY `fk_creative_work_types` (`creative_work_type_id`) REFERENCES `sc_creative_work_types` (`creative_work_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  INDEX `ix_creative_works` (`date_published` DESC, `date_created` DESC, `creative_work_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `sc_creative_work_texts` (
  `creative_work_id`            MEDIUMINT UNSIGNED  NOT NULL,
  `in_language`                 VARCHAR(13)         CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-GB',
  `name`                        VARCHAR(150)        DEFAULT NULL,
  `alternate_name`              VARCHAR(150)        DEFAULT NULL,
  `headline`                    VARCHAR(255)        DEFAULT NULL,
  `keywords`                    VARCHAR(767)        DEFAULT NULL,
  `description`                 VARCHAR(5000)       DEFAULT NULL,
  `disambiguating_description`  VARCHAR(255)        DEFAULT NULL,
  `text_plain`                  TEXT                DEFAULT NULL,
  `text_html`                   TEXT                DEFAULT NULL,
  `text_markdown`               TEXT                DEFAULT NULL,
  PRIMARY KEY (`creative_work_id`),
  FOREIGN KEY `fk_creative_work_texts` (`creative_work_id`) REFERENCES `sc_creative_works` (`creative_work_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY `fk_creative_work_languages` (`in_language`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FULLTEXT INDEX `ix_creative_work_texts` (`name`, `alternate_name`, `headline`, `keywords`, `description`, `disambiguating_description`, `text_plain`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;

--
-- Roles of persons and organizations in content creation and content management.
--
-- @see https://schema.org/character
--      `Person` as `character` in a `CreativeWork` is not included, because this
--      cannot be mapped to an actual `Person` role in an e-commerce `Organization`.
--
CREATE TABLE IF NOT EXISTS `sc_creative_work_roles` (
  `creative_work_role_id`  TINYINT UNSIGNED  NOT NULL  AUTO_INCREMENT,
  `creative_work_role`     VARCHAR(31)       NOT NULL,
  PRIMARY KEY (`creative_work_role_id`),
  UNIQUE KEY (`creative_work_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

INSERT IGNORE INTO `sc_creative_work_roles` (`creative_work_role_id`, `creative_work_role`) VALUES
   (NULL, 'accountablePerson'),
   (NULL, 'author'),
   (NULL, 'character'),
   (NULL, 'contributor'),
   (NULL, 'copyrightHolder'),
   (NULL, 'creator'),
   (NULL, 'editor'),
   (NULL, 'funder'),
   (NULL, 'maintainer'),
   (NULL, 'producer'),
   (NULL, 'provider'),
   (NULL, 'publisher'),
   (NULL, 'publisherImprint'),
   (NULL, 'sdPublisher'),
   (NULL, 'sourceOrganization'),
   (NULL, 'sponsor'),
   (NULL, 'translator');

CREATE TABLE IF NOT EXISTS `sc_creative_work_organizations` (
  `organization_id`        MEDIUMINT UNSIGNED  NOT NULL,
  `creative_work_role_id`  TINYINT UNSIGNED    NOT NULL,
  `creative_work_id`       MEDIUMINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`organization_id`, `creative_work_role_id`, `creative_work_id`),
  FOREIGN KEY (`organization_id`) REFERENCES `sc_organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`creative_work_role_id`) REFERENCES `sc_creative_work_roles` (`creative_work_role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`creative_work_id`) REFERENCES `sc_creative_works` (`creative_work_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;

CREATE TABLE IF NOT EXISTS `sc_creative_work_persons` (
  `person_id`              INT UNSIGNED        NOT NULL,
  `creative_work_role_id`  TINYINT UNSIGNED    NOT NULL,
  `creative_work_id`       MEDIUMINT UNSIGNED  NOT NULL,
  PRIMARY KEY (`person_id`, `creative_work_role_id`, `creative_work_id`),
  FOREIGN KEY (`person_id`) REFERENCES `sc_persons` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`creative_work_role_id`) REFERENCES `sc_creative_work_roles` (`creative_work_role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`creative_work_id`) REFERENCES `sc_creative_works` (`creative_work_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;


--
-- Ratings and reviews.
--
-- @see https://schema.org/AggregateRating Schema.org type `AggregateRating`
-- @see https://schema.org/ratingValue Schema.org property `ratingValue`
-- @see https://schema.org/ratingCount Schema.org property `ratingCount`
-- @see https://schema.org/reviewCount Schema.org property `reviewCount`
--
CREATE TABLE IF NOT EXISTS `sc_aggregate_ratings` (
  `id`                  INT UNSIGNED           NOT NULL  AUTO_INCREMENT,
  `item_reviewed_uuid`  BINARY(16)             NOT NULL,
  `rating_value`        DECIMAL(5,2) UNSIGNED  NOT NULL,
  `rating_count`        SMALLINT UNSIGNED      NULL  DEFAULT NULL,
  `review_count`        SMALLINT UNSIGNED      NULL  DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`item_reviewed_uuid`),
  INDEX `ix_aggregate_ratings` (`item_reviewed_uuid` ASC, `rating_value` DESC, `rating_count` DESC, `review_count` DESC)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii  COLLATE=ascii_bin;
