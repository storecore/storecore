<?php

declare(strict_types=1);

namespace StoreCore;

use Psr\Container\ContainerExceptionInterface;

/**
 * Generic exception in a container that implements the PSR-11 Container Interface.
 *
 * @see https://github.com/php-fig/container/blob/master/src/ContainerExceptionInterface.php
 * @see https://www.php.net/manual/en/class.throwable.php
 * @see https://www.php.net/manual/en/class.runtimeexception
 */
class ContainerException extends \RuntimeException
implements ContainerExceptionInterface, \Throwable
{
}
