<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\Container\ContainerInterface;

/**
 * Repository interface.
 * 
 * The StoreCore `RepositoryInterface` adds `set()` and `unset()` methods
 * to the `get()` and `has()` methods from the PSR-11 `ContainerInterface`.
 *
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-11/ PSR-11: Container interface
 * @version 1.0.0-alpha.1
 */
interface RepositoryInterface extends ContainerInterface
{
    public function set(IdentityInterface &$entity): string;
    public function unset(string $id): bool;
}
