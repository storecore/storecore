<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Session factory.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php.net/manual/en/ref.session.php
 * @version 1.0.0
 */
class SessionFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var StoreCore\Registry $Registry
     *   StoreCore global registry container.
     */
    private Registry $Registry;

    /**
     * @var int $sessionTimeout
     *   Session timeout in minutes.  Defaults to 15 minutes.
     */
    private int $sessionTimeout = 15;

    /**
     * Creates a session factory.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.  The registry is used to check if
     *   it already contains an active `Session` object.
     */
    public function __construct(Registry $registry)
    {
        $this->Registry = $registry;
    }

    /**
     * Creates a session.
     *
     * @param void
     * @return StoreCore\Session
     * @uses StoreCore\Session::__construct()
     * @uses StoreCore\SubjectObservers::populate()
     */
    public function createSession(): Session
    {
        if ($this->Registry->has('Session')) {
            return $this->Registry->get('Session');
        }

        $session = new Session($this->sessionTimeout * 60);
        SubjectObservers::populate($session);
        $this->Registry->set('Session', $session);
        return $session;
    }

    /**
     * Sets the session timeout in minutes.
     *
     * @param int $timeout_in_minutes
     *   Session expiration timeout in minutes as an integer, with a minimum
     *   of 1 minute.  This settings is ignored if the session was already
     *   started.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if `$timeout_in_minutes` is less than
     *   `1` minute.
     */
    public function setSessionTimeout(int $timeout_in_minutes): void
    {
        if ($timeout_in_minutes < 1) throw new \ValueError();
        $this->sessionTimeout = $timeout_in_minutes;
    }
}
