<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \SplObjectStorage;

use const \COUNT_NORMAL;
use const \COUNT_RECURSIVE;

/**
 * Collection of order items.
 *
 * @api
 * @package   StoreCore\OML
 * @see       https://schema.org/OrderItem Schema.org type `OrderItem`
 * @version   0.1.0
 */
class OrderItems implements \Countable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var \SplObjectStorage $storage
     *   The `OrderItem` objects in an `Order`, implemented as
     *   an SPL (Standard PHP Library) object storage.
     */
    private SplObjectStorage $storage;

    /**
     * Create a collection of order items.
     *
     * @param void
     */
    public function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    /**
     * Adds an object to the storage.
     *
     * @param StoreCore\OrderItem $object
     *   The object to add.
     *
     * @param mixed $info
     *   The data to associate with the object.
     *
     * @return void
     *
     * @see https://www.php.net/manual/en/splobjectstorage.attach.php
     *   `SplObjectStorage::attach()` method
     */
    public function attach(OrderItem $object, mixed $info = null): void
    {
        $this->storage->attach($object, $info);
    }

    /**
     * Counts the number of order items.
     *
     * @param int $mode
     *   If the OPTIONAL `mode` parameter is set to `COUNT_RECURSIVE` (or `1`),
     *   `OrderItems::count()` will recursively count the storage.
     *
     * @return int
     *   Returns the number of `OrderItem` objects in the storage.
     */
    public function count(int $mode = COUNT_NORMAL): int
    {
        if ($mode === COUNT_RECURSIVE) {
            $result = 0;
            foreach ($this->storage as $order_item) {
                $result += $order_item->count();
            }
            return $result;
        } else {
            return $this->storage->count($mode);
        }
    }
}
