<?php

declare(strict_types=1);

namespace StoreCore;

class LogicException extends \LogicException {}
