<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2020, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Database\Observers;

/**
 * Observer design pattern: observers/subject attacher.
 *
 * This helper class contains a single static method `populate()`
 * to attach all stored observers to a given subject.
 *
 * @api
 * @package StoreCore\Core
 * @version 2.0.0
 */
class SubjectObservers
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '2.0.0';

    /**
     * Attaches observers to a subject.
     *
     * @param \SplSubject &$subject
     *   The subject to which the observers are attached, if any exist.
     *   This subject MUST implement the `SplSubject` interface.
     *
     * @return void
     */
    public static function populate(\SplSubject &$subject): void
    {
        try {
            $model = new Observers(Registry::getInstance());
            $observers = $model->getSubjectObservers(\get_class($subject));
        } catch (\Exception $e) {
            return;
        }

        if ($observers === null) return;

        foreach ($observers as $observer_class) {
            if (class_exists($observer_class)) {
                $observer = new $observer_class();
                $subject->attach($observer);
            }
        }
    }
}
