<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use \DateTimeImmutable;
use \DateTimeZone;
use Psr\Clock\ClockInterface;

use function \empty;

/**
 * Clock.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-20/ PSR-20 Clock
 * @see     https://github.com/php-fig/clock PSR-20 Clock repository
 * @see     https://github.com/php-fig/clock/blob/master/src/ClockInterface.php ClockInterface
 * @version 1.0.0
 */
final readonly class Clock implements ClockInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0';

    /**
     * Returns the request time as a DateTimeImmutable object.
     *
     * @param void
     * @return \DateTimeImmutable
     */
    public function now(): DateTimeImmutable
    {
        if (!empty($_SERVER['REQUEST_TIME_FLOAT'])) {
            return DateTimeImmutable::createFromFormat('U.u', (string) $_SERVER['REQUEST_TIME_FLOAT']);
        } else {
            return new DateTimeImmutable('now', new DateTimeZone('UTC'));
        }
    }
}
