<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2019, 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP layout component <amp-layout>.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 *
 * @see https://amp.dev/documentation/components/amp-layout/
 *      Documentation: <amp-layout> - amp.dev
 * 
 * @see https://amp.dev/documentation/guides-and-tutorials/learn/amp-html-layout/
 *      AMPHTML Layout System
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/learn/amp-html-layout/layouts_demonstrated/
 *      Demonstrating AMP layouts
 */
class Layout extends AbstractComponent implements LayoutInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    protected const array SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_CONTAINER,
        LayoutInterface::LAYOUT_FILL,
        LayoutInterface::LAYOUT_FIXED,
        LayoutInterface::LAYOUT_FIXED_HEIGHT,
        LayoutInterface::LAYOUT_FLEX_ITEM,
        LayoutInterface::LAYOUT_INTRINSIC,
        LayoutInterface::LAYOUT_NODISPLAY,
        LayoutInterface::LAYOUT_RESPONSIVE,
    ];

    /**
     * @var array $attributes
     *   HTML and AMP attributes of the `<amp-layout>` component.  Defaults to
     *   `<amp-layout height="1" layout="responsive" width="1">`.
     */
    protected array $attributes = [
        'height' => 1,
        'layout' => LayoutInterface::LAYOUT_RESPONSIVE,
        'width'  => 1,
    ];

    /**
     * Converts the <amp-layout> component to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the AMP HTML component `<amp-layout>…</amp-layout>` as a
     *   string with attributes and contents.
     */
    public function __toString(): string
    {
        $result = '<amp-layout';
        foreach ($this->attributes as $attribute => $value) {
            $result .= ' ' . $attribute . '="' . $value . '"';
        }
        $result .= '>' . $this->innerHTML . '</amp-layout>';
        return $result;
    }
}
