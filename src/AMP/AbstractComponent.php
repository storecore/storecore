<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * Abstract AMP component.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://amp.dev/documentation/components/ AMP Component Catalogue
 * @see     https://amp.dev/documentation/examples/ AMP Websites Examples
 * @version 1.0.0-alpha.1
 */
abstract class AbstractComponent implements BlockInterface, LayoutInterface
{
    /**
     * @var array SUPPORTED_LAYOUTS
     *   The `layout` attribute values that are supported by an AMP HTML
     *   component.  By default, this attribute is set to `layout="responsive"`
     *   for most AMP components.
     */
    protected const array SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_CONTAINER,
        LayoutInterface::LAYOUT_FILL,
        LayoutInterface::LAYOUT_FIXED,
        LayoutInterface::LAYOUT_FIXED_HEIGHT,
        LayoutInterface::LAYOUT_FLEX_ITEM,
        LayoutInterface::LAYOUT_INTRINSIC,
        LayoutInterface::LAYOUT_NODISPLAY,
        LayoutInterface::LAYOUT_RESPONSIVE,
    ];

    /**
     * @param array $attributes
     *   HTML5 and AMP HTML attributes of the AMP component.
     *   Defaults to `layout="responsive"` for responsive AMP components.
     */
    protected array $attributes = [
        'layout' => LayoutInterface::LAYOUT_RESPONSIVE,
    ];

    /**
     * @param array|null $innerHTML
     *   Container for child nodes inserted into an AMP HTML object.
     */
    protected ?array $innerHTML = null;

    /**
     * Gets an attribute.
     *
     * @param string $name
     *   Name of the attribute to get.
     *
     * @return mixed
     *   Returns the attribute or `null` of the attribute does not exist.
     */
    public function __get(string $name): mixed
    {
        $name = strtolower($name);
        return match ($name) {
            'innerhtml' => $this->getInnerHTML(),
            'innertext' => $this->getInnerText(),
            'layout' => $this->getLayout(),
            default => array_key_exists($name, $this->attributes) ? $this->attributes[$name] : null,
        };
    }

    /**
     * Sets an attribute.
     *
     * @param string $name
     *   Name of the attribute to set.
     *
     * @param mixed $value
     *   Value of the attribute to set.
     *
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        $name = strtolower($name);
        match ($name) {
            'innerhtml' => $this->setHTML($value),
            'layout' => $this->setLayout($value),
            default => $this->attributes[$name] = $value,
        };
    }

    /**
     * Converts a concrete AMP component to AMP HTML.
     *
     * @param void
     * @return string
     */
    abstract public function __toString(): string;

    /**
     * Returns the value of a specified attribute of the AMP component.
     *
     * @param string $name
     *   The name of the attribute whose value you want to get.
     *
     * @return null|string
     *   A string containing the value of `$name`.  All modern web browsers
     *   return `null` when the specified attribute does not exist on
     *   the specified element, so this method mimics this behavior.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute
     */
    public function getAttribute(string $name): ?string
    {
        $name = strtolower($name);
        return $this->hasAttribute($name) ? (string) $this->attributes[$name] : null;
    }

    /**
     * Gets the object’s innerHTML.
     *
     * @param void
     * @return string
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML
     */
    protected function getInnerHTML(): string
    {
        return empty($this->innerHTML) ? '' : implode('', $this->innerHTML);
    }

    /**
     * Gets the object’s innerText.
     *
     * @param void
     * @return string
     * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/innerText
     */
    protected function getInnerText(): string
    {
        return strip_tags($this->getInnerHTML());
    }

    /**
     * Gets the AMP layout attribute.
     *
     * @param void
     *
     * @return null|string
     *   Returns the currently set AMP HTML `layout` attribute as a string.
     */
    final public function getLayout(): ?string
    {
        return $this->attributes['layout'];
    }

    /**
     * Determines whether the AMP component has the specified attribute or not.
     *
     * @param string $name
     * @return bool
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/hasAttribute
     */
    public function hasAttribute(string $name): bool
    {
        $name = strtolower($name);
        return array_key_exists($name, $this->attributes);
    }

    /**
     * Adds an HTML child node.
     *
     * @param \StoreCore\AMP\BlockInterface|string $node
     *   HTML node to add to the children of the AMP component.  MUST be a
     *   string with HTML or an object that can be converted to an AMP HTML
     *   string.
     *
     * @return void
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/setHTML
     *      Element setHTML() method, MDN Web APIs References
     */
    public function setHTML(BlockInterface|string $node): void
    {
        $this->innerHTML[] = $node;
    }

    /**
     * Sets the AMP layout attribute.
     *
     * @param string $layout
     *   String value for the AMP HTML `layout` attribute.  This MUST be one
     *   of the values in the `$SUPPORTED_LAYOUTS` array; this array SHOULD be
     *   overwritten in child classes for objects with a more limited number
     *   of allowed `layout` values.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the `$layout` parameter is an
     *   unsupported layout.
     */
    public function setLayout(string $layout): void
    {
        $layout = strtolower($layout);
        if (!in_array($layout, self::SUPPORTED_LAYOUTS)) {
            throw new \ValueError();
        }

        $this->attributes['layout'] = $layout;
    }
}
