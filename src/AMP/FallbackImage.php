<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use function \str_replace;

/**
 * AMP fallback image.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/placeholders/
 *      Placeholders & fallbacks - Style & Layout - amp.dev
 */
final class FallbackImage extends Image implements BlockInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * Gets the AMP fallback image tag.
     *
     * @param void
     *
     * @return string
     *   Returns the string `<amp-img fallback …>…</amp-img>` for a fallback
     *   image.  A common use case is adding a JPEG fallback image to a WebP
     *   image for browsers that do not support the WebP image file format.
     */
    public function __toString(): string
    {
        return str_replace('<amp-img ', '<amp-img fallback ', parent::__toString());
    }
}
