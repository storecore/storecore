<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP placeholder image.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/placeholders/
 *      Placeholders & fallbacks - amp.dev
 */
final class PlaceholderImage extends Image implements BlockInterface, LayoutInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array SUPPORTED_LAYOUTS
     *   An AMP placeholder image always fills the parent,
     *   so the only supported layout is `fill`.
     */
    protected const SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_FILL,
    ];

    /**
     * Gets the AMP placeholder image tag.
     *
     * @param void
     *
     * @return string
     *   Returns the string `<amp-img placeholder src="…" layout="fill"></amp-img>`
     *   for a placeholder image.  An AMP placeholder image has no `alt` text
     *   and no `width` and `height` dimensions.
     */
    public function __toString(): string
    {
        return
            '<amp-img placeholder src="' . $this->getSource()
            . '" layout="' . LayoutInterface::LAYOUT_FILL . '"></amp-img>';
    }

    public function getLayout(): string
    {
        return LayoutInterface::LAYOUT_FILL;
    }
}
