<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * StoreCore AMP block interface.
 *
 * AMP components that MAY be converted to AMP HTML tags or other kinds of
 * AMP HTML blocks SHOULD implement this `BlockInterface`.  The interface
 * implements the magic PHP method `__toString()` to convert component objects
 * to strings containing AMP HTML.
 *
 * @api
 * @package StoreCore\CMS
 * @version 1.0.0
 */
interface BlockInterface extends \Stringable
{
    /**
     * Creates a block of AMP HTML.
     *
     * @param void
     *
     * @return string
     *
     * @throws StoreCore\AMP\BlockRenderException
     *   Exception thrown if a AMP component cannot be fully rendered into a
     *   valid AMP HTML block.  This exception SHOULD be used if a required
     *   attribute is missing.
     */
    public function __toString(): string;
}
