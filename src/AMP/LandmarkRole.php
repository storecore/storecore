<?php

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * ARIA landmark roles.
 *
 * @api
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\CMS
 * @see       https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques#landmark_roles
 * @version   1.0.0
 */
enum LandmarkRole: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    case banner = 'role="banner"';
    case complementary = 'role="complementary"';
    case contentinfo = 'role="contentinfo"';
    case form = 'role="form"';
    case main = 'role="main"';
    case navigation = 'role="navigation"';
    case region = 'role="region"';
    case search = 'role="search"';
}
