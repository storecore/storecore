StoreCore AMP HTML Framework
============================

StoreCore AMP PHP Interfaces
----------------------------

```php
interface LayoutInterface {
    getLayout ( void ) : string|null
    setLayout ( string $layout ) : void
}

interface LightboxGalleryInterface {
    isLightbox ( void ) : bool
    setLightbox ( [ bool $lightbox = true ] ) : void
}
```

AMP components that MAY be converted to an HTML tag SHOULD implement the
`BlockInterface` from the core `StoreCore\AMP` namespace.  This
interface converts data objects to AMP HTML strings using the magic PHP method
`__toString()` and extends the native PHP `Stringable` interface:

```php
interface BlockInterface extends \Stringable {
    __toString ( void ) : string
}
```


StoreCore AMP Abstract Class
----------------------------

```php
abstract AbstractComponent implements BlockInterface, LayoutInterface {
    __get ( string $name ) : string|int|null
    __set ( string $name , mixed $value  ) : void
    getAttribute ( string $name ) : null|string
    getLayout ( void ) : string
    hasAttribute ( string $name ) : bool
    setHTML ( BlockInterface|string $node ) : void
    setLayout ( string $layout ) : void
}
```

Copyright © 2019, 2021, 2023 StoreCore™.  All rights reserved.
