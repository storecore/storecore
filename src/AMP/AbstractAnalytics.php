<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use \JsonException;
use \JsonSerializable;

use const \JSON_THROW_ON_ERROR;

use function \empty;
use function \json_encode;

/**
 * Abstract AMP Analytics <amp-analytics> component.
 *
 * @package StoreCore\BI
 * @version 0.2.0
 *
 * @see https://amp.dev/documentation/components/amp-analytics/
 *      Documentation: <amp-analytics> - amp.dev
 *
 * @see https://github.com/ampproject/amphtml/tree/master/extensions/amp-analytics
 *      Master of amphtml/extensions/amp-analytics in ampproject/amphtml 
 * 
 * @see https://amp.dev/documentation/guides-and-tutorials/optimize-and-measure/configure-analytics/analytics-vendors/
 *      List of AMP Analytics Vendors, AMP Guides & Tutorials
 */
abstract class AbstractAnalytics implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|string TYPE
     *   OPTIONAL `type` attribute of the `<amp-analytics>` AMP HTML component.
     *   The `type` attribute value is determined by the analytics vendor and
     *   MUST be set in child classes.
     */
    public const TYPE = null;

    /**
     * @var array $data
     *   Generic multidimensional array for JSON configuration settings.
     */
    protected array $data;

    /**
     * @var bool $dataCredentials
     *   This add `data-credentials="include"` to turn on the ability to read
     *   and write cookies on the request specified via the `config` attribute.
     *   This is an OPTIONAL attribute.
     */
    public bool $dataCredentials = false;

    /**
     * Converts the AMP object to an AMP HTML string.
     *
     * @param void
     *
     * @return string
     *   Returns the `<amp-analytics>` HTML tag for AMP Analytics with
     *   JSON `<script>` and JSON configuration settings data.
     * 
     * @throws StoreCore\AMP\BlockRenderException
     */
    public function __toString(): string
    {
        $result = '<amp-analytics';
        if (!empty(static::TYPE)) {
            $result .= ' type="' . static::TYPE . '"';
        }
        if ($this->dataCredentials) {
            $result .= ' data-credentials="include"';
        }
        $result .= '>';

        try {
            $result .= '<script type="application/json">' . $this->jsonSerialize() . '</script>';
        } catch (JsonException $e) {
            throw new BlockRenderException($e->getMessage(), $e->getCode(), $e);
        }

        $result .= '</amp-analytics>';
        return $result;
    }

    /**
     * Converts data to JSON.
     *
     * @param void
     *
     * @return string
     *   Returns the object variables as JSON (JavaScript Object Notation).
     * 
     * @throws \JsonException
     */
    public function jsonSerialize(): string
    {
        return json_encode($this->data, JSON_THROW_ON_ERROR);
    }
}
