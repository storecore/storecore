<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use LogicException;

use function \abs;
use function \count;
use function \explode;
use function \implode;
use function \is_int;
use function \is_string;
use function \rtrim;
use function \str_replace;
use function \str_starts_with;
use function \trim;

/**
 * AMP effects collection.
 *
 * The `amp-fx-collection` component provides a collection of preset visual
 * effects, such as parallax that can be easily enabled on any element via
 * attributes.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://amp.dev/documentation/components/websites/amp-fx-collection
 * @see     https://amp.dev/documentation/examples/components/amp-fx-collection/
 * @version 0.1.0
 */
final class EffectsCollection implements BlockInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';


    /**
     * @var string $dataDuration
     *   The duration over which the animation takes places.  The default
     *   value is '1000ms'.
     */
    protected string $dataDuration = '1000ms';

    /**
     * @var string $dataEasing
     *   The OPTIONAL `data-easing` parameter lets you vary the `fade-in`
     *   animation’s speed over the course of its duration.  The default is
     *   `ease-in` which is `cubic-bezier(0.40, 0.00, 0.40, 1.00)`.
     */
    public string $dataEasing = 'ease-in';

    /**
     * @var string $dataMarginEnd
     *   The OPTIONAL `data-margin-end` parameter determines when to stop
     *   a `fade-in-scroll` animation.  The value specified in percent dictates
     *   that the animation should have finished when the specified amount of
     *   the element being targeted is visible.  The default value is `50%`.
     */
    protected string $dataMarginEnd = '50%';

    /**
     * @var string $dataMarginStart
     *   The OPTIONAL `data-margin-start` parameter determines when to trigger
     *   the timed animation.  The value specified in percent dictates that the
     *   animation should be triggered when the element has crossed that
     *   percentage of the viewport.  The default value is `0%`.
     */
    protected string $dataMarginStart = '0%';

    /**
     * @var int|float $dataParallaxFactor
     *   The `data-parallax-factor` attribute for the `parallax` effect.
     *   Specifies a decimal value that controls how much faster or slower the
     *   element scrolls relative to the scrolling speed.  A value greater than
     *   `1` scrolls the element upward (element scrolls faster) when the user
     *   scrolls down the page; a value less than `1` scrolls the element
     *   downward (element scrolls slower) when the user scrolls downward.
     *   A value of `1` behaves normally.  A value of `0` effectively makes the
     *   element scroll fixed with the page.
     */
    protected int|float $dataParallaxFactor = 1;

    /**
     * @var bool $dataRepeat
     *   By default once the element is fully visible, AMP does not animate
     *   the opacity anymore.  If you want the opacity to change with a
     *   `fade-in-scroll` effect, even when the element has fully loaded,
     *   set the `data-repeat` attribute on the animation.
     */
    public bool $dataRepeat = false;

    /**
     * @var bool $fadeIn
     *   The `fade-in` effect allows an element to fade in once the element
     *   being targeted is visible in the viewport.
     */
    public bool $fadeIn = false;

    /**
     * @var bool $fadeInScroll
     *   The `fade-in-scroll` effect allows you to change the opacity of
     *   an element as it scrolls within the viewport.  This creates a scroll
     *   dependent fade animation.  By default once the element is fully
     *   visible we don’t animate the opacity anymore.
     */
    public bool $fadeInScroll = false;

    /**
     * @var bool $flyInBottom
     * @var bool $flyInLeft
     * @var bool $flyInRight
     * @var bool $flyInTop
     *   The `fly-in-bottom`, `fly-in-left`, `fly-in-right`, and `fly-in-top`
     *   effects allow an element’s position to be translated by a specified
     *   amount once it is in the viewport.
     */
    public bool $flyInBottom = false;
    public bool $flyInLeft = false;
    public bool $flyInRight = false;
    public bool $flyInTop = false;

    /**
     * @var StoreCore\AMP\BlockInterface|string $outerHTML
     *   The HTML or AMP HTML element to which the effects are applied.
     */
    public BlockInterface|string $outerHTML = '<div></div>';

    /**
     * @var bool $parallax
     *   The `parallax` effect allows an element to move as if it is nearer or
     *   farther relative to the foreground of the page content.  As the user
     *   scrolls the page, the element scrolls faster or slower depending on
     *   the value assigned to the `data-parallax-factor` attribute.
     */
    public bool $parallax = false;

    public function __get($name): mixed
    {
        return match ($name) {
            'dataDuration' => $this->dataDuration(),
            'dataMarginEnd' => $this->dataMarginEnd(),
            'dataMarginStart' => $this->dataMarginStart(),
            'dataParallaxFactor' => $this->dataParallaxFactor(),
            default => throw new LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'dataDuration' => $this->dataDuration($value),
            'dataMarginEnd' => $this->dataMarginEnd($value),
            'dataMarginStart' => $this->dataMarginStart($value),
            'dataParallaxFactor' => $this->dataParallaxFactor($value),
            default => throw new LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    public function __toString(): string
    {
        /*
         * AMP advises you don't combine:
         *
         * 1. `parallax`, `fly-in-top`, `fly-in-bottom`,
         * 2. `fly-in-left`, `fly-in-right`,
         * 3. `fade-in`, `fade-in-scroll`.
         */
        if (
            $this->parallax
            && ($this->flyInBottom || $this->flyInTop)
        ) {
            throw new BlockRenderException('The `parallax` visual effect should not be combined with `fly-in-top` or `fly-in-bottom`.');
        }

        if ($this->flyInLeft && $this->flyInRight) {
            throw new BlockRenderException('The `fly-in-left` and `fly-in-right` visual effects should not be combined.');
        }

        if ($this->fadeIn && $this->fadeInScroll) {
            throw new BlockRenderException('The `fade-in` and `fade-in-scroll` visual effects should not be combined.');
        }

        $result = (string) $this->outerHTML;

        // Add an (extra) block-level `div` element.
        if (strip_tags($result) === $result) {
            $result = '<div>' . $result . '</div>';
        }

        $fx = '';
        if ($this->parallax) {
            $fx = 'amp-fx="parallax" data-parallax-factor="' . $this->dataParallaxFactor . '"';
        }

        if ($this->fadeIn) {
            $fx = 'amp-fx="fade-in"';
            if ($this->dataDuration !== '1000ms') {
                $fx .= ' data-duration="' . $this->dataDuration . '"';
            }
            if ($this->dataEasing !== 'ease-in') {
                $fx .= ' data-easing="' . $this->dataEasing . '"';
            }
            if ($this->dataMarginStart !== '0%') {
                $fx .= ' data-margin-start="' . $this->dataMarginStart . '"';
            }
        }

        if ($this->fadeInScroll) {
            if ((int) rtrim($this->dataMarginEnd, '%') <= (int) rtrim($this->dataMarginStart, '%')) {
                throw new BlockRenderException('data-margin-end must be greater than data-margin-start', time());
            }

            $fx = 'amp-fx="fade-in-scroll"';
            if ($this->dataMarginStart !== '0%') {
                $fx .= ' data-margin-start="' . $this->dataMarginStart . '"';
            }
            if ($this->dataMarginEnd !== '50%') {
                $fx .= ' data-margin-end="' . $this->dataMarginEnd . '"';
            }
            if ($this->dataRepeat) {
                $fx .= ' data-repeat';
            }
        }

        $result = explode('>', $result, 2);
        $result[0] = trim($result[0]) . ' ' . $fx;
        $result = implode('>', $result);
        return $result;
    }

    /**
     * Sets and gets the `data-duration` attribute.
     *
     * @param null|int|string $duration
     *   Duration in milliseconds as an integer or a string like `2000ms`.
     *
     * @return string
     */
    public function dataDuration(null|int|string $duration = null): string
    {
        if ($duration !== null) {
            if (is_int($duration)) {
                $duration = abs($duration) . 'ms';
            }
            $this->dataDuration = $duration;
        }

        return $this->dataDuration;
    }


    /**
     * Sets and gets the `data-margin-end` attribute.
     *
     * @param null|int|string $percent
     *   Percentage as an integer or a string like '1%'.
     *
     * @return string
     */
    public function dataMarginEnd(null|int|string $percent = null): string
    {
        if ($percent !== null) {
            if (is_string($percent)) {
                $percent = str_replace('%', '', $percent);
                $percent = trim($percent);
                $percent = (int) $percent;
            }

            if ($percent < 0) {
                $percent = 0;
            } elseif ($percent > 100) {
                $percent = 100;
            }
            $this->dataMarginEnd = (string) $percent . '%';
        }

        return $this->dataMarginEnd;
    }

    /**
     * Sets and gets the `data-margin-start` attribute.
     *
     * @param null|int|string $percent
     *   Percentage as an integer or a string like '1%'.
     *
     * @return string
     */
    public function dataMarginStart(null|int|string $percent = null): string
    {
        if ($percent !== null) {
            if (is_string($percent)) {
                $percent = str_replace('%', '', $percent);
                $percent = trim($percent);
                $percent = (int) $percent;
            }

            if ($percent < 0) {
                $percent = 0;
            } elseif ($percent > 100) {
                $percent = 100;
            }
            $this->dataMarginStart = (string) $percent . '%';
        }

        return $this->dataMarginStart;
    }

    /**
     * Sets and gets the `data-parallax-factor` attribute.
     * 
     * @param int|float $factor
     * @return int|float
     */
    public function dataParallaxFactor(null|int|float $factor = null): int|float
    {
        if ($factor !== null) {
            if ($factor < 0) $factor = 0;
            $this->dataParallaxFactor = $factor;
        }

        return $this->dataParallaxFactor;
    }

    /**
     * Enables the `parallax` effect and optionally sets its factor.
     * 
     * @param int|float $factor
     * @return void
     */
    public function setParallax(int|float $factor = 1): void
    {
        $this->parallax = true;
        $this->dataParallaxFactor($factor);

        // `parallax` SHOULD NOT be combined with `fly-in-top` or `fly-in-bottom`
        $this->flyInTop = false;
        $this->flyInBottom = false;
    }
}
