<?php

declare(strict_types=1);

namespace StoreCore\AMP;

class BlockRenderException extends \LogicException {}
