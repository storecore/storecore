<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use StoreCore\CMS\HTMLImageElement;

use function \in_array;
use function \strtolower;

/**
 * AMP image <amp-img> component.
 *
 * @api
 * @package StoreCore\CMS
 * @version 0.2.3
 * 
 * @see https://amp.dev/documentation/components/amp-img/
 *      Documentation: <amp-img> - amp.dev
 * 
 * @see https://amp.dev/documentation/examples/components/amp-img/
 *      Example: <amp-img> - amp.dev
 * 
 * @see https://amp.dev/documentation/guides-and-tutorials/start/create/include_image/
 *      Include an image - Create your first AMP Page - amp.dev
 * 
 * @see https://amp.dev/documentation/examples/style-layout/how_to_support_images_with_unknown_dimensions/
 *      How to support Images with unknown Dimensions - amp.dev
 * 
 * @see https://stackoverflow.com/questions/50162825/how-to-lazy-load-amp-img
 *      How to lazy load amp-img? - Stack Overflow
 */
class Image extends HTMLImageElement implements BlockInterface, LayoutInterface, LightboxGalleryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.3';

    /**
     * @var array SUPPORTED_LAYOUTS
     *   Layouts supported by the `<amp-img>` element.
     */
    protected const SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_FILL,
        LayoutInterface::LAYOUT_FIXED,
        LayoutInterface::LAYOUT_FIXED_HEIGHT,
        LayoutInterface::LAYOUT_FLEX_ITEM,
        LayoutInterface::LAYOUT_INTRINSIC,
        LayoutInterface::LAYOUT_NODISPLAY,
        LayoutInterface::LAYOUT_RESPONSIVE,
    ];

    /**
     * @var StoreCore\AMP\FallbackImage|null $fallback
     *   OPTIONAL AMP fallback image.
     */
    private ?FallbackImage $fallback = null;

    /**
     * @var string $layout
     *   The `layout` attribute of the `<amp-img>` element.
     *   Defaults to `responsive`.
     */
    protected string $layout = LayoutInterface::LAYOUT_RESPONSIVE;

    /**
     * @var bool $lightbox
     *   The image is part of a lightbox gallery (`true`) or not (default `false`).
     */
    protected bool $lightbox = false;

    public function __get($name): mixed
    {
        return match ($name) {
            'fallback' => $this->fallback(),
            'layout' => $this->getLayout(),
            default => parent::__get($name),
        };
    }

    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'fallback' => $this->fallback($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Gets the <amp-img> AMP image element.
     *
     * @param void
     *
     * @return string
     *   Returns the AMP tag `<amp-img …>…</amp-img>` as a string.
     */
    public function __toString(): string
    {
        $result = parent::__toString();
        $result = explode(' ', $result, 2);
        $result[0] = '<amp-img';

        if ($this->isLightbox()) {
            $result[0] .= ' lightbox';
        }
        $result[0] .= ' layout="'. $this->getLayout() . '"';

        $result = implode(' ', $result);
        if ($this->fallback() !== null) {
            $result .= (string) $this->fallback();
        }
        $result .= '</amp-img>';

        return $result;
    }

    /**
     * Sets and gets a fallback image.
     *
     * @param null|StoreCore\AMP\FallbackImage $image
     *   AMP fallback image.
     *
     * @return null|StoreCore\AMP\FallbackImage
     */
    public function fallback(?FallbackImage $image = null): ?FallbackImage
    {
        if ($image !== null) {
            $this->fallback = $image;
        }

        return $this->fallback;
    }

    /**
     * Gets the layout attribute.
     *
     * @param void
     *
     * @return string
     *   Returns the string value of the AMP `layout` attribute.  Defaults to
     *   `responsive`: photos and other images are set to `layout="responsive"`
     *   in AMP by default for responsive web design (RWD).
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * @inheritDoc
     */
    public function isLightbox(): bool
    {
        return $this->lightbox;
    }

    /**
     * Sets the layout attribute.
     *
     * @param string $layout
     *   String value for the AMP `layout` attribute.  Must be one of the values
     *   in the `SUPPORTED_LAYOUTS` array.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws an value error exception if the `$layout` parameter is not
     *   a string or an unsupported layout.
     */
    public function setLayout(string $layout): void
    {
        $layout = strtolower($layout);
        if (!in_array($layout, self::SUPPORTED_LAYOUTS)) {
            throw new \ValueError();
        }

        $this->layout = $layout;
    }

    /**
     * @inheritDoc
     */
    public function setLightbox(bool $lightbox = true): void
    {
        $this->lightbox = $lightbox;
    }
}
