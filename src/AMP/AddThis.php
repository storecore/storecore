<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2019, 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP AddThis component <amp-addthis>.
 *
 * @package StoreCore\CMS
 * @see     https://amp.dev/documentation/components/amp-addthis
 * @see     https://www.addthis.com/academy/how-to-install-addthis-inline-share-buttons-on-amp-accelerated-mobile-pages/
 * @see     https://blog.amp.dev/2018/07/30/addthis-is-now-available-for-amp/
 * @version 1.0.0-alpha.1
 */
final class AddThis extends AbstractComponent implements BlockInterface, LayoutInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    protected const array SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_FILL,
        LayoutInterface::LAYOUT_FIXED,
        LayoutInterface::LAYOUT_FIXED_HEIGHT,
        LayoutInterface::LAYOUT_FLEX_ITEM,
        LayoutInterface::LAYOUT_NODISPLAY,
        LayoutInterface::LAYOUT_RESPONSIVE,
    ];

    /**
     * @param array $attributes
     *   HTML5 and AMP HTML attributes of the AddThis component.
     */
    protected array $attributes = [
        'height' => 48,
        'layout' => LayoutInterface::LAYOUT_RESPONSIVE,
        'width'  => 48,
    ];

    /**
     * Constructs an AMP component for AddThis.
     *
     * @param string|null $publisher_id
     *   Optional AddThis publisher ID.
     *
     * @param string|null $widget_id
     *   Optional AddThis widget ID.
     *
     * @return self
     */
    public function __construct(?string $publisher_id = null, ?string $widget_id = null)
    {
        if ($publisher_id !== null) {
            $this->setPublisherID($publisher_id);
        }

        if ($widget_id !== null) {
            $this->setWidgetID($widget_id);
        }
    }

    /**
     * Converts the AMP AddThis component to AMP HTML.
     *
     * @param void
     *
     * @return string
     *   Returns the `<amp-addthis …></amp-addthis>` component with its
     *   attributes as an HTML string.
     */
    public function __toString(): string
    {
        $result = '<amp-addthis';
        foreach ($this->attributes as $attribute => $value) {
            $result .= ' ' . $attribute . '="' . $value . '"';
        }
        $result .= '></amp-addthis>';
        return $result;
    }

    /**
     * Sets the AddThis publisher ID.
     *
     * @param string $publisher_id
     *   AddThis publisher identifier for the `data-pub-id` attribute.
     *
     * @return void
     */
    public function setPublisherID(string $publisher_id): void
    {
        $this->attributes['data-pub-id'] = $publisher_id;
    }

    /**
     * Sets the AddThis widget ID.
     *
     * @param string $widget_id
     *   AddThis widget identifier for the `data-widget-id` attribute.
     *
     * @return void
     */
    public function setWidgetID(string $widget_id): void
    {
        $this->attributes['data-widget-id'] = $widget_id;
    }
}
