<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

interface_exists(BlockInterface::class);
interface_exists(LayoutInterface::class);
interface_exists(LightboxGalleryInterface::class);
enum_exists(LandmarkRole::class);

/**
 * AMP carousel <amp-carousel>.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://amp.dev/documentation/components/amp-carousel
 * @see     https://amp.dev/documentation/examples/components/amp-carousel/
 * @see     https://amp.dev/documentation/examples/multimedia-animations/image_galleries_with_amp-carousel/
 * @version 1.0.0-alpha.1
 */
final class Carousel implements BlockInterface, LayoutInterface, LightboxGalleryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string TYPE_CAROUSEL
     *   AMP carousel `type="carousel"` attribute (default).
     */
    public const TYPE_CAROUSEL = 'carousel';

    /**
     * @var string TYPE_SLIDES
     *   AMP `type="slides"` attribute turns a carousel into a slider.
     */
    public const TYPE_SLIDES = 'slides';

    /**
     * @var string|null $ariaLabel
     *  OPTIONAL value of the ARIA `aria-label` HTML attribute, which defines
     *  a string value that labels the current element.
     */
    public ?string $ariaLabel = null;

    /**
     * @var bool $autoplay
     *   OPTIONAL `autoplay` attribute for `type="slides"` sliders only.
     *   If set to `true`, this advances the slide to the next slide without
     *   user interaction.
     */
    public bool $autoplay = false;

    /**
     * @var array $children
     *   Images, slides, and other child nodes of the carousel or slider.
     */
    private array $children = [];

    /**
     * @var int $delay
     *   Delay of autoplaying sliders in milliseconds.  Defaults to 5000.
     *   This delay is only used if the `type="slides"` and `autoplaying`
     *   are both set.
     */
    private int $delay = 5000;

    /**
     * @var int|null $height
     *   Required AMP carousel `height` attribute, specifies the carousel
     *   height in pixels.
     */
    private ?int $height = null;

    /**
     * @var string|null $Layout
     *   OPTIONAL `layout` attribute of the `<amp-carousel>` element.
     */
    protected ?string $layout = null;

    /**
     * @var bool $Lightbox
     *   The carousel is a lightbox gallery (`true`) or not (default `false`).
     */
    protected bool $lightbox = false;

    /**
     * @var StoreCore\AMP\LandmarkRole
     *   ARIA role, defaults to the `role="region"` landmark role.
     */
    public LandmarkRole $role = LandmarkRole::region;

    /**
     * @var string $type
     *   AMP carousel `type` HTML attribute, defaults to `carousel`.
     */
    private string $type = self::TYPE_CAROUSEL;

    /**
     * @var int|null $width
     *   OPTIONAL carousel `width` attribute for a fixed width in pixels.
     *   If the width is not specified, the carousel fills the full width
     *   of the parent container.
     */
    private ?int $width = null;


    /**
     * Creates a carousel or slider.
     *
     * @param string $amp_carousel_type
     *   OPTIONAL parameter to create a slider instead of a carousel (default).
     *   If set to 'slides' a slider is created, otherwise a 'carousel'.
     *
     * @return self
     */
    public function __construct(string $amp_carousel_type = self::TYPE_CAROUSEL)
    {
        $this->setType($amp_carousel_type);
    }

    /**
     * Generic accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'delay' => $this->delay,
            'height' => $this->height,
            'layout' => $this->getLayout(),
            'type' => $this->type,
            'width' => $this->width,
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Generic mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'delay' => $this->setDelay($delay),
            'height' => $this->setHeight($value),
            'layout' => $this->setLayout($value),
            'type' => $this->setType($value),
            'width' => $this->setWidth($value),
            default => throw new \LogicException('Undefined property: ' . __CLASS__ . '::$' . $name),
        };
    }

    /**
     * Gets the <amp-carousel> AMP carousel element.
     *
     * @param void
     *
     * @return string
     *   Returns the AMP tag `<amp-carousel …>…</amp-carousel>` as a string.
     */
    public function __toString(): string
    {
        $result = '<amp-carousel type="' . $this->type . '"';

        if ($this->getLayout() !== null) {
            $result .= ' layout="' . $this->getLayout() . '"';
        }

        if ($this->width !== null) {
            $result .= ' width="' . $this->width . '"';
        }
        if ($this->height !== null) {
            $result .= ' height="' . $this->height . '"';
        }

        $result .= ' ' . $this->role->value;
        if ($this->ariaLabel !== null) $result .= ' aria-label="' . $this->ariaLabel . '"';
        $result .= '>';

        if (!empty($this->children)) {
            foreach ($this->children as $child_node) {
                $result .= $child_node;
            }
        }

        $result .= '</amp-carousel>';
        return $result;
    }

    /**
     * Adds a photo, image, slide, or other node.
     *
     * @param StoreCore\AMP\BlockInterface|string $node
     *   HTML node to add to the children of the AMP carousel.
     *
     * @return int
     *   Returns the number of nodes in the carousel.
     *
     * @see https://www.php.net/array_push
     */
    public function append(BlockInterface|string $node): int
    {
        return array_push($this->children, (string) $node);
    }

    /**
     * Gets the AMP layout attribute.
     *
     * @param void
     *
     * @return string|null
     *   Returns the currently set AMP `layout` attribute as a string or `null`
     *   if the attribute is not set.
     */
    public function getLayout(): ?string
    {
        return $this->layout;
    }

    /**
     * @inheritDoc
     */
    public function isLightbox(): bool
    {
        return $this->lightbox;
    }

    /**
     * Enables autoplay on sliders.
     *
     * @param \DateInterval|int $delay
     *   OPTIONAL delay in milliseconds or a `DateInterval` for minutes and/or
     *   seconds.  Defaults to `5000` for a 5 seconds delay.
     *
     * @return void
     */
    public function setAutoplay(\DateInterval|int $delay = 5000): void
    {
        $this->autoplay = true;
        $this->setDelay($delay);
    }

    /**
     * Changes the slider delay.
     *
     * @param \DateInterval|int $delay
     *   Slider delay as an integer in milliseconds or a DateInterval in
     *   minutes and/or seconds.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws an invalid argument exception if the delay is not a number.
     */
    public function setDelay(\DateInterval|int $delay)
    {
        if (!is_int($delay)) {
            if ($delay instanceof \DateInterval) {
                $seconds = 60 * (int) $delay->format('%i') + (int) $delay->format('%s');
                $delay = 1000 * $seconds;
            } elseif (is_numeric($delay)) {
                $delay = (int) $delay;
            } else {
                throw new \ValueError();
            }
        }

        $this->delay = $delay;
    }

    /**
     * Sets the carousel height.
     *
     * @param int|string $height_in_pixels
     *   Carousel `height` in pixels (px) as an integer or a numeric string.
     *
     * @return void
     *
     * @throws \TypeError
     *   Throws an type error exception if the `height` is not a number.
     *
     * @throws \ValueError
     *   Throws a value error exception if the `height` is smaller than 1
     *   or greater than 16383.
     */
    public function setHeight(int|string $height_in_pixels): void
    {
        if (!is_int($height_in_pixels)) {
            if (is_numeric($height_in_pixels)) {
                $height_in_pixels = (int) $height_in_pixels;
            } else {
                throw new \TypeError();
            }
        }

        if ($height_in_pixels < 1 || $height_in_pixels > 16383) {
            throw new \ValueError();
        }
        $this->height = $height_in_pixels;
    }

    /**
     * Sets the AMP layout attribute.
     *
     * @param LayoutInterface|string $layout
     *   String value for the AMP `layout` attribute.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the layout is not supported.
     */
    public function setLayout(LayoutInterface|string $layout): void
    {
        $supported_layouts = [
            LayoutInterface::LAYOUT_FILL,
            LayoutInterface::LAYOUT_FIXED,
            LayoutInterface::LAYOUT_FIXED_HEIGHT,
            LayoutInterface::LAYOUT_FLEX_ITEM,
            LayoutInterface::LAYOUT_INTRINSIC,
            LayoutInterface::LAYOUT_NODISPLAY,
            LayoutInterface::LAYOUT_RESPONSIVE,
        ];
        if (!in_array($layout, $supported_layouts)) {
            throw new \ValueError;
        }

        $this->layout = $layout;
    }

    /**
     * @inheritDoc
     */
    public function setLightbox(bool $lightbox = true): void
    {
        $this->lightbox = $lightbox;
    }

    /**
     * Sets the carousel type.
     *
     * @param string $amp_carousel_type
     *   Case-insensitive AMP carousel type `carousel` (default) or `slides`.
     *
     * @return void
     *
     * @throws \ValueError
     *   Throws a value error exception if the carousel type is not the
     *   string value `carousel` or `slides`.
     */
    public function setType(string $amp_carousel_type): void
    {
        $amp_carousel_type = strtolower($amp_carousel_type);

        if ($amp_carousel_type === self::TYPE_CAROUSEL || $amp_carousel_type === self::TYPE_SLIDES) {
            $this->type = $amp_carousel_type;
        } else {
            throw new \ValueError();
        }
    }

    /**
     * Sets the carousel width.
     *
     * @param int|string $width_in_pixels
     *   Carousel width in pixels (px) as an integer or a numeric string.
     *
     * @return void
     *
     * @throws \TypeError
     *   Throws a type error exception if the `width` is not a number.
     *
     * @throws \ValueError
     *   Throws a value error exception if the `width` is smaller than 1
     *   or greater than 16383.
     * 
     * @see https://developers.google.com/speed/webp/faq#what_is_the_maximum_size_a_webp_image_can_be
     *   The maximum pixel dimensions of a WebP image is 16383 × 16383.
     */
    public function setWidth(int|string $width_in_pixels): void
    {
        if (!\is_int($width_in_pixels)) {
            if (is_numeric($width_in_pixels)) {
                $width_in_pixels = (int) $width_in_pixels;
            } else {
                throw new \TypeError();
            }
        }

        if ($width_in_pixels < 1 || $width_in_pixels > 16383) {
            throw new \ValueError();
        }
        $this->width = $width_in_pixels;
    }
}
