<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP layout interface.
 *
 * This interface describes the Accelerated Mobile Pages (AMP) `layout`
 * attribute for view models.  The interface constants are values that can be
 * used in the `layout` attribute.  The attribute MUST be accessed through the
 * setter and getter `setLayout()` and `getLayout()`.
 *
 * @api
 * @package StoreCore\CMS
 * @version 1.0.0
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/control_layout/
 *      Layout & media queries - amp.dev
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/learn/amp-html-layout/layouts_demonstrated/
 *      Demonstrating AMP layouts - amp.dev
 */
interface LayoutInterface
{
    /**
     * @var string LAYOUT_CONTAINER
     * @var string LAYOUT_FILL
     * @var string LAYOUT_FIXED
     * @var string LAYOUT_FIXED_HEIGHT
     * @var string LAYOUT_FLEX_ITEM
     * @var string LAYOUT_INTRINSIC
     * @var string LAYOUT_NODISPLAY
     * @var string LAYOUT_RESPONSIVE
     */
    public const LAYOUT_CONTAINER    = 'container';
    public const LAYOUT_FILL         = 'fill';
    public const LAYOUT_FIXED        = 'fixed';
    public const LAYOUT_FIXED_HEIGHT = 'fixed-height';
    public const LAYOUT_FLEX_ITEM    = 'flex-item';
    public const LAYOUT_INTRINSIC    = 'intrinsic';
    public const LAYOUT_NODISPLAY    = 'nodisplay';
    public const LAYOUT_RESPONSIVE   = 'responsive';

    /**
     * Gets the AMP layout attribute.
     *
     * @param void
     *
     * @return null|string
     *   Returns the currently set AMP `layout` attribute as a string.  This
     *   method MAY return `null` if the `layout` attribute of an AMP component
     *   is not yet set, but MUST return a string if the component has a
     *   default `layout`.
     */
    public function getLayout(): ?string;

    /**
     * Sets the AMP layout attribute.
     *
     * @param string $layout
     *   String value for the AMP layout attribute.
     *
     * @return void
     */
    public function setLayout(string $layout): void;
}
