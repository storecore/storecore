<?php

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP keyframes.
 *
 * @see https://amp.dev/documentation/guides-and-tutorials/develop/animations/triggering_css_animations/?format=websites
 *      Triggering CSS animations & transitions
 *
 * @see https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css
 *      Latest version of minified Material CSS
 */
class Keyframes
{
    protected array $keyframes = array(

    );
}
