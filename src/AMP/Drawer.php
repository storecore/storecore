<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019, 2021, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * Navigation drawer.
 *
 * The navigation drawer provided by this class is one of the main
 * implementations of the AMP HTML `<amp-sidebar>` component.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 *
 * @see https://m3.material.io/components/navigation-drawer/overview
 *      “Navigation drawer” in Material Design 3 (M3)
 * 
 * @see https://m2.material.io/components/navigation-drawer
 *      “Navigation drawer” in Material Design 2 (M2)
 * 
 * @see https://m3.material.io/components/bottom-sheets/overview
 *      “Bottom sheets” in Material Design 3 (M3)
 * 
 * @see https://m3.material.io/components/side-sheets/overview
 *      “Side sheets” in Material Design 3 (M3)
 */
final class Drawer extends Sidebar implements BlockInterface, LayoutInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $attributes
     *   AMP HTML attributes of the `<amp-sidebar>` component.  Defaults to
     *   `<amp-sidebar class="mdc-drawer" id="drawer" layout="nodisplay"
     *   on="tap:drawer.close" side="left">`.  Please note that the unique DOM
     *   object ID is set to `id="drawer"` for a navigation drawer.
     */
    protected array $attributes = [
        'class' => 'mdc-drawer',
        'id' => 'drawer',
        'layout' => LayoutInterface::LAYOUT_NODISPLAY,
        'on' => 'tap:drawer.close',
        'side'  => 'left',
    ];
}
