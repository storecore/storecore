<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2019–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

/**
 * AMP sidebar component <amp-sidebar>.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 *
 * @see https://amp.dev/documentation/components/amp-sidebar/
 *      Documentation: <amp-sidebar> - amp.dev
 *
 * @see https://amp.dev/documentation/examples/components/amp-sidebar/
 *      Example: <amp-sidebar> - amp.dev
 */
class Sidebar extends AbstractComponent implements BlockInterface, LayoutInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @inheritDoc
     */
    protected const array SUPPORTED_LAYOUTS = [
        LayoutInterface::LAYOUT_NODISPLAY,
    ];

    /**
     * @var array $attributes
     *   AMP HTML attributes of the `<amp-sidebar>` component.  Defaults to
     *   `<amp-sidebar layout="nodisplay" side="left">` for an initially
     *   hidden sidebar on the left.
     */
    protected array $attributes = [
        'layout' => LayoutInterface::LAYOUT_NODISPLAY,
        'side'   => 'left',
    ];

    /**
     * Converts the <amp-sidebar> component to a string.
     *
     * @param void
     *
     * @return string
     *   Returns the AMP HTML component `<amp-sidebar>…</amp-sidebar>`
     *   as a string with attributes and contents.
     */
    public function __toString(): string
    {
        $result = '<amp-sidebar';
        foreach ($this->attributes as $attribute => $value) {
            $result .= ' ' . $attribute . '="' . $value . '"';
        }
        $result .= '>' . $this->innerHTML . '</amp-sidebar>';
        return $result;
    }
}
