<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\AMP;

use \JsonSerializable;

use function \defined;
use function \json_decode;

/**
 * AMP Analytics <amp-analytics> component for Google Analytics.
 *
 * @api
 * @package StoreCore\BI
 * @version 0.2.0
 *
 * @see https://developers.google.com/analytics/devguides/collection/amp-analytics/
 *      Add Analytics to AMP pages
 *
 * @see https://support.google.com/analytics/answer/6343176?hl=en
 *      Measurement of Accelerated Mobile Pages (AMP)
 *
 * @see https://developers.google.com/gtagjs/devguide/amp
 *      Global site tag for AMP - Global site tag (gtag.js)
 */
final class GoogleAnalytics extends AbstractAnalytics implements BlockInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @inheritDoc
     */
    public const TYPE = 'gtag';

    /**
     * @inheritDoc
     */
    public bool $dataCredentials = true;

    /**
     * @var string $googleTagID
     *   Unique identifier for Google Analytics, also known as the “Google tag”,
     *   the “Google Analytics Tracking ID”, the “Google Analytics Measurement ID”,
     *   and the “Google Analytics Web Property ID”.
     */
    public readonly string $googleTagID;

    /**
     * Creates an AMP component for Google Analytics.
     *
     * @param string|null $google_tag_id
     *   Optional identifier of a Google Analytics property and the `gtag_id`
     *   in the JSON script for Google Analytics.  If this paramater is not set,
     *   the global constant `STORECORE_BI_GOOGLE_TAG_ID` is used.  Falls
     *   back to the test string value 'G-XXXXXXXXX' if no ID was found.
     *
     * @see https://support.google.com/analytics/answer/12326985?hl=en
     *      Google tag ID: Definition, Google Analytics Help Glossary
     */
    public function __construct(?string $google_tag_id = null)
    {
        if ($google_tag_id !== null) {
            $this->googleTagID = $google_tag_id;
        } elseif (defined('STORECORE_BI_GOOGLE_TAG_ID')) {
            $this->googleTagID = STORECORE_BI_GOOGLE_TAG_ID;
        } else {
            $this->googleTagID = 'G-XXXXXXXXX';
        }

        $this->data = json_decode('
            {
              "vars": {
                "gtag_id": "' . $this->googleTagID . '",
                "config": {
                  "' . $this->googleTagID . '": {
                    "groups": "default",
                    "site_speed_sample_rate": 1
                  }
                }
              }
            }
        ', true);
    }

    /**
     * Sets the performance measurement sample rate.
     *
     * @param int $site_speed_sample_rate
     *   Percentage of users as an integer from `0` to `100` that determines how
     *   often site speed beacons will be sent by Google Analytics.  By default,
     *   `1` percent of users will be measured.
     *
     * @return void
     */
    public function setSiteSpeedSampleRate(int $site_speed_sample_rate): void
    {
        if ($site_speed_sample_rate < 0) {
            $site_speed_sample_rate = 0;
        } elseif ($site_speed_sample_rate > 100) {
            $site_speed_sample_rate = 100;
        }

        $this->data['vars']['config'][$this->googleTagID]['site_speed_sample_rate'] = $site_speed_sample_rate;
    }
}
