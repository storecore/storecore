<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Types\UUID;

/**
 * Identity trait.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
trait IdentityTrait
{
    /**
     * @var null|StoreCore\Types\UUID $identifier
     *   Universally unique identifier (UUID) of a value object.
     */
    protected ?UUID $identifier = null;

    /**
     * Gets the object identifier.
     *
     * @param void
     * 
     * @return null|StoreCore\Types\UUID
     *   Returns an object identifier as a universally unique identifier (UUID)
     *   or `null` if the object has no identifier;
     */
    public function getIdentifier(): ?UUID
    {
        return $this->identifier;
    }

    /**
     * Checks if the object identifier was set.
     *
     * @param void
     * 
     * @return bool
     *   Returns `true` if the object has an identifier, otherwise `false`.
     */
    public function hasIdentifier(): bool
    {
        return $this->identifier !== null ? true : false;
    }

    /**
     * Sets the object identifier.
     *
     * @param StoreCore\Types\UUID|string $uuid
     *   Universally unique identifier (UUID) as an object or a string.
     *
     * @return void
     */
    public function setIdentifier(UUID|string $uuid): void
    {
        if (\is_string($uuid)) {
            $uuid = new UUID($uuid);
        }
        $this->identifier = $uuid;
    }
}
