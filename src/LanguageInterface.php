<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\I18N\Language;
use StoreCore\Types\LanguageCode;

/**
 * Language interface.
 *
 * @api
 * @package StoreCore\I18N
 * @see     https://schema.org/inLanguage Schema.org property `inLanguage`
 * @version 1.0.0
 */
interface LanguageInterface
{
    /**
     * Gets and optionally sets a language.
     *
     * @param null|StoreCore\I18N\Language|StoreCore\Types\LanguageCode|string $language
     *   OPTIONAL language to use for language-specific features.
     *
     * @return string
     *   Currently used language as a language code string from the IETF BCP 47 standard.
     */
    public function inLanguage(null|Language|LanguageCode|string $language = null): string;
}
