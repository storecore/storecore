<?php

declare(strict_types=1);

namespace StoreCore;

interface SingletonInterface
{
    public static function getInstance();
}
