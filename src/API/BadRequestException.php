<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * 400 Bad Request.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class BadRequestException extends AbstractClientException implements ClientExceptionInterface
{
    #[\Override]
    public function __construct(
        string $message = 'Bad Request',
        int $code = 400,
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
