<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Abstract HTTP 4xx client error.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://www.php-fig.org/psr/psr-18/ PSR-18: HTTP Client
 * @see     https://httpwg.org/specs/rfc9110.html#overview.of.status.codes
 * @version 1.0.0
 */
abstract class AbstractClientException extends \RuntimeException implements
    ClientExceptionInterface,
    \Throwable {}
