<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * 404 Not Found.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0
 */
class NotFoundException extends AbstractClientException implements ClientExceptionInterface
{
    public function __construct(
        string $message = 'Not Found',
        int $code = 404,
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
