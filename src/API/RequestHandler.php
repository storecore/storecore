<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace StoreCore\API;

use StoreCore\AbstractController;
use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\Engine\ResponseFactory;
use StoreCore\Engine\StreamFactory;
use StoreCore\Types\{URL, WebAPI};

/**
 * StoreCore™ RESTful API request handler.
 *
 * @api
 * @package StoreCore\Core
 * @version 0.1.0
 */
class RequestHandler extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * Handles an API server request.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     *
     * @return Psr\Http\Message\ResponseInterface
     *
     * @uses StoreCore\Engine\ResponseFactory::createResponse()
     *
     * @throws \TypeError
     *   Throws a type error exception if the `$request` is not an instance of
     *   the PSR-7 `Psr\Http\Message\ServerRequestInterface`.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /* Only allow GET, HEAD, POST, PUT, PATCH, or DELETE request method
         * for CRUD operations in the context of RESTful APIs.  An origin
         * server MUST generate an `Allow` header field in a 405 (Method Not
         * Allowed) response and MAY do so in any other response.
         */
        if (!\in_array($request->getMethod(), ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'])) {
            $factory = new ResponseFactory();
            $response = $factory->createResponse(405);
            $response->setHeader('Allow', 'GET, HEAD, POST, PUT, PATCH, DELETE');
            return $response;
        }

        /* API status at the root.  If StoreCore is installed correctly,
         * you SHOULD get a human readable, “pretty printed” JSON response
         * with API information.
         */
        if ($request->getRequestTarget() === '/api/v1') {
            $factory = new ResponseFactory();
            if ($request->getMethod() === 'HEAD') {
                $response = $factory->createResponse(204);
            } else {
                $response = $factory->createResponse(200);
            }
            $response->setHeader('Allow', 'GET, HEAD');

            $filename = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'provider.json');
            if ($filename === false) {
                $filename = __DIR__ . DIRECTORY_SEPARATOR . 'provider.json';
            }
            if (is_readable($filename)) {
                $response->setHeader('Cache-Control', 'public, max-age=300, s-max-age=3600');
                $response->setHeader('Content-Type', 'application/ld+json; charset=utf-8');
                $response->setHeader('Last-Modified', gmdate('D, d M Y H:i:s T', filemtime($filename)));
                if ($response->getStatusCode() !== 204) {
                    $factory = new StreamFactory();
                    $body = $factory->createStreamFromFile($filename, 'r');
                    $response->setBody($body);
                }
            }

            return $response;
        }

        // Only allow requests starting with `/api/v1/` for version 1 of the API.
        if (!str_starts_with($request->getRequestTarget(), '/api/v1/')) {
            throw new NotFoundException();
        }

        if (str_starts_with($request->getRequestTarget(), '/api/v1/brands')) {
            $controller = new Brands($this->Registry);
            return $controller->handle($request);
        }

        // Throw `404 Not Found` if we have no response by now.
        throw new NotFoundException();
    }

    /**
     * Returns the JSON-LD representation of an object or an array.
     *
     * @param \JsonSerializable|array $value
     * @return string
     * @see https://www.php.net/json_encode PHP function `json_encode()`
     * @see https://w3c.github.io/json-ld-bp/ JSON-LD Best Practices
     * @throws StoreCore\API\InternalServerErrorException
     */
    public static function jsonEncode(\JsonSerializable|array $value): string
    {
        try {
            return json_encode($value, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE | \JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            $this->Logger->error('JSON exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage(), $e->getTrace());
            throw new InternalServerErrorException($e->getMessage(), (int) $e->getCode(), $e);
        }
    }
}
