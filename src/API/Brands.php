<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\RequestHandlerInterface;
use StoreCore\AbstractController;
use StoreCore\Database\BrandRepository;
use StoreCore\Engine\ResponseFactory;
use StoreCore\Engine\StreamFactory;
use StoreCore\Types\QuantitativeValue;
use StoreCore\Types\UUID;

/**
 * API endpoint for brands.
 *
 * @api
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 */
class Brands extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\BrandRepository $model
     *   Data model for brand information.
     */
    private ?BrandRepository $model = null;

    /**
     * Counts the number of brands.
     *
     * @param void
     * @return Psr\Http\Message\ResponseInterface
     */
    public function count(): ResponseInterface
    {
        $this->model = new BrandRepository($this->Registry);
        $count = $this->model->count();
        $last_modified = $this->model->getLastModified();

        $json = ['value' => $count];
        $json = json_encode($json);
        $factory = new StreamFactory();
        $json = $factory->createStream($json);

        $factory = new ResponseFactory();
        $response = $factory->createResponse(200);
        $response->setHeader('Allow', 'GET, HEAD');
        $response->setHeader('Content-Type', 'application/ld+json; charset=utf-8');
        if ($last_modified instanceof \DateTimeInterface) {
            $response->setHeader('Last-Modified', $last_modified->format('D, d M Y H:i:s T'));
        }
        $response->setBody($json);
        return $response;
    }

    /**
     * Gets a brand.
     *
     * @param void
     * @return Psr\Http\Message\ResponseInterface
     * @throws Psr\Http\Client\ClientExceptionInterface
     */
    public function get(string $uuid): ResponseInterface
    {
        $uuid = strip_tags($uuid);
        $uuid = strtolower($uuid);
        $uuid = str_replace('-', '', $uuid);

        $this->model = new BrandRepository($this->Registry);
        if (!$this->model->has($uuid)) {
            throw new NotFoundException();
        }

        $brand = $this->model->get($uuid);
        $json = RequestHandler::jsonEncode($brand);
        $factory = new StreamFactory();
        $json = $factory->createStream($json);

        $factory = new ResponseFactory();
        $response = $factory->createResponse(200);
        $response->setHeader('Allow', 'GET');
        $response->setHeader('Content-Type', 'application/ld+json; charset=utf-8');
        $response->setBody($json);
        return $response;
    }

    /**
     * Handles an API server request for brands.
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     * @return Psr\Http\Message\ResponseInterface
     * @throws Psr\Http\Client\ClientExceptionInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getRequestTarget() === '/api/v1/brands:count') {
            return $this->count();
        }

        if ($request->getRequestTarget() === '/api/v1/brands') {
            if ($request->getMethod() === 'GET') {
                return $this->list();
            }
        }

        if (str_starts_with($request->getRequestTarget(), '/api/v1/brands/')) {
            $uuid = $request->getRequestTarget();
            $uuid = explode('/', $uuid);
            $uuid = end($uuid);
            if (!UUID::validate($uuid)) {
                $factory = new ResponseFactory();
                return $factory->createResponse(400);
            }

            if ($request->getMethod() === 'GET') {
                return $this->get($uuid);
            }
        }

        $factory = new ResponseFactory();
        return $factory->createResponse(404);
    }

    /**
     * Lists brands.
     *
     * @param void
     * @return Psr\Http\Message\ResponseInterface
     */
    public function list(): ResponseInterface
    {
        $this->model = new BrandRepository($this->Registry);
        $list = $this->model->list();
        $last_modified = $this->model->getLastModified();

        $json = RequestHandler::jsonEncode($list);
        $factory = new StreamFactory();
        $json = $factory->createStream($json);

        $factory = new ResponseFactory();
        $response = $factory->createResponse(200);
        $response->setHeader('Allow', 'GET, HEAD, PATCH, POST, PUT');
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');
        if ($last_modified instanceof \DateTimeInterface) {
            $response->setHeader('Last-Modified', $last_modified->format('D, d M Y H:i:s T'));
        }
        $response->setBody($json);
        return $response;
    }
}
