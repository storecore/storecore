<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\API;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * 500 Internal Server Error.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://httpwg.org/specs/rfc9110.html#rfc.section.15.6.1
 * @version 1.0.0
 */
class InternalServerErrorException extends \LogicException implements \Throwable
{
    public function __construct(
        string $message = 'Internal Server Error',
        int $code = 500,
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
