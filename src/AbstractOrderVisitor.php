<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Visitor design pattern: abstract order visitor.
 *
 * The abstract order visitor implements the `VisitorInterface` by
 * forcing concrete order visitors to implement the `visit()` method.
 * 
 * @package StoreCore\Core
 * @version 1.0.0
 */
abstract class AbstractOrderVisitor implements VisitorInterface
{
    /**
     * Concrete visit of an order.
     *
     * @param StoreCore\Order $order
     * @return void
     */
    abstract public function visit(Order $order): void;

    /**
     * Abstract implementation of an order visit.
     *
     * @param StoreCore\Order $order
     * @return void
     */
    #[\Override]
    public function visitOrder(Order $order): void
    {
        $this->visit($order);
    }
}
