<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2018–2019, 2021–2022, 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

/**
 * Wish list model.
 *
 * A wish list, like a list of favorites, is a list of products
 * that a customer or prospect might want to buy.
 *
 * @package StoreCore\OML
 * @version 1.0.0-alpha.1
 */
class WishList extends ShoppingList implements \Countable, IdentityInterface, VisitableInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';
}
