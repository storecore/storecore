<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

/**
 * People audience.
 *
 * A set of characteristics belonging to people, e.g. who compose an item’s
 * target audience.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/PeopleAudience Schema.org type `PeopleAudience`
 * @version 1.0.0-alpha.1
 */
class PeopleAudience extends Audience implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var int|float|null $requiredMinAge
     *   Audiences defined by a person’s minimum age.
     */
    public int|float|null $requiredMinAge = null;

    /**
     * @var int|float|null $requiredMaxAge
     *   Audiences defined by a person’s maximum age.
     */
    public int|float|null $requiredMaxAge = null;

    /**
     * @var int|float|null $suggestedMinAge
     *   Minimum recommended age in years for the audience or user.
     */
    public int|float|null $suggestedMinAge = null;

    /**
     * @var int|float|null $suggestedMaxAge
     *   Maximum recommended age in years for the audience or user.
     */
    public int|float|null $suggestedMaxAge = null;
}
