<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\{ProxyInterface, Proxy};
use StoreCore\Types\UUID;

interface_exists(IdentityInterface::class);
interface_exists(ProxyInterface::class);
class_exists(Proxy::class);
class_exists(UUID::class);

/**
 * Person proxy.
 *
 * @api
 * @package StoreCore\CRM
 * @version 1.0.0-alpha.1
 */
readonly class PersonProxy extends Proxy implements
    IdentityInterface,
    \JsonSerializable,
    ProxyInterface,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    #[\Override]
    public function __construct(
        UUID|\Stringable|string $identifier,
        string $type = 'Person',
        ?string $name = null
    ) {
        parent::__construct($identifier, $type, $name);
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['@id'] = rtrim(STORECORE_API_ROOT, '/') . '/persons/' . $this->getIdentifier();
        return $result;
    }
}
