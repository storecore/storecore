<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

/**
 * Schema.org organization types.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Organization#subtypes More specific types of an `Organization`
 * @version 23.0.0
 */
enum OrganizationType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '23.0.0';

    case AccountingService = 'https://schema.org/AccountingService';
    case AdultEntertainment = 'https://schema.org/AdultEntertainment';
    case Airline = 'https://schema.org/Airline';
    case AmusementPark = 'https://schema.org/AmusementPark';
    case AnimalShelter = 'https://schema.org/AnimalShelter';
    case ArchiveOrganization = 'https://schema.org/ArchiveOrganization';
    case ArtGallery = 'https://schema.org/ArtGallery';
    case Attorney = 'https://schema.org/Attorney';
    case AutoBodyShop = 'https://schema.org/AutoBodyShop';
    case AutoDealer = 'https://schema.org/AutoDealer';
    case AutomatedTeller = 'https://schema.org/AutomatedTeller';
    case AutomotiveBusiness = 'https://schema.org/AutomotiveBusiness';
    case AutoPartsStore = 'https://schema.org/AutoPartsStore';
    case AutoRental = 'https://schema.org/AutoRental';
    case AutoRepair = 'https://schema.org/AutoRepair';
    case AutoWash = 'https://schema.org/AutoWash';
    case Bakery = 'https://schema.org/Bakery';
    case BankOrCreditUnion = 'https://schema.org/BankOrCreditUnion';
    case BarOrPub = 'https://schema.org/BarOrPub';
    case BeautySalon = 'https://schema.org/BeautySalon';
    case BedAndBreakfast = 'https://schema.org/BedAndBreakfast';
    case BikeStore = 'https://schema.org/BikeStore';
    case BookStore = 'https://schema.org/BookStore';
    case BowlingAlley = 'https://schema.org/BowlingAlley';
    case Brewery = 'https://schema.org/Brewery';
    case CafeOrCoffeeShop = 'https://schema.org/CafeOrCoffeeShop';
    case Campground = 'https://schema.org/Campground';
    case Casino = 'https://schema.org/Casino';
    case ChildCare = 'https://schema.org/ChildCare';
    case ClothingStore = 'https://schema.org/ClothingStore';
    case CollegeOrUniversity = 'https://schema.org/CollegeOrUniversity';
    case ComedyClub = 'https://schema.org/ComedyClub';
    case ComputerStore = 'https://schema.org/ComputerStore';
    case Consortium = 'https://schema.org/Consortium';
    case ConvenienceStore = 'https://schema.org/ConvenienceStore';
    case Corporation = 'https://schema.org/Corporation';
    case DanceGroup = 'https://schema.org/DanceGroup';
    case DaySpa = 'https://schema.org/DaySpa';
    case Dentist = 'https://schema.org/Dentist';
    case DepartmentStore = 'https://schema.org/DepartmentStore';
    case DiagnosticLab = 'https://schema.org/DiagnosticLab';
    case Distillery = 'https://schema.org/Distillery';
    case DryCleaningOrLaundry = 'https://schema.org/DryCleaningOrLaundry';
    case EducationalOrganization = 'https://schema.org/EducationalOrganization';
    case Electrician = 'https://schema.org/Electrician';
    case ElectronicsStore = 'https://schema.org/ElectronicsStore';
    case ElementarySchool = 'https://schema.org/ElementarySchool';
    case EmergencyService = 'https://schema.org/EmergencyService';
    case EmploymentAgency = 'https://schema.org/EmploymentAgency';
    case EntertainmentBusiness = 'https://schema.org/EntertainmentBusiness';
    case ExerciseGym = 'https://schema.org/ExerciseGym';
    case FastFoodRestaurant = 'https://schema.org/FastFoodRestaurant';
    case FinancialService = 'https://schema.org/FinancialService';
    case FireStation = 'https://schema.org/FireStation';
    case Florist = 'https://schema.org/Florist';
    case FoodEstablishment = 'https://schema.org/FoodEstablishment';
    case FundingAgency = 'https://schema.org/FundingAgency';
    case FundingScheme = 'https://schema.org/FundingScheme';
    case FurnitureStore = 'https://schema.org/FurnitureStore';
    case GardenStore = 'https://schema.org/GardenStore';
    case GasStation = 'https://schema.org/GasStation';
    case GeneralContractor = 'https://schema.org/GeneralContractor';
    case GolfCourse = 'https://schema.org/GolfCourse';
    case GovernmentOffice = 'https://schema.org/GovernmentOffice';
    case GovernmentOrganization = 'https://schema.org/GovernmentOrganization';
    case GroceryStore = 'https://schema.org/GroceryStore';
    case HairSalon = 'https://schema.org/HairSalon';
    case HardwareStore = 'https://schema.org/HardwareStore';
    case HealthAndBeautyBusiness = 'https://schema.org/HealthAndBeautyBusiness';
    case HealthClub = 'https://schema.org/HealthClub';
    case HighSchool = 'https://schema.org/HighSchool';
    case HobbyShop = 'https://schema.org/HobbyShop';
    case HomeAndConstructionBusiness = 'https://schema.org/HomeAndConstructionBusiness';
    case HomeGoodsStore = 'https://schema.org/HomeGoodsStore';
    case Hospital = 'https://schema.org/Hospital';
    case Hostel = 'https://schema.org/Hostel';
    case Hotel = 'https://schema.org/Hotel';
    case HousePainter = 'https://schema.org/HousePainter';
    case HVACBusiness = 'https://schema.org/HVACBusiness';
    case IceCreamShop = 'https://schema.org/IceCreamShop';
    case InsuranceAgency = 'https://schema.org/InsuranceAgency';
    case InternetCafe = 'https://schema.org/InternetCafe';
    case JewelryStore = 'https://schema.org/JewelryStore';
    case LegalService = 'https://schema.org/LegalService';
    case Library = 'https://schema.org/Library';
    case LibrarySystem = 'https://schema.org/LibrarySystem';
    case LiquorStore = 'https://schema.org/LiquorStore';
    case LocalBusiness = 'https://schema.org/LocalBusiness';
    case Locksmith = 'https://schema.org/Locksmith';
    case LodgingBusiness = 'https://schema.org/LodgingBusiness';
    case MedicalBusiness = 'https://schema.org/MedicalBusiness';
    case MedicalClinic = 'https://schema.org/MedicalClinic';
    case MedicalOrganization = 'https://schema.org/MedicalOrganization';
    case MensClothingStore = 'https://schema.org/MensClothingStore';
    case MiddleSchool = 'https://schema.org/MiddleSchool';
    case MobilePhoneStore = 'https://schema.org/MobilePhoneStore';
    case Motel = 'https://schema.org/Motel';
    case MotorcycleDealer = 'https://schema.org/MotorcycleDealer';
    case MotorcycleRepair = 'https://schema.org/MotorcycleRepair';
    case MovieRentalStore = 'https://schema.org/MovieRentalStore';
    case MovieTheater = 'https://schema.org/MovieTheater';
    case MovingCompany = 'https://schema.org/MovingCompany';
    case MusicGroup = 'https://schema.org/MusicGroup';
    case MusicStore = 'https://schema.org/MusicStore';
    case NailSalon = 'https://schema.org/NailSalon';
    case NewsMediaOrganization = 'https://schema.org/NewsMediaOrganization';
    case NGO = 'https://schema.org/NGO';
    case NightClub = 'https://schema.org/NightClub';
    case Notary = 'https://schema.org/Notary';
    case OfficeEquipmentStore = 'https://schema.org/OfficeEquipmentStore';
    case OnlineBusiness = 'https://schema.org/OnlineBusiness';
    case OnlineStore = 'https://schema.org/OnlineStore';
    case Optician = 'https://schema.org/Optician';
    case Organization = 'https://schema.org/Organization';
    case OutletStore = 'https://schema.org/OutletStore';
    case PawnShop = 'https://schema.org/PawnShop';
    case PerformingGroup = 'https://schema.org/PerformingGroup';
    case PetStore = 'https://schema.org/PetStore';
    case Pharmacy = 'https://schema.org/Pharmacy';
    case Physician = 'https://schema.org/Physician';
    case Plumber = 'https://schema.org/Plumber';
    case PoliceStation = 'https://schema.org/PoliceStation';
    case PostOffice = 'https://schema.org/PostOffice';
    case Preschool = 'https://schema.org/Preschool';
    case ProfessionalService = 'https://schema.org/ProfessionalService';
    case Project = 'https://schema.org/Project';
    case PublicSwimmingPool = 'https://schema.org/PublicSwimmingPool';
    case RadioStation = 'https://schema.org/RadioStation';
    case RealEstateAgent = 'https://schema.org/RealEstateAgent';
    case RecyclingCenter = 'https://schema.org/RecyclingCenter';
    case ResearchOrganization = 'https://schema.org/ResearchOrganization';
    case ResearchProject = 'https://schema.org/ResearchProject';
    case Resort = 'https://schema.org/Resort';
    case Restaurant = 'https://schema.org/Restaurant';
    case RoofingContractor = 'https://schema.org/RoofingContractor';
    case School = 'https://schema.org/School';
    case SearchRescueOrganization = 'https://schema.org/SearchRescueOrganization';
    case SelfStorage = 'https://schema.org/SelfStorage';
    case ShoeStore = 'https://schema.org/ShoeStore';
    case ShoppingCenter = 'https://schema.org/ShoppingCenter';
    case SkiResort = 'https://schema.org/SkiResort';
    case SportingGoodsStore = 'https://schema.org/SportingGoodsStore';
    case SportsActivityLocation = 'https://schema.org/SportsActivityLocation';
    case SportsClub = 'https://schema.org/SportsClub';
    case SportsOrganization = 'https://schema.org/SportsOrganization';
    case SportsTeam = 'https://schema.org/SportsTeam';
    case StadiumOrArena = 'https://schema.org/StadiumOrArena';
    case Store = 'https://schema.org/Store';
    case TattooParlor = 'https://schema.org/TattooParlor';
    case TelevisionStation = 'https://schema.org/TelevisionStation';
    case TennisComplex = 'https://schema.org/TennisComplex';
    case TheaterGroup = 'https://schema.org/TheaterGroup';
    case TireShop = 'https://schema.org/TireShop';
    case TouristInformationCenter = 'https://schema.org/TouristInformationCenter';
    case ToyStore = 'https://schema.org/ToyStore';
    case TravelAgency = 'https://schema.org/TravelAgency';
    case VeterinaryCare = 'https://schema.org/VeterinaryCare';
    case WholesaleStore = 'https://schema.org/WholesaleStore';
    case Winery = 'https://schema.org/Winery';
    case WorkersUnion = 'https://schema.org/WorkersUnion';
}
