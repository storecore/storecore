<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\Types\URL;

/**
 * Bank or credit union.
 *
 * @package StoreCore\CRM
 * @version 1.0.0-rc.1
 * @see     https://schema.org/BankOrCreditUnion Schema.org type `BankOrCreditUnion`
 */
class BankOrCreditUnion extends FinancialService implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
