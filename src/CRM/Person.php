<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016-2017, 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\AbstractSubject;
use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Geo\Country;
use StoreCore\Geo\Place;
use StoreCore\Google\People\Name;
use StoreCore\I18N\Language;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\DefinedTerm;
use StoreCore\Types\EmailAddress;
use StoreCore\Types\GenderType;
use StoreCore\Types\URL;

use function \is_string;

interface_exists(\StoreCore\IdentityInterface::class);
trait_exists(\StoreCore\IdentityTrait::class);
class_exists(\StoreCore\AbstractSubject::class);
class_exists(\StoreCore\Google\People\Name::class);

/**
 * Person.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/Person Schema.org type `Person`
 * @version 0.4.2
 */
class Person extends AbstractSubject implements
    IdentityInterface,
    \JsonSerializable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.2';

    /**
     * @var StoreCore\Types\Date|null  $birthDate  Date of birth.
     * @var StoreCore\Geo\Place|null $birthPlace The place where the person was born..
     */
    public ?Date $birthDate = null;
    public ?Place $birthPlace = null;

    /**
     * @var StoreCore\Types\DateTime $dateCreated
     * @var StoreCore\Types\DateTime|null $dateDeleted
     * @var StoreCore\Types\DateTime|null $dateModified
     */
    public DateTime $dateCreated;
    public ?DateTime $dateDeleted = null;
    public ?DateTime $dateModified = null;

    /**
     * @var null|StoreCore\Types\Date $deathDate
     *   Date of death.
     */
    public ?Date $deathDate = null;

    /**
     * @var null|StoreCore\Geo\Place $deathPlace
     *   The place where the person died.
     */
    public ?Place $deathPlace = null;

    /**
     * @var StoreCore\EmailAddress|string|null $emailAddress
     */
    public EmailAddress|string|null $emailAddress = null;

    /**
     * @var string|null $eTag
     *   Entity tag (ETag) of the person entity.  This ETag is used
     *   internally to detect changes of the object properties.
     */
    public ?string $eTag = null;

    /**
     * @var int $gender
     *   The ISO/IEC 5218 gender code `0` for not known, `1` for male,
     *   or `2` for female.  Defaults to `0` if the gender was not set.  Because
     *   the formal ISO/IEC gender codes are not always obvious, you MAY also
     *   use the `isFemale()` and `isMale()` class methods to test for
     *   a specific gender.
     */
    public int $gender = 0;

    /**
     * @var bool $isAnonymous
     *   Flag that indicates if the person’s data were anonymized (`true`)
     *   or not (default `false`).
     */
    public bool $isAnonymous = false;

    /**
     * @var null|DefinedTerm|string
     *   The job title of the person (for example, Financial Manager).
     */
    public null|DefinedTerm|string $jobTitle = null;

    /**
     * @var null|StoreCore\I18N\Language $knowsLanguage
     *   Of a `Person`, and less typically of an `Organization`, to indicate
     *   a known language.  Use language codes from the IETF BCP 47 standard.
     */
    public null|(\Stringable&Language) $knowsLanguage = null;

    /**
     * @var null|StoreCore\Geo\Country $nationality
     *   Nationality of the person.
     */
    public ?Country $nationality = null;

    /**
     * @var StoreCore\Google\People\Name|null $name
     *   OPTIONAL name of the `Person`.  MAY be `null` if the `Person` is
     *   still unknown or personally identifiable information (PII) of a known
     *   `Person` was anonymized.
     */
    protected ?Name $name = null;

    /**
     * @var StoreCore\Types\URL|null $sameAs
     *   Generic Schema.org `Thing.sameAs` property.  URL of a reference Web
     *   page that unambiguously indicates the item’s identity.  E.g. the URL
     *   of the item’s Wikipedia page, Wikidata entry, or official website.
     */
    public ?URL $sameAs = null;

    /**
     * @var string|null $telephoneNumber
     *   The telephone number.
     */
    public ?string $telephoneNumber = null;

    /**
     * @var null|URL $url
     *   URL of the item.
     */
    protected ?URL $url;

    /**
     * Creates a person.
     *
     * @param string|StoreCore\Google\People\Name|null $name
     *   OPTIONAL `name` of the `Person` as a string or a `Name` object.
     *
     * @uses StoreCore\Types\DateTime::__construct()
     *
     * @uses setName()
     */
    public function __construct(string|Name|null $name = null)
    {
        $this->dateCreated = new DateTime('now');
        if ($name !== null) $this->setName($name);
    }

    /**
     * Reads data from inaccessible (protected or private) or non-existing properties.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'displayName' => $this->name !== null ? $this->name->getDisplayName() : null,
            'email' => $this->emailAddress !== null ? (string) $this->emailAddress : null,
            'familyName', 'lastName' => $this->name !== null ? $this->name->familyName : null,
            'firstName', 'givenName' => $this->name !== null ? $this->name->givenName : null,
            'honorificPrefix' => $this->name !== null ? $this->name->honorificPrefix : null,
            'honorificSuffix' => $this->name !== null ? $this->name->honorificSuffix : null,
            'identifier' => $this->getIdentifier(),
            'name' => $this->getName(),
            'phoneNumber' => $this->telephoneNumber,
            'url' => $this->url,
            default => null,
        };
    }

    /**
     * Writes data to inaccessible (protected or private) properties.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws \ValueError
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'displayName', 'name' => $this->setName($value),
            'email' => $this->emailAddress = $value,
            'familyName', 'lastName' => $this->setFamilyName($value),
            'firstName', 'givenName' => $this->setGivenName($value),
            'honorificPrefix' => $this->name->honorificPrefix = $value,
            'honorificSuffix' => $this->name->honorificSuffix = $value,
            'identifier' => $this->setIdentifier($value),
            'phoneNumber' => $this->telephoneNumber = $value,
            'url' => $this->setUrl($value),
            default => throw new \ValueError('Unknown property: ' . $name),
        };
    }

    /**
     * Anonymizes PII (personally identifiable information).
     *
     * @param void
     * @return void
     */
    public function anonymize(): void
    {
        $this->birthDate = null;
        $this->birthPlace = null;

        $this->deathDate = null;
        $this->deathPlace = null;

        $this->emailAddress = null;
        $this->telephoneNumber = null;

        $this->nationality = null;

        $this->anonymizedFlag = true;
    }

    /**
     * Gets the person’s name.
     *
     * @param void
     * @return StoreCore\Google\People\Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Checks if the person is female.
     *
     * @param void
     * @return bool
     */
    public function isFemale(): bool
    {
        return ($this->gender === 2) ? true : false;
    }

    /**
     * Checks if the person is male.
     *
     * @param void
     * @return bool
     */
    public function isMale(): bool
    {
        return ($this->gender === 1) ? true : false;
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     *
     * @param void
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = [];
        $result['@context'] = 'https://schema.org';
        $result['@type'] = 'Person';

        if ($this->hasIdentifier()) $result['identifier'] = (string) $this->getIdentifier();

        if ($this->name !== null) {
            $result['name'] = $this->name->getDisplayName();
            if (isset($this->name->honorificPrefix)) $result['honorificPrefix'] = $this->name->honorificPrefix;
            if (isset($this->name->honorificSuffix)) $result['honorificSuffix'] = $this->name->honorificSuffix;
        }

        $properties = $this->toArray();
        unset($properties['dateCreated'], $properties['dateDeleted'], $properties['dateModified']);
        unset($properties['name']);

        // Replace a full `Country` object with the two-letter ISO country code.
        if (isset($properties['nationality'])) {
            $properties['nationality'] = $properties['nationality']->identifier;
        }

        // Replace `Language` value objects with BCP 47 language identifiers.
        if (isset($properties['knowsLanguage'])) {
            if (\is_object($properties['knowsLanguage'])) {
                $properties['knowsLanguage'] = (string) $properties['knowsLanguage'];
            }
        }

        return $result + $properties;
    }

    /**
     * Sets the date of birth.
     *
     * @param StoreCore\Types\Date|string $birth_date
     *   Date of birth as a `Date` object or a string in the ISO and MySQL
     *   'YYYY-MM-DD' string format.
     *
     * @return void
     */
    public function setBirthDate(Date|string $birth_date): void
    {
        if (is_string($birth_date)) $birth_date = new Date($birth_date);
        $this->birthDate = $birth_date;
    }

    /**
     * Sets the place where the person was born.
     *
     * @param StoreCore\Geo\Place|string $birth_place
     *   Place of birth as a `Place` object or the name of a `Place`.
     *
     * @return void
     */
    public function setBirthPlace(Place|string $birth_place): void
    {
        if (is_string($birth_place)) $birth_place = new Place($birth_place);
        $this->birthPlace = $birth_place;
    }

    /**
     * Sets the person's date of death.
     *
     * @param StoreCore\Types\Date|string $death_date
     *   Date of death as a `Date` value object or an ISO string.
     *
     * @return void
     */
    public function setDeathDate($death_date): void
    {
        if (is_string($death_date)) $death_date = new Date($death_date);
        $this->deathDate = $death_date;
    }

    /**
     * Sets the place where the person died.
     *
     * @param StoreCore\Geo\Place|string $death_place
     *   Death place as a `Place` or the name of a `Place`.
     *
     * @return void
     */
    public function setDeathPlace(Place|string $death_place): void
    {
        if (is_string($death_place)) $death_place = new Place($death_place);
        $this->DeathPlace = $death_place;
    }

    /**
     * Sets the family name.
     *
     * @param string $family_name
     *   Family name (last name).
     *
     * @return void
     */
    private function setFamilyName(string $family_name): void
    {
        if ($this->name === null) $this->name = new Name();
        $this->name->familyName = trim($family_name);
    }

    /**
     * Sets the gender.
     *
     * @param StoreCore\Types\GenderType|int|string $gender
     *   Gender type as an enumaration member, an integer, or a string.
     *
     * @return void
     *
     * @see https://www.iso.org/standard/36266.html
     *   ISO/IEC 5218:2004 Information technology — Codes for the representation of human sexes
     *
     * @see https://schema.org/gender
     *   Schema.org property `gender`
     *
     * @see https://schema.org/GenderType
     *   Schema.org enumeration type `GenderType`
     *
     * @throws \DomainException
     *   Throws a domain exception if the gender is a numeric value other than
     *   `0` (unkown), `1` (male), or `2` (female).  `9` (nine) for “Not
     *   applicable” is converted to `0`.
     *
     * @throws \OutOfRangeException
     *   Throws an out of range logic exeption on an unknown string.
     *   Because not all languages are supported, this MAY indicate that
     *   the keyword list in this method needs an update.
     */
    public function setGender(GenderType|int|string $gender): void
    {
        if (\is_numeric($gender)) $gender = (int) $gender;

        if ($gender instanceof GenderType) {
            if ($gender === GenderType::Male) {
                $gender = 1;
            } elseif ($gender === GenderType::Female) {
                $gender = 2;
            }
        }

        if (\is_int($gender)) {
            if ($gender === 9) $gender = 0;
            if ($gender < 0 || $gender > 2) {
                throw new \DomainException();
            } else {
                $this->gender = $gender;
                return;
            }
        }

        // Map lowercase strings to ISO 5218 integers.
        $gender = mb_strtolower($gender, 'UTF-8');
        $gender = trim($gender);
        $map = [
            'http://schema.org/female' => 2,
            'http://schema.org/male' => 1,
            'https://schema.org/female' => 2,
            'https://schema.org/male' => 1,

            'desconocido' => 0,
            'f' => 2,
            'female' => 2,
            'femenino' => 2,
            'feminin' => 2,
            'féminin' => 2,
            'inconnu' => 0,
            'm' => 1,
            'male' => 1,
            'man' => 1,
            'mannelijk' => 1,
            'mannlich' => 1,
            'männlich' => 1,
            'masculin' => 1,
            'masculino' => 1,
            'not known' => 0,
            'onbekend' => 0,
            'unbekannt' => 0,
            'unknown' => 0,
            'v' => 2,
            'vrouw' => 2,
            'vrouwelijk' => 2,
            'weiblich' => 2,
        ];
        if (array_key_exists($gender, $map)) {
            $this->gender = $map[$gender];
        } else {
            throw new \OutOfRangeException('Unknown gender: ' . $gender);
        }
    }

    /**
     * Sets the given name.
     *
     * @param string $given_name
     *   Given name (first name).
     *
     * @return void
     */
    private function setGivenName(string $given_name): void
    {
        if ($this->name === null) $this->name = new Name();
        $this->name->givenName = trim($given_name);
    }

    /**
     * Sets or unsets the person’s name.
     *
     * @param StoreCore\Google\People\Name|string|null $name
     *   Name of the person or `null` to unset the name.
     *
     * @return void
     */
    public function setName(Name|string|null $name): void
    {
        if (is_string($name)) {
            $name = trim($name);
            $name = empty($name) ? null : new Name($name);
        }

        if ($name === null) {
            $this->unsetName();
        } else {
            $this->name = $name;
        }
    }

    /**
     * Sets a URL of the person.
     *
     * @param StoreCore\Types\URL|string $url
     * @return void
     */
    public function setUrl(URL|string $url): void
    {
        if (is_string($url)) $url = new URL($url);
        $this->url = $url;
    }

    /**
     * Returns the set object properties as an array.
     *
     * @internal
     * @param void
     * @return array
     */
    public function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }

    /**
     * Unsets a person’s name(s).
     *
     * @param void
     * @return void
     */
    public function unsetName(): void
    {
        $this->name = null;
    }
}
