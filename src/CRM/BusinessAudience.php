<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

/**
 * Business audience.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/BusinessAudience Schema.org type `BusinessAudience`
 * @version 1.0.0
 */
class BusinessAudience extends Audience
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|QuantitativeValue $numberOfEmployees
     *   The number of employees in an organization e.g. business.
     */
    public ?QuantitativeValue $numberOfEmployees = null;

    /**
     * @var null|QuantitativeValue $yearlyRevenue
     *   The size of the business in annual revenue.
     */
    public ?QuantitativeValue $yearlyRevenue = null;

    /**
     * @var null|QuantitativeValue $yearsInOperation
     *  The age of the business.
     */
    public ?QuantitativeValue $yearsInOperation = null;
}
