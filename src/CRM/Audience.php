<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Geo\AdministrativeArea;
use StoreCore\Types\Intangible;

/**
 * Audience.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/Audience Schema.org type `Audience`
 * @version 1.0.0-alpha.1
 */
class Audience extends Intangible implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var string $audienceType
     *   The target group associated with a given audience (e.g. veterans,
     *   car owners, musicians, etc.).
     *
     * @todo
     *   The `audienceType` property is included for compatibility with
     *   Schema.org, but it is unclear why one should not simply use the
     *   `name` property to describe a target audience type, for example:
     *
     *   ```json
     *   {
     *     "@context": "https://schema.org",
     *     "@type": "SpecialAnnouncement",
     *     "name": "New Online Payments Protection Program for small business",
     *     "audience": {
     *       "@type": "BusinessAudience",
     *       "name": "Small businesses"
     *     }
     *   }
     *   ```
     */
    public ?string $audienceType = null;

    /**
     * @var null|StoreCore\Geo\AdministrativeArea $geographicArea
     *   The geographic area associated with the audience.
     */
    public ?AdministrativeArea $geographicArea = null;
}
