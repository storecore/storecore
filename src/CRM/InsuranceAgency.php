<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\Types\URL;

/**
 * Insurance agency.
 *
 * @package StoreCore\CRM
 * @version 1.0.0-rc.1
 * @see     https://schema.org/InsuranceAgency Schema.org type `InsuranceAgency`
 */
class InsuranceAgency extends FinancialService implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
