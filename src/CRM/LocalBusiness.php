<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\Geo\HasMapTrait;

/**
 * Local business.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/LocalBusiness Schema.org type `LocalBusiness`
 * @see     https://developers.google.com/search/docs/advanced/structured-data/local-business Local Business Structured Data, Google Search Central
 */
class LocalBusiness extends Organization implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject
{
    use HasMapTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string|array $currenciesAccepted
     *   The currency accepted.
     */
    public null|string|array $currenciesAccepted = null;

    /**
     * @var null|string|array $openingHours
     *   The general opening hours for a business.
     */
    public null|string|array $openingHours = null;

    /**
     * @var null|string $paymentAccepted
     *   Cash, Credit Card, Cryptocurrency, Local Exchange Tradings System, etc.
     */
    public ?string $paymentAccepted = null;

    /**
     * @var null|string $priceRange
     *   The price range of the business, for example `$$$`.
     */
    public ?string $priceRange = null;

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();

        if ($this->currenciesAccepted !== null) {
            $result['currenciesAccepted'] = $this->currenciesAccepted;
        }

        if ($this->hasMap !== null) {
            $result['hasMap'] = $this->hasMap;
        }

        if ($this->openingHours !== null) {
            $result['openingHours'] = $this->openingHours;
        }

        if (!empty($this->paymentAccepted)) {
            $result['paymentAccepted'] = $this->paymentAccepted;
        }

        if ($this->priceRange !== null) {
            $result['priceRange'] = $this->priceRange;
        }

        return $result;
    }
}
