<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use Psr\Container\ContainerInterface;
use StoreCore\RepositoryInterface;
use StoreCore\Database\DataMapperInterface;

use StoreCore\Registry;
use StoreCore\Database\AbstractModel;
use StoreCore\Database\{ContainerException, NotFoundException};
use StoreCore\Database\OrganizationMapper;
use StoreCore\Types\DateTime;
use StoreCore\Types\{UUID, UUIDFactory};

/**
 * Organization repository.
 *
 * @api
 * @package StoreCore\CRM
 * @see     https://github.com/php-fig/container/blob/master/src/ContainerInterface.php
 * @see     https://www.php.net/manual/en/class.countable.php
 * @version 1.0.0-alpha.1
 */
class OrganizationRepository extends AbstractModel implements
    ContainerInterface,
    \Countable,
    RepositoryInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\DataMapperInterface $dataMapper
     *   Data access object (DAO) for database CRUD operations on organizations.
     */
    private readonly DataMapperInterface $dataMapper;

    /**
     * Creates a repository for organizations.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->dataMapper = new OrganizationMapper($this->Registry);
    }

    /**
     * Clears previously deleted organizations.
     *
     * @param void
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     */
    public function clear(): bool
    {
        $result = $this->Database->exec(
            'DELETE FROM `sc_organizations` WHERE `date_deleted` IS NOT NULL AND `date_deleted` < DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 DAY)'
        );
        return $result !== false ? true : false;
    }

    /**
     * Counts the number of active organizations.
     *
     * @param void
     *
     * @return int
     *   Returns the total number of organizations that are not flagged
     *   for deletion and that were not dissolved.
     */
    public function count(): int
    {
        try {
            $result = $this->Database->query(
                'SELECT COUNT(*) FROM `sc_organizations` WHERE `date_deleted` IS NULL AND `dissolution_date` IS NULL'
            );
            return (int) $result->fetchColumn(0);
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    #[\Override]
    public function get(string $id): Organization
    {
        $result = $this->dataMapper->read($id);
        if ($result === false) {
            throw new ContainerException();
        } elseif ($result === null) {
            throw new NotFoundException();
        } else {
            return $result;
        }
    }

    #[\Override]
    public function has(string $id): bool
    {
        $id = trim($id);
        $id = str_ireplace('-', '', $id);
        $id = strtolower($id);

        try {
            if (ctype_digit($id) && !UUID::validate($id)) {
                $id = (int) $id;
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_organizations` WHERE `organization_id` = :organization_id'
                );
                $statement->bindValue(':organization_id', $id, \PDO::PARAM_INT);
            } else {
                $statement = $this->Database->prepare(
                    'SELECT COUNT(*) FROM `sc_organizations` WHERE `organization_uuid` = UNHEX(:organization_uuid)'
                );
                $statement->bindValue(':organization_uuid', $id, \PDO::PARAM_STR);
            }
            if ($statement->execute() !== false) {
                $count = $statement->fetchColumn(0);
                $statement->closeCursor();
                unset($statement);
                if ($count === 1) {
                    return true;
                }
            }
        } catch (\PDOException $e) {
            $this->Logger->error('Database exception ' . $e->getCode() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . ': ' . $e->getMessage());
        }
        return false;
    }


    /**
     * Persists an organization.
     *
     * @param Organization &$organization
     *   Organization to store in the repository.
     *
     * @return bool
     *   Returns `true` on success, otherwise `false`.
     *
     * @uses StoreCore\Database\OrganizationMapper::save()
     * 
     * @uses StoreCore\Types\UUIDFactory::pseudoRandomUUID()
     *   If the `Organization` has no UUID, a public pseudo-random UUID is added.
     */
    public function set(object &$organization): string
    {
        if (!$organization->hasIdentifier()) {
            $organization->setIdentifier(UUIDFactory::pseudoRandomUUID());
        }

        if (\is_int($organization->metadata->id)) {
            $result = $this->dataMapper->update($organization);
        } else {
            $result = $this->dataMapper->create($organization);
        }

        if ($result === true) {
            return $organization->getIdentifier()->__toString();
        } else {
            throw new ContainerException();
        }
    }

    /**
     * Removes an organization.
     *
     * @param string $organization
     *   Identifier of the `Organization` to remove.
     *
     * @return bool
     *   Returns `true` on success or `false` on failure.
     */
    public function unset(string $id): bool
    {
        if (UUID::validate($id)) {
            $id = new UUID($id);
            return $this->dataMapper->delete($id);
        } elseif (ctype_digit($id)) {
            $id = (int) $id;
            return $this->dataMapper->delete($id);
        } else {
            return false;
        }
    }
}
