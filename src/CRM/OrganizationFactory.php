<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Registry;
use StoreCore\{OnlineBusiness, OnlineStore};
use StoreCore\Database\AbstractModel;
use StoreCore\Database\OrganizationMapper;
use StoreCore\Types\UUIDFactory;

/**
 * Organization factory.
 *
 * Class hierarchy for organizations:
 * 
 * - class Thing
 * - class Organization extends Thing
 * - class OnlineBusiness extends Organization
 * - class OnlineStore extends OnlineBusiness
 *
 * @package StoreCore\CRM
 * @version 1.0.0-alpha.1
 */
class OrganizationFactory extends AbstractModel
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\OrganizationMapper $dataMapper
     *   Organization data mapper for CRUD database operations.
     */
    private readonly OrganizationMapper $dataMapper;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\OrganizationProxy $parentOrganization
     *   The larger `Organization` that a newly created `Organization`
     *   is a `subOrganization` of, if any.
     */
    public null|Organization|OrganizationProxy $parentOrganization = null;

    /**
     * Creates an organization factory.
     *
     * @param StoreCore\Registry $registry
     *   Global StoreCore service locator.
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->dataMapper = new OrganizationMapper($this->Registry);
    }

    /**
     * Creates a new online business.
     *
     * @param null|string $name
     *   OPTIONAL name of the online business.
     *
     * @return StoreCore\OnlineBusiness
     *   Returns an new `OnlineBusiness` object with a new UUID.
     */
    public function createOnlineBusiness(?string $name = null): OnlineBusiness
    {
        return $this->createOrganization($name, OrganizationType::OnlineBusiness);
    }

    /**
     * Creates a new online store.
     *
     * @param null|string $name
     *   OPTIONAL name of the online store.
     *
     * @return StoreCore\OnlineStore
     *   Returns a new `OnlineStore` object with a new UUID.
     */
    public function createOnlineStore(?string $name = null): OnlineStore
    {
        return $this->createOrganization($name, OrganizationType::OnlineStore);
    }

    /**
     * Creates a new organization.
     *
     * @param string|null $name
     *   OPTIONAL name of the organization.
     *
     * @param StoreCore\CRM\OrganizationType $type
     *   OPTIONAL type of organization.  Defaults to generic `Organization`.
     *
     * @return StoreCore\CRM\Organization
     *   Returns a new `Organization` entity object with a new unique UUID.
     */
    final public function createOrganization(
        ?string $name = null,
        OrganizationType $type = OrganizationType::Organization,
    ): Organization {
        $result = match ($type) {
            OrganizationType::OnlineBusiness => new OnlineBusiness(),
            OrganizationType::OnlineStore => new OnlineStore(),
            default => new Organization(),
        };

        if ($result->type->name !== $type->name) {
            $result->type = $type;
        }

        if (!empty($name)) {
            $result->name = $name;
        }

        if ($this->parentOrganization !== null) {
            $result->parentOrganization = $this->parentOrganization;
        }

        $this->dataMapper->create($result);
        return $result;
    }
}
