<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use Psr\Container\ContainerInterface;
use StoreCore\IdentityInterface;
use StoreCore\Registry;
use StoreCore\Database\AbstractModel;
use StoreCore\Database\PersonRepository;

/**
 * Parties repository.
 *
 * A _party_ is an `Organization` or a `Person` that has some role in an
 * `Order` or an `Action`.  StoreCore does not have a separate superclass
 * for a party, but handles the distinction through this repository.
 *
 * @package StoreCore\CRM
 * @version 1.0.0
 */
class Parties extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    private OrganizationRepository $organizations;
    private PersonRepository $persons;

    #[Override]
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->organizations = new OrganizationRepository($this->Registry);
        $this->persons = new PersonRepository($this->Registry);
    }

    #[Override]
    public function get(string $id): IdentityInterface
    {
        if ($this->organizations->has($id)) {
            return $this->organizations->get($id);
        } else {
            return $this->persons->get($id);
        }
    }

    #[Override]
    public function has(string $id): bool
    {
        if ($this->organizations->has($id)) {
            return true;
        } else {
            return $this->persons->has($id);
        }
    }
}
