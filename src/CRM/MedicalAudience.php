<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\Types\MedicalCondition;
use StoreCore\Types\URL;

/**
 * Medical audience.
 *
 * @package StoreCore\CRM
 * @see     https://schema.org/MedicalAudience Schema.org type `MedicalAudience`
 * @version 1.0.0-alpha.1
 */
class MedicalAudience extends PeopleAudience implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Types\MedicalCondition $healthCondition
     *   Specifying the health condition(s) of a patient, medical study,
     *   or other target audience.
     */
    public ?MedicalCondition $healthCondition = null;

    /**
     * {@inheritDoc}
     *
     * Schema.org distinguishes two “types” of medical audiences, but
     * `MedicalAudience` inherits all properties from `PeopleAudience`,
     * so we set `PeopleAudience` as an `additionalType`.
     * 
     * - Thing > Intangible > Audience > MedicalAudience
     * - Thing > Intangible > Audience > PeopleAudience > MedicalAudience
     */
    public function __construct(string|array|null $param = null)
    {
        $this->additionalType = new URL('https://schema.org/PeopleAudience');
        parent::__construct($param);
    }
}
