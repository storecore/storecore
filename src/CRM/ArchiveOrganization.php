<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\CMS\ArchiveComponent;

/**
 * Archive organization.
 * 
 * An `ArchiveOrganization` is an `Organization` with archival holdings:
 * an `Organization` which keeps and preserves archival material and typically
 * makes it accessible to the public.
 *
 * @package StoreCore\Core
 * @version 1.0.0-rc.1
 * @see     https://schema.org/ArchiveOrganization Schema.org type `ArchiveOrganization`
 */
class ArchiveOrganization extends LocalBusiness implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var null|StoreCore\CMS\ArchiveComponent $archiveHeld
     *   Collection, fonds, or item held, kept or maintained by an
     *   `ArchiveOrganization`.  Inverse property: `holdingArchive`
     *   of an `ArchiveComponent`.
     */
    public ?ArchiveComponent $archiveHeld = null;
}
