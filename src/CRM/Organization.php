<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use Psr\Http\Message\UriInterface;
use StoreCore\IdentityInterface;

use StoreCore\IdentityTrait;
use StoreCore\SubjectTrait;

use StoreCore\CMS\ImageObject;
use StoreCore\CMS\TextObject;
use StoreCore\Database\Metadata;
use StoreCore\OML\PostalAddress;
use StoreCore\PIM\Brand;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\EmailAddress;
use StoreCore\Types\URL;
use StoreCore\Types\UUID;

/**
 * Organization.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/Organization Schema.org type `Organization`
 */
class Organization implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject,
    \Stringable
{
    use IdentityTrait;
    use SubjectTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string|URL $additionalType
     *   An additional type for the `Organization`, typically used for adding
     *   more specific types from external vocabularies in microdata syntax.
     */
    public null|string|URL $additionalType = null;

    /**
     * @var null|StoreCore\OML\PostalAddress $address
     *   Postal address of the organization.
     */
    public ?PostalAddress $address = null;

    /**
     * @var mull|string $alternateName
     *   An OPTIONAL alias for the `Organization`.
     */
    public ?string $alternateName = null;

    /**
     * @var StoreCore\PIM\Brand[]|null $brand
     *   The brand or brands owned or maintained by the `Organization`.
     */
    protected ?array $brand = null;

    /**
     * @var null|string|StoreCore\CMS\TextObject $description
     *   An OPTIONAL description of the `Organization`.
     */
    public null|string|TextObject $description = null;

    /**
     * @var null|StoreCore\Types\Date $dissolutionDate
     *   OPTIONAL.  The date that this organization was dissolved as a date
     *   value in ISO 8601 date format.
     */
    protected ?Date $dissolutionDate = null;

    /**
     * @var null|string $duns
     *   OPTIONAL.  The Dun & Bradstreet DUNS number for identifying an
     *   organization.
     */
    public ?string $duns = null;

    /**
     * @var null|StoreCore\Types\EmailAddress $email
     *   OPTIONAL email address of the `Organization`.
     */
    protected ?EmailAddress $email = null;

    /**
     * @var null|string $faxNumber
     *   OPTIONAL.  The fax number of the organization.
     */
    public ?string $faxNumber = null;

    /**
     * @var StoreCore\Types\Date|null $foundingDate
     *   OPTIONAL.  The date that this organization was founded as a date value
     *   in ISO 8601 date format.
     */
    public ?Date $foundingDate = null;

    /**
     * @var null|StoreCore\CMS\ImageObject|StoreCore\Types\URL|array $image
     *   One or more images of the `Organization`.  This can be a `URL` or
     *   a fully described `ImageObject`.
     */
    protected null|ImageObject|URL|array $image = null;

    /**
     * @var null|string $isicV4
     *   The International Standard of Industrial Classification of All Economic
     *   Activities (ISIC), Revision 4 code for a particular `Organization`,
     *   business `Person`, or `Place`.
     */
    public ?string $isicV4 = null;

    /**
     * @var null|string|array $iso6523Code
     *   An `Organization` identifier as defined in ISO 6523(-1).  Note that
     *   many existing `Organization` identifiers such as `leiCode`, `duns` and
     *   `vatID` can be expressed as an ISO 6523 identifier by setting the ICD
     *   part of the ISO 6523 identifier accordingly.
     */
    public null|string|array $iso6523Code = null;

    /**
     * @var null|string $legalName
     *   The official name of the organization, e.g. the registered company name.
     */
    public ?string $legalName = null;

    /**
     * @var null|string $leiCode
     *   OPTIONAL Legal Entity Identifier (LEI).  An organization identifier
     *   that uniquely identifies a legal entity as defined in ISO 17442.
     * 
     * @see https://en.wikipedia.org/wiki/Legal_Entity_Identifier
     *   Legal Entity Identifier
     */
    public ?string $leiCode = null;

    /**
     * @var StoreCore\CMS\ImageObject|StoreCore\Types\URL|null $logo
     *   An associated logo.
     */
    protected null|ImageObject|URL $logo = null;

    /**
     * @var StoreCore\Database\Metadata $metadata
     *   Internal technical metadata for object persistance.
     */
    public Metadata $metadata;

    /**
     * @var null|string $naics
     *   The North American Industry Classification System (NAICS) code for
     *   a particular `Organization` or business person.
     */
    public ?string $naics = null;

    /**
     * @var null|string $name
     *   The name of the `Organization`.
     */
    public ?string $name = null;

    /**
     * @var StoreCore\Types\URL|null $ownershipFundingInfo
     *   For an `Organization` (often but not necessarily a `NewsMediaOrganization`),
     *   a description of organizational ownership structure; funding and grants.
     *   In a news/media setting, this is with particular reference to editorial
     *   independence.  Note that the `funder` is also available and can be used
     *   to make basic funder information machine-readable.
     */
    protected ?URL $ownershipFundingInfo = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\OrganizationProxy $parentOrganization
     *   The larger `Organization` that this `Organization` is
     *   a `subOrganization` of, if any.  Supersedes `branchOf`.
     *   Inverse Schema.org property is `subOrganization`.
     */
    public null|Organization|OrganizationProxy $parentOrganization = null;

    /**
     * @var null|array $sameAs
     *   One or more URLs of reference Web pages that unambiguously indicate
     *   the item’s identity.  E.g. the URL of the item's Wikipedia page,
     *   Wikidata entry, or official website.
     */
    protected ?array $sameAs = null;

    /**
     * @var null|StoreCore\Types\URL $url
     *   URL of the `Organization`.
     */
    protected ?URL $url = null;

    /**
     * @var null|string $taxID
     *   The Tax or Fiscal identifier of the organization.
     *   Use the `Organization.vatID` property for Value-Added Tax (VAT).
     */
    public ?string $taxID = null;

    /**
     * @var null|string $telephone
     *   The telephone number of the organization.
     */
    public ?string $telephone = null;

    /**
     * @var StoreCore\CRM\OrganizationType $type
     *   Schema.org `@type` of `Organization`.
     */
    public OrganizationType $type = OrganizationType::Organization;

    /**
     * @var null|string $vatID
     *   The Value-Added Tax (VAT) identifier of the `Organization`.  Use the
     *   generic `Organization.taxID` for other types of tax identifiers.
     *
     * @see https://en.wikipedia.org/wiki/VAT_identification_number
     *   VAT identification number
     */
    public ?string $vatID = null;

    /**
     * Creates an organization.
     * 
     * @param null|string|array $param = null
     *   Name of the organization as a string or organization properties as
     *   a key-value array.
     */
    public function __construct(null|string|array $param = null)
    {
        if (\is_string($param)) {
            $this->name = $param;
        } elseif (\is_array($param) && !empty($param)) {
            foreach ($param as $key => $value) {
                $this->__set($key, $value);
            }
        }

        if (!isset($this->metadata)) {
            $this->metadata = new Metadata();
        }
    }

    /**
     * Reads data from inaccessible (protected or private) or non-existing properties.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        // snake_case to camelCase
        $name = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $name))));

        return match ($name) {
            'dateCreated' => $this->metadata->dateCreated,
            'dateModified' => $this->metadata->dateModified,
            'brand' => $this->getBrand(),
            'dissolutionDate' => $this->dissolutionDate,
            'email', 'emailAddress' => $this->email,
            'id', 'identifier' => $this->getIdentifier(),
            'image' => $this->image,
            'logo' => $this->logo,
            'url' => $this->url,
            'ownershipFundingInfo' => $this->ownershipFundingInfo,
            'sameAs' => $this->getSameAs(),
            default =>  throw new \LogicException(
                'Property ' . get_class($this) . '::$' . $name . ' does not exist.'
            )
        };
    }

    /**
     * Writes data to inaccessible (protected or private) properties.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws \ValueError
     */
    public function __set(string $name, mixed $value): void
    {
        // snake_case to camelCase
        $name = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $name))));

        match ($name) {
            'additionalType' => $this->additionalType = $value,
            'alternateName' => $this->alternateName = $value,
            'brand' => $this->setBrand($value),
            'commonName', 'name' => $this->name = $value,
            'dateCreated' => $this->metadata->dateCreated = $value,
            'dateModified' => $this->metadata->dateModified = $value,
            'dissolutionDate' => $this->setDissolutionDate($value),
            'dunsNumber' => $this->duns = $value,
            'email', 'emailAddress' => $this->setEmail($value),
            'faxNumber' => $this->faxNumber = $value,
            'foundingDate' => $this->setFoundingDate($value),
            'id', 'identifier' => $this->setIdentifier($value),
            'image' => $this->setImage($value),
            'legalName' => $this->legalName = $value,
            'leiCode' => $this->leiCode = $value,
            'logo' => $this->setLogo($value),
            'ownershipFundingInfo' => $this->setOwnershipFundingInfo($value),
            'parentOrganization' => $this->parentOrganization = $value,
            'sameAs' => $this->setSameAs($value),
            'taxID', 'taxId' => $this->taxID = $value,
            'telephone', 'telephoneNumber' => $this->telephone = $value,
            'type' => $this->type = $value,
            'url', 'websiteUrl' => $this->setUrl($value),
            'vatID', 'vatId' => $this->vatID = $value,
            default => throw new \LogicException(
                'Property ' . get_class($this) . '::$' . $name . ' does not exist.'
            )
        };
    }

    /**
     * Converts the `Organization` object to a JSON-LD string.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $result = json_encode($this, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE | \JSON_THROW_ON_ERROR);

        $result = str_replace('"@context":"http://schema.org', '"@context":"https://schema.org', $result);
        $result = str_replace('"@context":"https://schema.org/"', '"@context":"https://schema.org"', $result);

        $result = explode('"@context":"https://schema.org"', $result, 2);
        $result[1] = str_replace('"@context":"https://schema.org",', '', $result[1]);
        $result = $result[0] . '"@context":"https://schema.org"' . $result[1];

        return $result;
    }

    /**
     * Gets the organization brand(s).
     *
     * @internal
     * @param void
     * @return StoreCore\PIM\Brand|StoreCore\PIM\Brand[]|null
     */
    private function getBrand(): Brand|array|null
    {
        if (empty($this->brand)) {
            return null;
        } elseif (count($this->brand) === 1) {
            return $this->brand[0];
        } else {
            return $this->brand;
        }
    }

    /**
     * Gets URLs that unambiguously identify the organization.
     *
     * @param void
     * @return null|URL|URL[]
     */
    private function getSameAs(): null|URL|array
    {
        if (empty($this->sameAs)) {
            return null;
        }
        
        if (count($this->sameAs) === 1) {
            return $this->sameAs[0];
        } else {
            return $this->sameAs;
        }
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $json = [];

        if ($this->hasIdentifier()) {
            $json['@id'] = '/api/v1/organizations/' . $this->getIdentifier();
        }
        $json['@context'] = 'https://schema.org';
        $json['@type'] = $this->type->name;

        // Non-empty properties in alphabetical order
        if ($this->additionalType !== null) $json['additionalType'] = $this->additionalType;
        if ($this->address !== null) $json['address'] = $this->address;
        if ($this->getBrand() !== null) $json['brand'] = $this->getBrand();
        if ($this->hasIdentifier()) $json['identifier'] = $this->getIdentifier()->__toString();
        if ($this->dissolutionDate !== null) $json['dissolutionDate'] = $this->dissolutionDate->format('Y-m-d');
        if ($this->duns !== null) $json['duns'] = $this->duns;
        if ($this->email !== null) $json['email'] = $this->email->__toString();
        if ($this->faxNumber !== null) $json['faxNumber'] = $this->faxNumber;
        if ($this->foundingDate !== null) $json['foundingDate'] = $this->foundingDate->format('Y-m-d');
        if ($this->image !== null) $json['image'] = $this->image;
        if ($this->isicV4 !== null) $json['isicV4'] = $this->isicV4;
        if ($this->iso6523Code !== null) $json['iso6523Code'] = $this->iso6523Code;
        if ($this->legalName !== null) $json['legalName'] = $this->legalName;
        if ($this->leiCode !== null) $json['leiCode'] = $this->leiCode;
        if ($this->logo !== null) $json['logo'] = $this->logo;
        if ($this->naics !== null) $json['naics'] = $this->naics;
        if ($this->name !== null) $json['name'] = $this->name;
        if ($this->ownershipFundingInfo !== null) $json['ownershipFundingInfo'] = $this->ownershipFundingInfo;
        if ($this->parentOrganization !== null) $json['parentOrganization'] = $this->parentOrganization;
        if ($this->getSameAs() !== null) $json['sameAs'] = $this->getSameAs();
        if ($this->taxID !== null) $json['taxID'] = $this->taxID;
        if ($this->telephone !== null) $json['telephone'] = $this->telephone;
        if ($this->url !== null) $json['url'] = $this->url->__toString();
        if ($this->vatID !== null) $json['vatID'] = $this->vatID;

        return $json;
    }

    /**
     * Sets a brand of the organization.
     *
     * @param StoreCore\PIM\Brand|StoreCore\PIM\Brand[] $brand
     *   `Brand` or brands maintained by the `Organization`.
     *
     * @return void
     */
    private function setBrand(Brand|array $brand): void
    {
        if ($this->brand === null) {
            $this->brand = array();
        }

        if ($brand instanceof Brand) {
            $this->brand[] = $brand;
        } else {
            foreach ($brand as $value) {
                $this->setBrand($value);
            }
        }
    }

    /**
     * Sets an image.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\CMS\ImageObject|StoreCore\Types\URL|string $image
     *   Image as an `ImageObject` or some type of `URL`.
     *
     * @return void
     */
    private function setImage(UriInterface|ImageObject|URL|array|string $image): void
    {
        if ($image instanceof UriInterface) {
            $image = (string) $image;
        }
        if (\is_string($image)) {
            $image = new URL($image);
        }
        $this->image = $image;
    }

    /**
     * Sets the organization logo.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\CMS\ImageObject|StoreCore\Types\URL|string
     * @return void
     */
    private function setLogo(UriInterface|ImageObject|URL|string $logo): void
    {
        if ($logo instanceof UriInterface) {
            $logo = (string) $logo;
        }
        if (\is_string($logo)) {
            $logo = new URL($logo);
        }
        $this->logo = $logo;
    }

    /**
     * Sets the date the organization was dissolved.
     *
     * @param \DateTimeInterface|string $dissolution_date
     * @return void
     */
    private function setDissolutionDate(\DateTimeInterface|string $dissolution_date): void
    {
        if (\is_string($dissolution_date)) {
            $dissolution_date = new Date($dissolution_date, new \DateTimeZone('UTC'));
        } elseif (!$dissolution_date instanceof Date) {
            $dissolution_date = Date::createFromInterface($dissolution_date);
        }
        $this->dissolutionDate = $dissolution_date;
    }

    /**
     * Sets the e-mail address.
     *
     * @param StoreCore\Types\EmailAddress|string $email_address
     * @return void
     * @uses StoreCore\Types\EmailAddress::__construct()
     */
    private function setEmail(EmailAddress|string $email_address): void
    {
        if (\is_string($email_address)) {
            $email_address = new EmailAddress($email_address);
        }
        $this->email = $email_address;
    }

    /**
     * Sets the date the organization was founded.
     *
     * @param \DateTimeInterface|string $founding_date
     * @return void
     */
    private function setFoundingDate(\DateTimeInterface|string $founding_date): void
    {
        if (\is_string($founding_date)) {
            $founding_date = new Date($founding_date, new \DateTimeZone('UTC'));
        } elseif (!$founding_date instanceof Date) {
            $founding_date = Date::createFromInterface($founding_date);
        }
        $this->foundingDate = $founding_date;
    }

    /**
     * Sets the ownership and funding information.
     *
     * @param StoreCore\Types\URL|string $url
     * @return void
     */
    private function setOwnershipFundingInfo(URL|string $url): void
    {
        if (\is_string($url)) {
            $url = new URL($url);
        }
        $this->ownershipFundingInfo = $url;
    }

    /**
     * Sets one or more URLs that uniquely identify the organization.
     * 
     * @param string|URL|URL[] $same_as
     * @return void
     */
    private function setSameAs(string|URL|array $same_as): void
    {
        if (\is_string($same_as)) {
            $same_as = new URL($same_as);
        }

        if ($same_as instanceof URL) {
            $this->sameAs[0] = $same_as;
        } else {
            $this->sameAs = $same_as;
        }
    }

    /**
     * Sets the organization’s URL.
     *
     * @var Psr\Http\Message\UriInterface|StoreCore\Types\URL|string $url
     * @return void
     */
    private function setUrl(UriInterface|URL|string $url): void
    {
        if (!($url instanceof URL)) {
            $url = new URL($url);
        }
        $this->url = $url;
    }
}
