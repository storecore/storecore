<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

use StoreCore\IdentityInterface;
use StoreCore\CMS\Menu;
use StoreCore\Types\Rating;
use StoreCore\Types\URL;

/**
 * Food establishment.
 *
 * @package StoreCore\Core
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/FoodEstablishment Schema.org type `FoodEstablishment`
 * @see     https://developers.google.com/search/docs/advanced/structured-data/local-business Local Business Structured Data, Google Search Central
 */
class FoodEstablishment extends LocalBusiness implements
    IdentityInterface,
    \JsonSerializable,
    \SplSubject,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|bool|URL|string $acceptsReservations
     *   Indicates whether a `FoodEstablishment` accepts reservations.
     *   Values can be boolean, an `URL` at which reservations can be made
     *   or (for backwards compatibility) the strings `Yes` or `No`.
     */
    public null|bool|URL|string $acceptsReservations = null;

    /**
     * @var null|StoreCore\CMS\Menu|StoreCore\Types\URL|string $hasMenu
     *   Either the actual menu as a structured representation, as text,
     *   or a `URL` of the menu.  Supersedes `menu`.
     */
    public null|Menu|URL|string $hasMenu = null;

    /**
     * @var null|string $servesCuisine
     *   The cuisine of the restaurant.
     */
    public ?string $servesCuisine = null;

    /**
     * @var null|StoreCore\Types\Rating $starRating
     *   An official `Rating` for a lodging business or food establishment,
     *   e.g. from national associations or standards bodies.  Use the `author`
     *   property to indicate the rating organization, e.g. as an `Organization`
     *   with name such as (e.g. HOTREC, DEHOGA, WHR, or Hotelstars).
     */
    public ?Rating $starRating = null;

    #[\Override]
    public function __get(string $name): mixed
    {
        return match ($name) {
            'hasMenu', 'menu' => $this->hasMenu,
            default => parent::__get($name),
        };
    }

    #[\Override]
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'hasMenu', 'menu' => $this->hasMenu = $value,
            default => parent::__set($name, $value),
        };
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();

        if ($this->hasMenu !== null) {
            $result['hasMenu'] = $this->hasMenu;
        }

        if ($this->servesCuisine !== null) {
            $result['servesCuisine'] = $this->servesCuisine;
        }

        return $result;
    }
}
