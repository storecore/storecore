<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CRM;

/**
 * Researcher.
 *
 * @api
 * @package StoreCore\CRM
 * @see     https://schema.org/Researcher Schema.org type `Researcher`
 * @version 1.0.0
 */
class Researcher extends Audience implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
