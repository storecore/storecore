<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2015–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\Container\ContainerInterface;

/**
 * Global registry singleton.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://en.wikipedia.org/wiki/Singleton_pattern Singleton pattern
 * @version 1.0.0
 */
final class Registry extends AbstractContainer implements SingletonInterface, ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer)
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\Registry $instance
     *   Single instance of the global registry or `null` if there is no instance yet.
     */
    private static ?Registry $instance = null;

    // Disable object instantiation and cloning
    private function __construct() {}
    private function __clone() {}

    /**
     * Gets the single instance of the registry.
     *
     * @param void
     * @return self
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new Registry();
        }
        return self::$instance;
    }

    /**
     * Sets a shared value in the global registry.
     *
     * @param \Stringable|string $id
     *   Unique identifier of data to store in the registry.
     * 
     * @param mixed $value
     *   Global data to store in the registry.
     * 
     * @return void
     *
     * @throws Psr\Container\ContainerExceptionInterface
     *   Throws a PSR-11 `ContainerExceptionInterface` exception when the
     *   `$value` being stored is empty.
     */
    public function set(\Stringable|string $id, mixed $value): void
    {
        if (empty($value)) {
            throw new ContainerException();
        }
        $this->data[(string) $id] = $value;
    }
}
