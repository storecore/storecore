<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2020, 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use Psr\Log\{LoggerInterface, NullLogger};
use StoreCore\Registry;
use StoreCore\FileSystem\Logger;

use function \defined;

/**
 * Logger factory.
 *
 * @package StoreCore\Core
 * @version 1.0.0
 */
class LoggerFactory
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a logger.
     *
     * @param void
     *
     * @return Psr\Log\LoggerInterface
     *   Returns a logger that implements the PSR-3 Logger Interface.
     */
    public function createLogger(): LoggerInterface
    {
        if (defined('STORECORE_NULL_LOGGER') && STORECORE_NULL_LOGGER === true) {
            return new NullLogger();
        }

        $registry = Registry::getInstance();
        if (
            $registry->has('Logger')
            && $registry->get('Logger') instanceof LoggerInterface
        ) {
            return $registry->get('Logger');
        }

        return new Logger();
    }
}
