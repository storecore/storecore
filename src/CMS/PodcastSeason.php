<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Podcast season.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/PodcastSeason Schema.org type `PodcastSeason`
 * @version 0.1.0
 */
class PodcastSeason extends CreativeWorkSeason implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
