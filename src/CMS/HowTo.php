<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Duration;

/**
 * How-to.
 * 
 * Instructions that explain how to achieve a result by performing a sequence
 * of steps.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/HowTo Schema.org type `HowTo`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/how-to
 * @version 0.2.0
 * 
 */
class HowTo extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\Types\Duration|null $prepTime
     *   The length of time it takes to prepare the items to be used in
     *   instructions or a direction, in ISO 8601 duration format.
     */
    public ?Duration $prepTime = null;

    /**
     * @var StoreCore\Types\Duration|null $totalTime
     *   The total time required to perform instructions or a direction
     *   (including time to prepare the supplies), in ISO 8601 duration format.
     */
    public ?Duration $totalTime = null;
}
