<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Web page element.
 *
 * A web page element, like a table or an image.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WebPageElement Schema.org type `WebPageElement`
 * @version 0.1.0
 */
class WebPageElement extends AbstractCreativeWork implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
