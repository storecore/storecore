<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\{Organization, Person};

/**
 * Claim.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-rc.1
 * @see     https://schema.org/Claim Schema.org type `Claim`
 */
class Claim extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var null|AbstractCreativeWork $appearance
     *   Indicates an occurrence of a `Claim` in some `CreativeWork`.
     */
    public ?AbstractCreativeWork $appearance = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person $claimInterpreter
     *   For a `Claim` interpreted from `MediaObject` content, the
     *   `interpretedAsClaim` property can be used to indicate a `Claim`
     *   contained, implied or refined from the content of a `MediaObject`.
     */
    public null|Organization|Person $claimInterpreter = null;

    /**
     * @var null|AbstractCreativeWork $firstAppearance
     *   Indicates the first known occurrence of a `Claim` in some `CreativeWork`.
     */
    public ?AbstractCreativeWork $firstAppearance = null;
}
