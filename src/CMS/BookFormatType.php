<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Book format type.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/BookFormatType Schema.org enumeration type `BookFormatType`
 * @version 27.0.0
 */
enum BookFormatType: string
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  Matches Schema.org v27.0 from May 20, 2024.
     */
    public const string VERSION = '27.0.0';

    case AudiobookFormat = 'https://schema.org/AudiobookFormat';
    case EBook           = 'https://schema.org/EBook';
    case GraphicNovel    = 'https://schema.org/GraphicNovel';
    case Hardcover       = 'https://schema.org/Hardcover';
    case Paperback       = 'https://schema.org/Paperback';
}
