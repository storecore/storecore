<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Date;

/**
 * CreativeWorkSeries.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/CreativeWorkSeries Schema.org type `CreativeWorkSeries`
 * @version 0.1.0
 */
class CreativeWorkSeries extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|StoreCore\Types\Date $endDate
     *   The end date of the item.
     */
    protected ?Date $endDate = null;

    /**
     * @var null|StoreCore\Types\Date $startDate
     *   The start date of the item.
     */
    protected ?Date $startDate = null;

    #[\Override]
    public function __get(string $name): mixed
    {
        return match ($name) {
            'endDate' => $this->endDate,
            'startDate' => $this->startDate,
            default => parent::__get($name),
        };
    }

    #[\Override]
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'endDate' => $this->endDate = self::createDate($value),
            'startDate' => $this->startDate = self::createDate($value),
            default => parent::__set($name, $value),
        };
    }
}
