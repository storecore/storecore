<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Media review item.
 * 
 * A `MediaReviewItem` represents an item or group of closely related items
 * treated as a unit for the sake of evaluation in a `MediaReview`.  Authorship
 * etc. apply to the items rather than to the curation/grouping or reviewing party.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/MediaReviewItem Schema.org type `MediaReviewItem`
 */
class MediaReviewItem extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\CMS\MediaObject|array $mediaItemAppearance
     *   In the context of a `MediaReview`, indicates specific media item(s)
     *   that are grouped using a `MediaReviewItem`.
     */
    public null|MediaObject|array $mediaItemAppearance = null;
}
