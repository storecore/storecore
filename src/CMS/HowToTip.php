<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * How-to tip.
 *
 * A `HowToTip` is an explanation in the instructions for how to achieve a
 * result.  It provides supplementary information about a technique, supply,
 * author’s preference, etc.  It can explain what could be done, or what should
 * not be done, but doesn’t specify what should be done (see `HowToDirection`).
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/HowToTip Schema.org type `HowToTip`
 */
class HowToTip extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
