<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Live blog posting.
 * 
 * A `LiveBlogPosting` is a `BlogPosting` intended to provide a rolling
 * textual coverage of an ongoing `Event` through continuous updates.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/LiveBlogPosting Schema.org type `LiveBlogPosting`
 * @version 1.0.0-rc.1
 */
class LiveBlogPosting extends BlogPosting implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var null|DateTime $coverageEndTime
     *   The time when the live blog will stop covering the `Event`.
     *   Note that coverage MAY continue after the `Event` concludes.
     */
    public ?DateTime $coverageEndTime = null;

    /**
     * @var null|DateTime $coverageStartTime
     *   The time when the live blog will begin covering the `Event`. Note
     *   that coverage MAY begin before the event’s start time.  The
     *   `LiveBlogPosting` MAY also be created before coverage begins.
     */
    public ?DateTime $coverageStartTime = null;

    /**
     * @var null|StoreCore\CMS\BlogPosting $liveBlogUpdate
     *   An update to the LiveBlog.
     */
    public ?BlogPosting $liveBlogUpdate = null;
}
