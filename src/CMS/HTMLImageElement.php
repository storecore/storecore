<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2017–2019, 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \DomainException;
use \LogicException;
use \Stringable;
use \ValueError;

/**
 * HTML image.
 *
 * @package StoreCore\CMS
 * @version 0.5.0
 *
 * @see https://html.spec.whatwg.org/#the-img-element
 *      The img element - HTML Standard
 *
 * @see https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Images_in_HTML
 *      Images in HTML - Multimedia and Embedding - Structuring the web with HTML - Learn web development
 *
 * @see https://developers.google.com/web/fundamentals/design-and-ux/responsive/images
 *      Images - Web Fundamentals - Google Developers
 */
class HTMLImageElement extends AbstractHTMLElement implements Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.5.0';

    /**
     * @var string $alt
     *   Alternative text for the `alt` attribute of an `<img>` tag.
     *   This attribute MUST be a string but MAY be empty.
     */
    private string $alt = '';

    /**
     * @var string $decoding
     *   Image decoding hint to the browser.  Set to `async` by default
     *   to reduce delay in presenting other content.
     */
    private string $decoding = 'async';

    /**
     * @var null|int $height
     *   Image height in pixels.
     */
    private ?int $height = null;

    /**
     * @var null|string $loading
     *   Indicates how the browser should load the image: `eager` loads the
     *   image immediately and `lazy` defers loading the image until it
     *   reaches a calculated distance from the viewport, as defined by the
     *   browser.  This OPTIONAL attribute MAY be set to `null`;
     */
    private ?string $loading = 'lazy';

    /**
     * @var null|StoreCore\CMS\ImageObject $microdata
     *   OPTIONAL metadata that describes the image or image content.
     */
    public ?ImageObject $microdata = null;

    /**
     * @var string $src
     *   Image file URL for the `src` attribute of the `<img>`.  Defaults to
     *   a data URI for a 1×1 transparent pixel.
     */
    protected string $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';

    /**
     * @inheritDoc
     */
    protected string $tagName = 'IMG';

    /**
     * @var null|int $width
     *   Image width in pixels.
     */
    private ?int $width = null;

    /**
     * Creates an HTML image.
     *
     * @param null|int $width
     * @param null|int $height
     */
    public function __construct(?int $width = null, ?int $height = null)
    {
        if ($width !== null) {
            $this->setWidth($width);
            if ($height !== null) {
                $this->setHeight($height);
            }
        }
    }

    public function __get($name): mixed
    {
        return match ($name) {
            'alt' => $this->getAlt(),
            'decoding' => $this->getDecoding(),
            'height' => $this->getHeight(),
            'loading' => $this->getLoading(),
            'src' => $this->getSource(),
            'width' => $this->getWidth(),
            default => parent::__get($name),
        };
    }

    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'alt' => $this->setAlt($value),
            'height' => $this->setHeight($value),
            'src' => $this->setSource($value),
            'width' => $this->setWidth($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Converts the image object to an image HTML tag.
     *
     * @param void
     *
     * @return string
     *   Returns the image as a string with an HTML5 `<img>` tag.
     */
    public function __toString(): string
    {
        // If the `alt` text is not set, try to fetch
        // a text alternative from metadata.
        if (empty($this->getAlt()) && $this->microdata !== null) {
            if ($this->microdata->embeddedTextCaption !== null) {
                $this->setAlt($this->microdata->embeddedTextCaption);
            } elseif ($this->microdata->caption !== null && is_string($this->microdata->caption)) {
                $this->setAlt($this->microdata->caption);
            }
        }

        $result = '<img';

        if (!empty($this->class)) {
            $result = ' class="' . $this->class . '"';
        }

        if (!empty($this->id)) {
            $result = ' id="' . $this->id . '"';
        }

        $result .= ' alt="' . htmlentities($this->getAlt()) . '"';

        if ($this->getWidth() !== null) {
            $result .= ' width="' . $this->getWidth() . '"';
        }
        if ($this->getHeight() !== null) {
            $result .= ' height="' . $this->getHeight() . '"';
        }

        /*
         * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/loading
         *   In Firefox, the `loading` attribute must be defined before the `src`
         *   attribute, otherwise it has no effect (Firefox bug 1647077).
         */
        $result .= ' loading="' . $this->getLoading() 
            . '" src="' . $this->getSource() 
            . '" decoding="' . $this->getDecoding() . '">';

        return $result;
    }

    /**
     * Gets the image alt attribute.
     *
     * @param void
     *
     * @return string
     *   Returns the alternative text from the `alt` attribute of an image.
     *   This string MAY be empty.
     */
    public function getAlt(): string
    {
        return $this->alt;
    }

    /**
     * Gets the image decoding attribute.
     *
     * @param void
     *
     * @return string
     *   Returns the `decoding` HTML attribute as the string `sync`, `async`,
     *   or `auto`.
     */
    public function getDecoding(): string
    {
        return $this->decoding;
    }

    /**
     * Gets the image height.
     *
     * @param void
     *
     * @return int|null
     *   Returns the image `height` in pixel as an integer
     *   or `null` if the image `height` was not set.
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * Gets the loading attribute.
     *
     * @param void
     *
     * @return string
     *   Returns the `loading` attribute if it is set or `null` if it is not.
     */
    public function getLoading(): ?string
    {
        return $this->loading;
    }

    /**
     * Gets the image source (src).
     *
     * @param void
     *
     * @return string
     *   Returns the source URL of the image file.
     */
    public function getSource(): string
    {
        return $this->src;
    }

    /**
     * Gets the image width.
     *
     * @param void
     *
     * @return int|null
     *   Returns the image width in pixels as an integer or null if the image
     *   width was not set.
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * Sets the image alt attribute.
     *
     * @param string|null $alternative_text
     *   Alternative text for the `alt` attribute.  Null is accepted and handled
     *   as an empty string.
     *
     * @return void
     */
    public function setAlt(?string $alternative_text): void
    {
        if ($alternative_text === null) {
            $this->alt = '';
        } else {
            $alternative_text = strip_tags($alternative_text);
            $alternative_text = trim($alternative_text);
            $this->alt = $alternative_text;
        }
    }

    /**
     * Sets the image decoding attribute.
     *
     * @param string $decoding
     *   Value of the HTML `decoding` attribute.  Must be 'sync', 'async', or
     *   'auto'.
     *
     * @return void
     */
    public function setDecoding(string $decoding): void
    {
        $decoding = strtolower($decoding);
        if (!in_array($decoding, ['async', 'auto', 'sync'])) {
            throw new ValueError();
        }
        $this->decoding = $decoding;
    }

    /**
     * Sets the image height.
     *
     * @param int|string $height_in_pixels
     *   Image height in pixels (px).
     *
     * @return void
     *
     * @throws \DomainException
     *   Throws a domain exception if the height is smaller than 1 or greater
     *   than 4320, the default height of a 7680 × 4320 pixels 8K UHD image.
     *
     * @throws \ValueError
     *   Throws a value error exception if the height is not a number.
     */
    public function setHeight(int|string $height_in_pixels): void
    {
        if (!is_int($height_in_pixels)) {
            if (is_numeric($height_in_pixels)) {
                $height_in_pixels = (int) $height_in_pixels;
            } else {
                throw new ValueError();
            }
        }

        if ($height_in_pixels < 1 || $height_in_pixels > 4320) {
            throw new DomainException();
        }

        $this->height = $height_in_pixels;
    }

    /**
     * Sets the image loading attribute.
     *
     * @param null|string $loading
     *   Value of the HTML `loading` attribute.  Must be `eager` or `lazy` (default).
     *   Use `null` or an empty string to remove the attribute entirely.
     *
     * @return void
     */
    public function setLoading(?string $loading): void
    {
        if (is_string($loading)) {
            $loading = trim($loading);
            $loading = strtolower($loading);
        }

        if ($loading === 'eager' || $loading === 'lazy') {
            $this->loading = $loading;
        } elseif (empty($loading)) {
            $this->loading = null;
        } else {
            throw new ValueError();
        }
    }

    /**
     * Sets the image URL.
     *
     * @param Psr\Http\Message\UriInterface|string $image_url
     *   URL of the image file for the `src` attribute.  This MAY be a data URI.
     *
     * @return void
     */
    public function setSource(UriInterface|string $image_url): void
    {
        if ($image_url instanceof UriInterface) {
            $image_url = (string) $image_url;
        }
        $this->src = $image_url;
    }

    /**
     * Sets the image width.
     *
     * @param int|string $width_in_pixels
     *   Image width in pixels (px).
     *
     * @return void
     *
     * @throws \DomainException
     *   Throws a domain exception if the width is smaller than 1 or greater
     *   than 7680, the default width of a 7680 × 4320 pixels 8K UHD image.
     *
     * @throws \ValueError
     *   Throws an value error exception if the width is not a number.
     */
    public function setWidth(int|string $width_in_pixels): void
    {
        if (!is_int($width_in_pixels)) {
            if (is_numeric($width_in_pixels)) {
                $width_in_pixels = (int) $width_in_pixels;
            } else {
                throw new ValueError();
            }
        }

        if ($width_in_pixels < 1 || $width_in_pixels > 7680) {
            throw new DomainException();
        }

        $this->width = $width_in_pixels;
    }
}
