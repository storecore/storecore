<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Technical article.
 * 
 * A `TechArticle` is a technical article, such as a how-to (task) topics,
 * step-by-step, procedural troubleshooting, specifications, etc.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/TechArticle Schema.org type `TechArticle`
 * @version 0.1.0
 */
class TechArticle extends Article implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
