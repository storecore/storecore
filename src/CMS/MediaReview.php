<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Media review.
 * 
 * A `MediaReview` is a more specialized form of `Review` dedicated to the
 * evaluation of media content online, typically in the context of
 * fact-checking and misinformation.  For more general reviews of media in the
 * broader sense, use `UserReview`, `CriticReview` or other `Review` types.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/MediaReview Schema.org type `MediaReview`
 * @version 1.0.0-beta.1
 */
class MediaReview extends Review implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is in the “new” area of
     *   Schema.org.  Implementation feedback and adoption from applications
     *   and websites can help improve definitions.
     */
    public const string VERSION = '1.0.0-beta.1';
}
