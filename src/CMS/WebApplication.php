<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * Web application.
 * 
 * A `MobileApplication` is a `SoftwareApplication` designed specifically
 * to work well on a mobile device such as a telephone.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/SoftwareApplication Schema.org type `SoftwareApplication`
 * @see     https://schema.org/WebApplication Schema.org subtype `WebApplication`
 * @version 0.1.0
 */
class WebApplication extends SoftwareApplication implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
