<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Web page footer.
 *
 * The footer section of a web page.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WPFooter Schema.org type `WPFooter`
 * @version 0.1.0
 */
class WPFooter extends WebPageElement implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
