<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Math solver.
 *
 * A math solver which is capable of solving a subset of mathematical problems.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/MathSolver Schema.org type `MathSolver`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/math-solvers Math solver (`MathSolver`) structured data
 * @version 0.1.0
 */
class MathSolver extends AbstractCreativeWork implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|SolveMathAction|string $mathExpression
     *   A mathematical expression that may be solved for a specific variable,
     *   simplified, or transformed.  This can take many formats, e.g. LaTeX,
     *   Ascii-Math, or math as you would write with a keyboard.
     */
    public null|SolveMathAction|string $mathExpression = null;
}
