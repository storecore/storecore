<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016, 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\PropertyValue;

/**
 * Image object.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/ImageObject Schema.org type `ImageObject`
 * @version 1.0.0-alpha.1
 */
class ImageObject extends MediaObject implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\CMS\MediaObject|string $caption
     *   The caption for this object.  For downloadable machine formats
     *   (closed caption, subtitles etc.) use `MediaObject` and indicate
     *   the `encodingFormat`.
     */
    public null|MediaObject|string $caption = null;

    /**
     * @var null|StoreCore\CMS\MediaObject|string $embeddedTextCaption
     *   Represents textual captioning from a `MediaObject`, e.g. text of a meme.
     */
    public ?string $embeddedTextCaption = null;

    /**
     * @var null|StoreCore\Types\PropertyValue|string $exifData
     *   EXIF data for this object.
     */
    public null|PropertyValue|string $exifData = null;

    /**
     * @var null|bool $representativeOfPage
     *   Indicates whether this image is representative of the content of the page.
     */
    public ?bool $representativeOfPage = null;
}
