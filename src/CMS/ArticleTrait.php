<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\SpeakableSpecification;
use StoreCore\Types\URL;

/**
 * Article trait.
 *
 * This PHP `trait` supports all properties of a Schema.org `Article`.
 * Properties are `public` and `null` by default.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/Article Schema.org type `Article`
 * @see     https://schema.org/speakable Schema.org property `speakable`
 * @version 1.0.0
 */
trait ArticleTrait
{
    /**
     * @var null|string $articleBody
     *   The actual body of the article.
     */
    public ?string $articleBody = null;

    /**
     * @var null|string $articleSection
     *   An `Article` may belong to one or more “sections” in a magazine
     *   or newspaper, such as Sports, Lifestyle, etc.
     */
    public ?string $articleSection = null;

    /**
     * @var null|\JsonSerializable|string $backstory
     *   For an `Article`, typically a `NewsArticle`, the `backstory` property
     *   provides a textual summary giving a brief explanation of why and how
     *   an `Article` was created.  In a journalistic setting this could
     *   include information about reporting process, methods, interviews,
     *   data sources, etc.
     */
    public null|\JsonSerializable|string $backstory = null;

    /**
     * @var null|int|string $pageStart
     *   The page on which the work starts; for example `135` or `xiii`.
     */
    public null|int|string $pageStart = null;

    /**
     * @var null|int|string $pageEnd
     *   The page on which the work ends; for example `138` or `xvi`.
     */
    public null|int|string $pageEnd = null;

    /**
     * @var null|string $pagination
     *   Any description of pages that is not separated into `pageStart`
     *   and `pageEnd`; for example, `1-6, 9, 55` or `10-12, 46-49`.
     */
    public ?string $pagination = null;

    /**
     * @var null|SpeakableSpecification|URL $speakable
     *   Indicates sections of a Web page that are particularly “speakable”
     *   in the sense of being highlighted as being especially appropriate for
     *   text-to-speech conversion.  Other sections of a page may also be
     *   usefully spoken in particular circumstances; the `speakable` property
     *   serves to indicate the parts most likely to be generally useful for
     *   speech.
     */
    public null|SpeakableSpecification|URL $speakable = null;

    /**
     * @var null|int $wordCount
     *   The number of words in the text of the `Article`.
     */
    public ?int $wordCount = null;
}
