<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Real estate listing.
 *
 * A `RealEstateListing` is a listing that describes one or more real-estate
 * `Offer`s (whose `businessFunction` is typically to lease out, or to sell).
 * The `RealEstateListing` type itself represents the overall listing, as
 * manifested in some `WebPage`.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/RealEstateListing Schema.org type `RealEstateListing`
 * @version 1.0.0-rc.1
 */
class RealEstateListing extends WebPage
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';
}
