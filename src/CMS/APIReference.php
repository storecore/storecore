<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * API reference.
 * 
 * Reference documentation for application programming interfaces (APIs).
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/APIReference Schema.org type `APIReference`
 * @version 0.1.0
 */
class APIReference extends TechArticle implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
