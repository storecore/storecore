<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\IdentityInterface;
use StoreCore\CRM\{Organization, Person};
use StoreCore\I18N\Language;
use StoreCore\Types\AggregateRating;
use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\DefinedTerm;
use StoreCore\Types\URL;

/**
 * Abstract creative work.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/CreativeWork Schema.org type `CreativeWork`
 */
abstract class AbstractCreativeWork extends AbstractThing implements IdentityInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $abstract
     *   A short description that summarizes a `CreativeWork`.
     */
    public ?string $abstract = null;

    /**
     * @var null|StoreCore\Types\URL $acquireLicensePage
     *   Indicates a page documenting how licenses can be purchased
     *   or otherwise acquired, for the current item.
     */
    public ?URL $acquireLicensePage = null;

    /**
     * @var null|StoreCore\Types\AggregateRating $aggregateRating
     *   The overall rating, based on a collection of reviews or ratings,
     *   of the item.
     */
    public ?AggregateRating $aggregateRating = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person|array $author
     *   The `author` of content or a rating.  Please note that `author` is
     *   special in that HTML 5 provides a special mechanism for indicating
     *   authorship via the `rel` tag.  That is equivalent to this and MAY be
     *   used interchangeably. 
     */
    protected null|Organization|Person|array $author = null;

    /**
     * @var null|string $copyrightNotice
     *   Text of a notice appropriate for describing the copyright aspects of
     *   this `CreativeWork`, ideally indicating the owner of the copyright for
     *   the work.
     */
    public ?string $copyrightNotice = null;

    /**
     * @var null|StoreCore\CRM\Organization|StoreCore\CRM\Person|array $creator
     *	 The creator of this `CreativeWork`.  In Schema.org this is the same as
     *   the `author` property for `CreativeWork`.
     */
    public null|Organization|Person $creator = null;

    /**
     * @var null|string $creditText
     *   Text that can be used to credit person(s) and/or organization(s)
     *   associated with a published `CreativeWork`.
     */
    public ?string $creditText = null;

    /**
     * @var null|StoreCore\Types\DateTime $dateModified
     *   The date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed.
     */
    protected ?DateTime $dateModified = null;

    /**
     * @var null|StoreCore\Types\Date|StoreCore\Types\DateTime $datePublished
     * 	 Date of first broadcast/publication.
     */
    protected null|Date|DateTime $datePublished = null;

    /**
     * @var null|\DateTimeInterface $expires
     *   Date the content expires and is no longer useful or available.
     *   For example a `VideoObject` or `NewsArticle` whose availability or
     *   relevance is time-limited, or a `ClaimReview` fact check whose
     *   publisher wants to indicate that it may no longer be relevant
     *   (or helpful to highlight) after some date.
     */
    public ?\DateTimeInterface $expires = null;

    /**
     * @var null|string $headline
     *   Headline of an article.
     */
    public ?string $headline = null;

    /**
     * @var null|string $html
     *   The content of this `CreativeWork` as HTML or XHTML.
     */
    public ?string $html = null;

    /**
     * @var null|StoreCore\I18N\Language|string $inLanguage
     *   The language of the content.  Please use one of the language codes
     *   from the IETF BCP 47 standard.  Supersedes `language`.
     */
    public null|Language|string $inLanguage = null;

    /**
     * @var null|bool $isFamilyFriendly
     *   Indicates whether this content is family friendly.
     */
    public ?bool $isFamilyFriendly = null;

    /**
     * @var null|string|StoreCore\Types\DefinedTerm|StoreCore\Types\URL $keywords
     *   Keywords or tags used to describe this content.  Multiple entries in
     *   a keywords list are typically delimited by commas.
     */
    public null|string|DefinedTerm|URL $keywords = null;

    /**
     * @var null|StoreCore\Types\URL $license
     *   A license document that applies to this content, typically indicated 
     *   by `URL`.
     */
    public ?URL $license = null;

    /**
     * @var null|string $markdown
     *   The textual content of this `CreativeWork` in Markdown.
     */
    public ?string $markdown = null;

    /**
     * @var null|int|string $position
     *   The position of an item in a series or sequence of items.
     */
    public null|int|string $position = null;

    /**
     * @var null|StoreCore\Types\URL|string $genre
     *   Genre of the creative work.
     */
    public null|URL|string $genre = null;

    /**
     * @var null|StoreCore\Types\URL|string $schemaVersion
     *   Indicates (by URL or string) a particular version of a schema used in
     *   some `CreativeWork`.  This property was created primarily to indicate
     *   the use of a specific Schema.org release.
     */
    public null|URL|string $schemaVersion = null;

    /**
     * @var null|string $text
     *   The textual content of this `CreativeWork`.
     */
    public ?string $text = null;

    /**
     * @var null|StoreCore\Types\URL $thumbnailUrl
     *   A thumbnail image relevant to the `CreativeWork`.
     */
    public ?URL $thumbnailUrl = null;

    /**
     * @var StoreCore\CRM\Organization|StoreCore\CRM\Person $publisher
     *   The publisher of the creative work.
     */
    public null|Organization|Person $publisher = null;

    /**
     * Accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'author' => $this->author,
            'dateModified' => $this->dateModified,
            'datePublished' => $this->datePublished,
            'language' => $this->inLanguage,
            default => parent::__get($name),
        };
    }

    /**
     * Mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     *
     * @see https://schema.org/language#supersededby
     *      `language` has been superseded by `inLanguage`
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'author' => $this->setAuthor($value),
            'dateModified' => $this->setDateModified($value),
            'datePublished' => $this->setDatePublished($value),
            'language' => $this->inLanguage = $value,
            default => parent::__set($name, $value),
        };
    }

    /**
     * @param StoreCore\CRM\Organization|StoreCore\CRM\Person|array|string $author
     * @uses StoreCore\CRM\Person::__construct()
     * @return void
     */
    private function setAuthor(Organization|Person|array|string $author): void
    {
        if (is_string($author)) {
            $author = new Person($author);
        }
        $this->author = $author;
    }

    /**
     * @param \DateTimeInterface|string $value
     * @return void
     */
    private function setDateModified(\DateTimeInterface|string $value): void
    {
        if (is_string($value)) {
            $value = new DateTime($value);
        } elseif (!$value instanceof DateTime) {
            $value = DateTime::createFromInterface($value);
        }
        $this->dateModified = $value;
    }

    /**
     * @param \DateTimeInterface|string $value
     * @return void
     */
    private function setDatePublished(\DateTimeInterface|string $value): void
    {
        if ($value instanceof Date || $value instanceof DateTime) {
            $this->datePublished = $value;
        } elseif ($value instanceof \DateTimeInterface) {
            $this->datePublished = DateTime::createFromInterface($value);
        } else {
            try {
                // Distinguish `DateTime` from `Date`: a time contains 'T' or ':'
                if (str_contains($value, 'T') || str_contains($value, ':')) {
                    $this->datePublished = new DateTime($value);
                } else {
                    $this->datePublished = new Date($value);
                }
            } catch (\Throwable $t) {
                throw new \ValueError($t->getMessage(), $t->getCode(), $t);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $properties = parent::toArray();
        if ($this->author !== null) {
            $properties['author'] = $this->author;
        }
        if ($this->datePublished !== null) {
            $properties['datePublished'] = (string) $this->datePublished;
        }
        if ($this->expires !== null) {
            $properties['expires'] = $this->expires->format('Y-m-d H:i:sP');
        }
        ksort($properties, \SORT_STRING);
        return $properties;
    }
}
