<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Duration;

/**
 * Recipe.
 * 
 * A recipe.  For dietary restrictions covered by the recipe, a few common
 * restrictions are enumerated via `suitableForDiet`.  The `keywords` property
 * can also be used to add more detail.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/HowTo Schema.org type HowTo
 * @version 0.2.0
 */
class Recipe extends HowTo implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var null|StoreCore\Types\Duration $cookTime
     *   The time it takes to actually cook the dish, in ISO 8601 duration format.
     */
    public ?Duration $cookTime = null;
}
