<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2020–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\{Date, DateTime};
use StoreCore\Types\Duration;
use StoreCore\Types\URL;

/**
 * Media object.
 *
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/MediaObject Schema.org type `MediaObject`
 * @version 1.0.0-alpha.1
 */
class MediaObject extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\CMS\NewsArticle $associatedArticle
     *   A `NewsArticle` associated with the `MediaObject`.
     */
    public ?NewsArticle $associatedArticle = null;

    /**
     * @var StoreCore\Types\URL|null $contentUrl
     *   Actual bytes of the media object, for example the image file or
     *   video file.
     */
    public ?URL $contentUrl = null;

    /**
     * @var StoreCore\Types\Duration|null $duration
     *   The duration of the item (movie, audio recording, event, etc.)
     *   in ISO 8601 date format.
     */
    public ?Duration $duration = null;

    /**
     * @var StoreCore\Types\URL|null $embedUrl
     *   A URL pointing to a player for a specific video.  In general, this is
     *   the information in the `src` element of an `embed` tag and SHOULD NOT
     *   be the same as the content of the `loc` tag.
     */
    public ?URL $embedUrl = null;

    /**
     * @var string|null $sha256
     *   The SHA-2 SHA256 hash of the content of the item.
     */
    public ?string $sha256 = null;

    /**
     * @var null|StoreCore\Types\Date $uploadDate
     *   Date when this media object was uploaded to this site.
     */
    protected ?Date $uploadDate = null;

    #[\Override]
    public function __get(string $name): mixed
    {
        return match ($name) {
            'uploadDate' => $this->uploadDate,
            default => parent::__get($name),
        };
    }

    #[\Override]
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'uploadDate' => $this->setUploadDate($value),
            default => parent::__set($name, $value),
        };
    }

    /**
     * Sets the date the media object was first uploaded.
     *
     * @param \DateTimeInterface|string $upload_date
     * @return void
     * @uses StoreCore\Types\Date::__construct()
     * @uses StoreCore\Types\Date::createFromInterface()
     */
    public function setUploadDate(\DateTimeInterface|string $upload_date): void
    {
        if (\is_string($upload_date)) {
            $this->uploadDate = new Date($upload_date);
        } elseif (!$upload_date instanceof Date) {
            $this->uploadDate = Date::createFromInterface($upload_date);
        } else {
            $this->uploadDate = $upload_date;
        }
    }
}
