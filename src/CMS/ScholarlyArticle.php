<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Scholarly article.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/ScholarlyArticle Schema.org type `ScholarlyArticle`
 * @version 1.0.0
 */
class ScholarlyArticle extends Article
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';
}
