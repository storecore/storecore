<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Person;

/**
 * Book.
 *
 * @package StoreCore\PIM
 * @see     https://schema.org/Book Schema.org type `Book`
 * @version 1.0.0
 */
class Book extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var bool|null $abridged
     *   Indicates whether the book is an abridged edition.
     */
    public ?bool $abridged = null;

    /**
     * @var string|null $bookEdition
     *   The edition of the book.
     */
    public ?string $bookEdition = null;

    /**
     * @var StoreCore\CMS\BookFormatType|null $bookFormat
     *   The format of the book.
     */
    public ?BookFormatType $bookFormat = null;

    /**
     * @var StoreCore\CRM\Person|null $illustrator
     *   The illustrator of the book.
     */
    public ?Person $illustrator = null;

    /**
     * @var string|null $isbn
     *   The ISBN of the book.
     */
    public ?string $isbn = null;

    /**
     * @var int|null $numberOfPages
     *   The number of pages in the book.
     */
    public ?int $numberOfPages = null;
}
