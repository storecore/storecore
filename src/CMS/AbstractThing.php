<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use Psr\Http\Message\UriInterface;

use AbstractCreativeWork as CreativeWork;
use StoreCore\{IdentityInterface, IdentityTrait};
use StoreCore\Actions\Action;
use StoreCore\Types\Date;
use StoreCore\Types\Event;
use StoreCore\Types\PropertyValue;
use StoreCore\Types\URL;

/**
 * Abstract thing.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/Thing Schema.org type `Thing`
 */
abstract class AbstractThing implements IdentityInterface, \JsonSerializable
{
    use IdentityTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|StoreCore\Types\URL $additionalType
     *   An additional type for the item, typically used for adding more
     *   specific types from external vocabularies in microdata syntax.
     *   This is a relationship between something and a class that the
     *   `Thing` is in.
     */
    protected ?URL $additionalType = null;

    /**
     * @var null|string $alternateName
     *   An alias for the item.
     */
    public ?string $alternateName = null;

    /**
     * @var null|string $description
     *   A description of the item.
     */
    public ?string $description = null;

    /**
     * @var null|string $disambiguatingDescription
     *   A sub property of `description`.  A short `description` of the item
     *   used to disambiguate from other, similar items.  Information from
     *   other properties (in particular, `name`) may be necessary for the
     *   description to be useful for disambiguation.
     */
    public ?string $disambiguatingDescription = null;

    /**
     * @var null|StoreCore\Types\URL|int $id
     *   JSON-LD `@id` endpoint `URL` or internal identifier as an integer.
     */
    public null|URL|int $id = null;

    /**
     * @var null|StoreCore\CMS\ImageObject|StoreCore\Types\URL $image
     *   An `image` of the item.  This can be a `URL`
     *   or a fully described `ImageObject`.
     */
    public null|ImageObject|URL|array $image = null;

    /**
     * @var null|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\URL $mainEntityOfPage
     *   Indicates a `WebPage` (or other `CreativeWork`) for which this `Thing`
     *   is the main entity being described.  Inverse property: `mainEntity`.
     */
    public null|CreativeWork|URL $mainEntityOfPage = null;

    /**
     * @var null|string $name
     *   The name of the item.
     */
    public ?string $name = null;

    /**
     * @var null|StoreCore\Actions\Action $potentialAction
     *   Indicates a potential `Action`, which describes an idealized action
     *   in which this thing would play an “object” role.
     */
    public ?Action $potentialAction = null;

    /**
     * @var null|StoreCore\Types\URL $sameAs
     *   `URL` of a reference `WebPage` that unambiguously indicates the item’s
     *   identity, e.g. the `URL` of the item’s Wikipedia page, Wikidata entry,
     *   or official website.
     */
    public ?URL $sameAs = null;

    /**
     * @var null|StoreCore\CMS\AbstractCreativeWork|StoreCore\Types\Event $subjectOf
     *   A `CreativeWork` or `Event` about this `Thing`.
     *   Inverse property: `about`.
     */
    public null|CreativeWork|Event $subjectOf = null;

    /**
     * @var null|StoreCore\Types\URL $url
     *   `URL` of the item.
     */
    protected ?URL $url = null;

    /**
     * Creates a creative work thing.
     *
     * @param null|string|array $param
     *   OPTIONAL `name` of the `Thing` as a string or properties
     *   of the `Thing` as an array.
     */
    public function __construct(null|string|array $param = null)
    {
        if (\is_string($param)) {
            $this->name = trim($param);
        } elseif (\is_array($param)) {
            $param = array_filter($param);
            if (!empty($param)) {
                foreach ($param as $name => $value) {
                    $this->__set($name, $value);
                }
            }
        }
    }

    /**
     * Accessor.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'additionalType' => $this->additionalType,
            'url' => $this->url,
            default => null,
        };
    }

    /**
     * Mutator.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'additionalType' => $this->setAdditionalType($value),
            'url' => $this->setUrl($value),
            default => throw new \LogicException(
                'Property ' . get_class($this) . '::$' . $name . ' does not exist.'
            )
        };
    }

    /**
     * Creates a JSON-LD string representation of the object.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        $result = json_encode($this, \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);

        if (str_contains($result, 'schema.org')) {
            $result = str_replace('http://schema.org', 'https://schema.org', $result);
            $result = str_replace('"@context":"https://schema.org/"', '"@context":"https://schema.org"', $result);

            $separator = '"@context":"https://schema.org"';
            $result = explode($separator, $result, 2);
            $result[1] = str_replace('"additionalType":"https://schema.org/', '"additionalType":"', $result[1]);
            $result[1] = str_replace('"@context":"https://schema.org",', '', $result[1]);
            $result = implode($separator, $result);
        }

        return $result;
    }

    /**
     * Creates a “short” date without time.
     *
     * @param \DateTimeInterface|string $date
     * @param null|DateTimeZone $timezone
     * @see https://www.php.net/manual/en/function.date-create.php
     *   date_create(string $datetime = "now", ?DateTimeZone $timezone = null): DateTime|false
     */
    public static function createDate(
        \DateTimeInterface|string $date = 'now',
        ?DateTimeZone $timezone = null
    ): Date|false
    {
        if (\is_string($date)) {
            $date = new Date($date);
        } elseif (!($date instanceof Date)) {
            $date = Date::createFromInterface($date);
        }

        if ($timezone !== null) {
            $date->setTimezone($timezone);
        }
        return $date;
    }

    /**
     * Specify data which should be serialized to JSON-LD.
     *
     * @param void
     * @return array
     * @see https://www.php.net/manual/en/class.jsonserializable.php
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize(): array
    {
        $result = [];
        if ($this->id instanceof URL) $result['@id'] = (string) $this->id;
        $result['@context'] = 'https://schema.org';

        $reflect = new \ReflectionClass($this);
        $result['@type'] = $reflect->getShortName();
        unset($reflect);

        $properties = $this->toArray();
        if (isset($properties['id'])) unset($properties['id']);
        if (!empty($properties)) $result += $properties;

        foreach ($result as $key => $value) {
            if (is_object($value) && $value instanceof \JsonSerializable) {
                $value = $value->jsonSerialize();
                if (is_array($value) && !empty($value)) {
                    foreach ($value as $k => $v) {
                        if ($k === '@context' && str_contains($v, '://schema.org')) {
                            unset($value[$k]);
                        }
                    }
                }
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Sets the Thing.additionalType property.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string $url
     * @return void
     * @see https://schema.org/additionalType Schema.org property `additionalType`
     */
    private function setAdditionalType(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL($url);
        $this->additionalType = $url;
    }

    /**
     * Sets the Thing.url property.
     *
     * @param Psr\Http\Message\UriInterface|StoreCore\Types\URL|string $url
     * @return void
     */
    private function setUrl(UriInterface|URL|string $url): void
    {
        if (!$url instanceof URL) $url = new URL($url);
        $this->url = $url;
    }

    /**
     * Gets all public and set object properties.
     *
     * @param void
     * @return array
     */
    public function toArray(): array
    {
        $properties = array_filter(get_object_vars($this));
        ksort($properties, SORT_STRING);
        return $properties;
    }
}
