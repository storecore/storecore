<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Medical web page.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/MedicalWebPage Schema.org type `MedicalWebPage`
 * @version 1.0.0-rc.1
 */
class MedicalWebPage extends WebPage
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
