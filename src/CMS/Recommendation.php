<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Recommendation.
 *
 * `Recommendation` is a type of `Review` that suggests or proposes something
 * as the best option or best course of `Action`.  Recommendations MAY be for
 * products or services, or other concrete things, as in the case of a ranked
 * list or product guide.  A `Guide` may list multiple recommendations for
 * different categories.  For example, in a `Guide` about which TVs to buy,
 * the author may have several recommendations.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/Recommendation Schema.org type `Recommendation`
 * @version 1.0.0-rc.1
 */
class Recommendation extends Review
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var PhysicalActivityCategory|Thing|URL|string|null $category
     *   A `category` for the item.  Greater signs or slashes can be used to
     *   informally indicate a category hierarchy.
     */
    public PhysicalActivityCategory|Thing|URL|string|null $category = null;
}
