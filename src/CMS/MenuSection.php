<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\MenuItem;;

/**
 * Menu section.
 * 
 * A `MenuSection` ìs a sub-grouping of food or drink items in a menu.
 * E.g. courses (such as “Dinner”, “Breakfast”, etc.), specific type of dishes
 * (such as “Meat”, “Vegan”, “Drinks”, etc.), or some other classification
 * made by the menu provider.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/CreativeWork Schema.org type `MenuSection`
 */
class MenuSection extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|MenuItem|MenuItem[] $hasMenuItem
     *   A food or drink item contained in a menu or menu section.
     */
    public null|MenuItem|array $hasMenuItem = null;

    /**
     * @var null|MenuSection|MenuSection[] $hasMenuSection
     *   A subgrouping of the menu (by dishes, course, serving time period, etc.).
     */
    public null|MenuSection|array $hasMenuSection = null;
}
