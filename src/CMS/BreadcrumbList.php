<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\ItemList;

/**
 * Breadcrumb list.
 * 
 * A `BreadcrumbList` is a Schema.org `ItemList` consisting of a chain of
 * linked Web pages, typically described using at least their URL and their
 * name, and typically ending with the current page.
 * 
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/BreadcrumbList Schema.org type `BreadcrumbList`
 * @version 0.2.0
 */
class BreadcrumbList extends ItemList implements
    \ArrayAccess,
    \Countable,
    \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';
}
