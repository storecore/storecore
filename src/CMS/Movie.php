<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Duration;

/**
 * Movie.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/Movie Schema.org type Movie
 * @version 0.2.0
 */
class Movie extends AbstractCreativeWork implements
    \JsonSerializable,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';

    /**
     * @var StoreCore\Types\Duration|null $duration
     *   The duration of the item (movie, audio recording, event, etc.)
     *   in ISO 8601 date format.
     */
    public ?Duration $duration = null;
}
