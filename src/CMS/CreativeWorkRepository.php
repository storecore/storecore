<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use Psr\Container\ContainerInterface;
use StoreCore\Registry;
use StoreCore\Database\AbstractModel;
use StoreCore\Database\CreativeWorkCache;
use StoreCore\Types\UUIDFactory;

/**
 * Creative work repository.
 *
 * @api
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 */
class CreativeWorkRepository extends AbstractModel implements ContainerInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var StoreCore\Database\CreativeWorkCache $cache
     *   Write-through cache for creative works.
     */
    private CreativeWorkCache $cache;

    /**
     * Creates a repository for creative works.
     *
     * @param StoreCore\Registry $registry
     */
    public function __construct(Registry $registry)
    {
        parent::__construct($registry);
        $this->cache = new CreativeWorkCache($this->Registry);
    }

    /**
     * Gets a creative work from the repository.
     *
     * @param string $id
     *   Unique identifier of the `CreativeWork`.
     *
     * @return StoreCore\CMS\AbstractCreativeWork
     */
    public function get(string $id): AbstractCreativeWork
    {
        return $this->cache->get($id);
    }

    /**
     * Checks if the repository contains a creative work.
     *
     * @param string $id
     *   Unique identifier of the `CreativeWork`.
     * 
     * @return bool
     *   Returns `true` if the `CreativeWork` exists, `false` otherwise.
     */
    public function has(string $id): bool
    {
        return $this->cache->has($id);
    }

    /**
     * Stores a creative work in the repository
     *
     * @param StoreCore\CMS\AbstractCreativeWork $object
     *   The concrete `CreativeWork` to persist.
     *
     * @return bool
     *   Returns `true` on success, `false` otherwise.
     *
     * @uses StoreCore\Types\UUIDFactory::pseudoRandomUUID()
     *   Adds a pseudo-random universally unique ID if the `$object`
     *   does not have an identity.
     */
    public function set(AbstractCreativeWork &$object): bool
    {
        if ($object->hasIdentifier()) {
            $key = $object->getIdentifier();
        } else {
            $key = UUIDFactory::pseudoRandomUUID();
            $object->setIdentifier($key);
        }
        return $this->cache->set((string) $key, $object);
    }
}
