<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\ArchiveOrganization;
use StoreCore\Geo\{Place, PostalAddress};

/**
 * Archive component.
 *
 * An _archive component_ is an intangible Schema.org type to be applied to any
 * archive content, carrying with it a set of properties required to describe
 * archival items and collections.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-rc.1
 * @see     https://schema.org/ArchiveComponent Schema.org type `ArchiveComponent`
 * @see     https://github.com/schemaorg/schemaorg/issues/1758 Archives and their collections, issue #1758
 */
class ArchiveComponent extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var null|StoreCore\CRM\ArchiveOrganization $holdingArchive
     *   `ArchiveOrganization` that holds, keeps or maintains the
     *   `ArchiveComponent`.  Inverse property: `archiveHeld` of
     *   the `ArchiveOrganization`.
     */
    public ?ArchiveOrganization $holdingArchive = null;

    /**
     * @var null|Place|PostalAddress|string $itemLocation
     *   Current location of the item.
     */
    public null|Place|PostalAddress|string $itemLocation = null;
}
