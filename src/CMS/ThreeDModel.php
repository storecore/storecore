<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™Core™Core™Core™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * 3D model.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/3DModel Schema.org type `3DModel`
 * @version 1.0.0
 */
class ThreeDModel extends MediaObject implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|bool $isResizable
     *   Whether the `3DModel` allows resizing.  For example, room layout
     *   applications often do not allow 3DModel elements to be resized
     *   to reflect reality.
     */
    public ?bool $isResizable = null;
}
