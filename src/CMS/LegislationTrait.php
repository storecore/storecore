<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Legislation trait.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/Legislation Schema.org type `Legislation`
 * @see     https://schema.org/LegislationObject Schema.org subtype `LegislationObject`
 * 
 * @see https://en.wikipedia.org/wiki/Multiple_inheritance
 *   Because PHP does not support multiple inheritance, the `LegislationTrait`
 *   is used for `Legislation` as well as a `LegislationObject`.  This allows
 *   us to let the `LegislationObject` extend the `MediaObject`.
 */
trait LegislationTrait
{
    /**
     * @var null|Legislation $legislationTransposes
     *   Indicates that this `Legislation` (or part of `Legislation`) fulfills
     *   the objectives set by another `Legislation`, by passing appropriate
     *   implementation measures.  Typically, some legislations of European
     *   Union’s member states or regions transpose European Directives.  This
     *   indicates a legally binding link between the two legislations.
     */
    public ?Legislation $legislationTransposes = null;
}
