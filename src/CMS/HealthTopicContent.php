<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * Health topic content.
 *
 * `HealthTopicContent` is `WebContent` that is about some aspect of a health
 * topic, e.g. a condition, its symptoms or treatments.  Such content MAY be
 * comprised of several parts or sections and use different types of media.
 * Multiple instances of `WebContent` (and hence `HealthTopicContent`) can be
 * related using `hasPart` and `isPartOf` where there is some kind of content
 * hierarchy, and their content described with about and mentions e.g. building
 * upon the existing `MedicalCondition` vocabulary.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WebContent Schema.org type `WebContent`
 * @see     https://schema.org/HealthTopicContent Schema.org subtype `HealthTopicContent`
 * @version 1.0.0
 */
class HealthTopicContent extends WebContent implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
