<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * Web content.
 *
 * `WebContent` is a Schema.org type representing all `WebPage`, `WebSite` and
 * `WebPageElement` content.  It is sometimes the case that detailed
 * distinctions between Web pages, sites and their parts are not always
 * important or obvious.  The `WebContent` type makes it easier to describe
 * Web-addressable content without requiring such distinctions to always be
 * stated.  (The intent is that the existing types `WebPage`, `WebSite` and
 * `WebPageElement` will eventually be declared as subtypes of `WebContent`.)
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WebContent Schema.org type `WebContent`
 * @version 1.0.0
 */
class WebContent extends AbstractCreativeWork implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
