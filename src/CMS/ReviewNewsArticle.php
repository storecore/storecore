<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Review news article.
 *
 * A `ReviewNewsArticle` is a `NewsArticle` with a `CriticReview`.  This case
 * of multiple inheritance is implemented by extending a `CriticReview` and
 * passing the properties of an `Article` as an `ArticleTrait`.
 *
 * - Thing > CreativeWork > Review > CriticReview > ReviewNewsArticle
 * - Thing > CreativeWork > Article > NewsArticle > ReviewNewsArticle
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/ReviewNewsArticle Schema.org type `ReviewNewsArticle`
 * @version 1.0.0-beta.1
 */
class ReviewNewsArticle extends CriticReview implements \JsonSerializable
{
    use ArticleTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';
}
