<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * How-to step.
 *
 * A `HowToStep` is step in the instructions for how to achieve a result.
 * It is an ordered list with `HowToDirection` and/or `HowToTip` items.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/HowToStep Schema.org type `HowToStep`
 */
class HowToStep extends AbstractCreativeWork implements \JsonSerializable
{
    use ItemListTrait;

    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';
}
