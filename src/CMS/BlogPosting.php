<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Blog posting.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/BlogPosting Schema.org type `BlogPosting`
 * @version 1.0.0-rc.1
 */
class BlogPosting extends SocialMediaPosting implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
