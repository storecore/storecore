<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Trait for ItemList properties.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/ItemList Schema.org type `ItemList`
 * @see     https://schema.org/itemListElement Schema.org property `itemListElement`
 */
trait ItemListTrait
{
    /**
     * @var string VERSION
     *   Items in the `ItemList`.
     */
    public array $itemListElement = [];
}
