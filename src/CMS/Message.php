<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\CRM\Audience;
use StoreCore\CRM\{Organization, Person};

/**
 * Message.
 *
 * A single `Message` from a `sender` to one or more organizations or people.
 * 
 * @api
 * @package StoreCore\Core
 * @see     https://schema.org/Message Schema.org type Message
 * @version 1.0.0-alpha.1
 */
class Message extends AbstractCreativeWork implements \JsonSerializable, \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|\DateTimeInterface $dateRead
     *   The date and time at which the `Message` has been read by
     *   the `recipient` if a single `recipient` exists.
     */
    public null|\DateTimeInterface $dateRead = null;

    /**
     * @var null|\DateTimeInterface $dateSent
     *   The date and time at which the `Message` was sent.
     */
    public null|\DateTimeInterface $dateSent = null;

    /**
     * @var null|Audience|ContactPoint|Organization|Person $recipient
     *   A sub property of an `Action.participant`: the `participant` who is
     *   at the receiving end of the `Action`. 
     */
    public null|Audience|ContactPoint|Organization|Person $recipient = null;

    /**
     * @var null|Audience|ContactPoint|Organization|Person $sender
     *   A sub property of an `Action.participant`: the `participant` who is
     *   at the sending end of the `Action`.
     */
    public null|Audience|Organization|Person $sender = null;
}
