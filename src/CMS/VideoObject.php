<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016, 2020, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Video object.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/VideoObject Schema.org type `VideoObject`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/video
 * @version 0.2.0
 */
class VideoObject extends MediaObject implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';
}
