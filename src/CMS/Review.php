<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Rating;

/**
 * Review.
 *
 * A review of an item — for example, of a restaurant, movie, or store.
 * 
 * @package StoreCore\CMS
 * @version 0.4.0
 *
 * @see https://schema.org/Review
 *      Schema.org type Review
 *
 * @see https://developers.google.com/search/docs/advanced/structured-data/review-snippet
 *      Review Snippet - Advanced SEO Documentation - Google Search Central
 * 
 * @see https://developers.google.com/search/docs/advanced/structured-data/factcheck?hl=sv
 *      Fact Check Markup for Search - Advanced SEO Documentation - Google Search Central
 */
class Review extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.4.0';

    /**
     * @var null|\JsonSerializable $itemReviewed
     *   The item that is being reviewed or rated.
     */
    public ?\JsonSerializable $itemReviewed = null;

    /**
     * @var null|string $reviewBody
     *   The actual body of the `Review`.
     */
    public ?string $reviewBody = null;

    /**
     * @var null|StoreCore\Types\Rating $reviewRating
     *   The rating given in this review.  Note that reviews can themselves be
     *   rated; the `reviewRating` applies to rating given by the review.
     */
    public ?Rating $reviewRating = null;
}
