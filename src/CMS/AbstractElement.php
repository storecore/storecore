<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Abstract element.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://developer.mozilla.org/en-US/docs/Web/API/Element
 * @see     https://developer.mozilla.org/en-US/docs/Web/API/Element/className
 * @see     https://developer.mozilla.org/en-US/docs/Web/API/Element/id
 * @see     https://developer.mozilla.org/en-US/docs/Web/API/Element/tagName
 */
abstract class AbstractElement
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $className
     *   A string variable representing the `class` or space-separated classes
     *   of the current element.
     */
    public ?string $className = null;

    /**
     * @var null|string $id
     *   Optional HTML `id` attribute.
     */
    public ?string $id = null;

    /**
     * @var string $tagName
     *   String indicating the element’s tag name.  This string’s capitalization
     *   depends on the document type.  For DOM trees which represent HTML
     *   documents, the tag name is always in uppercase form, for example 'DIV'
     *   and 'IMG'.
     */
    protected string $tagName = '';
}
