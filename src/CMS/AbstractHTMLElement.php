<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \ErrorException;
use \LogicException;

/**
 * Abstract HTML element.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
 */
abstract class AbstractHTMLElement extends AbstractElement
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    public function __get($name): mixed
    {
        return match ($name) {
            'class' => $this->className,
            'tagName' => $this->tagName,
            default => throw new LogicException('Undefined property: ' . __CLASS__ . '::$' . $name, time()),
        };
    }

    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'class' => $this->className = $value,
            'tagName' => throw new ErrorException('HTML property Element.tagName is read-only'),
            default => throw new LogicException('Undefined property: ' . __CLASS__ . '::$' . $name, time()),
        };
    }
}
