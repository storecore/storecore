<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\Types\ComputerLanguage;
use StoreCore\Types\URL;

/**
 * Software source code.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/SoftwareSourceCode Schema.org type `SoftwareSourceCode`
 * @version 0.1.0
 */
class SoftwareSourceCode extends AbstractCreativeWork implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|StoreCore\Types\URL $codeRepository
     *   Link to the repository where the un-compiled, human readable code
     *   and related code is located (SVN, GitHub, CodePlex).
     */
    public ?URL $codeRepository = null;

    /**
     * @var null|ComputerLanguage|string $programmingLanguage 
     *   The computer programming language.
     */
    public null|ComputerLanguage|string $programmingLanguage = null;
}
