<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Music recording.
 *
 * A music recording (track), usually a single song.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/MusicRecording Schema.org type `MusicRecording`
 * @version 0.2.0
 */
class MusicRecording extends AbstractCreativeWork implements
    \JsonSerializable,
    \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';
}
