<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * HyperTocEntry.
 * 
 * A `HyperToEntry` is an item within a `HyperToc`, which represents a hypertext
 * table of contents for complex media objects, such as `VideoObject` and
 * `AudioObject`.  The `MediaObject` itself is indicated using `associatedMedia`.
 * Each section of interest within that content can be described with a
 * `HyperTocEntry`, with associated `startOffset` and `endOffset`.  When several
 * entries are all from the same file, `associatedMedia` is used on the
 * overarching `HyperTocEntry`; if the content has been split into multiple files,
 * they can be referenced using `associatedMedia` on each `HyperTocEntry`.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/HyperTocEntry Schema.org type `HyperTocEntry`
 */
class HyperTocEntry extends AbstractCreativeWork implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|MediaObject $associatedMedia
     *   A media object that encodes this `CreativeWork`.
     */
    public ?MediaObject $associatedMedia = null;

    /**
     * @var null|HyperTocEntry $tocContinuation
     *   A `HyperTocEntry` can have a `tocContinuation` indicated, which is
     *   another `HyperTocEntry` that would be the default next item to play
     *   or render.
     */
    public ?HyperTocEntry $tocContinuation = null;

    /**
     * @var null|string $utterances
     *   Text of an utterances (spoken words, lyrics etc.) that occurs at a
     *   certain section of a `MediaObject` represented by this `HyperTocEntry`.
     */
    public ?string $utterances = null;
}
