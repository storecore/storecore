<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * CompleteDataFeed.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/CompleteDataFeed Schema.org type `CompleteDataFeed`
 * @version 0.1.0
 */
class CompleteDataFeed extends DataFeed implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
