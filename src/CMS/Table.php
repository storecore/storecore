<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * Table.
 *
 * A table on a Web page..
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WebPageElement Schema.org type `WebPageElement`
 * @see     https://schema.org/Table Schema.org subtype `Table`
 * @version 0.1.0
 */
class Table extends WebPageElement implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
