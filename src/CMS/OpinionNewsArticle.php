<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Opinion news article.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/OpinionNewsArticle Schema.org type `OpinionNewsArticle`
 * @version 1.0.0-beta.1
 */
class OpinionNewsArticle extends NewsArticle implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';
}
