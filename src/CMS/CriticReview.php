<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Critic review.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/CriticReview Schema.org type `CriticReview`
 * @version 1.0.0-rc.1
 *
 * @see https://developers.google.com/search/updates?hl=cs#critic-review
 *   On June 10, 2021 Google deprecated the critic review documentation.
 *   Google initially tested critic review markup with a group of site owners,
 *   and ultimately found that it wasn’t useful for the ecosystem at scale.
 *   This deprecation did not affect any other features on Google Search
 *   that use review markup.  Feel free to leave the markup on your site
 *   so that search engines can better understand your web page.
 */
class CriticReview extends Review implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
