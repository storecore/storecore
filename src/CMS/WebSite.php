<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Web site.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/WebSite Schema.org type WebSite
 * @version 0.3.0
 */
class WebSite extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.3.0';
}
