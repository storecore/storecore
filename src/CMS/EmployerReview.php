<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Employer review.
 * 
 * An `EmployerReview` is a `Review` of an `Organization` regarding its role as
 * an employer, written by a current or former employee of that `Organization`.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/EmployerReview Schema.org type `EmployerReview`
 * @version 1.0.0-rc.1
 */
class EmployerReview extends Review implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';
}
