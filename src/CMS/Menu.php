<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2016–2018, 2020–2021, 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\MenuItem;;

/**
 * Menu.
 * 
 * A structured representation of food or drink items available
 * from a `FoodEstablishment`.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/Menu Schema.org type `Menu`
 */
class Menu extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';

    /**
     * @var null|MenuItem|MenuItem[] $hasMenuItem
     *   A food or drink item contained in a menu or menu section.
     */
    public null|MenuItem|array $hasMenuItem = null;

    /**
     * @var null|MenuSection|MenuSection[] $hasMenuSection
     *   A subgrouping of the menu (by dishes, course, serving time period, etc.).
     */
    public null|MenuSection|array $hasMenuSection = null;
}
