<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;
use StoreCore\IdentityInterface;

/**
 * Web page ad block.
 *
 * An advertising section of the page.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/WPAdBlock Schema.org type `WPAdBlock`
 * @version 0.1.0
 */
class WPAdBlock extends WebPageElement implements IdentityInterface, JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
