<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * HyperToc (hypertext table of contents).
 * 
 * A `HyperToc` represents a hypertext table of contents for complex media
 * objects, such as `VideoObject` and `AudioObject`.  Items in the table of
 * contents are indicated using the `tocEntry` property, and typed
 * `HyperTocEntry`.  For cases where the same larger work is split into
 * multiple files, `associatedMedia` can be used on individual `HyperTocEntry`
 * items.
 *
 * @package StoreCore\CMS
 * @version 0.1.0
 * @see     https://schema.org/HyperToc Schema.org type `HyperToc`
 */
class HyperToc extends AbstractCreativeWork
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
