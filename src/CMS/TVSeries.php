<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * TVSeries.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/TVSeries Schema.org type `TVSeries`
 * @version 0.1.0
 */
class TVSeries extends CreativeWorkSeries implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
