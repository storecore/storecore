<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use StoreCore\Types\Rating;
use StoreCore\Types\Thing;

/**
 * Claim review.
 *
 * A _claim review_ is a fact-checking `Review` of claims made (or reported)
 * in some `CreativeWork` (referenced via `itemReviewed`).
 * 
 * @package StoreCore\CMS
 * @version 1.0.0-alpha.1
 * @see     https://schema.org/ClaimReview Schema.org type `ClaimReview`
 * @see     https://developers.google.com/search/docs/appearance/structured-data/factcheck Fact check (`ClaimReview`) structured data
 */
class ClaimReview extends Review
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var null|string $claimReviewed
     *   A short summary of the specific claims reviewed in a `ClaimReview`.
     */
    public ?string $claimReviewed = null;
}
