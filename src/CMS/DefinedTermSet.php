<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021, 2023-2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use \JsonSerializable;

/**
 * Defined term set.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/DefinedTermSet Schema.org type `DefinedTermSet`
 * @version 1.0.0
 */
class DefinedTermSet extends AbstractCreativeWork implements JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * @var null|StoreCore\Types\DefinedTerm|array $hasDefinedTerm
     *   A defined term contained in this term set.
     */
    public null|DefinedTerm|array $hasDefinedTerm = null;
}
