<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Web page.
 *
 * A web page.  Every web page is implicitly assumed to be declared to be of type
 * `WebPage`, so the various properties about that webpage, such as `breadcrumb`
 * MAY be used.
 *
 * @api
 * @package StoreCore\CMS
 * @see     https://schema.org/WebPage Schema.org type `WebPage`
 * @version 0.2.0
 */
class WebPage extends AbstractCreativeWork implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.0';
}
