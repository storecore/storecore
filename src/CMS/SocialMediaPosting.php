<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

use AbstractCreativeWork as CreativeWork;

/**
 * Social media posting.
 *
 * @package StoreCore\Core
 * @see     https://schema.org/SocialMediaPosting Schema.org type `SocialMediaPosting`
 * @version 1.0.0-rc.1
 */
class SocialMediaPosting extends Article implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-rc.1';

    /**
     * @var null|StoreCore\CMS\AbstractCreativeWork $sharedContent
     *   A `CreativeWork` such as an image, video, or audio clip shared as part
     *   of this posting.
     */
    public ?CreativeWork $sharedContent = null;
}
