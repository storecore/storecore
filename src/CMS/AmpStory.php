<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * AMP story.
 *
 * @package StoreCore\CMS
 * @version 1.0.0-rc.1
 * @see     https://schema.org/AmpStory Schema.org type `AmpStory`
 * @see     https://amp.dev/documentation/components/amp-story AMP component `<amp-story>`
 */
class AmpStory extends MediaObject implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).  This term is proposed for full integration
     *   into Schema.org, pending implementation feedback and adoption from
     *   applications and websites.
     */
    public const string VERSION = '1.0.0-rc.1';
}
