<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\CMS;

/**
 * Ask the public news article.
 *
 * @package StoreCore\CMS
 * @see     https://schema.org/AskPublicNewsArticle Schema.org type `AskPublicNewsArticle`
 * @version 1.0.0-beta.1
 */
class AskPublicNewsArticle extends NewsArticle implements \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-beta.1';
}
