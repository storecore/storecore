<?php

declare(strict_types=1);

namespace StoreCore\Google\People;

/**
 * Name of a person.
 *
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2022 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @package   StoreCore\CRM
 * @see       https://developers.google.com/resources/api-libraries/documentation/people/v1/php/latest/class-Google_Service_People_Name.html
 * @see       https://developers.google.com/resources/api-libraries/documentation/people/v1/java/latest/com/google/api/services/people/v1/model/Name.html
 * @see       https://developers.google.com/resources/api-libraries/documentation/people/v1/java/latest/com/google/api/services/people/v1/model/class-use/Name.html
 * @version   0.1.0
 */
class Name implements \Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';


    /**
     * @var string|null $additionalName
     *   An additional name for a `Person`.  In Schema.org the `additionalName`
     *   MAY be used for a middle name, but we recommend to use the more
     *   distinctive `middleName` property for middle name(s).
     */
    public ?string $additionalName = null;

    /**
     * @var string|null $displayName
     *   The display name formatted according to the locale specified
     *   by the viewer’s account or the `Accept-Language` HTTP header.
     */
    protected ?string $displayName = null;

    /**
     * @var string|null $displayNameLastFirst
     *   The display name with the last name first formatted according to the
     *   locale specified by the viewer’s account or the `Accept-Language`
     *   HTTP header.
     */
    public ?string $displayNameLastFirst = null;

    /**
     * @var string|null $familyName
     *   Family name.  In the U.S., the last name of a `Person`.
     */
    public ?string $familyName = null;

    /**
     * @var string|null $fullName
     *   Full name of a person, similar to the generic `name` property of a
     *   Schema.org `Person` and `FN` in the vCard file format.  This property
     *   is not included in the Geogle People API and it MAY be constructed
     *   using other name elements.
     */
    protected ?string $fullName = null;

    /**
     * @var string|null $givenName
     *   Given name.  In the U.S., the first name of a `Person`.
     */
    public ?string $givenName = null;

    /**
     * @var string|null $honorificPrefix
     *   An honorific prefix preceding a person’s name, such as Mrs. or Dr.
     */
    public ?string $honorificPrefix = null;

    /**
     * @var string|null $honorificSuffix
     *   An honorific suffix following a person’s name, such as Jr.
     */
    public ?string $honorificSuffix = null;

    /**
     * @var string|null $middleName
     *   The middle name(s).
     */
    public ?string $middleName = null;

    /**
     * @var string|null $phoneticFamilyName
     *   The family name spelled as it sounds.
     */
    public ?string $phoneticFamilyName = null;

    /**
     * @var string|null $phoneticFullName
     *   The full name spelled as it sounds.
     */
    public ?string $phoneticFullName = null;

    /**
     * @var string|null $phoneticGivenName
     *   The given name spelled as it sounds.
     */
    public ?string $phoneticGivenName = null;

    /**
     * @var string|null $phoneticHonorificPrefix
     *   The honorific prefixes spelled as they sound.
     */
    public ?string $phoneticHonorificPrefix = null;

    /**
     * @var string|null $phoneticHonorificSuffix
     *   The honorific suffixes spelled as they sound.
     */
    public ?string $phoneticHonorificSuffix = null;

    /**
     * @var string|null $phoneticMiddleName
     *   The middle name(s) spelled as they sound.
     */
    public ?string $phoneticMiddleName = null;


    /**
     * Creates a person’s name.
     *
     * @param string|null $display_name
     *   OPTIONAL display name of the person.
     */
    public function __construct(?string $display_name = null)
    {
        if ($display_name !== null) $this->setDisplayName($display_name);
    }

    /**
     * Writes data to inaccessible (protected or private) properties.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'displayName' => $this->setDisplayName($value),
            default => throw new \ValueError('Unknown property: ' . $name),
        };
    }

    /**
     * Reads data from inaccessible (protected or private) or non-existing properties.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'displayName' => $this->getDisplayName(),
            'fullName' => $this->getFullName(),
            default => null,
        };
    }

    /**
     * Converts the person name to a string.
     *
     * @param void
     * @return string
     * @uses getFullName()
     */
    public function __toString(): string
    {
        if ($this->displayName !== null) {
            return $this->displayName;
        }

        if ($this->displayNameLastFirst !== null) {
            return $this->displayNameLastFirst;
        }

        if ($this->givenName !== null || $this->familyName !== null) {
            return trim($this->givenName . ' ' . $this->familyName);
        }

        return (string) $this->getFullName();
    }

    /**
     * Gets the display name.
     *
     * @param void
     * @return string
     */
    public function getDisplayName(): string
    {
        if ($this->displayName !== null) {
            return $this->displayName;
        } else {
            return trim($this->givenName . ' ' . $this->familyName);
        }
    }

    /**
     * Gets the full name.
     *
     * @param void
     * @return string
     */
    public function getFullName(): string
    {
        if ($this->fullName !== null) {
            return $this->fullName;
        } else {
            $result = '';
            if (!empty($this->honorificPrefix)) $result .= $this->honorificPrefix;
            if (!empty($this->givenName)) $result .= ' ' . $this->givenName;
            if (!empty($this->middleName)) $result .= ' ' . $this->middleName;
            if (!empty($this->familyName)) $result .= ' ' . $this->familyName;
            if (!empty($this->honorificSuffix)) $result .= ' ' . $this->honorificSuffix;
            return $result;
        }
    }

    /**
     * Sets the display name.
     *
     * @param string $display_name
     * @return void
     */
    public function setDisplayName(string $display_name): void
    {
        $this->displayName = $display_name;
    }

    /**
     * Sets the full name.
     *
     * @param string $full_name
     * @return void
     */
    public function setFullName(string $full_name): void
    {
        $this->fullName = $full_name;
    }
}
