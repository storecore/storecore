<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PostNL\Addresses;

use StoreCore\OML\AddressIndex;
use StoreCore\Types\CountryCode;

/**
 * PostNL KIX.
 *
 * KIX is an abbreviation of “klantindex”, Dutch for “customer index”.
 * The PostNL KIX is a barcode that identifies a unique address in the
 * Netherlands by its postal code, house number, and an optional addition to
 * the house number.  The KIX may be used for addresses outside the Netherlands
 * by prepending a two-letter ISO country code to a postal code.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 *
 * @see https://www.postnl.nl/Images/Brochure-KIX-code-van-PostNL_tcm10-10210.pdf
 *      Handleiding KIX code, PDF manual in Dutch
 */
class KIX extends AddressIndex
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /**
     * Creates a PostNL KIX.
     *
     * @param void
     * @uses \StoreCore\Types\CountryCode::__construct
     */
    public function __construct()
    {
        $this->countryCode = new CountryCode('NL');
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        if (!empty($this->countryCode) && (string) $this->countryCode !== 'NL') {
            if (!empty($this->postalCode)) {
                return $this->countryCode . $this->postalCode;
            } else {
                return '';
            }
        }

        return parent::__toString();
    }
}
