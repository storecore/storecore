<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PostNL\SendAndTrack;

use StoreCore\OML\{AbstractDeliveryEvent, DeliveryEventInterface};

/**
 * PostNL delivery event.
 *
 * @package StoreCore\OML
 * @version 0.1.0
 */
class DeliveryEvent extends AbstractDeliveryEvent implements DeliveryEventInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.1.0';
}
