<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PostNL\SendAndTrack;

use \Stringable;
use StoreCore\LanguageInterface;
use StoreCore\OML\TrackingInterface;

use \UnhandledMatchError;
use \ValueError;
use StoreCore\Geo\Country;
use StoreCore\Geo\CountryCodes;
use StoreCore\I18N\Language;
use StoreCore\OML\PostalCode;
use StoreCore\Types\CountryCode;
use StoreCore\Types\LanguageCode;

use function \array_key_exists;
use function \empty;
use function \http_build_query;
use function \is_string;
use function \isset;
use function \preg_replace;
use function \strlen;
use function \strtoupper;
use function \substr;
use function \time;
use function \trim;

/**
 * PostNL track & trace direct link.
 *
 * The PostNL tracking URL consists of five parameters.  These are handled by
 * properties and accessors of this class, and returned as a full URL by the
 * `getTrackingUrl()` and `__toString()` methods.
 *
 * | URL | Property       | Required |
 * | --- | -------------- | -------- |
 * |  B  | trackingNumber | Required |
 * |  D  | destination    | Required |
 * |  L  | language       | Optional |
 * |  P  | postalCode     | Optional |
 * |  T  | type           | Required |
 *
 * @see https://www.postnl.nl/Images/stappenkaart-track-trace-directe-links_tcm10-179375.pdf
 *      PostNL Stappenkaart: Genereer Track & Trace directe links voor uw klant (PDF in Dutch)
 *
 * @package StoreCore\OML
 * @see     https://schema.org/trackingUrl Schema.org property trackingUrl
 * @version 1.0.0-alpha.1
 */
class TrackingUrl implements LanguageInterface, Stringable, TrackingInterface
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0-alpha.1';

    /**
     * @var array $supportedLanguages
     *   Languages supported by the PostNL track & trace apps.
     *   The `$language` property MUST be set to one of the array keys.
     */
    public const SUPPORTED_LANGUAGES = [
        'cn' => 'Chinese',
        'de' => 'Deutsch',
        'en' => 'English',
        'es' => 'Español',
        'fr' => 'Français',
        'it' => 'Italiano',
        'nl' => 'Nederlands',
    ];

    /**
     * @var StoreCore\Types\CountryCode $destination
     *   Two-letter alphanumeric ISO code of the destination country.
     *   Defaults to 'NL' for the Netherlands.
     */
    private CountryCode $destination;

    /**
     * @var string $language
     *   Language of the customer-facing track & trace user interfaces.
     *   Defaults to 'en' for English; if the optional `L` language parameter
     *   is omitted from the track & trace link, the web interface for
     *   international parcel delivery tracking also defaults to English.
     *   MUST be set to one of the `SUPPORTED_LANGUAGES` keys.
     */
    private string $language = 'en';

    /**
     * @var null|StoreCore\OML\PostalCode $postalCode
     *   OPTIONAL postal code of the delivery address.
     */
    private ?PostalCode $postalCode = null;

    /**
     * @var string $type
     *   The track & trace link type may be 'C' for a “Consumer” (default)
     *   or 'B' for a “Business” (a B2B PostNL customer).  If the 'B' type is
     *   set, the link redirects to the PostNL business portal backoffice.
     */
    private string $type = 'C';

    /**
     * @var string $trackingNumber
     *   PostNL 3S code of delivery, also called “barcode.”
     */
    private string $trackingNumber;

    /**
     * Creates a PostNL track & trace direct link.
     *
     * @param string|null $tracking_number
     * @param null|StoreCore\Geo\Country|StoreCore\Types\CountryCode|string $destination_country
     * @param StoreCore\OML\PostalCode|string|null $postal_code = null
     * 
     * @uses setTrackingNumber()
     * @uses setDestination()
     * @uses setPostalCode()
     */
    public function __construct(
        null|string $tracking_number = null,
        null|Country|CountryCode|string $destination_country = 'NL',
        null|PostalCode|string $postal_code = null
    ) {
        if (empty($destination_country)) {
            $destination_country = new CountryCode('NL');
        }
        $this->setDestination($destination_country);

        if ($tracking_number !== null) $this->setTrackingNumber($tracking_number);
        if ($postal_code !== null) $this->setPostalCode($postal_code);
    }

    /**
     * Generic getter.
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return match ($name) {
            'destination', 'D' => $this->getDestination(),
            'language', 'L' => strtoupper($this->inLanguage()),
            'postalCode', 'P' => $this->getPostalCode(),
            'trackingNumber', 'B' => $this->getTrackingNumber(),
            'trackingUrl' => $this->getTrackingUrl(),
            'type', 'T' => $this->type === 'B' ? 'B' : 'C',
            default => throw new UnhandledMatchError('Undefined property: ' . $name, time()),
        };
    }

    /**
     * Generic setter.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        match ($name) {
            'destination', 'D' => $this->setDestination($value),
            'language', 'L' => $this->inLanguage($value),
            'postalCode', 'P' => $this->setPostalCode($value),
            'trackingNumber', 'B' => $this->setTrackingNumber($value),
            'type', 'T' => $this->type = strtoupper($value) === 'B' ? 'B' : 'C',
            default => throw new UnhandledMatchError('Undefined property: ' . $name, time()),
        };
    }

    /**
     * Converts the full tracking URL to a string.
     *
     * @param void
     * @return string
     * @uses ::getTrackingUrl()
     */
    public function __toString(): string
    {
        return $this->getTrackingUrl();
    }

    /**
     * Gets the destination country (D).
     *
     * @param void
     * @return string
     */
    public function getDestination(): string
    {
        return (string) $this->destination;
    }

    /**
     * Gets the postal code (P).
     *
     * @param void
     * @return string
     */
    public function getPostalCode(): string
    {
        $result = (string) $this->postalCode;
        $result = preg_replace('/\s+/', '', $result);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getTrackingNumber(): string
    {
        return (string) $this->trackingNumber;
    }

    /**
     * @inheritDoc
     */
    public function getTrackingUrl(): string
    {
        $params = [
            'B' => $this->getTrackingNumber(),
            'P' => $this->getPostalCode(),
            'D' => $this->getDestination(),
            'L' => strtoupper($this->inLanguage()),
            'T' => $this->type === 'B' ? 'B' : 'C',
        ];

        return 'https://postnl.nl/tracktrace/?' . http_build_query($params);
    }

    /**
     * Sets the destination country (D).
     *
     * @param StoreCore\Geo\Country|StoreCore\Types\CountryCode|string $country
     *   Destination country as a `Country` or `CountryCode` value object
     *   or a two-letter country code string.
     *
     * @return void
     *
     * @uses StoreCore\Geo\CountryCodes::createCountryCode()
     */
    public function setDestination(Country|CountryCode|string $country): void
    {
        if ($country instanceof Country) $country = $country->getCode();
        if (is_string($country)) $country = CountryCodes::createCountryCode($country);
        $this->destination = $country;
    }

    /**
     * Gets and sets the customer interface language.
     *
     * @param null|Language|LanguageCode|string $language
     *   OPTIONAL language or language code to set.
     *
     * @return string
     *   Lowercase two-letter ISO language code.  Note that the tracking URL
     *   uses uppercase language codes.
     */
    public function inLanguage(null|Language|LanguageCode|string $language = null): string
    {
        if ($language !== null) {
            if ($language instanceof Language) {
                $language = $language->identifier;
            } elseif ($language instanceof LanguageCode) {
                $language = (string) $language;
            }

            $language = trim($language);
            if (strlen($language) > 2) $language = substr($language, 0, 2);
            $language = strtolower($language);

            if (isset(self::SUPPORTED_LANGUAGES[$language])) {
                $this->language = $language;
            } else {
                throw new ValueError('Language code `' . $language . '` is not supported.');
            }
        }

        return $this->language;
    }

    /**
     * Sets the postal code.
     *
     * @param StoreCore\OML\PostalCode|string $postal_code
     *   Postal code of the destination address.  A string is converted
     *   to a stringable `PostalCode` value object.
     *
     * @return void
     */
    public function setPostalCode(PostalCode|string $postal_code): void
    {
        if (is_string($postal_code)) {
            $postal_code = preg_replace('/\s+/', '', $postal_code);
            $postal_code = new PostalCode($postal_code);
        }

        $this->postalCode = $postal_code;
    }

    /**
     * Sets the tracking number.
     *
     * @param string $tracking_number
     *   PostNL tracking number, often referred to as the “3S barcode”,
     *   “3S code”, or “barcode”.
     *
     * @return void
     */
    public function setTrackingNumber(string $tracking_number): void
    {
        $tracking_number = trim($tracking_number);
        $tracking_number = preg_replace('/\s+/', '', $tracking_number);
        $tracking_number = strtoupper($tracking_number);
        $this->trackingNumber = $tracking_number;
    }
}
