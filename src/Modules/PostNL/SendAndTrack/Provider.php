<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2021–2024 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore\PostNL\SendAndTrack;

use StoreCore\OML\{AbstractProvider, ProviderInterface};

/**
 * PostNL as parcel delivery provider.
 *
 * @package StoreCore\OML
 * @version 1.0.0
 */
class Provider extends AbstractProvider implements ProviderInterface, \JsonSerializable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '1.0.0';

    /** @inheritDoc */
    public string $name = 'PostNL';

    /** @inheritDoc */
    public ?string $url = 'https://www.postnl.nl/';
}
