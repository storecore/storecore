<?php

declare(strict_types=1);

namespace StoreCore\Modules;

use StoreCore\Database\AbstractModel;

abstract class AbstractSetup extends AbstractModel
{
}
