<?php

/**
 * @author    Ward van der Put <Ward.van.der.Put@storecore.org>
 * @copyright Copyright © 2023 StoreCore™
 * @license   https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

declare(strict_types=1);

namespace StoreCore;

use StoreCore\Database\Currencies;

use \JsonSerializable;
use \Stringable;

use function \is_int;
use function \is_string;
use function \str_pad;

use const \STR_PAD_RIGHT;

/**
 * Money.
 *
 * @package StoreCore\Core
 * @version 0.2.1
 * @see     https://martinfowler.com/eaaCatalog/money.html Money pattern, by Martin Fowler
 */
readonly class Money implements JsonSerializable, Stringable
{
    /**
     * @var string VERSION
     *   Semantic Version (SemVer).
     */
    public const string VERSION = '0.2.1';

    /**
     * @param StoreCore $currency
     *   Currency of the money amount.
     */
    private Currency $currency;

    /**
     * @param float $amount
     *   The amount of money.
     */
    private string $amount;

    /**
     * Creates an immutable money value object.
     *
     * @param string|float|int $amount
     * @param null|StoreCore\Currency|string $currency
     */
    public function __construct(string|float|int $amount, null|Currency|string $currency = null)
    {
        if ($currency === null) {
            $currency = new Currency();
        } elseif (is_string($currency)) {
            $currency = Currencies::createFromString($currency);
        }
        $this->currency = $currency;

        if (is_int($amount)) {
            $amount = $amount . '.' . str_pad('', $this->currency->precision, '0', STR_PAD_RIGHT);
        }

        $this->amount = (string) $amount;
    }

    /**
     * Returns the money amount string value.
     *
     * @param void
     * @return string
     */
    public function __toString(): string
    {
        return $this->amount;
    }

    /**
     * Gets the money amount as a float.
     *
     * @param void
     * @return float
     */
    public function getAmount(): float
    {
        return (float) $this->amount;
    }

    /**
     * Gets the money currency as a value object.
     *
     * @param void
     * @return StoreCore\Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * Specifies data which should be serialized to JSON-LD.
     *
     * @param void
     * @return string
     */
    public function jsonSerialize(): string
    {
        return $this->currency->code . ' ' . $this->__toString();
    }
}
