<?php

declare(strict_types=1);

namespace StoreCore;

class DependencyException extends LogicException {}
