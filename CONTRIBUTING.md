StoreCore Developer Guide
=========================

# 1. Introduction

## Key Words to Indicate Requirement Levels (RFC 2119)

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”,  “MAY”, and “OPTIONAL” in this document are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## Pardon Our Dunglish

As native speakers of Dutch, we sometimes write *Dunglish:* a portmanteau of
*Dutch and English* or *Dutch English.*  Please do feel free to correct our
mistakes.  Simply fork this [Developer Guide on GitLab] and submit a pull
request that fixes the mistake.

[Developer Guide on GitLab]: https://gitlab.com/storecore/storecore/-/blob/develop/CONTRIBUTING.md "StoreCore Developer Guide"


# 2. Developer policies

## 2.1 Normative references

**Don’t reinvent the wheel.** Please refer to the following standards and
recommendations for the current best practices.

- [AMP Design Principles] for AMP HTML components;
- [Google HTML/CSS Style Guide] for HTML and CSS;
- [Google JSON Style Guide] for JSON;
- [Google TypeScript Style Guide] for JavaScript and TypeScript;
- [Keep a Changelog] for the change log;
- [PER Coding Style Guide] for PHP;
- [PHP-PDS Skeleton] for the project directory structure;
- [PSR-1 Basic Coding Standard] for PHP;
- [PSR-3 Logger Interface] for logging;
- [PSR-4 Autoloader] for autoloading;
- [PSR-5 PHPDoc (proposal)] for PHPDoc and DocBlocks;
- [PSR-11 Container Interface] for dependency injection containers;
- [PSR-13 Link Definition Interfaces] for hypermedia links;
- [PSR-15 HTTP Server Request Handlers] for server middleware;
- [PSR-16 Simple Cache] for caching interfaces;
- [PSR-17 HTTP Factories] for factories that create HTTP objects;
- [PSR-20 Clock] for dates, times, and timestamps;
- [Semantic Versioning (SemVer)] for version IDs;
- [W3C JSON-LD Best Practices] for JSON-LD.

[AMP Design Principles]: https://principles.design/examples/amp-design-principles "AMP Design Principles"

[Google HTML/CSS Style Guide]: https://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml "Google HTML/CSS Style Guide"

[Google JSON Style Guide]: https://google.github.io/styleguide/jsoncstyleguide.xml "Google JSON Style Guide"

[Google TypeScript Style Guide]: https://google.github.io/styleguide/tsguide.html ""

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/ "Keep a Changelog"

[PER Coding Style Guide]: https://github.com/php-fig/per-coding-style/blob/master/spec.md "PER Coding Style Guide"

[PHP-PDS Skeleton]: https://github.com/php-pds/skeleton "php-pds/skeleton on GitHub"

[PSR-1 Basic Coding Standard]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md "PSR-1 Basic Coding Standard"

[PSR-3 Logger Interface]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md "PSR-3 Logger Interface"

[PSR-4 Autoloader]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md "PSR-4 Autoloader"

[PSR-5 PHPDoc (proposal)]: https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md "PSR-5 PHPDoc"

[PSR-11 Container Interface]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-11-container.md "PSR-11 Container interface"

[PSR-13 Link Definition Interfaces]: https://www.php-fig.org/psr/psr-13/ "PSR-13 Link Definition Interfaces"

[PSR-15 HTTP Server Request Handlers]: https://www.php-fig.org/psr/psr-15/ "PSR-15 HTTP Server Request Handlers"

[PSR-16 Simple Cache]: https://www.php-fig.org/psr/psr-16/ "PSR-16 Common Interface for Caching Libraries"

[PSR-17 HTTP Factories]: https://www.php-fig.org/psr/psr-17/ "PSR-17 HTTP Factories"

[PSR-20 Clock]: https://www.php-fig.org/psr/psr-20/ "PSR-20 Clock"

[Semantic Versioning (SemVer)]: https://semver.org/ "Semantic Versioning 2.0.0"

[W3C JSON-LD Best Practices]: https://w3c.github.io/json-ld-bp/ "JSON-LD Best Practices"


## 2.2 PHP namespaces and FQCN’s

The global namespace `StoreCore` is used for all system files that are critical
to the core.  StoreCore components are subsequently added in their own
`StoreCore\<project_name>` sub-namespace, for example `StoreCore\Database`
for databases and `StoreCore\FileSystem` for components related to files and
file handling.  This is an implementation of *fully qualified class names*
(FQCN’s) from the [PSR-4 Autoloader] recommendation:

- The fully qualified class name MUST have a top-level namespace name, also
  known as a “vendor namespace”.

- The fully qualified class name MAY have one or more sub-namespace names.

[PSR-4 Autoloader]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md


## 2.3 Global constants

Global constants match the StoreCore namespace and sub-namespace prefixes.
For example, the `StoreCore\Database` namespace uses constants with a
`STORECORE_DATABASE` prefix in constant names like `STORECORE_DATABASE_DEFAULT_USERNAME`
and `STORECORE_DATABASE_DEFAULT_PASSWORD`.


## 2.4 Packages

There are three packages dedicated to the core:

- `@package StoreCore\Core` for core system files and core services;
- `@package StoreCore\I18N` for internationalization (I18N) and localization (L10N);
- `@package StoreCore\Security` for everything related to security.

Furthermore, there are five packages for functional areas:

- `@package StoreCore\BI` for Business Intelligence (BI);
- `@package StoreCore\CMS` for the Content Management System (CMS);
- `@package StoreCore\CRM` for Customer Relationship Management (CRM);
- `@package StoreCore\OML` for Operations Management and Logistics (OML);
- `@package StoreCore\PIM` for Product Information Management (PIM).


# 3. Security

## 3.1 Protected configuration files

All files with a `config` filename are inaccessible.  These include common
filenames like `config.ini` and `config.php`.

This file protection is configured in the default `.htaccess` Apache system
file:

```
<Files "config.*">
  Order Allow,Deny
  Deny from all
</Files>
```


## 3.2 Protected file types

The following file extensions are publicly inaccessible:

* `.bak` for backup files
* `.ctp` for CakePHP template files
* `.dll` for dynamic-link libraries
* `.gz` for the gzip (GNU zip) compression format
* `.inc` and `.ssi` for server-side includes
* `.ini` for initialization and configuration files
* `.less`, `.sass`, and `.scss` for CSS style sheets
* `.log` for log files
* `.md` for Markdown documentation files
* `.phar` for the PHAR (PHP archive) package format
* `.phps` for PHP script output
* `.phtml` and `.tpl` for template files
* `.sh` for Bash Unix shell scripts
* `.sql` for SQL database files
* `.tmp` for temporary files
* `.twig` for Twig template files
* `.yaml` and `.yml` for YAML comfiguration and data files


## 3.3 Protected directories

There are six directories that MUST NOT be accessed publicly:

* `/config/` for configuration files
* `/logs/` for log files
* `/resources/` for data and cache files
* `/src/` for the source code library
* `/vendor/` for the third-party integrations
* `/tests/` for unit tests

Access to these directories is denied in `.htaccess`, so for example logs are
inaccessible through a URL like `http://www.example.com/logs/`.  It is however
RECOMMENDED to move these directories out of the web root entirely.  If a
directory is moved, the path can be set by uncommenting one of the directives
in the `File System` section of the `config.php` configuration file.

| Directory                   | Constant                                 |
| --------------------------- | ---------------------------------------- |
| `/resources/cache/`         | `STORECORE_FILESYSTEM_CACHE_DIR`         |
| `/resources/cache/data/`    | `STORECORE_FILESYSTEM_CACHE_DATA_DIR`    |
| `/resources/cache/objects/` | `STORECORE_FILESYSTEM_CACHE_OBJECTS_DIR` |
| `/resources/cache/pages/`   | `STORECORE_FILESYSTEM_CACHE_PAGES_DIR`   |
| `/logs/`                    | `STORECORE_FILESYSTEM_LOGS_DIR`          |
| `/src/`                     | `STORECORE_FILESYSTEM_SRC_DIR`           |


## 3.4 Logging

By default, StoreCore logs errors, warnings, and noticeable events to `.log`
files in the `/logs/` folder of the StoreCore file system.  Logging MAY be
switched off by enabling the null logger in the global `config.php`
configuration file:

```php
define('STORECORE_NULL_LOGGER', true);
```

However, disabling the default logging mechanism is NOT RECOMMENDED, as all
important events will go unnoticed.  If there is sufficient disk space and the
logging does not have noticeable performance side-effects at the file system
level, the null logger should be disabled.  This is the default configuration
setting:

```php
define('STORECORE_NULL_LOGGER', false);
```


# 4. Performance

This chapter contains several do’s and don’ts on PHP and MySQL performance.
An HTML version is also available at:

https://www.storecore.io/knowledge-base/developer-guides/performance-guidelines


## 4.1 Do your own math

Letting the server recalculate a fixed value over and over again, is lazy.
Simply calculate the fixed value once yourself.  Add a comment if you would
like to clarify a given value.

###### Incorrect:

```php
setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $request->Server['HTTP_HOST']);
```

###### Correct:

```php
setcookie('language', $code, time() + 2592000, '/', $request->Server['HTTP_HOST']);
```

###### Correct:

```php
// Cookie expires in 60 seconds * 60 minutes * 24 hours * 30 days = 2592000 seconds
setcookie('language', $code, time() + 2592000, '/', $request->Server['HTTP_HOST']);
```


## 4.2 Order database table columns for performance

In some databases, it is more efficient to order the columns in a specific
manner because of the way the disk access is performed.  The optimal order of
columns in a MySQL InnoDB table is:

- primary key
- combined primary keys as defined in the `KEY` order
- foreign keys used in `JOIN` queries
- columns with an `INDEX` used in `WHERE` conditions or `ORDER BY` statements
- others columns used in `WHERE` conditions
- others columns used in `ORDER BY` statements
- `VARCHAR` columns with a variable length
- large `TEXT` and `BLOB` columns.

When there are many `VARCHAR` columns (with variable length) in a MySQL table,
the column order MAY affect the performance of queries.  The less close a
column is to the beginning of the row, the more preceding columns the InnoDB
engine should examine to find out the offset of a given one.  Columns that are
closer to the beginning of the table are therefore selected faster.


## 4.3 Store DateTimes as UTC timestamps

Times and dates with times SHOULD be stored in Coordinated Universal Time
(UTC).

###### Incorrect:

```
`date_added`  DATETIME  NOT NULL
```

###### Correct:

```
`date_added`  TIMESTAMP  NOT NULL  DEFAULT CURRENT_TIMESTAMP
```

###### Incorrect:

```
`date_modified`  DATETIME  NOT NULL
```

###### Correct:

```
`date_modified`  TIMESTAMP  NOT NULL  ON UPDATE CURRENT_TIMESTAMP
```

When there are two timestamps, the logical thing to do is setting `date_added`
to `DEFAULT CURRENT_TIMESTAMP` for the initial `INSERT` query and
`date_modified` to `ON UPDATE CURRENT_TIMESTAMP` for subsequent `UPDATE`
queries:

```
`date_added`     TIMESTAMP  NOT NULL  DEFAULT CURRENT_TIMESTAMP
`date_modified`  TIMESTAMP  NULL      DEFAULT NULL  ON UPDATE CURRENT_TIMESTAMP
```

This, however, only works in MySQL 5.6+.  Older versions of MySQL will report
an error: “Incorrect table definition; there can be only one TIMESTAMP column
with CURRENT_TIMESTAMP in DEFAULT or ON UPDATE clause”.


## 4.4 Don’t cast MySQL integers to strings

String equality comparisons are much more expensive than integer compares.
If a database value is an integer, it MUST NOT be treated as a numeric string.
This holds especially true for primary keys and foreign keys.

###### Incorrect:

```php
$sql = "
    UPDATE sc_addresses
    SET customer_id = '" . (int) $customer_id . "'
    WHERE address_id = '" . (int) $address_id . "'";
```

```sql
UPDATE sc_addresses
   SET customer_id = '54321'
 WHERE address_id  = '67890';
```

###### Correct:

```php
$sql = '
    UPDATE sc_addresses
    SET customer_id = ' . (int) $customer_id . '
    WHERE address_id = ' . (int) $address_id;
```

```sql
UPDATE sc_addresses
   SET customer_id = 54321
 WHERE address_id  = 67890;
```


## 4.5 Don’t close and immediately re-open PHP tags

###### Incorrect:

```php
<?php echo $header; ?><?php echo $menu; ?>
```

###### Incorrect:

```php
<?php echo $header; ?>
<?php echo $menu; ?>
```

###### Correct:

```php
<?php
echo $header;
echo $menu;
?>
```

## 4.6 Return results early

Once the result of a PHP method or function has been established, it SHOULD
be returned.  The examples below demonstrate this may save memory and
computations.

###### Incorrect:

```php
public function hasDownload()
{
    $download = false;

    foreach ($this->getProducts() as $product) {
        if ($product['download']) {
            $download = true;
            break;
        }
    }

    return $download;
}
```

###### Correct:

```php
public function hasDownload()
{
    foreach ($this->getProducts() as $product) {
        if ($product['download']) {
            return true;
        }
    }

    return false;
}
```

Adding a temporary variable and two lines of code for a simple `true` or
`false` does not really make sense.  First breaking from an `if` nested in a
`foreach` loop doesn’t make much sense either if you can just as well `return`
the result immediately.

One of the reasons to return results at the end of a PHP method, is that no
`return` can ever be overlooked.  With a single `return` at the end, a method
has a single point of exit, much like the method signature with the function
parameters serves as a single point of entry.

However, if there is indeed a risk that a return might be overlooked, then the
method may be too long.  The `function` then probably can be split in multiple
functions, with each function handling one of the `return` values.

With a type delaration for the return value, introduced in PHP 7, there is need
to go looking for possible `return` values inside a method.  The `$download`
variable in the first, incorrect example illustrates that introducing an extra
variable for the result is no guarantee for clean code: the `return $download`
incorrectly suggests that a download is returned.

###### Correct:

```php
public function hasDownload(): bool
{
    foreach ($this->getProducts() as $product) {
        if ($product['download']) return true;
    }
    return false;
}
```


## 4.7 Shorten single if expressions

When a single `if` evaluation executes a single PHP statement, the full
expression MAY be written as a single line without curly brackets (braces).
This is an exception to the [PHP-FIG coding standards].

###### Correct:

```php
if ($this->defaultStore !== null) {
    return $this->defaultStore;
}
```

###### Correct:

```php
if ($this->defaultStore !== null) return $this->defaultStore;
```

###### Incorrect:

```php
if ($this->defaultStore !== null):
    return $this->defaultStore;
```

###### Incorrect:

```php
if ($this->defaultStore !== null)
    return $this->defaultStore;
```

[PHP-FIG coding standards]: https://www.php-fig.org/per/coding-style/ "PER Coding Style"


# 5. PHP development guidelines

## 5.1 Exceptions

Models and controllers SHOULD NOT terminate a script with an exit.  Use an
exception instead, so the application using the model or controller may respond
to the failure.  If the exception is not caught, it will result in a “Fatal
error: Uncaught exception.”

###### Incorrect:

```php
if (!file_exists($file)) {
    exit('Could not load file: ' . $file);
}
```

###### Correct:

```php
if (!file_exists($file)) {
    throw new \Exception('Could not load file: ' . $file);
}
```

In many cases it is RECOMMENDED to throw a more specific [Standard PHP Library
(SPL) exception], for example a [runtime exception] if a file only exists after
it was saved by an application.

###### Correct:

```php
if (!file_exists($file)) {
    throw new \RuntimeException('Could not load file: ' . $file);
}
```

[Standard PHP Library (SPL) exception]: https://www.php.net/manual/en/spl.exceptions.php
[runtime exception]: https://www.php.net/manual/en/class.runtimeexception.php


## 5.2 Shared data

StoreCore data is shared through the [service locator design pattern].
The centralized registry is the only link between applications and controllers.

At any given time there should be only one single instance of the registry.
The StoreCore registry is therefore implemented using the [singleton design
pattern].  Because the registry implements a `SingletonInterface`, it cannot be
instantiated.  Instead you should call the static `getInstance()` method.

###### Incorrect:

```php
$registry = new \StoreCore\Registry();
```

###### Correct:

```php
$registry = \StoreCore\Registry::getInstance();
```

[service locator design pattern]: https://en.wikipedia.org/wiki/Service_locator_pattern "Service locator pattern"

[singleton design pattern]: https://en.wikipedia.org/wiki/Singleton_pattern "Singleton pattern"


### 5.2.1 MVC models and controllers

Framework MVC controllers SHOULD extend the abstract core class
`StoreCore\AbstractController`.  Likewise MVC models MAY extend the abstract
class `StoreCore\AbstractModel`:

```php
namespace StoreCore;

class FooController extends AbstractController
{
    // <...>
}

class FooModel extends AbstractModel
{
    // <...>
}
```

If a model needs access to the database, it MAY extend the `AbstractModel`
from the `StoreCore\Database` namespace.  Therefore there a two abstract
prototypes for models available, one without and one with a database
connection:

```php
class FooModel extends \StoreCore\AbstractModel
{
    // <...>
}

class BarModel extends \StoreCore\Database\AbstractModel
{
    // <...>
}
```

The StoreCore abstract DAO (Data Access Object) provides a CRUD interface to
Create, Read, Update, and Delete database records.  It executes the four basic
SQL data manipulation operations: `INSERT`, `SELECT`, `UPDATE`, and `DELETE`.
Model classes that extend the abstract DAO MUST provide two class constants for
late static bindings: a `TABLE_NAME` with the database table name the model
operates on and a `PRIMARY_KEY` for the primary key column of this table.


### 5.2.2 Shared core services

The following objects are provided as services in the StoreCore registry:

| Service    | Implemented interface              | Instance of class                          |
| ---------- | ---------------------------------- | ------------------------------------------ |
| Autoloader | ·                                  | StoreCore\Autoloader                       |
| Clock      | Psr\Clock\ClockInterface           | StoreCore\Clock                            |
| Database   | ·                                  | StoreCore\Database\Connection              |
| Logger     | Psr\Log\LoggerInterface            | StoreCore\FileSystem\Logger                |
| Request    | Psr\Http\Message\RequestInterface  | StoreCore\Engine\ServerRequest             |
| Response   | Psr\Http\Message\ResponseInterface | StoreCore\Engine\Response                  |
| Route      | ·                                  | StoreCore\Route                            |
| Server     | Psr\Container\ContainerInterface   | StoreCore\Engine\ServerContainer           |
| Session    | Psr\SimpleCache\CacheInterface     | StoreCore\Session                          |
| Store      | StoreCore\IdentityInterface        | StoreCore\Store                            |
| URI        | Psr\Http\Message\UriInterface      | StoreCore\Engine\UniformResourceIdentifier |

Six of these services are loaded by the boot loader and are always available:

- [`Autoloader`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Autoloader.php)
- [`Clock implements ClockInterface`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Clock.php)
- [`Logger implements LoggerInterface`](https://gitlab.com/storecore/storecore/-/blob/develop/src/FileSystem/Logger.php)
- [`Request implements RequestInterface`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Engine/Request.php)
- [`Server implements ContainerInterface`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Engine/ServerRequest.php)
- [`URI implements UriInterface`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Engine/UniformResourceIdentifier.php)

The main front controller then loads:

- [`Route`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Route.php)
- [`Session`](https://gitlab.com/storecore/storecore/-/blob/develop/src/Session.php)


### 5.2.3 MVC class synopses

#### MVC interfaces

```php
interface Psr\Container\ContainerInterface {
    public get(string $id): mixed
    public has(string $id): bool
}

interface StoreCore\IdentityInterface {
    public getIdentifier(): StoreCore\Types\UUID|null;
    public hasIdentifier(): bool;
    public setIdentifier(StoreCore\Types\UUID|string $uuid): void;
}

interface StoreCore\RepositoryInterface
extends Psr\Container\ContainerInterface
{
    public get(string $id): mixed
    public has(string $id): bool
    public set(StoreCore\IdentityInterface &$entity): string;
    public unset(string $id): bool;
}

interface StoreCore\SingletonInterface {
    public static getInstance(): self
}

interface StoreCore\Database\CRUDInterface {
    public create(array $keyed_data): string|false;
    public read(mixed $value, ?string $key = null): array|false;
    public update(array $keyed_data, ?string $where_clause = null): bool;
    public delete(mixed $value, ?string $key = null): bool;
}
```


#### Abstract MVC models

```php
abstract class StoreCore\AbstractContainer
implements Psr\Container\ContainerInterface {
    public get(string $id): mixed
    public has(string $id): bool
}

abstract class StoreCore\AbstractModel {
    public __construct(StoreCore\Registry $registry )
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
}

abstract class StoreCore\Database\AbstractModel
extends StoreCore\AbstractModel {
    public __construct(StoreCore\Registry $registry)
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
}

abstract class StoreCore\Database\AbstractDataAccessObject
extends StoreCore\Database\AbstractModel,
implements StoreCore\Database\CRUDInterface {
    public create(array $keyed_data): string|false
    public delete(mixed $value, ?string $key = null): bool
    public read(mixed $value, ?string $key = null): array|false
    public update(array $keyed_data, ?string $where_clause = null): bool
}
```


#### MVC models

```php
class StoreCore\Registry extends StoreCore\AbstractContainer
implements StoreCore\SingletonInterface, Psr\Container\ContainerInterface {
    public get(string $key): mixed
    public static getInstance(): self
    public has(string $key): bool
    public set(string $key , mixed $value): void
}

class StoreCore\Session
implements Psr\SimpleCache\CacheInterface, StoreCore\SubjectInterface {
    public __construct(int $idle_timeout = 900)
    public clear(): bool
    public delete(string $key): bool
    public deleteMultiple(iterable $keys): bool
    public destroy(): bool
    public get(string $key, mixed $default = null): mixed
    public getMultiple(iterable $keys, mixed $default = null): iterable
    public has(string $key): bool
    public regenerate(): bool
    public set(string $key, mixed $value, null|int|\DateInterval $ttl = null): bool
    public setMultiple(iterable $values, null|int|\DateInterval $ttl = null): bool
}
```


#### MVC views

```php
class StoreCore\View implements \Stringable {
    public __construct(?string $template = null, ?array $values = null)
    public __set(string $name, mixed $value): void
    public __toString(): string
    public mapTemplate(): void
    public $this setTemplate(string $template): void
    public $this setValues(array $values): void
    public string render(): string
}

class StoreCore\AMP\Page
extends StoreCore\View implements \Stringable {
    public getTheme(): string
    public write(\Stringable|string $html): void

    /* Inherited methods */
    public __construct(?string $template = null, ?array $values = null)
    public __set(string $name, mixed $value): void
    public __toString(): string
    public mapTemplate(): void
    public $this setTemplate(string $template): void
    public $this setValues(array $values): void
    public string render(): string
}
```


#### MVC controllers

```php
class StoreCore\AbstractController {
    public __construct(StoreCore\Registry $registry )
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
}

class StoreCore\Admin\Controller
extends AbstractController {
    public __construct(StoreCore\Registry $registry )
    public __get(string $key): mixed
    public __set(string $key, mixed $value ): void
}
```


### 5.2.4 Reserved session variables

There are three session variable names that SHOULD NOT be used.  These are
reserved for internal use by the core `Session` class.  If one of these
reserved names is used, the `Session::set()` method will throw an invalid
argument exception:

- `string $_SESSION['HTTPS']`
- `string $_SESSION['HTTP_USER_AGENT']`
- `array $_SESSION['SESSION_OBJECT_POOL']`

The reserved `$_SESSION['HTTPS']` and `$_SESSION['HTTP_USER_AGENT']` session
variables contain copies of the global `$_SERVER['HTTPS']` and
`$_SERVER['HTTP_USER_AGENT']` server variables.  The `$_SESSION['HTTPS']` is
used to regenerate the session ID if the client connection switches from
plain HTTP to HTTP Secure (HTTP/S) and back.  The `$_SESSION['HTTP_USER_AGENT']`
variable is used to notice any changes of the HTTP client or the client
configuration.


### 5.2.5 Global session objects and variables

| Name     | Type   | Class or Method                            |
| -------- | ------ | ------------------------------------------ |
| Language | string | `StoreCore\I18N\Locale::load()`            |
| Token    | string | `StoreCore\Types\FormToken::getInstance()` |
| User     | object | `StoreCore\User`                           |


# 6. Internationalization (I18N) and localization (L13N)

## 6.1 Limitations of MVC-L

From the outset we decided multilingual support of [European languages]
SHOULD be a key feature of an open-source e-commerce community operating from
Europe.  Support of multiple languages would no longer be an option, but
a MUST.  For companies operating in bilingual and multilingual European
countries like Belgium, Luxembourg, Finland, and Switzerland this may of
course be an critical key feature too.

[European languages]: https://en.wikipedia.org/wiki/Languages_of_Europe "Languages of Europe"

The traditional MVC-L (model-view-controller–language) application structure
adds severe limitations to performance, maintenance, and scalability.
For example, a single language adds over 350 files in about 40 directories to
an OpenCart install.  If the [OpenCart MVC-L implementation] is expanded to
four or even more languages, file management becomes a dreadful task.

There are performance side-effects if a single MVC view consists of not only
one template file, but also several language files for all supported languages.

Furthermore, *consistency* within one language is difficult to maintain if
terms are spread out over dozens of language files.  For example, if the store
manager wants to change *shopping cart* to *shopping basket*, a developer will
have go over several files.  A more centralized approach with an end-user
interface for editing seems a much wiser choice.

[OpenCart MVC-L implementation]: http://docs.opencart.com/display/opencart/Introduction+to+MVC-L


## 6.2 Translation memory (TM)

StoreCore uses a [translation memory (TM)] to handle and maintain all language
strings.  The translation memory database table is defined in the main SQL DDL
(Data Definition Language) file `core-mysql.sql` for MySQL.  The translations
are in a separate SQL DML (Data Manipulation Language) file called
`i18n-dml.sql`.

[translation memory (TM)]: https://en.wikipedia.org/wiki/Translation_memory "Translation memory (TM)"


### 6.2.1 Root: Core or Master Languages

*Core languages*, or *masters*, are root-level language packs.  They SHOULD NOT
be deleted, which is prevented by a foreign key constraint `fk_language_id` on
the self-referencing key `parent_id`.  If the `language_id` is equal to the
`parent_id`, a language has no parent and is therefore a core language located
at the root of the language family tree.

Currently, the core supports four European master languages.  These are defined
in the `SUPPORTED_LANGUAGES` constant of the `Locale` class:

- `de-DE` for German
- `en-GB` for English
- `fr-FR` for French
- `nl-NL` for Dutch

If no language match is found, StoreCore defaults to `en-GB` for British
English.

Master languages cannot be deleted; they can only be disabled.  If you do try
to delete a master language from the database, the `DELETE` query fails on a
foreign key constraint.

###### Incorrect:

```sql
DELETE FROM sc_languages
      WHERE iso_code = 'de-DE';
```

###### Correct:

```sql
UPDATE sc_languages
   SET status = 0
 WHERE iso_code = 'de-DE';
```


### 6.2.2 Tree and branches: secondary languages

*Secondary languages* are derived from the core/master languages.  They only
contain differences with the master language.  For example, the “English -
United States” (en-US) language pack only contains the differences between
American English and British English in its “English - United Kingdom”
(en-GB) master.  This allows for global localization while maintaining language
consistency and a concise dictionary.


## 6.3 Content language negotiation

StoreCore uses the HTTP `Accept-Language` header to determine which content
language is preferred by visitors, customers, users, and client applications.
The current language can be found by supplying an array of supported languages
to the `AcceptLanguage::negotiate()` method.

```php
StoreCore\I18N\AcceptLanguage {
    public negotiate(array $supported, StoreCore\Types\LanguageCode $default = null): StoreCore\Types\LanguageCode
}
```

The `$supported` parameter must be an associative array of ISO language codes
that evaluate to `true`.  For example, if an application supports both English
and French, the supported languages may be defined as:

```php
$supported = array(
    'en-GB' => true,
    'fr-FR' => true,
);
```

This data structure allows you to temporarily disable a supported language,
without fully dropping it:

```php
$supported = array(
    'en-GB' => true,
    'fr-FR' => false,
);
```


## 6.4 Translation guidelines

### 6.4.1 Language components

The translations memory contains seven components, divided into two groups.
These groups are namespaced with an uppercase prefix.  The first group contains
basic language constructs in plain text, without any formatting:

- `ADJECTIVE` for adjectives
- `NOUN` for nouns and names
- `VERB` for verbs.

The second group is used in user interfaces and MAY contain formatting,
usually HTML5:

- `COMMAND` for menu commands and command buttons
- `ERROR` for error messages
- `HEADING` for headings and form labels
- `TEXT` for anything else.


### 6.4.2 Compound nouns

Compound nouns are handled as single nouns.  For example, *shopping cart* is
not stored as two terms like `NOUN_SHOPPING` plus `NOUN_CART`, but as a single
segment like `NOUN_SHOPPING_CART`.


### 6.4.3 Names as nouns

Names are treated as nouns.  Therefore they contain the default `NOUN` prefix,
for example `NOUN_PAYPAL` for *PayPal* and `NOUN_MYSQL` for *MySQL*.


### 6.4.4 Verbs to commands

Commands, menu commands and command buttons usually indicate an activity.
Therefore commands SHOULD be derived from verbs.  The translation memory SQL
file contains an example of this business logic.  The general verb *print*
in lowercase becomes the command **Print…** with an uppercase first letter and
three dots in user interfaces.

```sql
INSERT IGNORE INTO sc_translation_memory
    (translation_id, language_id, translation)
  VALUES
    ('VERB_PRINT',   0, 'print'),
    ('VERB_PRINT',   1, 'printen'),
    ('VERB_PRINT',   2, 'drucken'),
    ('VERB_PRINT',   3, 'imprimer');

INSERT IGNORE INTO sc_translation_memory
    (translation_id, language_id, translation)
  VALUES
    ('COMMAND_PRINT',   0, 'Print…'),
    ('COMMAND_PRINT',   1, 'Printen…'),
    ('COMMAND_PRINT',   2, 'Drucken…'),
    ('COMMAND_PRINT',   3, 'Imprimer…');
```

In some cases verbs are included in the translation memory for reference
purposes and consistency.  For example, the verb *to print* may in Dutch be
translated as *printen*, *afdrukken*, or *drukken*.  The definition `afdrukken`
of `VERB_PRINT` thus indicates the preferred translation.


### 6.4.5 Errors and exceptions

Error messages and exception message strings currently are not translated as
these are intended primarily for developers and server administrators.


# 7. Documentation

## 7.1 Copyright and version DocBlock

If a file is brought in from another open-source project, a DocBlock MUST be
added that clearly names the origin in the `@copyright` tag.

Example:

```
/**
 * @copyright Copyright (c) 2009-2014 FooBar
 */
```

If a component of the FooBar project is refactored, the unchanged code
establishes a baseline.  If this component does not use SemVer versioning, the
unchanged baseline MUST be denoted as `@version 0.0.0` for reference purposes:

```
/**
 * @copyright Copyright (c) 2009-2014 FooBar
 * @version   0.0.0
 */
```

Once the file is changed, copyright (or, actually, copyleft) is extended to
the StoreCore framework:

```
/**
 * @copyright Copyright (c) 2023 StoreCore
 * @copyright Portions copyright (c) 2009-2015 FooBar
 * @version   0.1.0
 */
```

Please note that documenting changes and proper attribution is REQUIRED by the
GNU General Public License (GPL):

*For the developers’ and authors’ protection, the GPL clearly explains that
there is no warranty for this free software.  For both users’ and authors’
sake, the GPL requires that modified versions be marked as changed, so that
their problems will not be attributed erroneously to authors of previous
versions.)*


## 7.2 Class versions

All abstract classes and classes MUST include a `VERSION` constant.  Because
PHP class constants are always accessible outside the class scope, this allows
for updates and possibly handling future compatibility issues.  For reference
purposes the `const` definition is usually included on the first line in the
`class` definition:

```php
<?php
class FooBar
{
    public const string VERSION = '0.1.0-alpha.1';

    // <...>
}
```

If the class file contains an file-level DocBlock or class-level DocBlock, the
PHPDoc `@version` tag MUST be set to the currently defined `VERSION`:

```php
<?php

declare(strict_types=1);

/**
 * Foo Bar
 *
 * @version 0.1.0-alpha.1
 */
class FooBar
{
    public const string = '0.1.0-alpha.1';

    // <...>
}
```

The [`SetupInterface` interface] in the `StoreCore\Modules` namespace
additionally prescribes the implementation of a `getVersion()` method.
This formalizes an important reminder: classes and modules MUST include
a publicly accessible version ID.

```php
<?php

declare(strict_types=1);

namespace StoreCore\Modules;

interface SetupInterface
{
    public function getVersion(): string;
    public function install(): bool;
    public function uninstall(): bool;
}
```

[`SetupInterface` interface]: https://gitlab.com/storecore/storecore/-/blob/develop/src/Modules/SetupInterface.php "StoreCore\Modules\SetupInterface"


# A. StoreCore class reference

## A.1 Interfaces and traits

### ContainerInterface

```php
interface ContainerInterface {
    public get(string $id): mixed
    public has(string $id): bool
}
```


### CRUDInterface

```php
interface CRUDInterface {
    public create(array $keyed_data): string|false;
    public read(mixed $value, ?string $key = null): array|false;
    public update(array $keyed_data, ?string $where_clause = null): bool;
    public delete(mixed $value, ?string $key = null): bool;
}
```


### IdentityInterface and IdentityTrait

```php
interface StoreCore\IdentityInterface {
    public getIdentifier(): StoreCore\Types\UUID|null
    public hasIdentifier(): bool
    public setIdentifier(StoreCore\Types\UUID|string $uuid): void
}

trait StoreCore\IdentityTrait
implements StoreCore\IdentityInterface {
    public getIdentifier(): StoreCore\Types\UUID|null
    public hasIdentifier(): bool
    public setIdentifier(StoreCore\Types\UUID|string $uuid): void
}
```


### SetupInterface

```php
interface StoreCore\Modules\SetupInterface {
    public getVersion(): string
    public install(): bool
    public function uninstall(): bool
}
```


### ValidateInterface

```php
interface StoreCore\Types\ValidateInterface {
    public static validate(mixed $variable): bool
}
```


## A.2 Abstract classes

### AbstractContainer

```php
abstract class StoreCore\AbstractContainer
implements Psr\Container\ContainerInterface {
    public get(string $id): mixed
    public function has(string $id): bool
}
```


### AbstractController

```php
abstract class StoreCore\AbstractController {
    public __construct(StoreCore\Registry $registry)
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
}
```


### AbstractDataAccessObject

```php
abstract class StoreCore\Database\AbstractDataAccessObject
extends StoreCore\Database\AbstractModel
implements StoreCore\Database\CRUDInterface {
    public create(array $keyed_data): string|false
    public delete(mixed $value, ?string $key = null): bool
    public read(mixed $value, ?string $key = null): array|false
    public update(array $keyed_data, ?string $where_clause = null): bool
}
```


### AbstractModel

```php
abstract class StoreCore\AbstractModel {
    public __construct(StoreCore\Registry $registry)
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
}
```


### Registry

```php
final class StoreCore\Registry
extends StoreCore\AbstractContainer
implements StoreCore\SingletonInterface, Psr\Container\ContainerInterface {
    public get(string $id): mixed
    public static getInstance(): self
    public has(string $id): bool
    public set(string $id, mixed $value): void
}
```


## A.3 @package StoreCore\Core

### Configurator

```php
class StoreCore\Admin\Configurator {
    public __construct()
    public save(): bool
    public set(string $name, mixed $value): void
    public static write(string $name, mixed $value): bool
}
```


### Store

```php
class StoreCore\Store
extends StoreCore\AbstractModel
implements StoreCore\IdentityInterface {

    /* Properties */
    public \DateTimeZone $dateTimeZone;
    public string|null $name = null;

    /* Methods */
    public __construct(StoreCore\Registry $registry)
    public __get(string $key): mixed
    public __set(string $key, mixed $value): void
    public close(): void
    public getCurrencies(): array
    public getIdentifier(): StoreCore\Types\UUID|null
    public getLanguages(): array
    public hasIdentifier(): bool
    public isDefault(): bool
    public isOpen(): bool
    public open(): void
    public setAsDefault(): void
    public setCurrencies(array $store_currencies): void
    public setIdentifier(StoreCore\Types\UUID|string $uuid): void
    public setLanguages(array $store_languages): void
}
```


### StoreMapper

```php
class StoreCore\Database\StoreMapper
extends StoreCore\Database\AbstractDataAccessObject
implements StoreCore\Database\CRUDInterface {
    public create(array $keyed_data): string|false
    public delete(mixed $value, ?string $key = null): bool
    public getNames(): array|false
    public getStoreByID(int $store_id): StoreCore\Store|null
    public getStoreByUUID(StoreCore\Types\UUID $store_uuid): StoreCore\Store|null
    public read(mixed $value, ?string $key = null): array|false
    public save(StoreCore\Store &$store): bool
    public update(array $keyed_data, ?string $where_clause = null): bool
}
```


### StoreRepository

```php
class StoreCore\Database\StoreRepository
extends StoreCore\Database\StoreMapper
implements \Countable, Psr\Container\ContainerInterface {
    public add(StoreCore\Store &$store): bool
    public count(): int
    public create(array $keyed_data): string|false
    public delete(mixed $value, ?string $key = null): bool
    public get(string $id): StoreCore\Store
    public getCurrentStore(): StoreCore\Store
    public getDefaultStore(): StoreCore\Store
    public getStoreByID(int $store_id): StoreCore\Store|null
    public getStoreByUUID(StoreCore\Types\UUID $store_uuid): StoreCore\Store|null
    public has(string $id): bool
    public read(mixed $value, ?string $key = null): array|false
    public update(array $keyed_data, ?string $where_clause = null): bool
}
```


# B. StoreCore configuration settings

List of global constants in alphabetical order.


#### (bool) STORECORE_BI = false

The constant `STORECORE_BI` is a global privacy setting that determines if
StoreCore MAY collect usage statistics (`true`) or not (default `false`) for
Business Intelligence (BI).


#### (bool) STORECORE_BI_GOOGLE_ANALYTICS = false

Boolean value to enable (`true`) or disable (default `false`) Google Analytics.
All other settings for Google Analytics are ignored if this constant is set to
`false`.


#### (bool|int) STORECORE_CMS_BLOCKLIST = false

Enables the StoreCore IP blocklist (`true`) or disables it (default `false`).
A setting of `(int) 0` indicates that the blocklist is used (just like `true`)
but currently contains no active IP bans and therefore can be skipped (just
like `false`).

If you use some other webserver mechanism to block IP addresses, such as a
deny list in `.htaccess`, the StoreCore CMS blocklist SHOULD be disabled to
prevent unnecessary checks and improve performance.


#### (string) STORECORE_DEFAULT_STORE

OPTIONAL universally unique identifier (UUID) of the *default store* or
*main store*: the store for which transactions and data are processed when
no other store is provided.  This UUID is defined automatically when a default
store is saved with the `save()` method of the database `StoreMapper`.


#### (string) STORECORE_LANGUAGES

OPTIONAL JSON encoded array of supported and enabled languages.  The array keys
are language codes of supported languages and the boolean array values determine
if these languages are enabled (`true`) or disabled (`false`).
